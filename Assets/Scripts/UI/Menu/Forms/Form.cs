using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CITG.UI.Menu
{
    public class Form : MonoBehaviour
    {

        [Inject]
        protected void Construct() => gameObject.SetActive(false);
    }
}