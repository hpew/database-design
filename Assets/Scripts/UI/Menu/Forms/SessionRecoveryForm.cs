using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Cysharp.Threading.Tasks;
using CITG.Utility;
using CITG.DataSystem;

namespace CITG.UI.Menu
{
    public class SessionRecoveryForm : MonoBehaviour
    {
        [SerializeField] Text _userData;

        CancelButton _cancelButton;
        ConfirmButton _confirmButton;

        [Inject]
        void Construct()
        {
            gameObject.SetActive(false);
            _confirmButton = gameObject.GetComponentInChildren<ConfirmButton>(true);
            _cancelButton = gameObject.GetComponentInChildren<CancelButton>(true);
        }

        public async UniTask<bool> Show(UserData data)
        {
            gameObject.SetActive(true);

            _userData.text = $"���: {data.FIO}\n" +
                $"������: {data.Group}\n" +
                $"�������� ������: {data.CreditBook}\n";

            var selectedButton = await ButtonObserver.Track(_confirmButton, _cancelButton);

            var result = false;
            if (selectedButton == _confirmButton) result = true;

            gameObject.SetActive(false);

            return result;
        }
    }
}