using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.Menu
{
    public class TaskForm : Form
    {
        UI.ExitButton _exitButton;

        [Inject]
        void Init() => _exitButton = gameObject.GetComponentInChildren<UI.ExitButton>(true);

        public async UniTask Show()
        {
            gameObject.SetActive(true);

            await ButtonObserver.Track(_exitButton);
            
            gameObject.SetActive(false);
        }
    }
}