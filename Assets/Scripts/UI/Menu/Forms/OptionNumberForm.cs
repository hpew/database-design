using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.Menu
{
    public class OptionNumberForm : Form
    {
        [SerializeField] Text _number;
        ConfirmButton _confirmButton;

        [Inject]
        void Init() => _confirmButton = gameObject.GetComponentInChildren<ConfirmButton>(true);

        public async UniTask Show(int number)
        {
            gameObject.SetActive(true);

            _number.text = $"� {number}";

            await ButtonObserver.Track(_confirmButton);

            gameObject.SetActive(false);
        }

        public void Disappear() => gameObject.SetActive(false);
    }
}