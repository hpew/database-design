using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using Zenject;
using Sirenix.OdinInspector;
using CITG.System;

namespace CITG.UI.Menu
{
    public class InputFieldAuthorizationForm : InputField
    {
        [SerializeField] Text _warningLabel;
        [SerializeField] TypeInput type;

        [Inject]
        void Init(Assets assets)
        {
            _simpleSprite = assets.InputFieldAuthorizationFormSprites.SpriteSimple;
            _borderSprite = assets.InputFieldAuthorizationFormSprites.SpriteBorder;
        }


        Sprite _simpleSprite;
        Sprite _borderSprite;


        public bool IsComplete { get; private set; }

        static Dictionary<TypeInput, string> _patternsValidate = new Dictionary<TypeInput, string>()
        {
            //[TypeInput.FIO] = @"+",//@"^[�-��-�]+\s+[�-��-�]+\s+[�-��-�]+$",
            [TypeInput.Group] = @"^[A-�]{3}-([1-5]-)?[0-9]{3}$",
            [TypeInput.CreditBook] = @"[0-9]+",
        };

        static Dictionary<TypeInput, string> _warnings = new Dictionary<TypeInput, string>()
        {
            //[TypeInput.FIO] = "*������ ��������� � ������",
            [TypeInput.Group] = "*������ ��������� ����� ���������, -, �����",
            [TypeInput.CreditBook] = "*������ �����",
        };

        static string WarningEmptyField = "*������������ ���� ��� �����";

        [Inject]
        private void Construct()
        {
            IsComplete = false;
            _warningLabel.gameObject.SetActive(false);

            image.sprite = _borderSprite;

            onEndEdit.AddListener(input => ValidateInput(input));
        }

        public void Clear()
        {
            text = "";
            IsComplete = false;
            _warningLabel.gameObject.SetActive(false);
            image.sprite = _simpleSprite;
        }

        public void ValidateCurrentStatus() => ValidateInput(text);


        private void ValidateInput(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                IsComplete = false;
                _warningLabel.text = WarningEmptyField;
                _warningLabel.gameObject.SetActive(true);
                image.sprite = _borderSprite;
            }
            else
            {
                var result = type == TypeInput.FIO ? true : Regex.IsMatch(input, _patternsValidate[type]);
                if (!result)
                {
                    IsComplete = false;
                    _warningLabel.text = _warnings[type];
                    _warningLabel.gameObject.SetActive(true);
                    image.sprite = _borderSprite;
                }
                else
                {
                    IsComplete = true;
                    _warningLabel.gameObject.SetActive(false);
                    image.sprite = _simpleSprite;
                }
            }
        }

        public enum TypeInput
        {
            FIO,
            Group,
            CreditBook
        }
    }
}