using System;
using UnityEngine.UI;
using Cysharp.Threading.Tasks;
using Zenject;
using CITG.DataSystem;
using CITG.Utility;

namespace CITG.UI.Menu
{
    public class AuthorizationForm : Form
    {
        UI.ExitButton _exitButton;
        ConfirmButton _confirmButton;
        Toggle _toggle;

        InputFieldAuthorizationForm _FIOInputField;
        InputFieldAuthorizationForm _groupInputField;
        InputFieldAuthorizationForm _creditBookInputField;

        [Inject] //�������� InputFieldAuthoriazationForm.Mode �� ������ �������� �������
        private void Init([Inject(Id = "FIO Input Field")] InputFieldAuthorizationForm FIOInputField, 
            [Inject(Id = "Group Input Field")] InputFieldAuthorizationForm groupInputField, 
            [Inject(Id = "CreditBook Input Field")] InputFieldAuthorizationForm creditBookInputField)
        {
            _exitButton = gameObject.GetComponentInChildren<UI.ExitButton>(true);
            _confirmButton = gameObject.GetComponentInChildren<ConfirmButton>(true);
            _toggle = gameObject.GetComponentInChildren<Toggle>(true);

            _FIOInputField = FIOInputField;
            _groupInputField = groupInputField;
            _creditBookInputField = creditBookInputField;
        }

        public async UniTask<Result> Show()
        {
            gameObject.SetActive(true);

            _FIOInputField.Clear();
            _groupInputField.Clear();
            _creditBookInputField.Clear();

            _toggle.isOn = false;
            _toggle.onValueChanged.AddListener(result => _confirmButton.interactable = result);
            _confirmButton.interactable = false;

            Result result;
            while (true)
            {
                var selectedButton = await ButtonObserver.Track(_confirmButton, _exitButton);

                if (selectedButton == _confirmButton)
                {
                    _FIOInputField.ValidateCurrentStatus();
                    _groupInputField.ValidateCurrentStatus();
                    _creditBookInputField.ValidateCurrentStatus();
                    if (_FIOInputField.IsComplete && _groupInputField.IsComplete && _creditBookInputField.IsComplete)
                    {
                        result = new Result(_FIOInputField.text, _groupInputField.text, _creditBookInputField.text);
                        break;
                    }
                }
                else if (selectedButton == _exitButton)
                {
                    result = Result.CancelResult;
                    break;
                }
            }

            _toggle.onValueChanged.RemoveAllListeners();

            gameObject.SetActive(false);

            return result;
        }

        public struct Result
        {
            public string FIO { get; private set; }
            public string Group { get; private set; }
            public string CreditBook { get; private set; }
            public bool IsCancel { get; private set; }

            public static Result CancelResult => new Result() { IsCancel = true };

            public Result(string FIO, string group, string creditBook)
            {
                this.FIO = FIO;
                Group = group;
                CreditBook = creditBook;

                IsCancel = false;
            }

        }
    }
}