using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CITG.UI.Menu
{

    public class GroupButtons : MonoBehaviour
    {
        [field: SerializeField] public Button StartTestButton { get; private set; }
        [field: SerializeField] public Button InfotTestButton { get; private set; }
        [field: SerializeField] public Button ExitTestButton { get; private set; }
    }
}