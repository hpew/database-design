using System;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.Menu
{

    public class PreviewWindow : MonoBehaviour
    {
        StartTestButton _startButton;
        TaskDescriptionButton _taskButton;
        ExitButton _exitButton;

        [Inject]
        void Construct(StartTestButton startButton, TaskDescriptionButton taskButton, ExitButton exitButton)
        {
            _startButton = startButton;
            _taskButton = taskButton;
            _exitButton = exitButton;

#if UNITY_WEBGL && !UNITY_EDITOR

            _exitButton.SetActive(false);

            var position = new Vector3(-180, -0.5f, 0);
            _startButton.transform.localPosition = position;

            position.x = 180;
            _taskButton.transform.localPosition = position;
#else
            var position = new Vector3(-362, -0.5f, 0);
            _startButton.transform.localPosition = position;

            position.x = 0;
            _taskButton.transform.localPosition = position;

            position.x = 362;
            _exitButton.transform.localPosition = position;
#endif
        }
    }
}