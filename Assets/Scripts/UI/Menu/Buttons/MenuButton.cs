using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;

namespace CITG.UI.Menu
{
    public abstract class MenuButton<T> : Button
        where T : MenuButton<T>
    {
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            MessageBroker.Default.Publish(new MessageClickedMenuButton<T>((T)this));
        }

    }
    public class MessageClickedMenuButton<T> where T : MenuButton<T>
    {
        public T Button { get; private set; }

        public MessageClickedMenuButton(T button) => Button = button;
    }

}