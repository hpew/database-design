using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI
{
    public class Glass : MonoBehaviour
    {
        RectTransform _rectTransform;

        [SerializeField] RectTransform _loweredRectTransform;
        [SerializeField] Image _openImage;

        [SerializeField] float timeAnimationMove = 0.25f;
        [SerializeField] float timeAnimationDissolve = 0.1f;

        Vector2 loweredSizeDelta;

        [Inject]
        void Construct([Inject(Id = "Canvas")]CanvasScaler scaler)
        {
            _rectTransform = gameObject.GetComponent<RectTransform>();

            _openImage.SetActive(false);
            _loweredRectTransform.SetActive(true);

            loweredSizeDelta = _loweredRectTransform.sizeDelta;
        }

        public async UniTask Open()
        {
            var openSizeDelta = new Vector2(loweredSizeDelta.x, _rectTransform.GetSize().y);
            await _loweredRectTransform.LerpSizeDelta(loweredSizeDelta, openSizeDelta, timeAnimationMove);

            _openImage.SetActive(true);
            await _openImage.LerpAlpha(0, 1, timeAnimationDissolve);

            _loweredRectTransform.SetActive(false);
        }

        public async UniTask Close()
        {
            _loweredRectTransform.sizeDelta = new Vector2(_loweredRectTransform.sizeDelta.x, _rectTransform.GetSize().y);
            _loweredRectTransform.SetActive(true);

            await _openImage.LerpAlpha(1, 0, timeAnimationDissolve);
            _openImage.SetActive(false);

            await _loweredRectTransform.LerpSizeDelta(_loweredRectTransform.sizeDelta, loweredSizeDelta, timeAnimationMove);
        }

        public void Show()
        {
            _openImage.SetActive(true);
            _loweredRectTransform.SetActive(false);
        }

        public void Unshow()
        {
            _loweredRectTransform.SetActive(true);
            _openImage.SetActive(false);
        }
        
    }
}