using System;
using UnityEngine;
using Zenject;

namespace CITG.UI
{
    public class GroupSwitchButtons : MonoBehaviour, IInitializable
    {
        SwitchButton[] _buttons;

        SwitchButton[] _workButtons;
        SwitchButton _activeButton;

        Action<int> _handlerOnClick;

        [Inject]
        void Construct() => _buttons = gameObject.GetComponentsInChildren<SwitchButton>();

        public void Initialize()
        {
            Unactivate();
            for (int i = 0; i < _buttons.Length; i++) SubscribeButton(_buttons[i], i);
        }

        public void Activate(int count, Action<int> handlerOnClick)
        {
            if (count < 0 || count > _buttons.Length) throw new Exception();

            _handlerOnClick = handlerOnClick;

            _workButtons = new SwitchButton[count];
            for (int i = 0; i < count; i++)
            {
                _workButtons[i] = _buttons[i];
                _workButtons[i].Enable();
            }

            _activeButton = _workButtons[0];
            _activeButton.SwitchOn();

            gameObject.SetActive(true);
        }
        public void Unactivate()
        {
            _handlerOnClick = null;
            _workButtons = null;
            for (int i = 0; i < _buttons.Length; i++) _buttons[i].Disable();
            Unshow();
        }

        public void Show() => gameObject.SetActive(false);
        public void Unshow() => gameObject.SetActive(false);

        private void SubscribeButton(SwitchButton button, int index)
        {
            button.onClick.AddListener(() =>
            {
                if (_handlerOnClick == null) return;

                _activeButton?.SwitchOff();
                _activeButton = button;
                _activeButton.SwitchOn();
                _handlerOnClick(index);
            });
        }
    }
}