using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;
using CITG.Utility;

namespace CITG.UI
{
    public class SwitchButton : Button
    {
        SpritesSwitchButton sprites;

        [Inject]
        void Construct(Assets assets) => sprites = assets.SpritesSwitchButton;

        public void SwitchOn() => image.sprite = sprites.ActiveSprite;
        public void SwitchOff() => image.sprite = sprites.InactiveSprite;

        public void Enable() => transform.parent.SetActive(true);
        public void Disable() => transform.parent.SetActive(false);
    }
}