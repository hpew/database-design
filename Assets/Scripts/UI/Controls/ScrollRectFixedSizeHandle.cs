using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Kudos.UI
{
    public class ScrollRectFixedSizeHandle : ScrollRect
    {
        override protected void LateUpdate()
        {
            base.LateUpdate();

            if (verticalScrollbar != null)
                verticalScrollbar.size = 0f;

            if (horizontalScrollbar != null)
                horizontalScrollbar.size = 0f;
        }

        override public void Rebuild(CanvasUpdate executing)
        {
            base.Rebuild(executing);

            if (verticalScrollbar != null)
                verticalScrollbar.size = 0f;

            if (horizontalScrollbar != null)
                horizontalScrollbar.size = 0f;
        }

        public override void OnBeginDrag(PointerEventData eventData) { }
        public override void OnDrag(PointerEventData eventData) { }
        public override void OnEndDrag(PointerEventData eventData) { }
    }
}