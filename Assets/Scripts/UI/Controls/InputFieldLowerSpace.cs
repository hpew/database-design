using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI
{
    public class InputFieldLowerSpace : InputField
    {
        [Inject]
        private void Construct() => onValidateInput += ValidateInput;

        private char ValidateInput(string input, int charIndex, char addedChar)
        {
            addedChar = char.ToLower(addedChar);
            if (char.IsWhiteSpace(addedChar)) return '\0';

            return addedChar;
        }
    }
}
