using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI
{
    public class InputFieldERFragment : InputField
    {
        [Inject]
        private void Construct()
        {
            image.raycastTarget = false;
            onValidateInput += ValidateInput;
        }

        public override void Select()
        {
            base.Select();
            image.raycastTarget = true;
            ActivateInputField();
        }

        public void Unselect()
        {
            image.raycastTarget = false;
            DeactivateInputField();
        }

        private char ValidateInput(string input, int charIndex, char addedChar)
        {
            addedChar = char.ToLower(addedChar);
            if (char.IsWhiteSpace(addedChar)) return '\0';

            return addedChar;
        }
    }
}
