using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace CITG.UI
{
    public class TextButton : Button
    {
        private Text text;

        public string Text
        {
            get => text.text;
            set => text.text = value;
        }

        [Inject]
        void Construct() => text = gameObject.GetComponentInChildren<Text>(true);
    }
}