using UnityEngine;
using UnityEngine.UI;
using Zenject;
using ChartAndGraph;
using CITG.System;

namespace CITG.UI.ResultScene
{
    public class Chart : MonoBehaviour
    {
        PieChart _pie;

        [Inject]
        void Construct() => _pie = gameObject.GetComponentInChildren<PieChart>(true);

        public void DrawResult(int score)
        {
            var correct = score;
            var errors = 100 - score;

            _pie.DataSource.SetValue("Correct", correct);
            _pie.DataSource.SetValue("Errors", errors);
        }

        void Start()
        {
            PieChart pie = GetComponent<PieChart>();
            if (pie != null)
            {
                pie.DataSource.SlideValue("Player 1", 50, 10f);
                pie.DataSource.SetValue("Player 2", Random.value * 10);
            }
        }

    }
}