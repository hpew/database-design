using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using System.Collections.Generic;
using CITG.TaskManagment;
using CITG.DataSystem;
using CITG.Utility;

namespace CITG.UI.ResultScene
{
    public class ReportWindow : MonoBehaviour
    {
        [SerializeField] TaskPanel _firstTaskPanel;
        [SerializeField] TaskPanel _secondTaskPanel; 
        [SerializeField] TaskPanel _thirdTaskPanel;
        [SerializeField] TaskPanel _fourthTaskPanel;

        Dictionary<TaskType, TaskPanel> _panels;

        ExitButton _exitButton;

        [Inject]
        void Construct()
        {
            _panels = new Dictionary<TaskType, TaskPanel>()
            {
                [TaskType.First] = _firstTaskPanel,
                [TaskType.Second] = _secondTaskPanel,
                [TaskType.Third] = _thirdTaskPanel,
                [TaskType.Fourth] = _fourthTaskPanel,
            };

            _exitButton = gameObject.GetComponentInChildren<ExitButton>(true);

            gameObject.SetActive(false);
        }

        public void DisplayReport(Report report)
        {
            foreach (var elements in _panels)
                elements.Value.Display(report.GetTaskReport(elements.Key));
        }

        public async UniTask Show()
        {
            gameObject.SetActive(true);

            await ButtonObserver.Track(_exitButton);

            gameObject.SetActive(false);
        }
    }

    [Serializable]
    public struct TaskPanel
    {
        [SerializeField] Text Description;
        [SerializeField] Text Score;
        [SerializeField] int MaxScore;

        public void Display(TaskReport report)
        {
            Description.text = report.Description;
            Score.text = $"{report.Score}/{MaxScore}";
        }
    }
}