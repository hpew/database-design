using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.DataSystem;

namespace CITG.UI.ResultScene
{
    public class InformationBoard : MonoBehaviour
    {
        [SerializeField] Text _creditBook;
        [SerializeField] Text _result;
        [SerializeField] Text _ratingAndOption;
        [SerializeField] Text _time;

        public void DisplayInformation(string creditBook, int score, TimeSpan span, int rating, int numberOption)
        {
            _creditBook.text = $"�������� ������ �{creditBook}";
            _result.text = $"���������� ������: {score}/100";
            _time.text = $"����� �����������: {span.ToString(@"hh\:mm\:ss")}";
            _ratingAndOption.text = $"�������: <color=#FF7D69>{numberOption}</color>                ������: <color=#FF7D69>{rating}</color>";
        }
    }
}