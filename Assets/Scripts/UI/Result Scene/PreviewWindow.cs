using System;
using UnityEngine;
using Zenject;
using CITG.DataSystem;
using CITG.Utility;

namespace CITG.UI.ResultScene
{
    public class PreviewWindow : MonoBehaviour
    {
        [Inject] InformationBoard _board;
        [Inject] Chart _chart;

        public void DisplayResult(string creditBook, int score, TimeSpan span, int rating, int numberOption)
        {
            _board.DisplayInformation(creditBook, score, span, rating, numberOption);
            _chart.DrawResult(score);
        }
    }
}