using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using CITG.Utility;

namespace CITG.UI
{
    public class InteractiveZone : MonoBehaviour
    {
        RectTransform _rectTransform;
        RectTransform _rectTransformCanvas;
        Camera _camera;

        [Inject]
        void Construct([Inject(Id = "Canvas")] RectTransform rectTransformCanvas, Camera camera)
        {
            _rectTransform = gameObject.GetComponent<RectTransform>();

            _rectTransformCanvas = rectTransformCanvas;
            _camera = camera;
        }

        public bool Contains(Vector2 position)
        {
            var rect = GetRectOnCamera();
            return rect.Contains(position);
        }

        public Rect GetRectOnCamera() => _rectTransform.GetWorlRectRelativeCameraSize(_camera, _rectTransformCanvas);
    }
}