using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Zenject;

namespace CITG.UI.MainScene
{
    public class WindowSQLCode : MonoBehaviour, IInitializable
    {
        [SerializeField] Text _tittle;
        [SerializeField] TMP_InputField _input;

        Func<string, string> _callbackValidation;

        public void Initialize()
        {
            _input.onEndEdit.AddListener(text =>
            {
                if (_callbackValidation == null) return;

                var newText = _callbackValidation(text);
                _input.SetTextWithoutNotify(newText);
            });
        }

        public string Code
        {
            get => _input.text;
            set
            {
                _input.verticalScrollbar.value = 0f;
                _input.SetTextWithoutNotify(value);
            }
        }

        public string Tittle
        {
            get => _tittle.text;
            set => _tittle.text = value;
        }

        public void Show(Func<string, string> callbackValidation)
        {
            _callbackValidation = callbackValidation;
            gameObject.SetActive(true);
        }
        public void Unshow()
        {
            _callbackValidation = null;
            gameObject.SetActive(false);
        }
    }
}