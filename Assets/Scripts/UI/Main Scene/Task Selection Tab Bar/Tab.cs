using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.TaskSelectionTabbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/TaskSelectionTabbar/Tab", 0), DisallowMultipleComponent]
    public class Tab : Image
    {
        protected TaskSelectionTabSprites.CoupleSprites sprites;

        [Inject]
        protected virtual void Construct(Assets assets)
        {
            sprites = assets.TaskSelectionTabSprites.PairWithoutAngle;
            sprite = sprites.Inactive;
        }

        public void Activate() => sprite = sprites.Active;
        public void Deactivate() => sprite = sprites.Inactive;
    }
}