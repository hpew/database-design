using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.TaskSelectionTabbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/TaskSelectionTabbar/TabAngle", 0), DisallowMultipleComponent]
    public class TabAngle : Tab
    {
        protected override void Construct(Assets assets)
        {
            sprites = assets.TaskSelectionTabSprites.PairAngle;
            sprite = sprites.Inactive;
        }
    }
}