using System;
using UnityEngine;
using CITG.Utility;

namespace CITG.UI.MainScene.TaskSelectionTabbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/TaskSelectionTabbar/TaskSelectionTabbar", 0), DisallowMultipleComponent]
    public class TaskSelectionTabbar : MonoBehaviour
    {
        [SerializeField] DictionaryTabs _dictionary;
        Tab currentTab;

        public void Select(TaskType type)
        {
            currentTab?.Deactivate();
            currentTab = _dictionary[type];
            currentTab.Activate();
        }
    }

    [Serializable]
    public class DictionaryTabs : UnitySerializedDictionary<TaskType, Tab> { }
}