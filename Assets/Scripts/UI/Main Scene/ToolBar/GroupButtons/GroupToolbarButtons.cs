using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;
using CITG.System;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/GroupToolbarButtons", 0), DisallowMultipleComponent]
    public class GroupToolbarButtons : MonoBehaviour
    {
        public void Appear() => gameObject.SetActive(true);
        public void Disappear() => gameObject.SetActive(false);
    }


}