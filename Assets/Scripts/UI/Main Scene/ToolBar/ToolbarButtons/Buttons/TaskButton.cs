using System;
using UnityEngine;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/TaskButton", 0), DisallowMultipleComponent]
    public class TaskButton : SwitchableToolbarButton<TaskButton> { }
}