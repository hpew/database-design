using System;
using UnityEngine;
using Zenject;
using CITG.System;
using CITG.Utility;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/HelpButton", 0), DisallowMultipleComponent]
    public class HelpButton : SwitchableToolbarButton<HelpButton> { }
}