using UnityEngine;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group First Task/CreatingEntityShapeButton", 0), DisallowMultipleComponent]
    public class CreatingEntityShapeButton : ToolbarButton<CreatingEntityShapeButton> { }
}