using UnityEngine;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group First Task/CreatingRelationshipShapeButton", 0), DisallowMultipleComponent]
    public class CreatingRelationshipShapeButton : ToolbarButton<CreatingRelationshipShapeButton> { }
}