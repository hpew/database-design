using System;
using UnityEngine;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group First Task/EntityFormationButton", 0), DisallowMultipleComponent]
    public class EntityFormationButton : SwitchableToolbarButton<EntityFormationButton> { }
}