using UnityEngine;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group Second Task/TableManagementButton", 0), DisallowMultipleComponent]
    public class TableManagementButton : SwitchableToolbarButton<TableManagementButton> { }
}