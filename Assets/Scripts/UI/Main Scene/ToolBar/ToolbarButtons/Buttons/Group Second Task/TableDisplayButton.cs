using UnityEngine;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group Second Task/TableDisplayButton", 0), DisallowMultipleComponent]
    public class TableDisplayButton : SwitchableToolbarButton<TableDisplayButton> { }
}