using UnityEngine;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group Third Task/CodeResetButton", 0), DisallowMultipleComponent]
    public class CodeResetButton : ToolbarButton<CodeResetButton> { }
}