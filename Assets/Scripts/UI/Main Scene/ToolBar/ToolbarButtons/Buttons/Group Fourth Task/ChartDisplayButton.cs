using UnityEngine;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Group Fourth Task/ChartDisplayButton", 0), DisallowMultipleComponent]
    public class ChartDisplayButton : SwitchableToolbarButton<ChartDisplayButton> { }
}
