using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UniRx;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.ToolbarSystem
{
    public interface ISwitchableToolbarButton
    {
        void SwitchOn();
        void SwitchOff();
    }

    public abstract class ToolbarButton<T> : Button
        where T : ToolbarButton<T>
    {
        public override void OnPointerClick(PointerEventData eventData)
        {
            base.OnPointerClick(eventData);
            MessageBroker.Default.Publish(new MessageClickedToolbarButton<T>((T)this));
        }
    }

    public abstract class SwitchableToolbarButton<T> : ToolbarButton<T>, ISwitchableToolbarButton
        where T : SwitchableToolbarButton<T>
    {
        SpritesSwitchableToolbarButton sprites;

        [Inject]
        void Construct(Assets assets)
        {
            var sprites = assets.SpriteTableSwitchableToolbarButton[GetType()];
            if (sprites.OnSprite == null || sprites.OffSprite == null) throw new Exception();

            this.sprites = sprites;

            image.sprite = sprites.OffSprite;
        }

        public virtual void SwitchOn() => image.sprite = sprites.OnSprite;
        public virtual void SwitchOff() => image.sprite = sprites.OffSprite;
    }

    public class MessageClickedToolbarButton<T> where T : ToolbarButton<T>
    {
        public T Button { get; private set; }

        public MessageClickedToolbarButton(T button) => Button = button;
    }
}