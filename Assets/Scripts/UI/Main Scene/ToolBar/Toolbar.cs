using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Sirenix.OdinInspector;
using CITG.Utility;
using Zenject;

namespace CITG.UI.MainScene.ToolbarSystem
{
    [AddComponentMenu("CITG/UI/Main Scene/Toolbar/Toolbar", 0), DisallowMultipleComponent]
    public class Toolbar : SerializedMonoBehaviour
    {
        [SerializeField] DictionaryGroupToolbarButtons _groups;
        GroupToolbarButtons _currentGroup;

        [Inject]
        void Construct()
        {
            _currentGroup = _groups[TaskType.First];
            _currentGroup.Appear();
        }

        public void Show(TaskType type)
        {
            if (!_groups.ContainsKey(type))
                throw new Exception();

            _currentGroup?.Disappear();
            _currentGroup = _groups[type];
            _currentGroup.Appear();
        }

        [Serializable]
        public class DictionaryGroupToolbarButtons : UnitySerializedDictionary<TaskType, GroupToolbarButtons> { }

    }
}
