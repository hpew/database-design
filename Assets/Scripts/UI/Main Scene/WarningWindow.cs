using System.Collections;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using CITG.Utility;
using Zenject;

namespace CITG.UI.MainScene
{
    public class WarningWindow : MonoBehaviour
    {
        [SerializeField] Text _description;
        
        ConfirmButton _confirmButton;
        RectTransform _rect;
        LockRect.Factory _factory;

        string Description
        {
            get => _description.text;
            set => _description.text = value;
        }

        [Inject]
        public void Construct(LockRect.Factory factory)
        {
            _confirmButton = gameObject.GetComponentInChildren<ConfirmButton>(true);

            _rect = gameObject.GetComponent<RectTransform>();
            _rect.SetActive(false);

            _factory = factory;
        }

        public async UniTask Show(string description)
        {
            Description = description;
            var lockRect = _factory.Create(_rect);
            _rect.SetActive(true);

            await ButtonObserver.Track(_confirmButton);

            lockRect.Destroy();
            _rect.SetActive(false);
        }
    }
}