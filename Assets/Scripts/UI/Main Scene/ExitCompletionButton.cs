using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using CITG.Utility;

namespace CITG.UI.MainScene
{
    public class ExitCompletionButton : Button
    {
        ConfirmationWindow _window;

        [Inject]
        void Construct([Inject(Id = "Exit Test")] ConfirmationWindow window)
        {
            _window = window;
            onClick.AddListener(HandleClick);

#if UNITY_WEBGL && !UNITY_EDITOR
            gameObject.SetActive(false);
#endif
        }

        public async void HandleClick()
        {
            var isConfirm = await _window.Show();


            if (isConfirm)
                MessageBroker.Default.Publish(new ExitTestMessage());
        }
    }

   public class ExitTestMessage { };
}