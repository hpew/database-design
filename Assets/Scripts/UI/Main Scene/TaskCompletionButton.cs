using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using CITG.Utility;

namespace CITG.UI.MainScene
{
    public class TaskCompletionButton : Button
    {
        ConfirmationWindow _window;
        RectTransform _rectButton;

        [Inject]
        void Construct([Inject(Id = "Completion Task")] ConfirmationWindow window)
        {
            _rectButton = transform.parent as RectTransform;
            _window = window;
            onClick.AddListener(HandleClick);
        }

        public async void HandleClick()
        {
            _rectButton.SetActive(false);

            var isConfirm = await _window.Show();

            _rectButton.SetActive(true);

            if (isConfirm)
                MessageBroker.Default.Publish(new TaskCompletionMessage());
        }
    }

    public class TaskCompletionMessage { };
}