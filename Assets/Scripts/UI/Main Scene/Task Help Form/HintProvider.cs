using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using CITG.Utility;
using CITG.UI.MainScene.ToolbarSystem;

namespace CITG.UI.MainScene.TaskHelpForm
{
    public class HintProvider
    {
        TaskButton _taskButton;
        HelpButton _helpButton;

        public HintProvider(TaskButton taskButton, HelpButton helpButton)
        {
            _taskButton = taskButton;
            _helpButton = helpButton;
        }

        public async UniTask<ISwitchableToolbarButton> DisplayHint(TypeHint type, CancellationToken displayToken)
        {
            var context = new TaskContext<bool>();

            ISwitchableToolbarButton button = (type) switch
            {
                TypeHint.Task => _taskButton,
                TypeHint.Help => _helpButton,
                _ => throw new Exception(),
            };

            var observable = new ButtonObserver.ObservableButton(button as Button);

            try
            {
                StartAnimation(button, context);

                while (true)
                {
                    await UniTask.Yield();
                    if (observable.IsClicked) break;
                    if (displayToken.IsCancellationRequested)
                    {
                        context.Cancel(false);
                        
                        displayToken.ThrowIfCancellationRequested();
                    }
                }

                await context.CancelAwaitFrame(true);
            }
            finally
            {
                context.Dispose();
                observable.Dispose();
            }

            return button;
        }

        private async void StartAnimation(ISwitchableToolbarButton button, TaskContext<bool> context)
        {
            var transform = (button as Button).transform;
            var range = new Couple<Vector3, Vector3>(transform.localScale, transform.localScale * 1.1f);
            var speed = 1f;

            try
            {
                while (true)
                {
                    button.SwitchOn();
                    await Observable.FromMicroCoroutine(_ => ChangeScale(transform, range, speed)).ToUniTask(cancellationToken: context.Token);

                    button.SwitchOff();
                    await UniTask.Delay(250, cancellationToken: context.Token);
                }
            }
            catch (OperationCanceledException) { return; }
            finally
            {
                if (context.Data)
                    transform.localScale = range.FirstElement;
            }
        }

        private IEnumerator ChangeScale(Transform transform, Couple<Vector3, Vector3> range, float speed)
        {
            var start = range.FirstElement;
            var end = range.SecondElement;

            var t = 0f;

            while (t <= 1)
            {
                transform.localScale = Vector3.Lerp(start, end, CalculateTime(t));
                t += Time.deltaTime * speed;

                yield return null;
            }

            transform.localScale = start;
        }

        private float CalculateTime(float t) => -Mathf.Pow(2f * t - 1f, 2.0f) + 1f;


        public enum TypeHint
        {
            Task,
            Help
        }
    }
}