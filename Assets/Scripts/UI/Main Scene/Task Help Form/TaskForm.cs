using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Cysharp.Threading.Tasks;
using CITG.Utility;
using CITG.SystemOptions;

namespace CITG.UI.MainScene.TaskHelpForm
{
    public class TaskForm : Form
    {
        IReadOnlyDictionary<TaskType, string> _taskDescriptions;

        [Inject]
        private void Construct(Option.Context context)
        {
            _taskDescriptions = context.TaskDescriptions;
        }

        public RectTransform Rect => _rect; 

        public override void ChangeDescription(TaskType displaying)
        {
            _description.text = _taskDescriptions[displaying];
        }
    }
}