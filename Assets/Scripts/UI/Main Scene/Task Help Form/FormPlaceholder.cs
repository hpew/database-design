using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using Cysharp.Threading.Tasks;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.SystemOptions;
using CITG.System;
using CITG.Utility;
using System.Threading;

namespace CITG.UI.MainScene.TaskHelpForm
{
    [AddComponentMenu("CITG/UI/Main Scene/TaskHelpForm/FormPlaceholder", 0), DisallowMultipleComponent]
    public class FormPlaceholder : IInitializable, IDisposable
    {
        Form _taskForm;
        Form _helpForm;

        CompositeDisposable _receivers;

        HintProvider _hintProvider;
        CancellationTokenSource _cancelHints;
        
        public FormPlaceholder(TaskForm taskForm, HelpForm helpForm, HintProvider hintProvider)
        {
            _taskForm = taskForm;
            _helpForm = helpForm;

            _hintProvider = hintProvider;
        }

        public async void Initialize()
        {
            _cancelHints = new CancellationTokenSource();
            var token = _cancelHints.Token;

            try
            {
                var button = await _hintProvider.DisplayHint(HintProvider.TypeHint.Task, token);

                await _taskForm.ShowWithFocus();
                button.SwitchOff();
            }
            catch (OperationCanceledException) { return; }
            finally
            {
                _cancelHints.Dispose();
            }

            _cancelHints = new CancellationTokenSource();
            token = _cancelHints.Token;

            try
            {
                var button = await _hintProvider.DisplayHint(HintProvider.TypeHint.Help, token);
                await _helpForm.ShowWithFocus();
                button.SwitchOff();
            }
            catch (OperationCanceledException) { return; }
            finally 
            {
                _cancelHints.Dispose();
                _cancelHints = null;
            }

            SubscribeToMessages();
        }

        public void Dispose()
        {
            _receivers?.Dispose();
            _cancelHints?.Cancel();
        }

        public void ChangeDescription(TaskType type)
        {
            _taskForm.ChangeDescription(type);
            _helpForm.ChangeDescription(type);
        }

        private void SubscribeToMessages()
        {
            _receivers = new CompositeDisposable();

            MessageBroker.Default.Receive<MessageClickedToolbarButton<TaskButton>>()
                .Subscribe(async msg =>
                {
                    var button = msg.Button;
                    button.SwitchOn();

                    try
                    {
                        await _taskForm.ShowWithFocus();
                    }
                    catch (OperationCanceledException) { return; }

                    button.SwitchOff();
                })
                .AddTo(_receivers);

            MessageBroker.Default.Receive<MessageClickedToolbarButton<HelpButton>>()
                .Subscribe(async msg =>
                {
                    var button = msg.Button;
                    button.SwitchOn();

                    try
                    {
                        await _helpForm.ShowWithFocus();
                    }
                    catch (OperationCanceledException) { return; }

                    button.SwitchOff();
                })
                .AddTo(_receivers);
        }
    }
}