using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CITG.Utility;

namespace CITG.UI.MainScene.TaskHelpForm
{
    public class ImageProvider : MonoBehaviour
    {
        [SerializeField] Image _imageFirstTask;
        [SerializeField] Image _imageSecondTask;
        [SerializeField] Image _imageThirdTask;
        [SerializeField] Image _imageFourTask;

        Image _currentImage;

        private Image this[TaskType type]
        {
            get
            {
                return type switch
                {
                    TaskType.First => _imageFirstTask,
                    TaskType.Second => _imageSecondTask,
                    TaskType.Third => _imageThirdTask,
                    TaskType.Fourth => _imageFourTask,
                    _ => throw new Exception()
                };
            }
        }

        public void Display(TaskType type)
        {
            var nextImage = this[type];

            if (nextImage != _currentImage)
            {
                _currentImage?.SetActive(false);

                nextImage.SetActive(true);
                _currentImage = nextImage;
            }
        }
    }
}