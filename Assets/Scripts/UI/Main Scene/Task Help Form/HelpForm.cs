using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Cysharp.Threading.Tasks;
using CITG.Utility;
using CITG.System;

namespace CITG.UI.MainScene.TaskHelpForm
{
    public class HelpForm : Form
    {
        ImageProvider _provider;
        DictionaryHelpDescription _helpDescriptions;

        [Inject]
        private void Construct(Assets assets)
        {
            _helpDescriptions = assets.HelpDescriptions;
            _provider = gameObject.GetComponentInChildren<ImageProvider>();
        }

        public override void ChangeDescription(TaskType type)
        {
            _provider.Display(type);
            _description.text = _helpDescriptions[type];
        }
    }
}