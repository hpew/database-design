using Cysharp.Threading.Tasks;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.MainScene.TaskHelpForm
{
    [AddComponentMenu("CITG/UI/Main Scene/TaskHelpForm/Form", 0), DisallowMultipleComponent]

    public abstract class Form : MonoBehaviour, IDisposable
    {
        [SerializeField] protected Text _description;

        [SerializeField] float timeAnimation = 0.2f;

        protected RectTransform _rect;
        ExitButton _exitButton;

        Vector2 displayedPosition;
        Vector2 hiddenPosition;

        LockRect.Factory _factoryLockRect;
        LockRect _lockRect;

        CancellationTokenSource _source;

        [Inject]
        private void Construct(LockRect.Factory factoryLockRect)
        {
            _factoryLockRect = factoryLockRect;

            _rect = gameObject.GetComponent<RectTransform>();
            _exitButton = gameObject.GetComponentInChildren<ExitButton>(true);

            _rect.SetActive(false);

            displayedPosition = _rect.anchoredPosition;
            hiddenPosition = new Vector2(0f, displayedPosition.y);

            _rect.anchoredPosition = hiddenPosition;

            _source = new CancellationTokenSource();
        }
        public void Dispose() => _source.Cancel();

        public abstract void ChangeDescription(TaskType type);

        public async UniTask ShowWithFocus()
        {
            var lockRect = _factoryLockRect.Create(_rect);
            _rect.SetActive(true);

            var cancel = _source.Token;

            try
            {
                await _rect.LerpAnchoredPosition(_rect.anchoredPosition, displayedPosition, timeAnimation, cancel);
                await ButtonObserver.Track(cancel, _exitButton);
                await _rect.LerpAnchoredPosition(_rect.anchoredPosition, hiddenPosition, timeAnimation, cancel);
            }
            catch (OperationCanceledException e) { Debug.Log(1); throw e; }

            _rect.SetActive(false);
            lockRect.Destroy();
        }

        public async UniTask Show()
        {
            _rect.SetActive(true);
            _exitButton.SetActive(false);

            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, displayedPosition, timeAnimation);
        }

        public async UniTask Unshow()
        {
            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, hiddenPosition, timeAnimation);
            _rect.SetActive(false);
            _exitButton.SetActive(true);
        }

    }
}