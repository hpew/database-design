using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.MainScene
{
    public class FormCreatingRelationship : MonoBehaviour, IInitializable, IDisposable
    {
        [SerializeField] InputField _outInputField;
        [SerializeField] InputField _inInputField;

        RectTransform _rect;
        LockRect.Factory _factory;

        ExitButton _exitButton;
        ConfirmButton _confirmButton;
        CancelButton _cancelButton;

        [SerializeField] float timeAnimation = 0.2f;

        Vector2 displayedPosition;
        Vector2 hiddenPosition;

        [Inject]
        public void Construct(LockRect.Factory factory)
        {
            _confirmButton = gameObject.GetComponentInChildren<ConfirmButton>(true);
            _cancelButton = gameObject.GetComponentInChildren<CancelButton>(true);
            _exitButton = gameObject.GetComponentInChildren<ExitButton>(true);

            _rect = gameObject.GetComponent<RectTransform>();
            _factory = factory;

            displayedPosition = _rect.anchoredPosition;
            hiddenPosition = new Vector2(_rect.sizeDelta.x, displayedPosition.y);

            _rect.anchoredPosition = hiddenPosition;
            _rect.SetActive(false);
        }

        public void Initialize()
        {
            _outInputField.onValidateInput += ValidateInput;
            _inInputField.onValidateInput += ValidateInput;
        } 

        public void Dispose()
        {
            _outInputField.onValidateInput -= ValidateInput;
            _inInputField.onValidateInput -= ValidateInput;
        }

        public async UniTask<Result> Open()
        {
            _inInputField.text = string.Empty;
            _outInputField.text = string.Empty;

            var lockRect = _factory.Create(_rect);
            _rect.SetActive(true);

            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, displayedPosition, timeAnimation);

            var selectedButton = await ButtonObserver.Track(_confirmButton, _cancelButton, _exitButton);

            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, hiddenPosition, timeAnimation);
            _rect.SetActive(false);
            lockRect.Destroy();

            if (selectedButton != _confirmButton) return new Result() { IsConfirm = false };

            return new Result()
            {
                IsConfirm = true,
                InputText = _inInputField.text,
                OutputText = _outInputField.text,
            };
        }

        private char ValidateInput(string input, int charIndex, char addedChar)
        {
            switch (addedChar)
            {
                case '1':
                    return addedChar;
                case '�':
                    return addedChar;
                case '�':
                    return '�';
            }

            return '\0';
        }

        public struct Result
        {
            public bool IsConfirm;
            public string OutputText;
            public string InputText;
        }
    }
}