using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.UICollections
{
    public class SelectableLine : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] Text _key;
        [SerializeField] Image _image;

        private Action<SelectableLine> _clickAction;

        private static string selectedColor = "#CCCCCC";

        public string Key
        {
            get => _key.text;
            private set => _key.text = value;
        }

        public object Value { get; private set; }

        public void Init(string key, object value, Action<SelectableLine> clickAction)
        {
            _clickAction = clickAction;

            Key = key;
            Value = value;
        }

        public void Destroy()
        {
            _clickAction = null;
            GameObject.Destroy(gameObject);
        }

        public void OnPointerClick(PointerEventData eventData) => _clickAction(this);

        public void Select()
        {
            Color color;
            if (ColorUtility.TryParseHtmlString(selectedColor, out color)) _image.color = color;
        }

        public void Unselect() => _image.color = Color.white;

        public class Factory : IFactory<Transform, string, object, Action<SelectableLine>, SelectableLine>
        {
            DiContainer _container;
            SelectableLine _prefab;

            public Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _prefab = assets.SelectableLine;
            }

            public SelectableLine Create(Transform content, string key, object value, Action<SelectableLine> clickAction)
            {
                var line = _container.InstantiatePrefab(_prefab, content).GetComponent<SelectableLine>();
                line.name = "Line";

                line.Init(key, value, clickAction);

                return line;
            }
        }
    }
}