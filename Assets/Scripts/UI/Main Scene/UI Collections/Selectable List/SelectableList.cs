using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.MainScene.UICollections
{
    public class SelectableList : MonoBehaviour
    {
        [SerializeField] Text _tittle;

        protected RectTransform _rect;
        protected RectTransform _content;

        private ConfirmButton _confrimButton;
        private CancelButton _cancelButton;
        private ExitButton _exitButton;
        private ScrollRect _scrollRect;

        private List<SelectableLine> _list;
        private SelectableLine _selectedLine;

        private SelectableLine.Factory _factoryLine;
        private LockRect.Factory _factoryLockRect;

        Vector2 displayedPosition;
        Vector2 hiddenPosition;

        [SerializeField] float timeAnimation = 0.2f;

        [Inject]
        void Construct(SelectableLine.Factory factoryLine, LockRect.Factory factoryLockRect)
        {
            _rect = gameObject.GetComponent<RectTransform>();

            _scrollRect = gameObject.GetComponentInChildren<ScrollRect>();
            _content = _scrollRect.content;

            _confrimButton = gameObject.GetComponentInChildren<ConfirmButton>(true);
            _cancelButton = gameObject.GetComponentInChildren<CancelButton>(true);
            _exitButton = gameObject.GetComponentInChildren<ExitButton>(true);

            _scrollRect = gameObject.GetComponentInChildren<ScrollRect>(true);
            _content = _scrollRect.content;

            _factoryLine = factoryLine;
            _factoryLockRect = factoryLockRect;

            displayedPosition = _rect.anchoredPosition;
            hiddenPosition = new Vector2(_rect.sizeDelta.x, displayedPosition.y);

            _rect.anchoredPosition = hiddenPosition;
            _rect.SetActive(false);
        }

        public async UniTask<object> Open(string tittle, Dictionary<string, object> elements)
        {
            if (elements == null || elements.Count == 0) throw new ArgumentNullException();

            _tittle.text = tittle;
            _list = new List<SelectableLine>();

            foreach (var pair in elements)
                _list.Add(_factoryLine.Create(_content, pair.Key, pair.Value, HandleClickLine));

            var lockRect = _factoryLockRect.Create(_rect);
            await ShowForm();

            var selectedButton = await ButtonObserver.Track(_cancelButton, _confrimButton, _exitButton);

            await UnshowForm();
            lockRect.Destroy();

            var result = selectedButton == _confrimButton ? Confirm() : Cancel();

            foreach (var line in _list)
                line.Destroy();

            _list.Clear();
            _selectedLine = null;

            return result;
        }

        public async UniTask<string> Open(string tittle, List<string> keys)
        {
            if (keys == null || keys.Count == 0) throw new ArgumentNullException();

            var dictionary = new Dictionary<string, object>();

            foreach (var element in keys) dictionary.Add(element, element);

            var result = await Open(tittle, dictionary);

            return (string)result ?? "";
        }

        private void HandleClickLine(SelectableLine line)
        {
            if (_selectedLine == line) return;

            _selectedLine?.Unselect();
            _selectedLine = line;
            _selectedLine.Select();
        }

        private object Confirm()
        {
            if (_selectedLine == null) return null;
            return _selectedLine.Value;
        }

        private object Cancel() => null;

        private async UniTask ShowForm()
        {
            _rect.SetActive(true);
            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, displayedPosition, timeAnimation);
        }
        private async UniTask UnshowForm()
        {
            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, hiddenPosition, timeAnimation);
            _rect.SetActive(false);
        }
    }
}