using System.Collections.Generic;
using Zenject;

namespace CITG.UI.MainScene.UICollections
{
    public class RelationshipsFormationList : UIList<RelationshipsFormationLineData>
    {
        [Inject] RelationshipsFormationLine.Factory _factory;

        protected override UILine<RelationshipsFormationLineData> CreateLine()
            => _factory.Create(_content, _broker, RelationshipsFormationLineData.Default);

        protected override UILine<RelationshipsFormationLineData> CreateLine(RelationshipsFormationLineData data)
            => _factory.Create(_content, _broker, data);

        protected override void HandleMessageLine(UILine<RelationshipsFormationLineData> line, ILineMessage message)
        {
            if (message.GetType() == typeof(LineMessageAdding)) AddLine();
            else if (message.GetType() == typeof(LineMessageDeleting)) RemoveLine(line);
        }
    }
}