using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.UICollections
{
    public class RelationshipsFormationLine : UILine<RelationshipsFormationLineData>
    {
        [SerializeField] InputField _nameRelationships;
        [SerializeField] InputField _parentEntity;
        [SerializeField] InputField _connectingEntity;
        [SerializeField] InputField _typeConnection;

        string NameRelationships
        {
            get => _nameRelationships.text;
            set => _nameRelationships.text = value;
        }
        string ParentEntity
        {
            get => _parentEntity.text;
            set => _parentEntity.text = value;
        }
        string ConnectingEntity
        {
            get => _connectingEntity.text;
            set => _connectingEntity.text = value;
        }
        string TypeConnection
        {
            get => _typeConnection.text;
            set => _typeConnection.text = value;
        }

        public override bool IsEmpty => string.IsNullOrEmpty(NameRelationships) || string.IsNullOrEmpty(ParentEntity)
            || string.IsNullOrEmpty(ConnectingEntity) || string.IsNullOrEmpty(TypeConnection);

        protected void Init(MessageBrokerUICollection<RelationshipsFormationLineData> broker, RelationshipsFormationLineData data)
        {
            base.Init(broker);

            NameRelationships = data.NameRelationships;
            ParentEntity = data.ParentEntity;
            ConnectingEntity = data.ConnectingEntity;
            TypeConnection = data.TypeConnection;
        }

        public override RelationshipsFormationLineData GetData() => new RelationshipsFormationLineData() 
        {
            NameRelationships = NameRelationships,
            ParentEntity = ParentEntity,
            ConnectingEntity = ConnectingEntity,
            TypeConnection = TypeConnection
        };

        public class Factory : IFactory<Transform, MessageBrokerUICollection<RelationshipsFormationLineData>,
            RelationshipsFormationLineData, UILine<RelationshipsFormationLineData>>
        {
            DiContainer _container;
            RelationshipsFormationLine _prefab;

            public Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _prefab = assets.RelationshipsFormationLinePrefab;
            }

            public UILine<RelationshipsFormationLineData> Create(Transform content, MessageBrokerUICollection<RelationshipsFormationLineData> broker,
                RelationshipsFormationLineData data)
            {
                var line = _container.InstantiatePrefab(_prefab, content).GetComponent<RelationshipsFormationLine>();
                line.name = "Line";

                line.Init(broker, data);

                return line;
            }
        }
    }

    public struct RelationshipsFormationLineData : ILineData
    {
        public string NameRelationships;
        public string ParentEntity;
        public string ConnectingEntity;
        public string TypeConnection;

        public static RelationshipsFormationLineData Default => new RelationshipsFormationLineData()
        {
            NameRelationships = "",
            ParentEntity = "",
            ConnectingEntity = "",
            TypeConnection = ""
        };

        public override string ToString()
        {
            return $"NameRelationships: {NameRelationships}\n" +
                $"ParentEntity: {ParentEntity}\n" +
                $"ConnectingEntity: {ConnectingEntity}\n" +
                $"TypeConnection: {TypeConnection}\n";
        }
    }
}
