using System;
using Cysharp.Threading.Tasks;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;

namespace CITG.UI.MainScene.UICollections
{
    public abstract class UIList<T> : MonoBehaviour
        where T : ILineData
    {
        [SerializeField] Button _addButton;

        protected RectTransform _rect;
        protected RectTransform _content;

        protected List<UILine<T>> _list;
        List<T> _backupData;

        protected MessageBrokerUICollection<T> _broker;

        protected LockRect.Factory _factoryLockRect;

        protected WarningWindow _warningWindow;

        private ConfirmButton _confrimButton;
        private CancelButton _cancelButton;
        private ExitButton _exitButton;
        private ScrollRect _scrollRect;


        Vector2 displayedPosition;
        Vector2 hiddenPosition;

        [SerializeField] float timeAnimation = 0.2f;

        static string WarningMessageEmptyLines = "� ����� ���� ������ �������, ������� ��, ������ ��� ���������!";

        [Inject]
        void Construct(LockRect.Factory factoryLockRect, WarningWindow warningWindow)
        {
            _rect = gameObject.GetComponent<RectTransform>();

            _scrollRect = gameObject.GetComponentInChildren<ScrollRect>();
            _content = _scrollRect.content;

            _confrimButton = gameObject.GetComponentInChildren<ConfirmButton>();
            _cancelButton = gameObject.GetComponentInChildren<CancelButton>();
            _exitButton = gameObject.GetComponentInChildren<ExitButton>();

            _broker = new MessageBrokerUICollection<T>(HandleMessageLine);

            _factoryLockRect = factoryLockRect;

            _warningWindow = warningWindow;

            displayedPosition = _rect.anchoredPosition;
            hiddenPosition = new Vector2(_rect.sizeDelta.x, displayedPosition.y);

            _rect.anchoredPosition = hiddenPosition;
            _rect.SetActive(false);
        }

        public async UniTask<List<T>> Open(List<T> data)
        {
            _list = new List<UILine<T>>();
            _backupData = data;

            _addButton.onClick.AddListener(() => AddLine());
            PrepareListBeforeEditing();

            if (data.Count != 0)
                for (int i = 0; i < data.Count; i++) AddLine(data[i]);

            HandleGeneratedList(_list);

            var lockRect = CreateLockRect();
            await ShowForm();

            Button clickedButton = null;

            while (true)
            {
                clickedButton = await ButtonObserver.Track(_cancelButton, _confrimButton, _exitButton);

                if (clickedButton != _confrimButton) break;

                var isEmptyLineFind = false;
                for (int i = 0; i < _list.Count; i++)
                {
                    if (_list[i].IsEmpty)
                    {
                        isEmptyLineFind = true;
                        await _warningWindow.Show(WarningMessageEmptyLines);
                        break;
                    }
                }

                if (!isEmptyLineFind) break;
            }

            await UnshowForm();
            lockRect.Destroy();

            _addButton.onClick.RemoveAllListeners();
            ClearListAfterEditing();

            return clickedButton == _confrimButton ? Confirm() : Cancel();
        }

        public async UniTask<List<T>> Open(List<T> data, params RectTransform[] additionalUIElements)
        {
            _list = new List<UILine<T>>();
            _backupData = data;

            _addButton.onClick.AddListener(() => AddLine());
            PrepareListBeforeEditing();

            if (data.Count != 0)
                for (int i = 0; i < data.Count; i++) AddLine(data[i]);

            HandleGeneratedList(_list);

            var lockRect = CreateLockRect();
            await ShowForm();

            Button clickedButton = null;

            while (true)
            {
                clickedButton = await ButtonObserver.Track(_cancelButton, _confrimButton, _exitButton);

                if (clickedButton != _confrimButton) break;

                var isEmptyLineFind = false;
                for (int i = 0; i < _list.Count; i++)
                {
                    if (_list[i].IsEmpty)
                    {
                        isEmptyLineFind = true;
                        await _warningWindow.Show(WarningMessageEmptyLines);
                        break;
                    }
                }

                if (!isEmptyLineFind) break;
            }

            await UnshowForm();
            lockRect.Destroy();

            _addButton.onClick.RemoveAllListeners();
            ClearListAfterEditing();

            return clickedButton == _confrimButton ? Confirm() : Cancel();
        }


        protected abstract UILine<T> CreateLine();
        protected abstract UILine<T> CreateLine(T data);
        protected abstract void HandleMessageLine(UILine<T> line, ILineMessage message);

        protected void AddLine()
        {
            var line = CreateLine();
            _list.Add(line);

            line.SiblingIndex--;
            HandleAddedLine(line);
            
            UpdateDisplay();

            LowerScrollToBottom();
        }
        protected void AddLine(T data)
        {
            var line = CreateLine(data);
            _list.Add(line);

            line.SiblingIndex--;
            HandleAddedLine(line);

            UpdateDisplay();

            LowerScrollToBottom();
        }
        protected void RemoveLine(UILine<T> line)
        {
            HandleDeletionLine(line);
            _list.Remove(line);
            line.Destroy();

            UpdateDisplay();
        }

        protected List<T> Confirm()
        {
            var result = new List<T>();
            for (int i = 0; i < _list.Count; i++)
            {
                var data = _list[i].Destroy();
                if (!_list[i].IsEmpty) result.Add(data);
            }

            _list.Clear();


            return result;
        }
        protected List<T> Cancel()
        {
            foreach (var line in _list) line.Destroy();
            _list.Clear();

            return _backupData;
        }

        protected void UpdateDisplay()
        {
            for (int i = 0; i < _list.Count; i++)
            {
                _list[i].Number = i + 1;
                HandleUpdateDisplayLine(i);
            }

            HandleEndUpdateDisplay();
        }


        protected virtual async UniTask ShowForm()
        {
            _rect.SetActive(true);
            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, displayedPosition, timeAnimation);
        }

        protected virtual async UniTask UnshowForm()
        {
            await _rect.LerpAnchoredPosition(_rect.anchoredPosition, hiddenPosition, timeAnimation);
            _rect.SetActive(false);
        }

        protected virtual LockRect CreateLockRect() => _factoryLockRect.Create(_rect);

        protected virtual void HandleGeneratedList(List<UILine<T>> list) { }
        protected virtual void PrepareListBeforeEditing() { }
        protected virtual void ClearListAfterEditing() { }

        protected virtual void HandleAddedLine(UILine<T> line) { }
        protected virtual void HandleDeletionLine(UILine<T> line) { }

        protected virtual void HandleUpdateDisplayLine(int index) { }
        protected virtual void HandleEndUpdateDisplay() { }

        private void LowerScrollToBottom()
        {
            Canvas.ForceUpdateCanvases();
            _scrollRect.verticalNormalizedPosition = 0f;
            Canvas.ForceUpdateCanvases();
        }

    }

}

