using System;
using UnityEngine;
using UnityEngine.UI;

namespace CITG.UI.MainScene.UICollections
{
    public abstract class UILine<T> : MonoBehaviour
        where T : ILineData
    {
        [SerializeField] protected Text _number;
        [SerializeField] protected Button _removeButton;

        protected MessageBrokerUICollection<T> _broker;

        public abstract bool IsEmpty { get; }
        public bool IsHideRemoveButton => !_removeButton.isActiveAndEnabled;

        public int Number
        { 
            set => _number.text = Convert.ToString(value);
        }

        public int SiblingIndex
        {
            get => transform.GetSiblingIndex();
            set => transform.SetSiblingIndex(value);
        }

        public abstract T GetData();

        protected virtual void Init(MessageBrokerUICollection<T> broker)
        {
            _broker = broker;
            _removeButton.onClick.AddListener(() => _broker.Send(this, new LineMessageDeleting()));
        }

        public virtual T Destroy()
        {
            _removeButton.onClick.RemoveAllListeners();
            _broker = null;

            GameObject.Destroy(gameObject);

            return GetData();
        }

        public virtual void HideRemoveButton() => _removeButton.gameObject.SetActive(false);
        public virtual void ShowRemoveButton() => _removeButton.gameObject.SetActive(true);
    }

    public interface ILineData { }


    class LineMessageDeleting : ILineMessage { }
    class LineMessageAdding : ILineMessage { }

    class LineMessageInputFieldEditing : ILineMessage
    {
        public string OldText;
        public string NewText;
    }
}
