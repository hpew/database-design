using System.Collections.Generic;
using Zenject;

namespace CITG.UI.MainScene.UICollections
{
    public class EntityFormationList : UIList<EntityFormationLineData>
    {
        [Inject] EntityFormationLine.Factory _factory;

        static string warningMessage = "������ ��������� �������� � ����������� �������!";

        protected override UILine<EntityFormationLineData> CreateLine() 
            => _factory.Create(_content, _broker, EntityFormationLineData.Default);

        protected override UILine<EntityFormationLineData> CreateLine(EntityFormationLineData data)
            => _factory.Create(_content, _broker, data);

        protected override void HandleMessageLine(UILine<EntityFormationLineData> line, ILineMessage message)
        {
            if (message.GetType() == typeof(LineMessageAdding)) AddLine();
            else if (message.GetType() == typeof(LineMessageDeleting)) RemoveLine(line);
            else if (message is LineMessageInputFieldEditing editingMessage) ValidatInputFieldEdit(line as EntityFormationLine, editingMessage.NewText);
        }

        private async void ValidatInputFieldEdit(EntityFormationLine sourceLine, string text)
        {
            if (text == string.Empty) return;

            foreach (EntityFormationLine line in _list)
            {
                if (line == sourceLine) continue;
                if (line.NameEntity == sourceLine.NameEntity)
                {
                    await _warningWindow.Show(warningMessage);
                    sourceLine.NameEntity = string.Empty;
                    break;
                }
            }
        }
    }
}