using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;

namespace CITG.UI.MainScene.UICollections
{
    public class EntityFormationLine : UILine<EntityFormationLineData>
    {
        [SerializeField] InputField _nameEntity;

        public string NameEntity
        {
            get => _nameEntity.text;
            set => _nameEntity.text = value;
        }

        public override bool IsEmpty => string.IsNullOrEmpty(NameEntity);

        protected void Init(MessageBrokerUICollection<EntityFormationLineData> broker, EntityFormationLineData data)
        {
            base.Init(broker);

            NameEntity = data.NameEntity;
            _nameEntity.onEndEdit.AddListener(value => broker.Send(this, new LineMessageInputFieldEditing() { NewText = value }));
        }

        public override EntityFormationLineData Destroy()
        {
            _nameEntity.onEndEdit.RemoveAllListeners();
            return base.Destroy();
        }

        public override EntityFormationLineData GetData() => new EntityFormationLineData() { NameEntity = NameEntity };

        public class Factory : IFactory<Transform, MessageBrokerUICollection<EntityFormationLineData>, 
            EntityFormationLineData, UILine<EntityFormationLineData>>
        {
            DiContainer _container;
            EntityFormationLine _prefab;

            public Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _prefab = assets.EntityFormationLinePrefab;
            }

            public UILine<EntityFormationLineData> Create(Transform content, MessageBrokerUICollection<EntityFormationLineData> broker, 
                EntityFormationLineData data)
            {
                var line = _container.InstantiatePrefab(_prefab, content).GetComponent<EntityFormationLine>();
                line.name = "Line";

                line.Init(broker, data);

                return line;
            }
        }
    }

    

    public struct EntityFormationLineData : ILineData
    {
        public string NameEntity;
        public static EntityFormationLineData Default => new EntityFormationLineData() { NameEntity = "" };
    }


}

