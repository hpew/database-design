using System;

namespace CITG.UI.MainScene.UICollections
{
    public interface ILineMessage { }
    public class MessageBrokerUICollection<T>
        where T : ILineData
    {
        Action<UILine<T>, ILineMessage> _messageHandler;

        public MessageBrokerUICollection(Action<UILine<T>, ILineMessage> messageHandler) => _messageHandler = messageHandler;

        public void Send(UILine<T> line, ILineMessage message) => _messageHandler(line, message);
    }
}