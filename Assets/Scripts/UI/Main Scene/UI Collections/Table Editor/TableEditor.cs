using CITG.Utility;
using Cysharp.Threading.Tasks;
using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CITG.UI.MainScene.UICollections
{
    public class TableEditor : UIList<FieldData>
    {
        [Inject] TableEditorLine.Factory _factory;
        TableEditorLine _linePrimaryKey;

        SelectableList _selectableList;

        Dictionary<Type, Action<TableEditorLine, ILineMessage>> _dictionaryMessageHandlers;

        TableData tableData;
        List<TableData> otherTableDatas;

        GeneratorID _generatorFieldID;

        static List<string> typesField = new List<string>() { "��������", "�����", "���� � �����" };

        static string WarningMessageIdenticalTableNames = "������ ��������� ������� � ����������� ����������!";
        static string WarningMessageEmptyTableNames = "������ ��������� �������� ������� ������ ����� ������������!";
        static string WarningMessageNotFoundLinkingTable = "��� ��������� ������!";
        static string WarningMessageNotFoundLinkingField = "��� ��������� �����!";

        [Inject]
        void Construct(TableEditorLine.Factory factory, SelectableList selectableList)
        {
            _factory = factory;
            _selectableList = selectableList;

            _dictionaryMessageHandlers = new Dictionary<Type, Action<TableEditorLine, ILineMessage>>()
            {
                [typeof(LineMessageAdding)] = (line, msg) => AddLine(),
                [typeof(LineMessageDeleting)] = (line, msg) => RemoveLine(line),
                [typeof(LineMessageSelectedPrimaryKey)] = (line, msg) => SelectPrimaryKey(line),
                [typeof(LineMessageEditingTypeField)] = (line, msg) => EditTypeField(line),
                [typeof(LineMessageInputFieldEditing)] = (line, msg) => ValidateInputFieldEdit(line, msg as LineMessageInputFieldEditing),
                [typeof(LineMessageSelectedLinkingTable)] = (line, msg) => SelectLinkingTable(line),
                [typeof(LineMessageSelectedLinkingField)] = (line, msg) => SelectLinkingField(line),
            };
        }

        public async UniTask<TableData> Open(TableData tableData, List<TableData> otherTableDatas, GeneratorID generatorFieldID)
        {
            _generatorFieldID = generatorFieldID;

            this.tableData = tableData;
            this.otherTableDatas = otherTableDatas;

            tableData.Fields = await base.Open(tableData.Fields);
            
            return tableData;
        }

        protected override UILine<FieldData> CreateLine()
            => _factory.Create(_content, _broker, new FieldData(_generatorFieldID.Create()));

        protected override UILine<FieldData> CreateLine(FieldData data)
            => _factory.Create(_content, _broker, data.Clone() as FieldData);


        protected override void HandleMessageLine(UILine<FieldData> line, ILineMessage message)
            => _dictionaryMessageHandlers[message.GetType()](line as TableEditorLine, message);

        protected override void HandleGeneratedList(List<UILine<FieldData>> list)
        {
            foreach (TableEditorLine line in list)
            {
                if (line.IsPrimaryKey)
                {
                    _linePrimaryKey = line;
                    break;
                }
            }
        }

        private void SelectPrimaryKey(TableEditorLine line)
        {
            if (_linePrimaryKey != null) _linePrimaryKey.IsPrimaryKey = false;
            _linePrimaryKey = line;
        }

        private async void EditTypeField(TableEditorLine line)
        {
            var type = await _selectableList.Open("��������� ����: ", typesField);
            if (type != string.Empty) line.TypeField = type;
        }

        private async void ValidateInputFieldEdit(TableEditorLine sourceLine, LineMessageInputFieldEditing message)
        {
            var oldFieldName = message.OldText;
            var newFieldName = message.NewText;

            if (!string.IsNullOrEmpty(oldFieldName) && string.IsNullOrEmpty(newFieldName))
            {
                await _warningWindow.Show(WarningMessageEmptyTableNames);
                sourceLine.FieldName = oldFieldName;
                return;
            }

            if (string.IsNullOrEmpty(oldFieldName) && string.IsNullOrEmpty(newFieldName))
            {
                sourceLine.FieldName = newFieldName;
                return;
            }

            foreach (var line in _list)
            {
                if (line == sourceLine) continue;
                var fieldData = line.GetData();
                if (newFieldName == fieldData.Name)
                {
                    await _warningWindow.Show(WarningMessageIdenticalTableNames);
                    sourceLine.FieldName = oldFieldName;
                    return;
                }
            }

            sourceLine.FieldName = newFieldName;
        }

        private async void SelectLinkingTable(TableEditorLine line)
        {
            if (otherTableDatas.Count == 0)
            {
                await _warningWindow.Show(WarningMessageNotFoundLinkingTable);
                return;
            }

            var dictionary = new Dictionary<string, object>();
            for (int i = 0; i < otherTableDatas.Count; i++) dictionary.Add(otherTableDatas[i].Name, i);

            var result = await _selectableList.Open("��������� �������: ", dictionary);
            if (result == null) return;

            var index = (int)result;
            var selectedTableData = otherTableDatas[index];

            line.LinkingTable = new FieldData.LinkingElement()
            {
                ID = selectedTableData.ID,
                Name = selectedTableData.Name
            };
        }

        private async void SelectLinkingField(TableEditorLine line)
        {
            var fieldData = line.GetData();
            var linkingTableData = otherTableDatas.Find(data => data.ID == fieldData.LinkingTable.ID);
            var fieldsLinkingTableData = linkingTableData.Fields;

            if (fieldsLinkingTableData.Count == 0)
            {
                await _warningWindow.Show(WarningMessageNotFoundLinkingField);
                return;
            }

            var dictionary = new Dictionary<string, object>();
            for (int i = 0; i < fieldsLinkingTableData.Count; i++) dictionary.Add(fieldsLinkingTableData[i].Name, i);

            var result = await _selectableList.Open("��������� ����: ", dictionary);
            if (result == null) return;

            var index = (int)result;
            var selectedFieldData = fieldsLinkingTableData[index];

            line.LinkingField = new FieldData.LinkingElement()
            {
                ID = selectedFieldData.ID,
                Name = selectedFieldData.Name
            };
        }
    }
}