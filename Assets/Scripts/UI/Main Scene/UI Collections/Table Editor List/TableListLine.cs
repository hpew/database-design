using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;


namespace CITG.UI.MainScene.UICollections
{
    public class TableListLine : UILine<TableData>
    {
        [SerializeField] Button _editButton;
        [SerializeField] InputField _nameTable;

        TableData _data;

        public string TableName
        {
            get => _data.Name;
            set
            {
                _data.Name = value;
                _nameTable.text = value;

                if (string.IsNullOrEmpty(value)) _editButton.interactable = false;
                else _editButton.interactable = true;
            }
        }

        public override bool IsEmpty => string.IsNullOrEmpty(TableName);

        protected void Init(MessageBrokerUICollection<TableData> broker, TableData data)
        {
            base.Init(broker);

            this._data = data;

            _nameTable.text = data.Name;
            if (string.IsNullOrEmpty(data.Name)) _editButton.interactable = false;
            else _editButton.interactable = true;

            _editButton.onClick.AddListener(() => _broker.Send(this, new LineMessageEditTable()));

            _nameTable.onEndEdit.AddListener(value => broker.Send(this, new LineMessageInputFieldEditing()
            {
                OldText = TableName,
                NewText = value
            }));
        }
        public override TableData Destroy()
        {
            _nameTable.onEndEdit.RemoveAllListeners();
            _editButton.onClick.RemoveAllListeners();
            return base.Destroy();
        }

        public override TableData GetData() => _data;

        public void SetData(TableData data) => data = data;

        public class Factory : IFactory<Transform, MessageBrokerUICollection<TableData>,
           TableData, UILine<TableData>>
        {
            DiContainer _container;
            TableListLine _prefab;

            public Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _prefab = assets.TableEditorListLine;
            }

            public UILine<TableData> Create(Transform content, MessageBrokerUICollection<TableData> broker,
                TableData data)
            {
                var line = _container.InstantiatePrefab(_prefab, content).GetComponent<TableListLine>();
                line.name = "Line";

                line.Init(broker, data);

                return line;
            }
        }
    }

    public class TableData : ILineData, ICloneable
    {
        public int ID { get; }
        public string Name { get; set; }
        public List<FieldData> Fields { get; set; }

        public TableData(int ID)
        {
            this.ID = ID;
            Name = string.Empty;
            Fields = new List<FieldData>();
        }

        public TableData(int ID, string name, List<FieldData> fields)
        {
            this.ID = ID;
            Name = name;
            Fields = fields;
        }

        public object Clone()
        {
            var clonableFields = new List<FieldData>();
            foreach (var field in Fields) clonableFields.Add(field.Clone() as FieldData);


            return new TableData(ID, Name, clonableFields);
        }
    }

    public class LineMessageEditTable : ILineMessage { }
}