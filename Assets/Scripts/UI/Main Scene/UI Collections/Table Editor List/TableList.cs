using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.Utility;
using CITG.UI.MainScene.TaskHelpForm;


namespace CITG.UI.MainScene.UICollections
{
    public class TableList : UIList<TableData>
    {
        [Inject] TableListLine.Factory _factory;
        [Inject] TableEditor _tableEditor;

        [Inject] TaskForm _taskForm;

        GeneratorID _generatorTableID;
        GeneratorID _generatorFieldID;

        static string WarningMessageIdenticalTableNames = "������ ��������� ������� � ����������� ����������!";
        static string WarningMessageEmptyTableNames = "������ ��������� �������� ������� ������ ����� ������������!";

        public async UniTask<List<TableData>> Open(List<TableData> data, GeneratorID generatorTableID, GeneratorID generatorFieldID)
        {
            _generatorTableID = generatorTableID;
            _generatorFieldID = generatorFieldID;

            return await Open(data);
        }

        protected async override UniTask ShowForm()
        {
            await UniTask.WhenAll(base.ShowForm(), _taskForm.Show());
        }

        protected async override UniTask UnshowForm()
        {
            await UniTask.WhenAll(base.UnshowForm(), _taskForm.Unshow());
        }

        protected override UILine<TableData> CreateLine()
            => _factory.Create(_content, _broker, new TableData(_generatorTableID.Create()));

        protected override UILine<TableData> CreateLine(TableData data) 
            => _factory.Create(_content, _broker, data.Clone() as TableData);
          
        protected override void HandleMessageLine(UILine<TableData> line, ILineMessage message)
        {
            if (message is LineMessageDeleting) RemoveLine(line);
            else if (message is LineMessageEditTable) EditTable((TableListLine)line);
            else if (message is LineMessageInputFieldEditing editingMessage) 
                ValidateInputFieldEdit(line as TableListLine, editingMessage);
        }

        protected override void HandleDeletionLine(UILine<TableData> deletionLine)
        {
            var deletionTableData = deletionLine.GetData();

            foreach (var line in _list)
            {
                if (line == deletionLine) continue;
                var tableData = line.GetData();

                foreach (var fieldData in tableData.Fields)
                    if (fieldData.LinkingTable.ID == deletionTableData.ID)
                    {
                        fieldData.LinkingTable = FieldData.LinkingElement.None;
                        fieldData.LinkingField = FieldData.LinkingElement.None;
                        fieldData.IsSecondaryKey = false;
                    }
            }
        }

        protected override LockRect CreateLockRect()
        {
            return _factoryLockRect.Create(_rect, _taskForm.Rect);
        }

        private async void ValidateInputFieldEdit(TableListLine sourceLine, LineMessageInputFieldEditing message)
        {
            var oldNameTable = message.OldText;
            var newNameTable = message.NewText;

            if (!string.IsNullOrEmpty(oldNameTable) && string.IsNullOrEmpty(newNameTable))
            {
                await _warningWindow.Show(WarningMessageEmptyTableNames);
                sourceLine.TableName = oldNameTable;
                return;
            }

            if (string.IsNullOrEmpty(oldNameTable) && string.IsNullOrEmpty(newNameTable))
            {
                sourceLine.TableName = newNameTable;
                return;
            }

            foreach (var line in _list)
            {
                if (line == sourceLine) continue;
                var tableData = line.GetData();
                if (newNameTable == tableData.Name)
                {
                    await _warningWindow.Show(WarningMessageIdenticalTableNames);
                    sourceLine.TableName = oldNameTable;
                    return;
                }
            }

            sourceLine.TableName = newNameTable;

            var sourceTableData = sourceLine.GetData();
            foreach (var line in _list)
            {
                if (line == sourceLine) continue;

                var tableData = line.GetData();
                foreach (var fieldData in tableData.Fields)
                {
                    if (fieldData.LinkingTable.ID == sourceTableData.ID)
                        fieldData.LinkingTable = new FieldData.LinkingElement()
                        {
                            ID = sourceTableData.ID,
                            Name = sourceTableData.Name,
                        };
                }
            }
        }
        private async void EditTable(TableListLine sourceLine)
        {
            var tableData = sourceLine.GetData();
            var otherTableDatas = new List<TableData>();

            foreach (TableListLine line in _list)
            {
                if (line == sourceLine) continue;
                otherTableDatas.Add(line.GetData());
            }

            tableData = await _tableEditor.Open(tableData, otherTableDatas, _generatorFieldID);
            sourceLine.SetData(tableData);

            foreach (var otherTableData in otherTableDatas)
            {
                foreach (var otherFieldData in otherTableData.Fields)
                {
                    if (otherFieldData.LinkingTable.ID == tableData.ID)
                    {
                        var index = tableData.Fields.FindIndex(field => field.ID == otherFieldData.LinkingField.ID);
                        if (index < 0)
                        {
                            otherFieldData.LinkingTable = FieldData.LinkingElement.None;
                            otherFieldData.LinkingField = FieldData.LinkingElement.None;
                            otherFieldData.IsSecondaryKey = false;
                        }
                        else
                        {
                            var fieldData = tableData.Fields[index];
                            otherFieldData.LinkingField = new FieldData.LinkingElement()
                            {
                                ID = fieldData.ID,
                                Name = fieldData.Name,
                            };
                        }
                    }
                }
            }
        }
    }


}