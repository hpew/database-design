using System.Collections;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using CITG.Utility;
using Zenject;

namespace CITG.UI.MainScene
{
    public class ConfirmationWindow : MonoBehaviour
    {
        ConfirmButton _confirmButton;
        CancelButton _cancelButton;

        RectTransform _rect;
        LockRect.Factory _factory;

        [Inject]
        public void Construct(LockRect.Factory factory)
        {
            _confirmButton = gameObject.GetComponentInChildren<ConfirmButton>(true);
            _cancelButton = gameObject.GetComponentInChildren<CancelButton>(true);

            _rect = gameObject.GetComponent<RectTransform>();
            _rect.SetActive(false);

            _factory = factory;
        }

        public async UniTask<bool> Show()
        {
            var lockRect = _factory.Create(_rect);
            _rect.SetActive(true);

            var selectedButton = await ButtonObserver.Track(_confirmButton, _cancelButton);

            lockRect.Destroy();
            _rect.SetActive(false);

            return selectedButton == _confirmButton;
        }
    }
}