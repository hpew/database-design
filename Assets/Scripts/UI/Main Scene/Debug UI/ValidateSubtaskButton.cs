using UnityEngine.UI;

namespace CITG.UI.MainScene.DebugUI
{
    public class ValidateSubtaskButton : DebugButton 
    {
        Text _label;

        protected override void Awake()
        {
            base.Awake();
            _label = gameObject.GetComponentInChildren<Text>();
        }

        public void UpdateDescription(string description) => _label.text = description;
    }
}