using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UniRx;

namespace CITG.UI.MainScene.DebugUI
{
    public class DebugButton : Button
    {
        Image _image;

        protected override void Awake()
        {
            _image = gameObject.GetComponent<Image>();
            SetColor(Color.white);
        }

        public void ResetCorrectness() => SetColor(Color.white);
        public void DisplayCorrectness(bool isCorrectness) 
            => _image.color = isCorrectness? Color.green : Color.red;

        protected void SetColor(Color color) => _image.color = color;
    }
}