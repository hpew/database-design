using UnityEngine.UI;

namespace CITG.UI.MainScene.DebugUI
{
    public class DebugLabel : Text
    {
        public void Clear() => this.text = string.Empty;
    }
}