using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using CITG.Utility;

namespace CITG.UI.MainScene.DebugUI
{
    public class DebugUIDesigner : MonoBehaviour
    {
        ValidateTaskButton _validateTaskButton;
        ValidateSubtaskButton  _validateSubtaskButton;

        DebugLabel _label;

        private void Awake()
        {
            _validateTaskButton = gameObject.GetComponentInChildren<ValidateTaskButton>(true);
            _validateSubtaskButton = gameObject.GetComponentInChildren<ValidateSubtaskButton>(true);

            _label = gameObject.GetComponentInChildren<DebugLabel>(true);

            ResetUI();
        }

        public void ResetUI()
        {
            _validateTaskButton.SetActive(false);
            _validateSubtaskButton.SetActive(false);

            _label.SetActive(false);
        }

        public DebugButton ActivateTaskButton() => ActiveDebugButton(_validateTaskButton);

        public DebugButton ActivateSubtaskButton(string description)
        {
            _validateSubtaskButton.UpdateDescription(description);
            return ActiveDebugButton(_validateSubtaskButton);
        }

        public DebugLabel ActivateDebugLabel()
        {
            _label.Clear();
            _label.SetActive(true);

            return _label;
        }

        private DebugButton ActiveDebugButton(DebugButton button)
        {
            button.ResetCorrectness();
            button.SetActive(true);

            button.onClick.RemoveAllListeners();

            return button;
        }
    }
}