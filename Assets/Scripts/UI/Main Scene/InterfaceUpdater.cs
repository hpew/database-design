using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CITG.UI.MainScene
{
    using TaskHelpForm;
    using ToolbarSystem;
    using TaskSelectionTabbarSystem;

    public class InterfaceUpdater
    {
        Toolbar _toolbar;
        TaskSelectionTabbar _tabbar;
        FormPlaceholder _placeholder;

        public InterfaceUpdater(Toolbar toolbar, TaskSelectionTabbar tabbar, FormPlaceholder placeholder)
        {
            _toolbar = toolbar;
            _tabbar = tabbar;
            _placeholder = placeholder;
        }

        public void Update(TaskType type)
        {
            _toolbar.Show(type);
            _tabbar.Select(type);
            _placeholder.ChangeDescription(type);
        }
    }
}