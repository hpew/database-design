using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace CITG.UI.MainScene
{
    public class ViewStopWatch : MonoBehaviour
    {
        Text _time;

        private void Awake() => _time = gameObject.GetComponent<Text>();

        public void UpdateTime(TimeSpan span) => _time.text = span.ToString(@"hh\:mm\:ss");
    }
}