using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using CITG.Utility;
using CITG.System;

namespace CITG.UI.MainScene
{
    public class LockRect : MonoBehaviour
    {
        RectTransform _rect;

        RectTransform[] _childrens;
        Transform[] _oldParentsChildren;
        int[] _oldSiblingIndex;

        Action _callbackDestroyed;

        void Init(RectTransform[] childrens, Action callbackDestroyed)
        {
            _rect = gameObject.GetComponent<RectTransform>();

            _childrens = childrens;

            _oldParentsChildren = new Transform[_childrens.Length];
            _oldSiblingIndex = new int[_childrens.Length];

            for (int i = 0; i < _childrens.Length; i++)
            {
                var children = _childrens[i];

                _oldParentsChildren[i] = children.parent;
                _oldSiblingIndex[i] = children.GetSiblingIndex();

                children.SetParent(_rect);
            }

            _callbackDestroyed = callbackDestroyed;
        }

        public void Destroy()
        {
            for (int i = 0; i < _childrens.Length; i++)
            {
                var children = _childrens[i];

                children.SetParent(_oldParentsChildren[i]);
                children.SetSiblingIndex(_oldSiblingIndex[i]);
            }

            Array.Clear(_childrens, 0, _childrens.Length);
            Array.Clear(_oldParentsChildren, 0, _oldParentsChildren.Length);
            Array.Clear(_oldSiblingIndex, 0, _oldSiblingIndex.Length);

            _callbackDestroyed();
            _callbackDestroyed = null;

            GameObject.Destroy(gameObject);
        }

        public class Factory : IFactory<RectTransform[], LockRect>
        {
            DiContainer _container;
            LockRect _prefab;

            RectTransform _lockRectZone;

            int counterRect = 0;
            public bool IsCreated => counterRect != 0;

            public Factory(DiContainer container, Assets assets, [Inject(Id = "Lock Rect Zone")] RectTransform lockRectZone)
            {
                _container = container;
                _prefab = assets.LockRect;

                _lockRectZone = lockRectZone;

                counterRect = 0;
            }

            public LockRect Create(params RectTransform[] childrens)
            {
                var rect = _container.InstantiatePrefab(_prefab, _lockRectZone).GetComponent<LockRect>();

                rect.name = $"LockRect - {++counterRect}";

                rect.Init(childrens, () => counterRect--);

                return rect;
            }
        }
    }
}