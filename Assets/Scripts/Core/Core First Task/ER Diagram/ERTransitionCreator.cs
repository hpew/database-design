using System;
using System.Collections;
using System.Threading;
using Cysharp.Threading.Tasks;
using UniRx;
using UnityEngine;
using CITG.UI.MainScene;
using CITG.Utility;

namespace CITG.ERDiagram
{
    public class ERTransitionCreator
    {
        ERTransition.Factory _factory;
        FormCreatingRelationship _formCreatingRelationship;
        Camera _camera;

        public ERTransitionCreator(ERTransition.Factory factory, FormCreatingRelationship formCreatingRelationship, Camera camera)
        {
            _factory = factory;
            _formCreatingRelationship = formCreatingRelationship;
            _camera = camera;
        }

        public async UniTask<ERTransition> Create<T>(ERFragment firstFragment, Transform parent) where T : ERTransition
        {
            var transition = _factory.Create<T>(parent, firstFragment.Position);

            var isConfirm = await UpdateTrackingTransitionCreation(firstFragment, transition);

            if (!isConfirm)
            {
                transition.Destroy();
                return null;
            }

            return transition;

        }

        private async UniTask<bool> UpdateTrackingTransitionCreation(ERFragment firstFragment, ERTransition transition)
        {
            while (true)
            {
                var mousePosition = _camera.GetMouseWorldPosition();
                var targetFragment = UpdateOverlapPoint(mousePosition);

                if (targetFragment == firstFragment) targetFragment = null;
                else if (firstFragment.ContainsOutputTransition(targetFragment)) targetFragment = null;

                var newPosition = targetFragment != null ? (Vector2)targetFragment.Position : mousePosition;
                transition.LookAtPosition(newPosition);

                if (Input.GetMouseButton(0))
                {
                    if (targetFragment == null)
                    {
                        return false;
                    }
                    else
                    {
                        var result = await _formCreatingRelationship.Open();

                        if (result.IsConfirm)
                        {
                            transition.EstablishConnection(firstFragment, targetFragment, result.InputText, result.OutputText);
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }

                await UniTask.Yield();
            }
        }

        private ERFragment UpdateOverlapPoint(Vector2 mousePosition)
        {
            var collider = Physics2D.OverlapPoint(mousePosition);
            var targetFragment = collider?.GetComponent<ERFragment>();

            return targetFragment;
        }
    }
}