using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Zenject;
using UniRx;
using CITG.UI.MainScene;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.DragAndDrop;
using CITG.Utility;

namespace CITG.ERDiagram
{
    public class ERModel : IInitializable, IDisposable
    {
        private Camera _camera;
        private DragAndDropController _dadController;

        private ERFragment.Factory _fragmentFactory;
        private ERTransitionCreator _transitionCreator;

        private Transform _groupFragments;
        private Transform _groupTransitions;

        List<IERElement> _elements;
        List<ERFragment> _fragments;
        List<ERTransition> _transitions;

        IERElement _selectedElement;

        Validator _validator;

        LockRect.Factory _lrFactory;

        CompositeDisposable _receiver;
        IDisposable _disposableUpdate;

        private Vector3 spawnPosition;

        public ERModel(Validator validator, ERFragment.Factory fragmentFactory, ERTransitionCreator transitionCreator,
            Camera camera, DragAndDropController controller, LockRect.Factory lrFactory)
        {
            _fragmentFactory = fragmentFactory;
            _transitionCreator = transitionCreator;

            _groupFragments = new GameObject("ER Fragments").transform;
            _groupFragments.transform.position = Vector3.zero;

            _groupTransitions = new GameObject("ER Transitions").transform;
            _groupTransitions.transform.position = Vector3.zero;

            _camera = camera;
            spawnPosition = new Vector3(camera.transform.position.x, camera.transform.position.y, 0);

            _dadController = controller;

            _lrFactory = lrFactory;

            _validator = validator;

            _elements = new List<IERElement>();
            _fragments = new List<ERFragment>();
            _transitions = new List<ERTransition>();
        }

        public void Initialize()
        {
            SubscribeToMessages();
        }

        public void Dispose()
        {
            _receiver?.Dispose();
            _disposableUpdate?.Dispose();

            foreach (var element in _elements) element.Dispose();
        }

        public void Show()
        {
            _disposableUpdate = StartUpdate();
            _groupFragments.SetActive(true);
            _groupTransitions.SetActive(true);
        }

        public void Unshow()
        {
            _disposableUpdate?.Dispose();
            _groupFragments.SetActive(false);
            _groupTransitions.SetActive(false);
        }

        public Validator.Status Validate() => _validator.Validate(_fragments.ToArray(), _transitions.ToArray());

        private void SubscribeToMessages()
        {
            _receiver = new CompositeDisposable();

            MessageBroker.Default
                .Receive<MessageClickedToolbarButton<CreatingEntityShapeButton>>()
                .Subscribe(msg => CreateFragment<EREntity>())
                .AddTo(_receiver);

            MessageBroker.Default
                .Receive<MessageClickedToolbarButton<CreatingRelationshipShapeButton>>()
                .Subscribe(msg => CreateFragment<ERRelationship>())
                .AddTo(_receiver);

            MessageBroker.Default
                 .Receive<ERFragment.Message>()
                 .Where(msg => msg.TypeMessage == ERFragment.Message.Type.ArrowTransitionButtonClick)
                 .Subscribe(msg => CreateTransition<ERArrow>(msg.Fragment))
                 .AddTo(_receiver);

            MessageBroker.Default
                 .Receive<ERFragment.Message>()
                 .Where(msg => msg.TypeMessage == ERFragment.Message.Type.LineTransitionButtonClick)
                 .Subscribe(msg => CreateTransition<ERLine>(msg.Fragment))
                 .AddTo(_receiver);
        }

        private IDisposable StartUpdate() => Observable
            .EveryUpdate()
            .Subscribe(_ =>
            {
                if (_lrFactory.IsCreated) return;

                TrackMouseClick();
                TrackDeleteDown();
            });

        private void TrackMouseClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var mousePosition = _camera.GetMouseWorldPosition();
                var colliders = Physics2D.OverlapPointAll(mousePosition);

                var findSelectedElement = false;
                IERElement lastTargetElement = null;

                foreach (var collider in colliders)
                {
                    var element = collider.GetComponent<IERElement>();
                    if (element == null) continue;

                    if (element == _selectedElement)
                    {
                        findSelectedElement = true;
                        break;
                    }

                    lastTargetElement = element;
                }

                if (findSelectedElement) return;
                if (lastTargetElement != null)
                {
                    _selectedElement?.Unselect();
                    _selectedElement = lastTargetElement;
                    _selectedElement.Select();
                    _dadController.Clear();

                    if (_selectedElement is ERFragment fragment) _dadController.SetMover(fragment.Collider);
                }

                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    _selectedElement?.Unselect();
                    _selectedElement = null;
                    _dadController.Clear();
                }
            }
        }
        private void TrackDeleteDown()
        {
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                if (_selectedElement != null)
                {
                    if (_selectedElement is ERTransition transition) DestroyTransition(transition);
                    else if (_selectedElement is ERFragment fragment) DestroyFragment(fragment);

                    _selectedElement = null;
                    _dadController.Clear();
                }
            }
        }

        private void CreateFragment<T>() where T : ERFragment
        {
            var fragment = _fragmentFactory.Create<T>(_groupFragments, spawnPosition);
            _elements.Add(fragment);
            _fragments.Add(fragment);
        }
        private async void CreateTransition<T>(ERFragment fragment) where T : ERTransition
        {
            _disposableUpdate?.Dispose();
            _disposableUpdate = null;

            try
            {
                var transition = await _transitionCreator.Create<T>(fragment, _groupTransitions);

                _disposableUpdate = StartUpdate();

                if (transition == null) return;

                _transitions.Add(transition);
                _elements.Add(transition);
            }
            catch (OperationCanceledException)
            {
                return;
            }
        }

        private void DestroyFragment(ERFragment fragment)
        {
            foreach (var outputTransition in fragment.OutputTransitions)
            {
                outputTransition.Destroy();

                _elements.Remove(outputTransition);
                _transitions.Remove(outputTransition);

            }

            foreach (var inputTransition in fragment.InputTransitions)
            {
                {
                    inputTransition.Destroy();

                    _elements.Remove(inputTransition);
                    _transitions.Remove(inputTransition);

                }
            }

            fragment.Destroy();

            _elements.Remove(fragment);
            _fragments.Remove(fragment);
        }
        private void DestroyTransition(ERTransition transition)
        {
            transition.Destroy();

            _elements.Remove(transition);
            _transitions.Remove(transition);
        }
    }

    public interface IERElement : IDisposable
    {
        void Select();
        void Unselect();
        void Destroy();
    }


}