using System;
using System.Linq;
using UnityEngine;
using Zenject;
using CITG.SystemOptions;
using CITG.xNodeGraphes.ERModel;

namespace CITG.ERDiagram
{
    public class Validator
    {
        ERModelVerificationData verificationData;

        public Validator(Option.Context context) => verificationData = context.ERModelVerificationData;

        public Status Validate(ERFragment[] fragments, ERTransition[] transitions)
        {
            if (fragments.Length == 0 && transitions.Length == 0)
            {
                Debug.LogWarning("ERFragments and ERTransitions empty");
                return Status.None;
            }

            var verificationEntities = verificationData.VerificationEntities;
            var graph = verificationData.VerificationGraph;

            var entities = Array.ConvertAll(Array
                .FindAll(fragments, fragment => fragment is EREntity), fragment => fragment as EREntity);
            var relationships = Array.ConvertAll(Array
                .FindAll(fragments, fragment => fragment is ERRelationship), fragment => fragment as EREntity);
            
            if (entities.Length != verificationEntities.Length)
            {
                Debug.LogWarning($"���������� ��������� �� ��������� � �������� �����������: " +
                    $"{entities.Length} - {verificationEntities.Length}");
                return Status.None;
            }

            foreach (var verificationEntity in verificationEntities)
            {
                if (Array.Find(entities, entity => entity.Context == verificationEntity) == null)
                {
                    Debug.LogWarning($"����� �������� ��� \"{verificationEntity}\" �� �������.");
                    return 0;
                }
            }

            if (relationships.Length != verificationData.CountRelationships) return Status.CorrectFragments;
            else return graph.Validate(entities) ? Status.CorrectModel : Status.CorrectFragments;
        }

        public enum Status
        {
            CorrectFragments,
            CorrectModel,
            None
        }
    }
}