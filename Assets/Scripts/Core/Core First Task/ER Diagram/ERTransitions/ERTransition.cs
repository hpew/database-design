using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Zenject;
using UniRx;
using CITG.System;
using CITG.Utility;

namespace CITG.ERDiagram
{
    public class ERTransition : MonoBehaviour, IERElement
    {
        public ERFragment FirstFragment { get; private set; }
        public ERFragment SecondFragment { get; private set; }


        [SerializeField] protected TextMesh _firstText;
        [SerializeField] protected TextMesh _secondText;

        protected LineRenderer _lineRenderer;
        protected CapsuleCollider2D _collider;

        protected IDisposable _updateConnection;

        [SerializeField] protected float delta = 0.15f;
        [SerializeField] protected float scalerCollider = 1.5f;

        protected static string colorStandOut = "#A1C4E5";
        protected Color defaultColor;

        public string OutputText
        {
            get => _firstText.text;
            set => _firstText.text = value;
        }
        public string InputText
        {
            get => _secondText.text;
            set => _secondText.text = value;
        }

        protected virtual void Init()
        {
            _lineRenderer = gameObject.GetComponent<LineRenderer>();
            _collider = gameObject.GetComponent<CapsuleCollider2D>();

            defaultColor = _lineRenderer.startColor;
        }

        public void Destroy()
        {
            Dispose();
            GameObject.Destroy(gameObject);
        }

        public void Dispose()
        {
            _updateConnection?.Dispose();

            FirstFragment?.RemoveOutputTransition(this);
            SecondFragment?.RemoveInputTransition(this);

            FirstFragment = null;
            SecondFragment = null;
        }

        public virtual void Select()
        {
            Color newColor;
            if (ColorUtility.TryParseHtmlString(colorStandOut, out newColor))
            {
                _lineRenderer.material.SetColor("_Color", newColor);
            }
        }
        public virtual void Unselect()
        {
            _lineRenderer.material.SetColor("_Color", defaultColor);
        }

        public virtual void LookAtPosition(Vector3 position)
        {
            _lineRenderer.SetPosition(0, transform.position);
            _lineRenderer.SetPosition(1, position);
        }
        public virtual void EstablishConnection(ERFragment firstElement, ERFragment secondElement, string inputText, string outputText)
        {
            OutputText = outputText;
            InputText = inputText;

            _secondText.SetActive(true);
            _firstText.SetActive(true);

            _collider.enabled = true;

            FirstFragment = firstElement;
            FirstFragment.AddOutputTransition(this);

            SecondFragment = secondElement;
            SecondFragment.AddInputTransition(this);

            _updateConnection = Observable.EveryUpdate()
                .Subscribe(delegate
                {
                    UpdateConnection();
                });
        }

        protected virtual void FinallyUpdateConnection(UpdateConnectionData data) { }

        private void UpdateConnection()
        {
            var start = FirstFragment.Position;
            var end = SecondFragment.Position;

            var direction = (Vector2)(end - start).normalized;
            var distance = Vector2.Distance(start, end);

            var deltaX = delta * Vector2.Dot(direction, Vector2.up);
            var deltaY = delta * Vector2.Dot(direction, Vector2.left);

            var deltaStart = new Vector2(start.x + deltaX, start.y + deltaY);
            var deltaEnd = new Vector2(end.x + deltaX, end.y + deltaY);

            _lineRenderer.SetPosition(0, deltaStart);
            _lineRenderer.SetPosition(1, deltaEnd);

            var hitsStartToEnd = Physics2D.RaycastAll(deltaStart, direction, distance);
            var hitsEndToStart= Physics2D.RaycastAll(deltaEnd, -direction, distance);
          
            var hitEnd = Array.Find(hitsStartToEnd, x => x.collider == SecondFragment.Collider);
            var hitStart = Array.Find(hitsEndToStart, x => x.collider == FirstFragment.Collider);

            if (hitStart != default) SetText(_firstText, hitStart, direction);
            if (hitEnd != default) SetText(_secondText, hitEnd, -direction);


            if (!_collider.enabled)
                _collider.enabled = true;

            transform.position = deltaStart + direction * distance * .5f;
            transform.right = direction;
            _collider.size = new Vector2(distance * scalerCollider, _collider.size.y);

            var data = new UpdateConnectionData()
            {
                Start = start,
                End = end,
                DeltaStart = deltaStart,
                DeltaEnd = deltaEnd,
                Direction = direction,
                HitStart = hitStart,
                HitEnd = hitEnd,
            };

            FinallyUpdateConnection(data);
        }

        private void SetText(TextMesh text, RaycastHit2D hit, Vector2 direction) 
            => text.transform.position = hit.point + direction * 0.1f;

        protected struct UpdateConnectionData
        {
            public Vector2 Start;
            public Vector2 End;
            public Vector2 DeltaStart;
            public Vector2 DeltaEnd;
            public Vector2 Direction;
            public RaycastHit2D HitStart;
            public RaycastHit2D HitEnd;
        }

        public class Factory
        {
            DiContainer _container;
            TableERTransitionPrefabs _table;

            public Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _table = assets.TableERTransitionPrefabs;
            }

            public ERTransition Create<T>(Transform parent, Vector3 position) where T : ERTransition
            {
                var type = typeof(T);
                var prefab = _table[type];

                var transition = _container.InstantiatePrefab(prefab, parent).GetComponent<ERTransition>();
                transition.name = $"ER Transition {type.Name}";
                transition.transform.position = position;

                transition.Init();

                return transition;
            }

        }
    }
}