using System;
using UnityEngine;

namespace CITG.ERDiagram
{
    public class ERArrow : ERTransition
    {
        [SerializeField] Arrow firstArrow;
        [SerializeField] Arrow secondArrow;

        protected override void Init()
        {
            base.Init();

            firstArrow.Init();
            secondArrow.Init();

            firstArrow.SetActive(true);
            secondArrow.SetActive(false);
        }

        public override void LookAtPosition(Vector3 position)
        {
            base.LookAtPosition(position);

            var distance = Vector2.Distance(transform.position, position) * 0.5f;
            var direction = (position - transform.position).normalized;

            firstArrow.Direction = direction;
            firstArrow.Position = transform.position + direction * distance;
        }
        public override void EstablishConnection(ERFragment firstElement, ERFragment secondElement, string outputText, string inputText)
        {
            base.EstablishConnection(firstElement, secondElement, outputText, inputText);
            secondArrow.SetActive(true);
        }

        public override void Select()
        {
            Color newColor;
            if (ColorUtility.TryParseHtmlString(colorStandOut, out newColor))
            {
                _lineRenderer.material.SetColor("_Color", newColor);

                firstArrow.Color = newColor;
                secondArrow.Color = newColor;
            }
        }

        public override void Unselect()
        {
            _lineRenderer.material.SetColor("_Color", defaultColor);
            firstArrow.Color = defaultColor;
            secondArrow.Color = defaultColor;
        }


        protected override void FinallyUpdateConnection(UpdateConnectionData data)
        {
            var hitStart = data.HitStart;
            var hitEnd = data.HitEnd;
            var direction = data.Direction;

            firstArrow.Position = hitStart.point + direction * 0.3f;
            firstArrow.Direction = direction;

            secondArrow.Position = hitEnd.point - direction * 0.3f;
            secondArrow.Direction = direction;
        }

        [Serializable]
        struct Arrow
        {
            [SerializeField] GameObject gameObject;

            Transform transform;
            SpriteRenderer renderer;

            public Vector3 Position
            {
                get => transform.position;
                set => transform.position = value;
            }

            public Vector3 Direction
            {
                get => -transform.up;
                set => transform.up = -value;
            }

            public Color Color
            {
                get => renderer.color;
                set => renderer.color = value;
            }

            public void Init()
            {
                transform = gameObject.GetComponent<Transform>();
                renderer = gameObject.GetComponent<SpriteRenderer>();
            }

            public void SetActive(bool value) => gameObject.SetActive(value);
        }
    }
}