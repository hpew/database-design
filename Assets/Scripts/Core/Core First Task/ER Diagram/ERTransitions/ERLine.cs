using System;
using UnityEngine;

namespace CITG.ERDiagram
{
    public class ERLine : ERTransition
    {
        [SerializeField] Point point;

        protected override void Init()
        {
            base.Init();
            point.Init();
        }

        public override void LookAtPosition(Vector3 position)
        {
            base.LookAtPosition(position);

            var distance = Vector2.Distance(transform.position, position) * 0.5f;
            var direction = (position - transform.position).normalized;

            point.Position = transform.position + direction * distance;
        }

        public override void Select()
        {
            Color newColor;
            if (ColorUtility.TryParseHtmlString(colorStandOut, out newColor))
            {
                _lineRenderer.material.SetColor("_Color", newColor);
                point.Color = newColor;
            }
        }

        public override void Unselect()
        {
            _lineRenderer.material.SetColor("_Color", defaultColor);
            point.Color = defaultColor;
        }


        protected override void FinallyUpdateConnection(UpdateConnectionData data)
        {
            var hitStart = data.HitStart;
            var direction = data.Direction;

            point.Position = hitStart.point + direction * 0.3f;
        }

        [Serializable]
        struct Point
        {
            [SerializeField] GameObject gameObject;

            Transform transform;
            SpriteRenderer renderer;

            public Vector3 Position
            {
                get => transform.position;
                set => transform.position = value;
            }

            public Color Color
            {
                get => renderer.color;
                set => renderer.color = value;
            }

            public void Init()
            {
                transform = gameObject.GetComponent<Transform>();
                renderer = gameObject.GetComponent<SpriteRenderer>();
            }

            public void SetActive(bool value) => gameObject.SetActive(value);
        }
    }
}
