using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Zenject;
using UniRx;
using Sirenix.OdinInspector;
using CITG.UI;
using CITG.System;
using CITG.Utility;
using CITG.DragAndDrop;

namespace CITG.ERDiagram
{
    public abstract class ERFragment : MonoBehaviour, IERElement, IPointerClickHandler
    {
        List<ERTransition> _outputTransitions;
        List<ERTransition> _inputputTransitions;

        [ShowInInspector] public ERTransition[] OutputTransitions => _outputTransitions?.ToArray();
        [ShowInInspector] public ERTransition[] InputTransitions => _inputputTransitions?.ToArray();

        public Collider2D Collider { get; private set; }

        [SerializeField] Image _background;
        [SerializeField] Button _arrowLinkDisplayButton;
        [SerializeField] Button _lineLinkDisplayButton;

        private InputFieldERFragment _inputName;
        private Canvas _canvas;

        CancellationTokenSource _cancellationAnimation;

        public Vector3 Position
        {
            get => transform.position;
            private set => transform.position = value;
        }

        public string Context => _inputName.text;

        Color defaultColor;
        static string colorStandOut = "#767373";

        static int selectedOrder = 1;
        static int defaultOrder = 0;

        protected virtual void Init(Vector3 position)
        {
            Collider = gameObject.GetComponent<Collider2D>();

            _inputName = gameObject.GetComponentInChildren<InputFieldERFragment>(true);
            _inputName.onEndEdit.AddListener(delegate { _inputName.Unselect(); });

            _canvas = gameObject.GetComponentInChildren<Canvas>(true);

            _outputTransitions = new List<ERTransition>();
            _inputputTransitions = new List<ERTransition>();

            _arrowLinkDisplayButton.SetRaycastTarget(false);
            _lineLinkDisplayButton.SetRaycastTarget(false);

            transform.position = new Vector3(position.x, position.y, 0f);
            defaultColor = _background.color;

            _arrowLinkDisplayButton.onClick.AddListener(()
                => MessageBroker.Default.Publish(new Message(this, Message.Type.ArrowTransitionButtonClick)));

            _lineLinkDisplayButton.onClick.AddListener(()
                => MessageBroker.Default.Publish(new Message(this, Message.Type.LineTransitionButtonClick)));
        }

        public void Destroy()
        {
            Dispose();
            GameObject.Destroy(gameObject);
        }

        public void Dispose()
        {
            _cancellationAnimation?.Cancel();

            _outputTransitions.Clear();
            _inputputTransitions.Clear();
        }

        public void OnPointerClick(PointerEventData eventData) { if (eventData.clickCount == 2) _inputName.Select(); }

        public void Select()
        {
            Color color;
            if (ColorUtility.TryParseHtmlString(colorStandOut, out color)) _background.color = color;
            _canvas.sortingOrder = selectedOrder;
            
            ActivateLinkDisplayButtons();
        }
        public void Unselect()
        {
            _background.color = defaultColor;
            _canvas.sortingOrder = defaultOrder;

            DeactivateLinkDisplayButtons();
        }

        public void AddOutputTransition(ERTransition transion) => _outputTransitions.Add(transion);
        public void AddInputTransition(ERTransition transition) => _inputputTransitions.Add(transition);

        public void RemoveOutputTransition(ERTransition transition) => _outputTransitions.Remove(transition);
        public void RemoveInputTransition(ERTransition transition) => _inputputTransitions.Remove(transition);

        public bool ContainsOutputTransition(ERFragment fragment)
        {
            var transition = _outputTransitions.Find(transition => transition.SecondFragment == fragment);
            return transition != null;
        }
        public bool ContainsInputTransition(ERFragment fragment)
        {
            var transition = _inputputTransitions.Find(transition => transition.FirstFragment == fragment);
            return transition != null;
        }

        protected virtual async UniTask ShowLinkDisplayButtons(CancellationToken token) { }
        protected virtual async UniTask UnshowLinkDisplayButtons(CancellationToken token) { }

        private async void ActivateLinkDisplayButtons() 
        {
            _cancellationAnimation?.CancelUnity();

            try
            {
                _cancellationAnimation = new CancellationTokenSource();
                var token = _cancellationAnimation.Token;

                await ShowLinkDisplayButtons(token);

                _arrowLinkDisplayButton.SetRaycastTarget(true);
                _lineLinkDisplayButton.SetRaycastTarget(true);
            }
            catch (OperationCanceledException)
            {
                return;
            }
            finally
            {
                _cancellationAnimation?.Dispose();
                _cancellationAnimation = null;
            }
        }

        private async void DeactivateLinkDisplayButtons()
        {
            _arrowLinkDisplayButton.SetRaycastTarget(false);
            _lineLinkDisplayButton.SetRaycastTarget(false);

            _cancellationAnimation?.CancelUnity();

            try
            {
                _cancellationAnimation = new CancellationTokenSource();
                var token = _cancellationAnimation.Token;

                await UnshowLinkDisplayButtons(token);
            }
            catch (OperationCanceledException)
            {
                return;
            }
            finally
            {
                _cancellationAnimation?.Dispose();
                _cancellationAnimation = null;
            }
        }


        public class Factory
        {
            DiContainer _container;
            TableERFragmentPrefabs _table;

            private Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _table = assets.TableERFragmentPrefabs;
            }

            public ERFragment Create<T>(Transform parent, Vector3 position) where T : ERFragment
            {
                var type = typeof(T);
                var prefab = _table[type];


                var fragment = _container.InstantiatePrefab(prefab, parent).GetComponent<ERFragment>();
                fragment.name = $"ER Fragment {type.Name}";

                fragment.Init(position);

                return fragment;
            }
        }

        public class Message
        {
            public ERFragment Fragment { get; private set; }
            public Type TypeMessage { get; private set; }

            public Message(ERFragment fragment, Type type)
            {
                Fragment = fragment;
                TypeMessage = type;
            }

            public enum Type
            {
                ArrowTransitionButtonClick,
                LineTransitionButtonClick,
            }

        }
    }
}