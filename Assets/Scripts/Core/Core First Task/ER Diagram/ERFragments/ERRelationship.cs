using System;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using CITG.Utility;

namespace CITG.ERDiagram
{
    public class ERRelationship : ERFragment 
    {
        [SerializeField] RectTransform _canvasLine;
        [SerializeField] RectTransform _canvasArrow;

        [SerializeField] PositonsCanvas positionsCanvasLine;
        [SerializeField] PositonsCanvas positionsCanvasArrow;

        [SerializeField] float timeAnimation = 0.25f;

        protected override async UniTask ShowLinkDisplayButtons(CancellationToken token)
        {
            var taskLine = _canvasLine.LerpAnchoredPosition(_canvasLine.anchoredPosition, positionsCanvasLine.VisiblePosition, timeAnimation, token);
            var taskArrow = _canvasArrow.LerpAnchoredPosition(_canvasArrow.anchoredPosition, positionsCanvasArrow.VisiblePosition, timeAnimation, token);

            await UniTask.WhenAll(taskLine, taskArrow);
        }

        protected override async UniTask UnshowLinkDisplayButtons(CancellationToken token)
        {
            var taskLine = _canvasLine.LerpAnchoredPosition(_canvasLine.anchoredPosition, positionsCanvasLine.HiddenPosition, timeAnimation, token);
            var taskArrow = _canvasArrow.LerpAnchoredPosition(_canvasArrow.anchoredPosition, positionsCanvasArrow.HiddenPosition, timeAnimation, token);

            await UniTask.WhenAll(taskLine, taskArrow);
        }


        [Serializable]
        struct PositonsCanvas
        {
            public Vector2 HiddenPosition;
            public Vector2 VisiblePosition;
        }
    }
}