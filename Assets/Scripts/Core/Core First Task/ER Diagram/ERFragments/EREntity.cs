using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using CITG.Utility;

namespace CITG.ERDiagram
{
    public class EREntity : ERFragment
    {
        [SerializeField] RectTransform _groupButtons;

        [SerializeField] float timeAnimation = 0.25f;
        Vector2 defaultPosition;

        protected override void Init(Vector3 position)
        {
            base.Init(position);
            defaultPosition = _groupButtons.anchoredPosition;
        }

        protected override async UniTask ShowLinkDisplayButtons(CancellationToken token)
        {
            var targetPosition = new Vector2(defaultPosition.x, 0f);
            await _groupButtons.LerpAnchoredPosition(_groupButtons.anchoredPosition, targetPosition, timeAnimation, token);
        }

        protected override async UniTask UnshowLinkDisplayButtons(CancellationToken token)
        {
            await _groupButtons.LerpAnchoredPosition(_groupButtons.anchoredPosition, defaultPosition, timeAnimation, token);
        }
    }
}