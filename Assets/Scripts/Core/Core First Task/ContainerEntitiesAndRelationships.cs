using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;
using UniRx;
using CITG.UI.MainScene.UICollections;
using CITG.UI.MainScene.ToolbarSystem;

namespace CITG.EntitiesAndRelationships
{
    public class ContainerEntitiesAndRelationships : IInitializable, IDisposable
    {
        EntityFormationList _entityFormationList;
        RelationshipsFormationList _relationshipsFormationList;

        List<EntityFormationLineData> _entities;
        List<RelationshipsFormationLineData> _relationships;

        CompositeDisposable _receiver;

        public ContainerEntitiesAndRelationships(EntityFormationList entityFormationList, RelationshipsFormationList relationshipsFormationList)
        {
            _entityFormationList = entityFormationList;
            _relationshipsFormationList = relationshipsFormationList;

            _entities = new List<EntityFormationLineData>();
            _relationships = new List<RelationshipsFormationLineData>();
        }

        public void Initialize()
        {
            _receiver = new CompositeDisposable();

            MessageBroker.Default.Receive<MessageClickedToolbarButton<EntityFormationButton>>()
                .Subscribe(async msg =>
                {
                    var button = msg.Button;
                    button.SwitchOn();

                    _entities = await _entityFormationList.Open(_entities);

                    button.SwitchOff();

                }).AddTo(_receiver);

            MessageBroker.Default.Receive<MessageClickedToolbarButton<FormingRelationshipsButton>>()
                .Subscribe(async msg =>
                {
                    var button = msg.Button;
                    button.SwitchOn();
                    
                    _relationships = await _relationshipsFormationList.Open(_relationships);

                    button.SwitchOff();
                }).AddTo(_receiver);
        }

        public void Dispose()
        {
            _receiver?.Dispose();
        }

        public List<string> GetEntities() => _entities.ConvertAll(entity => entity.NameEntity);
    }
}