using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;
using Zenject;
using CITG.UI;
using CITG.Utility;

namespace CITG.DragAndDrop
{
    public class DragAndDropBehaviour : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        protected Camera _camera;
        protected Collider2D _collider;
        protected InteractiveZone _zone;

        protected Vector2 offset;

        private bool isFrozen = false;
        private bool isDown = false; //�������. OnDrag �����������, ����� OnPointerDown ��������� �� �������� ������� ����� ������� 

        [ShowInInspector]
        public bool IsFrozen
        {
            get => isFrozen;
            set => isFrozen = value;
        }

        protected Vector2 Position
        {
            get => transform.position;
            set => transform.position = new Vector3(value.x, value.y, transform.position.z);
        }

        [Inject]
        void Construct(Camera camera, InteractiveZone zone)
        {
            _collider = gameObject.GetComponent<Collider2D>();
            _camera = camera;
            _zone = zone;
        }

        public virtual  void OnPointerDown(PointerEventData eventData)
        {
          //  Debug.Log("DOWN");
            offset = _camera.GetMouseWorldPosition() - Position;
            isDown = true;
        }    

        public virtual void OnPointerUp(PointerEventData eventData)
        {
           // Debug.Log("UP");
            offset = Vector2.zero;
            isDown = false;
        }

        public virtual void OnDrag(PointerEventData eventData)
        {
            return;

            if (!isDown) return;
            if (isFrozen) return;

            var newPosition = _camera.GetMouseWorldPosition() - offset;

            var sizeX = _collider.bounds.size.x * .5f;
            var sizeY = _collider.bounds.size.y * .5f;

            var min = new Vector2(newPosition.x - sizeX, newPosition.y - sizeY);
            var max = new Vector2(newPosition.x + sizeX, newPosition.y + sizeY);

            var rect = _zone.GetRectOnCamera();

            if (min.x < rect.min.x) newPosition.x = rect.min.x + sizeX;
            if (min.y < rect.min.y) newPosition.y = rect.min.y + sizeY;
            if (max.x > rect.max.x) newPosition.x = rect.max.x - sizeX;
            if (max.y > rect.max.y) newPosition.y = rect.max.y - sizeY;
           
            Position = newPosition;
        }
    }
}