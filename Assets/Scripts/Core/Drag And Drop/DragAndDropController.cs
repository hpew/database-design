using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UniRx;
using CITG.Utility;
using CITG.UI;
using CITG.UI.MainScene;

namespace CITG.DragAndDrop
{
    public class DragAndDropController : IDisposable
    {
        private Camera _camera;
        private Collider2D _collider;
        private InteractiveZone _zone;

        LockRect.Factory _lockRectFactory;

        private IDisposable _disposableUpdate;

        private Vector2 lastPosition;
        private Vector2 offset;
        private bool isDown = false; 

        private Vector2 Position
        {
            get => _collider.transform.position;
            set => _collider.transform.position = new Vector3(value.x, value.y, _collider.transform.position.z);
        }

        public DragAndDropController(Camera camera, InteractiveZone zone, LockRect.Factory lockRectFactory)
        {
            _camera = camera;
            _zone = zone;
            _lockRectFactory = lockRectFactory;
        }

        public void Dispose() => _disposableUpdate?.Dispose();

        public void SetMover(Collider2D collider)
        {
            if (collider == _collider) return;

            Clear();
            _collider = collider;

            HandleMouseButtonDown();

            _disposableUpdate = Observable.EveryUpdate()
                .Subscribe(_ => Update());
        }

        public void Clear()
        {
            _disposableUpdate?.Dispose();
            _disposableUpdate = null;

            _collider = null;
        }
        private void Update()
        {
            if (_lockRectFactory.IsCreated) return;

            if (Input.GetMouseButtonDown(0)) HandleMouseButtonDown();
            if (Input.GetMouseButtonUp(0)) HandleMouseButtonUp();

            if (isDown) UpdateDrag();
        }

        private void HandleMouseButtonDown()
        {
            var mousePosition = _camera.GetMouseWorldPosition();
            var colliders = Physics2D.OverlapPointAll(mousePosition);

            if (Array.Find(colliders, collider => collider == _collider) != null)
            {
                lastPosition = mousePosition;
                offset = mousePosition - Position;

                isDown = true;
            }
        }

        private void HandleMouseButtonUp() => isDown = false;

        private void UpdateDrag()
        {
            if (_collider == null) return;

            var mousePosition = _camera.GetMouseWorldPosition();
            if (lastPosition.Approximately(mousePosition)) return;

            lastPosition = mousePosition;
            var newPosition = mousePosition - offset;

            var sizeX = _collider.bounds.size.x * .5f;
            var sizeY = _collider.bounds.size.y * .5f;

            var min = new Vector2(newPosition.x - sizeX, newPosition.y - sizeY);
            var max = new Vector2(newPosition.x + sizeX, newPosition.y + sizeY);

            var rect = _zone.GetRectOnCamera();

            if (min.x < rect.min.x) newPosition.x = rect.min.x + sizeX;
            if (min.y < rect.min.y) newPosition.y = rect.min.y + sizeY;
            if (max.x > rect.max.x) newPosition.x = rect.max.x - sizeX;
            if (max.y > rect.max.y) newPosition.y = rect.max.y - sizeY;

            Position = newPosition;
        }
    }
}