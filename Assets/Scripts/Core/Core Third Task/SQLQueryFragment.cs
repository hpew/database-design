using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using CITG.Utility;
using Zenject;
using CITG.UI;
using CITG.UI.MainScene;

namespace CITG.VisualSQLQueryConstructionSystem
{
    [SelectionBase]
    public class SQLQueryFragment : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [SerializeField] string value;
        [SerializeField] float scaleModifySelecting = 1.2f;
        [SerializeField] float scaleModifyReviews = 1.7f;
        [SerializeField] Vector2 overlapBoxSize;

        private Camera _camera;
        private Collider2D _collider;
        private InteractiveZone _zone;

        private LockRect.Factory _factory;

        private Canvas _canvas;
        private Text _text;

        private SQLQueryFragmentPlace _place;

        private Vector2 startPosition;
        private Vector2 defaultScale;
        private Vector2 offset;

        private bool isEnglared = false;
        private bool isSelected = false;

        
        private LayerMask layer;

        private static int selectedOrder = 2;
        private static int defaultOrder = 1;


        public string Value => value;

        public Vector2 Position
        {
            get => transform.position; 
            private set => transform.position = new Vector3(value.x, value.y, transform.position.z);
        }

        [Inject]
        void Construct(Camera camera, InteractiveZone zone, LockRect.Factory factory)
        {
            _collider = gameObject.GetComponent<Collider2D>();
            _canvas = gameObject.GetComponentInChildren<Canvas>(true);
            _text = gameObject.GetComponentInChildren<Text>(true);

            _camera = camera;
            _zone = zone;
            _factory = factory;

            _text.text = value;

            defaultScale = transform.localScale;

            layer = LayerMask.GetMask("SQL Query Fragment Place");
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left && !isEnglared) Select();
            else if (eventData.button == PointerEventData.InputButton.Right && !isSelected) Increase();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (eventData.button == PointerEventData.InputButton.Left && isSelected) Unselect();
            else if (eventData.button == PointerEventData.InputButton.Right && isEnglared) Reduce();
        }

        public void OnDrag(PointerEventData eventData)
        {
            if (!isSelected) return;

            var newPosition = _camera.GetMouseWorldPosition() - offset; 

            var sizeX = _collider.bounds.size.x * .5f;
            var sizeY = _collider.bounds.size.y * .5f;

            var min = new Vector2(newPosition.x - sizeX, newPosition.y - sizeY);
            var max = new Vector2(newPosition.x + sizeX, newPosition.y + sizeY);

            var rect = _zone.GetRectOnCamera();

            if (min.x < rect.min.x) newPosition.x = rect.min.x + sizeX;
            if (min.y < rect.min.y) newPosition.y = rect.min.y + sizeY;
            if (max.x > rect.max.x) newPosition.x = rect.max.x - sizeX;
            if (max.y > rect.max.y) newPosition.y = rect.max.y - sizeY;

            
            var collider = Physics2D.OverlapBox(Position, overlapBoxSize, 4, layer);

            if (collider == null && _place != null)
            {
                _place.StopStandOut();
                _place = null;
            }
            else if (collider != null)
            {
                var targetPlace = collider.GetComponent<SQLQueryFragmentPlace>();
                if (targetPlace != null && targetPlace != _place)
                {
                    _place?.StopStandOut();
                    _place = null;

                    if (targetPlace.CurrentFragment == null)
                    {
                        _place = targetPlace;
                        _place.StandOut();
                    }
                }
            }

            Position = newPosition;
        }

        public void ChangePosition(Vector2 position)
        {
            Position = position;
            startPosition = transform.localPosition;
        }

        private void Select()
        {
            isSelected = true;

            offset = _camera.GetMouseWorldPosition() - Position;
            _place?.ClearFragment(this);

            _canvas.sortingOrder = selectedOrder;
            transform.localScale = defaultScale * scaleModifySelecting;
        }

        private void Unselect()
        {
            isSelected = false;

            if (_place == null) transform.localPosition = startPosition;
            else if (_place.CurrentFragment == null)
            {
                Position = _place.transform.position;
                _place.SetFragment(this);
            }

            _canvas.sortingOrder = defaultOrder;
            transform.localScale = defaultScale;
        }

        private void Increase()
        {
            isEnglared = true;

            _canvas.sortingOrder = selectedOrder;
            transform.localScale = defaultScale * scaleModifyReviews;
        }

        private void Reduce()
        {
            isEnglared = false;

            _canvas.sortingOrder = defaultOrder;
            transform.localScale = defaultScale;
        }

#if UNITY_EDITOR
        private void OnDrawGizmos()
        {
            var defaultColor = Gizmos.color;
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(Position, overlapBoxSize); ;
            Gizmos.color = defaultColor;
        }
#endif

    }
}
