using System;
using UnityEngine;
using CITG.UI;
using CITG.xNodeGraphes.SQLQueryGraphSystem;
using CITG.SystemOptions;

namespace CITG.VisualSQLQueryConstructionSystem
{

    public class VisualSQLQueryManager
    {
        VisualSQLQueryGroup _groupQueries;
        GroupSwitchButtons _groupSwitchButtons;
        SQLQueryGraph[] _graphes;

        public VisualSQLQueryManager(Option.Context context,
            VisualSQLQueryGroup group, GroupSwitchButtons groupSwitchButtons)
        {
            _groupQueries = group;
            _groupSwitchButtons = groupSwitchButtons;

            _graphes = new SQLQueryGraph[context.VisualSQLQueryDatas.Count];
            for (int i = 0; i < context.VisualSQLQueryDatas.Count; i++)
                _graphes[i] = context.VisualSQLQueryDatas[i].Graph;
        }

        public void Activate()
        {
            _groupSwitchButtons.Activate(_groupQueries.Count, SwitchButton);
            _groupQueries.Activate();
        }
        public void Unactivate()
        {
            _groupSwitchButtons.Unactivate();
            _groupQueries.Unactivate();
        }

        public void Show()
        {
            _groupSwitchButtons.Show();
            _groupQueries.Unshow();
        }
        public void Unshow()
        {   
            _groupSwitchButtons.Unshow();
            _groupQueries.Unshow();
        }

        public bool[] Validate()
        {
            var validations = new bool[_groupQueries.Count];

            for (int i = 0; i < validations.Length; i++)
                validations[i] = _graphes[i].CheckFragments(_groupQueries[i].GetGeneratedCodeFragments());

            return validations;
        }

        public bool ValidateCurrentQuery()
        {
            var current = _groupQueries.Current;
            return _graphes[current.index].CheckFragments(current.query.GetGeneratedCodeFragments());
        }

        private void SwitchButton(int index)
        {
            _groupQueries.SwitchQuery(index);
        }    
    }
}