using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using Zenject;


namespace CITG.VisualSQLQueryConstructionSystem 
{
    public class VisualSQLQuery : MonoBehaviour
    {
        SQLQueryFragment[] _fragments;
        SQLQueryFragmentPlace[] _places;

        public SQLQueryFragment[] Fragments => _fragments;
        public SQLQueryFragmentPlace[] Places => _places;

        public Vector2 LocalPosition
        {
            get => transform.localPosition;
            set => transform.localPosition = value;
        }

        public string Description { get; private set; }

        public Queue<string> GetGeneratedCodeFragments()
        {
            var queue = new Queue<string>();

            foreach (var place in _places)
            {
                var fragment = place.CurrentFragment;
                if (fragment == null) continue;

                queue.Enqueue(fragment.Value);
            }

            return queue;
        }

        public void Validate()
        {

        }

        private void Mix()
        {
            int[] arr = { 1, 2, 3, 4, 5 };

            var positions = new Vector2[_fragments.Length];
            for (int i = 0; i < _fragments.Length; i++)
                positions[i] = _fragments[i].Position;

            positions = positions.OrderBy(position => Random.Range(0, positions.Length)).ToArray();

            for (int i = 0; i < positions.Length; i++)
                _fragments[i].ChangePosition(positions[i]);
        }

        public class Factory : IFactory<VisualSQLQuery, Transform, Vector3, string, VisualSQLQuery>
        {
            DiContainer _container;

            public Factory(DiContainer container) => _container = container;

            public VisualSQLQuery Create(VisualSQLQuery prefab, Transform parent,
                Vector3 localPosition, string description)
            {
                var query = _container.InstantiatePrefab(prefab, parent).GetComponent<VisualSQLQuery>();

                query.LocalPosition = localPosition;
                query.Description = description;
                query.name = query.name.Replace("(Clone)", "");

                query._fragments = query.GetComponentsInChildren<SQLQueryFragment>();
                query._places = query.GetComponentsInChildren<SQLQueryFragmentPlace>();
                query.Mix();

                return query;
            }
        }
    }
}