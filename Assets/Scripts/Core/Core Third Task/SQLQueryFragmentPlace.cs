using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;


namespace CITG.VisualSQLQueryConstructionSystem
{
    public class SQLQueryFragmentPlace : MonoBehaviour
    {
        public SQLQueryFragment CurrentFragment { get; private set; }
        private SpriteRenderer _sprite;

        static string selectionColor = "#9D9D9D";

        [Inject]
        void Construct() => _sprite = gameObject.GetComponent<SpriteRenderer>();

        public void StandOut()
        {
            Color color;
            if (ColorUtility.TryParseHtmlString(selectionColor, out color))
                _sprite.color = color;
        }

        public void StopStandOut() => _sprite.color = Color.white;

        public void SetFragment(SQLQueryFragment fragment) => CurrentFragment = fragment;

        public void ClearFragment(SQLQueryFragment fragment)
        {
            if (CurrentFragment != fragment) return;
            CurrentFragment = null;
        }
    }
}