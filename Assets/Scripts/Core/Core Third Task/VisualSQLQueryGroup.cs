using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Zenject;
using CITG.SystemOptions;
using CITG.Utility;
using CITG.xNodeGraphes.SQLQueryGraphSystem;

namespace CITG.VisualSQLQueryConstructionSystem
{
    public class VisualSQLQueryGroup : MonoBehaviour
    {
        [SerializeField] Text _description;

        List<VisualSQLQuery> _queries;
        VisualSQLQuery _currentQuery;
        int currentIndex = -1;

        VisualSQLQuery.Factory _factory;

        [Inject]
        void Construct(DiContainer cointaer, Option.Context context, VisualSQLQuery.Factory factory)
        {
            Unshow();

            _factory = factory;
            _queries = new List<VisualSQLQuery>();

            var datas = context.VisualSQLQueryDatas;

            foreach (var data in datas)
            {
                var query = _factory.Create(data.SQLQuery, transform, Vector3.zero, data.Description);
                _queries.Add(query);

                query.SetActive(false);
            }
        }

        public int Count => _queries.Count;
        public VisualSQLQuery this[int index] => _queries[index];
        public (VisualSQLQuery query, int index) Current => (_currentQuery, currentIndex);

        public void Activate()
        {
            _currentQuery = _queries[0];
            currentIndex = 0;

            _description.text = _currentQuery.Description;

            _currentQuery.SetActive(true);
            Show();
        }

        public void Unactivate()
        {
            if (_currentQuery == null) return;

            _currentQuery.SetActive(false);
            Unshow();

            _currentQuery = null;
            currentIndex = -1;
        }

        public void Show() => gameObject.SetActive(true);
        public void Unshow() => gameObject.SetActive(false);

        public void SwitchQuery(int index)
        {
            _currentQuery.SetActive(false);

            _currentQuery = _queries[index];
            currentIndex = index;

            _description.text = _currentQuery.Description;
            _currentQuery.SetActive(true);
        }

    }
}