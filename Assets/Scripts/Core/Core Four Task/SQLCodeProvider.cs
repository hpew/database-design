using System;
using System.Text;
using Zenject;
using UniRx;
using CITG.UI;
using CITG.UI.MainScene;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.SystemOptions;

namespace CITG.SQLCode
{
    public class SQLCodeProvider
    {
        Container[] _containers;

        int current;

        public SQLCodeProvider(Option.Context context)
        {
            var datas = context.SQLCodeDatas;
            _containers = new Container[datas.Length];

            for (int i = 0; i < _containers.Length; i++)
            {
                _containers[i] = new Container(
                    ValidateCode(datas[i].StartCode),
                    ValidateCode(datas[i].FinalCode),
                    datas[i].Tittle);
            }

            current = 0;
        }

        public Container Current => _containers[current];
        public int CurrentIndex => current;
        public int Count => _containers.Length;
        public Container this[int index]
            => _containers[index];

        public Container Switch(int index)
        {
            current = index;
            return _containers[current];
        }

        public string Reset()
        {
            _containers[current].SavedCode = _containers[current].StartCode;
            return _containers[current].SavedCode;
        }

        public string Update(string code)
        {
            _containers[current].SavedCode = ValidateCode(code);
            return _containers[current].SavedCode;
        }

        private static string ValidateCode(string code)
        {
            StringBuilder _builder = new StringBuilder();
            for (int i = 0; i < code.Length; i++)
            {
                if ((int)code[i] == 13) continue;
                _builder.Append(char.ToLower(code[i]));
            }

            return _builder.ToString();
        }


        public struct Container
        {
            public string SavedCode;
            public string StartCode;
            public string FinalCode;

            public string Tittle;

            public Container(string startCode, string finalCode, string tittle)
            {
                SavedCode = StartCode = startCode;
                FinalCode = finalCode;
                Tittle = tittle;
            }
        }
    }
}