using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace CITG.SQLCode
{
    public class CodeParser
    {
        static char[] tokenSymbols = { '(', ')', '{', '}', ',' };

        public CodeParser() { }

        public bool Validate(string inputCode, string matchCode, bool isCreatedDatabase = false)
        {
            Stack<string> inputStack;
            Stack<string> matchStack;

            if (!GetTokens(inputCode, out inputStack)) return false;
            if (!GetTokens(matchCode, out matchStack))
            {
                Debug.LogWarning("Match Stack has not correct datas: contains incorrect characters");
                return false;
            }

            //Debug.Log("Input Stack: ");
            //foreach (var token in inputStack) Debug.Log(token);

            //Debug.Log("Match Stack: ");
            //foreach (var token in matchStack) Debug.Log(token);

            if (inputStack.Count == 0) return false;
            
            if (matchStack.Count == 0)
            {
                Debug.LogWarning("Match Stack has not correct datas: stack is empty");
                return false;
            }

            if (!isCreatedDatabase)
            {

                if (matchStack.Pop() != ")")
                {
                    Debug.LogWarning("Match Stack has not correct datas");
                    return false;
                }

                if (inputStack.Pop() != ")") return false;

                if (inputStack.Peek() == ",") inputStack.Pop();
            }

            while (matchStack.Count != 0)
            {
                if (inputStack.Count == 0) return false;

                var matchToken = matchStack.Pop();
                var inputToken = inputStack.Pop();

                if (matchToken != inputToken) return false;
            }

            if (inputStack.Count != 0) return false;

            return true;
        }

        private bool GetTokens(string code, out Stack<string> stack)
        {
            stack = new Stack<string>();
            var currentToken = new StringBuilder();

            for (int i = 0; i < code.Length; i++)
            {
                var symbol = code[i];

                if (char.IsLetterOrDigit(symbol)) currentToken.Append(symbol);
                else if (tokenSymbols.Contains(symbol))
                {
                    if (currentToken.Length != 0)
                    {
                        stack.Push(currentToken.ToString());
                        currentToken = currentToken.Clear();
                    }

                    stack.Push(Convert.ToString(code[i]));
                }
                else if (char.IsWhiteSpace(symbol) || char.IsControl(symbol))
                {
                    if (currentToken.Length != 0)
                    {
                        stack.Push(currentToken.ToString());
                        currentToken = currentToken.Clear();
                    }
                }
                else return false;
            }

            if (currentToken.Length != 0) stack.Push(currentToken.ToString());

            return true;
        }
    }
}