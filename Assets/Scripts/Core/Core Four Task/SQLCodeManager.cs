using System;
using Zenject;
using UniRx;
using CITG.UI;
using CITG.UI.MainScene;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.SystemOptions;

namespace CITG.SQLCode
{
    public class SQLCodeManager : IInitializable, IDisposable
    {
        SQLCodeProvider _provider;
        CodeParser _parser;

        WindowSQLCode _window;
        GroupSwitchButtons _group;

        IDisposable _receiver;

        public SQLCodeManager(SQLCodeProvider provider, GroupSwitchButtons group, WindowSQLCode window)
        {
            _provider = provider;
            _parser = new CodeParser();

            _window = window;
            _group = group;

            _window.Unshow();
        }

        public void Initialize()
        {
            _receiver = MessageBroker.Default.Receive<MessageClickedToolbarButton<CodeResetButton>>()
                .Subscribe(msg =>
                {
                    _window.Code = _provider.Reset();
                });
        }
        public void Dispose() => _receiver?.Dispose();

        public void Activate()
        {
            _group.Activate(_provider.Count, SwitchCode);
            var container = _provider.Current;

            _window.Tittle = container.Tittle;
            _window.Code = container.SavedCode;

            _window.Show(ValidateEndEdit);
        }

        public void Unactivate()
        {
            _group.Unactivate();
            _window.Unshow();
        }

        public bool[] Validate()
        {
            var validations = new bool[_provider.Count];

            for (int i = 0; i < _provider.Count; i++)
            {
                var container = _provider[i];
                validations[i] = _parser.Validate(container.SavedCode, container.FinalCode, i == 0);
            }    
                
            return validations;
        }

        public bool ValidateCurrentCode()
        {
            var current = _provider.Current;
            return _parser.Validate(current.SavedCode, current.FinalCode, _provider.CurrentIndex == 0);
        }


        private void SwitchCode(int index)
        {
            var container = _provider.Switch(index);

            _window.Tittle = container.Tittle;
            _window.Code = container.SavedCode;
        }

        private string ValidateEndEdit(string code)
            => _provider.Update(code);
    }
}