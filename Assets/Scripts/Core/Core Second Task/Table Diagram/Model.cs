using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Zenject;
using UniRx;
using CITG.UI.MainScene;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.UI.MainScene.UICollections;
using CITG.DragAndDrop;
using CITG.Utility;
using CITG.TableSystem;

namespace CITG.TableDiagram
{
    public class Model : IInitializable, IDisposable
    {
        private Camera _camera;
        private DragAndDropController _dadController;

        private Fragment.Factory _fragmentFactory;

        private Transform _groupFragments;

        List<Fragment> _fragments;
        Fragment _selectedElement;

        TableManager _tableManager;
        ConnectionMap _map;

        SelectableList _selectableList;
        LockRect.Factory _lrFactory;
        WarningWindow _warningWindow;

        CompositeDisposable _receiver;
        IDisposable _disposableUpdate;

        private Vector3 spawnPosition;

        static string WarningMessageNotFoundLinkingTable = "��� ��������� ������!";

        public bool IsShowing => _groupFragments.gameObject.activeSelf;

        public Model(Fragment.Factory fragmentFactory, Camera camera, TableManager tableManager, ConnectionMap map,
            DragAndDropController controller, LockRect.Factory lrFactory, SelectableList selectableList,
            WarningWindow warningWindow)
        {
            _fragmentFactory = fragmentFactory;

            _groupFragments = new GameObject("Table Diagram Fragments").transform;
            _groupFragments.transform.position = Vector3.zero;
            _groupFragments.SetActive(false);

            _camera = camera;
            spawnPosition = new Vector3(camera.transform.position.x, camera.transform.position.y, 0);

            _tableManager = tableManager;
            _map = map;

            _dadController = controller;

            _selectableList = selectableList;
            _lrFactory = lrFactory;
            _warningWindow = warningWindow;

            _fragments = new List<Fragment>();
        }

        public void Initialize()
        {
            _receiver = new CompositeDisposable();

            MessageBroker.Default
                .Receive<MessageClickedToolbarButton<TableDisplayButton>>()
                .Subscribe(async msg =>
                {
                    msg.Button.SwitchOn();
                    await CreateFragment();
                    msg.Button.SwitchOff();
                })
                .AddTo(_receiver);

            _tableManager.onUpdatedTables += UpdateFragments;
        }

        public void Dispose()
        {
            _receiver?.Dispose();
            _disposableUpdate?.Dispose();

            _tableManager.onUpdatedTables -= UpdateFragments;

            foreach (var fragment in _fragments) fragment.Dispose();
        }

        public void Activate()
        {
            _disposableUpdate = StartUpdate();
            _groupFragments.SetActive(true);
            _map.StartTrack(); //Table Diagram Connection Parent
        }

        public void Unactivate()
        {
            _disposableUpdate?.Dispose();
            _groupFragments.SetActive(false);
            _map.StopTrack();

            _selectedElement?.Unselect();
        }

        public void Show()
        {
            _groupFragments.SetActive(true);
            _map.Show();
        }
        public void Unshow()
        {
            _groupFragments.SetActive(false);
            _map.Unshow();
        }

        private void UpdateFragments()
        {
            var tables = _tableManager.Tables;

            var tempFragments = new List<Fragment>();
            foreach (var fragment in _fragments)
            {
                var table = tables.Find(table => table.ID == fragment.TableID);
                if (table == null)
                {
                    if (fragment == _selectedElement)
                    {
                        _dadController.Clear();
                        _selectedElement = null;
                    }    
                    fragment.Destroy();
                    continue;
                }

                fragment.UpdateData(table);
                tempFragments.Add(fragment);
            }

            _fragments.Clear();
            _fragments = tempFragments;

            _map.UpdateData(_fragments);
        }

        private IDisposable StartUpdate() => Observable
            .EveryUpdate()
            .Subscribe(_ =>
            {
                if (_lrFactory.IsCreated) return;

                TrackMouseClick();
                TrackDeleteDown();
            });

        private void TrackMouseClick()
        {
            if (Input.GetMouseButtonDown(0))
            {
                var mousePosition = _camera.GetMouseWorldPosition();
                var colliders = Physics2D.OverlapPointAll(mousePosition);

                var findSelectedElement = false;
                Fragment lastTargetElement = null;

                foreach (var collider in colliders)
                {
                    var element = collider.GetComponent<Fragment>();
                    if (element == null) continue;

                    if (element == _selectedElement)
                    {
                        findSelectedElement = true;
                        break;
                    }

                    lastTargetElement = element;
                }

                if (findSelectedElement) return;
                if (lastTargetElement != null)
                {
                    _selectedElement?.Unselect();
                    _selectedElement = lastTargetElement;
                    _selectedElement.Select();
                    _dadController.Clear();

                    if (_selectedElement is Fragment fragment) _dadController.SetMover(fragment.Collider);
                }

                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    _selectedElement?.Unselect();
                    _selectedElement = null;
                    _dadController.Clear();
                }
            }
        }
        private void TrackDeleteDown()
        {
            if (Input.GetKeyDown(KeyCode.Delete))
            {
                if (_selectedElement != null)
                {
                    if (_selectedElement is Fragment fragment) DestroyFragment(fragment);

                    _selectedElement = null;
                    _dadController.Clear();
                }
            }
        }

        private async Cysharp.Threading.Tasks.UniTask CreateFragment()
        {
            var tables = _tableManager.Tables;
            var filteredTables = new Dictionary<string, object>();

            foreach (var table in tables)
            {
                if (_fragments.Find(fragment => fragment.TableID == table.ID) == null)
                    filteredTables.Add(table.Name, table);
            }

            if (filteredTables.Count == 0)
            {
                await _warningWindow.Show(WarningMessageNotFoundLinkingTable);
                return;
            }

            var result = await _selectableList.Open("��������� �������: ", filteredTables);
            if (result == null) return;

            var selectedTable = result as Table;

            var fragment = _fragmentFactory.Create(_groupFragments, spawnPosition, selectedTable);

            _map.UpdateData(fragment, _fragments);
            _fragments.Add(fragment);
        }

        private void DestroyFragment(Fragment fragment)
        {
            fragment.Destroy();
            _fragments.Remove(fragment);

            _map.UpdateData(_fragments);
        }
    }
}