using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;
using CITG.Utility;
using CITG.TableSystem;

namespace CITG.TableDiagram
{
    public class LineFragment : MonoBehaviour
    {
        [SerializeField] Image _imagePrimaryKey;
        [SerializeField] Image _imageSecondaryKey;
        [SerializeField] Text _value;

        [SerializeField] Chain _chain;
        
        Field _field;

        List<LineFragment> _referencingLines;
        LineFragment _targetLine;

        public int ID => _field.ID;
        public int LinkingTableID => _field.LinkingTable.ID;
        public int LinkingFieldID => _field.LinkingField.ID;
        public int Number { get; private set; }

        Field.TypeKey TypeKey
        {
            set
            {
                switch (value)
                {
                    case Field.TypeKey.None:
                        _imagePrimaryKey.SetActive(false);
                        _imageSecondaryKey.SetActive(false);
                        break;
                    case Field.TypeKey.Primary:
                        _imagePrimaryKey.SetActive(true);
                        _imageSecondaryKey.SetActive(false);
                        break;
                    case Field.TypeKey.Secondary:
                        _imagePrimaryKey.SetActive(false);
                        _imageSecondaryKey.SetActive(true);
                        break;
                }
            }
        }

        public string Value
        {
            get => _value.text;
            set => _value.text = value;
        }

        public void Init(Field field, int number)
        {
            _referencingLines = new List<LineFragment>();
            _targetLine = null;

            _field = field;

            TypeKey = field.Key;
            Value = field.Name;

            Number = number;
        }

        public void Destroy() => GameObject.Destroy(gameObject);

        private void AddReferencing(LineFragment line) => _referencingLines.Add(line);

        public class Factory : IFactory<Transform, Field, int, LineFragment>
        {
            DiContainer _container;
            LineFragment _prefab;

            public Factory(DiContainer container, Assets assets)
            {
                _container = container;
                _prefab = assets.LineFragmentTableDiagram;
            }

            public LineFragment Create(Transform content, Field field, int number)
            {
                var line = _container.InstantiatePrefab(_prefab, content).GetComponent<LineFragment>();
                line.name = "Line Fragment Table Diagram";

                line.Init(field, number);

                return line;
            }
        }

        public struct Chain
        {
            public Transform Left;
            public Transform Right;
        }
    }
}