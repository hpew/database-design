using System;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;
using CITG.System;
using CITG.TableSystem;
using CITG.Utility;

namespace CITG.TableDiagram
{
    public class ConnectionMap
    {
        List<Connection> _connections;
        Connection.Factory _factory;

        Transform _groupConnections;

        IDisposable _tracker;

        public ConnectionMap(Assets assets)
        {
            _groupConnections = new GameObject("Table Diagram Connection Parent").transform;
            _factory = new Connection.Factory(assets, _groupConnections);
            _connections = new List<Connection>();

            _groupConnections.SetActive(false);
        }

        public void StartTrack()
        {
            _tracker = Observable.EveryUpdate()
                .Subscribe(_ => Track());

            _groupConnections.SetActive(true);
        }

        public void StopTrack()
        {
            _tracker?.Dispose();
            _groupConnections.SetActive(false);
        }

        public void Show() => _groupConnections.SetActive(true);
        public void Unshow() => _groupConnections.SetActive(false);

        public void UpdateData(Fragment addedFragment, List<Fragment> currentFragments)
        {
            foreach (var firstLine in addedFragment.Lines)
            {
                if (firstLine.LinkingTableID == -1) continue;

                var secondFragment = currentFragments.Find(fragment => fragment.TableID == firstLine.LinkingTableID);
                if (secondFragment == null) continue;

                var secondLine = secondFragment.Lines.Find(line => line.ID == firstLine.LinkingFieldID);
                if (secondLine == null) continue;


                _connections.Add(CreateConnection(addedFragment, secondFragment, firstLine, secondLine));
            }

            foreach (var firstFragment in currentFragments)
            {
                foreach (var firstLine in firstFragment.Lines)
                {
                    if (firstLine.LinkingTableID != addedFragment.TableID) continue;

                    var secondLine = addedFragment.Lines.Find(line => line.ID == firstLine.LinkingFieldID);
                    if (secondLine == null) continue;

                    if (_connections.FindIndex(connection => connection.FirstLine == secondLine 
                    && connection.SecondLine == firstLine) > 0)
                        continue;

                    _connections.Add(CreateConnection(firstFragment, addedFragment, firstLine, secondLine));
                }
            }
        }

        public void UpdateData(List<Fragment> fragments)
        {
            Clear();
            foreach (var firstFragment in fragments)
            {
                foreach (var firstLine in firstFragment.Lines)
                {
                    if (firstLine.LinkingTableID == -1) continue;

                    var secondFragment = fragments.Find(fragment => fragment.TableID == firstLine.LinkingTableID);
                    if (secondFragment == null) continue;

                    var secondLine = secondFragment.Lines.Find(line => line.ID == firstLine.LinkingFieldID);
                    if (secondLine == null) continue;

                    if (_connections.FindIndex(connection => connection.FirstLine == secondLine 
                    && connection.SecondLine == firstLine) > 0)
                        continue;

                    _connections.Add(CreateConnection(firstFragment, secondFragment, firstLine, secondLine));
                }
            }
        }

        private void Clear()
        {
            foreach (var connection in _connections) connection.Destroy();
            _connections.Clear();
        }

        private void Track()
        {
            if (_connections.Count == 0) return;
            foreach (var connection in _connections) connection.Update();
        }

        private Connection CreateConnection(Fragment firstFragment, Fragment secondFragment, LineFragment firstLine, LineFragment secondLine)
        {
            var fragments = new Couple<Fragment>()
            {
                FirstElement = firstFragment,
                SecondElement = secondFragment
            };

            var lines = new Couple<LineFragment>()
            {
                FirstElement = firstLine,
                SecondElement = secondLine
            };

            return  _factory.Create(fragments, lines);
        }

        
    }

    public class Connection
    {
        public Fragment FirstFragment { get; private set; }
        public LineFragment FirstLine { get; private set; }

        public Fragment SecondFragment { get; private set; }
        public LineFragment SecondLine { get; private set; }

        private LineRenderer _lineRenderer;

        LineLocationsChain _firstLocations;
        LineLocationsChain _secondLocations;

        MinDistance oldMinDistance = MinDistance.None;

        public Connection(Couple<Fragment> fragments, Couple<LineFragment> lines, LineRenderer lineRenderer)
        {
            FirstFragment = fragments.FirstElement;
            SecondFragment = fragments.SecondElement;

            FirstLine = lines.FirstElement;
            SecondLine = lines.SecondElement;

            _lineRenderer = lineRenderer;

            if (FirstLine.Number < 6) _firstLocations = FirstFragment.LinesChains[FirstLine.Number - 1];
            else _firstLocations = FirstFragment.LinesChains[FirstFragment.LinesChains.Length - 1];

            if (SecondLine.Number < 6) _secondLocations = SecondFragment.LinesChains[SecondLine.Number - 1];
            else _secondLocations = SecondFragment.LinesChains[SecondFragment.LinesChains.Length - 1];

            _lineRenderer.SetActive(true);
        }

        public void Destroy()
        {
            GameObject.Destroy(_lineRenderer.gameObject);
        }

        public void Update()
        {
            var minDistance = GetMinDistance();

            switch (minDistance)
            {
                case MinDistance.LL:
                    _lineRenderer.SetPosition(0, _firstLocations.PositionLeftChain);
                    _lineRenderer.SetPosition(1, _secondLocations.PositionLeftChain);

                    //_firstChain.position = _firstLocations.PositionLeftChain;
                    //_secondChain.position = _secondLocations.PositionLeftChain;
                    break;
                case MinDistance.LR:
                    _lineRenderer.SetPosition(0, _firstLocations.PositionLeftChain);
                    _lineRenderer.SetPosition(1, _secondLocations.PositionRightChain);
                    break;
                case MinDistance.RL:
                    _lineRenderer.SetPosition(0, _firstLocations.PositionRightChain);
                    _lineRenderer.SetPosition(1, _secondLocations.PositionLeftChain);
                    break;
                case MinDistance.RR:
                    _lineRenderer.SetPosition(0, _firstLocations.PositionRightChain);
                    _lineRenderer.SetPosition(1, _secondLocations.PositionRightChain);
                    break;
            }

            if (minDistance == oldMinDistance) return;
            oldMinDistance = minDistance;
        }

        private float[] GetDistances()
        {
            float[] distances = new float[4];

            distances[0] = Vector3.Distance(_firstLocations.PositionLeftChain, _secondLocations.PositionLeftChain); //LL
            distances[1] = Vector3.Distance(_firstLocations.PositionLeftChain, _secondLocations.PositionRightChain); //LR
            distances[2] = Vector3.Distance(_firstLocations.PositionRightChain, _secondLocations.PositionLeftChain); // RL
            distances[3] = Vector3.Distance(_firstLocations.PositionRightChain, _secondLocations.PositionRightChain); //RR 

            return distances;
        }
        private int GetIndexMin(float[] array)
        {
            float min = array[0];
            int index = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] < min)
                {
                    min = array[i];
                    index = i;
                }
            }

            return index;
        }

        private MinDistance GetMinDistance()
        {
            var distances = GetDistances();
            var index = GetIndexMin(distances);

            return (MinDistance)index;
        }

        enum MinDistance
        {
            None = -1,
            LL = 0,
            LR = 1,
            RL = 2,
            RR = 3
        }

        public class Factory : IFactory<Couple<Fragment>, Couple<LineFragment>, Connection>
        {
            LineRenderer _lineRendererPrefab;
            Transform _chainPrefab;

            Transform _groupConnections;

            public Factory(Assets assets, Transform groupConnections)
            {
                _lineRendererPrefab = assets.ConnectionTableFragmentDiagram;
                _chainPrefab = assets.ChainTableFragmentDiagram;

                _groupConnections = groupConnections;
            }

            public Connection Create(Couple<Fragment> fragments, Couple<LineFragment> lines)
            {

                var lineRenderer = GameObject.Instantiate(_lineRendererPrefab, _groupConnections);

                var chains = new Couple<Transform>()
                {
                    FirstElement = GameObject.Instantiate(_chainPrefab, lineRenderer.transform),
                    SecondElement = GameObject.Instantiate(_chainPrefab, lineRenderer.transform),
                };

                return new Connection(fragments, lines, lineRenderer);

            }
        }
    }
}