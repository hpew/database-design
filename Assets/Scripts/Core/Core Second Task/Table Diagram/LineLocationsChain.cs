using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using CITG.Utility;

namespace CITG.TableDiagram
{
    public class LineLocationsChain : MonoBehaviour
    {
        [SerializeField] Transform _leftChain;
        [SerializeField] Transform _rightChain;

        public Vector3 PositionLeftChain => _leftChain.transform.position;
        public Vector3 PositionRightChain => _rightChain.transform.position;
    }
}