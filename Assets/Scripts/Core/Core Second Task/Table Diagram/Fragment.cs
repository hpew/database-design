using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.System;
using CITG.TableSystem;

namespace CITG.TableDiagram
{
    public class Fragment : MonoBehaviour
    {
        [SerializeField] Text _tableName;
        [SerializeField] Image _background;
        [SerializeField] RectTransform _content;

        public Collider2D Collider { get; private set; }
        private Canvas _canvas;

        private Table _table;

        public List<LineFragment> Lines { get; private set; }
        private LineFragment.Factory _factory;

        public LineLocationsChain[] LinesChains { get; private set; }

        Color defaultColor;
        static string colorStandOut = "#767373";

        static int selectedOrder = 1;
        static int defaultOrder = 0;

        public Vector3 Position => transform.position;

        public int TableID => _table.ID;
        public string TableName
        {
            get => _tableName.text;
            private set => _tableName.text = value;
        }

        protected virtual void Init(Vector3 position, Table table, LineFragment.Factory factory)
        {
            Collider = gameObject.GetComponent<Collider2D>();
            LinesChains = gameObject.GetComponentsInChildren<LineLocationsChain>(true);

            _canvas = gameObject.GetComponentInChildren<Canvas>(true);

            transform.position = new Vector3(position.x, position.y, 0f);
            defaultColor = _background.color;

            _factory = factory;

            UpdateData(table);
        }

        public void Destroy()
        {
            Dispose();
            GameObject.Destroy(gameObject);
        }

        public void Dispose()
        {
        }

        public void UpdateData(Table table)
        {
            _table = table;
            TableName = _table.Name;

            if (Lines != null)
            {
                foreach (var line in Lines) line.Destroy();
                Lines.Clear();
            }

            Lines = new List<LineFragment>();
            foreach (var field in table.Fields)
            {
                var line = _factory.Create(_content, field, Lines.Count + 1);
                Lines.Add(line);
            }
        }

        public void Select()
        {
            Color color;
            if (ColorUtility.TryParseHtmlString(colorStandOut, out color)) _background.color = color;
            _canvas.sortingOrder = selectedOrder;
        }
        public void Unselect()
        {
            _background.color = defaultColor;
            _canvas.sortingOrder = defaultOrder;
        }


        public class Factory : IFactory<Transform, Vector3, Table, Fragment>
        {
            DiContainer _container;
            Fragment _prefab;
            LineFragment.Factory _factory;

            private Factory(DiContainer container, Assets assets, LineFragment.Factory factory)
            {
                _container = container;
                _prefab = assets.FragmentTableDiagram;
                _factory = factory;
            }

            public Fragment Create(Transform parent, Vector3 position, Table table)
            {
                var fragment = _container.InstantiatePrefab(_prefab, parent).GetComponent<Fragment>();
                fragment.name = $"Fragment Table Diagram";

                fragment.Init(position, table, _factory);

                return fragment;
            }
        }
    }
}