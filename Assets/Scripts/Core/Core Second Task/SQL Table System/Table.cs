﻿using System;
using System.Collections.Generic;
using CITG.UI.MainScene.UICollections;

namespace CITG.TableSystem
{
    public class Table
    {
        public int ID { get; }
        public string Name { get; private set; }
        public List<Field> Fields { get; private set; }

        public Table(int ID, string name)
        {
            this.ID = ID;
            Name = name;
            Fields = new List<Field>();
        }

        public Table(TableData data)
        {
            this.ID = data.ID;
            Name = data.Name;

            Fields = new List<Field>();
            foreach (var line in data.Fields) Fields.Add(new Field(line));
        }

        public TableData GetData()
        {
            var fieldDatas = new List<FieldData>();
            foreach (var field in Fields) fieldDatas.Add(field.GetData());

            return new TableData(ID, Name, fieldDatas);
        }

        public void SetData(TableData data)
        {
            Name = data.Name;
            Fields = new List<Field>();

            foreach (var line in data.Fields)
            {
                Fields.Add(new Field(line));
            }
        }
    }

    public class Field
    {
        public int ID { get; }
        public string Name { get; set; }
        public TypeField Type { get; set; }
        public TypeKey Key { get; private set; }
        public FieldData.LinkingElement LinkingTable { get; set; }
        public FieldData.LinkingElement LinkingField { get; set; }

        static Dictionary<TypeField, string> _dictionaryTypeFieldToLiteral = new Dictionary<TypeField, string>()
        {
            [TypeField.Numeric] = "числовой",
            [TypeField.Text] = "текст",
            [TypeField.Date] = "дата и время"
        };
        static Dictionary<string, TypeField> _dictionaryLiteralToTypeField = new Dictionary<string, TypeField>()
        {
            ["числовой"] = TypeField.Numeric,
            ["текст"] = TypeField.Text,
            ["дата и время"] = TypeField.Date
        };

        public Field(FieldData data)
        {
            ID = data.ID;
            Name = data.Name;
            Type = _dictionaryLiteralToTypeField[data.Type];

            if (data.IsPrimaryKey) Key = TypeKey.Primary;
            else if (data.IsSecondaryKey) Key = TypeKey.Secondary;
            else Key = TypeKey.None;

            LinkingTable = data.LinkingTable;
            LinkingField = data.LinkingField;
        }

        public FieldData GetData()
        {
            var data = new FieldData(ID);
            data.Name = Name;
            data.Type = _dictionaryTypeFieldToLiteral[Type];

            switch (Key)
            {
                case TypeKey.Primary:
                    data.IsPrimaryKey = true;
                    break;
                case TypeKey.Secondary:
                    data.IsSecondaryKey = true;
                    break;
            }

            data.LinkingTable = LinkingTable;
            data.LinkingField = LinkingField;

            return data;
        }

        public enum TypeKey
        {
            None, 
            Primary,
            Secondary
        }

        public enum TypeField
        {
            Numeric, 
            Text,
            Date
        }
    }
}
