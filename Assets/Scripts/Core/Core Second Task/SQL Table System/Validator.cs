using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Sirenix.OdinInspector;
using CITG.SystemOptions;

namespace CITG.TableSystem
{
    public class Validator
    {
        TableSystemVerificationData verificationData;

        public Validator(Option.Context context) => verificationData = context.TableSystemVerificationData;

        public Statistics Validate(List<Table> tables)
        {
            var tableVerifications = verificationData.TableVerifications;

            var statistics = new Statistics()
            {
                Tables = new Dictionary<TableSystemVerificationData.TableVerification, bool>(),
                IsExtraTable = false,
            };

            if (tables.Count > tableVerifications.Length) statistics.IsExtraTable = true;

            //if (tables.Count != tableVerifications.Length)
            //{
            //    Debug.LogWarning($"Tables empty or does not match the quantity requirement {tableVerifications.Length}");
            //    return false;
            //}

            foreach (var tableVer in tableVerifications)
            {
                statistics.Tables.Add(tableVer, false);

                var table = tables.Find(table => table.Name == tableVer.Name);
                if (table == null)
                {
                    Debug.LogWarning($"Table not found '{tableVer.Name}'");
                    continue;
                }

                var fields = table.Fields;
                var fieldVerifications = tableVer.FieldVerifications;

                if (fields.Count != fieldVerifications.Length)
                {
                    Debug.LogWarning($"{table.Name} -> fields empty " +
                        $"or does not match the quantity requirement {fieldVerifications.Length}");
                    continue;
                }

                var isErrorFind = false;
                foreach (var fieldVer in fieldVerifications)
                {
                    var field = fields.Find(field => field.Name == fieldVer.Name);
                    if (field == null)
                    {
                        Debug.LogWarning($"{table.Name} -> field not found '{fieldVer.Name}'");
                        isErrorFind = true;
                        break;
                    }

                    if (field.Type != fieldVer.TypeField)
                    {
                        Debug.LogWarning($"{table.Name} -> {field.Name} -> type mismatch '{fieldVer.TypeField}'");
                        isErrorFind = true;
                        break;
                    }

                    if (field.Key != fieldVer.TypeKey)
                    {
                        Debug.LogWarning($"{table.Name} -> {field.Name} -> key Mismatch '{fieldVer.TypeKey}'");
                        isErrorFind = true;
                        break;
                    }

                    if (field.LinkingTable.Name != fieldVer.LinkTable)
                    {
                        Debug.LogWarning($"{table.Name} -> {field.Name} -> related table mismatch '{fieldVer.LinkTable}'");
                        isErrorFind = true;
                        break;
                    }

                    if (field.LinkingField.Name != fieldVer.LinkField)
                    {
                        Debug.LogWarning($"{table.Name} -> {field.Name} -> related field mismatch '{fieldVer.LinkField}'");
                        isErrorFind = true;
                        break;
                    }
                }

                statistics.Tables[tableVer] = !isErrorFind;
            }

            return statistics;
        }

        public struct Statistics
        {
            public Dictionary<TableSystemVerificationData.TableVerification, bool> Tables;
            public bool IsExtraTable;
        }
    }
}