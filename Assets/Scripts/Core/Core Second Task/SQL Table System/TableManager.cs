using System;
using System.Collections.Generic;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;
using UniRx;
using CITG.EntitiesAndRelationships;
using CITG.UI.MainScene.UICollections;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.UI.MainScene.TaskHelpForm;
using CITG.Utility;


namespace CITG.TableSystem
{
    public class TableManager : IInitializable, IDisposable
    {
        TableList _editorList;
        ContainerEntitiesAndRelationships _container;

        public List<Table> Tables { get; private set; }

        GeneratorID _generatorTableID;
        GeneratorID _generatorFieldID;

        CompositeDisposable _receiver;

        public event Action onUpdatedTables = delegate { };

        public TableManager(Validator validator, TableList editorList, ContainerEntitiesAndRelationships container, FormPlaceholder placeholder)
        {
            Tables = new List<Table>();

            _editorList = editorList;
            _container = container;

            _generatorTableID = new GeneratorID();
            _generatorFieldID = new GeneratorID();
        }

        public void Dispose()
        {
            _receiver?.Dispose();
            onUpdatedTables = null;
        }

        public void Initialize()
        {
            _receiver = new CompositeDisposable();

            MessageBroker.Default.Receive<MessageClickedToolbarButton<TableManagementButton>>()
                .Subscribe(async msg =>
                {
                    var button = msg.Button;

                    button.SwitchOn();

                    await EditTables();

                    button.SwitchOff();
                }).AddTo(_receiver);
        }

        public void CreateTable()
        {
            Tables = new List<Table>();
            var entities = _container.GetEntities();
            foreach (var entity in entities)
                Tables.Add(new Table(_generatorTableID.Create(), entity));
        }

        private async UniTask EditTables()
        {
            var tableDatas = Tables.ConvertAll(table => table.GetData());
            var newTableDatas = await _editorList.Open(tableDatas, _generatorTableID, _generatorFieldID);

            if (tableDatas != newTableDatas)
            {
                Tables = new List<Table>();
                foreach (var tableData in newTableDatas) Tables.Add(new Table(tableData));

                onUpdatedTables();
            }
        }
    }
}