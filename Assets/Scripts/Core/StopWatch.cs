using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using CITG.UI.MainScene;

namespace CITG.StopWatchSystem
{
    public class StopWatch : IDisposable
    {
        IDisposable _watcher;
        ViewStopWatch _view;

        public TimeSpan CurrentTime { get; private set; }

        public StopWatch(ViewStopWatch view) => _view = view;

        public void Dispose() => _watcher?.Dispose();

        public void Start()
        {
            CurrentTime = new TimeSpan();
            _watcher =  Observable.FromMicroCoroutine(_ => Update())
                .Subscribe();
        }

        public void Start(TimeSpan time)
        {
            CurrentTime = time;
            _watcher = Observable.FromMicroCoroutine(_ => Update(time.TotalSeconds))
                .Subscribe();
        }

        public TimeSpan Stop()
        {
            if (_watcher == null) throw new Exception();
            _watcher.Dispose();
            _watcher = null;

            return CurrentTime;
        }

        IEnumerator Update(double currentTime = 0f)
        {
            while (true)
            {
                currentTime = currentTime + Time.deltaTime;
                CurrentTime = TimeSpan.FromSeconds(currentTime);

                _view.UpdateTime(CurrentTime);

                yield return null;
            }
        }
    }
}