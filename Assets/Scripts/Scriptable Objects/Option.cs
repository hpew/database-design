using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEditor;
using Sirenix.OdinInspector;
using CITG.TableSystem;
using CITG.VisualSQLQueryConstructionSystem;
using CITG.xNodeGraphes.SQLQueryGraphSystem;
using CITG.xNodeGraphes.ERModel;
using CITG.Utility;

namespace CITG.SystemOptions
{
    [CreateAssetMenu(fileName = "Option", menuName = "System Options/Option", order = 51)]
    public class Option : ScriptableObject
    {
        [SerializeField] int number;
        [ShowInInspector] static int countVisualSQLQuery = 3; //��������� �������


        [SerializeField, HideInInspector] DictionaryTaskDescription _taskDescriptions;

        [SerializeField] ERModelVerificationData erModelVerificationData;
        [SerializeField] TableSystemVerificationData.TableVerification[] tableVerifications;
        [SerializeField] VisualSQLQueryData[] visualSQLQueryDatas;
        [SerializeField] SQLCodeData[] _SQLCodeDatas;

        public int Number => number;

        public Context CreateContext(bool isDebug) => new Context()
        {
            Number = number,
            TaskDescriptions = _taskDescriptions.ToDictionary(pair => pair.Key, pair => pair.Value),
            ERModelVerificationData = erModelVerificationData,
            TableSystemVerificationData = new TableSystemVerificationData(tableVerifications),
            VisualSQLQueryDatas = FilterSQLQuery(visualSQLQueryDatas, countVisualSQLQuery, isDebug),
            SQLCodeDatas = _SQLCodeDatas,
        };

        private List<VisualSQLQueryData> FilterSQLQuery(VisualSQLQueryData[] queries, int countVisualSQLQuery, bool isDebug = false)
        {
            if (isDebug) return queries.ToList();
            else
            {
                var filteredQueries = new List<VisualSQLQueryData>();

                var indexes = new List<int>();
                for (var i = 0; i < queries.Length; i++) indexes.Add(i);

                for (int i = 0; i < countVisualSQLQuery; i++)
                {
                    var index = indexes[Random.Range(0, indexes.Count)];
                    indexes.Remove(index);

                    filteredQueries.Add(queries[index]);
                }

                return filteredQueries;
            }
        }

        public struct Context
        {
            public int Number;
            public IReadOnlyDictionary<TaskType, string> TaskDescriptions;
            public ERModelVerificationData ERModelVerificationData;
            public TableSystemVerificationData TableSystemVerificationData;
            public List<VisualSQLQueryData> VisualSQLQueryDatas;
            public SQLCodeData[] SQLCodeDatas;

        }
    }

    [Serializable]
    public struct ERModelVerificationData
    {
        public string[] VerificationEntities;
        public int CountRelationships;
        public ERModelNodeGraph VerificationGraph;
    }

    [Serializable]
    public struct TableSystemVerificationData
    {
        public TableVerification[] TableVerifications;

        public TableSystemVerificationData(TableVerification[] tableVerifications) => TableVerifications = tableVerifications;

        [Serializable]
        public struct TableVerification
        {
            public string Name;
            public FieldVerification[] FieldVerifications;
        }

        [Serializable]
        public struct FieldVerification
        {
            public string Name;
            public Field.TypeField TypeField;
            public Field.TypeKey TypeKey;

            [CustomValueDrawer("DrawerLink")]
            public string LinkTable;
            [CustomValueDrawer("DrawerLink")]
            public string LinkField;


#if UNITY_EDITOR
            private string DrawerLink(string value, GUIContent label)
            {
                if (TypeKey == Field.TypeKey.Secondary)
                {
                    return EditorGUILayout.TextField(label, value);
                }

                return value;
            }
#endif
        }
    }

    [Serializable]
    public struct VisualSQLQueryData
    {
        public VisualSQLQuery SQLQuery;
        public SQLQueryGraph Graph;
        [TextArea(minLines: 5, maxLines: 10)] public string Description;
    }

    [Serializable]
    public struct SQLCodeData
    {
        public string Tittle;
        [TextArea(minLines: 10, maxLines: 20)]  public string FinalCode;
        [TextArea(minLines: 10, maxLines: 20)] public string StartCode;
    }

    [Serializable]
    public struct TaskDescription
    {
        [TextArea(minLines: 10, maxLines: 20)] public string TaskText;
        [TextArea(minLines: 10, maxLines: 20)] public string HelpText;
    }

    [Serializable]
    public class DictionaryTaskDescription : UnitySerializedDictionary<TaskType, string> { }
}