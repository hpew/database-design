using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace CITG.SystemOptions
{
    [CreateAssetMenu(fileName = "Container Options", menuName = "System Options/Container Options", order = 51)]
    public class ContainerOptions : ScriptableObject
    {
        [SerializeField] Option[] _options;

        public Option GetOption(int number)
        {
            var option = Array.Find(_options, option => option.Number == number);
            if (option == null) return _options[0];

            return option;
        }

        public Option GetRandomOption() => _options[Random.Range(0, _options.Length)];
    }
}