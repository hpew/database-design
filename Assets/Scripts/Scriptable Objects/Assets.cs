using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using CITG.UI;
using CITG.UI.MainScene;
using CITG.UI.MainScene.UICollections;
using CITG.Utility;
using CITG.SystemOptions;
using CITG.ERDiagram;
using CITG.TableDiagram;

namespace CITG.System
{
    [CreateAssetMenu(fileName = "Assets", menuName = "System/Assets", order = 51)]
    public class Assets : ScriptableObject
    {
        #region Menu

        [FoldoutGroup("Menu")]
        [SerializeField] InputFieldAuthorizationFormSprites inputFieldAuthorizationFormSprites;
        public InputFieldAuthorizationFormSprites InputFieldAuthorizationFormSprites => inputFieldAuthorizationFormSprites;

        #endregion

        #region Main Scene

        #region Settings 

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Settings")]
        [SerializeField] SettingsMainScene _settingsMainScene;
        public SettingsMainScene SettingsMainScene => _settingsMainScene;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Settings")]
        [SerializeField] DictionaryHelpDescription _helpDescription;

        public DictionaryHelpDescription HelpDescriptions => _helpDescription;

        #endregion

        #region UI

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Toolbar")]
        [SerializeField] SpriteTableSwitchableToolbarButton spriteTableSwitchableToolbarButton;
        public SpriteTableSwitchableToolbarButton SpriteTableSwitchableToolbarButton => spriteTableSwitchableToolbarButton;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Task Selection Tab")]
        [SerializeField] TaskSelectionTabSprites taskSelectionTabSprites;
        public TaskSelectionTabSprites TaskSelectionTabSprites => taskSelectionTabSprites;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Switch Buttons")]
        [SerializeField] SpritesSwitchButton spritesSwitchButton;
        public SpritesSwitchButton SpritesSwitchButton => spritesSwitchButton;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Prefabs")]
        [SerializeField] GameObject _switchButton;
        public GameObject SwitchButton => _switchButton;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Prefabs")]
        [SerializeField] LockRect _lockRect;
        public LockRect LockRect => _lockRect;
        #endregion

        #region ER Model

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/ER Model")]
        [SerializeField] TableERFragmentPrefabs _tableERFragmentPrefabs;
        public TableERFragmentPrefabs TableERFragmentPrefabs => _tableERFragmentPrefabs;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/ER Model")]
        [SerializeField] TableERTransitionPrefabs _tableERTransitionPrefabs;
        public TableERTransitionPrefabs TableERTransitionPrefabs => _tableERTransitionPrefabs;

        #endregion

        #region Table Diagram 

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Table Diagram")]
        [SerializeField] Fragment _fragmentTableDiagram;
        public Fragment FragmentTableDiagram => _fragmentTableDiagram;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Table Diagram")]
        [SerializeField] LineFragment _lineFragmentTableDiagram;
        public LineFragment LineFragmentTableDiagram => _lineFragmentTableDiagram;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Table Diagram")]
        [SerializeField] LineRenderer _connectionTableFragmentDiagram;
        public LineRenderer ConnectionTableFragmentDiagram => _connectionTableFragmentDiagram;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/Table Diagram")]
        [SerializeField] Transform _chainTableFragmentDiagram;
        public Transform ChainTableFragmentDiagram => _chainTableFragmentDiagram;

        #endregion

        #region UI Collection Line


        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/UICollections")]
        [SerializeField] EntityFormationLine _entityFormationLinePrefab;
        public EntityFormationLine EntityFormationLinePrefab => _entityFormationLinePrefab;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/UICollections")]
        [SerializeField] RelationshipsFormationLine _relationshipsFormationLinePrefab;
        public RelationshipsFormationLine RelationshipsFormationLinePrefab => _relationshipsFormationLinePrefab;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/UICollections")]
        [SerializeField] TableListLine _tableEditorListLine;
        public TableListLine TableEditorListLine => _tableEditorListLine;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/UICollections")]
        [SerializeField] TableEditorLine _tableEditorLine;
        public TableEditorLine TableEditorLine => _tableEditorLine;

        [FoldoutGroup("Main Scene")]
        [FoldoutGroup("Main Scene/UICollections")]
        [SerializeField] SelectableLine _selectableLine;
        public SelectableLine SelectableLine => _selectableLine;

        #endregion

        #endregion
    }

    [Serializable]
    public class SettingsMainScene
    {
        [SerializeField] TaskType firstActiveTask = TaskType.First;
        public TaskType ActiveTask => firstActiveTask;

        [SerializeField] bool onDebug = true;
        public bool OnDebug { get => onDebug; }

        [SerializeField] int defaultNumberOption = 1;
        public int DefaultNumberOption => defaultNumberOption;
    }


    [Serializable]
    public class SpriteTableSwitchableToolbarButton
    {
        [SerializeField] List<KeyValue> _list;
        public SpritesSwitchableToolbarButton this[Type type] => _list.Find(element => element.TypeName == type.Name).Sprites;

        [Serializable]
        public struct KeyValue
        {
            public string TypeName;
            public SpritesSwitchableToolbarButton Sprites;
        }
    }

    [Serializable] //����� ����� ����� �� SerializableDictionary � ������� ����� Readonly
    public class TableERFragmentPrefabs
    {
        [SerializeField] List<KeyValue> _list;
        public ERFragment this[Type type] => _list.Find(element => element.TypeName == type.Name).Prefab;

        [Serializable]
        public struct KeyValue
        {
            public string TypeName;
            public ERFragment Prefab;
        }
    }

    [Serializable]
    public class TableERTransitionPrefabs
    {
        [SerializeField] List<KeyValue> _list;
        public ERTransition this[Type type] => _list.Find(element => element.TypeName == type.Name).Prefab;

        [Serializable]
        public struct KeyValue
        {
            public string TypeName;
            public ERTransition Prefab;
        }
    }

    [Serializable]
    public class DictionaryHelpDescription
    {
        [TextArea(20, 20), SerializeField] string _descriptionFirstTask;
        [TextArea(20, 20), SerializeField] string _descriptionSecondTask;
        [TextArea(20, 20), SerializeField] string _descriptionThirdTask;
        [TextArea(20, 20), SerializeField] string _descriptionFourTask;

        public string this[TaskType displaying]
        {
            get
            {
                return displaying switch
                {
                    TaskType.First => _descriptionFirstTask,
                    TaskType.Second => _descriptionSecondTask,
                    TaskType.Third => _descriptionThirdTask,
                    TaskType.Fourth => _descriptionFourTask,
                    _ => throw new Exception()
                };
            }
        }

    }


    [Serializable]
    public struct InputFieldAuthorizationFormSprites
    {
        public Sprite SpriteSimple;
        public Sprite SpriteBorder;
    }
    [Serializable]
    public struct SpritesSwitchableToolbarButton
    {
        public Sprite OnSprite;
        public Sprite OffSprite;
    }
    [Serializable]
    public struct SpritesSwitchButton
    {
        public Sprite ActiveSprite;
        public Sprite InactiveSprite;
    }
    [Serializable]
    public struct TaskSelectionTabSprites
    {
        public CoupleSprites PairAngle;
        public CoupleSprites PairWithoutAngle;

        [Serializable]
        public struct CoupleSprites
        {
            public Sprite Active;
            public Sprite Inactive;
        }
    }
}