using System;
using Cysharp.Threading.Tasks;
using UnityEngine;
using Zenject;
using UniRx;
using CITG.UI;
using CITG.UI.Menu;
using CITG.Utility;
using CITG.DataSystem;
using CITG.SessionSystem;

namespace CITG.Menu
{
    public class Menu : IDisposable
    {
        Glass _glass;
        PreviewWindow _previewWindow;

        TaskForm _taskForm;
        AuthorizationForm _authorizationForm;
        OptionNumberForm _optionNumberForm;

        SessionRecoveryForm _sessionRecoveryForm;

        IDisposable _receiver;

        public Menu(PreviewWindow previewWindow, Glass glass, 
            TaskForm taskForm, AuthorizationForm authorizationForm, OptionNumberForm optionNumberForm,
            SessionRecoveryForm sessionRecoveryForm)
        {
            _previewWindow = previewWindow;
            _glass = glass;

            _taskForm = taskForm;
            _authorizationForm = authorizationForm;
            _optionNumberForm = optionNumberForm;

            _sessionRecoveryForm = sessionRecoveryForm;
        }

        public void Dispose() => _receiver?.Dispose();

        public async UniTask<bool> DisplaySessionRecovery(UserData data)
        {
            _previewWindow.SetActive(false);
            _glass.Show();

            var result = await _sessionRecoveryForm.Show(data);

            _glass.Unshow();
            return result;
        }

        public void Display()
        {
            _previewWindow.SetActive(true);

            _receiver = MessageBroker.Default.Receive<MessageClickedMenuButton<TaskDescriptionButton>>()
                .Subscribe(_ => ShowTask());
        }

        public async UniTask<(bool, UserData)> OpenRegistration(int numberOption)
        {
            await OpenGlass();

            var result = await _authorizationForm.Show();
            
            if (result.IsCancel)
            {
                await CloseGlass();
                return (true, new UserData());
            }

            await _optionNumberForm.Show(numberOption);

            return (false, new UserData(result.FIO, result.Group, result.CreditBook, numberOption));
        }

        private async void ShowTask()
        {
            await OpenGlass();

            await _taskForm.Show();

            await CloseGlass();
        }

        private async UniTask OpenGlass()
        {
            _previewWindow.SetActive(false);
            await _glass.Open();
        }

        private async UniTask CloseGlass()
        {
            await _glass.Close();
            _previewWindow.SetActive(true);
        }
    }
}