using System;
using System.Collections.Generic;
using System.Text;
using CITG.SystemOptions;

namespace CITG.DataSystem
{
    [Serializable]
    public struct Report
    {
        private Dictionary<TaskType, TaskReport> _taskReports;

        public readonly UserData UserData;

        public Report(UserData userData)
        {
            UserData = userData;

            TotalScore = 0;
            Time = default;

            _taskReports = new Dictionary<TaskType, TaskReport>()
            {
                [TaskType.First] = new TaskReport(),
                [TaskType.Second] = new TaskReport(),
                [TaskType.Third] = new TaskReport(),
                [TaskType.Fourth] = new TaskReport()
            };
        }

        public int TotalScore { get; private set; }
        public TimeSpan Time { get; set; }

        public void UpdateTaskReport(TaskType type, TaskReport report)
        {
            _taskReports[type] = report;

            TotalScore = 0;
            foreach (var r in _taskReports.Values) TotalScore += r.Score;
        }

        public TaskReport GetTaskReport(TaskType type) => _taskReports[type];


        public static Report DebugReport => new Report(UserData.DebugData);

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            foreach (var element in _taskReports)
                builder.Append($"{element.Key}: {element.Value}\n");

            return $"User Data:\n {UserData}\n" +
                $"Score: {TotalScore}\n" +
                $"Time Span: {Time}\n" +
                $"TaskReports: {builder}";
        }
    } 

    [Serializable]
    public struct TaskReport
    {
        public int Score;
        public string Description;

        public override string ToString() => $"Score: {Score}, Description: {Description}";
    }
}