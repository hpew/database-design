using System;

namespace CITG.DataSystem
{
    [Serializable]
    public struct UserData
    {
        public UserData(string FIO, string group, string creditBook, int numberOption)
        {
            this.FIO = FIO;
            Group = group;
            CreditBook = creditBook;
            NumberOption = numberOption;
        }

        public string FIO { get; private set; }
        public string Group { get; private set; }
        public string CreditBook { get; private set; }
        public int NumberOption { get; private set; }

        public static UserData DebugData => new UserData("������ ���� ��������", "���-032", "071200", 1);

        public override string ToString()
        {
            return $"FIO: {FIO}\n" +
                $"Group: {Group}\n" +
                $"Credit Book: {CreditBook}\n" +
                $"Number Option: {NumberOption}";
        }
    }
}
