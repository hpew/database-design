using System;
using System.Runtime.InteropServices;
using System.IO;
using System.Text;
using SFB;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using CITG.UI;
using CITG.UI.ResultScene;
using CITG.DataSystem;
using CITG.Utility;
using CITG.SessionSystem;

namespace CITG.ResultManagment
{
    public class ResultManager : IInitializable
    {
        SessionProvider _provider;


        PreviewWindow _previewW;
        ReportWindow _reportW;

        Glass _glass;

        Button _reportButton;
        Button _saveButton;
        Button _exitButton;

        int rating = 2;

        Report report;
        
        public ResultManager(PreviewWindow previewW, ReportWindow reportW, Glass glass,
            [Inject(Id = "Report Button")] Button reportButton,
            [Inject(Id = "Save Button")] Button saveButton,
            [Inject(Id = "Exit Button")] Button exitButton,
            SessionProvider provider = null)
        {
            _provider = provider;

            _previewW = previewW;
            _reportW = reportW;

            _reportButton = reportButton;
            _saveButton = saveButton;
            _exitButton = exitButton;

            _glass = glass;

            _reportButton.onClick.AddListener(ShowReport);
            _exitButton.onClick.AddListener(QuitApplication);

            _saveButton.transform.parent.SetActive(false);

#if UNITY_WEBGL && !UNITY_EDITOR
            _exitButton.transform.parent.SetActive(false);
            _saveButton.transform.parent.SetActive(true);
            _saveButton.onClick.AddListener(SaveResult);
#endif
        }

        public void Initialize()
        {
            if (_provider == null)
            {
                Debug.Log("Session Provider is NULL");
                _provider = new SessionProvider();
                _provider.CreateDebugSession();
            }

            report = _provider.Context.GetReport();
            _provider.CloseSession();

            var score = report.TotalScore;

            if (score < 20) rating = 2;
            else if (score >= 20 && score <= 45) rating = 3;
            else if (score >= 46 && score <= 90) rating = 4;
            else rating = 5;

            _previewW.DisplayResult(report.UserData.CreditBook, score, report.Time, rating, report.UserData.NumberOption);
            _reportW.DisplayReport(report);
        }

        private async void ShowReport()
        {
            _previewW.SetActive(false);
            await _glass.Open();

            await _reportW.Show();

            await _glass.Close();
            _previewW.SetActive(true);
        }

#if UNITY_WEBGL && !UNITY_EDITOR
        //[DllImport("__Internal")]
        //private static extern void DownloadFile(string gameObjectName, string methodName, string filename, byte[] byteArray, int byteArraySize);

        [DllImport("__Internal")]
        private static extern void downloadToFile(string content, string fileName);

        private void SaveResult()
        {
            var userData = report.UserData;

            var name = $"{userData.FIO} " +
                    $"{userData.Group} " +
                    $"{DateTime.Now.ToString("dd/MM/yyyy")} " +
                    $"{DateTime.Now.ToString("HH_mm")}";

            var description = $"�������: {userData.FIO}\n" +
                $"������: {userData.Group}\n" +
                $"�������� ������: {userData.CreditBook}\n" +
                $"���������� ������: {report.TotalScore}\n" +
                $"������: {rating}";

            var bytes = Encoding.UTF8.GetBytes(description);
            //DownloadFile("", "SaveResult", "Report.txt", bytes, bytes.Length);

            downloadToFile(description, "Report.txt");

        #region old
            //try
            //{
            //    var path = StandaloneFileBrowser.SaveFilePanel("����������",
            //        "",
            //        $"{userData.FIO} " +
            //        $"{userData.Group} " +
            //        $"{DateTime.Now.ToString("dd/MM/yyyy")} " +
            //        $"{DateTime.Now.ToString("HH_mm")}", "txt");

            //    if (path.Length != 0)
            //    {
            //        using (var outputFile = new StreamWriter(path, false))
            //        {
            //            outputFile.WriteLine($"�������: {userData.FIO}");
            //            outputFile.WriteLine($"������: {userData.Group}");
            //            outputFile.WriteLine($"�������� ������: {userData.CreditBook}");
            //            outputFile.WriteLine($"���������� ������: {report.TotalScore}");
            //            outputFile.WriteLine($"������: {rating}");
            //        }
            //    }
            //}
            //catch (Exception)
            //{

            //}
        #endregion
        }

#endif

        private void QuitApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
                            Application.Quit();
#endif
        }
    }

}