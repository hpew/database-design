using System;
using System.Collections.Generic;
using System.IO;
using SFB;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using CITG.UI;
using CITG.UI.ResultScene;
using CITG.DataSystem;
using CITG.Utility;

namespace CITG.ResultManagment
{
    public class ButtonManager
    {
        [Inject(Id = "Report Button")] Button _reportButton;
        [Inject(Id = "Save Button")] Button _saveButton;
        [Inject(Id = "Exit Button")] Button _exitButton;

        ReportWindow _reportW;
        PreviewWindow _previewW;
        Glass _glass;

        Report report;

        public ButtonManager(
            [Inject(Id = "Report Button")] Button reportButton,
            [Inject(Id = "Save Button")] Button saveButton,
            [Inject(Id = "Exit Button")] Button exitButton,
            ReportWindow reportWindow,
            PreviewWindow resultWindow,
            Glass glass,
            Report report = default)
        {
            _reportButton = reportButton;
            _saveButton = saveButton;
            _exitButton = exitButton;

            _reportW = reportWindow;
            _previewW = resultWindow;
            _glass = glass;

            _reportButton.onClick.AddListener(ShowReport);
            _saveButton.onClick.AddListener(SaveResult);
        }

        private async void ShowReport()
        {
            _previewW.SetActive(false);
            await _glass.Open();

            await _reportW.Show();

            await _glass.Close();
            _previewW.SetActive(true);
        }

        private void SaveResult()
        {
            var userData = report.UserData;

            try
            {
                var path = StandaloneFileBrowser.SaveFilePanel("����������",
                    "",
                    $"{userData.FIO} " +
                    $"{userData.Group} " +
                    $"{DateTime.Now.ToString("dd/MM/yyyy")} " +
                    $"{DateTime.Now.ToString("HH_mm")}", "txt");

                if (path.Length != 0)
                {
                    using (var outputFile = new StreamWriter(path, false))
                    {
                        outputFile.WriteLine($"�������: {userData.FIO}");
                        outputFile.WriteLine($"������: {userData.Group}");
                        outputFile.WriteLine($"�������� ������: {userData.CreditBook}");
                        outputFile.WriteLine($"���������� ������: {report.TotalScore}");
                      //  outputFile.WriteLine($"������: {rating}");
                    }
                }
            }
            catch (Exception)
            {

            }
        }
    }
}