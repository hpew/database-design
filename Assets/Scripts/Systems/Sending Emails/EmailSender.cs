using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using UnityEngine;


namespace CITG.SendningEmails
{
    public class EmailSender : MonoBehaviour
    {
        public void SendMessage()
        {
            var mail = new MailMessage();
            var server = new SmtpClient("smtp.gmail.com");

            server.Timeout = 10000;
            server.DeliveryMethod = SmtpDeliveryMethod.Network;
            server.UseDefaultCredentials = false;
            server.Port = 587;

            mail.From = new MailAddress("*******");
            mail.To.Add(new MailAddress("*******"));

            mail.Subject = "�������� ���������";
            mail.Body = "��� �������� ���������.";

            server.Credentials = new NetworkCredential("*******", "********");

            ServicePointManager.ServerCertificateValidationCallback =
                delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };

            mail.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
            server.Send(mail);
        }
    }
}
