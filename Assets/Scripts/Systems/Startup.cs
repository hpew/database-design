using System;
using UnityEngine;
using Zenject;
using UniRx;
using Cysharp.Threading.Tasks;
using CITG.SessionSystem;
using CITG.SystemOptions;
using CITG.DataSystem;
using CITG.UI.Menu;

namespace CITG.StartupSystem
{
    public class Startup : IInitializable
    {
        ZenjectSceneLoader _loader;
        Menu.Menu _menu;
        ContainerOptions _container;

        CompositeDisposable _receiver;

        public Startup(ZenjectSceneLoader loader, Menu.Menu menu, ContainerOptions container)
        {
            _loader = loader;
            _menu = menu;
            _container = container;
        }

        public async void Initialize()
        {
            var provider = new SessionProvider();
            var interuptedSession = provider.LoadInterruptedSession();

            if (interuptedSession != null)
            {
                Debug.Log(interuptedSession);

                var result = await _menu.DisplaySessionRecovery(interuptedSession.Report.UserData);
                
                if (result)
                {
                    RestoreSession(provider, interuptedSession);
                    return;
                }
            }

            _menu.Display();

            _receiver = new CompositeDisposable();

            MessageBroker.Default.Receive<MessageClickedMenuButton<StartTestButton>>()
                .Subscribe(_ => StartTest(provider)).AddTo(_receiver);

            MessageBroker.Default.Receive<MessageClickedMenuButton<ExitButton>>()
                .Subscribe(_ => Exit()).AddTo(_receiver);

        }

        private void RestoreSession(SessionProvider provider, Session session)
        {
            provider.RestoreInterruptedSession(session);
            LoadScene(provider);
        }

        private async void StartTest(SessionProvider provider)
        {
            var numberOption = _container.GetRandomOption().Number;

            (bool isCancel, UserData data) result = await _menu.OpenRegistration(numberOption);

            if (result.isCancel)
            {
                _menu.Display();
                return;
            }

            provider.CreateNewSession(new Report(result.data));


            LoadScene(provider);
        }

        private void Exit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
                                Application.Quit();
#endif
        }

        private void LoadScene(SessionProvider provider)
        {
            var operation = _loader.LoadSceneAsync(1, UnityEngine.SceneManagement.LoadSceneMode.Single, container =>
            {
                container.BindInstance(provider);
            });
        }         
    }
}