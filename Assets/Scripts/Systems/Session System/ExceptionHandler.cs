using System;

namespace CITG.SessionSystem
{
    static class ExceptionHandler
    {
        public static void ThrowIfSessionNotOpen(Session session)
        {
            if (session == null) throw new ExceptionNonOpenSession();
        }

        public static void ThrowIfSessionOpen(Session session)
        {
            if (session != null) throw new ExceptionExistingSession();
        }
    }

    public class ExceptionNonOpenSession : Exception
    {
        public ExceptionNonOpenSession()
            : base("There is no open session at the moment.") { }
    }

    public class ExceptionExistingSession : Exception
    {
        public ExceptionExistingSession()
        : base("It is not possible to create or find an interrupted session because an open session already exists." +
              "\nClose it before starting a new session.") {}
    }
}