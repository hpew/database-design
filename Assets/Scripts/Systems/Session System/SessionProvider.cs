using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using Cysharp.Threading.Tasks;
using UnityEngine;
using CITG.DataSystem;

namespace CITG.SessionSystem
{
    public class SessionProvider
    {
        private static int storageTime = 2; //�����
        private static string dataPath = $@"{Application.persistentDataPath}\CurrentSession.dat";

        private Session _session;
        private Context _context;

        public Context Context
        {
            get
            {
                ExceptionHandler.ThrowIfSessionNotOpen(_session);
                return _context;
            }
        }

        public Session LoadInterruptedSession()
        {
            ExceptionHandler.ThrowIfSessionOpen(_session);

            try
            {
                if (!File.Exists(dataPath)) return null;

                using (var stream = File.Open(dataPath, FileMode.Open))
                {
                    var formatter = new BinaryFormatter();
                    var session = (Session)formatter.Deserialize(stream);

                    var hours = (DateTime.UtcNow - session.DateOpen).TotalHours;
                    if (hours > storageTime) return null;
                    return session;
                }
            }
            catch (Exception e)
            {
                PlatformSafeMessage("Failed to Load: " + e.Message);
                return null;
            }
        }

        public void RestoreInterruptedSession(Session session)
        {
            ExceptionHandler.ThrowIfSessionOpen(_session);
            _session = new Session(DateTime.UtcNow, session);

            _context = new Context(_session, SaveSession);

            SaveSession(_session);
        }

        public void CreateNewSession(Report report)
        {
            ExceptionHandler.ThrowIfSessionOpen(_session);
            _session = new Session(DateTime.UtcNow, report);

            _context = new Context(_session, SaveSession);

            SaveSession(_session);
        }

        public void CreateDebugSession()
        {
            ExceptionHandler.ThrowIfSessionOpen(_session);
            _session = new Session(DateTime.UtcNow, Report.DebugReport);
            _context = new Context(_session);
        }

        public void CloseSession()
        {
            ExceptionHandler.ThrowIfSessionNotOpen(_session);

            if (File.Exists(dataPath)) { Debug.Log("DELETE"); File.Delete(dataPath); }
            _context = null;
            _session = null;
        }

        private void SaveSession(Session session)
        {
            try
            {
                using (var stream = File.Open(dataPath, FileMode.Create))
                {
                    var formatter = new BinaryFormatter();
                    formatter.Serialize(stream, session);

#if UNITY_WEBGL && !UNITY_EDITOR
                    SyncFiles();
#endif
                }
            }
            catch (Exception e)
            {
                PlatformSafeMessage("Failed to Load: " + e.Message);
            }
        }

        #region Serialize/Deserialize Logic For WebGL
        [DllImport("__Internal")]
        private static extern void SyncFiles();

        [DllImport("__Internal")]
        private static extern void WindowAlert(string message);

        private static void PlatformSafeMessage(string message)
        {
#if UNITY_WEBGL && !UNITY_EDITOR
            WindowAlert(message);
#elif UNITY_EDITOR
            Debug.LogError(message);
#endif
        }
        #endregion
    }
}