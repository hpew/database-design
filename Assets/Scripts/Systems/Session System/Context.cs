using System;
using Cysharp.Threading.Tasks;
using CITG.DataSystem;

namespace CITG.SessionSystem
{

    public class Context
    {
        Action<Session> _callbackSavingTask;
        Session _session;

        public Context(Session session, Action<Session> callbackSavingTask = null)
        {
            _session = session;
            _callbackSavingTask = callbackSavingTask;
        }

        public Report GetReport() => _session.Report;
        public TaskType GetTypeLastCompletedTask() => _session.TypeLastCompletedTask;

        public void UpdateSession(Report report, TaskType lastCompletedTask)
        {
            _session.Report = report;
            _session.TypeLastCompletedTask = lastCompletedTask;

            if (_callbackSavingTask != null) _callbackSavingTask(_session);
        }
    }
}