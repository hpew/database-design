using System;
using CITG.DataSystem;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace CITG.SessionSystem
{
    [Serializable]
    public class Session
    {
        public Session(DateTime dateOpen, Report report, TaskType typeLastCompletedTask = TaskType.None)
        {
            Report = report;
            DateOpen = dateOpen;
            TypeLastCompletedTask = TaskType.None;
        }

        public Session(DateTime dateOpen, Session session)
        {
            DateOpen = dateOpen;
            Report = session.Report;
            TypeLastCompletedTask = session.TypeLastCompletedTask;
        }

        public Report Report { get; set; }
        public DateTime DateOpen { get; set; }
        public TaskType TypeLastCompletedTask { get; set; }

        public override string ToString()
        {
            return "Session: \n" +
                $"Report:\n {Report}\n" +
                $"DateOpen: {DateOpen.ToShortDateString()}\n" +
                $"Type: {TypeLastCompletedTask}";
        }
    }
}
