using System;
using System.Collections.Generic;
using UnityEngine;
using CITG.System;

namespace CITG.TaskManagment
{
    public class TaskContainer
    {
        FirstTask _firstTask;
        SecondTask _secondTask;
        ThirdTask _thirdTask;
        FourthTask _fourTask;

        Dictionary<Task, Task> _sequence;

        public TaskContainer(FirstTask firstTask, SecondTask secondTask, ThirdTask thirdTask, FourthTask fourTask)
        {
            _firstTask = firstTask;
            _secondTask = secondTask;
            _thirdTask = thirdTask;
            _fourTask = fourTask;

            _sequence = new Dictionary<Task, Task>()
            {
                [_firstTask] = _secondTask,
                [_secondTask] = _thirdTask,
                [_thirdTask] = _fourTask
            };
        }

        public Task GetNext(Task task)
        {
            if (task == null) return _firstTask;
            if (_sequence.ContainsKey(task)) return _sequence[task];
            return null;
        }

        public Task GetTask(TaskType type)
            => type switch
            {
                TaskType.None => null,
                TaskType.First => _firstTask,
                TaskType.Second => _secondTask,
                TaskType.Third => _thirdTask,
                TaskType.Fourth => _fourTask,
                _ => throw new Exception(),
            };
    }
}