using System;
using Zenject;
using UniRx;
using CITG.VisualSQLQueryConstructionSystem;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.TableDiagram;
using CITG.DataSystem;

namespace CITG.TaskManagment
{
    public class ThirdTask : Task, IInitializable, IDisposable
    {
        VisualSQLQueryManager _manager;
        Model _model;
        IDisposable _receiver;

        public ThirdTask(VisualSQLQueryManager manager, Model model)
        {
            _manager = manager;
            _model = model;
        }

        public override TaskType Type => TaskType.Third;

        public void Initialize()
        {
            Unactivate();
            _receiver = MessageBroker.Default.Receive<MessageClickedToolbarButton<ChartDisplayButton>>()
                .Subscribe(msg =>
                {
                    if (_model.IsShowing)
                    {
                        msg.Button.SwitchOff();
                        _model.Unshow();
                        _manager.Show();
                    }
                    else
                    {
                        msg.Button.SwitchOn();
                        _model.Show();
                        _manager.Unshow();
                    }
                });
        }

        public void Dispose() => _receiver.Dispose();

        public override void Activate() => _manager.Activate();
        public override void Unactivate() => _manager.Unactivate();

        public override TaskReport Validate()
        {
            var validations = _manager.Validate();

            var score = 0;
            var description = "";

            score += validations[0] ? 6 : 0;
            description += $"������ 1: {score}/6\n";

            for (int i = 1; i < validations.Length; i++)
            {
                var scoreQuery = validations[i] ? 7 : 0;
                score += scoreQuery;

                description += $"������ {i + 1}: {scoreQuery}/7\n";
            }

            return new TaskReport()
            {
                Description = description,
                Score = score,
            };
        }

        public bool DebugValidate()
        {
            var validations = _manager.Validate();

            foreach (var validation in validations)
                if (!validation) return false;

            return true;
        }

        public bool DebugValidateCurrentQuery()
            => _manager.ValidateCurrentQuery();
    }
}
