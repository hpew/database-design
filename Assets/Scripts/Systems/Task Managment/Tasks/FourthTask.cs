using Zenject;
using CITG.SQLCode;
using CITG.DataSystem;

namespace CITG.TaskManagment
{
    public class FourthTask : Task, IInitializable
    {
        SQLCodeManager _manager;

        public FourthTask(SQLCodeManager manager)
        {
            _manager = manager;
        }

        public override TaskType Type => TaskType.Fourth;

        public void Initialize() => Unactivate();

        public override void Activate() => _manager.Activate();
        public override void Unactivate() => _manager.Unactivate();

        public override TaskReport Validate()
        {
            var validations = _manager.Validate();

            var score = 0;
            var description = "";

            score += validations[0] ? 5 : 0;
            description += $"������ 1: {score}/5\n";

            for (int i = 1; i < validations.Length; i++)
            {
                var scoreQuery = validations[i] ? 6 : 0;
                score += scoreQuery;

                description += $"������ {i + 1}: {scoreQuery}/6\n";
            }

            return new TaskReport()
            {
                Description = description,
                Score = score,
            };
        }

        public bool DebugValidate()
        {
            var validations = _manager.Validate();
            foreach (var validation in validations)
                if (!validation) return false;

            return true;
        }

        public bool DebugValidateCurrentCode()
            => _manager.ValidateCurrentCode();
    }
}
