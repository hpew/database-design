using CITG.DataSystem;

namespace CITG.TaskManagment
{
    public abstract class Task
    {
        public abstract TaskType Type { get; }
        public abstract void Activate();
        public abstract void Unactivate();
        public abstract TaskReport Validate();
    }
}