using CITG.DataSystem;
using CITG.ERDiagram;

namespace CITG.TaskManagment
{
    public class FirstTask : Task
    {
        ERModel _ERModel;

        const int targetScore = 10;

        public FirstTask(ERModel ERModel)
        {
            _ERModel = ERModel;
        }

        public override TaskType Type => TaskType.First;

        public override void Activate() => _ERModel.Show();

        public override void Unactivate() => _ERModel.Unshow();

        public override TaskReport Validate()
        {
            var status = _ERModel.Validate();

            var scoreEntity = 0;
            var scoreRelationShip = 0;

            switch (status)
            {
                case Validator.Status.None:
                    scoreEntity = 0;
                    scoreRelationShip = 0;
                    break;
                case Validator.Status.CorrectFragments:
                    scoreEntity = targetScore;
                    scoreRelationShip = 0;
                    break;
                case Validator.Status.CorrectModel:
                    scoreEntity = targetScore;
                    scoreRelationShip = targetScore;
                    break;
            }


            var description = $"��������: {scoreEntity}/10\n" +
                $"����� ���������: {scoreRelationShip}/10";


            var report = new TaskReport()
            {
                Description = description,
                Score = scoreEntity + scoreRelationShip,
            };

            return report;
        }

        public Validator.Status DebugValidate() => _ERModel.Validate();
    }
}