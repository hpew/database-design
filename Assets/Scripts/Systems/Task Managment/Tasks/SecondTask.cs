using UnityEngine;
using CITG.DataSystem;
using CITG.TableSystem;
using CITG.TableDiagram;

namespace CITG.TaskManagment
{
    public class SecondTask : Task
    {
        TableManager _tableManager;
        Validator _validator;
        Model _model;

        SecondTask(TableManager tableManager, Validator validator, Model model)
        {
            _tableManager = tableManager;
            _validator = validator;

            _model = model;
        }

        public override TaskType Type => TaskType.Second;

        public override void Activate()
        {
            _tableManager.CreateTable();
            _model.Activate();
        }

        public override void Unactivate()
        {
            _model.Unactivate();
        }

        public override TaskReport Validate()
        {
            var statistics = _validator.Validate(_tableManager.Tables);

            var description = "";
            var score = 0;
            foreach (var element in statistics.Tables)
            {
                var name = element.Key.Name;
                var scoreTable = element.Value ? 5 : 0;
                score += scoreTable;

                description += $"������� �{name}�: {scoreTable}/5\n";
            }

            if (statistics.IsExtraTable)
            {
                description += $"������ �������: -5";

                score -= 5;
                if (score < 0) score = 0;
            }

            return new TaskReport()
            {
                Description = description,
                Score = score,
            };
        }

        public (bool, string) DebugValidate()
        {
            var statistics = _validator.Validate(_tableManager.Tables);

            var description = "";
            var score = 0;

            foreach (var element in statistics.Tables)
            {
                var name = element.Key.Name;
                var scoreTable = element.Value ? 5 : 0;
                score += scoreTable;

                description += $"������� �{name}�: {scoreTable}/5\n";
            }

            if (statistics.IsExtraTable)
            {
                description += $"������ �������: -5";

                score -= 5;
                if (score < 0) score = 0;
            }

            return (score == 25, description);
        }
    }
}
