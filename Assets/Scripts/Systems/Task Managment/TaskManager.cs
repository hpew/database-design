using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using Cysharp.Threading.Tasks;
using CITG.UI.MainScene;
using CITG.SystemOptions;
using CITG.StopWatchSystem;
using CITG.SessionSystem;

namespace CITG.TaskManagment
{
    public class TaskManager : IInitializable, IDisposable
    {
        protected ZenjectSceneLoader _loader;
        protected SessionProvider _provider;
        protected Option.Context _context;
        protected TaskContainer _container;
        protected Task _currentTask;

        protected StopWatch _stopWatch;

        private InterfaceUpdater _updater;
        private CompositeDisposable _receiver;

        public TaskManager(ZenjectSceneLoader loader, Option.Context context, TaskContainer container,
            InterfaceUpdater updater, StopWatch stopWatch, SessionProvider provider = null)
        {
            _loader = loader;

            _provider = provider;

            _context = context;

            _container = container;

            _updater = updater;
            _stopWatch = stopWatch;
        }

        public virtual void Initialize()
        {
            SubscribeToMessage();
            PrepareTesting();
        }

        public virtual void Dispose()
        {
            _receiver?.Dispose();
        }

        protected virtual void SubscribeToMessage()
        {
            _receiver = new CompositeDisposable();

            MessageBroker.Default.Receive<TaskCompletionMessage>()
                .Subscribe(_ => CompleteCurrentTask()).AddTo(_receiver);

            MessageBroker.Default.Receive<ExitTestMessage>()
                .Subscribe(_ => QuitApplication()).AddTo(_receiver);
        }

        protected virtual void PrepareTesting()
        {
            var report = _provider.Context.GetReport();
            var type =  _provider.Context.GetTypeLastCompletedTask();

            var task = _container.GetNext(_container.GetTask(type));

            if (task == null) CompleteTesting(true);
            else
            {
                _stopWatch.Start(report.Time);
                ActivateTask(task);
            }
        }

        protected  virtual void ActivateTask(Task task)
        {
            _currentTask?.Unactivate();
            _currentTask = task; Debug.Log("Activate Next Task -> " + _currentTask.GetType());
            _currentTask.Activate();


            _updater.Update(task.Type);
        }
       
        protected void CompleteCurrentTask()
        {
            var tReport = _currentTask.Validate();
            var type = _currentTask.Type;
            var report = _provider.Context.GetReport();

            report.UpdateTaskReport(type, tReport);
            report.Time = _stopWatch.CurrentTime;

            _provider.Context.UpdateSession(report, type);

            var nextTask = _container.GetNext(_currentTask);

            if (nextTask == null) CompleteTesting();
            else ActivateTask(nextTask);
        }

        private void CompleteTesting(bool isRedirection = false)
        {
            var report = _provider.Context.GetReport();
            var type = _currentTask.Type;

            if (!isRedirection) report.Time = _stopWatch.Stop();

            _provider.Context.UpdateSession(report, type);

            _loader.LoadScene(2, UnityEngine.SceneManagement.LoadSceneMode.Single, container =>
            {
                container.BindInstance(_provider);
            });
        }

        private void QuitApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
                    Application.Quit();
#endif
        }
    }
}