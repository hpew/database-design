using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;
using UniRx;
using CITG.System;
using CITG.UI;
using CITG.UI.MainScene;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.UI.MainScene.TaskSelectionTabbarSystem;
using CITG.UI.MainScene.TaskHelpForm;
using CITG.DataSystem;
using CITG.SystemOptions;
using CITG.Utility;
using CITG.StopWatchSystem;
using CITG.SessionSystem;

namespace CITG.TaskManagment
{
    using DebugHelpging;

    public class DebugTaskManager : TaskManager
    {
        private IDisposable _receiver;
        private Button _debugButton;

        TaskType activeTask;

        DebugHelper _helper;

        public DebugTaskManager(ZenjectSceneLoader loader, Option.Context context, TaskContainer container,
            InterfaceUpdater updater, StopWatch stopWatch, TaskType activeTask,
            DebugHelper helper,
            SessionProvider provider = null)
            : base(loader, context, container, updater, stopWatch, provider)
        {
            this.activeTask = activeTask;
            _helper = helper;
        }

        protected override void PrepareTesting()
        {
            if (_provider != null)
            {
                base.PrepareTesting();
                return;
            }

            _provider = new SessionProvider();
            _provider.CreateDebugSession();

            var task = _container.GetTask(activeTask);
            _stopWatch.Start();
            ActivateTask(task);
        }

        protected override void ActivateTask(Task task)
        {
            base.ActivateTask(task);
            _helper.DebugTask(task.Type);
        }

    }
}