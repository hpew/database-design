using CITG.UI.MainScene.DebugUI;

namespace CITG.TaskManagment.DebugHelpging.States
{
    public class SecondTaskDebugState : ITaskDebugState
    {
        SecondTask _task;

        public SecondTaskDebugState(SecondTask task) => _task = task;

        void ITaskDebugState.UpdateUI(DebugUIDesigner designer)
        {
            var taskButton = designer.ActivateTaskButton();
            var label = designer.ActivateDebugLabel();

            taskButton.onClick.AddListener(() =>
            {
                var (result, description) = _task.DebugValidate();

                UnityEngine.Debug.Log(description);

                taskButton.DisplayCorrectness(result);
                label.text = description;
            });
        }
    }
}