using System.Collections.Generic;
using CITG.UI.MainScene.DebugUI;
using Status = CITG.ERDiagram.Validator.Status;

namespace CITG.TaskManagment.DebugHelpging.States
{
    class FirstTaskDebugState : ITaskDebugState
    {
        FirstTask _task;

        public FirstTaskDebugState(FirstTask task) => _task = task;

        void ITaskDebugState.UpdateUI(DebugUIDesigner designer)
        {
            var taskButton = designer.ActivateTaskButton();
            var subTaskButton = designer.ActivateSubtaskButton("��������� ��������");

            taskButton.onClick.AddListener(() 
                => taskButton.DisplayCorrectness(_task.DebugValidate() == Status.CorrectModel));

            subTaskButton.onClick.AddListener(() =>
            {
                var status = _task.DebugValidate();
                subTaskButton.DisplayCorrectness(status == Status.CorrectModel || status == Status.CorrectFragments);
            });
        }
    }
}