using CITG.UI.MainScene.DebugUI;

namespace CITG.TaskManagment.DebugHelpging.States
{
    public class FourthTaskDebugState : ITaskDebugState
    {
        FourthTask _task;

        public FourthTaskDebugState(FourthTask task) => _task = task;

        void ITaskDebugState.UpdateUI(DebugUIDesigner designer)
        {
            var taskButton = designer.ActivateTaskButton();
            var subTaskButton = designer.ActivateSubtaskButton("�������� ������� ���");

            taskButton.onClick.AddListener(() =>
            {
                var result = _task.DebugValidate();
                taskButton.DisplayCorrectness(result);
            });

            subTaskButton.onClick.AddListener(() =>
            {
                subTaskButton.DisplayCorrectness(_task.DebugValidateCurrentCode());
            });
        }
    }
}