using CITG.UI.MainScene.DebugUI;

namespace CITG.TaskManagment.DebugHelpging
{
    using States;

    public class DebugHelper
    {
        DebugUIDesigner _designer;
        TaskDebugStateMachine _stateMachine;

        public DebugHelper(TaskType type, TaskContainer container, DebugUIDesigner designer)
        {
            _designer = designer;
            _stateMachine = new TaskDebugStateMachine(container, type);
        }

        public void DebugTask(TaskType type)
        {
            UnityEngine.Debug.Log("Debug Task ----> " + type);
            _stateMachine.GotoNextState(type);
            _designer.ResetUI();
            _stateMachine.CurrentState.UpdateUI(_designer);
        }
    }
}