using System.Collections.Generic;
using CITG.UI.MainScene.DebugUI;

namespace CITG.TaskManagment.DebugHelpging.States
{
    class TaskDebugStateMachine
    {
        Dictionary<TaskType, ITaskDebugState> _dictionaryStates;


        public TaskDebugStateMachine(TaskContainer container, TaskType activeTask)
        {
            _dictionaryStates = new Dictionary<TaskType, ITaskDebugState>()
            {
                [TaskType.First] = new FirstTaskDebugState(container.GetTask(TaskType.First) as FirstTask),
                [TaskType.Second] = new SecondTaskDebugState(container.GetTask(TaskType.Second) as SecondTask),
                [TaskType.Third] = new ThirdTaskDebugState(container.GetTask(TaskType.Third) as ThirdTask),
                [TaskType.Fourth] = new FourthTaskDebugState(container.GetTask(TaskType.Fourth) as FourthTask)

            };

            CurrentState = _dictionaryStates[activeTask];
        }

        public ITaskDebugState CurrentState { get; private set; }

        public void GotoNextState(TaskType type) => CurrentState = _dictionaryStates[type];
    }

    interface ITaskDebugState
    {
        void UpdateUI(DebugUIDesigner designer);
    }
}