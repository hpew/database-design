using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace CITG.Utility
{

    public class TaskContext<T> : IDisposable
    {
        CancellationTokenSource _source;


        public CancellationToken Token => _source.Token;
        public T Data { get; private set; }

        public TaskContext()
        {
            _source = new CancellationTokenSource();
        }

        public void Dispose() => _source.Dispose();

        public void Cancel(T data = default)
        {
            Data = data;
            _source.Cancel();
        }

        public async Task CancelAwaitFrame(T data = default)
        {
            Data = data;
            await _source.CancelUnity();
        }
    }
}