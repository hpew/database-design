using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
namespace CITG.Utility
{
    public static class ButtonObserver
    {
        public static async UniTask<Button> Track(params Button[] buttons)
        {
            var observableButtons = new ObservableButton[buttons.Length];

            for (int i = 0; i < observableButtons.Length; i++)
                observableButtons[i] = new ObservableButton(buttons[i]);

            Button selectedButton = null;
            while (selectedButton == null)
            {
                await UniTask.Yield();

                for (int i = 0; i < observableButtons.Length; i++)
                    if (observableButtons[i].IsClicked)
                    {
                        selectedButton = observableButtons[i].Button;
                        break;
                    }
            }

            for (int i = 0; i < observableButtons.Length; i++)
                observableButtons[i].Dispose();
                
            return selectedButton;
        }

        public static async UniTask<Button> Track(CancellationToken token, params Button[] buttons)
        {
            var observableButtons = new ObservableButton[buttons.Length];

            for (int i = 0; i < observableButtons.Length; i++)
                observableButtons[i] = new ObservableButton(buttons[i]);

            Button selectedButton = null;
            while (selectedButton == null)
            {
                await UniTask.Yield();
                if (token.IsCancellationRequested) token.ThrowIfCancellationRequested();

                for (int i = 0; i < observableButtons.Length; i++)
                    if (observableButtons[i].IsClicked)
                    {
                        selectedButton = observableButtons[i].Button;
                        break;
                    }
            }

            for (int i = 0; i < observableButtons.Length; i++)
                observableButtons[i].Dispose();

            if (token.IsCancellationRequested) token.ThrowIfCancellationRequested();

            return selectedButton;
        }

        public class ObservableButton : IDisposable
        {
            public Button Button { get; private set; }
            public bool IsClicked { get; private set; }

            public ObservableButton(Button button)
            {
                Button = button;
                IsClicked = false;

                Button.onClick.AddListener(HandleClick);
            }

            public void Dispose()
            {
                Button.onClick.RemoveListener(HandleClick);
                Button = null;
            }

            private void HandleClick() => IsClicked = true;
        }
    }
}