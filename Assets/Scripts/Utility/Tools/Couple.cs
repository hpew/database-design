using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CITG.Utility
{
    public struct Couple<T>
    {
        public T FirstElement;
        public T SecondElement;

        public Couple(T firstElement, T secondElement)
        {
            FirstElement = firstElement;
            SecondElement = secondElement;
        }
    }

    public struct Couple<T,K>
    {
        public T FirstElement;
        public K SecondElement;

        public Couple(T firstElement, K secondElement)
        {
            FirstElement = firstElement;
            SecondElement = secondElement;
        }
    }
}