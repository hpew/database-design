namespace CITG.Utility
{
    public class GeneratorID
    {
        int currentID;

        public GeneratorID()
        {
            currentID = 0;
        }

        public int Create() => ++currentID;
    }
}