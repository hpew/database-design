using System;
using UnityEngine;
namespace CITG.Utility
{
	public class FPSDisplay : MonoBehaviour
	{
		FPSCounter _fpsCounter;

		[SerializeField] bool isShow = true;

		Rect rect;

		int width;
		int height;
		GUIStyle labelStyle;

		static string[] stringsFrom00To99 =
		{
		"00", "01", "02", "03", "04", "05", "06", "07", "08", "09",
		"10", "11", "12", "13", "14", "15", "16", "17", "18", "19",
		"20", "21", "22", "23", "24", "25", "26", "27", "28", "29",
		"30", "31", "32", "33", "34", "35", "36", "37", "38", "39",
		"40", "41", "42", "43", "44", "45", "46", "47", "48", "49",
		"50", "51", "52", "53", "54", "55", "56", "57", "58", "59",
		"60", "61", "62", "63", "64", "65", "66", "67", "68", "69",
		"70", "71", "72", "73", "74", "75", "76", "77", "78", "79",
		"80", "81", "82", "83", "84", "85", "86", "87", "88", "89",
		"90", "91", "92", "93", "94", "95", "96", "97", "98", "99",
        "100", "101", "102", "103", "104", "105", "106", "107", "108", "109",
		"110", "111", "112", "113", "114", "115", "116", "117", "118", "119",
		"120", "121", "122", "123", "124", "125", "126", "127", "128", "129",
		"130", "131", "132", "133", "134", "135", "136", "137", "138", "139",
		"140", "141", "142", "143", "144", "145", "146", "147", "148", "149",
		"150", "151", "152", "153", "154", "155", "156", "157", "158", "159",
		"160", "161", "162", "163", "164", "165", "166", "167", "168", "169",
		"170", "171", "172", "173", "174", "175", "176", "177", "178", "179",
		"180", "181", "182", "183", "184", "185", "186", "187", "188", "189",
		"190", "191", "192", "193", "194", "195", "196", "197", "198", "199",
		};


	    void Awake()
		{
			width = Screen.width;
			height = Screen.height;

			rect = new Rect(50, 50, width - 30, height - 30);

			_fpsCounter = new FPSCounter();
		}

		void OnGUI()
		{
			if (!isShow) return;

			// Display the label at the center of the window.
			labelStyle = new GUIStyle(GUI.skin.GetStyle("label"));
			labelStyle.alignment = TextAnchor.UpperLeft;

			var size = 5;

#if UNITY_ANDROID && !UNITY_EDITOR
            size = 8;
#endif

			// Modify the size of the font based on the window.
			labelStyle.fontSize = size * (width / 200);

			Display(rect, _fpsCounter.AverageFPS, labelStyle);
		}

		void Display(Rect rect, int fps, GUIStyle style)
		{
			var @default = GUI.contentColor;
			GUI.contentColor = Color.red;
			GUI.Label(rect, stringsFrom00To99[Mathf.Clamp(fps, 0, 199)], style);
			GUI.contentColor = @default;

		}
	}

}
