using System;
using UnityEngine;

namespace CITG.Utility
{
    public static class ComponentUtility
    {
        public static T GetComponentNullException<T>(this Component source)
        {
            var component = source.GetComponent<T>();
            if (component == null) throw new Exception();

            return component;
        }

        public static T GetComponentInChildrenNullException<T>(this Component source, bool includeInactive = false)
        {
            var component = source.GetComponentInChildren<T>(includeInactive);
            if (component == null) throw new Exception();

            return component;
        }
    }
}