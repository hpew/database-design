using System;
using System.Collections;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

namespace CITG.Utility
{
    public static class Extensions
    {
        public static void SetActive(this Component component, bool value) => component.gameObject.SetActive(value);
        public static void SetRaycastTarget(this Selectable selectable, bool value) => selectable.image.raycastTarget = value;
    }

    public static class CameraExtensions
    {
        public static Vector2 GetMouseWorldPosition(this Camera camera)
            => camera.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y));

        public static Vector2 ConvertWorldToCanvasPosition(this Camera camera, Vector2 worldPosition, RectTransform rectTransformCanvas)
        {
            var viewportPosition = camera.WorldToViewportPoint(worldPosition); //������� �� 0 �� 1
            var canvasPosition = new Vector2(viewportPosition.x * rectTransformCanvas.sizeDelta.x,
                viewportPosition.y * rectTransformCanvas.sizeDelta.y); //������� ������� �� �������, ����������� vieport`��

            return canvasPosition;
        }

        public static Vector2 ConvertCanvasToWorldPosition(this Camera camera, Vector2 canvasPosition, RectTransform rectTransformCanvas)
        {
            var viewportPosition = new Vector2(canvasPosition.x / rectTransformCanvas.sizeDelta.x,
                canvasPosition.y / rectTransformCanvas.sizeDelta.y);

            var worldPosition = camera.ViewportToWorldPoint(viewportPosition);

            return worldPosition;
        }
    }

    public static class RectTransformExtensions
    {
        public static Vector2 GetSize(this RectTransform rectTransform) => rectTransform.rect.size;

        /// <summary>
        /// Converts the Rect RectTransform of an element to World Rect
        /// </summary>
        /// <param name="rt"></param>
        /// <returns></returns>
        public static Rect GetWorldRect(this RectTransform rt)
        {
            //�������� scale, ���� � ����� ���������� scaleFactor. 1 �� ���������, ��� ��� ������������ Scale With Screen Size
            var scale = Vector2.one;
            var rectSizeUI = rt.GetSize();

            var corners = new Vector3[4];
            rt.GetWorldCorners(corners);

            var topLeft = corners[0];
            var worldSize = new Vector2(scale.x * rectSizeUI.x, scale.y * rectSizeUI.y);

            return new Rect(topLeft, worldSize);
        }

        public static Rect GetWorlRectRelativeCameraSize(this RectTransform rectTransform, Camera camera, RectTransform rectTransformCanvas)
        {
            var rect = rectTransform.GetWorldRect();

            var min = camera.ConvertCanvasToWorldPosition(rect.min, rectTransformCanvas);
            var max = camera.ConvertCanvasToWorldPosition(rect.max, rectTransformCanvas);

            return new Rect(min.x, min.y, max.x - min.x, max.y - min.y);
        }

        #region Lerps

        public static async UniTask LerpSizeDelta(this RectTransform rect, Vector2 start, Vector2 target, float time, 
            CancellationToken cancel = default) => await Observable.FromMicroCoroutine(_ => LerpRectSizeDelta(rect, start, target, time))
            .ToUniTask(cancellationToken: cancel);

        private static IEnumerator LerpRectSizeDelta(RectTransform rect, Vector2 start, Vector2 target, float time)
        {
            var t = 0f;
            rect.sizeDelta = start;

            while (t < 1.0f)
            {
                rect.sizeDelta = Vector2.Lerp(start, target, t);
                t += Time.deltaTime / time;

                yield return null;
            }

            rect.sizeDelta = target;
        }


        public static async UniTask LerpAnchoredPosition(this RectTransform rect, Vector2 start, Vector2 target, float time, 
            CancellationToken cancel = default) => await Observable.FromMicroCoroutine(_ => LerpRectAnchoredPosition(rect, start, target, time))
            .ToUniTask(cancellationToken: cancel);

        private static IEnumerator LerpRectAnchoredPosition(RectTransform rect, Vector2 start, Vector2 target, float time)
        {
            var t = 0f;
            rect.anchoredPosition = start;

            while (t < 1.0f)
            {
                //Debug.Log(rect.anchoredPosition.ToString("F4") + " --- " + target.ToString("F4"));

                rect.anchoredPosition = Vector2.Lerp(start, target, t);
                t += Time.deltaTime / time;


                yield return null;
            }

            rect.anchoredPosition = target;
        }

        #endregion
    }

    public static class ImageExtensions
    {
        public static async UniTask LerpColor(this Image image, Color start, Color target, float time, CancellationToken cancel = default)
            => await Observable.FromMicroCoroutine(_ => LerpImageColor(image, start, target, time))
            .ToUniTask(cancellationToken: cancel);

        private static IEnumerator LerpImageColor(Image image, Color start, Color target, float time)
        {
            var t = 0f;
            image.color = start;

            while (t < 1.0f)
            {
                image.color = Color.Lerp(start, target, t);
                t += Time.deltaTime / time;

                yield return null;
            }

            image.color = target;
        }

        public static UniTask LerpAlpha(this Image image, float start, float target, float time, CancellationToken cancel = default)
        {
            var color = image.color;

            var startColor = new Color(color.r, color.g, color.b, start);
            var targetColor = new Color(color.r, color.g, color.b, target);

            return image.LerpColor(startColor, targetColor, time, cancel);
        }
    }

    public static class VectorExtensions
    {
        public static bool Approximately(this Vector3 one, Vector3 two)
        {
            if (!Mathf.Approximately(one.x, two.x)) return false;
            if (!Mathf.Approximately(one.y, two.y)) return false;
            if (!Mathf.Approximately(one.z, two.z)) return false;

            return true;
        }

        public static bool Approximately(this Vector2 one, Vector2 two)
        {
            if (!Mathf.Approximately(one.x, two.x)) return false;
            if (!Mathf.Approximately(one.y, two.y)) return false;

            return true;
        }
    }

    public static class StringExtensions
    {
        public static string WithoutWhiteSpace(this string line) => line.Replace(" ", string.Empty);

        public static string WithoutWhiteSpaceAndNewLine(this string line)
        {
            line = line.WithoutWhiteSpace();
            line = line.Replace("\n", "").Replace("\r", "");

            return line;
        }

        public static string WithoutNewLine(this string line)
        {
            return line.Replace("\n", "").Replace("\r", "");
        }
    }

    public static class TaskExtensions
    {
        /// <summary>
        /// ������ �������. ������� Cancel �� ����� ����� �� ��������� �����������, ���� �� �������� ���������.
        /// ������� �������� ��������� �� ������������ ��� ������������ � ����� ����������� �� ������ � ���� ����.
        /// ������������ await CancelUnity, ����� ������� ����� �� ���������� ������, � ������� ������� ������� ����������� ���������� � ������� ��� ������.
        /// </summary>
        /// <param name="source"></param>
        public static async UniTask CancelUnity(this CancellationTokenSource source)
        {
            source.Cancel();
            await UniTask.Yield();
            await UniTask.Yield();
        }
    }

    public static class EnumExtensions
    {
        public static bool Has<T>(this Enum type, T value)
        {
            try
            {
                return (((int)(object)type & (int)(object)value) == (int)(object)value);
            }
            catch
            {
                return false;
            }
        }

        public static bool Is<T>(this Enum type, T value)
        {
            try
            {
                return (int)(object)type == (int)(object)value;
            }
            catch
            {
                return false;
            }
        }


        public static T Add<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)(((int)(object)type | (int)(object)value));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not append value from enumerated type '{0}'.",
                        typeof(T).Name
                        ), ex);
            }
        }


        public static T Remove<T>(this Enum type, T value)
        {
            try
            {
                return (T)(object)(((int)(object)type & ~(int)(object)value));
            }
            catch (Exception ex)
            {
                throw new ArgumentException(
                    string.Format(
                        "Could not remove value from enumerated type '{0}'.",
                        typeof(T).Name
                        ), ex);
            }
        }
    }

}