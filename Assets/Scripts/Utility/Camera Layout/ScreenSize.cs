﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class ScreenSize
{
    static Camera camera;

    static Camera Camera
    {
        get
        {
            if (camera == null) camera = Camera.main;

            return camera;
        }
    }

    public static float GetScreenToWorldHeight
    {
        get
        {
            var topRightCorner = new Vector2(1, 1);
            var edgeVector = Camera.ViewportToWorldPoint(topRightCorner); var height = edgeVector.y * 2;
            return height;
        }
    }
    public static float GetScreenToWorldWidth
    {
        get
        {
            var topRightCorner = new Vector2(1, 1);
            var edgeVector = Camera.ViewportToWorldPoint(topRightCorner); var width = edgeVector.x * 2;
            return width;
        }
    }
}
