using Zenject;

namespace CITG.ZenjectTools
{
    public class MenuInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<CITG.Menu.Menu>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<CITG.StartupSystem.Startup>()
                .AsSingle()
                .NonLazy();
        }
    }

}