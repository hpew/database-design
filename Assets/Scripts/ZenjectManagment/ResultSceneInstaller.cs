using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace CITG.ZenjectTools
{
    public class ResultSceneInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindInterfacesAndSelfTo<ResultManagment.ResultManager>()
                .AsSingle()
                .NonLazy();
        }
    }
}