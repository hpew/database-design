using UnityEngine;
using Zenject;

namespace CITG.ZenjectTools
{
    [CreateAssetMenu(fileName = "ScriptableObjectsProjectContextInstaller", menuName = "Installers/ScriptableObjectsProjectContextInstaller")]
    public class ScriptableObjectsProjectContextInstaller : ScriptableObjectInstaller<ScriptableObjectsProjectContextInstaller>
    {
        [SerializeField] System.Assets _assets;
        [SerializeField] SystemOptions.ContainerOptions _containerOptions;

        public override void InstallBindings()
        {
            Container.BindInstance(_assets);
            Container.BindInstance(_containerOptions);
        }
    }
}