using UnityEngine; 
using Zenject;

namespace CITG.ZenjectTools
{
    public class MainSceneInstaller : MonoInstaller
    {
        [InjectOptional] SessionSystem.SessionProvider _provider = null;
        [Inject] SystemOptions.ContainerOptions _containerOptions;
        [Inject] System.Assets _assets;

        public override void InstallBindings()
        { 
            Container.BindInterfacesAndSelfTo<DragAndDrop.DragAndDropController>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.InterfaceUpdater>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<StopWatchSystem.StopWatch>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.TaskHelpForm.HintProvider>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.TaskHelpForm.FormPlaceholder>()
                .AsSingle()
                .NonLazy();      

            BindFactory();
            BindTasks();

            Startup();
        }

        private void Startup()
        {
            var isDebug = _assets.SettingsMainScene.OnDebug;

            var numberOption = _assets.SettingsMainScene.DefaultNumberOption;
            if (_provider != null) numberOption = _provider.Context.GetReport().UserData.NumberOption;

            var option = _containerOptions.GetOption(numberOption);
            Container.BindInstance(option.CreateContext(isDebug || _provider == null));

            if (isDebug || _provider == null)
            {
                Container.BindInterfacesAndSelfTo<TaskManagment.DebugHelpging.DebugHelper>()
                    .AsSingle()
                    .WithArguments(_assets.SettingsMainScene.ActiveTask)
                    .NonLazy();

                Container.BindInterfacesAndSelfTo<TaskManagment.DebugTaskManager>()
                    .AsSingle()
                    .WithArguments(_assets.SettingsMainScene.ActiveTask)
                    .NonLazy();
            }
            else
            {
                Container.BindInterfacesAndSelfTo<TaskManagment.TaskManager>()
                    .AsSingle()
                    .NonLazy();
            }
        }

        private void BindFactory()
        {
            Container.BindInterfacesAndSelfTo<UI.MainScene.UICollections.EntityFormationLine.Factory>()
               .AsSingle()
               .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.UICollections.RelationshipsFormationLine.Factory>()
               .AsSingle()
               .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.UICollections.TableListLine.Factory>()
               .AsSingle()
               .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.UICollections.TableEditorLine.Factory>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.UICollections.SelectableLine.Factory>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<UI.MainScene.LockRect.Factory>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<ERDiagram.ERFragment.Factory>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<ERDiagram.ERTransition.Factory>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TableDiagram.Fragment.Factory>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TableDiagram.LineFragment.Factory>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<VisualSQLQueryConstructionSystem.VisualSQLQuery.Factory>()
                .AsSingle()
                .NonLazy();
        }

        private void BindTasks()
        {
            BindFirstTask();
            BindSecondTask();
            BindThirdTask();
            BindFourTask();

            Container.BindInterfacesAndSelfTo<TaskManagment.TaskContainer>()
                .AsSingle()
                .NonLazy();
        }



        private void BindFirstTask()
        {
            Container.BindInterfacesAndSelfTo<ERDiagram.Validator>()
               .AsSingle()
               .NonLazy();

            Container.BindInterfacesAndSelfTo<ERDiagram.ERTransitionCreator>()
               .AsSingle()
               .NonLazy();

            Container.BindInterfacesAndSelfTo<ERDiagram.ERModel>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<EntitiesAndRelationships.ContainerEntitiesAndRelationships>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TaskManagment.FirstTask>()
              .AsSingle()
              .NonLazy();
        }
       
        private void BindSecondTask()
        {
            Container.BindInterfacesAndSelfTo<TableSystem.Validator>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<TableSystem.TableManager>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<TableDiagram.ConnectionMap>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TableDiagram.Model>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TaskManagment.SecondTask>()
                 .AsSingle()
                 .NonLazy();
        }

        private void BindThirdTask()
        {
            Container.BindInterfacesAndSelfTo<VisualSQLQueryConstructionSystem.VisualSQLQueryManager>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TaskManagment.ThirdTask>()
                .AsSingle()
                .NonLazy();
        }

        private void BindFourTask()
        {
            Container.BindInterfacesAndSelfTo<SQLCode.SQLCodeProvider>()
                .AsSingle()
                .NonLazy();

            Container.BindInterfacesAndSelfTo<SQLCode.SQLCodeManager>()
                 .AsSingle()
                 .NonLazy();

            Container.BindInterfacesAndSelfTo<TaskManagment.FourthTask>()
                .AsSingle()
                .NonLazy();
        }
    }
}