﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using XNode;
using CITG.Utility;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{
	[CreateAssetMenu(fileName = "SQL Query Graph", menuName = "System/Graphes/SQL Query Graph")]
	public class SQLQueryGraph : NodeGraph
	{
		[SerializeField] StartNode _startNode;
		[SerializeField] SQLQueryNode _endNode;
		[SerializeField] SQLQueryNode _currentNode;

		[SerializeField, TextArea(minLines: 15, maxLines: 50)] string debugField;
		public string DebugField => debugField;
		public SQLQueryNode CurrentNode => _currentNode;

		public bool CheckFragment(string fragment)
        {
			var result = _currentNode.HandleInputFragment(fragment);

			if (result.Status == SQLQueryNode.ResultStatus.Unsuccessful) return false;

			if (result.Status == SQLQueryNode.ResultStatus.End)
            {
				if (_currentNode == _endNode) return true;
				return false;
            }

			if (result.NextNode != _currentNode)
            {
				_currentNode.OnExit();
				_currentNode = result.NextNode;
				_currentNode.OnEnter(fragment);
            }

			return true;
		}

		public bool CheckFragments(Queue<string> fragments)
        {
			if (fragments.Count == 0) return false;
			Reset();

			fragments.Enqueue(string.Empty);

			while (true)
            {
				if (fragments.Count == 0) return false;
				var fragment = fragments.Dequeue();

				var result = _currentNode.HandleInputFragment(fragment);

				if (result.Status == SQLQueryNode.ResultStatus.Unsuccessful) return false;

				if (result.Status == SQLQueryNode.ResultStatus.Successfully)
                {
					if (result.NextNode != _currentNode)
                    {
						_currentNode.OnExit();
						_currentNode = result.NextNode;
						_currentNode.OnEnter(fragment);
					}

					continue;
                }

				if (result.Status == SQLQueryNode.ResultStatus.End)
                {
					if (_currentNode == _endNode) return true;
					else return false;
                }
            }
        }

		public void Reset()
        {
			_currentNode?.OnExit();
			_currentNode = _startNode;
			_currentNode?.OnEnter(string.Empty);
		}
	}
}