﻿using System;
using System.Collections.Generic;
using UnityEngine;
using XNodeEditor;
using CITG.xNodeGraphes.SQLQueryGraphSystem;
using CITG.Utility;

namespace CITG.Editor
{
    [CustomNodeGraphEditor(typeof(SQLQueryGraph))]
    public class SQLQueryGraphEditor : NodeGraphEditor
    {
        Queue<string> _fragmentsBigCheck;
        Queue<string> _fragmentsOneCheck;

        public override void OnOpen()
        {
            base.OnOpen();
            _fragmentsBigCheck = new Queue<string>();
            _fragmentsOneCheck = new Queue<string>();
        }

        public override string GetNodeMenuName(Type type)
        {
            if (type.Namespace == typeof(SQLQueryGraph).Namespace)
            {
                return base.GetNodeMenuName(type).Replace("CITG/x Node Graphes/SQL Query Graph System/", "");
            }
            else return null;
        }

        public override void OnGUI()
        {
            base.OnGUI();

            var graph = target as SQLQueryGraph;

            if (GUILayout.Button("RESET"))
            {
                var tokens = graph.DebugField.WithoutNewLine().Split(';');

                _fragmentsBigCheck = new Queue<string>(tokens);

                _fragmentsOneCheck = new Queue<string>(tokens);
                _fragmentsOneCheck.Enqueue(string.Empty);

                graph.Reset();
            }

            if (_fragmentsOneCheck.Count != 0)
            {
                if (GUILayout.Button($"Check Next Fragment - {_fragmentsOneCheck.Peek()}"))
                    Debug.Log(graph.CheckFragment(_fragmentsOneCheck.Dequeue()));
            }

            if (_fragmentsBigCheck.Count != 0)
                if (GUILayout.Button($"Check All Fragments"))
                    Debug.Log(graph.CheckFragments(_fragmentsBigCheck));
        }
    }
}