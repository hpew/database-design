using System;
using UnityEngine;
using XNode;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{
    public class StartNode : SQLQueryNode
    {
        public override object GetValue(NodePort port) => null;

        public override Result HandleInputFragment(string fragment)
        {
            var targetGraph = graph as SQLQueryGraph;
            var exitPort = GetOutputPort("exit");

            if (!exitPort.IsConnected)
                throw new Exception("Node isn`t connected");

            var connections = exitPort.GetConnections();
            if (connections == null || connections.Count == 0) return Result.End;


            var connection = connections.Find(port => (port.node as SQLQueryNode).ContainsFragment(fragment));
            if (connection == null) return Result.Unsuccessful;

            return new Result()
            {
                NextNode = connection.node as SQLQueryNode,
                Status = ResultStatus.Successfully,
            };
        }

        public override bool ContainsFragment(string fragment) => false;
    }
}