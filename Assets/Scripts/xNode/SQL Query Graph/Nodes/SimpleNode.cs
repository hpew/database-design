using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{

    public class SimpleNode : SQLQueryNode
    {
        [SerializeField] string fragment;

        public override object GetValue(NodePort port) => fragment;

        public override Result HandleInputFragment(string fragment)
        {
            var connections = GetConnectionExitPort();
            if (connections == null || connections.Count == 0) return Result.End;

            var connection = connections.Find(port => (port.node as SQLQueryNode).ContainsFragment(fragment));
            if (connection == null) return Result.Unsuccessful;

            return new Result()
            {
                NextNode = connection.node as SQLQueryNode,
                Status = ResultStatus.Successfully
            };
        }

        public override bool ContainsFragment(string fragment) => this.fragment == fragment;
    }
}