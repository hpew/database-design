﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using XNodeEditor;
using UnityEngine;
using CITG.xNodeGraphes.SQLQueryGraphSystem;

namespace CITG.Editor
{
    [CustomNodeEditor(typeof(SQLQueryNode))]
    public class SQLQueryNodeEditor : NodeEditor
    {
		public override void OnHeaderGUI()
		{
			var node = target as SQLQueryNode;
			var graph = node.graph as SQLQueryGraph;

			var defaultColor = GUI.color;
			GUI.color = Color.white;

            if (graph.CurrentNode == node) GUI.color = Color.blue;

            string title = target.name;
            GUILayout.Label(title, NodeEditorResources.styles.nodeHeader, GUILayout.Height(30));

            GUI.color = Color.white;
            if (node.IsEntered) GUILayout.Label("Is Entered", NodeEditorResources.styles.nodeHeader, GUILayout.Height(30));

            GUI.color = defaultColor;
        }
	}
}