using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using CITG.Utility;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{
    public class SymmetricNode : SQLQueryNode
    {
        [SerializeField] string firstFragment;
        [SerializeField] string secondFragment;
        [SerializeField] string middleFragment = "=";

        Status status;

        static Status finishStatus = Status.FirstFragmentChecked | Status.EqualFragmentChecked | Status.SecondFragmentChecked;

        public override object GetValue(NodePort port) => firstFragment + " - " + secondFragment;

        public override Result HandleInputFragment(string fragment)
        {
            if (status == Status.FirstFragmentChecked || status == Status.SecondFragmentChecked)
            {
                if (fragment == middleFragment)
                {
                    status = status.Add(Status.EqualFragmentChecked);
                    return new Result()
                    {
                        Status = ResultStatus.Successfully,
                        NextNode = this
                    };
                }
                else return Result.Unsuccessful;
            }
            else if (status == (Status.FirstFragmentChecked | Status.EqualFragmentChecked))
            {
                if (fragment == secondFragment)
                {
                    status = finishStatus;

                    return new Result()
                    {
                        NextNode = this,
                        Status = ResultStatus.Successfully
                    };
                }
                else return Result.Unsuccessful;
            }
            else if (status == (Status.SecondFragmentChecked | Status.EqualFragmentChecked))
            {
                if (fragment == firstFragment)
                {
                    status = finishStatus;

                    return new Result()
                    {
                        NextNode = this,
                        Status = ResultStatus.Successfully
                    };
                }
                else return Result.Unsuccessful;
            }
            else if (status == finishStatus)
            {
                var connections = GetConnectionExitPort();
                if (connections == null || connections.Count == 0) return Result.End;

                var connection = connections.Find(port => (port.node as SQLQueryNode).ContainsFragment(fragment));
                if (connection == null) return Result.Unsuccessful;

                return new Result()
                {
                    NextNode = connection.node as SQLQueryNode,
                    Status = ResultStatus.Successfully,
                };
            }

            return Result.Unsuccessful;
        }

        public override bool ContainsFragment(string fragment) => fragment == firstFragment || fragment == secondFragment;

        public override void OnEnter(string fragment)
        {
            base.OnEnter(fragment);
            if (fragment == firstFragment) status = Status.FirstFragmentChecked;
            else if (fragment == secondFragment) status = Status.SecondFragmentChecked;
            else throw new Exception("EqualsNode get not correct fragment");
        }

        [Flags]
        enum Status
        {
            FirstFragmentChecked = 2,
            SecondFragmentChecked = 4,
            EqualFragmentChecked = 8,
        }
    }
}
