using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{
    public abstract class SQLQueryNode : Node
	{
		[Input] public Empty enter;
		[Output] public Empty exit;

		public bool IsEntered { get; private set; }

		public abstract Result HandleInputFragment(string fragment);
		public abstract bool ContainsFragment(string fragment);

		public virtual void OnEnter(string fragment) => IsEntered = true;
		public virtual void OnExit() => IsEntered = false;

		protected List<NodePort> GetConnectionExitPort()
		{
			var exitPort = GetOutputPort("exit");
			return exitPort.GetConnections();
		}

		[Serializable]
		public class Empty { }

		public struct Result
		{
			public SQLQueryNode NextNode;
			public ResultStatus Status;

			public static Result Unsuccessful => new Result() { Status = ResultStatus.Unsuccessful };

			public static Result End => new Result() { Status = ResultStatus.End };
		}

		public enum ResultStatus
        {
			Successfully,
			Unsuccessful,
			End
		}
	}
}