using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{
    public class ComplexSymmetricNode : SQLQueryNode
    {
        [Output] public Empty andConnections;

        [SerializeField] string symmetricFragment = "AND";

        List<SQLQueryNode> _connectionNodes;
        SQLQueryNode _currentNode;

        int countSymmetricFragments;

        public override bool ContainsFragment(string fragment)
        {
            var nodes = GetNodes();

            var node = nodes.Find(node => node.ContainsFragment(fragment));
            if (node != null) return true;
            return false;
        }

        public override Result HandleInputFragment(string fragment)
        {
            if (_currentNode != null)
            {
                var result = _currentNode.HandleInputFragment(fragment);

                if (result.Status == ResultStatus.Successfully)
                {
                    if (_currentNode != result.NextNode)
                    {
                        _currentNode?.OnExit();
                        _currentNode = result.NextNode;
                        _currentNode.OnEnter(fragment);
                    }

                    return new Result()
                    {
                        NextNode = this,
                        Status = ResultStatus.Successfully
                    };
                }
                else if (result.Status == ResultStatus.Unsuccessful) return Result.Unsuccessful;
                else
                {
                    if (countSymmetricFragments != 0)
                    {
                        if (fragment != symmetricFragment) return Result.Unsuccessful;

                        _currentNode.OnExit();
                        _currentNode = null;

                        countSymmetricFragments--;
                        return new Result()
                        {
                            NextNode = this,
                            Status = ResultStatus.Successfully
                        };
                    }

                    var connections = GetConnectionExitPort();
                    if (connections == null || connections.Count == 0) return Result.End;

                    var connection = connections.Find(port => (port.node as SQLQueryNode).ContainsFragment(fragment));
                    if (connection == null) return Result.Unsuccessful;

                    return new Result()
                    {
                        NextNode = connection.node as SQLQueryNode,
                        Status = ResultStatus.Successfully
                    };
                }
            }
            else
            {
                var node = _connectionNodes.Find(node => node.ContainsFragment(fragment));
                if (node == null) return Result.Unsuccessful;

                _currentNode = node;
                _currentNode.OnEnter(fragment);

                _connectionNodes.Remove(node);

                return new Result()
                {
                    NextNode = this,
                    Status = ResultStatus.Successfully
                };
            }
        }

        public override void OnEnter(string fragment)
        {
            base.OnEnter(fragment);
            _connectionNodes = GetNodes();
            countSymmetricFragments = _connectionNodes.Count - 1;

            var node = _connectionNodes.Find(node => node.ContainsFragment(fragment));
            if (node == null) throw new Exception("AndNode get not correct fragment");

            _currentNode?.OnExit();
            _currentNode = node;
            _currentNode.OnEnter(fragment);

            _connectionNodes.Remove(node);
        }

        public override void OnExit()
        {
            base.OnExit();
            _connectionNodes = null;

            _currentNode?.OnExit();
            _currentNode = null;
        }

        public override object GetValue(NodePort port) => null;

        private List<SQLQueryNode> GetNodes()
        {
            var port = GetOutputPort(nameof(andConnections));

            if (!port.IsConnected) throw new Exception("And Connections isn`t connected");
            return port.GetConnections().ConvertAll(port => port.node as SQLQueryNode);
        }
    }  
}