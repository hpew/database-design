using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using CITG.Utility;

namespace CITG.xNodeGraphes.SQLQueryGraphSystem
{
    public class EnumerationNode : SQLQueryNode
    {
        [SerializeField] List<string> _fragments;
        List<string> _unsolicitedFragments;

        public override object GetValue(NodePort port) => _fragments == null ? 0 : _fragments.Count;

        public override Result HandleInputFragment(string fragment)
        {
            if (_unsolicitedFragments.Count == 0)
            {
                var connections = GetConnectionExitPort();
                if (connections == null || connections.Count == 0) return Result.End;

                var connection = connections.Find(port => (port.node as SQLQueryNode).ContainsFragment(fragment));
                if (connection == null) return Result.Unsuccessful;

                return new Result()
                {
                    NextNode = connection.node as SQLQueryNode,
                    Status = ResultStatus.Successfully,
                };
            }

            if (_unsolicitedFragments.Contains(fragment))
            {
                _unsolicitedFragments.Remove(fragment);
                return new Result()
                {
                    NextNode = this,
                    Status = ResultStatus.Successfully,
                };
            }

            return Result.Unsuccessful;
        }

        public override bool ContainsFragment(string fragment) => _fragments.Contains(fragment);

        public override void OnEnter(string fragment)
        {
            base.OnEnter(fragment);

            _unsolicitedFragments = new List<string>(_fragments);
            if (_unsolicitedFragments.Contains(fragment)) _unsolicitedFragments.Remove(fragment);
        }
    }
}