using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNodeEditor;
using CITG.xNodeGraphes.ERModel;

namespace CITG.Editor
{
    [CustomNodeGraphEditor(typeof(ERModelNodeGraph))]
    public class ERModelNodeGraphEditor : NodeGraphEditor
    {
        public override string GetNodeMenuName(Type type)
        {
            if (type.Namespace == typeof(ERModelNodeGraph).Namespace)
            {
                return base.GetNodeMenuName(type).Replace("CITG/x Node Graphes/ER Model/", "");
            }
            else return null;
        }

        public override void OnGUI()
        {
            base.OnGUI();

            var graph = target as ERModelNodeGraph;

            if (GUILayout.Button("RESET")) ;// graph.Reset();
        }
    }
}
