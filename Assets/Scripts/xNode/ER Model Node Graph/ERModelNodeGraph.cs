using System;
using UnityEngine;
using XNode;
using CITG.ERDiagram;

namespace CITG.xNodeGraphes.ERModel
{
    [CreateAssetMenu(fileName = "ER Model Node Graph", menuName = "System/Graphes/ER Model Node Graph")]
    public class ERModelNodeGraph : NodeGraph
    {
		[SerializeField] EntryNode _entryNode;

		public bool Validate(EREntity[] entities) => _entryNode.Validate(entities);
    }
}