using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using CITG.ERDiagram;

namespace CITG.xNodeGraphes.ERModel
{
    public class EntityNode : ERNode
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] public EntryNode.Empty _entryPort;

        [Input, SerializeField] public Empty _enter;
        [Output, SerializeField] public Empty _exit;

        [SerializeField] protected string context;
        public string Context => context;

        public bool Validate(EREntity entity)
        {
            if (entity.Context != Context) return NotificationManager.GetContextMismatchError(Context, entity.Context);

            var enterPorts = GetInputPort(nameof(_enter));
            var exitPorts = GetOutputPort(nameof(_exit));

            var inputConnections = enterPorts.GetConnections();
            if (entity.InputTransitions.Length != inputConnections.Count) 
                return NotificationManager.GetErrorMismatchCountTransitions(Context, entity.InputTransitions.Length, inputConnections.Count);

            var outputConnections = exitPorts.GetConnections();
            if (entity.OutputTransitions.Length != outputConnections.Count)
                return NotificationManager.GetErrorMismatchCountTransitions(Context, entity.OutputTransitions.Length, outputConnections.Count, false);

            foreach (var connection in outputConnections)
            {
                var relationNode = connection.node as RelationNode;
                if (relationNode == null) throw new EntityNodeException();

                var isFind = false;
                foreach (var transition in entity.OutputTransitions)
                {
                    var status = relationNode.Validate(transition);
                    if (status == RelationNode.StatusValidate.Incorrectly) return NotificationManager.GetErrorEntityNotValidate(Context);
                    if (status == RelationNode.StatusValidate.NotFound) continue;
                    if (status == RelationNode.StatusValidate.Correctly)
                    {
                        isFind = true;
                        break;
                    }
                }

                if (!isFind) return NotificationManager.GetErrorEntityNotValidate(Context);
            }

            return true;
        }
    }
}