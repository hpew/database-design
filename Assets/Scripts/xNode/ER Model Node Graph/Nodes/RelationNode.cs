using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using CITG.ERDiagram;

namespace CITG.xNodeGraphes.ERModel
{
    public class RelationNode : ERNode
    {
        [Input(connectionType = ConnectionType.Override), SerializeField] public Empty _enter;
        [Output(connectionType = ConnectionType.Override), SerializeField] public Empty _exit;

        [SerializeField] protected Ratio outputRatio;
        [SerializeField] protected Ratio inputRatio;
        public Ratio OutputRatio => outputRatio;
        public Ratio InputRatio => inputRatio;
        
        public StatusValidate Validate(ERTransition transition)
        {
            var port = GetOutputPort(nameof(_exit));
            if (!port.IsConnected) throw new RelationNodeException(RelationNodeException.Type.NonConnections);

            var linkEntityNode = port.Connection.node as EntityNode;
            if (linkEntityNode == null) throw new RelationNodeException(RelationNodeException.Type.NonEntityNode);

            var line = transition as ERLine;
            if (line == null) return StatusValidate.Incorrectly;

            var relation = line.SecondFragment as ERRelationship;
            if (relation == null) return StatusValidate.Incorrectly;

            if (relation.OutputTransitions.Length == 0 || relation.OutputTransitions.Length > 1) return StatusValidate.Incorrectly;
            if (relation.InputTransitions.Length == 0 || relation.InputTransitions.Length > 1) return StatusValidate.Incorrectly;

            var arrow = relation.OutputTransitions[0] as ERArrow;
            if (arrow == null) return StatusValidate.Incorrectly;

            var linkEntity = arrow.SecondFragment as EREntity;
            if (linkEntity == null) return StatusValidate.Incorrectly;

            if (linkEntity.Context != linkEntityNode.Context) return StatusValidate.NotFound;

            var outputText = OutputRatio == Ratio.One ? "1" : "�";
            var inputText = InputRatio == Ratio.One ? "1" : "�";

            if (line.OutputText != outputText) return StatusValidate.Incorrectly;
            if (line.InputText != string.Empty) return StatusValidate.Incorrectly;


            if (arrow.OutputText != string.Empty) return StatusValidate.Incorrectly;
            if (arrow.InputText != inputText) return StatusValidate.Incorrectly;

            if (linkEntityNode.Validate(linkEntity)) return StatusValidate.Correctly;
            else return StatusValidate.Incorrectly;
        }


        public enum Ratio { One, Many }
        public enum StatusValidate
        {
            Correctly,
            Incorrectly,
            NotFound,
        }
    }
}