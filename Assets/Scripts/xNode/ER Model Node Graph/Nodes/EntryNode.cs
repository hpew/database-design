using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;
using CITG.ERDiagram;

namespace CITG.xNodeGraphes.ERModel
{
    public class EntryNode : Node
    {
        [Output, SerializeField] Empty _entityPort;

        public bool Validate(EREntity[] entities)
        {
            var entitiesPort = GetOutputPort(nameof(_entityPort));
            if (!entitiesPort.IsConnected) throw new EntryNodeException(EntryNodeException.Type.NonConnections);

            var connections = entitiesPort.GetConnections();
            var entityNodes = new List<EntityNode>();

            foreach (var connection in connections)
            {
                if (connection.node is EntityNode)
                    entityNodes.Add(connection.node as EntityNode);
                else throw new EntryNodeException(EntryNodeException.Type.NonEntityNode);
            }

            foreach (var entityNode in entityNodes)
            {
                var entity = Array.Find(entities, entity => entity.Context == entityNode.Context);
                if (entity == null) return NotificationManager.GetErrorEntityNotFound(entityNode.Context);

                if (!entityNode.Validate(entity)) return NotificationManager.GetErrorEntityNotValidate(entityNode.Context);
            }

            return true;
        }

        [Serializable]
        public class Empty { }
    }
}