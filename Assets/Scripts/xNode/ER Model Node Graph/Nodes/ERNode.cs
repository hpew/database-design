using System;
using System.Collections.Generic;
using UnityEngine;
using XNode;

namespace CITG.xNodeGraphes.ERModel
{
    public abstract class ERNode : Node
    {
        public override object GetValue(NodePort port)
        {
            return port.GetConnections();
        }

        [Serializable]
        public class Empty { }
	}
}