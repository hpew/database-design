using System;
using System.Collections.Generic;
using UnityEngine;

namespace CITG.xNodeGraphes.ERModel
{
    static class NotificationManager
    {
        public static bool GetErrorEntityNotFound(string entity)
        {
            Debug.LogWarning($"Entity not found: {entity}");
            return false;
        }

        public static bool GetErrorEntityNotValidate(string entity)
        {
            Debug.LogWarning($"Entity not validate: {entity}");
            return false;
        }

        public static bool GetContextMismatchError(string expectedEntity, string notAppropriateEntity)
        {
            Debug.LogWarning($"Entity contexts don't match:\n" +
                $"Expected Entity: {expectedEntity}\n" +
                $"Not Appropriate Entity: {notAppropriateEntity}\n");
            return false;
        }

        public static bool GetErrorMismatchCountTransitions(string entity, int expectedCount, int realCount, bool isInput = true)
        {
            Debug.LogWarning($"The number of {(isInput ? "input" : "output")} input transitions does not match:\n" +
              $"Entity: {entity}\n" +
              $"Expected Count: {expectedCount}\n" +
              $"RealCount Count: {realCount}\n");

            return false;
        }
    }

    public class EntryNodeException : Exception
    {
        static Dictionary<Type, string> _messages = new Dictionary<Type, string>()
        {
            [Type.NonEntityNode] = "NonEntity Nodes are connected to the entry node.",
            [Type.NonConnections] = "Entry node has no connection",
        };

        public EntryNodeException(Type type) : base(_messages[type]) { }

        public enum Type
        {
            NonEntityNode,
            NonConnections
        }
    }

    public class EntityNodeException : Exception
    {
        static Dictionary<Type, string> _messages = new Dictionary<Type, string>()
        {
            [Type.NonRelationNode] = "NonRelation Nodes are connected to the entity node.",
        };

        public EntityNodeException() : base(_messages[Type.NonRelationNode]) { }

        public enum Type
        {
            NonRelationNode,
        }
    }

    public class RelationNodeException : Exception
    {
        static Dictionary<Type, string> _messages = new Dictionary<Type, string>()
        {
            [Type.NonEntityNode] = "NonEntity Nodes are connected to the relation node.",
            [Type.NonConnections] = "Relation node has no connection",
        };

        public RelationNodeException(Type type) : base(_messages[type]) { }

        public enum Type
        {
            NonEntityNode,
            NonConnections,

        }
    }
}