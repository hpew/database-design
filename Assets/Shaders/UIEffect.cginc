#ifndef UI_EFFECT_INCLUDED
#define UI_EFFECT_INCLUDED


sampler2D _TransitionTex;
sampler2D _ParamTex;

#if GRAYSCALE | SEPIA | NEGA | PIXEL | MONO | CUTOFF | HUE
#define UI_TONE
#endif

#if ADD | SUBTRACT | FILL
#define UI_COLOR
#endif

#if FASTBLUR | MEDIUMBLUR | DETAILBLUR
#define UI_BLUR
#endif

// Unpack float to low-precision [0-1] fixed4.
fixed4 UnpackToVec4(half value)
{

	fixed4 unpacked;

	unpacked.x = (value % 64) * 0.015873h;
	value = floor(value * 0.015625h);

	unpacked.y = (value % 64) * 0.015873h;
	value = floor(value * 0.015625h);

	unpacked.z = (value % 64) * 0.015873h;
	value = floor(value * 0.015625h);

	unpacked.w = (value % 64) * 0.015873h;
	return unpacked;
}

// Unpack float to low-precision [0-1] fixed3.
fixed3 UnpackToVec3(half value)
{

	fixed3 unpacked;

	unpacked.x = (value % 256) * 0.003921569h;
	value = floor(value * 0.00390625h);

	unpacked.y = (value % 256) * 0.003921569h;
	value = floor(value * 0.00390625h);

	unpacked.z = (value % 256) * 0.003921569h;
	return unpacked;
}

// Unpack float to low-precision [0-1] half2.
half2 UnpackToVec2(float value)
{
	half2 unpacked;

	unpacked.x = (value % (4096)) / 4095;
	value = floor(value * 0.000244140625h);

	unpacked.y = (value % 4096) / 4095;
	return unpacked;
}

// Sample texture with blurring.
// * Fast: Sample texture with 3x3 kernel.
// * Medium: Sample texture with 5x5 kernel.
// * Detail: Sample texture with 7x7 kernel.
fixed4 Tex2DBlurring (sampler2D tex, half2 texcood, half2 blur, half4 mask)
{
	#if FASTBLUR && EX
	const int KERNEL_SIZE = 5;
	const half KERNEL_[5] = { 0.2486h, 0.7046h, 1.0h, 0.7046h, 0.2486h};
	#elif MEDIUMBLUR && EX
	const int KERNEL_SIZE = 9;
	const half KERNEL_[9] = { 0.0438h, 0.1719h, 0.4566h, 0.8204h, 1.0h, 0.8204h, 0.4566h, 0.1719h, 0.0438h};
	#elif DETAILBLUR && EX
	const int KERNEL_SIZE = 13;
	const half KERNEL_[13] = { 0.0438h, 0.1138h, 0.2486h, 0.4566h, 0.7046h, 0.9141h, 1.0h, 0.9141h, 0.7046h, 0.4566, 0.2486h, 0.1138h, 0.0438h};
	#elif FASTBLUR
	const int KERNEL_SIZE = 3;
	const half KERNEL_[3] = { 0.4566h, 1.0h, 0.4566h};
	#elif MEDIUMBLUR
	const int KERNEL_SIZE = 5;
	const half KERNEL_[5] = { 0.2486h, 0.7046h, 1.0h, 0.7046h, 0.2486h};
	#elif DETAILBLUR
	const int KERNEL_SIZE = 7;
	const half KERNEL_[7] = { 0.1719h, 0.4566h, 0.8204h, 1.0h, 0.8204h, 0.4566h, 0.1719h};
	#else
	const int KERNEL_SIZE = 1;
	const half KERNEL_[1] = { 1.0h };
	#endif

	fixed4 o = 0.0h;
	fixed sum = 0.0h;
	fixed2 shift = 0.0h;
	fixed kernelSizeHalf = KERNEL_SIZE * 0.5h;

	#if EX
	fixed2 buf = fixed2(0.5h, 0.0h);
	#endif

	for(int x = 0; x < KERNEL_SIZE; x++)
	{
		shift.x = blur.x * (x - kernelSizeHalf);
		
		for(int y = 0; y < KERNEL_SIZE; y++)
		{
			fixed2 uv = texcood + shift;
			shift.y = blur.y * (y - kernelSizeHalf);
			fixed weight = KERNEL_[x] * KERNEL_[y];
			sum += weight;
			#if EX
			o += lerp(buf.xxxy, tex2D(tex, uv), min(mask.x <= uv.x, uv.x <= mask.z) * min(mask.y <= uv.y, uv.y <= mask.w)) * weight;
			#else
			o += tex2D(tex, uv) * weight;
			#endif
		}
	}

	return o / sum;
}

// Sample texture with blurring.
// * Fast: Sample texture with 3x3 kernel.
// * Medium: Sample texture with 5x5 kernel.
// * Detail: Sample texture with 7x7 kernel.
fixed4 Tex2DBlurring (sampler2D tex, half2 texcood, half2 blur)
{
	return Tex2DBlurring(tex, texcood, blur, fixed4(0.0h, 0.0h, 1.0h, 1.0h));
}


// Sample texture with blurring.
// * Fast: Sample texture with 3x1 kernel.
// * Medium: Sample texture with 5x1 kernel.
// * Detail: Sample texture with 7x1 kernel.
fixed4 Tex2DBlurring1D (sampler2D tex, half2 uv, half2 blur)
{
	#if FASTBLUR
	const int KERNEL_SIZE = 1.5;
	#elif MEDIUMBLUR
	const int KERNEL_SIZE = 2.5;
	#elif DETAILBLUR
	const int KERNEL_SIZE = 3.5;
	#else
	const int KERNEL_SIZE = 0.5;
	#endif
	fixed4 o = 0.0h;
	fixed sum = 0.0h;
	fixed weight;

	for(int i = -KERNEL_SIZE; i <= KERNEL_SIZE; i++)
	{
		weight = 1.0h / (abs(i) + 2.0h);
		o += tex2D(tex, uv + (blur * i)) * weight;
		sum += weight;
	}

	return o / sum;
}

fixed3 shift_hue(fixed3 RGB, half VSU, half VSW)
{
	fixed3 result;

	half VSUom = 1.0h - VSU;
	half VSUopt = VSUom * 0.114h;

	result.x = (0.299h + 0.701h * VSU + 0.168h * VSW) * RGB.x
			 + (0.587h		  * VSUom + 0.330h * VSW) * RGB.y
			 + (		       VSUopt - 0.497h * VSW) * RGB.z;

	result.y = (0.299h       * VSUom - 0.328h   * VSW) * RGB.x
			 + (0.587h + 0.413h * VSU + 0.035h  * VSW) * RGB.y
			 + (		     VSUopt + 0.292h    * VSW) * RGB.z;

	result.z = (0.299h - 0.3h   * VSU + 1.25h  * VSW) * RGB.x
			 + (0.587h - 0.588h * VSU - 1.05h  * VSW) * RGB.y
			 + (0.114h + 0.886h * VSU - 0.203h * VSW) * RGB.z;

	return result;
}

// Apply tone effect.
fixed4 ApplyToneEffect(fixed4 color, fixed factor)
{
	#ifdef GRAYSCALE
	color.rgb = lerp(color.rgb, Luminance(color.rgb), factor);

	#elif SEPIA
	color.rgb = lerp(color.rgb, Luminance(color.rgb) * half3(1.07h, 0.74h, 0.43h), factor);

	#elif NEGA
	color.rgb = lerp(color.rgb, 1.0h - color.rgb, factor);
	#endif

	return color;
}

// Apply color effect.
fixed4 ApplyColorEffect(fixed4 color, fixed4 factor)
{
	#if FILL
	color.rgb = lerp(color.rgb, factor.rgb, factor.a);

	#elif ADD
	color.rgb += factor.rgb * factor.a;

	#elif SUBTRACT
	color.rgb -= factor.rgb * factor.a;

	#else
	color.rgb = lerp(color.rgb, color.rgb * factor.rgb, factor.a);
	#endif

	#if CUTOFF
	color.a = factor.a;
	#endif

	return color;
}

// Apply transition effect.
fixed4 ApplyTransitionEffect(fixed4 color, half3 transParam)
{
	fixed4 param = tex2D(_ParamTex, float2(0.25h, transParam.z));
	float alpha = tex2D(_TransitionTex, transParam.xy).a;

	#if REVERSE
    fixed effectFactor = 1.0h - param.x;
	#else
    fixed effectFactor = param.x;
	#endif

	#if FADE
	color.a *= saturate(alpha + (1.0h - effectFactor + effectFactor));
	#elif CUTOFF
	color.a *= step(0.001h, color.a * alpha - effectFactor);
	#elif DISSOLVE
    fixed width = param.y * 0.25h;
    fixed softness = 16.0h / param.z;
	fixed3 dissolveColor = tex2D(_ParamTex, float2(0.75h, transParam.z)).rgb;
	float factor = alpha - effectFactor * (1.0h + width) + width;
	fixed edgeLerp = step(factor, color.a) * saturate((width - factor) * softness);
	color = ApplyColorEffect(color, fixed4(dissolveColor, edgeLerp));
	color.a *= saturate(factor * (softness + softness));
	#endif

	return color;
}


// Apply shiny effect.
half4 ApplyShinyEffect(half4 color, half2 shinyParam)
{
	fixed4 param1 = tex2D(_ParamTex, float2(0.25h, shinyParam.y));
	fixed4 param2 = tex2D(_ParamTex, float2(0.75h, shinyParam.y));
	half location = (shinyParam.x - (param1.x + param1.x - 0.5h)) / param1.y;
	half normalized = 1.0h - saturate(abs(location));
	half shinePower = smoothstep(0.0h, param1.z, normalized) * 0.5h;
	half3 reflectColor = lerp((fixed3)1.0h, color.rgb * 7.0h, param2.x);

	color.rgb += color.a * shinePower * param1.w * reflectColor;

	return color;
}

half3 RgbToHsv(half3 c)
{
	half4 K = half4(0.0h, -0.333333h, 0.666666h, -1.0h);
	half4 p = lerp(half4(c.bg, K.wz), half4(c.gb, K.xy), step(c.b, c.g));
	half4 q = lerp(half4(p.xyw, c.r), half4(c.r, p.yzx), step(p.x, c.r));

	half d = q.x - min(q.w, q.y);
	half e = 1.0e-10;
	return half3(abs(q.z + (q.w - q.y) / (6.0h * d + e)), d / (q.x + e), q.x);
}

half3 HsvToRgb(half3 c) 
{
	c = half3(c.x, clamp(c.yz, 0.0h, 1.0h));
	half4 K = half4(1.0h, 0.66666h, 0.333333h, 3.0h);
	half3 p = abs(frac(c.xxx + K.xyz) * 6.0h - K.www);
	return c.z * lerp(K.xxx, clamp(p.xyz - K.xxx, 0.0h, 1.0h), c.y);
}

// Apply Hsv effect.
half4 ApplyHsvEffect(half4 color, half param)
{
	fixed4 param1 = tex2D(_ParamTex, float2(0.25h, param));
	fixed4 param2 = tex2D(_ParamTex, float2(0.75h, param));

	half3 hsv = RgbToHsv(color.rgb);
	half3 range = abs(hsv - param1.rgb);
	half diff = max(max(min(1.0h - range.x, range.x), min(1.0h - range.y, range.y) * 0.1h), min(1.0h - range.z, range.z) * 0.1h);

	fixed masked = step(diff, param1.w);
	color.rgb = HsvToRgb(hsv + (param2.xyz - 0.5h) * masked);

	return color;
}

#endif // UI_EFFECT_INCLUDED
