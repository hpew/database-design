using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using Sirenix.OdinInspector.Editor;
using CITG;
using CITG.System;
using CITG.SystemOptions;
using CITG.UI;
using CITG.Utility;

[CustomEditor(typeof(Option))]
public class OptionEditor : OdinEditor
{
    Option option;
    Dictionary<TaskType, Vector2> _scrolls;
    bool isShowTaskDescription = false;

    protected override void OnEnable()
    {
        base.OnEnable();
        option = target as Option;

        var type = option.GetType();
        var fieldInfo = type.GetField("_taskDescriptions", BindingFlags.Instance | BindingFlags.NonPublic);

        var taskDescriptions = fieldInfo.GetValue(option) as DictionaryTaskDescription;

        if (taskDescriptions == null || taskDescriptions.Count == 0)
        {
            taskDescriptions = new DictionaryTaskDescription()
            {
                [TaskType.First] = "",
                [TaskType.Second] = "",
                [TaskType.Third] = "",
                [TaskType.Fourth] = "",
            };

            fieldInfo.SetValue(option, taskDescriptions);
        }

        _scrolls = new Dictionary<TaskType, Vector2>()
        {
            [TaskType.First] = Vector2.zero,
            [TaskType.Second] = Vector2.zero,
            [TaskType.Third] = Vector2.zero,
            [TaskType.Fourth] = Vector2.zero,
        };
    }

    public override void OnInspectorGUI()
    {
        Undo.RecordObject(serializedObject.targetObject, "Option");

        base.OnInspectorGUI();

        isShowTaskDescription = EditorGUILayout.BeginFoldoutHeaderGroup(isShowTaskDescription, "Task Descriptions");

        if (isShowTaskDescription)
        {
            var type = option.GetType();
            var fieldInfo = type.GetField("_taskDescriptions", BindingFlags.Instance | BindingFlags.NonPublic);

            var taskDescriptions = fieldInfo.GetValue(option) as DictionaryTaskDescription;

            var couples = new List<Couple<TaskType, string>>();
            foreach (var pair in taskDescriptions)
                couples.Add(new Couple<TaskType, string>(pair.Key, pair.Value));

            var styleTextArea = new GUIStyle(EditorStyles.textArea);
            styleTextArea.wordWrap = true;

            EditorGUILayout.BeginVertical();
            for (int i = 0; i < couples.Count; i++)
            {
                var couple = couples[i];

                EditorGUILayout.LabelField($"{couple.FirstElement}");
                _scrolls[couple.FirstElement] = EditorGUILayout.BeginScrollView(_scrolls[couple.FirstElement],
                    GUILayout.Height(250));

                couple.SecondElement = EditorGUILayout.TextArea(couple.SecondElement, styleTextArea, GUILayout.ExpandHeight(true));

                EditorGUILayout.EndScrollView();

                taskDescriptions[couple.FirstElement] = couple.SecondElement;
            }

            fieldInfo.SetValue(option, taskDescriptions);
            EditorGUILayout.EndVertical();
        }

        EditorGUI.EndFoldoutHeaderGroup();

        EditorUtility.SetDirty(serializedObject.targetObject);
    }
}
