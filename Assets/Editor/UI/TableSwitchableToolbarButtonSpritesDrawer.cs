using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEngine;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.System;

namespace CITG.Editor
{
    using KeyValue = SpriteTableSwitchableToolbarButton.KeyValue;

    [CustomPropertyDrawer(typeof(SpriteTableSwitchableToolbarButton))]
    public class TableSwitchableToolbarButtonSpritesDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Undo.RecordObject(property.serializedObject.targetObject, "SpriteTableSwitchableToolbarButton");

            var findedTypes = Assembly.GetAssembly(typeof(SwitchableToolbarButton<>))
                .GetTypes()
                .Where(t => t.BaseType != null
                && t.BaseType.IsGenericType
                && t.BaseType.GetGenericTypeDefinition() == typeof(SwitchableToolbarButton<>)).ToList();

            var table = fieldInfo.GetValue(property.serializedObject.targetObject) as SpriteTableSwitchableToolbarButton;

            var listFieldInfo = fieldInfo.FieldType.GetField("_list", BindingFlags.Instance | BindingFlags.NonPublic);
            var list = listFieldInfo.GetValue(table) as List<KeyValue>;

            if (list == null)
            {
                Debug.LogWarning("TableSwitchableToolbarButtonSpritesDrawer List Is Null");

                list = new List<KeyValue>();
                foreach (var type in findedTypes)
                {
                    var keyValue = new KeyValue()
                    {
                        TypeName = type.Name,
                        Sprites = new SpritesSwitchableToolbarButton()
                    };

                    list.Add(keyValue);
                }

                listFieldInfo.SetValue(table, list);
            }
            else if (findedTypes.Count > list.Count)
            {
                foreach (var type in findedTypes)
                {
                    if (list.FindIndex(element => element.TypeName == type.Name) < 0)
                        list.Add(new KeyValue()
                        {
                            TypeName = type.Name,
                            Sprites = new SpritesSwitchableToolbarButton()
                        });
                }
            }
            else if (findedTypes.Count < list.Count)
            {
                for (int i = 0; i < list.Count; i++) 
                {
                    if (findedTypes.FindIndex(type => type.Name == list[i].TypeName) < 0)
                        list.Remove(list[i]);
                }
            }


            EditorGUI.BeginProperty(position, label, property);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var heightLine = base.GetPropertyHeight(property, label);

            EditorGUI.PrefixLabel(position,
                                 GUIUtility.GetControlID(FocusType.Passive),
                                 label);

            var widthColumnTypeName = position.width * .2f;
            var widthColumnSpriteField = position.width * .35f;

            var xColumnTypeName = position.x;
            var xColumnOnSprite = position.x + widthColumnTypeName + 10;
            var xColumnOffSprite = xColumnOnSprite + widthColumnSpriteField + 10;

            var yHeader = position.y + heightLine;

            var rectColumnTypeName = new Rect(xColumnTypeName, yHeader, widthColumnTypeName, heightLine);
            var rectColumnOnSprite = new Rect(xColumnOnSprite, yHeader, widthColumnSpriteField, heightLine);
            var rectColumnOffSprite = new Rect(xColumnOffSprite, yHeader, widthColumnSpriteField, heightLine);

            EditorGUI.LabelField(rectColumnTypeName, "Types");
            EditorGUI.LabelField(rectColumnOnSprite, "OnSprite");
            EditorGUI.LabelField(rectColumnOffSprite, "OffSprite");


            for (int i = 0; i < list.Count; i++)
            {
                var y = yHeader + heightLine * (i + 1);

                var rectTypeName = new Rect(xColumnTypeName, y, widthColumnTypeName, heightLine);

                EditorGUI.LabelField(rectTypeName, list[i].TypeName);

                var sprites = list[i].Sprites;

                var rectOnSprite = new Rect(xColumnOnSprite, y, widthColumnSpriteField, heightLine);
                sprites.OnSprite = EditorGUI.ObjectField(rectOnSprite, sprites.OnSprite, typeof(Sprite), false) as Sprite;

                var rectOffSprite = new Rect(xColumnOffSprite, y, widthColumnSpriteField, heightLine);
                sprites.OffSprite = EditorGUI.ObjectField(rectOffSprite, sprites.OffSprite, typeof(Sprite), false) as Sprite;

                list[i] = new KeyValue()
                {
                    TypeName = list[i].TypeName,
                    Sprites = sprites
                };
            }

            EditorGUI.EndProperty();
            EditorGUI.indentLevel = indent;

            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var table = fieldInfo.GetValue(property.serializedObject.targetObject) as SpriteTableSwitchableToolbarButton;

            var listFieldInfo = fieldInfo.FieldType.GetField("_list", BindingFlags.Instance | BindingFlags.NonPublic);
            var list = listFieldInfo.GetValue(table) as List<KeyValue>;

            var count = list != null ? list.Count : 0;

            return base.GetPropertyHeight(property, label) * 2 + base.GetPropertyHeight(property, label) * count;
        }
    }
}
