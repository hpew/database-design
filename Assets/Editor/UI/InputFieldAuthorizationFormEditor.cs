using UnityEditor;
using UnityEditor.UI;
using UnityEngine;

namespace CITG.Editor
{
    [CustomEditor(typeof(UI.Menu.InputFieldAuthorizationForm))]
    public class InputFieldAuthorizationFormEditor : InputFieldEditor
    {
        public override void OnInspectorGUI()
        {
            var warningLabel = serializedObject.FindProperty("_warningLabel");

            EditorGUILayout.PropertyField(warningLabel, new GUIContent("Warning Label"));

            var type = serializedObject.FindProperty("type");
            EditorGUILayout.PropertyField(type, new GUIContent("Type"));

            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();
        }
    }
}

