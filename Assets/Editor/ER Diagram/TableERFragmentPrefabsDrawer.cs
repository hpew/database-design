using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine.UIElements;
using UnityEngine;
using CITG.UI.MainScene.ToolbarSystem;
using CITG.System;
using CITG.ERDiagram;

namespace CITG.Editor
{
    using KeyValue = TableERFragmentPrefabs.KeyValue;

    [CustomPropertyDrawer(typeof(TableERFragmentPrefabs))]
    public class TableERFragmentPrefabsDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            Undo.RecordObject(property.serializedObject.targetObject, "TableERFragmentPrefabs");

            var findedTypes = Assembly.GetAssembly(typeof(ERFragment))
                .GetTypes()
                .Where(t => t.BaseType == typeof(ERFragment)).ToList();

            var table = fieldInfo.GetValue(property.serializedObject.targetObject) as TableERFragmentPrefabs;

            var listFieldInfo = fieldInfo.FieldType.GetField("_list", BindingFlags.Instance | BindingFlags.NonPublic);
            var list = listFieldInfo.GetValue(table) as List<KeyValue>;

            if (list == null)
            {
                Debug.LogWarning("TableERFragmentPrefabs List Is Null");

                list = new List<KeyValue>();
                foreach (var type in findedTypes)
                {
                    var keyValue = new KeyValue()
                    {
                        TypeName = type.Name,
                        Prefab = null
                    };

                    list.Add(keyValue);
                }

                listFieldInfo.SetValue(table, list);
            }
            else if (findedTypes.Count > list.Count)
            {
                foreach (var type in findedTypes)
                {
                    if (list.FindIndex(element => element.TypeName == type.Name) < 0)
                        list.Add(new KeyValue()
                        {
                            TypeName = type.Name,
                            Prefab = null
                        });
                }
            }
            else if (findedTypes.Count < list.Count)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    if (findedTypes.FindIndex(type => type.Name == list[i].TypeName) < 0)
                        list.Remove(list[i]);
                }
            }


            EditorGUI.BeginProperty(position, label, property);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            var heightLine = base.GetPropertyHeight(property, label);

            EditorGUI.PrefixLabel(position,
                                 GUIUtility.GetControlID(FocusType.Passive),
                                 label);

            var widthColumnTypeName = position.width * .2f;
            var widthColumnPrefab = position.width * .4f;

            var xColumnTypeName = position.x;
            var xColumnPrefab = position.x + widthColumnPrefab + 10;

            var yHeader = position.y + heightLine;

            var rectColumnTypeName = new Rect(xColumnTypeName, yHeader, widthColumnTypeName, heightLine);
            var rectColumnPrefab = new Rect(xColumnPrefab, yHeader, widthColumnPrefab, heightLine);

            EditorGUI.LabelField(rectColumnTypeName, "Types");
            EditorGUI.LabelField(rectColumnPrefab, "Prefab");


            for (int i = 0; i < list.Count; i++)
            {
                var y = yHeader + heightLine * (i + 1);

                var rectTypeName = new Rect(xColumnTypeName, y, widthColumnTypeName, heightLine);
                EditorGUI.LabelField(rectTypeName, list[i].TypeName);

                var prefab = list[i].Prefab;

                var rectPrefab = new Rect(xColumnPrefab, y, widthColumnPrefab, heightLine);
                prefab = EditorGUI.ObjectField(rectPrefab, prefab, typeof(ERFragment), false) as ERFragment;

                list[i] = new KeyValue()
                {
                    TypeName = list[i].TypeName,
                    Prefab = prefab
                };
            }

            EditorGUI.EndProperty();
            EditorGUI.indentLevel = indent;

            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            var table = fieldInfo.GetValue(property.serializedObject.targetObject) as TableERFragmentPrefabs;

            var listFieldInfo = fieldInfo.FieldType.GetField("_list", BindingFlags.Instance | BindingFlags.NonPublic);
            var list = listFieldInfo.GetValue(table) as List<KeyValue>;

            var count = list != null ? list.Count : 0;

            return base.GetPropertyHeight(property, label) * 2 + base.GetPropertyHeight(property, label) * count;
        }
    }
}