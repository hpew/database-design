﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct VirtActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5, typename T6>
struct VirtActionInvoker6
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, T6, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5, T6 p6)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, p6, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// System.Action`1<System.Single>
struct Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9;
// System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>
struct Action_2_tF65C920A10937B33C65584F87099045F42BBB100;
// System.Action`2<System.Object,System.Object>
struct Action_2_t4FB8E5660AE634E13BF340904C61FEA9DCE9D52D;
// System.Comparison`1<Coffee.UIEffects.GraphicConnector>
struct Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A;
// System.Comparison`1<System.Object>
struct Comparison_1_tB56E8E7C2BF431D44E8EBD15EA3E6F41AAFF03D2;
// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Object>
struct Dictionary_2_t1AB2593B33426A85D2383240510B43F23D5DC559;
// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>
struct Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.Type,Coffee.UIEffects.GraphicConnector>
struct Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Func`2<System.Object,System.String>
struct Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Hash128>
struct IEqualityComparer_1_tA9E1216E3D0E39599722188CA6BD4CA96ED9E60A;
// System.Collections.Generic.IEqualityComparer`1<System.Type>
struct IEqualityComparer_1_t7EEC9B4006D6D425748908D52AA799197F29A165;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>
struct KeyCollection_t2B75718A2C6F1DCF13F75B09E3A50FC7F6E5A4E5;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Type,Coffee.UIEffects.GraphicConnector>
struct KeyCollection_t4108B2D8647F45A2E9B84592800F5298FF66AC2B;
// System.Collections.Generic.List`1<System.Action>
struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235;
// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5;
// System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector>
struct List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<Coffee.UIEffects.UIShadow>
struct List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30;
// System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect>
struct List_1_t01BE5878AE01033101C05CE072C892141BBDA54F;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181;
// System.Collections.Generic.List`1<UnityEngine.Vector4>
struct List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>
struct ValueCollection_t86605E20D040D865E552BB3FC969F4F2825E4403;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Type,Coffee.UIEffects.GraphicConnector>
struct ValueCollection_tA09BE39EC1BD83F5D7081BF8F4605F5B85FDEDC2;
// System.Collections.Generic.Dictionary`2/Entry<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>[]
struct EntryU5BU5D_t00F7CBA84D6B0B37B779E98D2B3477878212BBA1;
// System.Collections.Generic.Dictionary`2/Entry<System.Type,Coffee.UIEffects.GraphicConnector>[]
struct EntryU5BU5D_tE1935C6D14917EEC6A5FF48718A00A5D1C6FF988;
// System.Action[]
struct ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// Coffee.UIEffects.GraphicConnector[]
struct GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// Coffee.UIEffects.UIShadow[]
struct UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186;
// Coffee.UIEffects.UISyncEffect[]
struct UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// Coffee.UIEffects.BaseMaterialEffect
struct BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065;
// Coffee.UIEffects.BaseMeshEffect
struct BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A;
// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Coffee.UIEffects.EffectPlayer
struct EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// Coffee.UIEffects.GraphicConnector
struct GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// Coffee.UIEffects.IParameterTexture
struct IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// Coffee.UIEffects.MaterialCache
struct MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// Coffee.UIEffects.ParameterTexture
struct ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// System.Type
struct Type_t;
// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E;
// Coffee.UIEffects.UIDissolve
struct UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A;
// Coffee.UIEffects.UIEffect
struct UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21;
// Coffee.UIEffects.UIFlip
struct UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D;
// Coffee.UIEffects.UIGradient
struct UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813;
// Coffee.UIEffects.UIHsvModifier
struct UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2;
// Coffee.UIEffects.UIShadow
struct UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB;
// Coffee.UIEffects.UIShiny
struct UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146;
// Coffee.UIEffects.UISyncEffect
struct UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E;
// Coffee.UIEffects.UITransitionEffect
struct UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// Coffee.UIEffects.BaseMaterialEffect/<>c
struct U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958;
// Coffee.UIEffects.EffectPlayer/<>c
struct U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715;
// Coffee.UIEffects.GraphicConnector/<>c
struct U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// Coffee.UIEffects.MaterialCache/MaterialEntry
struct MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7;
// Coffee.UIEffects.ParameterTexture/<>c
struct U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B;
// UnityEngine.RectTransform/ReapplyDrivenProperties
struct ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE;

IL2CPP_EXTERN_C RuntimeClass* Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_2_tF65C920A10937B33C65584F87099045F42BBB100_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BlurEx_tBF2C48944F19C9913EAFB3463909F0A84DE431B3_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* BlurMode_tF85C2C8ED8FC06036AA9CDF0AC0C29E56F48FBEC_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EffectMode_t81EABEB9816582A75850DECB66BA88C7C6BF1999_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EffectMode_tC89B96C68859C4857B80D2758E32C8303FCCD1E8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t01BE5878AE01033101C05CE072C892141BBDA54F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringBuilder_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral055F326E6AF2473ADEC1A21756F2574F6644221B;
IL2CPP_EXTERN_C String_t* _stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587;
IL2CPP_EXTERN_C String_t* _stringLiteral2786830AA9031CD84AE668243586F5295E0F9C48;
IL2CPP_EXTERN_C String_t* _stringLiteral2CB6662AD5190716C933E5698CA92FC65E715115;
IL2CPP_EXTERN_C String_t* _stringLiteral3B2C1C62D4D1C2A0C8A9AC42DB00D33C654F9AD0;
IL2CPP_EXTERN_C String_t* _stringLiteral8366508A811BDFAE9F6D4C8A797C52DC11DF47A1;
IL2CPP_EXTERN_C String_t* _stringLiteralB7BABBCFFC6BE004BD0E7D3897197B4FD50BF1ED;
IL2CPP_EXTERN_C String_t* _stringLiteralCAE03A8A39415A629D62FC97776BF380BC1C7564;
IL2CPP_EXTERN_C String_t* _stringLiteralD9276D4AAFC733CC809DC1F50EADEF80DE33D80D;
IL2CPP_EXTERN_C String_t* _stringLiteralF54472B8669373DE9BE5584699CBC7AEA9CEF091;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2_Invoke_mD347DED36015B897E49370E126492889CE4CEF2A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Action_2__ctor_m160CB1F6CF097F0AF0BD833BA54178EF5ADBA5F4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Comparison_1__ctor_mAA9CF9E41CC6F86684E8C44A7BFD13D42CDE86E4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponents_TisUIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_mE1D283405E85DB09AF57FA34A2F39D40B40FBC41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m3320410185FF9C49F194CC82EECEB845CF1FD719_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m8E8BC37A23AC889755C202C425E13B2F7189C501_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Remove_mC6318BC7A4B155ECA98F5CF06F61F51674000E07_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_TryGetValue_m8CA53842365EB034411824904D39811C29D7BE17_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m949E2D644B0A3B0F74E1FD5193BCD30878145AE0_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mD138DC514ABD671F73D4141EB5D75C9BBE6E788C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Concat_TisString_t_m7C95F30602BBABF6D41F782A16AE43477A957F3D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Distinct_TisString_t_m0980366EB945AB2A9F39B3AC0E03CF634F6CAA94_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Select_TisRuntimeObject_TisString_t_m540DAFC430ED92CA1E45E595E529F4A7CC4D7574_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_mDC3D53B081A01539860E8FEC7989D90D94D6071E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mF7B2CA3A45C72E5E1DBE6349EA4256E795251B5F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m26B8A870C49C4EBBEE8635F2B0848275E23E83DC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_m5DB0F7F826FFCB704F566795D5DA5A1F5ABD313D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m33273E2D2434DA63CFC046FA4864F2310CE5AAD3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mE8C9752EB54C57B7495AF3ED1F0E2AD106CED6C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Clear_m75F5B96DFF821D80AF54DCCBF40A4C66163B3EE7_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_GetEnumerator_mF09ECF08D0194AACE9E36B4310774C83E03873DB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m2FAEA539D1EBCFCF82B74CEA88321CA0D425646C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Remove_m905C6A86518BE8E31A16F5C7CAD8730B548CC554_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Sort_m395F5F609C162FDEF77CB38D6161676BE5CB0A62_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m2C641C171CDB35066D77221A29BF02745472353F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m30A66B91842A107D2965D350964CD92B2D41584D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m5EB82AEEC5544577504971655B371654980D6989_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ParameterTexture_UpdateParameterTexture_mF9DFFADF593F9954190F1161E8C2E117077B2B63_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CAddConnectorU3Eb__4_0_mC99D9E9096B78F00A3509748193374ACB23CBB91_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CInitializeU3Eb__16_0_mD35535E6182BFEEE7882780DE18A230F2611C9E9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3COnEnableU3Eb__7_0_m03524910EABBB0BB877A6F4343AADDF0B2C8B8D2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CSetShaderVariantsU3Eb__15_0_m86A0BD07D9513900697A9A21089709A5AE45B974_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec_U3CSetShaderVariantsU3Eb__15_1_mB4C34D6B741E7313073BF9AA37C633F2A40B3DB3_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UIDissolve_U3COnEnableU3Eb__54_0_m6FEE944A61838694BDB117ADC92E7733C84C07AF_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UIShiny_U3COnEnableU3Eb__37_0_mD8C5F5D76DC15E78E11D122BB45ABD3C6A94E89E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UITransitionEffect_U3CHideU3Eb__47_0_mF3CCECEBADA02DE02348B5455B049D700C33B4B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UITransitionEffect_U3CShowU3Eb__46_0_m57FEB42ADD3994430AF16F786501D9B18B5ED71A_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFF044342E9B6C8DF269362CF89A9DCD2DF079E5A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>
struct Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t00F7CBA84D6B0B37B779E98D2B3477878212BBA1* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t2B75718A2C6F1DCF13F75B09E3A50FC7F6E5A4E5 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t86605E20D040D865E552BB3FC969F4F2825E4403 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___entries_1)); }
	inline EntryU5BU5D_t00F7CBA84D6B0B37B779E98D2B3477878212BBA1* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t00F7CBA84D6B0B37B779E98D2B3477878212BBA1** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t00F7CBA84D6B0B37B779E98D2B3477878212BBA1* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___keys_7)); }
	inline KeyCollection_t2B75718A2C6F1DCF13F75B09E3A50FC7F6E5A4E5 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t2B75718A2C6F1DCF13F75B09E3A50FC7F6E5A4E5 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t2B75718A2C6F1DCF13F75B09E3A50FC7F6E5A4E5 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ___values_8)); }
	inline ValueCollection_t86605E20D040D865E552BB3FC969F4F2825E4403 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t86605E20D040D865E552BB3FC969F4F2825E4403 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t86605E20D040D865E552BB3FC969F4F2825E4403 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<System.Type,Coffee.UIEffects.GraphicConnector>
struct Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tE1935C6D14917EEC6A5FF48718A00A5D1C6FF988* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t4108B2D8647F45A2E9B84592800F5298FF66AC2B * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tA09BE39EC1BD83F5D7081BF8F4605F5B85FDEDC2 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___entries_1)); }
	inline EntryU5BU5D_tE1935C6D14917EEC6A5FF48718A00A5D1C6FF988* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tE1935C6D14917EEC6A5FF48718A00A5D1C6FF988** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tE1935C6D14917EEC6A5FF48718A00A5D1C6FF988* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___keys_7)); }
	inline KeyCollection_t4108B2D8647F45A2E9B84592800F5298FF66AC2B * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t4108B2D8647F45A2E9B84592800F5298FF66AC2B ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t4108B2D8647F45A2E9B84592800F5298FF66AC2B * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ___values_8)); }
	inline ValueCollection_tA09BE39EC1BD83F5D7081BF8F4605F5B85FDEDC2 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tA09BE39EC1BD83F5D7081BF8F4605F5B85FDEDC2 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tA09BE39EC1BD83F5D7081BF8F4605F5B85FDEDC2 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Action>
struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____items_1)); }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* get__items_1() const { return ____items_1; }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_StaticFields, ____emptyArray_5)); }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ActionU5BU5D_t4184CD78B103476FA93E685EDBF3C083DBA9E2C2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector>
struct List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9, ____items_1)); }
	inline GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF* get__items_1() const { return ____items_1; }
	inline GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9_StaticFields, ____emptyArray_5)); }
	inline GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF* get__emptyArray_5() const { return ____emptyArray_5; }
	inline GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(GraphicConnectorU5BU5D_tA5C233D96575825D417D62979C44822A1566C8CF* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Coffee.UIEffects.UIShadow>
struct List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30, ____items_1)); }
	inline UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186* get__items_1() const { return ____items_1; }
	inline UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30_StaticFields, ____emptyArray_5)); }
	inline UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UIShadowU5BU5D_t28C80DDCDC6B846B1C5915A8DD0E88289692C186* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect>
struct List_1_t01BE5878AE01033101C05CE072C892141BBDA54F  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F, ____items_1)); }
	inline UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6* get__items_1() const { return ____items_1; }
	inline UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t01BE5878AE01033101C05CE072C892141BBDA54F_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F_StaticFields, ____emptyArray_5)); }
	inline UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UISyncEffectU5BU5D_t108850CF086B7C05C95282C09C2168B2C02E91B6* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F, ____items_1)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get__items_1() const { return ____items_1; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_StaticFields, ____emptyArray_5)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55, ____array_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__array_0() const { return ____array_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// Coffee.UIEffects.GraphicConnector
struct GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F  : public RuntimeObject
{
public:

public:
};

struct GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields
{
public:
	// System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector> Coffee.UIEffects.GraphicConnector::s_Connectors
	List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * ___s_Connectors_0;
	// System.Collections.Generic.Dictionary`2<System.Type,Coffee.UIEffects.GraphicConnector> Coffee.UIEffects.GraphicConnector::s_ConnectorMap
	Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * ___s_ConnectorMap_1;
	// Coffee.UIEffects.GraphicConnector Coffee.UIEffects.GraphicConnector::s_EmptyConnector
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___s_EmptyConnector_2;

public:
	inline static int32_t get_offset_of_s_Connectors_0() { return static_cast<int32_t>(offsetof(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields, ___s_Connectors_0)); }
	inline List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * get_s_Connectors_0() const { return ___s_Connectors_0; }
	inline List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 ** get_address_of_s_Connectors_0() { return &___s_Connectors_0; }
	inline void set_s_Connectors_0(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * value)
	{
		___s_Connectors_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Connectors_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_ConnectorMap_1() { return static_cast<int32_t>(offsetof(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields, ___s_ConnectorMap_1)); }
	inline Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * get_s_ConnectorMap_1() const { return ___s_ConnectorMap_1; }
	inline Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B ** get_address_of_s_ConnectorMap_1() { return &___s_ConnectorMap_1; }
	inline void set_s_ConnectorMap_1(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * value)
	{
		___s_ConnectorMap_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ConnectorMap_1), (void*)value);
	}

	inline static int32_t get_offset_of_s_EmptyConnector_2() { return static_cast<int32_t>(offsetof(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields, ___s_EmptyConnector_2)); }
	inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * get_s_EmptyConnector_2() const { return ___s_EmptyConnector_2; }
	inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F ** get_address_of_s_EmptyConnector_2() { return &___s_EmptyConnector_2; }
	inline void set_s_EmptyConnector_2(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * value)
	{
		___s_EmptyConnector_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EmptyConnector_2), (void*)value);
	}
};


// Coffee.UIEffects.MaterialCache
struct MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4  : public RuntimeObject
{
public:

public:
};

struct MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry> Coffee.UIEffects.MaterialCache::materialMap
	Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * ___materialMap_0;

public:
	inline static int32_t get_offset_of_materialMap_0() { return static_cast<int32_t>(offsetof(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields, ___materialMap_0)); }
	inline Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * get_materialMap_0() const { return ___materialMap_0; }
	inline Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 ** get_address_of_materialMap_0() { return &___materialMap_0; }
	inline void set_materialMap_0(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * value)
	{
		___materialMap_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___materialMap_0), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// Packer
struct Packer_t306B7D1606732281C53D6EC7070031FAA96CB70D  : public RuntimeObject
{
public:

public:
};


// Coffee.UIEffects.ParameterTexture
struct ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1  : public RuntimeObject
{
public:
	// UnityEngine.Texture2D Coffee.UIEffects.ParameterTexture::_texture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ____texture_0;
	// System.Boolean Coffee.UIEffects.ParameterTexture::_needUpload
	bool ____needUpload_1;
	// System.Int32 Coffee.UIEffects.ParameterTexture::_propertyId
	int32_t ____propertyId_2;
	// System.String Coffee.UIEffects.ParameterTexture::_propertyName
	String_t* ____propertyName_3;
	// System.Int32 Coffee.UIEffects.ParameterTexture::_channels
	int32_t ____channels_4;
	// System.Int32 Coffee.UIEffects.ParameterTexture::_instanceLimit
	int32_t ____instanceLimit_5;
	// System.Byte[] Coffee.UIEffects.ParameterTexture::_data
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____data_6;
	// System.Collections.Generic.Stack`1<System.Int32> Coffee.UIEffects.ParameterTexture::_stack
	Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * ____stack_7;

public:
	inline static int32_t get_offset_of__texture_0() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____texture_0)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get__texture_0() const { return ____texture_0; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of__texture_0() { return &____texture_0; }
	inline void set__texture_0(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		____texture_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____texture_0), (void*)value);
	}

	inline static int32_t get_offset_of__needUpload_1() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____needUpload_1)); }
	inline bool get__needUpload_1() const { return ____needUpload_1; }
	inline bool* get_address_of__needUpload_1() { return &____needUpload_1; }
	inline void set__needUpload_1(bool value)
	{
		____needUpload_1 = value;
	}

	inline static int32_t get_offset_of__propertyId_2() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____propertyId_2)); }
	inline int32_t get__propertyId_2() const { return ____propertyId_2; }
	inline int32_t* get_address_of__propertyId_2() { return &____propertyId_2; }
	inline void set__propertyId_2(int32_t value)
	{
		____propertyId_2 = value;
	}

	inline static int32_t get_offset_of__propertyName_3() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____propertyName_3)); }
	inline String_t* get__propertyName_3() const { return ____propertyName_3; }
	inline String_t** get_address_of__propertyName_3() { return &____propertyName_3; }
	inline void set__propertyName_3(String_t* value)
	{
		____propertyName_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____propertyName_3), (void*)value);
	}

	inline static int32_t get_offset_of__channels_4() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____channels_4)); }
	inline int32_t get__channels_4() const { return ____channels_4; }
	inline int32_t* get_address_of__channels_4() { return &____channels_4; }
	inline void set__channels_4(int32_t value)
	{
		____channels_4 = value;
	}

	inline static int32_t get_offset_of__instanceLimit_5() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____instanceLimit_5)); }
	inline int32_t get__instanceLimit_5() const { return ____instanceLimit_5; }
	inline int32_t* get_address_of__instanceLimit_5() { return &____instanceLimit_5; }
	inline void set__instanceLimit_5(int32_t value)
	{
		____instanceLimit_5 = value;
	}

	inline static int32_t get_offset_of__data_6() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____data_6)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__data_6() const { return ____data_6; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__data_6() { return &____data_6; }
	inline void set__data_6(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____data_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_6), (void*)value);
	}

	inline static int32_t get_offset_of__stack_7() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1, ____stack_7)); }
	inline Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * get__stack_7() const { return ____stack_7; }
	inline Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 ** get_address_of__stack_7() { return &____stack_7; }
	inline void set__stack_7(Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * value)
	{
		____stack_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stack_7), (void*)value);
	}
};

struct ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Action> Coffee.UIEffects.ParameterTexture::updates
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___updates_8;

public:
	inline static int32_t get_offset_of_updates_8() { return static_cast<int32_t>(offsetof(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields, ___updates_8)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_updates_8() const { return ___updates_8; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_updates_8() { return &___updates_8; }
	inline void set_updates_8(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___updates_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___updates_8), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.Text.StringBuilder
struct StringBuilder_t  : public RuntimeObject
{
public:
	// System.Char[] System.Text.StringBuilder::m_ChunkChars
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_ChunkChars_0;
	// System.Text.StringBuilder System.Text.StringBuilder::m_ChunkPrevious
	StringBuilder_t * ___m_ChunkPrevious_1;
	// System.Int32 System.Text.StringBuilder::m_ChunkLength
	int32_t ___m_ChunkLength_2;
	// System.Int32 System.Text.StringBuilder::m_ChunkOffset
	int32_t ___m_ChunkOffset_3;
	// System.Int32 System.Text.StringBuilder::m_MaxCapacity
	int32_t ___m_MaxCapacity_4;

public:
	inline static int32_t get_offset_of_m_ChunkChars_0() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkChars_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_ChunkChars_0() const { return ___m_ChunkChars_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_ChunkChars_0() { return &___m_ChunkChars_0; }
	inline void set_m_ChunkChars_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_ChunkChars_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkChars_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkPrevious_1() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkPrevious_1)); }
	inline StringBuilder_t * get_m_ChunkPrevious_1() const { return ___m_ChunkPrevious_1; }
	inline StringBuilder_t ** get_address_of_m_ChunkPrevious_1() { return &___m_ChunkPrevious_1; }
	inline void set_m_ChunkPrevious_1(StringBuilder_t * value)
	{
		___m_ChunkPrevious_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ChunkPrevious_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_ChunkLength_2() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkLength_2)); }
	inline int32_t get_m_ChunkLength_2() const { return ___m_ChunkLength_2; }
	inline int32_t* get_address_of_m_ChunkLength_2() { return &___m_ChunkLength_2; }
	inline void set_m_ChunkLength_2(int32_t value)
	{
		___m_ChunkLength_2 = value;
	}

	inline static int32_t get_offset_of_m_ChunkOffset_3() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_ChunkOffset_3)); }
	inline int32_t get_m_ChunkOffset_3() const { return ___m_ChunkOffset_3; }
	inline int32_t* get_address_of_m_ChunkOffset_3() { return &___m_ChunkOffset_3; }
	inline void set_m_ChunkOffset_3(int32_t value)
	{
		___m_ChunkOffset_3 = value;
	}

	inline static int32_t get_offset_of_m_MaxCapacity_4() { return static_cast<int32_t>(offsetof(StringBuilder_t, ___m_MaxCapacity_4)); }
	inline int32_t get_m_MaxCapacity_4() const { return ___m_MaxCapacity_4; }
	inline int32_t* get_address_of_m_MaxCapacity_4() { return &___m_MaxCapacity_4; }
	inline void set_m_MaxCapacity_4(int32_t value)
	{
		___m_MaxCapacity_4 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// Coffee.UIEffects.BaseMaterialEffect/<>c
struct U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields
{
public:
	// Coffee.UIEffects.BaseMaterialEffect/<>c Coffee.UIEffects.BaseMaterialEffect/<>c::<>9
	U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Boolean> Coffee.UIEffects.BaseMaterialEffect/<>c::<>9__15_0
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___U3CU3E9__15_0_1;
	// System.Func`2<System.Object,System.String> Coffee.UIEffects.BaseMaterialEffect/<>c::<>9__15_1
	Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * ___U3CU3E9__15_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields, ___U3CU3E9__15_0_1)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_U3CU3E9__15_0_1() const { return ___U3CU3E9__15_0_1; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_U3CU3E9__15_0_1() { return &___U3CU3E9__15_0_1; }
	inline void set_U3CU3E9__15_0_1(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___U3CU3E9__15_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_0_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__15_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields, ___U3CU3E9__15_1_2)); }
	inline Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * get_U3CU3E9__15_1_2() const { return ___U3CU3E9__15_1_2; }
	inline Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 ** get_address_of_U3CU3E9__15_1_2() { return &___U3CU3E9__15_1_2; }
	inline void set_U3CU3E9__15_1_2(Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * value)
	{
		___U3CU3E9__15_1_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__15_1_2), (void*)value);
	}
};


// Coffee.UIEffects.EffectPlayer/<>c
struct U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields
{
public:
	// Coffee.UIEffects.EffectPlayer/<>c Coffee.UIEffects.EffectPlayer/<>c::<>9
	U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * ___U3CU3E9_0;
	// UnityEngine.Canvas/WillRenderCanvases Coffee.UIEffects.EffectPlayer/<>c::<>9__7_0
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields, ___U3CU3E9__7_0_1)); }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__7_0_1), (void*)value);
	}
};


// Coffee.UIEffects.GraphicConnector/<>c
struct U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields
{
public:
	// Coffee.UIEffects.GraphicConnector/<>c Coffee.UIEffects.GraphicConnector/<>c::<>9
	U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * ___U3CU3E9_0;
	// System.Comparison`1<Coffee.UIEffects.GraphicConnector> Coffee.UIEffects.GraphicConnector/<>c::<>9__4_0
	Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__4_0_1), (void*)value);
	}
};


// Coffee.UIEffects.MaterialCache/MaterialEntry
struct MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7  : public RuntimeObject
{
public:
	// UnityEngine.Material Coffee.UIEffects.MaterialCache/MaterialEntry::material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material_0;
	// System.Int32 Coffee.UIEffects.MaterialCache/MaterialEntry::referenceCount
	int32_t ___referenceCount_1;

public:
	inline static int32_t get_offset_of_material_0() { return static_cast<int32_t>(offsetof(MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7, ___material_0)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_material_0() const { return ___material_0; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_material_0() { return &___material_0; }
	inline void set_material_0(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___material_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___material_0), (void*)value);
	}

	inline static int32_t get_offset_of_referenceCount_1() { return static_cast<int32_t>(offsetof(MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7, ___referenceCount_1)); }
	inline int32_t get_referenceCount_1() const { return ___referenceCount_1; }
	inline int32_t* get_address_of_referenceCount_1() { return &___referenceCount_1; }
	inline void set_referenceCount_1(int32_t value)
	{
		___referenceCount_1 = value;
	}
};


// Coffee.UIEffects.ParameterTexture/<>c
struct U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields
{
public:
	// Coffee.UIEffects.ParameterTexture/<>c Coffee.UIEffects.ParameterTexture/<>c::<>9
	U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * ___U3CU3E9_0;
	// UnityEngine.Canvas/WillRenderCanvases Coffee.UIEffects.ParameterTexture/<>c::<>9__16_0
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___U3CU3E9__16_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields, ___U3CU3E9__16_0_1)); }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__16_0_1), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.GraphicConnector>
struct Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63, ___list_0)); }
	inline List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * get_list_0() const { return ___list_0; }
	inline List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63, ___current_3)); }
	inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * get_current_3() const { return ___current_3; }
	inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<System.Object>
struct Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UIShadow>
struct Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361, ___list_0)); }
	inline List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * get_list_0() const { return ___list_0; }
	inline List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361, ___current_3)); }
	inline UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * get_current_3() const { return ___current_3; }
	inline UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UISyncEffect>
struct Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B, ___list_0)); }
	inline List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * get_list_0() const { return ___list_0; }
	inline List_1_t01BE5878AE01033101C05CE072C892141BBDA54F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B, ___current_3)); }
	inline UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * get_current_3() const { return ___current_3; }
	inline UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.Color32
struct Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.Hash128
struct Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A 
{
public:
	// System.UInt32 UnityEngine.Hash128::m_u32_0
	uint32_t ___m_u32_0_0;
	// System.UInt32 UnityEngine.Hash128::m_u32_1
	uint32_t ___m_u32_1_1;
	// System.UInt32 UnityEngine.Hash128::m_u32_2
	uint32_t ___m_u32_2_2;
	// System.UInt32 UnityEngine.Hash128::m_u32_3
	uint32_t ___m_u32_3_3;

public:
	inline static int32_t get_offset_of_m_u32_0_0() { return static_cast<int32_t>(offsetof(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A, ___m_u32_0_0)); }
	inline uint32_t get_m_u32_0_0() const { return ___m_u32_0_0; }
	inline uint32_t* get_address_of_m_u32_0_0() { return &___m_u32_0_0; }
	inline void set_m_u32_0_0(uint32_t value)
	{
		___m_u32_0_0 = value;
	}

	inline static int32_t get_offset_of_m_u32_1_1() { return static_cast<int32_t>(offsetof(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A, ___m_u32_1_1)); }
	inline uint32_t get_m_u32_1_1() const { return ___m_u32_1_1; }
	inline uint32_t* get_address_of_m_u32_1_1() { return &___m_u32_1_1; }
	inline void set_m_u32_1_1(uint32_t value)
	{
		___m_u32_1_1 = value;
	}

	inline static int32_t get_offset_of_m_u32_2_2() { return static_cast<int32_t>(offsetof(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A, ___m_u32_2_2)); }
	inline uint32_t get_m_u32_2_2() const { return ___m_u32_2_2; }
	inline uint32_t* get_address_of_m_u32_2_2() { return &___m_u32_2_2; }
	inline void set_m_u32_2_2(uint32_t value)
	{
		___m_u32_2_2 = value;
	}

	inline static int32_t get_offset_of_m_u32_3_3() { return static_cast<int32_t>(offsetof(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A, ___m_u32_3_3)); }
	inline uint32_t get_m_u32_3_3() const { return ___m_u32_3_3; }
	inline uint32_t* get_address_of_m_u32_3_3() { return &___m_u32_3_3; }
	inline void set_m_u32_3_3(uint32_t value)
	{
		___m_u32_3_3 = value;
	}
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// Coffee.UIEffects.Matrix2x3
struct Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED 
{
public:
	// System.Single Coffee.UIEffects.Matrix2x3::m00
	float ___m00_0;
	// System.Single Coffee.UIEffects.Matrix2x3::m01
	float ___m01_1;
	// System.Single Coffee.UIEffects.Matrix2x3::m02
	float ___m02_2;
	// System.Single Coffee.UIEffects.Matrix2x3::m10
	float ___m10_3;
	// System.Single Coffee.UIEffects.Matrix2x3::m11
	float ___m11_4;
	// System.Single Coffee.UIEffects.Matrix2x3::m12
	float ___m12_5;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m01_1() { return static_cast<int32_t>(offsetof(Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED, ___m01_1)); }
	inline float get_m01_1() const { return ___m01_1; }
	inline float* get_address_of_m01_1() { return &___m01_1; }
	inline void set_m01_1(float value)
	{
		___m01_1 = value;
	}

	inline static int32_t get_offset_of_m02_2() { return static_cast<int32_t>(offsetof(Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED, ___m02_2)); }
	inline float get_m02_2() const { return ___m02_2; }
	inline float* get_address_of_m02_2() { return &___m02_2; }
	inline void set_m02_2(float value)
	{
		___m02_2 = value;
	}

	inline static int32_t get_offset_of_m10_3() { return static_cast<int32_t>(offsetof(Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED, ___m10_3)); }
	inline float get_m10_3() const { return ___m10_3; }
	inline float* get_address_of_m10_3() { return &___m10_3; }
	inline void set_m10_3(float value)
	{
		___m10_3 = value;
	}

	inline static int32_t get_offset_of_m11_4() { return static_cast<int32_t>(offsetof(Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED, ___m11_4)); }
	inline float get_m11_4() const { return ___m11_4; }
	inline float* get_address_of_m11_4() { return &___m11_4; }
	inline void set_m11_4(float value)
	{
		___m11_4 = value;
	}

	inline static int32_t get_offset_of_m12_5() { return static_cast<int32_t>(offsetof(Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED, ___m12_5)); }
	inline float get_m12_5() const { return ___m12_5; }
	inline float* get_address_of_m12_5() { return &___m12_5; }
	inline void set_m12_5(float value)
	{
		___m12_5 = value;
	}
};


// UnityEngine.Rect
struct Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt32
struct UInt32_tE60352A06233E4E69DD198BCC67142159F686B15 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_tE60352A06233E4E69DD198BCC67142159F686B15, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.AdditionalCanvasShaderChannels
struct AdditionalCanvasShaderChannels_t72A9ACBEE2E5AB5834D5F978421028757954396C 
{
public:
	// System.Int32 UnityEngine.AdditionalCanvasShaderChannels::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AdditionalCanvasShaderChannels_t72A9ACBEE2E5AB5834D5F978421028757954396C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.AnimatorUpdateMode
struct AnimatorUpdateMode_t5E08EEA8E26DAB1765A82EAD782A0E047920B439 
{
public:
	// System.Int32 UnityEngine.AnimatorUpdateMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AnimatorUpdateMode_t5E08EEA8E26DAB1765A82EAD782A0E047920B439, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Coffee.UIEffects.BlurMode
struct BlurMode_tF85C2C8ED8FC06036AA9CDF0AC0C29E56F48FBEC 
{
public:
	// System.Int32 Coffee.UIEffects.BlurMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlurMode_tF85C2C8ED8FC06036AA9CDF0AC0C29E56F48FBEC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Coffee.UIEffects.ColorMode
struct ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D 
{
public:
	// System.Int32 Coffee.UIEffects.ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.ColorSpace
struct ColorSpace_tAD694F94295170CB332A0F99BBE086F4AC8C15BA 
{
public:
	// System.Int32 UnityEngine.ColorSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorSpace_tAD694F94295170CB332A0F99BBE086F4AC8C15BA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Coffee.UIEffects.EffectArea
struct EffectArea_tD3F8EEEB6D0067386B4A25C1BDDD1E45BA9F5C37 
{
public:
	// System.Int32 Coffee.UIEffects.EffectArea::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectArea_tD3F8EEEB6D0067386B4A25C1BDDD1E45BA9F5C37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Coffee.UIEffects.EffectAreaExtensions
struct EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B  : public RuntimeObject
{
public:

public:
};

struct EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields
{
public:
	// UnityEngine.Rect Coffee.UIEffects.EffectAreaExtensions::rectForCharacter
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rectForCharacter_0;
	// UnityEngine.Vector2[] Coffee.UIEffects.EffectAreaExtensions::splitedCharacterPosition
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___splitedCharacterPosition_1;

public:
	inline static int32_t get_offset_of_rectForCharacter_0() { return static_cast<int32_t>(offsetof(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields, ___rectForCharacter_0)); }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  get_rectForCharacter_0() const { return ___rectForCharacter_0; }
	inline Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * get_address_of_rectForCharacter_0() { return &___rectForCharacter_0; }
	inline void set_rectForCharacter_0(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  value)
	{
		___rectForCharacter_0 = value;
	}

	inline static int32_t get_offset_of_splitedCharacterPosition_1() { return static_cast<int32_t>(offsetof(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields, ___splitedCharacterPosition_1)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_splitedCharacterPosition_1() const { return ___splitedCharacterPosition_1; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_splitedCharacterPosition_1() { return &___splitedCharacterPosition_1; }
	inline void set_splitedCharacterPosition_1(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___splitedCharacterPosition_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___splitedCharacterPosition_1), (void*)value);
	}
};


// Coffee.UIEffects.EffectMode
struct EffectMode_tC89B96C68859C4857B80D2758E32C8303FCCD1E8 
{
public:
	// System.Int32 Coffee.UIEffects.EffectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectMode_tC89B96C68859C4857B80D2758E32C8303FCCD1E8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.FilterMode
struct FilterMode_tE90A08FD96A142C761463D65E524BCDBFEEE3D19 
{
public:
	// System.Int32 UnityEngine.FilterMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterMode_tE90A08FD96A142C761463D65E524BCDBFEEE3D19, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HideFlags
struct HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_tDC64149E37544FF83B2B4222D3E9DC8188766A12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Coffee.UIEffects.ShadowStyle
struct ShadowStyle_t728299F8C4055199FB08EA2C7C1D990108881B9A 
{
public:
	// System.Int32 Coffee.UIEffects.ShadowStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShadowStyle_t728299F8C4055199FB08EA2C7C1D990108881B9A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_tBED5388A0445FE978F97B41D247275B036407932 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_tBED5388A0445FE978F97B41D247275B036407932, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.TextureWrapMode
struct TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureWrapMode_t86DDA8206E4AA784A1218D0DE3C5F6826D7549EB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UIVertex
struct UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A 
{
public:
	// UnityEngine.Vector3 UnityEngine.UIVertex::position
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position_0;
	// UnityEngine.Vector3 UnityEngine.UIVertex::normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___normal_1;
	// UnityEngine.Vector4 UnityEngine.UIVertex::tangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___tangent_2;
	// UnityEngine.Color32 UnityEngine.UIVertex::color
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___color_3;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv0
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___uv0_4;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv1
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___uv1_5;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv2
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___uv2_6;
	// UnityEngine.Vector4 UnityEngine.UIVertex::uv3
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___uv3_7;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___position_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_position_0() const { return ___position_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_normal_1() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_normal_1() const { return ___normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_normal_1() { return &___normal_1; }
	inline void set_normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___normal_1 = value;
	}

	inline static int32_t get_offset_of_tangent_2() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___tangent_2)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_tangent_2() const { return ___tangent_2; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_tangent_2() { return &___tangent_2; }
	inline void set_tangent_2(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___tangent_2 = value;
	}

	inline static int32_t get_offset_of_color_3() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___color_3)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_color_3() const { return ___color_3; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_color_3() { return &___color_3; }
	inline void set_color_3(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___color_3 = value;
	}

	inline static int32_t get_offset_of_uv0_4() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___uv0_4)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_uv0_4() const { return ___uv0_4; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_uv0_4() { return &___uv0_4; }
	inline void set_uv0_4(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___uv0_4 = value;
	}

	inline static int32_t get_offset_of_uv1_5() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___uv1_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_uv1_5() const { return ___uv1_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_uv1_5() { return &___uv1_5; }
	inline void set_uv1_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___uv1_5 = value;
	}

	inline static int32_t get_offset_of_uv2_6() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___uv2_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_uv2_6() const { return ___uv2_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_uv2_6() { return &___uv2_6; }
	inline void set_uv2_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___uv2_6 = value;
	}

	inline static int32_t get_offset_of_uv3_7() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A, ___uv3_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_uv3_7() const { return ___uv3_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_uv3_7() { return &___uv3_7; }
	inline void set_uv3_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___uv3_7 = value;
	}
};

struct UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A_StaticFields
{
public:
	// UnityEngine.Color32 UnityEngine.UIVertex::s_DefaultColor
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___s_DefaultColor_8;
	// UnityEngine.Vector4 UnityEngine.UIVertex::s_DefaultTangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___s_DefaultTangent_9;
	// UnityEngine.UIVertex UnityEngine.UIVertex::simpleVert
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  ___simpleVert_10;

public:
	inline static int32_t get_offset_of_s_DefaultColor_8() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A_StaticFields, ___s_DefaultColor_8)); }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  get_s_DefaultColor_8() const { return ___s_DefaultColor_8; }
	inline Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * get_address_of_s_DefaultColor_8() { return &___s_DefaultColor_8; }
	inline void set_s_DefaultColor_8(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  value)
	{
		___s_DefaultColor_8 = value;
	}

	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_simpleVert_10() { return static_cast<int32_t>(offsetof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A_StaticFields, ___simpleVert_10)); }
	inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  get_simpleVert_10() const { return ___simpleVert_10; }
	inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * get_address_of_simpleVert_10() { return &___simpleVert_10; }
	inline void set_simpleVert_10(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  value)
	{
		___simpleVert_10 = value;
	}
};


// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Positions
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Positions_0;
	// System.Collections.Generic.List`1<UnityEngine.Color32> UnityEngine.UI.VertexHelper::m_Colors
	List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * ___m_Colors_1;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv0S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv0S_2;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv1S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv1S_3;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv2S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv2S_4;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Uv3S
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Uv3S_5;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> UnityEngine.UI.VertexHelper::m_Normals
	List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * ___m_Normals_6;
	// System.Collections.Generic.List`1<UnityEngine.Vector4> UnityEngine.UI.VertexHelper::m_Tangents
	List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * ___m_Tangents_7;
	// System.Collections.Generic.List`1<System.Int32> UnityEngine.UI.VertexHelper::m_Indices
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___m_Indices_8;
	// System.Boolean UnityEngine.UI.VertexHelper::m_ListsInitalized
	bool ___m_ListsInitalized_11;

public:
	inline static int32_t get_offset_of_m_Positions_0() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Positions_0)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Positions_0() const { return ___m_Positions_0; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Positions_0() { return &___m_Positions_0; }
	inline void set_m_Positions_0(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Positions_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Positions_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Colors_1() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Colors_1)); }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * get_m_Colors_1() const { return ___m_Colors_1; }
	inline List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 ** get_address_of_m_Colors_1() { return &___m_Colors_1; }
	inline void set_m_Colors_1(List_1_tE21C42BE31D35DD3ECF3322C6CA057E27A81B4D5 * value)
	{
		___m_Colors_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Colors_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv0S_2() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv0S_2)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv0S_2() const { return ___m_Uv0S_2; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv0S_2() { return &___m_Uv0S_2; }
	inline void set_m_Uv0S_2(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv0S_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv0S_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv1S_3() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv1S_3)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv1S_3() const { return ___m_Uv1S_3; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv1S_3() { return &___m_Uv1S_3; }
	inline void set_m_Uv1S_3(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv1S_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv1S_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv2S_4() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv2S_4)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv2S_4() const { return ___m_Uv2S_4; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv2S_4() { return &___m_Uv2S_4; }
	inline void set_m_Uv2S_4(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv2S_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv2S_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Uv3S_5() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Uv3S_5)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Uv3S_5() const { return ___m_Uv3S_5; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Uv3S_5() { return &___m_Uv3S_5; }
	inline void set_m_Uv3S_5(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Uv3S_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Uv3S_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_Normals_6() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Normals_6)); }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * get_m_Normals_6() const { return ___m_Normals_6; }
	inline List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 ** get_address_of_m_Normals_6() { return &___m_Normals_6; }
	inline void set_m_Normals_6(List_1_t577D28CFF6DFE3F6A8D4409F7A21CBF513C04181 * value)
	{
		___m_Normals_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Normals_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Tangents_7() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Tangents_7)); }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * get_m_Tangents_7() const { return ___m_Tangents_7; }
	inline List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A ** get_address_of_m_Tangents_7() { return &___m_Tangents_7; }
	inline void set_m_Tangents_7(List_1_t14D5F8426BD7087A7AEB49D4DE3DEF404C8BE65A * value)
	{
		___m_Tangents_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Tangents_7), (void*)value);
	}

	inline static int32_t get_offset_of_m_Indices_8() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_Indices_8)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_m_Indices_8() const { return ___m_Indices_8; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_m_Indices_8() { return &___m_Indices_8; }
	inline void set_m_Indices_8(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___m_Indices_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Indices_8), (void*)value);
	}

	inline static int32_t get_offset_of_m_ListsInitalized_11() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55, ___m_ListsInitalized_11)); }
	inline bool get_m_ListsInitalized_11() const { return ___m_ListsInitalized_11; }
	inline bool* get_address_of_m_ListsInitalized_11() { return &___m_ListsInitalized_11; }
	inline void set_m_ListsInitalized_11(bool value)
	{
		___m_ListsInitalized_11 = value;
	}
};

struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.UI.VertexHelper::s_DefaultTangent
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___s_DefaultTangent_9;
	// UnityEngine.Vector3 UnityEngine.UI.VertexHelper::s_DefaultNormal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___s_DefaultNormal_10;

public:
	inline static int32_t get_offset_of_s_DefaultTangent_9() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultTangent_9)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_s_DefaultTangent_9() const { return ___s_DefaultTangent_9; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_s_DefaultTangent_9() { return &___s_DefaultTangent_9; }
	inline void set_s_DefaultTangent_9(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___s_DefaultTangent_9 = value;
	}

	inline static int32_t get_offset_of_s_DefaultNormal_10() { return static_cast<int32_t>(offsetof(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55_StaticFields, ___s_DefaultNormal_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_s_DefaultNormal_10() const { return ___s_DefaultNormal_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_s_DefaultNormal_10() { return &___s_DefaultNormal_10; }
	inline void set_s_DefaultNormal_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___s_DefaultNormal_10 = value;
	}
};


// Coffee.UIEffects.UIEffect/BlurEx
struct BlurEx_tBF2C48944F19C9913EAFB3463909F0A84DE431B3 
{
public:
	// System.Int32 Coffee.UIEffects.UIEffect/BlurEx::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlurEx_tBF2C48944F19C9913EAFB3463909F0A84DE431B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Coffee.UIEffects.UIGradient/Direction
struct Direction_tB22B6E22625C7A14093ED58DA5EA3CB4870B82FE 
{
public:
	// System.Int32 Coffee.UIEffects.UIGradient/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tB22B6E22625C7A14093ED58DA5EA3CB4870B82FE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Coffee.UIEffects.UIGradient/GradientStyle
struct GradientStyle_t8A6BB2B5251D4B0FA5978FF104329F3044DDB338 
{
public:
	// System.Int32 Coffee.UIEffects.UIGradient/GradientStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GradientStyle_t8A6BB2B5251D4B0FA5978FF104329F3044DDB338, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Coffee.UIEffects.UITransitionEffect/EffectMode
struct EffectMode_t81EABEB9816582A75850DECB66BA88C7C6BF1999 
{
public:
	// System.Int32 Coffee.UIEffects.UITransitionEffect/EffectMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EffectMode_t81EABEB9816582A75850DECB66BA88C7C6BF1999, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// Coffee.UIEffects.EffectPlayer
struct EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C  : public RuntimeObject
{
public:
	// System.Boolean Coffee.UIEffects.EffectPlayer::play
	bool ___play_0;
	// System.Single Coffee.UIEffects.EffectPlayer::initialPlayDelay
	float ___initialPlayDelay_1;
	// System.Single Coffee.UIEffects.EffectPlayer::duration
	float ___duration_2;
	// System.Boolean Coffee.UIEffects.EffectPlayer::loop
	bool ___loop_3;
	// System.Single Coffee.UIEffects.EffectPlayer::loopDelay
	float ___loopDelay_4;
	// UnityEngine.AnimatorUpdateMode Coffee.UIEffects.EffectPlayer::updateMode
	int32_t ___updateMode_5;
	// System.Single Coffee.UIEffects.EffectPlayer::_time
	float ____time_7;
	// System.Action`1<System.Single> Coffee.UIEffects.EffectPlayer::_callback
	Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ____callback_8;

public:
	inline static int32_t get_offset_of_play_0() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ___play_0)); }
	inline bool get_play_0() const { return ___play_0; }
	inline bool* get_address_of_play_0() { return &___play_0; }
	inline void set_play_0(bool value)
	{
		___play_0 = value;
	}

	inline static int32_t get_offset_of_initialPlayDelay_1() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ___initialPlayDelay_1)); }
	inline float get_initialPlayDelay_1() const { return ___initialPlayDelay_1; }
	inline float* get_address_of_initialPlayDelay_1() { return &___initialPlayDelay_1; }
	inline void set_initialPlayDelay_1(float value)
	{
		___initialPlayDelay_1 = value;
	}

	inline static int32_t get_offset_of_duration_2() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ___duration_2)); }
	inline float get_duration_2() const { return ___duration_2; }
	inline float* get_address_of_duration_2() { return &___duration_2; }
	inline void set_duration_2(float value)
	{
		___duration_2 = value;
	}

	inline static int32_t get_offset_of_loop_3() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ___loop_3)); }
	inline bool get_loop_3() const { return ___loop_3; }
	inline bool* get_address_of_loop_3() { return &___loop_3; }
	inline void set_loop_3(bool value)
	{
		___loop_3 = value;
	}

	inline static int32_t get_offset_of_loopDelay_4() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ___loopDelay_4)); }
	inline float get_loopDelay_4() const { return ___loopDelay_4; }
	inline float* get_address_of_loopDelay_4() { return &___loopDelay_4; }
	inline void set_loopDelay_4(float value)
	{
		___loopDelay_4 = value;
	}

	inline static int32_t get_offset_of_updateMode_5() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ___updateMode_5)); }
	inline int32_t get_updateMode_5() const { return ___updateMode_5; }
	inline int32_t* get_address_of_updateMode_5() { return &___updateMode_5; }
	inline void set_updateMode_5(int32_t value)
	{
		___updateMode_5 = value;
	}

	inline static int32_t get_offset_of__time_7() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ____time_7)); }
	inline float get__time_7() const { return ____time_7; }
	inline float* get_address_of__time_7() { return &____time_7; }
	inline void set__time_7(float value)
	{
		____time_7 = value;
	}

	inline static int32_t get_offset_of__callback_8() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C, ____callback_8)); }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * get__callback_8() const { return ____callback_8; }
	inline Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 ** get_address_of__callback_8() { return &____callback_8; }
	inline void set__callback_8(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * value)
	{
		____callback_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____callback_8), (void*)value);
	}
};

struct EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields
{
public:
	// System.Collections.Generic.List`1<System.Action> Coffee.UIEffects.EffectPlayer::s_UpdateActions
	List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * ___s_UpdateActions_6;

public:
	inline static int32_t get_offset_of_s_UpdateActions_6() { return static_cast<int32_t>(offsetof(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields, ___s_UpdateActions_6)); }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * get_s_UpdateActions_6() const { return ___s_UpdateActions_6; }
	inline List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 ** get_address_of_s_UpdateActions_6() { return &___s_UpdateActions_6; }
	inline void set_s_UpdateActions_6(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * value)
	{
		___s_UpdateActions_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UpdateActions_6), (void*)value);
	}
};


// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.Shader
struct Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

struct Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// System.Action`1<System.Single>
struct Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9  : public MulticastDelegate_t
{
public:

public:
};


// System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>
struct Action_2_tF65C920A10937B33C65584F87099045F42BBB100  : public MulticastDelegate_t
{
public:

public:
};


// System.Comparison`1<Coffee.UIEffects.GraphicConnector>
struct Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.String>
struct Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82  : public MulticastDelegate_t
{
public:

public:
};


// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF  : public Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072  : public Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1
{
public:

public:
};

struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields
{
public:
	// UnityEngine.RectTransform/ReapplyDrivenProperties UnityEngine.RectTransform::reapplyDrivenProperties
	ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * ___reapplyDrivenProperties_4;

public:
	inline static int32_t get_offset_of_reapplyDrivenProperties_4() { return static_cast<int32_t>(offsetof(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_StaticFields, ___reapplyDrivenProperties_4)); }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * get_reapplyDrivenProperties_4() const { return ___reapplyDrivenProperties_4; }
	inline ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE ** get_address_of_reapplyDrivenProperties_4() { return &___reapplyDrivenProperties_4; }
	inline void set_reapplyDrivenProperties_4(ReapplyDrivenProperties_t1441259DADA8FE33A95334AC24C017DFA3DEB4CE * value)
	{
		___reapplyDrivenProperties_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___reapplyDrivenProperties_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// Coffee.UIEffects.BaseMeshEffect
struct BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.RectTransform Coffee.UIEffects.BaseMeshEffect::_rectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ____rectTransform_4;
	// UnityEngine.UI.Graphic Coffee.UIEffects.BaseMeshEffect::_graphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ____graphic_5;
	// Coffee.UIEffects.GraphicConnector Coffee.UIEffects.BaseMeshEffect::_connector
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ____connector_6;
	// System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect> Coffee.UIEffects.BaseMeshEffect::syncEffects
	List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * ___syncEffects_7;

public:
	inline static int32_t get_offset_of__rectTransform_4() { return static_cast<int32_t>(offsetof(BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A, ____rectTransform_4)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get__rectTransform_4() const { return ____rectTransform_4; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of__rectTransform_4() { return &____rectTransform_4; }
	inline void set__rectTransform_4(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		____rectTransform_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rectTransform_4), (void*)value);
	}

	inline static int32_t get_offset_of__graphic_5() { return static_cast<int32_t>(offsetof(BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A, ____graphic_5)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get__graphic_5() const { return ____graphic_5; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of__graphic_5() { return &____graphic_5; }
	inline void set__graphic_5(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		____graphic_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____graphic_5), (void*)value);
	}

	inline static int32_t get_offset_of__connector_6() { return static_cast<int32_t>(offsetof(BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A, ____connector_6)); }
	inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * get__connector_6() const { return ____connector_6; }
	inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F ** get_address_of__connector_6() { return &____connector_6; }
	inline void set__connector_6(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * value)
	{
		____connector_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____connector_6), (void*)value);
	}

	inline static int32_t get_offset_of_syncEffects_7() { return static_cast<int32_t>(offsetof(BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A, ___syncEffects_7)); }
	inline List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * get_syncEffects_7() const { return ___syncEffects_7; }
	inline List_1_t01BE5878AE01033101C05CE072C892141BBDA54F ** get_address_of_syncEffects_7() { return &___syncEffects_7; }
	inline void set_syncEffects_7(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * value)
	{
		___syncEffects_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___syncEffects_7), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// Coffee.UIEffects.BaseMaterialEffect
struct BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065  : public BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A
{
public:
	// UnityEngine.Hash128 Coffee.UIEffects.BaseMaterialEffect::_effectMaterialHash
	Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ____effectMaterialHash_11;
	// System.Int32 Coffee.UIEffects.BaseMaterialEffect::<parameterIndex>k__BackingField
	int32_t ___U3CparameterIndexU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of__effectMaterialHash_11() { return static_cast<int32_t>(offsetof(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065, ____effectMaterialHash_11)); }
	inline Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  get__effectMaterialHash_11() const { return ____effectMaterialHash_11; }
	inline Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A * get_address_of__effectMaterialHash_11() { return &____effectMaterialHash_11; }
	inline void set__effectMaterialHash_11(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  value)
	{
		____effectMaterialHash_11 = value;
	}

	inline static int32_t get_offset_of_U3CparameterIndexU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065, ___U3CparameterIndexU3Ek__BackingField_12)); }
	inline int32_t get_U3CparameterIndexU3Ek__BackingField_12() const { return ___U3CparameterIndexU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CparameterIndexU3Ek__BackingField_12() { return &___U3CparameterIndexU3Ek__BackingField_12; }
	inline void set_U3CparameterIndexU3Ek__BackingField_12(int32_t value)
	{
		___U3CparameterIndexU3Ek__BackingField_12 = value;
	}
};

struct BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields
{
public:
	// UnityEngine.Hash128 Coffee.UIEffects.BaseMaterialEffect::k_InvalidHash
	Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___k_InvalidHash_8;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> Coffee.UIEffects.BaseMaterialEffect::s_TempVerts
	List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___s_TempVerts_9;
	// System.Text.StringBuilder Coffee.UIEffects.BaseMaterialEffect::s_StringBuilder
	StringBuilder_t * ___s_StringBuilder_10;

public:
	inline static int32_t get_offset_of_k_InvalidHash_8() { return static_cast<int32_t>(offsetof(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields, ___k_InvalidHash_8)); }
	inline Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  get_k_InvalidHash_8() const { return ___k_InvalidHash_8; }
	inline Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A * get_address_of_k_InvalidHash_8() { return &___k_InvalidHash_8; }
	inline void set_k_InvalidHash_8(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  value)
	{
		___k_InvalidHash_8 = value;
	}

	inline static int32_t get_offset_of_s_TempVerts_9() { return static_cast<int32_t>(offsetof(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields, ___s_TempVerts_9)); }
	inline List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * get_s_TempVerts_9() const { return ___s_TempVerts_9; }
	inline List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F ** get_address_of_s_TempVerts_9() { return &___s_TempVerts_9; }
	inline void set_s_TempVerts_9(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * value)
	{
		___s_TempVerts_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_TempVerts_9), (void*)value);
	}

	inline static int32_t get_offset_of_s_StringBuilder_10() { return static_cast<int32_t>(offsetof(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields, ___s_StringBuilder_10)); }
	inline StringBuilder_t * get_s_StringBuilder_10() const { return ___s_StringBuilder_10; }
	inline StringBuilder_t ** get_address_of_s_StringBuilder_10() { return &___s_StringBuilder_10; }
	inline void set_s_StringBuilder_10(StringBuilder_t * value)
	{
		___s_StringBuilder_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_StringBuilder_10), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// Coffee.UIEffects.UIFlip
struct UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D  : public BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A
{
public:
	// System.Boolean Coffee.UIEffects.UIFlip::m_Horizontal
	bool ___m_Horizontal_8;
	// System.Boolean Coffee.UIEffects.UIFlip::m_Veritical
	bool ___m_Veritical_9;

public:
	inline static int32_t get_offset_of_m_Horizontal_8() { return static_cast<int32_t>(offsetof(UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D, ___m_Horizontal_8)); }
	inline bool get_m_Horizontal_8() const { return ___m_Horizontal_8; }
	inline bool* get_address_of_m_Horizontal_8() { return &___m_Horizontal_8; }
	inline void set_m_Horizontal_8(bool value)
	{
		___m_Horizontal_8 = value;
	}

	inline static int32_t get_offset_of_m_Veritical_9() { return static_cast<int32_t>(offsetof(UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D, ___m_Veritical_9)); }
	inline bool get_m_Veritical_9() const { return ___m_Veritical_9; }
	inline bool* get_address_of_m_Veritical_9() { return &___m_Veritical_9; }
	inline void set_m_Veritical_9(bool value)
	{
		___m_Veritical_9 = value;
	}
};


// Coffee.UIEffects.UIGradient
struct UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813  : public BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A
{
public:
	// Coffee.UIEffects.UIGradient/Direction Coffee.UIEffects.UIGradient::m_Direction
	int32_t ___m_Direction_9;
	// UnityEngine.Color Coffee.UIEffects.UIGradient::m_Color1
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color1_10;
	// UnityEngine.Color Coffee.UIEffects.UIGradient::m_Color2
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color2_11;
	// UnityEngine.Color Coffee.UIEffects.UIGradient::m_Color3
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color3_12;
	// UnityEngine.Color Coffee.UIEffects.UIGradient::m_Color4
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color4_13;
	// System.Single Coffee.UIEffects.UIGradient::m_Rotation
	float ___m_Rotation_14;
	// System.Single Coffee.UIEffects.UIGradient::m_Offset1
	float ___m_Offset1_15;
	// System.Single Coffee.UIEffects.UIGradient::m_Offset2
	float ___m_Offset2_16;
	// Coffee.UIEffects.UIGradient/GradientStyle Coffee.UIEffects.UIGradient::m_GradientStyle
	int32_t ___m_GradientStyle_17;
	// UnityEngine.ColorSpace Coffee.UIEffects.UIGradient::m_ColorSpace
	int32_t ___m_ColorSpace_18;
	// System.Boolean Coffee.UIEffects.UIGradient::m_IgnoreAspectRatio
	bool ___m_IgnoreAspectRatio_19;

public:
	inline static int32_t get_offset_of_m_Direction_9() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Direction_9)); }
	inline int32_t get_m_Direction_9() const { return ___m_Direction_9; }
	inline int32_t* get_address_of_m_Direction_9() { return &___m_Direction_9; }
	inline void set_m_Direction_9(int32_t value)
	{
		___m_Direction_9 = value;
	}

	inline static int32_t get_offset_of_m_Color1_10() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Color1_10)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color1_10() const { return ___m_Color1_10; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color1_10() { return &___m_Color1_10; }
	inline void set_m_Color1_10(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color1_10 = value;
	}

	inline static int32_t get_offset_of_m_Color2_11() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Color2_11)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color2_11() const { return ___m_Color2_11; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color2_11() { return &___m_Color2_11; }
	inline void set_m_Color2_11(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color2_11 = value;
	}

	inline static int32_t get_offset_of_m_Color3_12() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Color3_12)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color3_12() const { return ___m_Color3_12; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color3_12() { return &___m_Color3_12; }
	inline void set_m_Color3_12(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color3_12 = value;
	}

	inline static int32_t get_offset_of_m_Color4_13() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Color4_13)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color4_13() const { return ___m_Color4_13; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color4_13() { return &___m_Color4_13; }
	inline void set_m_Color4_13(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color4_13 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_14() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Rotation_14)); }
	inline float get_m_Rotation_14() const { return ___m_Rotation_14; }
	inline float* get_address_of_m_Rotation_14() { return &___m_Rotation_14; }
	inline void set_m_Rotation_14(float value)
	{
		___m_Rotation_14 = value;
	}

	inline static int32_t get_offset_of_m_Offset1_15() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Offset1_15)); }
	inline float get_m_Offset1_15() const { return ___m_Offset1_15; }
	inline float* get_address_of_m_Offset1_15() { return &___m_Offset1_15; }
	inline void set_m_Offset1_15(float value)
	{
		___m_Offset1_15 = value;
	}

	inline static int32_t get_offset_of_m_Offset2_16() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_Offset2_16)); }
	inline float get_m_Offset2_16() const { return ___m_Offset2_16; }
	inline float* get_address_of_m_Offset2_16() { return &___m_Offset2_16; }
	inline void set_m_Offset2_16(float value)
	{
		___m_Offset2_16 = value;
	}

	inline static int32_t get_offset_of_m_GradientStyle_17() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_GradientStyle_17)); }
	inline int32_t get_m_GradientStyle_17() const { return ___m_GradientStyle_17; }
	inline int32_t* get_address_of_m_GradientStyle_17() { return &___m_GradientStyle_17; }
	inline void set_m_GradientStyle_17(int32_t value)
	{
		___m_GradientStyle_17 = value;
	}

	inline static int32_t get_offset_of_m_ColorSpace_18() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_ColorSpace_18)); }
	inline int32_t get_m_ColorSpace_18() const { return ___m_ColorSpace_18; }
	inline int32_t* get_address_of_m_ColorSpace_18() { return &___m_ColorSpace_18; }
	inline void set_m_ColorSpace_18(int32_t value)
	{
		___m_ColorSpace_18 = value;
	}

	inline static int32_t get_offset_of_m_IgnoreAspectRatio_19() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813, ___m_IgnoreAspectRatio_19)); }
	inline bool get_m_IgnoreAspectRatio_19() const { return ___m_IgnoreAspectRatio_19; }
	inline bool* get_address_of_m_IgnoreAspectRatio_19() { return &___m_IgnoreAspectRatio_19; }
	inline void set_m_IgnoreAspectRatio_19(bool value)
	{
		___m_IgnoreAspectRatio_19 = value;
	}
};

struct UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_StaticFields
{
public:
	// UnityEngine.Vector2[] Coffee.UIEffects.UIGradient::s_SplitedCharacterPosition
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_SplitedCharacterPosition_8;

public:
	inline static int32_t get_offset_of_s_SplitedCharacterPosition_8() { return static_cast<int32_t>(offsetof(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_StaticFields, ___s_SplitedCharacterPosition_8)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_SplitedCharacterPosition_8() const { return ___s_SplitedCharacterPosition_8; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_SplitedCharacterPosition_8() { return &___s_SplitedCharacterPosition_8; }
	inline void set_s_SplitedCharacterPosition_8(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_SplitedCharacterPosition_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_SplitedCharacterPosition_8), (void*)value);
	}
};


// Coffee.UIEffects.UIShadow
struct UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB  : public BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A
{
public:
	// System.Int32 Coffee.UIEffects.UIShadow::_graphicVertexCount
	int32_t ____graphicVertexCount_10;
	// Coffee.UIEffects.UIEffect Coffee.UIEffects.UIShadow::_uiEffect
	UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * ____uiEffect_11;
	// System.Single Coffee.UIEffects.UIShadow::m_BlurFactor
	float ___m_BlurFactor_12;
	// Coffee.UIEffects.ShadowStyle Coffee.UIEffects.UIShadow::m_Style
	int32_t ___m_Style_13;
	// UnityEngine.Color Coffee.UIEffects.UIShadow::m_EffectColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_EffectColor_14;
	// UnityEngine.Vector2 Coffee.UIEffects.UIShadow::m_EffectDistance
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_EffectDistance_15;
	// System.Boolean Coffee.UIEffects.UIShadow::m_UseGraphicAlpha
	bool ___m_UseGraphicAlpha_16;
	// System.Int32 Coffee.UIEffects.UIShadow::<parameterIndex>k__BackingField
	int32_t ___U3CparameterIndexU3Ek__BackingField_18;
	// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShadow::<paramTex>k__BackingField
	ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___U3CparamTexU3Ek__BackingField_19;

public:
	inline static int32_t get_offset_of__graphicVertexCount_10() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ____graphicVertexCount_10)); }
	inline int32_t get__graphicVertexCount_10() const { return ____graphicVertexCount_10; }
	inline int32_t* get_address_of__graphicVertexCount_10() { return &____graphicVertexCount_10; }
	inline void set__graphicVertexCount_10(int32_t value)
	{
		____graphicVertexCount_10 = value;
	}

	inline static int32_t get_offset_of__uiEffect_11() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ____uiEffect_11)); }
	inline UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * get__uiEffect_11() const { return ____uiEffect_11; }
	inline UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 ** get_address_of__uiEffect_11() { return &____uiEffect_11; }
	inline void set__uiEffect_11(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * value)
	{
		____uiEffect_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____uiEffect_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_BlurFactor_12() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___m_BlurFactor_12)); }
	inline float get_m_BlurFactor_12() const { return ___m_BlurFactor_12; }
	inline float* get_address_of_m_BlurFactor_12() { return &___m_BlurFactor_12; }
	inline void set_m_BlurFactor_12(float value)
	{
		___m_BlurFactor_12 = value;
	}

	inline static int32_t get_offset_of_m_Style_13() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___m_Style_13)); }
	inline int32_t get_m_Style_13() const { return ___m_Style_13; }
	inline int32_t* get_address_of_m_Style_13() { return &___m_Style_13; }
	inline void set_m_Style_13(int32_t value)
	{
		___m_Style_13 = value;
	}

	inline static int32_t get_offset_of_m_EffectColor_14() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___m_EffectColor_14)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_EffectColor_14() const { return ___m_EffectColor_14; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_EffectColor_14() { return &___m_EffectColor_14; }
	inline void set_m_EffectColor_14(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_EffectColor_14 = value;
	}

	inline static int32_t get_offset_of_m_EffectDistance_15() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___m_EffectDistance_15)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_EffectDistance_15() const { return ___m_EffectDistance_15; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_EffectDistance_15() { return &___m_EffectDistance_15; }
	inline void set_m_EffectDistance_15(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_EffectDistance_15 = value;
	}

	inline static int32_t get_offset_of_m_UseGraphicAlpha_16() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___m_UseGraphicAlpha_16)); }
	inline bool get_m_UseGraphicAlpha_16() const { return ___m_UseGraphicAlpha_16; }
	inline bool* get_address_of_m_UseGraphicAlpha_16() { return &___m_UseGraphicAlpha_16; }
	inline void set_m_UseGraphicAlpha_16(bool value)
	{
		___m_UseGraphicAlpha_16 = value;
	}

	inline static int32_t get_offset_of_U3CparameterIndexU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___U3CparameterIndexU3Ek__BackingField_18)); }
	inline int32_t get_U3CparameterIndexU3Ek__BackingField_18() const { return ___U3CparameterIndexU3Ek__BackingField_18; }
	inline int32_t* get_address_of_U3CparameterIndexU3Ek__BackingField_18() { return &___U3CparameterIndexU3Ek__BackingField_18; }
	inline void set_U3CparameterIndexU3Ek__BackingField_18(int32_t value)
	{
		___U3CparameterIndexU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CparamTexU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB, ___U3CparamTexU3Ek__BackingField_19)); }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * get_U3CparamTexU3Ek__BackingField_19() const { return ___U3CparamTexU3Ek__BackingField_19; }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 ** get_address_of_U3CparamTexU3Ek__BackingField_19() { return &___U3CparamTexU3Ek__BackingField_19; }
	inline void set_U3CparamTexU3Ek__BackingField_19(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * value)
	{
		___U3CparamTexU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CparamTexU3Ek__BackingField_19), (void*)value);
	}
};

struct UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields
{
public:
	// System.Collections.Generic.List`1<Coffee.UIEffects.UIShadow> Coffee.UIEffects.UIShadow::tmpShadows
	List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * ___tmpShadows_8;
	// System.Collections.Generic.List`1<UnityEngine.UIVertex> Coffee.UIEffects.UIShadow::s_Verts
	List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___s_Verts_9;

public:
	inline static int32_t get_offset_of_tmpShadows_8() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields, ___tmpShadows_8)); }
	inline List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * get_tmpShadows_8() const { return ___tmpShadows_8; }
	inline List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 ** get_address_of_tmpShadows_8() { return &___tmpShadows_8; }
	inline void set_tmpShadows_8(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * value)
	{
		___tmpShadows_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tmpShadows_8), (void*)value);
	}

	inline static int32_t get_offset_of_s_Verts_9() { return static_cast<int32_t>(offsetof(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields, ___s_Verts_9)); }
	inline List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * get_s_Verts_9() const { return ___s_Verts_9; }
	inline List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F ** get_address_of_s_Verts_9() { return &___s_Verts_9; }
	inline void set_s_Verts_9(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * value)
	{
		___s_Verts_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Verts_9), (void*)value);
	}
};


// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};


// Coffee.UIEffects.UIDissolve
struct UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A  : public BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065
{
public:
	// System.Boolean Coffee.UIEffects.UIDissolve::_lastKeepAspectRatio
	bool ____lastKeepAspectRatio_16;
	// Coffee.UIEffects.EffectArea Coffee.UIEffects.UIDissolve::_lastEffectArea
	int32_t ____lastEffectArea_17;
	// System.Single Coffee.UIEffects.UIDissolve::m_EffectFactor
	float ___m_EffectFactor_19;
	// System.Single Coffee.UIEffects.UIDissolve::m_Width
	float ___m_Width_20;
	// System.Single Coffee.UIEffects.UIDissolve::m_Softness
	float ___m_Softness_21;
	// UnityEngine.Color Coffee.UIEffects.UIDissolve::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_22;
	// Coffee.UIEffects.ColorMode Coffee.UIEffects.UIDissolve::m_ColorMode
	int32_t ___m_ColorMode_23;
	// UnityEngine.Texture Coffee.UIEffects.UIDissolve::m_TransitionTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_TransitionTexture_24;
	// Coffee.UIEffects.EffectArea Coffee.UIEffects.UIDissolve::m_EffectArea
	int32_t ___m_EffectArea_25;
	// System.Boolean Coffee.UIEffects.UIDissolve::m_KeepAspectRatio
	bool ___m_KeepAspectRatio_26;
	// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIDissolve::m_Player
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * ___m_Player_27;
	// System.Boolean Coffee.UIEffects.UIDissolve::m_Reverse
	bool ___m_Reverse_28;

public:
	inline static int32_t get_offset_of__lastKeepAspectRatio_16() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ____lastKeepAspectRatio_16)); }
	inline bool get__lastKeepAspectRatio_16() const { return ____lastKeepAspectRatio_16; }
	inline bool* get_address_of__lastKeepAspectRatio_16() { return &____lastKeepAspectRatio_16; }
	inline void set__lastKeepAspectRatio_16(bool value)
	{
		____lastKeepAspectRatio_16 = value;
	}

	inline static int32_t get_offset_of__lastEffectArea_17() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ____lastEffectArea_17)); }
	inline int32_t get__lastEffectArea_17() const { return ____lastEffectArea_17; }
	inline int32_t* get_address_of__lastEffectArea_17() { return &____lastEffectArea_17; }
	inline void set__lastEffectArea_17(int32_t value)
	{
		____lastEffectArea_17 = value;
	}

	inline static int32_t get_offset_of_m_EffectFactor_19() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_EffectFactor_19)); }
	inline float get_m_EffectFactor_19() const { return ___m_EffectFactor_19; }
	inline float* get_address_of_m_EffectFactor_19() { return &___m_EffectFactor_19; }
	inline void set_m_EffectFactor_19(float value)
	{
		___m_EffectFactor_19 = value;
	}

	inline static int32_t get_offset_of_m_Width_20() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_Width_20)); }
	inline float get_m_Width_20() const { return ___m_Width_20; }
	inline float* get_address_of_m_Width_20() { return &___m_Width_20; }
	inline void set_m_Width_20(float value)
	{
		___m_Width_20 = value;
	}

	inline static int32_t get_offset_of_m_Softness_21() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_Softness_21)); }
	inline float get_m_Softness_21() const { return ___m_Softness_21; }
	inline float* get_address_of_m_Softness_21() { return &___m_Softness_21; }
	inline void set_m_Softness_21(float value)
	{
		___m_Softness_21 = value;
	}

	inline static int32_t get_offset_of_m_Color_22() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_Color_22)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_22() const { return ___m_Color_22; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_22() { return &___m_Color_22; }
	inline void set_m_Color_22(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_22 = value;
	}

	inline static int32_t get_offset_of_m_ColorMode_23() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_ColorMode_23)); }
	inline int32_t get_m_ColorMode_23() const { return ___m_ColorMode_23; }
	inline int32_t* get_address_of_m_ColorMode_23() { return &___m_ColorMode_23; }
	inline void set_m_ColorMode_23(int32_t value)
	{
		___m_ColorMode_23 = value;
	}

	inline static int32_t get_offset_of_m_TransitionTexture_24() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_TransitionTexture_24)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_TransitionTexture_24() const { return ___m_TransitionTexture_24; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_TransitionTexture_24() { return &___m_TransitionTexture_24; }
	inline void set_m_TransitionTexture_24(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_TransitionTexture_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TransitionTexture_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_EffectArea_25() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_EffectArea_25)); }
	inline int32_t get_m_EffectArea_25() const { return ___m_EffectArea_25; }
	inline int32_t* get_address_of_m_EffectArea_25() { return &___m_EffectArea_25; }
	inline void set_m_EffectArea_25(int32_t value)
	{
		___m_EffectArea_25 = value;
	}

	inline static int32_t get_offset_of_m_KeepAspectRatio_26() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_KeepAspectRatio_26)); }
	inline bool get_m_KeepAspectRatio_26() const { return ___m_KeepAspectRatio_26; }
	inline bool* get_address_of_m_KeepAspectRatio_26() { return &___m_KeepAspectRatio_26; }
	inline void set_m_KeepAspectRatio_26(bool value)
	{
		___m_KeepAspectRatio_26 = value;
	}

	inline static int32_t get_offset_of_m_Player_27() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_Player_27)); }
	inline EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * get_m_Player_27() const { return ___m_Player_27; }
	inline EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C ** get_address_of_m_Player_27() { return &___m_Player_27; }
	inline void set_m_Player_27(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * value)
	{
		___m_Player_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_Reverse_28() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A, ___m_Reverse_28)); }
	inline bool get_m_Reverse_28() const { return ___m_Reverse_28; }
	inline bool* get_address_of_m_Reverse_28() { return &___m_Reverse_28; }
	inline void set_m_Reverse_28(bool value)
	{
		___m_Reverse_28 = value;
	}
};

struct UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields
{
public:
	// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIDissolve::s_ParamTex
	ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___s_ParamTex_14;
	// System.Int32 Coffee.UIEffects.UIDissolve::k_TransitionTexId
	int32_t ___k_TransitionTexId_15;
	// UnityEngine.Texture Coffee.UIEffects.UIDissolve::_defaultTransitionTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ____defaultTransitionTexture_18;

public:
	inline static int32_t get_offset_of_s_ParamTex_14() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields, ___s_ParamTex_14)); }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * get_s_ParamTex_14() const { return ___s_ParamTex_14; }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 ** get_address_of_s_ParamTex_14() { return &___s_ParamTex_14; }
	inline void set_s_ParamTex_14(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * value)
	{
		___s_ParamTex_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParamTex_14), (void*)value);
	}

	inline static int32_t get_offset_of_k_TransitionTexId_15() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields, ___k_TransitionTexId_15)); }
	inline int32_t get_k_TransitionTexId_15() const { return ___k_TransitionTexId_15; }
	inline int32_t* get_address_of_k_TransitionTexId_15() { return &___k_TransitionTexId_15; }
	inline void set_k_TransitionTexId_15(int32_t value)
	{
		___k_TransitionTexId_15 = value;
	}

	inline static int32_t get_offset_of__defaultTransitionTexture_18() { return static_cast<int32_t>(offsetof(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields, ____defaultTransitionTexture_18)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get__defaultTransitionTexture_18() const { return ____defaultTransitionTexture_18; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of__defaultTransitionTexture_18() { return &____defaultTransitionTexture_18; }
	inline void set__defaultTransitionTexture_18(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		____defaultTransitionTexture_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____defaultTransitionTexture_18), (void*)value);
	}
};


// Coffee.UIEffects.UIEffect
struct UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21  : public BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065
{
public:
	// System.Single Coffee.UIEffects.UIEffect::m_EffectFactor
	float ___m_EffectFactor_15;
	// System.Single Coffee.UIEffects.UIEffect::m_ColorFactor
	float ___m_ColorFactor_16;
	// System.Single Coffee.UIEffects.UIEffect::m_BlurFactor
	float ___m_BlurFactor_17;
	// Coffee.UIEffects.EffectMode Coffee.UIEffects.UIEffect::m_EffectMode
	int32_t ___m_EffectMode_18;
	// Coffee.UIEffects.ColorMode Coffee.UIEffects.UIEffect::m_ColorMode
	int32_t ___m_ColorMode_19;
	// Coffee.UIEffects.BlurMode Coffee.UIEffects.UIEffect::m_BlurMode
	int32_t ___m_BlurMode_20;
	// System.Boolean Coffee.UIEffects.UIEffect::m_AdvancedBlur
	bool ___m_AdvancedBlur_21;

public:
	inline static int32_t get_offset_of_m_EffectFactor_15() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_EffectFactor_15)); }
	inline float get_m_EffectFactor_15() const { return ___m_EffectFactor_15; }
	inline float* get_address_of_m_EffectFactor_15() { return &___m_EffectFactor_15; }
	inline void set_m_EffectFactor_15(float value)
	{
		___m_EffectFactor_15 = value;
	}

	inline static int32_t get_offset_of_m_ColorFactor_16() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_ColorFactor_16)); }
	inline float get_m_ColorFactor_16() const { return ___m_ColorFactor_16; }
	inline float* get_address_of_m_ColorFactor_16() { return &___m_ColorFactor_16; }
	inline void set_m_ColorFactor_16(float value)
	{
		___m_ColorFactor_16 = value;
	}

	inline static int32_t get_offset_of_m_BlurFactor_17() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_BlurFactor_17)); }
	inline float get_m_BlurFactor_17() const { return ___m_BlurFactor_17; }
	inline float* get_address_of_m_BlurFactor_17() { return &___m_BlurFactor_17; }
	inline void set_m_BlurFactor_17(float value)
	{
		___m_BlurFactor_17 = value;
	}

	inline static int32_t get_offset_of_m_EffectMode_18() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_EffectMode_18)); }
	inline int32_t get_m_EffectMode_18() const { return ___m_EffectMode_18; }
	inline int32_t* get_address_of_m_EffectMode_18() { return &___m_EffectMode_18; }
	inline void set_m_EffectMode_18(int32_t value)
	{
		___m_EffectMode_18 = value;
	}

	inline static int32_t get_offset_of_m_ColorMode_19() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_ColorMode_19)); }
	inline int32_t get_m_ColorMode_19() const { return ___m_ColorMode_19; }
	inline int32_t* get_address_of_m_ColorMode_19() { return &___m_ColorMode_19; }
	inline void set_m_ColorMode_19(int32_t value)
	{
		___m_ColorMode_19 = value;
	}

	inline static int32_t get_offset_of_m_BlurMode_20() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_BlurMode_20)); }
	inline int32_t get_m_BlurMode_20() const { return ___m_BlurMode_20; }
	inline int32_t* get_address_of_m_BlurMode_20() { return &___m_BlurMode_20; }
	inline void set_m_BlurMode_20(int32_t value)
	{
		___m_BlurMode_20 = value;
	}

	inline static int32_t get_offset_of_m_AdvancedBlur_21() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21, ___m_AdvancedBlur_21)); }
	inline bool get_m_AdvancedBlur_21() const { return ___m_AdvancedBlur_21; }
	inline bool* get_address_of_m_AdvancedBlur_21() { return &___m_AdvancedBlur_21; }
	inline void set_m_AdvancedBlur_21(bool value)
	{
		___m_AdvancedBlur_21 = value;
	}
};

struct UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_StaticFields
{
public:
	// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIEffect::s_ParamTex
	ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___s_ParamTex_14;

public:
	inline static int32_t get_offset_of_s_ParamTex_14() { return static_cast<int32_t>(offsetof(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_StaticFields, ___s_ParamTex_14)); }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * get_s_ParamTex_14() const { return ___s_ParamTex_14; }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 ** get_address_of_s_ParamTex_14() { return &___s_ParamTex_14; }
	inline void set_s_ParamTex_14(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * value)
	{
		___s_ParamTex_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParamTex_14), (void*)value);
	}
};


// Coffee.UIEffects.UIHsvModifier
struct UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2  : public BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065
{
public:
	// UnityEngine.Color Coffee.UIEffects.UIHsvModifier::m_TargetColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_TargetColor_15;
	// System.Single Coffee.UIEffects.UIHsvModifier::m_Range
	float ___m_Range_16;
	// System.Single Coffee.UIEffects.UIHsvModifier::m_Hue
	float ___m_Hue_17;
	// System.Single Coffee.UIEffects.UIHsvModifier::m_Saturation
	float ___m_Saturation_18;
	// System.Single Coffee.UIEffects.UIHsvModifier::m_Value
	float ___m_Value_19;

public:
	inline static int32_t get_offset_of_m_TargetColor_15() { return static_cast<int32_t>(offsetof(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2, ___m_TargetColor_15)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_TargetColor_15() const { return ___m_TargetColor_15; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_TargetColor_15() { return &___m_TargetColor_15; }
	inline void set_m_TargetColor_15(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_TargetColor_15 = value;
	}

	inline static int32_t get_offset_of_m_Range_16() { return static_cast<int32_t>(offsetof(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2, ___m_Range_16)); }
	inline float get_m_Range_16() const { return ___m_Range_16; }
	inline float* get_address_of_m_Range_16() { return &___m_Range_16; }
	inline void set_m_Range_16(float value)
	{
		___m_Range_16 = value;
	}

	inline static int32_t get_offset_of_m_Hue_17() { return static_cast<int32_t>(offsetof(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2, ___m_Hue_17)); }
	inline float get_m_Hue_17() const { return ___m_Hue_17; }
	inline float* get_address_of_m_Hue_17() { return &___m_Hue_17; }
	inline void set_m_Hue_17(float value)
	{
		___m_Hue_17 = value;
	}

	inline static int32_t get_offset_of_m_Saturation_18() { return static_cast<int32_t>(offsetof(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2, ___m_Saturation_18)); }
	inline float get_m_Saturation_18() const { return ___m_Saturation_18; }
	inline float* get_address_of_m_Saturation_18() { return &___m_Saturation_18; }
	inline void set_m_Saturation_18(float value)
	{
		___m_Saturation_18 = value;
	}

	inline static int32_t get_offset_of_m_Value_19() { return static_cast<int32_t>(offsetof(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2, ___m_Value_19)); }
	inline float get_m_Value_19() const { return ___m_Value_19; }
	inline float* get_address_of_m_Value_19() { return &___m_Value_19; }
	inline void set_m_Value_19(float value)
	{
		___m_Value_19 = value;
	}
};

struct UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_StaticFields
{
public:
	// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIHsvModifier::s_ParamTex
	ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___s_ParamTex_14;

public:
	inline static int32_t get_offset_of_s_ParamTex_14() { return static_cast<int32_t>(offsetof(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_StaticFields, ___s_ParamTex_14)); }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * get_s_ParamTex_14() const { return ___s_ParamTex_14; }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 ** get_address_of_s_ParamTex_14() { return &___s_ParamTex_14; }
	inline void set_s_ParamTex_14(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * value)
	{
		___s_ParamTex_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParamTex_14), (void*)value);
	}
};


// Coffee.UIEffects.UIShiny
struct UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146  : public BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065
{
public:
	// System.Single Coffee.UIEffects.UIShiny::_lastRotation
	float ____lastRotation_15;
	// Coffee.UIEffects.EffectArea Coffee.UIEffects.UIShiny::_lastEffectArea
	int32_t ____lastEffectArea_16;
	// System.Single Coffee.UIEffects.UIShiny::m_EffectFactor
	float ___m_EffectFactor_17;
	// System.Single Coffee.UIEffects.UIShiny::m_Width
	float ___m_Width_18;
	// System.Single Coffee.UIEffects.UIShiny::m_Rotation
	float ___m_Rotation_19;
	// System.Single Coffee.UIEffects.UIShiny::m_Softness
	float ___m_Softness_20;
	// System.Single Coffee.UIEffects.UIShiny::m_Brightness
	float ___m_Brightness_21;
	// System.Single Coffee.UIEffects.UIShiny::m_Gloss
	float ___m_Gloss_22;
	// Coffee.UIEffects.EffectArea Coffee.UIEffects.UIShiny::m_EffectArea
	int32_t ___m_EffectArea_23;
	// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIShiny::m_Player
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * ___m_Player_24;

public:
	inline static int32_t get_offset_of__lastRotation_15() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ____lastRotation_15)); }
	inline float get__lastRotation_15() const { return ____lastRotation_15; }
	inline float* get_address_of__lastRotation_15() { return &____lastRotation_15; }
	inline void set__lastRotation_15(float value)
	{
		____lastRotation_15 = value;
	}

	inline static int32_t get_offset_of__lastEffectArea_16() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ____lastEffectArea_16)); }
	inline int32_t get__lastEffectArea_16() const { return ____lastEffectArea_16; }
	inline int32_t* get_address_of__lastEffectArea_16() { return &____lastEffectArea_16; }
	inline void set__lastEffectArea_16(int32_t value)
	{
		____lastEffectArea_16 = value;
	}

	inline static int32_t get_offset_of_m_EffectFactor_17() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_EffectFactor_17)); }
	inline float get_m_EffectFactor_17() const { return ___m_EffectFactor_17; }
	inline float* get_address_of_m_EffectFactor_17() { return &___m_EffectFactor_17; }
	inline void set_m_EffectFactor_17(float value)
	{
		___m_EffectFactor_17 = value;
	}

	inline static int32_t get_offset_of_m_Width_18() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_Width_18)); }
	inline float get_m_Width_18() const { return ___m_Width_18; }
	inline float* get_address_of_m_Width_18() { return &___m_Width_18; }
	inline void set_m_Width_18(float value)
	{
		___m_Width_18 = value;
	}

	inline static int32_t get_offset_of_m_Rotation_19() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_Rotation_19)); }
	inline float get_m_Rotation_19() const { return ___m_Rotation_19; }
	inline float* get_address_of_m_Rotation_19() { return &___m_Rotation_19; }
	inline void set_m_Rotation_19(float value)
	{
		___m_Rotation_19 = value;
	}

	inline static int32_t get_offset_of_m_Softness_20() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_Softness_20)); }
	inline float get_m_Softness_20() const { return ___m_Softness_20; }
	inline float* get_address_of_m_Softness_20() { return &___m_Softness_20; }
	inline void set_m_Softness_20(float value)
	{
		___m_Softness_20 = value;
	}

	inline static int32_t get_offset_of_m_Brightness_21() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_Brightness_21)); }
	inline float get_m_Brightness_21() const { return ___m_Brightness_21; }
	inline float* get_address_of_m_Brightness_21() { return &___m_Brightness_21; }
	inline void set_m_Brightness_21(float value)
	{
		___m_Brightness_21 = value;
	}

	inline static int32_t get_offset_of_m_Gloss_22() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_Gloss_22)); }
	inline float get_m_Gloss_22() const { return ___m_Gloss_22; }
	inline float* get_address_of_m_Gloss_22() { return &___m_Gloss_22; }
	inline void set_m_Gloss_22(float value)
	{
		___m_Gloss_22 = value;
	}

	inline static int32_t get_offset_of_m_EffectArea_23() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_EffectArea_23)); }
	inline int32_t get_m_EffectArea_23() const { return ___m_EffectArea_23; }
	inline int32_t* get_address_of_m_EffectArea_23() { return &___m_EffectArea_23; }
	inline void set_m_EffectArea_23(int32_t value)
	{
		___m_EffectArea_23 = value;
	}

	inline static int32_t get_offset_of_m_Player_24() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146, ___m_Player_24)); }
	inline EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * get_m_Player_24() const { return ___m_Player_24; }
	inline EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C ** get_address_of_m_Player_24() { return &___m_Player_24; }
	inline void set_m_Player_24(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * value)
	{
		___m_Player_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_24), (void*)value);
	}
};

struct UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_StaticFields
{
public:
	// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShiny::s_ParamTex
	ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___s_ParamTex_14;

public:
	inline static int32_t get_offset_of_s_ParamTex_14() { return static_cast<int32_t>(offsetof(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_StaticFields, ___s_ParamTex_14)); }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * get_s_ParamTex_14() const { return ___s_ParamTex_14; }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 ** get_address_of_s_ParamTex_14() { return &___s_ParamTex_14; }
	inline void set_s_ParamTex_14(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * value)
	{
		___s_ParamTex_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParamTex_14), (void*)value);
	}
};


// Coffee.UIEffects.UISyncEffect
struct UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E  : public BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065
{
public:
	// Coffee.UIEffects.BaseMeshEffect Coffee.UIEffects.UISyncEffect::m_TargetEffect
	BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * ___m_TargetEffect_13;

public:
	inline static int32_t get_offset_of_m_TargetEffect_13() { return static_cast<int32_t>(offsetof(UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E, ___m_TargetEffect_13)); }
	inline BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * get_m_TargetEffect_13() const { return ___m_TargetEffect_13; }
	inline BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A ** get_address_of_m_TargetEffect_13() { return &___m_TargetEffect_13; }
	inline void set_m_TargetEffect_13(BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * value)
	{
		___m_TargetEffect_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetEffect_13), (void*)value);
	}
};


// Coffee.UIEffects.UITransitionEffect
struct UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981  : public BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065
{
public:
	// System.Boolean Coffee.UIEffects.UITransitionEffect::_lastKeepAspectRatio
	bool ____lastKeepAspectRatio_16;
	// Coffee.UIEffects.UITransitionEffect/EffectMode Coffee.UIEffects.UITransitionEffect::m_EffectMode
	int32_t ___m_EffectMode_18;
	// System.Single Coffee.UIEffects.UITransitionEffect::m_EffectFactor
	float ___m_EffectFactor_19;
	// UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::m_TransitionTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___m_TransitionTexture_20;
	// Coffee.UIEffects.EffectArea Coffee.UIEffects.UITransitionEffect::m_EffectArea
	int32_t ___m_EffectArea_21;
	// System.Boolean Coffee.UIEffects.UITransitionEffect::m_KeepAspectRatio
	bool ___m_KeepAspectRatio_22;
	// System.Single Coffee.UIEffects.UITransitionEffect::m_DissolveWidth
	float ___m_DissolveWidth_23;
	// System.Single Coffee.UIEffects.UITransitionEffect::m_DissolveSoftness
	float ___m_DissolveSoftness_24;
	// UnityEngine.Color Coffee.UIEffects.UITransitionEffect::m_DissolveColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DissolveColor_25;
	// System.Boolean Coffee.UIEffects.UITransitionEffect::m_PassRayOnHidden
	bool ___m_PassRayOnHidden_26;
	// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UITransitionEffect::m_Player
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * ___m_Player_27;

public:
	inline static int32_t get_offset_of__lastKeepAspectRatio_16() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ____lastKeepAspectRatio_16)); }
	inline bool get__lastKeepAspectRatio_16() const { return ____lastKeepAspectRatio_16; }
	inline bool* get_address_of__lastKeepAspectRatio_16() { return &____lastKeepAspectRatio_16; }
	inline void set__lastKeepAspectRatio_16(bool value)
	{
		____lastKeepAspectRatio_16 = value;
	}

	inline static int32_t get_offset_of_m_EffectMode_18() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_EffectMode_18)); }
	inline int32_t get_m_EffectMode_18() const { return ___m_EffectMode_18; }
	inline int32_t* get_address_of_m_EffectMode_18() { return &___m_EffectMode_18; }
	inline void set_m_EffectMode_18(int32_t value)
	{
		___m_EffectMode_18 = value;
	}

	inline static int32_t get_offset_of_m_EffectFactor_19() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_EffectFactor_19)); }
	inline float get_m_EffectFactor_19() const { return ___m_EffectFactor_19; }
	inline float* get_address_of_m_EffectFactor_19() { return &___m_EffectFactor_19; }
	inline void set_m_EffectFactor_19(float value)
	{
		___m_EffectFactor_19 = value;
	}

	inline static int32_t get_offset_of_m_TransitionTexture_20() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_TransitionTexture_20)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get_m_TransitionTexture_20() const { return ___m_TransitionTexture_20; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of_m_TransitionTexture_20() { return &___m_TransitionTexture_20; }
	inline void set_m_TransitionTexture_20(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		___m_TransitionTexture_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TransitionTexture_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_EffectArea_21() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_EffectArea_21)); }
	inline int32_t get_m_EffectArea_21() const { return ___m_EffectArea_21; }
	inline int32_t* get_address_of_m_EffectArea_21() { return &___m_EffectArea_21; }
	inline void set_m_EffectArea_21(int32_t value)
	{
		___m_EffectArea_21 = value;
	}

	inline static int32_t get_offset_of_m_KeepAspectRatio_22() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_KeepAspectRatio_22)); }
	inline bool get_m_KeepAspectRatio_22() const { return ___m_KeepAspectRatio_22; }
	inline bool* get_address_of_m_KeepAspectRatio_22() { return &___m_KeepAspectRatio_22; }
	inline void set_m_KeepAspectRatio_22(bool value)
	{
		___m_KeepAspectRatio_22 = value;
	}

	inline static int32_t get_offset_of_m_DissolveWidth_23() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_DissolveWidth_23)); }
	inline float get_m_DissolveWidth_23() const { return ___m_DissolveWidth_23; }
	inline float* get_address_of_m_DissolveWidth_23() { return &___m_DissolveWidth_23; }
	inline void set_m_DissolveWidth_23(float value)
	{
		___m_DissolveWidth_23 = value;
	}

	inline static int32_t get_offset_of_m_DissolveSoftness_24() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_DissolveSoftness_24)); }
	inline float get_m_DissolveSoftness_24() const { return ___m_DissolveSoftness_24; }
	inline float* get_address_of_m_DissolveSoftness_24() { return &___m_DissolveSoftness_24; }
	inline void set_m_DissolveSoftness_24(float value)
	{
		___m_DissolveSoftness_24 = value;
	}

	inline static int32_t get_offset_of_m_DissolveColor_25() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_DissolveColor_25)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DissolveColor_25() const { return ___m_DissolveColor_25; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DissolveColor_25() { return &___m_DissolveColor_25; }
	inline void set_m_DissolveColor_25(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DissolveColor_25 = value;
	}

	inline static int32_t get_offset_of_m_PassRayOnHidden_26() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_PassRayOnHidden_26)); }
	inline bool get_m_PassRayOnHidden_26() const { return ___m_PassRayOnHidden_26; }
	inline bool* get_address_of_m_PassRayOnHidden_26() { return &___m_PassRayOnHidden_26; }
	inline void set_m_PassRayOnHidden_26(bool value)
	{
		___m_PassRayOnHidden_26 = value;
	}

	inline static int32_t get_offset_of_m_Player_27() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981, ___m_Player_27)); }
	inline EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * get_m_Player_27() const { return ___m_Player_27; }
	inline EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C ** get_address_of_m_Player_27() { return &___m_Player_27; }
	inline void set_m_Player_27(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * value)
	{
		___m_Player_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Player_27), (void*)value);
	}
};

struct UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields
{
public:
	// System.Int32 Coffee.UIEffects.UITransitionEffect::k_TransitionTexId
	int32_t ___k_TransitionTexId_14;
	// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UITransitionEffect::s_ParamTex
	ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___s_ParamTex_15;
	// UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::_defaultTransitionTexture
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ____defaultTransitionTexture_17;

public:
	inline static int32_t get_offset_of_k_TransitionTexId_14() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields, ___k_TransitionTexId_14)); }
	inline int32_t get_k_TransitionTexId_14() const { return ___k_TransitionTexId_14; }
	inline int32_t* get_address_of_k_TransitionTexId_14() { return &___k_TransitionTexId_14; }
	inline void set_k_TransitionTexId_14(int32_t value)
	{
		___k_TransitionTexId_14 = value;
	}

	inline static int32_t get_offset_of_s_ParamTex_15() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields, ___s_ParamTex_15)); }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * get_s_ParamTex_15() const { return ___s_ParamTex_15; }
	inline ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 ** get_address_of_s_ParamTex_15() { return &___s_ParamTex_15; }
	inline void set_s_ParamTex_15(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * value)
	{
		___s_ParamTex_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ParamTex_15), (void*)value);
	}

	inline static int32_t get_offset_of__defaultTransitionTexture_17() { return static_cast<int32_t>(offsetof(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields, ____defaultTransitionTexture_17)); }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * get__defaultTransitionTexture_17() const { return ____defaultTransitionTexture_17; }
	inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE ** get_address_of__defaultTransitionTexture_17() { return &____defaultTransitionTexture_17; }
	inline void set__defaultTransitionTexture_17(Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * value)
	{
		____defaultTransitionTexture_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____defaultTransitionTexture_17), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  m_Items[1];

public:
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  m_Items[1];

public:
	inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  value)
	{
		m_Items[index] = value;
	}
};


// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2__ctor_mE1761BE81335B68DA4E0F742344DA72F092A29C1_gshared (Action_2_t4FB8E5660AE634E13BF340904C61FEA9DCE9D52D * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_gshared (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Func_2__ctor_mA7F3C5A0612B84E910DE92E77BA95101FD68EEDB_gshared (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Select_TisRuntimeObject_TisRuntimeObject_mC0F1DA980E0433D70A6CF9DD7CD1942BB7FE87C0_gshared (RuntimeObject* ___source0, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Concat_TisRuntimeObject_mD248DD20C01000944006DA903D282009D2524A9E_gshared (RuntimeObject* ___first0, RuntimeObject* ___second1, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Distinct<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_Distinct_TisRuntimeObject_m22422CAC0AFDD8D24FFC549AC238F61B469B2A42_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* Enumerable_ToArray_TisRuntimeObject_m21E15191FE8BDBAE753CC592A1DB55EA3BCE7B5B_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Single>::Invoke(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_gshared (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * __this, float ___obj0, const RuntimeMethod* method);
// System.Void System.Comparison`1<System.Object>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Comparison_1__ctor_mDE1798563694D30022D3B7E1010347C573690B4A_gshared (Comparison_1_tB56E8E7C2BF431D44E8EBD15EA3E6F41AAFF03D2 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Sort_m5EB3F127CD42F1ACA97F4DB8754C49F23B64D750_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, Comparison_1_tB56E8E7C2BF431D44E8EBD15EA3E6F41AAFF03D2 * ___comparison0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_m048C13E0F44BDC16F7CF01D14E918A84EE72C62C_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, RuntimeObject * ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Object>::TryGetValue(!0,!1&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_TryGetValue_mB45814AFBB5AF9B75911CC4B96E79357B699C2A2_gshared (Dictionary_2_t1AB2593B33426A85D2383240510B43F23D5DC559 * __this, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___key0, RuntimeObject ** ___value1, const RuntimeMethod* method);
// System.Void System.Action`2<System.Object,System.Object>::Invoke(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_2_Invoke_m54EE979C4D83695ED736A3177A68C2968C8C4382_gshared (Action_2_t4FB8E5660AE634E13BF340904C61FEA9DCE9D52D * __this, RuntimeObject * ___arg10, RuntimeObject * ___arg21, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Object>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m38F60974E87F145DD5F86F98C1641145C21FDF0E_gshared (Dictionary_2_t1AB2593B33426A85D2383240510B43F23D5DC559 * __this, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___key0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Object>::Remove(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Dictionary_2_Remove_m9A51B2B9CAA969C71C5FECED8AB6DCD05E4BFA3F_gshared (Dictionary_2_t1AB2593B33426A85D2383240510B43F23D5DC559 * __this, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m7C68CDC635A0F3711B0287490895C8715C4F5254_gshared (Dictionary_2_t1AB2593B33426A85D2383240510B43F23D5DC559 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Int32>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3_gshared (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Push(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_gshared (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, int32_t ___item0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Int32>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_gshared_inline (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Int32>::Pop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C_gshared (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Resources_Load_TisRuntimeObject_m39B6A35CFE684CD1FFF77873E20D7297B36A55E8_gshared (String_t* ___path0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_gshared (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_gshared_inline (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_gshared_inline (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___index0, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Component::GetComponents<System.Object>(System.Collections.Generic.List`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Component_GetComponents_TisRuntimeObject_mB0812CB4355C4F5E2EA27C7049C668609E72F31D_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___results0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  ___item0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118_gshared (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);

// Coffee.UIEffects.GraphicConnector Coffee.UIEffects.BaseMeshEffect::get_connector()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// UnityEngine.UI.Graphic Coffee.UIEffects.BaseMeshEffect::get_graphic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect>::GetEnumerator()
inline Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B  List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B  (*) (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UISyncEffect>::get_Current()
inline UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_inline (Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B * __this, const RuntimeMethod* method)
{
	return ((  UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * (*) (Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::SetMaterialDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UISyncEffect>::MoveNext()
inline bool Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5 (Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UISyncEffect>::Dispose()
inline void Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446 (Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA (Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Hash128::get_isValid()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB (Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A * __this, const RuntimeMethod* method);
// System.Void System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>::.ctor(System.Object,System.IntPtr)
inline void Action_2__ctor_m160CB1F6CF097F0AF0BD833BA54178EF5ADBA5F4 (Action_2_tF65C920A10937B33C65584F87099045F42BBB100 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tF65C920A10937B33C65584F87099045F42BBB100 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_2__ctor_mE1761BE81335B68DA4E0F742344DA72F092A29C1_gshared)(__this, ___object0, ___method1, method);
}
// UnityEngine.Material Coffee.UIEffects.MaterialCache::Register(UnityEngine.Material,UnityEngine.Hash128,System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * MaterialCache_Register_m41B9F13914C2BBF0F30CC45DBF8E8713E15079F8 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___hash1, Action_2_tF65C920A10937B33C65584F87099045F42BBB100 * ___onModifyMaterial2, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic3, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.MaterialCache::Unregister(UnityEngine.Hash128)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialCache_Unregister_m5F0CFE2E6BFD7A6D3E0862D6034B1CB4F3A5B394 (Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___hash0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.ParameterTexture::RegisterMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat0, const RuntimeMethod* method);
// System.Void System.Func`2<System.Object,System.Boolean>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Where<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,System.Boolean>)
inline RuntimeObject* Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364 (RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_gshared)(___source0, ___predicate1, method);
}
// System.Void System.Func`2<System.Object,System.String>::.ctor(System.Object,System.IntPtr)
inline void Func_2__ctor_m5DB0F7F826FFCB704F566795D5DA5A1F5ABD313D (Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Func_2__ctor_mA7F3C5A0612B84E910DE92E77BA95101FD68EEDB_gshared)(__this, ___object0, ___method1, method);
}
// System.Collections.Generic.IEnumerable`1<!!1> System.Linq.Enumerable::Select<System.Object,System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Func`2<!!0,!!1>)
inline RuntimeObject* Enumerable_Select_TisRuntimeObject_TisString_t_m540DAFC430ED92CA1E45E595E529F4A7CC4D7574 (RuntimeObject* ___source0, Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * ___selector1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 *, const RuntimeMethod*))Enumerable_Select_TisRuntimeObject_TisRuntimeObject_mC0F1DA980E0433D70A6CF9DD7CD1942BB7FE87C0_gshared)(___source0, ___selector1, method);
}
// System.String[] UnityEngine.Material::get_shaderKeywords()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Material_get_shaderKeywords_mDAD743C090C3CEE0B2883140B244853D71C5E9E0 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Concat<System.String>(System.Collections.Generic.IEnumerable`1<!!0>,System.Collections.Generic.IEnumerable`1<!!0>)
inline RuntimeObject* Enumerable_Concat_TisString_t_m7C95F30602BBABF6D41F782A16AE43477A957F3D (RuntimeObject* ___first0, RuntimeObject* ___second1, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*))Enumerable_Concat_TisRuntimeObject_mD248DD20C01000944006DA903D282009D2524A9E_gshared)(___first0, ___second1, method);
}
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::Distinct<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline RuntimeObject* Enumerable_Distinct_TisString_t_m0980366EB945AB2A9F39B3AC0E03CF634F6CAA94 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Distinct_TisRuntimeObject_m22422CAC0AFDD8D24FFC549AC238F61B469B2A42_gshared)(___source0, method);
}
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m21E15191FE8BDBAE753CC592A1DB55EA3BCE7B5B_gshared)(___source0, method);
}
// System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_shaderKeywords_m9EC5EFA52BF30597B1692C623806E7167B1C7688 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___value0, const RuntimeMethod* method);
// System.Void System.Text.StringBuilder::set_Length(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder_set_Length_m7C1756193B05DCA5A23C5DC98EE90A9FC685A27A (StringBuilder_t * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.Shader UnityEngine.Material::get_shader()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.String System.IO.Path::GetFileName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_GetFileName_m4CCFEEE8CA42DE16FCAF05D765EFB88E7086744A (String_t* ___path0, const RuntimeMethod* method);
// System.Text.StringBuilder System.Text.StringBuilder::Append(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringBuilder_t * StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1 (StringBuilder_t * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_name(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMeshEffect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_OnEnable_m900129F36764E412C7FCE57A7C00DB091F48F4C6 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.ParameterTexture::Register(Coffee.UIEffects.IParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_Register_m418624C23515BB36BFE2E30FDC66AB2DD855D62A (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMeshEffect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_OnDisable_m714D32A1FD3155B874FD836C76954208747FED85 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.ParameterTexture::Unregister(Coffee.UIEffects.IParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_Unregister_mC8283ADEEC7D0B15321727EC445C0909671918BC (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMeshEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
inline void List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, const RuntimeMethod*))List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC_gshared)(__this, method);
}
// System.Void System.Text.StringBuilder::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9 (StringBuilder_t * __this, const RuntimeMethod* method);
// Coffee.UIEffects.GraphicConnector Coffee.UIEffects.GraphicConnector::FindConnector(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0 (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Graphic>()
inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.RectTransform>()
inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect>::.ctor(System.Int32)
inline void List_1__ctor_m2C641C171CDB35066D77221A29BF02745472353F (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F *, int32_t, const RuntimeMethod*))List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared)(__this, ___capacity0, method);
}
// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIBehaviour__ctor_m869436738107AF382FD4D10DE9641F8241B323C7 (UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::PopulateUIVertex(UnityEngine.UIVertex&,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555 (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * ___vertex0, int32_t ___i1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Int32 UnityEngine.UI.VertexHelper::get_currentVertCount()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807 (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::Set(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_Set_mACEFC5D6FC4E52EDE4480B222485B1FB49951F5B (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_width(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_set_width_m07D84AD7C7093EDCCD94A7B93A9447CA9917DD9D (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_height(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_set_height_m4A00B16C122F44FEF4BA074386F3DC11FF4B4D23 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_xMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_yMin()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 Coffee.UIEffects.Matrix2x3::op_Multiply(Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0 (Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  ___m0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v1, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___x0, float ___y1, float ___width2, float ___height3, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_one()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42 (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_right_m42ED15112D219375D2B6879E62ED925D002F15AF (const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::.ctor()
inline void List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WillRenderCanvases__ctor_m8A46E9A5DED6B54DC2A8A3137AE3637081EADFB6 (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Canvas_add_willRenderCanvases_m00E391FCCE9839EEB6D7A729DCBF6B841FDF02B7 (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * ___value0, const RuntimeMethod* method);
// System.Void System.Action::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Action>::Add(!0)
inline void List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9 (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<System.Action>::Remove(!0)
inline bool List_1_Remove_m2FAEA539D1EBCFCF82B74CEA88321CA0D425646C (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// System.Void System.Action`1<System.Single>::Invoke(!0)
inline void Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * __this, float ___obj0, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *, float, const RuntimeMethod*))Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_gshared)(__this, ___obj0, method);
}
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84 (const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.GraphicConnector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector__ctor_mF95FB5F86269F4423CC5F9DFA370EA4849FF6DAD (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.GraphicConnector::AddConnector(Coffee.UIEffects.GraphicConnector)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_AddConnector_m1D64A1D518EE65C19E8A0D2EE1D3B4584F7C3CFE (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___connector0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector>::Add(!0)
inline void List_1_Add_mE8C9752EB54C57B7495AF3ED1F0E2AD106CED6C6 (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * __this, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 *, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void System.Comparison`1<Coffee.UIEffects.GraphicConnector>::.ctor(System.Object,System.IntPtr)
inline void Comparison_1__ctor_mAA9CF9E41CC6F86684E8C44A7BFD13D42CDE86E4 (Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A *, RuntimeObject *, intptr_t, const RuntimeMethod*))Comparison_1__ctor_mDE1798563694D30022D3B7E1010347C573690B4A_gshared)(__this, ___object0, ___method1, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector>::Sort(System.Comparison`1<!0>)
inline void List_1_Sort_m395F5F609C162FDEF77CB38D6161676BE5CB0A62 (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * __this, Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * ___comparison0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 *, Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A *, const RuntimeMethod*))List_1_Sort_m5EB3F127CD42F1ACA97F4DB8754C49F23B64D750_gshared)(__this, ___comparison0, method);
}
// System.Type System.Object::GetType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B (RuntimeObject * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<System.Type,Coffee.UIEffects.GraphicConnector>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m8CA53842365EB034411824904D39811C29D7BE17 (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * __this, Type_t * ___key0, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B *, Type_t *, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F **, const RuntimeMethod*))Dictionary_2_TryGetValue_m048C13E0F44BDC16F7CF01D14E918A84EE72C62C_gshared)(__this, ___key0, ___value1, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector>::GetEnumerator()
inline Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63  List_1_GetEnumerator_mF09ECF08D0194AACE9E36B4310774C83E03873DB (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63  (*) (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.GraphicConnector>::get_Current()
inline GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * Enumerator_get_Current_m26B8A870C49C4EBBEE8635F2B0848275E23E83DC_inline (Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 * __this, const RuntimeMethod* method)
{
	return ((  GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * (*) (Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Coffee.UIEffects.GraphicConnector>::Add(!0,!1)
inline void Dictionary_2_Add_m8E8BC37A23AC889755C202C425E13B2F7189C501 (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * __this, Type_t * ___key0, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B *, Type_t *, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F *, const RuntimeMethod*))Dictionary_2_Add_m830DC29CD6F7128D4990D460CCCDE032E3B693D9_gshared)(__this, ___key0, ___value1, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.GraphicConnector>::MoveNext()
inline bool Enumerator_MoveNext_mF7B2CA3A45C72E5E1DBE6349EA4256E795251B5F (Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.GraphicConnector>::Dispose()
inline void Enumerator_Dispose_mDC3D53B081A01539860E8FEC7989D90D94D6071E (Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A (String_t* ___name0, const RuntimeMethod* method);
// UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.GraphicConnector>::.ctor()
inline void List_1__ctor_m5EB82AEEC5544577504971655B371654980D6989 (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.Type,Coffee.UIEffects.GraphicConnector>::.ctor()
inline void Dictionary_2__ctor_mD138DC514ABD671F73D4141EB5D75C9BBE6E788C (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>::TryGetValue(!0,!1&)
inline bool Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019 (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * __this, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___key0, MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 ** ___value1, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 *, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A , MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 **, const RuntimeMethod*))Dictionary_2_TryGetValue_mB45814AFBB5AF9B75911CC4B96E79357B699C2A2_gshared)(__this, ___key0, ___value1, method);
}
// System.Void Coffee.UIEffects.MaterialCache/MaterialEntry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialEntry__ctor_m409D7E4C408065F797EFAC708ADD6836DDAD7FC8 (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___source0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>::Invoke(!0,!1)
inline void Action_2_Invoke_mD347DED36015B897E49370E126492889CE4CEF2A (Action_2_tF65C920A10937B33C65584F87099045F42BBB100 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___arg10, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___arg21, const RuntimeMethod* method)
{
	((  void (*) (Action_2_tF65C920A10937B33C65584F87099045F42BBB100 *, Material_t8927C00353A72755313F046D0CE85178AE8218EE *, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 *, const RuntimeMethod*))Action_2_Invoke_m54EE979C4D83695ED736A3177A68C2968C8C4382_gshared)(__this, ___arg10, ___arg21, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>::Add(!0,!1)
inline void Dictionary_2_Add_m3320410185FF9C49F194CC82EECEB845CF1FD719 (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * __this, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___key0, MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 *, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A , MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 *, const RuntimeMethod*))Dictionary_2_Add_m38F60974E87F145DD5F86F98C1641145C21FDF0E_gshared)(__this, ___key0, ___value1, method);
}
// System.Void Coffee.UIEffects.MaterialCache/MaterialEntry::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialEntry_Release_mB302360107E7F32A760184279DA7E3D24149498E (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>::Remove(!0)
inline bool Dictionary_2_Remove_mC6318BC7A4B155ECA98F5CF06F61F51674000E07 (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * __this, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___key0, const RuntimeMethod* method)
{
	return ((  bool (*) (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 *, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A , const RuntimeMethod*))Dictionary_2_Remove_m9A51B2B9CAA969C71C5FECED8AB6DCD05E4BFA3F_gshared)(__this, ___key0, method);
}
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Hash128,Coffee.UIEffects.MaterialCache/MaterialEntry>::.ctor()
inline void Dictionary_2__ctor_m949E2D644B0A3B0F74E1FD5193BCD30878145AE0 (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 *, const RuntimeMethod*))Dictionary_2__ctor_m7C68CDC635A0F3711B0287490895C8715C4F5254_gshared)(__this, method);
}
// System.Void Coffee.UIEffects.Matrix2x3::.ctor(UnityEngine.Rect,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE (Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rect0, float ___cos1, float ___sin2, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E (float ___f0, const RuntimeMethod* method);
// System.Single Packer::ToFloat(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_mA7D23F51E9C8A6E62848712A27E2309BC31F94A7 (float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Int32>::.ctor(System.Int32)
inline void Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3 (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 *, int32_t, const RuntimeMethod*))Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3_gshared)(__this, ___capacity0, method);
}
// System.Void System.Collections.Generic.Stack`1<System.Int32>::Push(!0)
inline void Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3 (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, int32_t ___item0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 *, int32_t, const RuntimeMethod*))Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_gshared)(__this, ___item0, method);
}
// System.Void Coffee.UIEffects.ParameterTexture::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_Initialize_m947A572C82BF5BB6BC7A84E95F3498401183F29A (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Stack`1<System.Int32>::get_Count()
inline int32_t Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_inline (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 *, const RuntimeMethod*))Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.Stack`1<System.Int32>::Pop()
inline int32_t Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 *, const RuntimeMethod*))Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C_gshared)(__this, method);
}
// System.Void Coffee.UIEffects.ParameterTexture::SetData(Coffee.UIEffects.IParameterTexture,System.Int32,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_SetData_m3870BC4215A7F85A68A38D2E6B50DB27D65284BB (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, int32_t ___channelId1, uint8_t ___value2, const RuntimeMethod* method);
// System.Int32 UnityEngine.Shader::PropertyToID(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F (String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, int32_t ___nameID0, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value1, const RuntimeMethod* method);
// UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t QualitySettings_get_activeColorSpace_m65BE7300D1A12D2981B492329B32673199CCE7F4 (const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, bool ___linear4, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_filterMode_m045141DB0FEFE496885D45F5F23B15BC0E77C8D0 (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture_set_wrapMode_m1233D2DF48DC20996F8EE26E866D4BDD2AC8050D (Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::LoadRawTextureData(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_LoadRawTextureData_m93A620CC97332F351305E3A93AD11CB2E0EFDAF4 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660 (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * __this, bool ___updateMipmaps0, bool ___makeNoLongerReadable1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55 (float ___a0, float ___b1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___lhs0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Texture Coffee.UIEffects.UIDissolve::get_defaultTransitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UIDissolve_get_defaultTransitionTexture_m16638E0BF5F9B8E7E089B2BAF0D5B5325CDBB0D4 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// !!0 UnityEngine.Resources::Load<UnityEngine.Texture>(System.String)
inline Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8 (String_t* ___path0, const RuntimeMethod* method)
{
	return ((  Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * (*) (String_t*, const RuntimeMethod*))Resources_Load_TisRuntimeObject_m39B6A35CFE684CD1FFF77873E20D7297B36A55E8_gshared)(___path0, method);
}
// System.Void Coffee.UIEffects.EffectPlayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, const RuntimeMethod* method);
// UnityEngine.Texture Coffee.UIEffects.UIDissolve::get_transitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Object::GetInstanceID()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Hash128::.ctor(System.UInt32,System.UInt32,System.UInt32,System.UInt32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Hash128__ctor_m4F9101AF4B79AB2FBC395F48875C21A4E2101581 (Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A * __this, uint32_t ___u32_00, uint32_t ___u32_11, uint32_t ___u32_22, uint32_t ___u32_33, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17 (String_t* ___format0, RuntimeObject * ___arg01, const RuntimeMethod* method);
// System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D (Material_t8927C00353A72755313F046D0CE85178AE8218EE * __this, Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * ___value0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMaterialEffect::SetShaderVariants(UnityEngine.Material,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___variants1, const RuntimeMethod* method);
// System.Single Coffee.UIEffects.ParameterTexture::GetNormalizedIndex(Coffee.UIEffects.IParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, const RuntimeMethod* method);
// UnityEngine.RectTransform Coffee.UIEffects.BaseMeshEffect::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// UnityEngine.Rect UnityEngine.RectTransform::get_rect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2 (RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * __this, const RuntimeMethod* method);
// UnityEngine.Rect Coffee.UIEffects.EffectAreaExtensions::GetEffectArea(Coffee.UIEffects.EffectArea,UnityEngine.UI.VertexHelper,UnityEngine.Rect,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7 (int32_t ___area0, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh1, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rectangle2, float ___aspectRatio3, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method);
// System.Single Packer::ToFloat(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662 (float ___x0, float ___y1, const RuntimeMethod* method);
// System.Single Packer::ToFloat(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_m7691266B0941E760FB34C506126EEAF8A8FBE1D7 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::SetUIVertex(UnityEngine.UIVertex,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  ___vertex0, int32_t ___i1, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.ParameterTexture::SetData(Coffee.UIEffects.IParameterTexture,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, int32_t ___channelId1, float ___value2, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMeshEffect::OnDidApplyAnimationProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method);
// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIDissolve::get_effectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.EffectPlayer::Play(System.Boolean,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, bool ___reset0, Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___callback1, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.EffectPlayer::Stop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_Stop_mCDDB096B24E282D85C931BC545CE0C03FE4AF357 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, bool ___reset0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMaterialEffect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Single>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3 (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_gshared)(__this, ___object0, ___method1, method);
}
// System.Void Coffee.UIEffects.EffectPlayer::OnEnable(System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___callback0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMaterialEffect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.EffectPlayer::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMaterialEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.ParameterTexture::.ctor(System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, int32_t ___channels0, int32_t ___instanceLimit1, String_t* ___propertyName2, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.UIDissolve::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_effectFactor_mDAD33D00CAF5DB3E62CFDFE65263D7A6E59C0FC2 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean Coffee.UIEffects.UIEffect::get_advancedBlur()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UIEffect_get_advancedBlur_m37BF17E16B82618676E23CF6338093ECA5AEB4AD_inline (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::GetUIVertexStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_GetUIVertexStream_mA3E62A7B45BFFFC73D72BC7B8BFAD5388F8578BA (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___stream0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.VertexHelper::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_Clear_mBF3FB3CEA5153F8F72C74FFD6006A7AFF62C18BA (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
inline int32_t List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, const RuntimeMethod*))List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_gshared_inline)(__this, method);
}
// Coffee.UIEffects.BlurMode Coffee.UIEffects.UIEffect::get_blurMode()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UIEffect_get_blurMode_m97D9F46E04E8B291EEB1E7ADCAE27842981FA6DD_inline (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.UIEffect::GetBounds(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Int32,System.Int32,UnityEngine.Rect&,UnityEngine.Rect&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_GetBounds_m35577243A9C58E39854A2538290A13D64C205BAD (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, int32_t ___start1, int32_t ___count2, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * ___posBounds3, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * ___uvBounds4, bool ___global5, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_xMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Rect::get_yMax()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
inline UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, int32_t, const RuntimeMethod*))List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_gshared_inline)(__this, ___index0, method);
}
// System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Rect_Contains_m51C65159B1706EB00CC962D7CD1CEC2EBD85BC3A (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector4_op_Implicit_m3A59F157B9B8A3C2DD495B6F9B76F3C0D40BDFCC (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___v0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,!0)
inline void List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3 (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___index0, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  ___value1, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, int32_t, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A , const RuntimeMethod*))List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_gshared)(__this, ___index0, ___value1, method);
}
// System.Void UnityEngine.UI.VertexHelper::AddUIVertexTriangleStream(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VertexHelper_AddUIVertexTriangleStream_m3FC7DF3D1DA3F0D40025258E3B8FF5830EE7CE55 (VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * __this, List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
inline void List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, const RuntimeMethod*))List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_gshared)(__this, method);
}
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// UnityEngine.RectTransform UnityEngine.UI.Graphic::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * Graphic_get_rectTransform_m87D5A808474C6B71649CBB153DEBF5F268189EFF (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_yMin(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_set_yMin_mA2FDFF7C8C2361A4CF3F446BAB9A861F923F763A (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_xMin(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_set_xMin_mC91AC74347F8E3D537E8C5D70015E9B8EA872A3F (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_yMax(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_set_yMax_m4E7A7C5E88FA369D6ED022C939427F4895F6635D (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::set_xMax(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rect_set_xMax_m4E466ED07B11CC5457BD62517418C493C0DDF2E3 (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * __this, float ___value0, const RuntimeMethod* method);
// System.Single Coffee.UIEffects.UIGradient::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIGradient_get_rotation_m77FADBF0C5FC07638849FBB32F7DC3739876108E (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_get_normalized_m1F7F7AA3B7AC2414F245395C3785880B847BF7F5 (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 Coffee.UIEffects.UIGradient::get_offset2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  UIGradient_get_offset2_m5B5F98FAADFFC9ACD34BC208000DCC0D0318507C (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method);
// Coffee.UIEffects.UIGradient/Direction Coffee.UIEffects.UIGradient::get_direction()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UIGradient_get_direction_m094F3EB30335D4C941FFE1C9CD92A1EA46FFD209_inline (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::LerpUnclamped(UnityEngine.Color,UnityEngine.Color,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_LerpUnclamped_m04B5A0416E75DC33705FCD18B0B453A2ACD50793 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___a0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___b1, float ___t2, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color32_op_Implicit_m63F14F1A14B1A9A3EE4D154413EE229D3E001623 (Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  ___c0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_linear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_gamma()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_gamma_mB6C6DA08F57C698AAB65B93F16B58F7C3F8F7E16 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_op_Multiply_mFD03CB228034C2D37F326B7AFF27C861E95447B7 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___a0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___b1, const RuntimeMethod* method);
// UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___c0, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_white()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E (const RuntimeMethod* method);
// System.Void UnityEngine.Color::RGBToHSV(UnityEngine.Color,System.Single&,System.Single&,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color_RGBToHSV_mDC3A14DCF9D4A898AF97613CD07D94BFF8402194 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___rgbColor0, float* ___H1, float* ___S2, float* ___V3, const RuntimeMethod* method);
// UnityEngine.Color UnityEngine.Color::get_red()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<Coffee.UIEffects.UIEffect>()
inline UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void Coffee.UIEffects.UIShadow::set_paramTex(Coffee.UIEffects.ParameterTexture)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___value0, const RuntimeMethod* method);
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShadow::get_paramTex()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Component::GetComponents<Coffee.UIEffects.UIShadow>(System.Collections.Generic.List`1<!!0>)
inline void Component_GetComponents_TisUIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_mE1D283405E85DB09AF57FA34A2F39D40B40FBC41 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * ___results0, const RuntimeMethod* method)
{
	((  void (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 *, const RuntimeMethod*))Component_GetComponents_TisRuntimeObject_mB0812CB4355C4F5E2EA27C7049C668609E72F31D_gshared)(__this, ___results0, method);
}
// System.Collections.Generic.List`1/Enumerator<!0> System.Collections.Generic.List`1<Coffee.UIEffects.UIShadow>::GetEnumerator()
inline Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361  List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462 (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361  (*) (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 *, const RuntimeMethod*))List_1_GetEnumerator_m1739A5E25DF502A6984F9B98CFCAC2D3FABCF233_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UIShadow>::get_Current()
inline UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_inline (Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 * __this, const RuntimeMethod* method)
{
	return ((  UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * (*) (Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UIShadow>::MoveNext()
inline bool Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932 (Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Coffee.UIEffects.UIShadow>::Dispose()
inline void Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75 (Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *, const RuntimeMethod*))Enumerator_Dispose_mCFB225D9E5E597A1CC8F958E53BEA1367D8AC7B8_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.UIShadow>::Clear()
inline void List_1_Clear_m75F5B96DFF821D80AF54DCCBF40A4C66163B3EE7 (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 *, const RuntimeMethod*))List_1_Clear_m5FB5A9C59D8625FDFB06876C4D8848F0F07ABFD0_gshared)(__this, method);
}
// System.Single Coffee.UIEffects.UIEffect::get_effectFactor()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float UIEffect_get_effectFactor_m00273C0BB3B80F99858764F8D80FD8D1BD7CB92E_inline (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method);
// UnityEngine.Color Coffee.UIEffects.UIShadow::get_effectColor()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 Coffee.UIEffects.UIShadow::get_effectDistance()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  UIShadow_get_effectDistance_m2AB1626DC40D34C0D91FF336342F938195B201EE_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method);
// Coffee.UIEffects.ShadowStyle Coffee.UIEffects.UIShadow::get_style()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UIShadow_get_style_mCAA0D48FB84802EBB12777BD82587CF6B1A20379_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method);
// System.Boolean Coffee.UIEffects.UIShadow::get_useGraphicAlpha()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UIShadow_get_useGraphicAlpha_mEC09F1BCF8D368129B771A94C129F048C7C0CE1E_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.UIShadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color,System.Int32&,System.Int32&,UnityEngine.Vector2,Coffee.UIEffects.ShadowStyle,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_ApplyShadow_m5422EC048717AA600AD8A06FCD534E94350034E1 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color1, int32_t* ___start2, int32_t* ___end3, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___distance4, int32_t ___style5, bool ___alpha6, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.UIShadow::ApplyShadowZeroAlloc(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color,System.Int32&,System.Int32&,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color1, int32_t* ___start2, int32_t* ___end3, float ___x4, float ___y5, bool ___alpha6, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
inline int32_t List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903 (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, const RuntimeMethod*))List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
inline void List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4 (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___value0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, int32_t, const RuntimeMethod*))List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4_gshared)(__this, ___value0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(!0)
inline void List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588 (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A , const RuntimeMethod*))List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.Vector3::Set(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3_Set_m12EA2C6DF9F94ABD0462F422A20959A53EED90D7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___newX0, float ___newY1, float ___newZ2, const RuntimeMethod* method);
// System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5 (Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * __this, float ___r0, float ___g1, float ___b2, float ___a3, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.UIShadow>::.ctor()
inline void List_1__ctor_m30A66B91842A107D2965D350964CD92B2D41584D (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 *, const RuntimeMethod*))List_1__ctor_m0F0E00088CF56FEACC9E32D8B7D91B93D91DAA3B_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
inline void List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118 (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *, int32_t, const RuntimeMethod*))List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118_gshared)(__this, ___capacity0, method);
}
// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIShiny::get_effectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.UIShiny::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_effectFactor_mDC9BF9072772D010621E6ECEEFA08070094F9C37 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// Coffee.UIEffects.BaseMeshEffect Coffee.UIEffects.UISyncEffect::get_targetEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect>::Add(!0)
inline void List_1_Add_m33273E2D2434DA63CFC046FA4864F2310CE5AAD3 (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * __this, UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F *, UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Boolean System.Collections.Generic.List`1<Coffee.UIEffects.UISyncEffect>::Remove(!0)
inline bool List_1_Remove_m905C6A86518BE8E31A16F5C7CAD8730B548CC554 (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * __this, UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * ___item0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F *, UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E *, const RuntimeMethod*))List_1_Remove_m753F7B4281CC4D02C07AE90726F51EF34B588DF7_gshared)(__this, ___item0, method);
}
// UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::get_defaultTransitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UITransitionEffect_get_defaultTransitionTexture_mB6C7677AF8930EA977477611EC747B8E44B9FE80 (const RuntimeMethod* method);
// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UITransitionEffect::get_effectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method);
// UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::get_transitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.UITransitionEffect::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_effectFactor_m6316BCC7814AE16693E998498FE3F199DEFE6BC7 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, float ___value0, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.BaseMaterialEffect/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mFFE62F74902632613ECF890912C74E4243194389 (U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * __this, const RuntimeMethod* method);
// System.String System.String::ToUpper()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_ToUpper_m4BC629F8059C3E0C4E3F7C7E04DB50EBB0C1A05A (String_t* __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.EffectPlayer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6D644EF6639B2C8942C4F547E344568976E93B5C (U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Action>::get_Count()
inline int32_t List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_inline (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, const RuntimeMethod*))List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.List`1<System.Action>::get_Item(System.Int32)
inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_inline (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * (*) (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.Void System.Action::Invoke()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * __this, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.GraphicConnector/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m827FF5CD8B256D37FC61A732312435C6F12041CE (U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_m69AA5B06236FACFF316DDFAD131B2622B397AC49 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, bool ___allowDestroyingAssets1, const RuntimeMethod* method);
// System.Void Coffee.UIEffects.ParameterTexture/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m685DAAD421CE9C891A571D72713AD4D024D5B079 (U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Coffee.UIEffects.BaseMaterialEffect::get_parameterIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t BaseMaterialEffect_get_parameterIndex_mCEAD6C474EC79C1D3F2E3B840905E10254EFE61F (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method)
{
	{
		// public int parameterIndex { get; set; }
		int32_t L_0 = __this->get_U3CparameterIndexU3Ek__BackingField_12();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::set_parameterIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_set_parameterIndex_m087EF0A73CE6D3519D94F9AD309EB3414A227524 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int parameterIndex { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CparameterIndexU3Ek__BackingField_12(L_0);
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * BaseMaterialEffect_get_paramTex_m102366D70593F2CD331F01163A461AD61BE46051 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method)
{
	{
		// get { return null; }
		return (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)NULL;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::SetMaterialDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// connector.SetMaterialDirty(graphic);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0;
		L_0 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1;
		L_1 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(11 /* System.Void Coffee.UIEffects.GraphicConnector::SetMaterialDirty(UnityEngine.UI.Graphic) */, L_0, L_1);
		// foreach (var effect in syncEffects)
		List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * L_2 = ((BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A *)__this)->get_syncEffects_7();
		NullCheck(L_2);
		Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B  L_3;
		L_3 = List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A(L_2, /*hidden argument*/List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A_RuntimeMethod_var);
		V_0 = L_3;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001f:
		{
			// foreach (var effect in syncEffects)
			UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * L_4;
			L_4 = Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_inline((Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *)(&V_0), /*hidden argument*/Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_RuntimeMethod_var);
			// effect.SetMaterialDirty();
			NullCheck(L_4);
			BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(L_4, /*hidden argument*/NULL);
		}

IL_002b:
		{
			// foreach (var effect in syncEffects)
			bool L_5;
			L_5 = Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5((Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_001f;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446((Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *)(&V_0), /*hidden argument*/Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446_RuntimeMethod_var);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x44, IL_0044)
	}

IL_0044:
	{
		// }
		return;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.BaseMaterialEffect::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  BaseMaterialEffect_GetMaterialHash_m56C3761346414A62F442ECB7A389A4F8844744D1 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_0 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_0;
	}
}
// UnityEngine.Material Coffee.UIEffects.BaseMaterialEffect::GetModifiedMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * BaseMaterialEffect_GetModifiedMaterial_mABA10CC6BFFDFD3A82EEFF3D9156AFD08D071B1A (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, const RuntimeMethod* method)
{
	{
		// return GetModifiedMaterial(baseMaterial, graphic);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = ___baseMaterial0;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1;
		L_1 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2;
		L_2 = VirtFuncInvoker2< Material_t8927C00353A72755313F046D0CE85178AE8218EE *, Material_t8927C00353A72755313F046D0CE85178AE8218EE *, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(30 /* UnityEngine.Material Coffee.UIEffects.BaseMaterialEffect::GetModifiedMaterial(UnityEngine.Material,UnityEngine.UI.Graphic) */, __this, L_0, L_1);
		return L_2;
	}
}
// UnityEngine.Material Coffee.UIEffects.BaseMaterialEffect::GetModifiedMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * BaseMaterialEffect_GetModifiedMaterial_mB36BD87123ED2DC4AF906BE069DCAD95AC1A54ED (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2__ctor_m160CB1F6CF097F0AF0BD833BA54178EF5ADBA5F4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_tF65C920A10937B33C65584F87099045F42BBB100_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * V_0 = NULL;
	Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  G_B4_0;
	memset((&G_B4_0), 0, sizeof(G_B4_0));
	Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  G_B3_0;
	memset((&G_B3_0), 0, sizeof(G_B3_0));
	{
		// if (!isActiveAndEnabled) return baseMaterial;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000a;
		}
	}
	{
		// if (!isActiveAndEnabled) return baseMaterial;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___baseMaterial0;
		return L_1;
	}

IL_000a:
	{
		// var oldHash = _effectMaterialHash;
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_2 = __this->get__effectMaterialHash_11();
		// _effectMaterialHash = GetMaterialHash(baseMaterial);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___baseMaterial0;
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_4;
		L_4 = VirtFuncInvoker1< Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A , Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(29 /* UnityEngine.Hash128 Coffee.UIEffects.BaseMaterialEffect::GetMaterialHash(UnityEngine.Material) */, __this, L_3);
		__this->set__effectMaterialHash_11(L_4);
		// var modifiedMaterial = baseMaterial;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = ___baseMaterial0;
		V_0 = L_5;
		// if (_effectMaterialHash.isValid)
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A * L_6 = __this->get_address_of__effectMaterialHash_11();
		bool L_7;
		L_7 = Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB((Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A *)L_6, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		if (!L_7)
		{
			G_B4_0 = L_2;
			goto IL_0047;
		}
	}
	{
		// modifiedMaterial = MaterialCache.Register(baseMaterial, _effectMaterialHash, ModifyMaterial, graphic);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = ___baseMaterial0;
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_9 = __this->get__effectMaterialHash_11();
		Action_2_tF65C920A10937B33C65584F87099045F42BBB100 * L_10 = (Action_2_tF65C920A10937B33C65584F87099045F42BBB100 *)il2cpp_codegen_object_new(Action_2_tF65C920A10937B33C65584F87099045F42BBB100_il2cpp_TypeInfo_var);
		Action_2__ctor_m160CB1F6CF097F0AF0BD833BA54178EF5ADBA5F4(L_10, __this, (intptr_t)((intptr_t)GetVirtualMethodInfo(__this, 31)), /*hidden argument*/Action_2__ctor_m160CB1F6CF097F0AF0BD833BA54178EF5ADBA5F4_RuntimeMethod_var);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_11 = ___graphic1;
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_12;
		L_12 = MaterialCache_Register_m41B9F13914C2BBF0F30CC45DBF8E8713E15079F8(L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		G_B4_0 = G_B3_0;
	}

IL_0047:
	{
		// MaterialCache.Unregister(oldHash);
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		MaterialCache_Unregister_m5F0CFE2E6BFD7A6D3E0862D6034B1CB4F3A5B394(G_B4_0, /*hidden argument*/NULL);
		// return modifiedMaterial;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_13 = V_0;
		return L_13;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_ModifyMaterial_m5B22D41500FF33965FDC9E4843527EC5C535BCD6 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	{
		// if (isActiveAndEnabled && paramTex != null)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		// paramTex.RegisterMaterial(newMaterial);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_2;
		L_2 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___newMaterial0;
		NullCheck(L_2);
		ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9(L_2, L_3, /*hidden argument*/NULL);
	}

IL_001c:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::SetShaderVariants(UnityEngine.Material,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___variants1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Concat_TisString_t_m7C95F30602BBABF6D41F782A16AE43477A957F3D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Distinct_TisString_t_m0980366EB945AB2A9F39B3AC0E03CF634F6CAA94_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Select_TisRuntimeObject_TisString_t_m540DAFC430ED92CA1E45E595E529F4A7CC4D7574_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_m5DB0F7F826FFCB704F566795D5DA5A1F5ABD313D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CSetShaderVariantsU3Eb__15_0_m86A0BD07D9513900697A9A21089709A5AE45B974_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CSetShaderVariantsU3Eb__15_1_mB4C34D6B741E7313073BF9AA37C633F2A40B3DB3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral3B2C1C62D4D1C2A0C8A9AC42DB00D33C654F9AD0);
		s_Il2CppMethodInitialized = true;
	}
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_0 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_1 = NULL;
	int32_t V_2 = 0;
	String_t* V_3 = NULL;
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * G_B2_0 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B2_1 = NULL;
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * G_B1_0 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B1_1 = NULL;
	Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * G_B4_0 = NULL;
	RuntimeObject* G_B4_1 = NULL;
	Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * G_B3_0 = NULL;
	RuntimeObject* G_B3_1 = NULL;
	{
		// var keywords = variants.Where(x => 0 < (int) x)
		//     .Select(x => x.ToString().ToUpper())
		//     .Concat(newMaterial.shaderKeywords)
		//     .Distinct()
		//     .ToArray();
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = ___variants1;
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->get_U3CU3E9__15_0_1();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0020;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * L_3 = ((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_4 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)il2cpp_codegen_object_new(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8_il2cpp_TypeInfo_var);
		Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C(L_4, L_3, (intptr_t)((intptr_t)U3CU3Ec_U3CSetShaderVariantsU3Eb__15_0_m86A0BD07D9513900697A9A21089709A5AE45B974_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_mCA84157864A199574AD0B7F3083F99B54DC1F98C_RuntimeMethod_var);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_5 = L_4;
		((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->set_U3CU3E9__15_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0020:
	{
		RuntimeObject* L_6;
		L_6 = Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364((RuntimeObject*)(RuntimeObject*)G_B2_1, G_B2_0, /*hidden argument*/Enumerable_Where_TisRuntimeObject_m51DA29A5CB10D532C42135ADA3270F6E695B9364_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * L_7 = ((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->get_U3CU3E9__15_1_2();
		Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * L_8 = L_7;
		G_B3_0 = L_8;
		G_B3_1 = L_6;
		if (L_8)
		{
			G_B4_0 = L_8;
			G_B4_1 = L_6;
			goto IL_0044;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * L_9 = ((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * L_10 = (Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 *)il2cpp_codegen_object_new(Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82_il2cpp_TypeInfo_var);
		Func_2__ctor_m5DB0F7F826FFCB704F566795D5DA5A1F5ABD313D(L_10, L_9, (intptr_t)((intptr_t)U3CU3Ec_U3CSetShaderVariantsU3Eb__15_1_mB4C34D6B741E7313073BF9AA37C633F2A40B3DB3_RuntimeMethod_var), /*hidden argument*/Func_2__ctor_m5DB0F7F826FFCB704F566795D5DA5A1F5ABD313D_RuntimeMethod_var);
		Func_2_t060A650AB95DEF14D4F579FA5999ACEFEEE0FD82 * L_11 = L_10;
		((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->set_U3CU3E9__15_1_2(L_11);
		G_B4_0 = L_11;
		G_B4_1 = G_B3_1;
	}

IL_0044:
	{
		RuntimeObject* L_12;
		L_12 = Enumerable_Select_TisRuntimeObject_TisString_t_m540DAFC430ED92CA1E45E595E529F4A7CC4D7574(G_B4_1, G_B4_0, /*hidden argument*/Enumerable_Select_TisRuntimeObject_TisString_t_m540DAFC430ED92CA1E45E595E529F4A7CC4D7574_RuntimeMethod_var);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_13 = ___newMaterial0;
		NullCheck(L_13);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_14;
		L_14 = Material_get_shaderKeywords_mDAD743C090C3CEE0B2883140B244853D71C5E9E0(L_13, /*hidden argument*/NULL);
		RuntimeObject* L_15;
		L_15 = Enumerable_Concat_TisString_t_m7C95F30602BBABF6D41F782A16AE43477A957F3D(L_12, (RuntimeObject*)(RuntimeObject*)L_14, /*hidden argument*/Enumerable_Concat_TisString_t_m7C95F30602BBABF6D41F782A16AE43477A957F3D_RuntimeMethod_var);
		RuntimeObject* L_16;
		L_16 = Enumerable_Distinct_TisString_t_m0980366EB945AB2A9F39B3AC0E03CF634F6CAA94(L_15, /*hidden argument*/Enumerable_Distinct_TisString_t_m0980366EB945AB2A9F39B3AC0E03CF634F6CAA94_RuntimeMethod_var);
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_17;
		L_17 = Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582(L_16, /*hidden argument*/Enumerable_ToArray_TisString_t_mE824E1F8EB2A50DC8E24291957CBEED8C356E582_RuntimeMethod_var);
		V_0 = L_17;
		// newMaterial.shaderKeywords = keywords;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = ___newMaterial0;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_19 = V_0;
		NullCheck(L_18);
		Material_set_shaderKeywords_m9EC5EFA52BF30597B1692C623806E7167B1C7688(L_18, L_19, /*hidden argument*/NULL);
		// s_StringBuilder.Length = 0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		StringBuilder_t * L_20 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_StringBuilder_10();
		NullCheck(L_20);
		StringBuilder_set_Length_m7C1756193B05DCA5A23C5DC98EE90A9FC685A27A(L_20, 0, /*hidden argument*/NULL);
		// s_StringBuilder.Append(Path.GetFileName(newMaterial.shader.name));
		StringBuilder_t * L_21 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_StringBuilder_10();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_22 = ___newMaterial0;
		NullCheck(L_22);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_23;
		L_23 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_22, /*hidden argument*/NULL);
		NullCheck(L_23);
		String_t* L_24;
		L_24 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		String_t* L_25;
		L_25 = Path_GetFileName_m4CCFEEE8CA42DE16FCAF05D765EFB88E7086744A(L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_t * L_26;
		L_26 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_21, L_25, /*hidden argument*/NULL);
		// foreach (var keyword in keywords)
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_27 = V_0;
		V_1 = L_27;
		V_2 = 0;
		goto IL_00b6;
	}

IL_0092:
	{
		// foreach (var keyword in keywords)
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_28 = V_1;
		int32_t L_29 = V_2;
		NullCheck(L_28);
		int32_t L_30 = L_29;
		String_t* L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		V_3 = L_31;
		// s_StringBuilder.Append("-");
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		StringBuilder_t * L_32 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_StringBuilder_10();
		NullCheck(L_32);
		StringBuilder_t * L_33;
		L_33 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_32, _stringLiteral3B2C1C62D4D1C2A0C8A9AC42DB00D33C654F9AD0, /*hidden argument*/NULL);
		// s_StringBuilder.Append(keyword);
		StringBuilder_t * L_34 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_StringBuilder_10();
		String_t* L_35 = V_3;
		NullCheck(L_34);
		StringBuilder_t * L_36;
		L_36 = StringBuilder_Append_mD02AB0C74C6F55E3E330818C77EC147E22096FB1(L_34, L_35, /*hidden argument*/NULL);
		int32_t L_37 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)1));
	}

IL_00b6:
	{
		// foreach (var keyword in keywords)
		int32_t L_38 = V_2;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_39 = V_1;
		NullCheck(L_39);
		if ((((int32_t)L_38) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_39)->max_length))))))
		{
			goto IL_0092;
		}
	}
	{
		// newMaterial.name = s_StringBuilder.ToString();
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_40 = ___newMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		StringBuilder_t * L_41 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_StringBuilder_10();
		NullCheck(L_41);
		String_t* L_42;
		L_42 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_41);
		NullCheck(L_40);
		Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781(L_40, L_42, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method)
{
	{
		// base.OnEnable();
		BaseMeshEffect_OnEnable_m900129F36764E412C7FCE57A7C00DB091F48F4C6(__this, /*hidden argument*/NULL);
		// if (paramTex != null)
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		// paramTex.Register(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		ParameterTexture_Register_m418624C23515BB36BFE2E30FDC66AB2DD855D62A(L_1, __this, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnDisable();
		BaseMeshEffect_OnDisable_m714D32A1FD3155B874FD836C76954208747FED85(__this, /*hidden argument*/NULL);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// if (paramTex != null)
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		// paramTex.Unregister(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		ParameterTexture_Unregister_mC8283ADEEC7D0B15321727EC445C0909671918BC(L_1, __this, /*hidden argument*/NULL);
	}

IL_0020:
	{
		// MaterialCache.Unregister(_effectMaterialHash);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_2 = __this->get__effectMaterialHash_11();
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		MaterialCache_Unregister_m5F0CFE2E6BFD7A6D3E0862D6034B1CB4F3A5B394(L_2, /*hidden argument*/NULL);
		// _effectMaterialHash = k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_3 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		__this->set__effectMaterialHash_11(L_3);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497 (BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * __this, const RuntimeMethod* method)
{
	{
		BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMaterialEffect__cctor_mE50CD52D2F92D00BB3AD6A076F045C83F8878203 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringBuilder_t_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// protected static readonly Hash128 k_InvalidHash = new Hash128();
		il2cpp_codegen_initobj((((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_address_of_k_InvalidHash_8()), sizeof(Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A ));
		// protected static readonly List<UIVertex> s_TempVerts = new List<UIVertex>();
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_0 = (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *)il2cpp_codegen_object_new(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_il2cpp_TypeInfo_var);
		List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC(L_0, /*hidden argument*/List_1__ctor_m66CF20877114338DBA82AEDF855E0DF0CD2FE8EC_RuntimeMethod_var);
		((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->set_s_TempVerts_9(L_0);
		// private static readonly StringBuilder s_StringBuilder = new StringBuilder();
		StringBuilder_t * L_1 = (StringBuilder_t *)il2cpp_codegen_object_new(StringBuilder_t_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m5A81DE19E748F748E19FF13FB6FFD2547F9212D9(L_1, /*hidden argument*/NULL);
		((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->set_s_StringBuilder_10(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Coffee.UIEffects.GraphicConnector Coffee.UIEffects.BaseMeshEffect::get_connector()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * V_0 = NULL;
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * G_B2_0 = NULL;
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * G_B1_0 = NULL;
	{
		// get { return _connector ?? (_connector = GraphicConnector.FindConnector(graphic)); }
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0 = __this->get__connector_6();
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_001e;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2;
		L_2 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_3;
		L_3 = GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0(L_2, /*hidden argument*/NULL);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_4 = L_3;
		V_0 = L_4;
		__this->set__connector_6(L_4);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_5 = V_0;
		G_B2_0 = L_5;
	}

IL_001e:
	{
		return G_B2_0;
	}
}
// UnityEngine.UI.Graphic Coffee.UIEffects.BaseMeshEffect::get_graphic()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * V_0 = NULL;
	{
		// get { return _graphic ? _graphic : _graphic = GetComponent<Graphic>(); }
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = __this->get__graphic_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2;
		L_2 = Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1(__this, /*hidden argument*/Component_GetComponent_TisGraphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_mC2B96FBBFDBEB7FC16A23436F3C7A3C2740CAAA1_RuntimeMethod_var);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_3 = L_2;
		V_0 = L_3;
		__this->set__graphic_5(L_3);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_4 = V_0;
		return L_4;
	}

IL_001d:
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_5 = __this->get__graphic_5();
		return L_5;
	}
}
// UnityEngine.RectTransform Coffee.UIEffects.BaseMeshEffect::get_rectTransform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * V_0 = NULL;
	{
		// get { return _rectTransform ? _rectTransform : _rectTransform = GetComponent<RectTransform>(); }
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_0 = __this->get__rectTransform_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_2;
		L_2 = Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79(__this, /*hidden argument*/Component_GetComponent_TisRectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_m98D387B909AC36B37BF964576557C064222B3C79_RuntimeMethod_var);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_3 = L_2;
		V_0 = L_3;
		__this->set__rectTransform_4(L_3);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4 = V_0;
		return L_4;
	}

IL_001d:
	{
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_5 = __this->get__rectTransform_4();
		return L_5;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.Mesh)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_ModifyMesh_mD039BBBF805029CDB8980BC571AF1C8EEA5DE77B (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___mesh0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_ModifyMesh_m5ED9F4CB50A4155F08EBF97C991240485AD2E794 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, const RuntimeMethod* method)
{
	{
		// ModifyMesh(vh, graphic);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_0 = ___vh0;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1;
		L_1 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		VirtActionInvoker2< VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 *, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(21 /* System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic) */, __this, L_0, L_1);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_ModifyMesh_m6EE8AFAC5195AFA3B2B0073E95ACB45FF3838912 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// connector.SetVerticesDirty(graphic);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0;
		L_0 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1;
		L_1 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(10 /* System.Void Coffee.UIEffects.GraphicConnector::SetVerticesDirty(UnityEngine.UI.Graphic) */, L_0, L_1);
		// foreach (var effect in syncEffects)
		List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * L_2 = __this->get_syncEffects_7();
		NullCheck(L_2);
		Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B  L_3;
		L_3 = List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A(L_2, /*hidden argument*/List_1_GetEnumerator_mB07D9E8C24A5B557E8DC9CB90FF3BA10BEB63F9A_RuntimeMethod_var);
		V_0 = L_3;
	}

IL_001d:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002b;
		}

IL_001f:
		{
			// foreach (var effect in syncEffects)
			UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * L_4;
			L_4 = Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_inline((Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *)(&V_0), /*hidden argument*/Enumerator_get_Current_m68B7DC77E17C5624D673C065FD4E73EE7598089A_RuntimeMethod_var);
			// effect.SetVerticesDirty();
			NullCheck(L_4);
			VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, L_4);
		}

IL_002b:
		{
			// foreach (var effect in syncEffects)
			bool L_5;
			L_5 = Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5((Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *)(&V_0), /*hidden argument*/Enumerator_MoveNext_mC04A4A622FDC121FD4AE3F3C949394880137E2B5_RuntimeMethod_var);
			if (L_5)
			{
				goto IL_001f;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x44, FINALLY_0036);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0036;
	}

FINALLY_0036:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446((Enumerator_t68FD0F06BE498A2294852A1342A323869B99108B *)(&V_0), /*hidden argument*/Enumerator_Dispose_m53A19C425E4ACD887AE41F384EFF42429F9E0446_RuntimeMethod_var);
		IL2CPP_END_FINALLY(54)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(54)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x44, IL_0044)
	}

IL_0044:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_OnEnable_m900129F36764E412C7FCE57A7C00DB091F48F4C6 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	{
		// connector.OnEnable(graphic);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0;
		L_0 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1;
		L_1 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(8 /* System.Void Coffee.UIEffects.GraphicConnector::OnEnable(UnityEngine.UI.Graphic) */, L_0, L_1);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_OnDisable_m714D32A1FD3155B874FD836C76954208747FED85 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	{
		// connector.OnDisable(graphic);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0;
		L_0 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_1;
		L_1 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(9 /* System.Void Coffee.UIEffects.GraphicConnector::OnDisable(UnityEngine.UI.Graphic) */, L_0, L_1);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_SetEffectParamsDirty_m73280E2344924B24DC679377A461ED67FAFCEA19 (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	{
		// if (!isActiveAndEnabled) return;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!isActiveAndEnabled) return;
		return;
	}

IL_0009:
	{
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::OnDidApplyAnimationProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	{
		// if (!isActiveAndEnabled) return;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!isActiveAndEnabled) return;
		return;
	}

IL_0009:
	{
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMeshEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m2C641C171CDB35066D77221A29BF02745472353F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t01BE5878AE01033101C05CE072C892141BBDA54F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// internal readonly List<UISyncEffect> syncEffects = new List<UISyncEffect>(0);
		List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * L_0 = (List_1_t01BE5878AE01033101C05CE072C892141BBDA54F *)il2cpp_codegen_object_new(List_1_t01BE5878AE01033101C05CE072C892141BBDA54F_il2cpp_TypeInfo_var);
		List_1__ctor_m2C641C171CDB35066D77221A29BF02745472353F(L_0, 0, /*hidden argument*/List_1__ctor_m2C641C171CDB35066D77221A29BF02745472353F_RuntimeMethod_var);
		__this->set_syncEffects_7(L_0);
		UIBehaviour__ctor_m869436738107AF382FD4D10DE9641F8241B323C7(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Rect Coffee.UIEffects.EffectAreaExtensions::GetEffectArea(Coffee.UIEffects.EffectArea,UnityEngine.UI.VertexHelper,UnityEngine.Rect,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7 (int32_t ___area0, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh1, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rectangle2, float ___aspectRatio3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_0;
	memset((&V_0), 0, sizeof(V_0));
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	{
		// Rect rect = default(Rect);
		il2cpp_codegen_initobj((&V_0), sizeof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 ));
		int32_t L_0 = ___area0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001f;
			}
			case 1:
			{
				goto IL_0031;
			}
			case 2:
			{
				goto IL_0026;
			}
		}
	}
	{
		goto IL_00c7;
	}

IL_001f:
	{
		// rect = rectangle;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_1 = ___rectangle2;
		V_0 = L_1;
		// break;
		goto IL_00c9;
	}

IL_0026:
	{
		// rect = rectForCharacter;
		IL2CPP_RUNTIME_CLASS_INIT(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_2 = ((EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields*)il2cpp_codegen_static_fields_for(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var))->get_rectForCharacter_0();
		V_0 = L_2;
		// break;
		goto IL_00c9;
	}

IL_0031:
	{
		// UIVertex vertex = default(UIVertex);
		il2cpp_codegen_initobj((&V_1), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// float xMin = float.MaxValue;
		V_2 = ((std::numeric_limits<float>::max)());
		// float yMin = float.MaxValue;
		V_3 = ((std::numeric_limits<float>::max)());
		// float xMax = float.MinValue;
		V_4 = (-(std::numeric_limits<float>::max)());
		// float yMax = float.MinValue;
		V_5 = (-(std::numeric_limits<float>::max)());
		// for (int i = 0; i < vh.currentVertCount; i++)
		V_6 = 0;
		goto IL_00aa;
	}

IL_0058:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_3 = ___vh1;
		int32_t L_4 = V_6;
		NullCheck(L_3);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_3, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_1), L_4, /*hidden argument*/NULL);
		// float x = vertex.position.x;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_5 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = L_5.get_position_0();
		float L_7 = L_6.get_x_2();
		V_7 = L_7;
		// float y = vertex.position.y;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_8 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = L_8.get_position_0();
		float L_10 = L_9.get_y_3();
		V_8 = L_10;
		// xMin = Mathf.Min(xMin, x);
		float L_11 = V_2;
		float L_12 = V_7;
		float L_13;
		L_13 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_11, L_12, /*hidden argument*/NULL);
		V_2 = L_13;
		// yMin = Mathf.Min(yMin, y);
		float L_14 = V_3;
		float L_15 = V_8;
		float L_16;
		L_16 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		// xMax = Mathf.Max(xMax, x);
		float L_17 = V_4;
		float L_18 = V_7;
		float L_19;
		L_19 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_17, L_18, /*hidden argument*/NULL);
		V_4 = L_19;
		// yMax = Mathf.Max(yMax, y);
		float L_20 = V_5;
		float L_21 = V_8;
		float L_22;
		L_22 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_20, L_21, /*hidden argument*/NULL);
		V_5 = L_22;
		// for (int i = 0; i < vh.currentVertCount; i++)
		int32_t L_23 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_23, (int32_t)1));
	}

IL_00aa:
	{
		// for (int i = 0; i < vh.currentVertCount; i++)
		int32_t L_24 = V_6;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_25 = ___vh1;
		NullCheck(L_25);
		int32_t L_26;
		L_26 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_25, /*hidden argument*/NULL);
		if ((((int32_t)L_24) < ((int32_t)L_26)))
		{
			goto IL_0058;
		}
	}
	{
		// rect.Set(xMin, yMin, xMax - xMin, yMax - yMin);
		float L_27 = V_2;
		float L_28 = V_3;
		float L_29 = V_4;
		float L_30 = V_2;
		float L_31 = V_5;
		float L_32 = V_3;
		Rect_Set_mACEFC5D6FC4E52EDE4480B222485B1FB49951F5B((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_27, L_28, ((float)il2cpp_codegen_subtract((float)L_29, (float)L_30)), ((float)il2cpp_codegen_subtract((float)L_31, (float)L_32)), /*hidden argument*/NULL);
		// break;
		goto IL_00c9;
	}

IL_00c7:
	{
		// rect = rectangle;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_33 = ___rectangle2;
		V_0 = L_33;
	}

IL_00c9:
	{
		// if (0 < aspectRatio)
		float L_34 = ___aspectRatio3;
		if ((!(((float)(0.0f)) < ((float)L_34))))
		{
			goto IL_0103;
		}
	}
	{
		// if (rect.width < rect.height)
		float L_35;
		L_35 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		float L_36;
		L_36 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((float)L_35) < ((float)L_36))))
		{
			goto IL_00f3;
		}
	}
	{
		// rect.width = rect.height * aspectRatio;
		float L_37;
		L_37 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		float L_38 = ___aspectRatio3;
		Rect_set_width_m07D84AD7C7093EDCCD94A7B93A9447CA9917DD9D((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), ((float)il2cpp_codegen_multiply((float)L_37, (float)L_38)), /*hidden argument*/NULL);
		// }
		goto IL_0103;
	}

IL_00f3:
	{
		// rect.height = rect.width / aspectRatio;
		float L_39;
		L_39 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		float L_40 = ___aspectRatio3;
		Rect_set_height_m4A00B16C122F44FEF4BA074386F3DC11FF4B4D23((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), ((float)((float)L_39/(float)L_40)), /*hidden argument*/NULL);
	}

IL_0103:
	{
		// return rect;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_41 = V_0;
		return L_41;
	}
}
// System.Void Coffee.UIEffects.EffectAreaExtensions::GetPositionFactor(Coffee.UIEffects.EffectArea,System.Int32,UnityEngine.Rect,UnityEngine.Vector2,System.Boolean,System.Boolean,System.Single&,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectAreaExtensions_GetPositionFactor_m133BD727A155DAC41FE3FD89F266BA037EEA56AF (int32_t ___area0, int32_t ___index1, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rect2, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___position3, bool ___isText4, bool ___isTMPro5, float* ___x6, float* ___y7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		// if (isText && area == EffectArea.Character)
		bool L_0 = ___isText4;
		if (!L_0)
		{
			goto IL_003f;
		}
	}
	{
		int32_t L_1 = ___area0;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_003f;
		}
	}
	{
		// index = isTMPro ? (index + 3) % 4 : index % 4;
		bool L_2 = ___isTMPro5;
		if (L_2)
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_3 = ___index1;
		G_B5_0 = ((int32_t)((int32_t)L_3%(int32_t)4));
		goto IL_0016;
	}

IL_0011:
	{
		int32_t L_4 = ___index1;
		G_B5_0 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)3))%(int32_t)4));
	}

IL_0016:
	{
		___index1 = G_B5_0;
		// x = splitedCharacterPosition[index].x;
		float* L_5 = ___x6;
		IL2CPP_RUNTIME_CLASS_INIT(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_6 = ((EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields*)il2cpp_codegen_static_fields_for(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var))->get_splitedCharacterPosition_1();
		int32_t L_7 = ___index1;
		NullCheck(L_6);
		float L_8 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_7)))->get_x_0();
		*((float*)L_5) = (float)L_8;
		// y = splitedCharacterPosition[index].y;
		float* L_9 = ___y7;
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_10 = ((EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields*)il2cpp_codegen_static_fields_for(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var))->get_splitedCharacterPosition_1();
		int32_t L_11 = ___index1;
		NullCheck(L_10);
		float L_12 = ((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_y_1();
		*((float*)L_9) = (float)L_12;
		// }
		return;
	}

IL_003f:
	{
		// else if (area == EffectArea.Fit)
		int32_t L_13 = ___area0;
		if ((!(((uint32_t)L_13) == ((uint32_t)1))))
		{
			goto IL_0080;
		}
	}
	{
		// x = Mathf.Clamp01((position.x - rect.xMin) / rect.width);
		float* L_14 = ___x6;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_15 = ___position3;
		float L_16 = L_15.get_x_0();
		float L_17;
		L_17 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_18;
		L_18 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_19;
		L_19 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)((float)il2cpp_codegen_subtract((float)L_16, (float)L_17))/(float)L_18)), /*hidden argument*/NULL);
		*((float*)L_14) = (float)L_19;
		// y = Mathf.Clamp01((position.y - rect.yMin) / rect.height);
		float* L_20 = ___y7;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_21 = ___position3;
		float L_22 = L_21.get_y_1();
		float L_23;
		L_23 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_24;
		L_24 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_25;
		L_25 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)((float)il2cpp_codegen_subtract((float)L_22, (float)L_23))/(float)L_24)), /*hidden argument*/NULL);
		*((float*)L_20) = (float)L_25;
		// }
		return;
	}

IL_0080:
	{
		// x = Mathf.Clamp01(position.x / rect.width + 0.5f);
		float* L_26 = ___x6;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_27 = ___position3;
		float L_28 = L_27.get_x_0();
		float L_29;
		L_29 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_30;
		L_30 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)il2cpp_codegen_add((float)((float)((float)L_28/(float)L_29)), (float)(0.5f))), /*hidden argument*/NULL);
		*((float*)L_26) = (float)L_30;
		// y = Mathf.Clamp01(position.y / rect.height + 0.5f);
		float* L_31 = ___y7;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_32 = ___position3;
		float L_33 = L_32.get_y_1();
		float L_34;
		L_34 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_35;
		L_35 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)il2cpp_codegen_add((float)((float)((float)L_33/(float)L_34)), (float)(0.5f))), /*hidden argument*/NULL);
		*((float*)L_31) = (float)L_35;
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectAreaExtensions::GetNormalizedFactor(Coffee.UIEffects.EffectArea,System.Int32,Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2,System.Boolean,UnityEngine.Vector2&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectAreaExtensions_GetNormalizedFactor_mE54719FA9D4F34F2EEAF3418B47B37E3F0DA67FF (int32_t ___area0, int32_t ___index1, Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  ___matrix2, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___position3, bool ___isText4, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * ___nomalizedPos5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (isText && area == EffectArea.Character)
		bool L_0 = ___isText4;
		if (!L_0)
		{
			goto IL_0025;
		}
	}
	{
		int32_t L_1 = ___area0;
		if ((!(((uint32_t)L_1) == ((uint32_t)2))))
		{
			goto IL_0025;
		}
	}
	{
		// nomalizedPos = matrix * splitedCharacterPosition[(index + 3) % 4];
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = ___nomalizedPos5;
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_3 = ___matrix2;
		IL2CPP_RUNTIME_CLASS_INIT(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_4 = ((EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields*)il2cpp_codegen_static_fields_for(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var))->get_splitedCharacterPosition_1();
		int32_t L_5 = ___index1;
		NullCheck(L_4);
		int32_t L_6 = ((int32_t)((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)3))%(int32_t)4));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0(L_3, L_7, /*hidden argument*/NULL);
		*(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_2 = L_8;
		// }
		return;
	}

IL_0025:
	{
		// nomalizedPos = matrix * position;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_9 = ___nomalizedPos5;
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_10 = ___matrix2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11 = ___position3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12;
		L_12 = Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0(L_10, L_11, /*hidden argument*/NULL);
		*(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_9 = L_12;
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectAreaExtensions::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectAreaExtensions__cctor_m62D31F3EED0577276B9296CD675BEE743245ABE5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static readonly Rect rectForCharacter = new Rect(0, 0, 1, 1);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70((&L_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		((EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields*)il2cpp_codegen_static_fields_for(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var))->set_rectForCharacter_0(L_0);
		// static readonly Vector2[] splitedCharacterPosition = {Vector2.up, Vector2.one, Vector2.right, Vector2.zero};
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_1 = (Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)SZArrayNew(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_2 = L_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C(/*hidden argument*/NULL);
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_3);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_4 = L_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5;
		L_5 = Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42(/*hidden argument*/NULL);
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_5);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_6 = L_4;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_7;
		L_7 = Vector2_get_right_m42ED15112D219375D2B6879E62ED925D002F15AF(/*hidden argument*/NULL);
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_7);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_8 = L_6;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9;
		L_9 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		NullCheck(L_8);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_9);
		((EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_StaticFields*)il2cpp_codegen_static_fields_for(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var))->set_splitedCharacterPosition_1(L_8);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.EffectPlayer::OnEnable(System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3COnEnableU3Eb__7_0_m03524910EABBB0BB877A6F4343AADDF0B2C8B8D2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * G_B3_0 = NULL;
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * G_B2_0 = NULL;
	{
		// if (s_UpdateActions == null)
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = ((EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields*)il2cpp_codegen_static_fields_for(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var))->get_s_UpdateActions_6();
		if (L_0)
		{
			goto IL_0035;
		}
	}
	{
		// s_UpdateActions = new List<Action>();
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_1 = (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *)il2cpp_codegen_object_new(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB(L_1, /*hidden argument*/List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		((EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields*)il2cpp_codegen_static_fields_for(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var))->set_s_UpdateActions_6(L_1);
		// Canvas.willRenderCanvases += () =>
		// {
		//     var count = s_UpdateActions.Count;
		//     for (int i = 0; i < count; i++)
		//     {
		//         s_UpdateActions[i].Invoke();
		//     }
		// };
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var);
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_2 = ((U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var))->get_U3CU3E9__7_0_1();
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_3 = L_2;
		G_B2_0 = L_3;
		if (L_3)
		{
			G_B3_0 = L_3;
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var);
		U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * L_4 = ((U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_5 = (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 *)il2cpp_codegen_object_new(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		WillRenderCanvases__ctor_m8A46E9A5DED6B54DC2A8A3137AE3637081EADFB6(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec_U3COnEnableU3Eb__7_0_m03524910EABBB0BB877A6F4343AADDF0B2C8B8D2_RuntimeMethod_var), /*hidden argument*/NULL);
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_6 = L_5;
		((U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var))->set_U3CU3E9__7_0_1(L_6);
		G_B3_0 = L_6;
	}

IL_0030:
	{
		Canvas_add_willRenderCanvases_m00E391FCCE9839EEB6D7A729DCBF6B841FDF02B7(G_B3_0, /*hidden argument*/NULL);
	}

IL_0035:
	{
		// s_UpdateActions.Add(OnWillRenderCanvases);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_7 = ((EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields*)il2cpp_codegen_static_fields_for(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var))->get_s_UpdateActions_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_8 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_8, __this, (intptr_t)((intptr_t)EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_7);
		List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9(L_7, L_8, /*hidden argument*/List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var);
		// if (play)
		bool L_9 = __this->get_play_0();
		if (!L_9)
		{
			goto IL_0062;
		}
	}
	{
		// _time = -initialPlayDelay;
		float L_10 = __this->get_initialPlayDelay_1();
		__this->set__time_7(((-L_10)));
		// }
		goto IL_006d;
	}

IL_0062:
	{
		// _time = 0;
		__this->set__time_7((0.0f));
	}

IL_006d:
	{
		// _callback = callback;
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_11 = ___callback0;
		__this->set__callback_8(L_11);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m2FAEA539D1EBCFCF82B74CEA88321CA0D425646C_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// _callback = null;
		__this->set__callback_8((Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)NULL);
		// s_UpdateActions.Remove(OnWillRenderCanvases);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = ((EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields*)il2cpp_codegen_static_fields_for(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var))->get_s_UpdateActions_6();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_1 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_1, __this, (intptr_t)((intptr_t)EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_2;
		L_2 = List_1_Remove_m2FAEA539D1EBCFCF82B74CEA88321CA0D425646C(L_0, L_1, /*hidden argument*/List_1_Remove_m2FAEA539D1EBCFCF82B74CEA88321CA0D425646C_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer::Play(System.Boolean,System.Action`1<System.Single>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, bool ___reset0, Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * ___callback1, const RuntimeMethod* method)
{
	{
		// if (reset)
		bool L_0 = ___reset0;
		if (!L_0)
		{
			goto IL_000e;
		}
	}
	{
		// _time = 0;
		__this->set__time_7((0.0f));
	}

IL_000e:
	{
		// play = true;
		__this->set_play_0((bool)1);
		// if (callback != null)
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_1 = ___callback1;
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// _callback = callback;
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_2 = ___callback1;
		__this->set__callback_8(L_2);
	}

IL_001f:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer::Stop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_Stop_mCDDB096B24E282D85C931BC545CE0C03FE4AF357 (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, bool ___reset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (reset)
		bool L_0 = ___reset0;
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		// _time = 0;
		__this->set__time_7((0.0f));
		// if (_callback != null)
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_1 = __this->get__callback_8();
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		// _callback(_time);
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_2 = __this->get__callback_8();
		float L_3 = __this->get__time_7();
		NullCheck(L_2);
		Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E(L_2, L_3, /*hidden argument*/Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_RuntimeMethod_var);
	}

IL_0027:
	{
		// play = false;
		__this->set_play_0((bool)0);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer::OnWillRenderCanvases()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	float G_B6_0 = 0.0f;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B6_1 = NULL;
	float G_B5_0 = 0.0f;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B5_1 = NULL;
	float G_B7_0 = 0.0f;
	float G_B7_1 = 0.0f;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B7_2 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B10_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B9_0 = NULL;
	float G_B11_0 = 0.0f;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B11_1 = NULL;
	{
		// if (!play || !Application.isPlaying || _callback == null)
		bool L_0 = __this->get_play_0();
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		bool L_1;
		L_1 = Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567(/*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_2 = __this->get__callback_8();
		if (L_2)
		{
			goto IL_0018;
		}
	}

IL_0017:
	{
		// return;
		return;
	}

IL_0018:
	{
		// _time += updateMode == AnimatorUpdateMode.UnscaledTime
		//     ? Time.unscaledDeltaTime
		//     : Time.deltaTime;
		float L_3 = __this->get__time_7();
		int32_t L_4 = __this->get_updateMode_5();
		G_B5_0 = L_3;
		G_B5_1 = __this;
		if ((((int32_t)L_4) == ((int32_t)2)))
		{
			G_B6_0 = L_3;
			G_B6_1 = __this;
			goto IL_002f;
		}
	}
	{
		float L_5;
		L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		G_B7_0 = L_5;
		G_B7_1 = G_B5_0;
		G_B7_2 = G_B5_1;
		goto IL_0034;
	}

IL_002f:
	{
		float L_6;
		L_6 = Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84(/*hidden argument*/NULL);
		G_B7_0 = L_6;
		G_B7_1 = G_B6_0;
		G_B7_2 = G_B6_1;
	}

IL_0034:
	{
		NullCheck(G_B7_2);
		G_B7_2->set__time_7(((float)il2cpp_codegen_add((float)G_B7_1, (float)G_B7_0)));
		// var current = _time / duration;
		float L_7 = __this->get__time_7();
		float L_8 = __this->get_duration_2();
		V_0 = ((float)((float)L_7/(float)L_8));
		// if (duration <= _time)
		float L_9 = __this->get_duration_2();
		float L_10 = __this->get__time_7();
		if ((!(((float)L_9) <= ((float)L_10))))
		{
			goto IL_007e;
		}
	}
	{
		// play = loop;
		bool L_11 = __this->get_loop_3();
		__this->set_play_0(L_11);
		// _time = loop ? -loopDelay : 0;
		bool L_12 = __this->get_loop_3();
		G_B9_0 = __this;
		if (L_12)
		{
			G_B10_0 = __this;
			goto IL_0072;
		}
	}
	{
		G_B11_0 = (0.0f);
		G_B11_1 = G_B9_0;
		goto IL_0079;
	}

IL_0072:
	{
		float L_13 = __this->get_loopDelay_4();
		G_B11_0 = ((-L_13));
		G_B11_1 = G_B10_0;
	}

IL_0079:
	{
		NullCheck(G_B11_1);
		G_B11_1->set__time_7(G_B11_0);
	}

IL_007e:
	{
		// _callback(current);
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_14 = __this->get__callback_8();
		float L_15 = V_0;
		NullCheck(L_14);
		Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E(L_14, L_15, /*hidden argument*/Action_1_Invoke_mB4E4B9A52AFDB6F7EF89A35E53068E836B1C312E_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * __this, const RuntimeMethod* method)
{
	{
		// public float duration = 1;
		__this->set_duration_2((1.0f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.GraphicConnector::Init()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_Init_m26072284172DE1108E2D55D752D2DA4AF20A552A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// AddConnector(new GraphicConnector());
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0 = (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F *)il2cpp_codegen_object_new(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector__ctor_mF95FB5F86269F4423CC5F9DFA370EA4849FF6DAD(L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_AddConnector_m1D64A1D518EE65C19E8A0D2EE1D3B4584F7C3CFE(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::AddConnector(Coffee.UIEffects.GraphicConnector)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_AddConnector_m1D64A1D518EE65C19E8A0D2EE1D3B4584F7C3CFE (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___connector0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Comparison_1__ctor_mAA9CF9E41CC6F86684E8C44A7BFD13D42CDE86E4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mE8C9752EB54C57B7495AF3ED1F0E2AD106CED6C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Sort_m395F5F609C162FDEF77CB38D6161676BE5CB0A62_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CAddConnectorU3Eb__4_0_mC99D9E9096B78F00A3509748193374ACB23CBB91_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * G_B2_0 = NULL;
	List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * G_B2_1 = NULL;
	Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * G_B1_0 = NULL;
	List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * G_B1_1 = NULL;
	{
		// s_Connectors.Add(connector);
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * L_0 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_Connectors_0();
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1 = ___connector0;
		NullCheck(L_0);
		List_1_Add_mE8C9752EB54C57B7495AF3ED1F0E2AD106CED6C6(L_0, L_1, /*hidden argument*/List_1_Add_mE8C9752EB54C57B7495AF3ED1F0E2AD106CED6C6_RuntimeMethod_var);
		// s_Connectors.Sort((x, y) => y.priority - x.priority);
		List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * L_2 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_Connectors_0();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var);
		Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * L_3 = ((U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var))->get_U3CU3E9__4_0_1();
		Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * L_4 = L_3;
		G_B1_0 = L_4;
		G_B1_1 = L_2;
		if (L_4)
		{
			G_B2_0 = L_4;
			G_B2_1 = L_2;
			goto IL_002f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var);
		U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * L_5 = ((U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * L_6 = (Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A *)il2cpp_codegen_object_new(Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A_il2cpp_TypeInfo_var);
		Comparison_1__ctor_mAA9CF9E41CC6F86684E8C44A7BFD13D42CDE86E4(L_6, L_5, (intptr_t)((intptr_t)U3CU3Ec_U3CAddConnectorU3Eb__4_0_mC99D9E9096B78F00A3509748193374ACB23CBB91_RuntimeMethod_var), /*hidden argument*/Comparison_1__ctor_mAA9CF9E41CC6F86684E8C44A7BFD13D42CDE86E4_RuntimeMethod_var);
		Comparison_1_t1D7B36597285571D11FEAEF2988C0797830CFB7A * L_7 = L_6;
		((U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var))->set_U3CU3E9__4_0_1(L_7);
		G_B2_0 = L_7;
		G_B2_1 = G_B1_1;
	}

IL_002f:
	{
		NullCheck(G_B2_1);
		List_1_Sort_m395F5F609C162FDEF77CB38D6161676BE5CB0A62(G_B2_1, G_B2_0, /*hidden argument*/List_1_Sort_m395F5F609C162FDEF77CB38D6161676BE5CB0A62_RuntimeMethod_var);
		// }
		return;
	}
}
// Coffee.UIEffects.GraphicConnector Coffee.UIEffects.GraphicConnector::FindConnector(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0 (Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m8E8BC37A23AC889755C202C425E13B2F7189C501_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m8CA53842365EB034411824904D39811C29D7BE17_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_mDC3D53B081A01539860E8FEC7989D90D94D6071E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_mF7B2CA3A45C72E5E1DBE6349EA4256E795251B5F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m26B8A870C49C4EBBEE8635F2B0848275E23E83DC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mF09ECF08D0194AACE9E36B4310774C83E03873DB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * V_1 = NULL;
	Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63  V_2;
	memset((&V_2), 0, sizeof(V_2));
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * V_3 = NULL;
	GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * V_4 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	{
		// if (!graphic) return s_EmptyConnector;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_000e;
		}
	}
	{
		// if (!graphic) return s_EmptyConnector;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_2 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_EmptyConnector_2();
		return L_2;
	}

IL_000e:
	{
		// var type = graphic.GetType();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_3 = ___graphic0;
		NullCheck(L_3);
		Type_t * L_4;
		L_4 = Object_GetType_m571FE8360C10B98C23AAF1F066D92C08CC94F45B(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// GraphicConnector connector = null;
		V_1 = (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F *)NULL;
		// if (s_ConnectorMap.TryGetValue(type, out connector)) return connector;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * L_5 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_ConnectorMap_1();
		Type_t * L_6 = V_0;
		NullCheck(L_5);
		bool L_7;
		L_7 = Dictionary_2_TryGetValue_m8CA53842365EB034411824904D39811C29D7BE17(L_5, L_6, (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F **)(&V_1), /*hidden argument*/Dictionary_2_TryGetValue_m8CA53842365EB034411824904D39811C29D7BE17_RuntimeMethod_var);
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		// if (s_ConnectorMap.TryGetValue(type, out connector)) return connector;
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_8 = V_1;
		return L_8;
	}

IL_0028:
	{
		// foreach (var c in s_Connectors)
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * L_9 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_Connectors_0();
		NullCheck(L_9);
		Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63  L_10;
		L_10 = List_1_GetEnumerator_mF09ECF08D0194AACE9E36B4310774C83E03873DB(L_9, /*hidden argument*/List_1_GetEnumerator_mF09ECF08D0194AACE9E36B4310774C83E03873DB_RuntimeMethod_var);
		V_2 = L_10;
	}

IL_0033:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0057;
		}

IL_0035:
		{
			// foreach (var c in s_Connectors)
			GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_11;
			L_11 = Enumerator_get_Current_m26B8A870C49C4EBBEE8635F2B0848275E23E83DC_inline((Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m26B8A870C49C4EBBEE8635F2B0848275E23E83DC_RuntimeMethod_var);
			V_3 = L_11;
			// if (!c.IsValid(graphic)) continue;
			GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_12 = V_3;
			Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_13 = ___graphic0;
			NullCheck(L_12);
			bool L_14;
			L_14 = VirtFuncInvoker1< bool, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(6 /* System.Boolean Coffee.UIEffects.GraphicConnector::IsValid(UnityEngine.UI.Graphic) */, L_12, L_13);
			if (!L_14)
			{
				goto IL_0057;
			}
		}

IL_0046:
		{
			// s_ConnectorMap.Add(type, c);
			IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
			Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * L_15 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_ConnectorMap_1();
			Type_t * L_16 = V_0;
			GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_17 = V_3;
			NullCheck(L_15);
			Dictionary_2_Add_m8E8BC37A23AC889755C202C425E13B2F7189C501(L_15, L_16, L_17, /*hidden argument*/Dictionary_2_Add_m8E8BC37A23AC889755C202C425E13B2F7189C501_RuntimeMethod_var);
			// return c;
			GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_18 = V_3;
			V_4 = L_18;
			IL2CPP_LEAVE(0x76, FINALLY_0062);
		}

IL_0057:
		{
			// foreach (var c in s_Connectors)
			bool L_19;
			L_19 = Enumerator_MoveNext_mF7B2CA3A45C72E5E1DBE6349EA4256E795251B5F((Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_mF7B2CA3A45C72E5E1DBE6349EA4256E795251B5F_RuntimeMethod_var);
			if (L_19)
			{
				goto IL_0035;
			}
		}

IL_0060:
		{
			IL2CPP_LEAVE(0x70, FINALLY_0062);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0062;
	}

FINALLY_0062:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_mDC3D53B081A01539860E8FEC7989D90D94D6071E((Enumerator_t9509E2C8CEBC4A99C3DF34FEB4CCE8A368956F63 *)(&V_2), /*hidden argument*/Enumerator_Dispose_mDC3D53B081A01539860E8FEC7989D90D94D6071E_RuntimeMethod_var);
		IL2CPP_END_FINALLY(98)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(98)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x76, IL_0076)
		IL2CPP_JUMP_TBL(0x70, IL_0070)
	}

IL_0070:
	{
		// return s_EmptyConnector;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_20 = ((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->get_s_EmptyConnector_2();
		return L_20;
	}

IL_0076:
	{
		// }
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_21 = V_4;
		return L_21;
	}
}
// System.Int32 Coffee.UIEffects.GraphicConnector::get_priority()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GraphicConnector_get_priority_m90E013783C0364AC6BBB00E5ED2D6F16CEB1E056 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, const RuntimeMethod* method)
{
	{
		// get { return -1; }
		return (-1);
	}
}
// UnityEngine.AdditionalCanvasShaderChannels Coffee.UIEffects.GraphicConnector::get_extraChannel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t GraphicConnector_get_extraChannel_mBACD9FB6B2EFD87E46936C068B0928C983B66493 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, const RuntimeMethod* method)
{
	{
		// get { return AdditionalCanvasShaderChannels.TexCoord1; }
		return (int32_t)(1);
	}
}
// System.Boolean Coffee.UIEffects.GraphicConnector::IsValid(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GraphicConnector_IsValid_m1C83898406961516C0B3B3EBF11F070397224BB3 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	{
		// return true;
		return (bool)1;
	}
}
// UnityEngine.Shader Coffee.UIEffects.GraphicConnector::FindShader(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * GraphicConnector_FindShader_m79BE9DE2146376902CBE2647B0CF83A8409A67D3 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, String_t* ___shaderName0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2CB6662AD5190716C933E5698CA92FC65E715115);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return Shader.Find("Hidden/" + shaderName);
		String_t* L_0 = ___shaderName0;
		String_t* L_1;
		L_1 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral2CB6662AD5190716C933E5698CA92FC65E715115, L_0, /*hidden argument*/NULL);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_2;
		L_2 = Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::OnEnable(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_OnEnable_m0B0E987BB2634B8784713B5B40D091E5CD339638 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::OnDisable(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_OnDisable_mE8D7E9477569A5C57F19D7003B5E919760130739 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::SetVerticesDirty(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_SetVerticesDirty_m929125B81A45CC98E05D7A42417D62E9BB99CB20 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (graphic)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// graphic.SetVerticesDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2 = ___graphic0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(28 /* System.Void UnityEngine.UI.Graphic::SetVerticesDirty() */, L_2);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::SetMaterialDirty(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_SetMaterialDirty_m168E165024112455DDCD7AAE3B65926C9890D990 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (graphic)
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// graphic.SetMaterialDirty();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2 = ___graphic0;
		NullCheck(L_2);
		VirtActionInvoker0::Invoke(29 /* System.Void UnityEngine.UI.Graphic::SetMaterialDirty() */, L_2);
	}

IL_000e:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::GetPositionFactor(Coffee.UIEffects.EffectArea,System.Int32,UnityEngine.Rect,UnityEngine.Vector2,System.Single&,System.Single&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_GetPositionFactor_m78368E99AED4E4C093AFE2D0E4608519C14F2B58 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, int32_t ___area0, int32_t ___index1, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rect2, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___position3, float* ___x4, float* ___y5, const RuntimeMethod* method)
{
	{
		// if (area == EffectArea.Fit)
		int32_t L_0 = ___area0;
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0043;
		}
	}
	{
		// x = Mathf.Clamp01((position.x - rect.xMin) / rect.width);
		float* L_1 = ___x4;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___position3;
		float L_3 = L_2.get_x_0();
		float L_4;
		L_4 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_5;
		L_5 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_6;
		L_6 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)((float)il2cpp_codegen_subtract((float)L_3, (float)L_4))/(float)L_5)), /*hidden argument*/NULL);
		*((float*)L_1) = (float)L_6;
		// y = Mathf.Clamp01((position.y - rect.yMin) / rect.height);
		float* L_7 = ___y5;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = ___position3;
		float L_9 = L_8.get_y_1();
		float L_10;
		L_10 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_11;
		L_11 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_12;
		L_12 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)((float)((float)il2cpp_codegen_subtract((float)L_9, (float)L_10))/(float)L_11)), /*hidden argument*/NULL);
		*((float*)L_7) = (float)L_12;
		// }
		return;
	}

IL_0043:
	{
		// x = Mathf.Clamp01(position.x / rect.width + 0.5f);
		float* L_13 = ___x4;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_14 = ___position3;
		float L_15 = L_14.get_x_0();
		float L_16;
		L_16 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_17;
		L_17 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)il2cpp_codegen_add((float)((float)((float)L_15/(float)L_16)), (float)(0.5f))), /*hidden argument*/NULL);
		*((float*)L_13) = (float)L_17;
		// y = Mathf.Clamp01(position.y / rect.height + 0.5f);
		float* L_18 = ___y5;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_19 = ___position3;
		float L_20 = L_19.get_y_1();
		float L_21;
		L_21 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect2), /*hidden argument*/NULL);
		float L_22;
		L_22 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)il2cpp_codegen_add((float)((float)((float)L_20/(float)L_21)), (float)(0.5f))), /*hidden argument*/NULL);
		*((float*)L_18) = (float)L_22;
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.GraphicConnector::IsText(UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool GraphicConnector_IsText_mEB728B485D4E21FA91A2CF75597708D345B2DE82 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return graphic && graphic is Text;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0012;
		}
	}
	{
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_2 = ___graphic0;
		return (bool)((!(((RuntimeObject*)(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 *)((Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 *)IsInstClass((RuntimeObject*)L_2, Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0);
	}

IL_0012:
	{
		return (bool)0;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::SetExtraChannel(UnityEngine.UIVertex&,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_SetExtraChannel_m494279F3F5E68D70DE0F43EF25B939358E965603 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * ___vertex0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value1, const RuntimeMethod* method)
{
	{
		// vertex.uv1 = value;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * L_0 = ___vertex0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = ___value1;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_2;
		L_2 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_1, /*hidden argument*/NULL);
		L_0->set_uv1_5(L_2);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::GetNormalizedFactor(Coffee.UIEffects.EffectArea,System.Int32,Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2,UnityEngine.Vector2&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector_GetNormalizedFactor_m6555A1423252B5DDD7A71FA1C5400AE805A50DC3 (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, int32_t ___area0, int32_t ___index1, Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  ___matrix2, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___position3, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * ___normalizedPos4, const RuntimeMethod* method)
{
	{
		// normalizedPos = matrix * position;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = ___normalizedPos4;
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_1 = ___matrix2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___position3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3;
		L_3 = Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0(L_1, L_2, /*hidden argument*/NULL);
		*(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)L_0 = L_3;
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector__ctor_mF95FB5F86269F4423CC5F9DFA370EA4849FF6DAD (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GraphicConnector__cctor_mC73D5A65390F0C216962225BEB934BCBEF9EE522 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_mD138DC514ABD671F73D4141EB5D75C9BBE6E788C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m5EB82AEEC5544577504971655B371654980D6989_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly List<GraphicConnector> s_Connectors = new List<GraphicConnector>();
		List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 * L_0 = (List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9 *)il2cpp_codegen_object_new(List_1_t678FB0F4C6439F449014A1FE6DE6CF31F0946FE9_il2cpp_TypeInfo_var);
		List_1__ctor_m5EB82AEEC5544577504971655B371654980D6989(L_0, /*hidden argument*/List_1__ctor_m5EB82AEEC5544577504971655B371654980D6989_RuntimeMethod_var);
		((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->set_s_Connectors_0(L_0);
		// private static readonly Dictionary<Type, GraphicConnector> s_ConnectorMap =
		//     new Dictionary<Type, GraphicConnector>();
		Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B * L_1 = (Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B *)il2cpp_codegen_object_new(Dictionary_2_tE62C20548B6ED9DA63B52D56067495A4C37A939B_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mD138DC514ABD671F73D4141EB5D75C9BBE6E788C(L_1, /*hidden argument*/Dictionary_2__ctor_mD138DC514ABD671F73D4141EB5D75C9BBE6E788C_RuntimeMethod_var);
		((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->set_s_ConnectorMap_1(L_1);
		// private static readonly GraphicConnector s_EmptyConnector = new GraphicConnector();
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_2 = (GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F *)il2cpp_codegen_object_new(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector__ctor_mF95FB5F86269F4423CC5F9DFA370EA4849FF6DAD(L_2, /*hidden argument*/NULL);
		((GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_StaticFields*)il2cpp_codegen_static_fields_for(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var))->set_s_EmptyConnector_2(L_2);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Material Coffee.UIEffects.MaterialCache::Register(UnityEngine.Material,UnityEngine.Hash128,System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_t8927C00353A72755313F046D0CE85178AE8218EE * MaterialCache_Register_m41B9F13914C2BBF0F30CC45DBF8E8713E15079F8 (Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___hash1, Action_2_tF65C920A10937B33C65584F87099045F42BBB100 * ___onModifyMaterial2, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_2_Invoke_mD347DED36015B897E49370E126492889CE4CEF2A_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Add_m3320410185FF9C49F194CC82EECEB845CF1FD719_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * V_0 = NULL;
	{
		// if (!hash.isValid) return null;
		bool L_0;
		L_0 = Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB((Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A *)(&___hash1), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000b;
		}
	}
	{
		// if (!hash.isValid) return null;
		return (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)NULL;
	}

IL_000b:
	{
		// if (!materialMap.TryGetValue(hash, out entry))
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * L_1 = ((MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields*)il2cpp_codegen_static_fields_for(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var))->get_materialMap_0();
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_2 = ___hash1;
		NullCheck(L_1);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019(L_1, L_2, (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_004d;
		}
	}
	{
		// entry = new MaterialEntry()
		// {
		//     material = new Material(baseMaterial)
		//     {
		//         hideFlags = HideFlags.HideAndDontSave,
		//     },
		// };
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_4 = (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 *)il2cpp_codegen_object_new(MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7_il2cpp_TypeInfo_var);
		MaterialEntry__ctor_m409D7E4C408065F797EFAC708ADD6836DDAD7FC8(L_4, /*hidden argument*/NULL);
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_5 = L_4;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_6 = ___baseMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = (Material_t8927C00353A72755313F046D0CE85178AE8218EE *)il2cpp_codegen_object_new(Material_t8927C00353A72755313F046D0CE85178AE8218EE_il2cpp_TypeInfo_var);
		Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245(L_7, L_6, /*hidden argument*/NULL);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = L_7;
		NullCheck(L_8);
		Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC(L_8, ((int32_t)61), /*hidden argument*/NULL);
		NullCheck(L_5);
		L_5->set_material_0(L_8);
		V_0 = L_5;
		// onModifyMaterial(entry.material, graphic);
		Action_2_tF65C920A10937B33C65584F87099045F42BBB100 * L_9 = ___onModifyMaterial2;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_10 = V_0;
		NullCheck(L_10);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_11 = L_10->get_material_0();
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_12 = ___graphic3;
		NullCheck(L_9);
		Action_2_Invoke_mD347DED36015B897E49370E126492889CE4CEF2A(L_9, L_11, L_12, /*hidden argument*/Action_2_Invoke_mD347DED36015B897E49370E126492889CE4CEF2A_RuntimeMethod_var);
		// materialMap.Add(hash, entry);
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * L_13 = ((MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields*)il2cpp_codegen_static_fields_for(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var))->get_materialMap_0();
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_14 = ___hash1;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_15 = V_0;
		NullCheck(L_13);
		Dictionary_2_Add_m3320410185FF9C49F194CC82EECEB845CF1FD719(L_13, L_14, L_15, /*hidden argument*/Dictionary_2_Add_m3320410185FF9C49F194CC82EECEB845CF1FD719_RuntimeMethod_var);
	}

IL_004d:
	{
		// entry.referenceCount++;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_16 = V_0;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_17 = L_16;
		NullCheck(L_17);
		int32_t L_18 = L_17->get_referenceCount_1();
		NullCheck(L_17);
		L_17->set_referenceCount_1(((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1)));
		// return entry.material;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_19 = V_0;
		NullCheck(L_19);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_20 = L_19->get_material_0();
		return L_20;
	}
}
// System.Void Coffee.UIEffects.MaterialCache::Unregister(UnityEngine.Hash128)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialCache_Unregister_m5F0CFE2E6BFD7A6D3E0862D6034B1CB4F3A5B394 (Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  ___hash0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_Remove_mC6318BC7A4B155ECA98F5CF06F61F51674000E07_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// if (!hash.isValid || !materialMap.TryGetValue(hash, out entry)) return;
		bool L_0;
		L_0 = Hash128_get_isValid_mDDF7C5C41DD77702C5B3AAFA5AB9E8530D1FBBFB((Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A *)(&___hash0), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * L_1 = ((MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields*)il2cpp_codegen_static_fields_for(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var))->get_materialMap_0();
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_2 = ___hash0;
		NullCheck(L_1);
		bool L_3;
		L_3 = Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019(L_1, L_2, (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 **)(&V_0), /*hidden argument*/Dictionary_2_TryGetValue_m7BDF77E2BA31DD6491CB0902D1FF40595FB70019_RuntimeMethod_var);
		if (L_3)
		{
			goto IL_0019;
		}
	}

IL_0018:
	{
		// if (!hash.isValid || !materialMap.TryGetValue(hash, out entry)) return;
		return;
	}

IL_0019:
	{
		// if (--entry.referenceCount > 0) return;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_4 = V_0;
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_5 = L_4;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_referenceCount_1();
		V_1 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_6, (int32_t)1));
		int32_t L_7 = V_1;
		NullCheck(L_5);
		L_5->set_referenceCount_1(L_7);
		int32_t L_8 = V_1;
		if ((((int32_t)L_8) <= ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		// if (--entry.referenceCount > 0) return;
		return;
	}

IL_002e:
	{
		// entry.Release();
		MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * L_9 = V_0;
		NullCheck(L_9);
		MaterialEntry_Release_mB302360107E7F32A760184279DA7E3D24149498E(L_9, /*hidden argument*/NULL);
		// materialMap.Remove(hash);
		IL2CPP_RUNTIME_CLASS_INIT(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * L_10 = ((MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields*)il2cpp_codegen_static_fields_for(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var))->get_materialMap_0();
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_11 = ___hash0;
		NullCheck(L_10);
		bool L_12;
		L_12 = Dictionary_2_Remove_mC6318BC7A4B155ECA98F5CF06F61F51674000E07(L_10, L_11, /*hidden argument*/Dictionary_2_Remove_mC6318BC7A4B155ECA98F5CF06F61F51674000E07_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.MaterialCache::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialCache__ctor_mA988DB48B7DC46EC3020C7DEC2DCBBD9AD785C8A (MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.MaterialCache::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialCache__cctor_m46A748483AC54F7C2C85EF3ED75A0247BA85E583 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2__ctor_m949E2D644B0A3B0F74E1FD5193BCD30878145AE0_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static Dictionary<Hash128, MaterialEntry> materialMap = new Dictionary<Hash128, MaterialEntry>();
		Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 * L_0 = (Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1 *)il2cpp_codegen_object_new(Dictionary_2_t59CD5C3289DBF251417D781DA2D3C69A7FB43FD1_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m949E2D644B0A3B0F74E1FD5193BCD30878145AE0(L_0, /*hidden argument*/Dictionary_2__ctor_m949E2D644B0A3B0F74E1FD5193BCD30878145AE0_RuntimeMethod_var);
		((MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_StaticFields*)il2cpp_codegen_static_fields_for(MaterialCache_t3A1EAECFC6B9700E96335A9F104084C49DDA93D4_il2cpp_TypeInfo_var))->set_materialMap_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.Matrix2x3::.ctor(UnityEngine.Rect,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE (Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rect0, float ___cos1, float ___sin2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// float dx = -rect.xMin / rect.width - center;
		float L_0;
		L_0 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		float L_1;
		L_1 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		V_0 = ((float)il2cpp_codegen_subtract((float)((float)((float)((-L_0))/(float)L_1)), (float)(0.5f)));
		// float dy = -rect.yMin / rect.height - center;
		float L_2;
		L_2 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		float L_3;
		L_3 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		V_1 = ((float)il2cpp_codegen_subtract((float)((float)((float)((-L_2))/(float)L_3)), (float)(0.5f)));
		// m00 = cos / rect.width;
		float L_4 = ___cos1;
		float L_5;
		L_5 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		__this->set_m00_0(((float)((float)L_4/(float)L_5)));
		// m01 = -sin / rect.height;
		float L_6 = ___sin2;
		float L_7;
		L_7 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		__this->set_m01_1(((float)((float)((-L_6))/(float)L_7)));
		// m02 = dx * cos - dy * sin + center;
		float L_8 = V_0;
		float L_9 = ___cos1;
		float L_10 = V_1;
		float L_11 = ___sin2;
		__this->set_m02_2(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_11)))), (float)(0.5f))));
		// m10 = sin / rect.width;
		float L_12 = ___sin2;
		float L_13;
		L_13 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		__this->set_m10_3(((float)((float)L_12/(float)L_13)));
		// m11 = cos / rect.height;
		float L_14 = ___cos1;
		float L_15;
		L_15 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&___rect0), /*hidden argument*/NULL);
		__this->set_m11_4(((float)((float)L_14/(float)L_15)));
		// m12 = dx * sin + dy * cos + center;
		float L_16 = V_0;
		float L_17 = ___sin2;
		float L_18 = V_1;
		float L_19 = ___cos1;
		__this->set_m12_5(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_16, (float)L_17)), (float)((float)il2cpp_codegen_multiply((float)L_18, (float)L_19)))), (float)(0.5f))));
		// }
		return;
	}
}
IL2CPP_EXTERN_C  void Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE_AdjustorThunk (RuntimeObject * __this, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  ___rect0, float ___cos1, float ___sin2, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED * _thisAdjusted = reinterpret_cast<Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED *>(__this + _offset);
	Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE(_thisAdjusted, ___rect0, ___cos1, ___sin2, method);
}
// UnityEngine.Vector2 Coffee.UIEffects.Matrix2x3::op_Multiply(Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0 (Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  ___m0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___v1, const RuntimeMethod* method)
{
	{
		// return new Vector2(
		//     (m.m00 * v.x) + (m.m01 * v.y) + m.m02,
		//     (m.m10 * v.x) + (m.m11 * v.y) + m.m12
		// );
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_0 = ___m0;
		float L_1 = L_0.get_m00_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___v1;
		float L_3 = L_2.get_x_0();
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_4 = ___m0;
		float L_5 = L_4.get_m01_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___v1;
		float L_7 = L_6.get_y_1();
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_8 = ___m0;
		float L_9 = L_8.get_m02_2();
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_10 = ___m0;
		float L_11 = L_10.get_m10_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = ___v1;
		float L_13 = L_12.get_x_0();
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_14 = ___m0;
		float L_15 = L_14.get_m11_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_16 = ___v1;
		float L_17 = L_16.get_y_1();
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_18 = ___m0;
		float L_19 = L_18.get_m12_5();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_20;
		memset((&L_20), 0, sizeof(L_20));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_20), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), (float)((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)))), (float)L_9)), ((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_11, (float)L_13)), (float)((float)il2cpp_codegen_multiply((float)L_15, (float)L_17)))), (float)L_19)), /*hidden argument*/NULL);
		return L_20;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Packer::ToFloat(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_mA7D23F51E9C8A6E62848712A27E2309BC31F94A7 (float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method)
{
	float G_B5_0 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B15_0 = 0.0f;
	float G_B20_0 = 0.0f;
	{
		// x = x < 0 ? 0 : 1 < x ? 1 : x;
		float L_0 = ___x0;
		if ((((float)L_0) < ((float)(0.0f))))
		{
			goto IL_001a;
		}
	}
	{
		float L_1 = ___x0;
		if ((((float)(1.0f)) < ((float)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		float L_2 = ___x0;
		G_B5_0 = L_2;
		goto IL_001f;
	}

IL_0013:
	{
		G_B5_0 = (1.0f);
		goto IL_001f;
	}

IL_001a:
	{
		G_B5_0 = (0.0f);
	}

IL_001f:
	{
		___x0 = G_B5_0;
		// y = y < 0 ? 0 : 1 < y ? 1 : y;
		float L_3 = ___y1;
		if ((((float)L_3) < ((float)(0.0f))))
		{
			goto IL_003b;
		}
	}
	{
		float L_4 = ___y1;
		if ((((float)(1.0f)) < ((float)L_4)))
		{
			goto IL_0034;
		}
	}
	{
		float L_5 = ___y1;
		G_B10_0 = L_5;
		goto IL_0040;
	}

IL_0034:
	{
		G_B10_0 = (1.0f);
		goto IL_0040;
	}

IL_003b:
	{
		G_B10_0 = (0.0f);
	}

IL_0040:
	{
		___y1 = G_B10_0;
		// z = z < 0 ? 0 : 1 < z ? 1 : z;
		float L_6 = ___z2;
		if ((((float)L_6) < ((float)(0.0f))))
		{
			goto IL_005c;
		}
	}
	{
		float L_7 = ___z2;
		if ((((float)(1.0f)) < ((float)L_7)))
		{
			goto IL_0055;
		}
	}
	{
		float L_8 = ___z2;
		G_B15_0 = L_8;
		goto IL_0061;
	}

IL_0055:
	{
		G_B15_0 = (1.0f);
		goto IL_0061;
	}

IL_005c:
	{
		G_B15_0 = (0.0f);
	}

IL_0061:
	{
		___z2 = G_B15_0;
		// w = w < 0 ? 0 : 1 < w ? 1 : w;
		float L_9 = ___w3;
		if ((((float)L_9) < ((float)(0.0f))))
		{
			goto IL_007d;
		}
	}
	{
		float L_10 = ___w3;
		if ((((float)(1.0f)) < ((float)L_10)))
		{
			goto IL_0076;
		}
	}
	{
		float L_11 = ___w3;
		G_B20_0 = L_11;
		goto IL_0082;
	}

IL_0076:
	{
		G_B20_0 = (1.0f);
		goto IL_0082;
	}

IL_007d:
	{
		G_B20_0 = (0.0f);
	}

IL_0082:
	{
		___w3 = G_B20_0;
		// return (Mathf.FloorToInt(w * PRECISION) << 18)
		//        + (Mathf.FloorToInt(z * PRECISION) << 12)
		//        + (Mathf.FloorToInt(y * PRECISION) << 6)
		//        + Mathf.FloorToInt(x * PRECISION);
		float L_12 = ___w3;
		int32_t L_13;
		L_13 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_12, (float)(63.0f))), /*hidden argument*/NULL);
		float L_14 = ___z2;
		int32_t L_15;
		L_15 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_14, (float)(63.0f))), /*hidden argument*/NULL);
		float L_16 = ___y1;
		int32_t L_17;
		L_17 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_16, (float)(63.0f))), /*hidden argument*/NULL);
		float L_18 = ___x0;
		int32_t L_19;
		L_19 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_18, (float)(63.0f))), /*hidden argument*/NULL);
		return ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_13<<(int32_t)((int32_t)18))), (int32_t)((int32_t)((int32_t)L_15<<(int32_t)((int32_t)12))))), (int32_t)((int32_t)((int32_t)L_17<<(int32_t)6)))), (int32_t)L_19))));
	}
}
// System.Single Packer::ToFloat(UnityEngine.Vector4)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_mC5388DBB78FE07E8AB135B8759A20EAA43AF320E (Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___factor0, const RuntimeMethod* method)
{
	{
		// return ToFloat(Mathf.Clamp01(factor.x), Mathf.Clamp01(factor.y), Mathf.Clamp01(factor.z),
		//     Mathf.Clamp01(factor.w));
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_0 = ___factor0;
		float L_1 = L_0.get_x_1();
		float L_2;
		L_2 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_1, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_3 = ___factor0;
		float L_4 = L_3.get_y_2();
		float L_5;
		L_5 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_4, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_6 = ___factor0;
		float L_7 = L_6.get_z_3();
		float L_8;
		L_8 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_7, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_9 = ___factor0;
		float L_10 = L_9.get_w_4();
		float L_11;
		L_11 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_10, /*hidden argument*/NULL);
		float L_12;
		L_12 = Packer_ToFloat_mA7D23F51E9C8A6E62848712A27E2309BC31F94A7(L_2, L_5, L_8, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Single Packer::ToFloat(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_m7691266B0941E760FB34C506126EEAF8A8FBE1D7 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	float G_B5_0 = 0.0f;
	float G_B10_0 = 0.0f;
	float G_B15_0 = 0.0f;
	{
		// x = x < 0 ? 0 : 1 < x ? 1 : x;
		float L_0 = ___x0;
		if ((((float)L_0) < ((float)(0.0f))))
		{
			goto IL_001a;
		}
	}
	{
		float L_1 = ___x0;
		if ((((float)(1.0f)) < ((float)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		float L_2 = ___x0;
		G_B5_0 = L_2;
		goto IL_001f;
	}

IL_0013:
	{
		G_B5_0 = (1.0f);
		goto IL_001f;
	}

IL_001a:
	{
		G_B5_0 = (0.0f);
	}

IL_001f:
	{
		___x0 = G_B5_0;
		// y = y < 0 ? 0 : 1 < y ? 1 : y;
		float L_3 = ___y1;
		if ((((float)L_3) < ((float)(0.0f))))
		{
			goto IL_003b;
		}
	}
	{
		float L_4 = ___y1;
		if ((((float)(1.0f)) < ((float)L_4)))
		{
			goto IL_0034;
		}
	}
	{
		float L_5 = ___y1;
		G_B10_0 = L_5;
		goto IL_0040;
	}

IL_0034:
	{
		G_B10_0 = (1.0f);
		goto IL_0040;
	}

IL_003b:
	{
		G_B10_0 = (0.0f);
	}

IL_0040:
	{
		___y1 = G_B10_0;
		// z = z < 0 ? 0 : 1 < z ? 1 : z;
		float L_6 = ___z2;
		if ((((float)L_6) < ((float)(0.0f))))
		{
			goto IL_005c;
		}
	}
	{
		float L_7 = ___z2;
		if ((((float)(1.0f)) < ((float)L_7)))
		{
			goto IL_0055;
		}
	}
	{
		float L_8 = ___z2;
		G_B15_0 = L_8;
		goto IL_0061;
	}

IL_0055:
	{
		G_B15_0 = (1.0f);
		goto IL_0061;
	}

IL_005c:
	{
		G_B15_0 = (0.0f);
	}

IL_0061:
	{
		___z2 = G_B15_0;
		// return (Mathf.FloorToInt(z * PRECISION) << 16)
		//        + (Mathf.FloorToInt(y * PRECISION) << 8)
		//        + Mathf.FloorToInt(x * PRECISION);
		float L_9 = ___z2;
		int32_t L_10;
		L_10 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_9, (float)(255.0f))), /*hidden argument*/NULL);
		float L_11 = ___y1;
		int32_t L_12;
		L_12 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_11, (float)(255.0f))), /*hidden argument*/NULL);
		float L_13 = ___x0;
		int32_t L_14;
		L_14 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_13, (float)(255.0f))), /*hidden argument*/NULL);
		return ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_10<<(int32_t)((int32_t)16))), (int32_t)((int32_t)((int32_t)L_12<<(int32_t)8)))), (int32_t)L_14))));
	}
}
// System.Single Packer::ToFloat(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662 (float ___x0, float ___y1, const RuntimeMethod* method)
{
	float G_B5_0 = 0.0f;
	float G_B10_0 = 0.0f;
	{
		// x = x < 0 ? 0 : 1 < x ? 1 : x;
		float L_0 = ___x0;
		if ((((float)L_0) < ((float)(0.0f))))
		{
			goto IL_001a;
		}
	}
	{
		float L_1 = ___x0;
		if ((((float)(1.0f)) < ((float)L_1)))
		{
			goto IL_0013;
		}
	}
	{
		float L_2 = ___x0;
		G_B5_0 = L_2;
		goto IL_001f;
	}

IL_0013:
	{
		G_B5_0 = (1.0f);
		goto IL_001f;
	}

IL_001a:
	{
		G_B5_0 = (0.0f);
	}

IL_001f:
	{
		___x0 = G_B5_0;
		// y = y < 0 ? 0 : 1 < y ? 1 : y;
		float L_3 = ___y1;
		if ((((float)L_3) < ((float)(0.0f))))
		{
			goto IL_003b;
		}
	}
	{
		float L_4 = ___y1;
		if ((((float)(1.0f)) < ((float)L_4)))
		{
			goto IL_0034;
		}
	}
	{
		float L_5 = ___y1;
		G_B10_0 = L_5;
		goto IL_0040;
	}

IL_0034:
	{
		G_B10_0 = (1.0f);
		goto IL_0040;
	}

IL_003b:
	{
		G_B10_0 = (0.0f);
	}

IL_0040:
	{
		___y1 = G_B10_0;
		// return (Mathf.FloorToInt(y * PRECISION) << 12)
		//        + Mathf.FloorToInt(x * PRECISION);
		float L_6 = ___y1;
		int32_t L_7;
		L_7 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_6, (float)(4095.0f))), /*hidden argument*/NULL);
		float L_8 = ___x0;
		int32_t L_9;
		L_9 = Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E(((float)il2cpp_codegen_multiply((float)L_8, (float)(4095.0f))), /*hidden argument*/NULL);
		return ((float)((float)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_7<<(int32_t)((int32_t)12))), (int32_t)L_9))));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.ParameterTexture::.ctor(System.Int32,System.Int32,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, int32_t ___channels0, int32_t ___instanceLimit1, String_t* ___propertyName2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// public ParameterTexture(int channels, int instanceLimit, string propertyName)
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		// _propertyName = propertyName;
		String_t* L_0 = ___propertyName2;
		__this->set__propertyName_3(L_0);
		// _channels = ((channels - 1) / 4 + 1) * 4;
		int32_t L_1 = ___channels0;
		__this->set__channels_4(((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1))/(int32_t)4)), (int32_t)1)), (int32_t)4)));
		// _instanceLimit = ((instanceLimit - 1) / 2 + 1) * 2;
		int32_t L_2 = ___instanceLimit1;
		__this->set__instanceLimit_5(((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1))/(int32_t)2)), (int32_t)1)), (int32_t)2)));
		// _data = new byte[_channels * _instanceLimit];
		int32_t L_3 = __this->get__channels_4();
		int32_t L_4 = __this->get__instanceLimit_5();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_3, (int32_t)L_4)));
		__this->set__data_6(L_5);
		// _stack = new Stack<int>(_instanceLimit);
		int32_t L_6 = __this->get__instanceLimit_5();
		Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * L_7 = (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 *)il2cpp_codegen_object_new(Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55_il2cpp_TypeInfo_var);
		Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3(L_7, L_6, /*hidden argument*/Stack_1__ctor_m38BE08C17D6CADEE2442D62775E20B841CB0C0C3_RuntimeMethod_var);
		__this->set__stack_7(L_7);
		// for (int i = 1; i < _instanceLimit + 1; i++)
		V_0 = 1;
		goto IL_0068;
	}

IL_0058:
	{
		// _stack.Push(i);
		Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * L_8 = __this->get__stack_7();
		int32_t L_9 = V_0;
		NullCheck(L_8);
		Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3(L_8, L_9, /*hidden argument*/Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_RuntimeMethod_var);
		// for (int i = 1; i < _instanceLimit + 1; i++)
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0068:
	{
		// for (int i = 1; i < _instanceLimit + 1; i++)
		int32_t L_11 = V_0;
		int32_t L_12 = __this->get__instanceLimit_5();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1)))))
		{
			goto IL_0058;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::Register(Coffee.UIEffects.IParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_Register_m418624C23515BB36BFE2E30FDC66AB2DD855D62A (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Initialize();
		ParameterTexture_Initialize_m947A572C82BF5BB6BC7A84E95F3498401183F29A(__this, /*hidden argument*/NULL);
		// if (target.parameterIndex <= 0 && 0 < _stack.Count)
		RuntimeObject* L_0 = ___target0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex() */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_0);
		if ((((int32_t)L_1) > ((int32_t)0)))
		{
			goto IL_002e;
		}
	}
	{
		Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * L_2 = __this->get__stack_7();
		NullCheck(L_2);
		int32_t L_3;
		L_3 = Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_inline(L_2, /*hidden argument*/Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_RuntimeMethod_var);
		if ((((int32_t)0) >= ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		// target.parameterIndex = _stack.Pop();
		RuntimeObject* L_4 = ___target0;
		Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * L_5 = __this->get__stack_7();
		NullCheck(L_5);
		int32_t L_6;
		L_6 = Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C(L_5, /*hidden argument*/Stack_1_Pop_m975CA51F3A72228535A3AE9FE726481EF9E0B76C_RuntimeMethod_var);
		NullCheck(L_4);
		InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void Coffee.UIEffects.IParameterTexture::set_parameterIndex(System.Int32) */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_4, L_6);
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::Unregister(Coffee.UIEffects.IParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_Unregister_mC8283ADEEC7D0B15321727EC445C0909671918BC (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (0 < target.parameterIndex)
		RuntimeObject* L_0 = ___target0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex() */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_0);
		if ((((int32_t)0) >= ((int32_t)L_1)))
		{
			goto IL_0021;
		}
	}
	{
		// _stack.Push(target.parameterIndex);
		Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * L_2 = __this->get__stack_7();
		RuntimeObject* L_3 = ___target0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex() */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_3);
		NullCheck(L_2);
		Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3(L_2, L_4, /*hidden argument*/Stack_1_Push_mB57657F2527B7C25AB60EB9BBD09C7B42B14F3C3_RuntimeMethod_var);
		// target.parameterIndex = 0;
		RuntimeObject* L_5 = ___target0;
		NullCheck(L_5);
		InterfaceActionInvoker1< int32_t >::Invoke(1 /* System.Void Coffee.UIEffects.IParameterTexture::set_parameterIndex(System.Int32) */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_5, 0);
	}

IL_0021:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::SetData(Coffee.UIEffects.IParameterTexture,System.Int32,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_SetData_m3870BC4215A7F85A68A38D2E6B50DB27D65284BB (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, int32_t ___channelId1, uint8_t ___value2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		// int index = (target.parameterIndex - 1) * _channels + channelId;
		RuntimeObject* L_0 = ___target0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex() */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = __this->get__channels_4();
		int32_t L_3 = ___channelId1;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)1)), (int32_t)L_2)), (int32_t)L_3));
		// if (0 < target.parameterIndex && _data[index] != value)
		RuntimeObject* L_4 = ___target0;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex() */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_4);
		if ((((int32_t)0) >= ((int32_t)L_5)))
		{
			goto IL_0036;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_6 = __this->get__data_6();
		int32_t L_7 = V_0;
		NullCheck(L_6);
		int32_t L_8 = L_7;
		uint8_t L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		uint8_t L_10 = ___value2;
		if ((((int32_t)L_9) == ((int32_t)L_10)))
		{
			goto IL_0036;
		}
	}
	{
		// _data[index] = value;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_11 = __this->get__data_6();
		int32_t L_12 = V_0;
		uint8_t L_13 = ___value2;
		NullCheck(L_11);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (uint8_t)L_13);
		// _needUpload = true;
		__this->set__needUpload_1((bool)1);
	}

IL_0036:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::SetData(Coffee.UIEffects.IParameterTexture,System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, int32_t ___channelId1, float ___value2, const RuntimeMethod* method)
{
	{
		// SetData(target, channelId, (byte) (Mathf.Clamp01(value) * 255));
		RuntimeObject* L_0 = ___target0;
		int32_t L_1 = ___channelId1;
		float L_2 = ___value2;
		float L_3;
		L_3 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(L_2, /*hidden argument*/NULL);
		ParameterTexture_SetData_m3870BC4215A7F85A68A38D2E6B50DB27D65284BB(__this, L_0, L_1, (uint8_t)il2cpp_codegen_cast_floating_point<uint8_t, int32_t, float>(((float)il2cpp_codegen_multiply((float)L_3, (float)(255.0f)))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::RegisterMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_propertyId == 0)
		int32_t L_0 = __this->get__propertyId_2();
		if (L_0)
		{
			goto IL_0019;
		}
	}
	{
		// _propertyId = Shader.PropertyToID(_propertyName);
		String_t* L_1 = __this->get__propertyName_3();
		int32_t L_2;
		L_2 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(L_1, /*hidden argument*/NULL);
		__this->set__propertyId_2(L_2);
	}

IL_0019:
	{
		// if (mat)
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___mat0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		// mat.SetTexture(_propertyId, _texture);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_5 = ___mat0;
		int32_t L_6 = __this->get__propertyId_2();
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_7 = __this->get__texture_0();
		NullCheck(L_5);
		Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60(L_5, L_6, L_7, /*hidden argument*/NULL);
	}

IL_0033:
	{
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.ParameterTexture::GetNormalizedIndex(Coffee.UIEffects.IParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, RuntimeObject* ___target0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return ((float) target.parameterIndex - 0.5f) / _instanceLimit;
		RuntimeObject* L_0 = ___target0;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = InterfaceFuncInvoker0< int32_t >::Invoke(0 /* System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex() */, IParameterTexture_t787687BB8296E84E95E5DAB6E50621E66776A212_il2cpp_TypeInfo_var, L_0);
		int32_t L_2 = __this->get__instanceLimit_5();
		return ((float)((float)((float)il2cpp_codegen_subtract((float)((float)((float)L_1)), (float)(0.5f)))/(float)((float)((float)L_2))));
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::Initialize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_Initialize_m947A572C82BF5BB6BC7A84E95F3498401183F29A (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_UpdateParameterTexture_mF9DFFADF593F9954190F1161E8C2E117077B2B63_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_U3CInitializeU3Eb__16_0_mD35535E6182BFEEE7882780DE18A230F2611C9E9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * G_B3_0 = NULL;
	WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * G_B2_0 = NULL;
	{
		// if (updates == null)
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = ((ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields*)il2cpp_codegen_static_fields_for(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var))->get_updates_8();
		if (L_0)
		{
			goto IL_0035;
		}
	}
	{
		// updates = new List<Action>();
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_1 = (List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 *)il2cpp_codegen_object_new(List_1_t458734AF850139150AB40DFB2B5D1BCE23178235_il2cpp_TypeInfo_var);
		List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB(L_1, /*hidden argument*/List_1__ctor_m8F3A8E6C64C39DA66FF5F99E7A6BB97B41A482BB_RuntimeMethod_var);
		((ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields*)il2cpp_codegen_static_fields_for(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var))->set_updates_8(L_1);
		// Canvas.willRenderCanvases += () =>
		// {
		//     var count = updates.Count;
		//     for (int i = 0; i < count; i++)
		//     {
		//         updates[i].Invoke();
		//     }
		// };
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var);
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_2 = ((U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var))->get_U3CU3E9__16_0_1();
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_3 = L_2;
		G_B2_0 = L_3;
		if (L_3)
		{
			G_B3_0 = L_3;
			goto IL_0030;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var);
		U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * L_4 = ((U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_5 = (WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 *)il2cpp_codegen_object_new(WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958_il2cpp_TypeInfo_var);
		WillRenderCanvases__ctor_m8A46E9A5DED6B54DC2A8A3137AE3637081EADFB6(L_5, L_4, (intptr_t)((intptr_t)U3CU3Ec_U3CInitializeU3Eb__16_0_mD35535E6182BFEEE7882780DE18A230F2611C9E9_RuntimeMethod_var), /*hidden argument*/NULL);
		WillRenderCanvases_t459621B4F3FA2571DE0ED6B4DEF0752F2E9EE958 * L_6 = L_5;
		((U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var))->set_U3CU3E9__16_0_1(L_6);
		G_B3_0 = L_6;
	}

IL_0030:
	{
		Canvas_add_willRenderCanvases_m00E391FCCE9839EEB6D7A729DCBF6B841FDF02B7(G_B3_0, /*hidden argument*/NULL);
	}

IL_0035:
	{
		// if (!_texture)
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_7 = __this->get__texture_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_8;
		L_8 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_7, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_009c;
		}
	}
	{
		// bool isLinear = QualitySettings.activeColorSpace == ColorSpace.Linear;
		int32_t L_9;
		L_9 = QualitySettings_get_activeColorSpace_m65BE7300D1A12D2981B492329B32673199CCE7F4(/*hidden argument*/NULL);
		V_0 = (bool)((((int32_t)L_9) == ((int32_t)1))? 1 : 0);
		// _texture = new Texture2D(_channels / 4, _instanceLimit, TextureFormat.RGBA32, false, isLinear);
		int32_t L_10 = __this->get__channels_4();
		int32_t L_11 = __this->get__instanceLimit_5();
		bool L_12 = V_0;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_13 = (Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF *)il2cpp_codegen_object_new(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF_il2cpp_TypeInfo_var);
		Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3(L_13, ((int32_t)((int32_t)L_10/(int32_t)4)), L_11, 4, (bool)0, L_12, /*hidden argument*/NULL);
		__this->set__texture_0(L_13);
		// _texture.filterMode = FilterMode.Point;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_14 = __this->get__texture_0();
		NullCheck(L_14);
		Texture_set_filterMode_m045141DB0FEFE496885D45F5F23B15BC0E77C8D0(L_14, 0, /*hidden argument*/NULL);
		// _texture.wrapMode = TextureWrapMode.Clamp;
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_15 = __this->get__texture_0();
		NullCheck(L_15);
		Texture_set_wrapMode_m1233D2DF48DC20996F8EE26E866D4BDD2AC8050D(L_15, 1, /*hidden argument*/NULL);
		// updates.Add(UpdateParameterTexture);
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_16 = ((ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields*)il2cpp_codegen_static_fields_for(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var))->get_updates_8();
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_17 = (Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 *)il2cpp_codegen_object_new(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6_il2cpp_TypeInfo_var);
		Action__ctor_m07BE5EE8A629FBBA52AE6356D57A0D371BE2574B(L_17, __this, (intptr_t)((intptr_t)ParameterTexture_UpdateParameterTexture_mF9DFFADF593F9954190F1161E8C2E117077B2B63_RuntimeMethod_var), /*hidden argument*/NULL);
		NullCheck(L_16);
		List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9(L_16, L_17, /*hidden argument*/List_1_Add_m7701B455B6EA0411642596847118B51F91DA8BC9_RuntimeMethod_var);
		// _needUpload = true;
		__this->set__needUpload_1((bool)1);
	}

IL_009c:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture::UpdateParameterTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParameterTexture_UpdateParameterTexture_mF9DFFADF593F9954190F1161E8C2E117077B2B63 (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_needUpload && _texture)
		bool L_0 = __this->get__needUpload_1();
		if (!L_0)
		{
			goto IL_003a;
		}
	}
	{
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_1 = __this->get__texture_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_003a;
		}
	}
	{
		// _needUpload = false;
		__this->set__needUpload_1((bool)0);
		// _texture.LoadRawTextureData(_data);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_3 = __this->get__texture_0();
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = __this->get__data_6();
		NullCheck(L_3);
		Texture2D_LoadRawTextureData_m93A620CC97332F351305E3A93AD11CB2E0EFDAF4(L_3, L_4, /*hidden argument*/NULL);
		// _texture.Apply(false, false);
		Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * L_5 = __this->get__texture_0();
		NullCheck(L_5);
		Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660(L_5, (bool)0, (bool)0, /*hidden argument*/NULL);
	}

IL_003a:
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Coffee.UIEffects.UIDissolve::get_effectFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIDissolve_get_effectFactor_mB944EE30EB78C6EF26D9BBFD3E0E3F4949831D22 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectFactor; }
		float L_0 = __this->get_m_EffectFactor_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_effectFactor_mDAD33D00CAF5DB3E62CFDFE65263D7A6E59C0FC2 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		float L_2 = __this->get_m_EffectFactor_19();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_EffectFactor = value;
		float L_5 = ___value0;
		__this->set_m_EffectFactor_19(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIDissolve::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIDissolve_get_width_m05E9EDE3012FAF89A60F49C567612C3459F70DD0 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Width; }
		float L_0 = __this->get_m_Width_20();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_width(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_width_m4260A791CD3364EA0823369787298EB1F7C95EBE (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Width, value)) return;
		float L_2 = __this->get_m_Width_20();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Width, value)) return;
		return;
	}

IL_0021:
	{
		// m_Width = value;
		float L_5 = ___value0;
		__this->set_m_Width_20(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIDissolve::get_softness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIDissolve_get_softness_m1057EF2111B6739297278CBFA3395B682721C7E9 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Softness; }
		float L_0 = __this->get_m_Softness_21();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_softness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_softness_m937B3F02AF8B03419481A65756AE2581E24B1E47 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Softness, value)) return;
		float L_2 = __this->get_m_Softness_21();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Softness, value)) return;
		return;
	}

IL_0021:
	{
		// m_Softness = value;
		float L_5 = ___value0;
		__this->set_m_Softness_21(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Color Coffee.UIEffects.UIDissolve::get_color()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIDissolve_get_color_mEB32F760AD1BD1125D983FC1B313022EC1248359 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Color; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color_22();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_color(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_color_mCCDFC676B00E5090890B47C5EF1FAB654AF10BA9 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Color == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color_22();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_Color == value) return;
		return;
	}

IL_000f:
	{
		// m_Color = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_Color_22(L_3);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Texture Coffee.UIEffects.UIDissolve::get_transitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return m_TransitionTexture
		//     ? m_TransitionTexture
		//     : defaultTransitionTexture;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = __this->get_m_TransitionTexture_24();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_2;
		L_2 = UIDissolve_get_defaultTransitionTexture_m16638E0BF5F9B8E7E089B2BAF0D5B5325CDBB0D4(/*hidden argument*/NULL);
		return L_2;
	}

IL_0013:
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3 = __this->get_m_TransitionTexture_24();
		return L_3;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_transitionTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_transitionTexture_m033F2F344098C2C1879D4D73B03EF787B8380238 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_TransitionTexture == value) return;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = __this->get_m_TransitionTexture_24();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_TransitionTexture == value) return;
		return;
	}

IL_000f:
	{
		// m_TransitionTexture = value;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3 = ___value0;
		__this->set_m_TransitionTexture_24(L_3);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Texture Coffee.UIEffects.UIDissolve::get_defaultTransitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UIDissolve_get_defaultTransitionTexture_m16638E0BF5F9B8E7E089B2BAF0D5B5325CDBB0D4 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral055F326E6AF2473ADEC1A21756F2574F6644221B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return _defaultTransitionTexture
		//     ? _defaultTransitionTexture
		//     : (_defaultTransitionTexture = Resources.Load<Texture>("Default-Transition"));
		IL2CPP_RUNTIME_CLASS_INIT(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = ((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->get__defaultTransitionTexture_18();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_2;
		L_2 = Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8(_stringLiteral055F326E6AF2473ADEC1A21756F2574F6644221B, /*hidden argument*/Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8_RuntimeMethod_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->set__defaultTransitionTexture_18(L_3);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_4 = ((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->get__defaultTransitionTexture_18();
		return L_4;
	}
}
// Coffee.UIEffects.EffectArea Coffee.UIEffects.UIDissolve::get_effectArea()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIDissolve_get_effectArea_m12B5A1099A85CEA396F92CCF83EC37E43C65DC82 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectArea; }
		int32_t L_0 = __this->get_m_EffectArea_25();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_effectArea(Coffee.UIEffects.EffectArea)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_effectArea_mC95BE4CAD13FFD675BF38A4A6EDA16896D0B3C05 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_EffectArea == value) return;
		int32_t L_0 = __this->get_m_EffectArea_25();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_EffectArea == value) return;
		return;
	}

IL_000a:
	{
		// m_EffectArea = value;
		int32_t L_2 = ___value0;
		__this->set_m_EffectArea_25(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.UIDissolve::get_keepAspectRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIDissolve_get_keepAspectRatio_mF2B7DBCF08E8EA6D7494006ED4F377C660ED5874 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_KeepAspectRatio; }
		bool L_0 = __this->get_m_KeepAspectRatio_26();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_keepAspectRatio(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_keepAspectRatio_m9BA74A15B0B3E6AFEFCD30F9A4502F2EFF850B92 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_KeepAspectRatio == value) return;
		bool L_0 = __this->get_m_KeepAspectRatio_26();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_KeepAspectRatio == value) return;
		return;
	}

IL_000a:
	{
		// m_KeepAspectRatio = value;
		bool L_2 = ___value0;
		__this->set_m_KeepAspectRatio_26(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.ColorMode Coffee.UIEffects.UIDissolve::get_colorMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIDissolve_get_colorMode_m500352BC8F5DD2FE5EF279D4D94F2CEFD572C2D8 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// get { return m_ColorMode; }
		int32_t L_0 = __this->get_m_ColorMode_23();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::set_colorMode(Coffee.UIEffects.ColorMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_set_colorMode_mFE133DE54ACEADE97857F52F22473D7714B362E5 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_ColorMode == value) return;
		int32_t L_0 = __this->get_m_ColorMode_23();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_ColorMode == value) return;
		return;
	}

IL_000a:
	{
		// m_ColorMode = value;
		int32_t L_2 = ___value0;
		__this->set_m_ColorMode_23(L_2);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIDissolve::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIDissolve_get_paramTex_mC7CD59F8E1E87B0FC575638D6D6182C7CF21B55C (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return s_ParamTex; }
		IL2CPP_RUNTIME_CLASS_INIT(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->get_s_ParamTex_14();
		return L_0;
	}
}
// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIDissolve::get_effectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * V_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B2_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B1_0 = NULL;
	{
		// get { return m_Player ?? (m_Player = new EffectPlayer()); }
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0 = __this->get_m_Player_27();
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_2 = (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C *)il2cpp_codegen_object_new(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE(L_2, /*hidden argument*/NULL);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_3 = L_2;
		V_0 = L_3;
		__this->set_m_Player_27(L_3);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_0018:
	{
		return G_B2_0;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.UIDissolve::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  UIDissolve_GetMaterialHash_m92BC68A7FE25F1F1130B46A13B67C9D49BAFB392 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	{
		// if (!isActiveAndEnabled || !material || !material.shader)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___material0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}

IL_001d:
	{
		// return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_6 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_6;
	}

IL_0023:
	{
		// var shaderVariantId = (uint) ((int) m_ColorMode << 6);
		int32_t L_7 = __this->get_m_ColorMode_23();
		V_0 = ((int32_t)((int32_t)L_7<<(int32_t)6));
		// var resourceId = (uint) transitionTexture.GetInstanceID();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_8;
		L_8 = UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9;
		L_9 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		// return new Hash128(
		//     (uint) material.GetInstanceID(),
		//     k_ShaderId + shaderVariantId,
		//     resourceId,
		//     0
		// );
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_10 = ___material0;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_10, /*hidden argument*/NULL);
		uint32_t L_12 = V_0;
		uint32_t L_13 = V_1;
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Hash128__ctor_m4F9101AF4B79AB2FBC395F48875C21A4E2101581((&L_14), L_11, L_12, L_13, 0, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_ModifyMaterial_m864A13BF4E46CED12F1C1C03C70C3AD540E0D46D (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD9276D4AAFC733CC809DC1F50EADEF80DE33D80D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var connector = GraphicConnector.FindConnector(graphic);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic1;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1;
		L_1 = GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0(L_0, /*hidden argument*/NULL);
		// newMaterial.shader = Shader.Find(string.Format("Hidden/{0} (UIDissolve)", newMaterial.shader.name));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___newMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___newMaterial0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralD9276D4AAFC733CC809DC1F50EADEF80DE33D80D, L_5, /*hidden argument*/NULL);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_7;
		L_7 = Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D(L_2, L_7, /*hidden argument*/NULL);
		// SetShaderVariants(newMaterial, m_ColorMode);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = ___newMaterial0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_10 = L_9;
		int32_t L_11 = __this->get_m_ColorMode_23();
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1(__this, L_8, L_10, /*hidden argument*/NULL);
		// newMaterial.SetTexture(k_TransitionTexId, transitionTexture);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_14 = ___newMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		int32_t L_15 = ((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->get_k_TransitionTexId_15();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_16;
		L_16 = UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60(L_14, L_15, L_16, /*hidden argument*/NULL);
		// paramTex.RegisterMaterial(newMaterial);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_17;
		L_17 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = ___newMaterial0;
		NullCheck(L_17);
		ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9(L_17, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_ModifyMesh_m7BA4DB2233B3E73643612C8E08256D896A8B77FB (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * V_1 = NULL;
	float V_2 = 0.0f;
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_3;
	memset((&V_3), 0, sizeof(V_3));
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_4;
	memset((&V_4), 0, sizeof(V_4));
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var normalizedIndex = paramTex.GetNormalizedIndex(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		float L_2;
		L_2 = ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5(L_1, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		// var tex = transitionTexture;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3;
		L_3 = UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		// var aspectRatio = m_KeepAspectRatio && tex ? ((float) tex.width) / tex.height : -1;
		bool L_4 = __this->get_m_KeepAspectRatio_26();
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0034;
		}
	}

IL_002d:
	{
		G_B6_0 = (-1.0f);
		goto IL_0043;
	}

IL_0034:
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_7);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_9);
		G_B6_0 = ((float)((float)((float)((float)L_8))/(float)((float)((float)L_10))));
	}

IL_0043:
	{
		V_2 = G_B6_0;
		// var rect = m_EffectArea.GetEffectArea(vh, rectTransform.rect, aspectRatio);
		int32_t L_11 = __this->get_m_EffectArea_25();
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_12 = ___vh0;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_13;
		L_13 = BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_14;
		L_14 = RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_13, /*hidden argument*/NULL);
		float L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_16;
		L_16 = EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7(L_11, L_12, L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		// var vertex = default(UIVertex);
		il2cpp_codegen_initobj((&V_4), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// var count = vh.currentVertCount;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_17 = ___vh0;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		// for (var i = 0; i < count; i++)
		V_6 = 0;
		goto IL_00e8;
	}

IL_0072:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_19 = ___vh0;
		int32_t L_20 = V_6;
		NullCheck(L_19);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_19, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_4), L_20, /*hidden argument*/NULL);
		// connector.GetPositionFactor(m_EffectArea, i, rect, vertex.position, out x, out y);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_21;
		L_21 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		int32_t L_22 = __this->get_m_EffectArea_25();
		int32_t L_23 = V_6;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_24 = V_3;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_25 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = L_25.get_position_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_27;
		L_27 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_26, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker6< int32_t, int32_t, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 , Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , float*, float* >::Invoke(12 /* System.Void Coffee.UIEffects.GraphicConnector::GetPositionFactor(Coffee.UIEffects.EffectArea,System.Int32,UnityEngine.Rect,UnityEngine.Vector2,System.Single&,System.Single&) */, L_21, L_22, L_23, L_24, L_27, (float*)(&V_7), (float*)(&V_8));
		// vertex.uv0 = new Vector2(
		//     Packer.ToFloat(vertex.uv0.x, vertex.uv0.y),
		//     Packer.ToFloat(x, y, normalizedIndex)
		// );
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_28 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_29 = L_28.get_uv0_4();
		float L_30 = L_29.get_x_1();
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_31 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_32 = L_31.get_uv0_4();
		float L_33 = L_32.get_y_2();
		float L_34;
		L_34 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_30, L_33, /*hidden argument*/NULL);
		float L_35 = V_7;
		float L_36 = V_8;
		float L_37 = V_0;
		float L_38;
		L_38 = Packer_ToFloat_m7691266B0941E760FB34C506126EEAF8A8FBE1D7(L_35, L_36, L_37, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_39;
		memset((&L_39), 0, sizeof(L_39));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_39), L_34, L_38, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_40;
		L_40 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_39, /*hidden argument*/NULL);
		(&V_4)->set_uv0_4(L_40);
		// vh.SetUIVertex(vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_41 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_42 = V_4;
		int32_t L_43 = V_6;
		NullCheck(L_41);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_41, L_42, L_43, /*hidden argument*/NULL);
		// for (var i = 0; i < count; i++)
		int32_t L_44 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_00e8:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_45 = V_6;
		int32_t L_46 = V_5;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_0072;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::SetEffectParamsDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_SetEffectParamsDirty_m654479EEBFC40E3EC6A07438BC27CF1BC4A33D83 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// paramTex.SetData(this, 0, m_EffectFactor); // param1.x : location
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_1 = __this->get_m_EffectFactor_19();
		NullCheck(L_0);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_0, __this, 0, L_1, /*hidden argument*/NULL);
		// paramTex.SetData(this, 1, m_Width); // param1.y : width
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_2;
		L_2 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_3 = __this->get_m_Width_20();
		NullCheck(L_2);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_2, __this, 1, L_3, /*hidden argument*/NULL);
		// paramTex.SetData(this, 2, m_Softness); // param1.z : softness
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_4;
		L_4 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_5 = __this->get_m_Softness_21();
		NullCheck(L_4);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_4, __this, 2, L_5, /*hidden argument*/NULL);
		// paramTex.SetData(this, 4, m_Color.r); // param2.x : red
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_6;
		L_6 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_7 = __this->get_address_of_m_Color_22();
		float L_8 = L_7->get_r_0();
		NullCheck(L_6);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_6, __this, 4, L_8, /*hidden argument*/NULL);
		// paramTex.SetData(this, 5, m_Color.g); // param2.y : green
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_9;
		L_9 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_10 = __this->get_address_of_m_Color_22();
		float L_11 = L_10->get_g_1();
		NullCheck(L_9);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_9, __this, 5, L_11, /*hidden argument*/NULL);
		// paramTex.SetData(this, 6, m_Color.b); // param2.z : blue
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_12;
		L_12 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_13 = __this->get_address_of_m_Color_22();
		float L_14 = L_13->get_b_2();
		NullCheck(L_12);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_12, __this, 6, L_14, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::SetVerticesDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_SetVerticesDirty_mDFDAC5475D74749DE0261E86EAC59E16A03F396E (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// base.SetVerticesDirty();
		BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9(__this, /*hidden argument*/NULL);
		// _lastKeepAspectRatio = m_KeepAspectRatio;
		bool L_0 = __this->get_m_KeepAspectRatio_26();
		__this->set__lastKeepAspectRatio_16(L_0);
		// _lastEffectArea = m_EffectArea;
		int32_t L_1 = __this->get_m_EffectArea_25();
		__this->set__lastEffectArea_17(L_1);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::OnDidApplyAnimationProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_OnDidApplyAnimationProperties_m74FCBDF6CEC9CB6D293687D6651CC4F36A5E5E3E (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// base.OnDidApplyAnimationProperties();
		BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E(__this, /*hidden argument*/NULL);
		// if (_lastKeepAspectRatio != m_KeepAspectRatio
		//     || _lastEffectArea != m_EffectArea)
		bool L_0 = __this->get__lastKeepAspectRatio_16();
		bool L_1 = __this->get_m_KeepAspectRatio_26();
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_0022;
		}
	}
	{
		int32_t L_2 = __this->get__lastEffectArea_17();
		int32_t L_3 = __this->get_m_EffectArea_25();
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_0028;
		}
	}

IL_0022:
	{
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
	}

IL_0028:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::Play(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_Play_m11CEE7F8593F191A9B3A6E7908318DB68F4D0822 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, bool ___reset0, const RuntimeMethod* method)
{
	{
		// effectPlayer.Play(reset);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A(__this, /*hidden argument*/NULL);
		bool L_1 = ___reset0;
		NullCheck(L_0);
		EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802(L_0, L_1, (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)NULL, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::Stop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_Stop_mB2C1169F0638C0692559A4366BFA2C3D7BADB676 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, bool ___reset0, const RuntimeMethod* method)
{
	{
		// effectPlayer.Stop(reset);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A(__this, /*hidden argument*/NULL);
		bool L_1 = ___reset0;
		NullCheck(L_0);
		EffectPlayer_Stop_mCDDB096B24E282D85C931BC545CE0C03FE4AF357(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_OnEnable_m185245A83DA598463AED735F1E901B86AB945B7B (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIDissolve_U3COnEnableU3Eb__54_0_m6FEE944A61838694BDB117ADC92E7733C84C07AF_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnEnable();
		BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE(__this, /*hidden argument*/NULL);
		// effectPlayer.OnEnable((f) => effectFactor = m_Reverse ? 1f - f : f);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A(__this, /*hidden argument*/NULL);
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_1 = (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)il2cpp_codegen_object_new(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3(L_1, __this, (intptr_t)((intptr_t)UIDissolve_U3COnEnableU3Eb__54_0_m6FEE944A61838694BDB117ADC92E7733C84C07AF_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		NullCheck(L_0);
		EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_OnDisable_m1381CC4F4C83657F28506892C9B8C12BC966B774 (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	{
		// base.OnDisable();
		BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580(__this, /*hidden argument*/NULL);
		// effectPlayer.OnDisable();
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve__ctor_m99E95E10A3C975048CAE46A47A0784384791777D (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// float m_EffectFactor = 0.5f;
		__this->set_m_EffectFactor_19((0.5f));
		// float m_Width = 0.5f;
		__this->set_m_Width_20((0.5f));
		// float m_Softness = 0.5f;
		__this->set_m_Softness_21((0.5f));
		// Color m_Color = new Color(0.0f, 0.25f, 1.0f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_0), (0.0f), (0.25f), (1.0f), /*hidden argument*/NULL);
		__this->set_m_Color_22(L_0);
		// ColorMode m_ColorMode = ColorMode.Add;
		__this->set_m_ColorMode_23(2);
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve__cctor_mBF082F322F7071A5B1CCFBF4987AF53ABB84765F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2786830AA9031CD84AE668243586F5295E0F9C48);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly ParameterTexture s_ParamTex = new ParameterTexture(8, 128, "_ParamTex");
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)il2cpp_codegen_object_new(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876(L_0, 8, ((int32_t)128), _stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587, /*hidden argument*/NULL);
		((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->set_s_ParamTex_14(L_0);
		// private static readonly int k_TransitionTexId = Shader.PropertyToID("_TransitionTex");
		int32_t L_1;
		L_1 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral2786830AA9031CD84AE668243586F5295E0F9C48, /*hidden argument*/NULL);
		((UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_StaticFields*)il2cpp_codegen_static_fields_for(UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A_il2cpp_TypeInfo_var))->set_k_TransitionTexId_15(L_1);
		return;
	}
}
// System.Void Coffee.UIEffects.UIDissolve::<OnEnable>b__54_0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIDissolve_U3COnEnableU3Eb__54_0_m6FEE944A61838694BDB117ADC92E7733C84C07AF (UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * __this, float ___f0, const RuntimeMethod* method)
{
	UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * G_B2_0 = NULL;
	UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * G_B1_0 = NULL;
	float G_B3_0 = 0.0f;
	UIDissolve_tBBA075862E946E36E192CB8935B251AF2A73931A * G_B3_1 = NULL;
	{
		// effectPlayer.OnEnable((f) => effectFactor = m_Reverse ? 1f - f : f);
		bool L_0 = __this->get_m_Reverse_28();
		G_B1_0 = __this;
		if (L_0)
		{
			G_B2_0 = __this;
			goto IL_000c;
		}
	}
	{
		float L_1 = ___f0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0013;
	}

IL_000c:
	{
		float L_2 = ___f0;
		G_B3_0 = ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_2));
		G_B3_1 = G_B2_0;
	}

IL_0013:
	{
		NullCheck(G_B3_1);
		UIDissolve_set_effectFactor_mDAD33D00CAF5DB3E62CFDFE65263D7A6E59C0FC2(G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.AdditionalCanvasShaderChannels Coffee.UIEffects.UIEffect::get_uvMaskChannel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIEffect_get_uvMaskChannel_m321DED93A70D6F9C73D1709E4DC7112E977B9404 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return connector.extraChannel; }
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0;
		L_0 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtFuncInvoker0< int32_t >::Invoke(5 /* UnityEngine.AdditionalCanvasShaderChannels Coffee.UIEffects.GraphicConnector::get_extraChannel() */, L_0);
		return L_1;
	}
}
// System.Single Coffee.UIEffects.UIEffect::get_effectFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIEffect_get_effectFactor_m00273C0BB3B80F99858764F8D80FD8D1BD7CB92E (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectFactor; }
		float L_0 = __this->get_m_EffectFactor_15();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_effectFactor_m4CE7979B846A9CC8968C5C50A4333743F9925674 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		float L_2 = __this->get_m_EffectFactor_15();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_EffectFactor = value;
		float L_5 = ___value0;
		__this->set_m_EffectFactor_15(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIEffect::get_colorFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIEffect_get_colorFactor_m75D2A042A8576CEAC03C4B798E4F7DB235AC6ABD (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_ColorFactor; }
		float L_0 = __this->get_m_ColorFactor_16();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_colorFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_colorFactor_m8AB6B802918563B908F4E37AD6BE06592B626864 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_ColorFactor, value)) return;
		float L_2 = __this->get_m_ColorFactor_16();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_ColorFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_ColorFactor = value;
		float L_5 = ___value0;
		__this->set_m_ColorFactor_16(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIEffect::get_blurFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIEffect_get_blurFactor_mB06AFF2FC0044E3ECBD81DBF2C9303FB908E81A4 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_BlurFactor; }
		float L_0 = __this->get_m_BlurFactor_17();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_blurFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_blurFactor_m3C443FD10A75407F541E6EB620B9B55EF85F90F3 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_BlurFactor, value)) return;
		float L_2 = __this->get_m_BlurFactor_17();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_BlurFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_BlurFactor = value;
		float L_5 = ___value0;
		__this->set_m_BlurFactor_17(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.EffectMode Coffee.UIEffects.UIEffect::get_effectMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIEffect_get_effectMode_m4D2487E963178FE58E1AA2FE976227C0B4645C1F (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectMode; }
		int32_t L_0 = __this->get_m_EffectMode_18();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_effectMode(Coffee.UIEffects.EffectMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_effectMode_mCFE02D088374C730DD8E35992E50FD1E1F574991 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_EffectMode == value) return;
		int32_t L_0 = __this->get_m_EffectMode_18();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_EffectMode == value) return;
		return;
	}

IL_000a:
	{
		// m_EffectMode = value;
		int32_t L_2 = ___value0;
		__this->set_m_EffectMode_18(L_2);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// Coffee.UIEffects.ColorMode Coffee.UIEffects.UIEffect::get_colorMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIEffect_get_colorMode_m7A813F78AF2153B5C7A381D7F29A1D9C4511E664 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_ColorMode; }
		int32_t L_0 = __this->get_m_ColorMode_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_colorMode(Coffee.UIEffects.ColorMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_colorMode_m606B079D79B95DA229EF15B571723CEDE7110309 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_ColorMode == value) return;
		int32_t L_0 = __this->get_m_ColorMode_19();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_ColorMode == value) return;
		return;
	}

IL_000a:
	{
		// m_ColorMode = value;
		int32_t L_2 = ___value0;
		__this->set_m_ColorMode_19(L_2);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// Coffee.UIEffects.BlurMode Coffee.UIEffects.UIEffect::get_blurMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIEffect_get_blurMode_m97D9F46E04E8B291EEB1E7ADCAE27842981FA6DD (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_BlurMode; }
		int32_t L_0 = __this->get_m_BlurMode_20();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_blurMode(Coffee.UIEffects.BlurMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_blurMode_m92E5F74746B132960206951A9BCD75B16F1C8ADA (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_BlurMode == value) return;
		int32_t L_0 = __this->get_m_BlurMode_20();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_BlurMode == value) return;
		return;
	}

IL_000a:
	{
		// m_BlurMode = value;
		int32_t L_2 = ___value0;
		__this->set_m_BlurMode_20(L_2);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIEffect::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIEffect_get_paramTex_m9108661AB3CFCE2A89F16E49DEAD847166E30EDF (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return s_ParamTex; }
		IL2CPP_RUNTIME_CLASS_INIT(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ((UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_StaticFields*)il2cpp_codegen_static_fields_for(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var))->get_s_ParamTex_14();
		return L_0;
	}
}
// System.Boolean Coffee.UIEffects.UIEffect::get_advancedBlur()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIEffect_get_advancedBlur_m37BF17E16B82618676E23CF6338093ECA5AEB4AD (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_AdvancedBlur; }
		bool L_0 = __this->get_m_AdvancedBlur_21();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIEffect::set_advancedBlur(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_set_advancedBlur_m9CAFAD3649A2C0F169B9F8B421AD0E94EDACC140 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_AdvancedBlur == value) return;
		bool L_0 = __this->get_m_AdvancedBlur_21();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_AdvancedBlur == value) return;
		return;
	}

IL_000a:
	{
		// m_AdvancedBlur = value;
		bool L_2 = ___value0;
		__this->set_m_AdvancedBlur_21(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.UIEffect::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  UIEffect_GetMaterialHash_mB81CBA44D34978619BA96B13CFFD3491CE6700F0 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	int32_t G_B6_0 = 0;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	int32_t G_B7_1 = 0;
	{
		// if (!isActiveAndEnabled || !material || !material.shader)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___material0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}

IL_001d:
	{
		// return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_6 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_6;
	}

IL_0023:
	{
		// var shaderVariantId = (uint) (((int) m_EffectMode << 6) + ((int) m_ColorMode << 9) +
		//                               ((int) m_BlurMode << 11) + ((m_AdvancedBlur ? 1 : 0) << 13));
		int32_t L_7 = __this->get_m_EffectMode_18();
		int32_t L_8 = __this->get_m_ColorMode_19();
		int32_t L_9 = __this->get_m_BlurMode_20();
		bool L_10 = __this->get_m_AdvancedBlur_21();
		G_B5_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_7<<(int32_t)6)), (int32_t)((int32_t)((int32_t)L_8<<(int32_t)((int32_t)9))))), (int32_t)((int32_t)((int32_t)L_9<<(int32_t)((int32_t)11)))));
		if (L_10)
		{
			G_B6_0 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)((int32_t)L_7<<(int32_t)6)), (int32_t)((int32_t)((int32_t)L_8<<(int32_t)((int32_t)9))))), (int32_t)((int32_t)((int32_t)L_9<<(int32_t)((int32_t)11)))));
			goto IL_004a;
		}
	}
	{
		G_B7_0 = 0;
		G_B7_1 = G_B5_0;
		goto IL_004b;
	}

IL_004a:
	{
		G_B7_0 = 1;
		G_B7_1 = G_B6_0;
	}

IL_004b:
	{
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)G_B7_1, (int32_t)((int32_t)((int32_t)G_B7_0<<(int32_t)((int32_t)13)))));
		// return new Hash128(
		//     (uint) material.GetInstanceID(),
		//     k_ShaderId + shaderVariantId,
		//     0,
		//     0
		// );
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_11 = ___material0;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_11, /*hidden argument*/NULL);
		uint32_t L_13 = V_0;
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Hash128__ctor_m4F9101AF4B79AB2FBC395F48875C21A4E2101581((&L_14), L_12, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)16), (int32_t)L_13)), 0, 0, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void Coffee.UIEffects.UIEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_ModifyMaterial_mBB59C215B8E1F198565EDEB57537247DD4D48DAA (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlurEx_tBF2C48944F19C9913EAFB3463909F0A84DE431B3_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BlurMode_tF85C2C8ED8FC06036AA9CDF0AC0C29E56F48FBEC_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectMode_tC89B96C68859C4857B80D2758E32C8303FCCD1E8_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB7BABBCFFC6BE004BD0E7D3897197B4FD50BF1ED);
		s_Il2CppMethodInitialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B2_1 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B2_2 = NULL;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B2_3 = NULL;
	UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * G_B2_4 = NULL;
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B1_1 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B1_2 = NULL;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B1_3 = NULL;
	UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * G_B1_4 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B3_2 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* G_B3_3 = NULL;
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * G_B3_4 = NULL;
	UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * G_B3_5 = NULL;
	{
		// var connector = GraphicConnector.FindConnector(graphic);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic1;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1;
		L_1 = GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0(L_0, /*hidden argument*/NULL);
		// newMaterial.shader = Shader.Find(string.Format("Hidden/{0} (UIEffect)", newMaterial.shader.name));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___newMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___newMaterial0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralB7BABBCFFC6BE004BD0E7D3897197B4FD50BF1ED, L_5, /*hidden argument*/NULL);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_7;
		L_7 = Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D(L_2, L_7, /*hidden argument*/NULL);
		// SetShaderVariants(newMaterial, m_EffectMode, m_ColorMode, m_BlurMode,
		//     m_AdvancedBlur ? BlurEx.Ex : BlurEx.None);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = ___newMaterial0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_10 = L_9;
		int32_t L_11 = __this->get_m_EffectMode_18();
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(EffectMode_tC89B96C68859C4857B80D2758E32C8303FCCD1E8_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_14 = L_10;
		int32_t L_15 = __this->get_m_ColorMode_19();
		int32_t L_16 = L_15;
		RuntimeObject * L_17 = Box(ColorMode_t83DD019D491EBCD6A28A35C21CF6A7952AD15E9D_il2cpp_TypeInfo_var, &L_16);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_17);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_17);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_18 = L_14;
		int32_t L_19 = __this->get_m_BlurMode_20();
		int32_t L_20 = L_19;
		RuntimeObject * L_21 = Box(BlurMode_tF85C2C8ED8FC06036AA9CDF0AC0C29E56F48FBEC_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		ArrayElementTypeCheck (L_18, L_21);
		(L_18)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_21);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_22 = L_18;
		bool L_23 = __this->get_m_AdvancedBlur_21();
		G_B1_0 = 3;
		G_B1_1 = L_22;
		G_B1_2 = L_22;
		G_B1_3 = L_8;
		G_B1_4 = __this;
		if (L_23)
		{
			G_B2_0 = 3;
			G_B2_1 = L_22;
			G_B2_2 = L_22;
			G_B2_3 = L_8;
			G_B2_4 = __this;
			goto IL_0066;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		G_B3_5 = G_B1_4;
		goto IL_0067;
	}

IL_0066:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
		G_B3_5 = G_B2_4;
	}

IL_0067:
	{
		int32_t L_24 = G_B3_0;
		RuntimeObject * L_25 = Box(BlurEx_tBF2C48944F19C9913EAFB3463909F0A84DE431B3_il2cpp_TypeInfo_var, &L_24);
		NullCheck(G_B3_2);
		ArrayElementTypeCheck (G_B3_2, L_25);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (RuntimeObject *)L_25);
		NullCheck(G_B3_5);
		BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1(G_B3_5, G_B3_4, G_B3_3, /*hidden argument*/NULL);
		// paramTex.RegisterMaterial(newMaterial);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_26;
		L_26 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_27 = ___newMaterial0;
		NullCheck(L_26);
		ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9(L_26, L_27, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_ModifyMesh_m4633FC2713ED5D9CE152E9FFC484F89F9554B543 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_7;
	memset((&V_7), 0, sizeof(V_7));
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_10;
	memset((&V_10), 0, sizeof(V_10));
	int32_t V_11 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_12;
	memset((&V_12), 0, sizeof(V_12));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_13;
	memset((&V_13), 0, sizeof(V_13));
	bool V_14 = false;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_15;
	memset((&V_15), 0, sizeof(V_15));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_16;
	memset((&V_16), 0, sizeof(V_16));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_17;
	memset((&V_17), 0, sizeof(V_17));
	int32_t V_18 = 0;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_19;
	memset((&V_19), 0, sizeof(V_19));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_20;
	memset((&V_20), 0, sizeof(V_20));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_21;
	memset((&V_21), 0, sizeof(V_21));
	int32_t V_22 = 0;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_23;
	memset((&V_23), 0, sizeof(V_23));
	int32_t V_24 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_25;
	memset((&V_25), 0, sizeof(V_25));
	int32_t G_B7_0 = 0;
	int32_t G_B13_0 = 0;
	{
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var normalizedIndex = paramTex.GetNormalizedIndex(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		float L_2;
		L_2 = ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5(L_1, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		// if (m_BlurMode != BlurMode.None && advancedBlur)
		int32_t L_3 = __this->get_m_BlurMode_20();
		if (!L_3)
		{
			goto IL_03bd;
		}
	}
	{
		bool L_4;
		L_4 = UIEffect_get_advancedBlur_m37BF17E16B82618676E23CF6338093ECA5AEB4AD_inline(__this, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_03bd;
		}
	}
	{
		// vh.GetUIVertexStream(s_TempVerts);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_5 = ___vh0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_6 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		NullCheck(L_5);
		VertexHelper_GetUIVertexStream_mA3E62A7B45BFFFC73D72BC7B8BFAD5388F8578BA(L_5, L_6, /*hidden argument*/NULL);
		// vh.Clear();
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_7 = ___vh0;
		NullCheck(L_7);
		VertexHelper_Clear_mBF3FB3CEA5153F8F72C74FFD6006A7AFF62C18BA(L_7, /*hidden argument*/NULL);
		// var count = s_TempVerts.Count;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_8 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		NullCheck(L_8);
		int32_t L_9;
		L_9 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_8, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		V_1 = L_9;
		// int bundleSize = connector.IsText(graphic) ? 6 : count;
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_10;
		L_10 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_11 = ___graphic1;
		NullCheck(L_10);
		bool L_12;
		L_12 = VirtFuncInvoker1< bool, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(13 /* System.Boolean Coffee.UIEffects.GraphicConnector::IsText(UnityEngine.UI.Graphic) */, L_10, L_11);
		if (L_12)
		{
			goto IL_0059;
		}
	}
	{
		int32_t L_13 = V_1;
		G_B7_0 = L_13;
		goto IL_005a;
	}

IL_0059:
	{
		G_B7_0 = 6;
	}

IL_005a:
	{
		V_2 = G_B7_0;
		// Rect posBounds = default(Rect);
		il2cpp_codegen_initobj((&V_3), sizeof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 ));
		// Rect uvBounds = default(Rect);
		il2cpp_codegen_initobj((&V_4), sizeof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 ));
		// Vector3 size = default(Vector3);
		il2cpp_codegen_initobj((&V_5), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		// Vector3 tPos = default(Vector3);
		il2cpp_codegen_initobj((&V_6), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		// Vector3 tUV = default(Vector3);
		il2cpp_codegen_initobj((&V_7), sizeof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E ));
		// float expand = (float) blurMode * 6 * 2;
		int32_t L_14;
		L_14 = UIEffect_get_blurMode_m97D9F46E04E8B291EEB1E7ADCAE27842981FA6DD_inline(__this, /*hidden argument*/NULL);
		V_8 = ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((float)((float)L_14)), (float)(6.0f))), (float)(2.0f)));
		// for (int i = 0; i < count; i += bundleSize)
		V_9 = 0;
		goto IL_039f;
	}

IL_00a0:
	{
		// GetBounds(s_TempVerts, i, bundleSize, ref posBounds, ref uvBounds, true);
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_15 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_16 = V_9;
		int32_t L_17 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var);
		UIEffect_GetBounds_m35577243A9C58E39854A2538290A13D64C205BAD(L_15, L_16, L_17, (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), (Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_4), (bool)1, /*hidden argument*/NULL);
		// Vector2 uvMask = new Vector2(Packer.ToFloat(uvBounds.xMin, uvBounds.yMin),
		//     Packer.ToFloat(uvBounds.xMax, uvBounds.yMax));
		float L_18;
		L_18 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_4), /*hidden argument*/NULL);
		float L_19;
		L_19 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_4), /*hidden argument*/NULL);
		float L_20;
		L_20 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_18, L_19, /*hidden argument*/NULL);
		float L_21;
		L_21 = Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_4), /*hidden argument*/NULL);
		float L_22;
		L_22 = Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_4), /*hidden argument*/NULL);
		float L_23;
		L_23 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_21, L_22, /*hidden argument*/NULL);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_10), L_20, L_23, /*hidden argument*/NULL);
		// for (int j = 0; j < bundleSize; j += 6)
		V_11 = 0;
		goto IL_0391;
	}

IL_00e7:
	{
		// Vector3 cornerPos1 = s_TempVerts[i + j + 1].position;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_24 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_25 = V_9;
		int32_t L_26 = V_11;
		NullCheck(L_24);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_27;
		L_27 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_24, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_25, (int32_t)L_26)), (int32_t)1)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = L_27.get_position_0();
		V_12 = L_28;
		// Vector3 cornerPos2 = s_TempVerts[i + j + 4].position;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_29 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_30 = V_9;
		int32_t L_31 = V_11;
		NullCheck(L_29);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_32;
		L_32 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_29, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)L_31)), (int32_t)4)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = L_32.get_position_0();
		V_13 = L_33;
		// bool hasOuterEdge = (bundleSize == 6)
		//                     || !posBounds.Contains(cornerPos1)
		//                     || !posBounds.Contains(cornerPos2);
		int32_t L_34 = V_2;
		if ((((int32_t)L_34) == ((int32_t)6)))
		{
			goto IL_0134;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_35 = V_12;
		bool L_36;
		L_36 = Rect_Contains_m51C65159B1706EB00CC962D7CD1CEC2EBD85BC3A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), L_35, /*hidden argument*/NULL);
		if (!L_36)
		{
			goto IL_0134;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = V_13;
		bool L_38;
		L_38 = Rect_Contains_m51C65159B1706EB00CC962D7CD1CEC2EBD85BC3A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), L_37, /*hidden argument*/NULL);
		G_B13_0 = ((((int32_t)L_38) == ((int32_t)0))? 1 : 0);
		goto IL_0135;
	}

IL_0134:
	{
		G_B13_0 = 1;
	}

IL_0135:
	{
		V_14 = (bool)G_B13_0;
		// if (hasOuterEdge)
		bool L_39 = V_14;
		if (!L_39)
		{
			goto IL_0221;
		}
	}
	{
		// Vector3 cornerUv1 = s_TempVerts[i + j + 1].uv0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_40 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_41 = V_9;
		int32_t L_42 = V_11;
		NullCheck(L_40);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_43;
		L_43 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_40, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_41, (int32_t)L_42)), (int32_t)1)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_44 = L_43.get_uv0_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_44, /*hidden argument*/NULL);
		// Vector3 cornerUv2 = s_TempVerts[i + j + 4].uv0;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_46 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_47 = V_9;
		int32_t L_48 = V_11;
		NullCheck(L_46);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_49;
		L_49 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_46, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_47, (int32_t)L_48)), (int32_t)4)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_50 = L_49.get_uv0_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51;
		L_51 = Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B(L_50, /*hidden argument*/NULL);
		V_15 = L_51;
		// Vector3 centerPos = (cornerPos1 + cornerPos2) / 2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_52 = V_12;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_53 = V_13;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_54;
		L_54 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_52, L_53, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_55;
		L_55 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_54, (2.0f), /*hidden argument*/NULL);
		V_16 = L_55;
		// Vector3 centerUV = (cornerUv1 + cornerUv2) / 2;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_56 = V_15;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_57;
		L_57 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_45, L_56, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_58;
		L_58 = Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline(L_57, (2.0f), /*hidden argument*/NULL);
		V_17 = L_58;
		// size = (cornerPos1 - cornerPos2);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_59 = V_12;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_60 = V_13;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_61;
		L_61 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_59, L_60, /*hidden argument*/NULL);
		V_5 = L_61;
		// size.x = 1 + expand / Mathf.Abs(size.x);
		float L_62 = V_8;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_63 = V_5;
		float L_64 = L_63.get_x_2();
		float L_65;
		L_65 = fabsf(L_64);
		(&V_5)->set_x_2(((float)il2cpp_codegen_add((float)(1.0f), (float)((float)((float)L_62/(float)L_65)))));
		// size.y = 1 + expand / Mathf.Abs(size.y);
		float L_66 = V_8;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_67 = V_5;
		float L_68 = L_67.get_y_3();
		float L_69;
		L_69 = fabsf(L_68);
		(&V_5)->set_y_3(((float)il2cpp_codegen_add((float)(1.0f), (float)((float)((float)L_66/(float)L_69)))));
		// size.z = 1 + expand / Mathf.Abs(size.z);
		float L_70 = V_8;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_71 = V_5;
		float L_72 = L_71.get_z_4();
		float L_73;
		L_73 = fabsf(L_72);
		(&V_5)->set_z_4(((float)il2cpp_codegen_add((float)(1.0f), (float)((float)((float)L_70/(float)L_73)))));
		// tPos = centerPos - Vector3.Scale(size, centerPos);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_74 = V_16;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_75 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_76 = V_16;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_77;
		L_77 = Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline(L_75, L_76, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_78;
		L_78 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_74, L_77, /*hidden argument*/NULL);
		V_6 = L_78;
		// tUV = centerUV - Vector3.Scale(size, centerUV);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_79 = V_17;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_80 = V_5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_81 = V_17;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_82;
		L_82 = Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline(L_80, L_81, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_83;
		L_83 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_79, L_82, /*hidden argument*/NULL);
		V_7 = L_83;
	}

IL_0221:
	{
		// for (int k = 0; k < 6; k++)
		V_18 = 0;
		goto IL_0383;
	}

IL_0229:
	{
		// UIVertex vt = s_TempVerts[i + j + k];
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_84 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_85 = V_9;
		int32_t L_86 = V_11;
		int32_t L_87 = V_18;
		NullCheck(L_84);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_88;
		L_88 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_84, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_85, (int32_t)L_86)), (int32_t)L_87)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		V_19 = L_88;
		// Vector3 pos = vt.position;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_89 = V_19;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_90 = L_89.get_position_0();
		V_20 = L_90;
		// Vector2 uv0 = vt.uv0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_91 = V_19;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_92 = L_91.get_uv0_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_93;
		L_93 = Vector4_op_Implicit_m3A59F157B9B8A3C2DD495B6F9B76F3C0D40BDFCC(L_92, /*hidden argument*/NULL);
		V_21 = L_93;
		// if (hasOuterEdge && (pos.x < posBounds.xMin || posBounds.xMax < pos.x))
		bool L_94 = V_14;
		if (!L_94)
		{
			goto IL_02b4;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_95 = V_20;
		float L_96 = L_95.get_x_2();
		float L_97;
		L_97 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), /*hidden argument*/NULL);
		if ((((float)L_96) < ((float)L_97)))
		{
			goto IL_0278;
		}
	}
	{
		float L_98;
		L_98 = Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_99 = V_20;
		float L_100 = L_99.get_x_2();
		if ((!(((float)L_98) < ((float)L_100))))
		{
			goto IL_02b4;
		}
	}

IL_0278:
	{
		// pos.x = pos.x * size.x + tPos.x;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_101 = V_20;
		float L_102 = L_101.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_103 = V_5;
		float L_104 = L_103.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_105 = V_6;
		float L_106 = L_105.get_x_2();
		(&V_20)->set_x_2(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_102, (float)L_104)), (float)L_106)));
		// uv0.x = uv0.x * size.x + tUV.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_107 = V_21;
		float L_108 = L_107.get_x_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_109 = V_5;
		float L_110 = L_109.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_111 = V_7;
		float L_112 = L_111.get_x_2();
		(&V_21)->set_x_0(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_108, (float)L_110)), (float)L_112)));
	}

IL_02b4:
	{
		// if (hasOuterEdge && (pos.y < posBounds.yMin || posBounds.yMax < pos.y))
		bool L_113 = V_14;
		if (!L_113)
		{
			goto IL_0314;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_114 = V_20;
		float L_115 = L_114.get_y_3();
		float L_116;
		L_116 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), /*hidden argument*/NULL);
		if ((((float)L_115) < ((float)L_116)))
		{
			goto IL_02d8;
		}
	}
	{
		float L_117;
		L_117 = Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_3), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_118 = V_20;
		float L_119 = L_118.get_y_3();
		if ((!(((float)L_117) < ((float)L_119))))
		{
			goto IL_0314;
		}
	}

IL_02d8:
	{
		// pos.y = pos.y * size.y + tPos.y;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_120 = V_20;
		float L_121 = L_120.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_122 = V_5;
		float L_123 = L_122.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_124 = V_6;
		float L_125 = L_124.get_y_3();
		(&V_20)->set_y_3(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_121, (float)L_123)), (float)L_125)));
		// uv0.y = uv0.y * size.y + tUV.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_126 = V_21;
		float L_127 = L_126.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_128 = V_5;
		float L_129 = L_128.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_130 = V_7;
		float L_131 = L_130.get_y_3();
		(&V_21)->set_y_1(((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_127, (float)L_129)), (float)L_131)));
	}

IL_0314:
	{
		// vt.uv0 = new Vector2(Packer.ToFloat((uv0.x + 0.5f) / 2f, (uv0.y + 0.5f) / 2f),
		//     normalizedIndex);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_132 = V_21;
		float L_133 = L_132.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_134 = V_21;
		float L_135 = L_134.get_y_1();
		float L_136;
		L_136 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(((float)((float)((float)il2cpp_codegen_add((float)L_133, (float)(0.5f)))/(float)(2.0f))), ((float)((float)((float)il2cpp_codegen_add((float)L_135, (float)(0.5f)))/(float)(2.0f))), /*hidden argument*/NULL);
		float L_137 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_138;
		memset((&L_138), 0, sizeof(L_138));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_138), L_136, L_137, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_139;
		L_139 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_138, /*hidden argument*/NULL);
		(&V_19)->set_uv0_4(L_139);
		// vt.position = pos;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_140 = V_20;
		(&V_19)->set_position_0(L_140);
		// connector.SetExtraChannel(ref vt, uvMask);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_141;
		L_141 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_142 = V_10;
		NullCheck(L_141);
		VirtActionInvoker2< UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  >::Invoke(14 /* System.Void Coffee.UIEffects.GraphicConnector::SetExtraChannel(UnityEngine.UIVertex&,UnityEngine.Vector2) */, L_141, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_19), L_142);
		// s_TempVerts[i + j + k] = vt;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_143 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		int32_t L_144 = V_9;
		int32_t L_145 = V_11;
		int32_t L_146 = V_18;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_147 = V_19;
		NullCheck(L_143);
		List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3(L_143, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_144, (int32_t)L_145)), (int32_t)L_146)), L_147, /*hidden argument*/List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_RuntimeMethod_var);
		// for (int k = 0; k < 6; k++)
		int32_t L_148 = V_18;
		V_18 = ((int32_t)il2cpp_codegen_add((int32_t)L_148, (int32_t)1));
	}

IL_0383:
	{
		// for (int k = 0; k < 6; k++)
		int32_t L_149 = V_18;
		if ((((int32_t)L_149) < ((int32_t)6)))
		{
			goto IL_0229;
		}
	}
	{
		// for (int j = 0; j < bundleSize; j += 6)
		int32_t L_150 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_150, (int32_t)6));
	}

IL_0391:
	{
		// for (int j = 0; j < bundleSize; j += 6)
		int32_t L_151 = V_11;
		int32_t L_152 = V_2;
		if ((((int32_t)L_151) < ((int32_t)L_152)))
		{
			goto IL_00e7;
		}
	}
	{
		// for (int i = 0; i < count; i += bundleSize)
		int32_t L_153 = V_9;
		int32_t L_154 = V_2;
		V_9 = ((int32_t)il2cpp_codegen_add((int32_t)L_153, (int32_t)L_154));
	}

IL_039f:
	{
		// for (int i = 0; i < count; i += bundleSize)
		int32_t L_155 = V_9;
		int32_t L_156 = V_1;
		if ((((int32_t)L_155) < ((int32_t)L_156)))
		{
			goto IL_00a0;
		}
	}
	{
		// vh.AddUIVertexTriangleStream(s_TempVerts);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_157 = ___vh0;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_158 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		NullCheck(L_157);
		VertexHelper_AddUIVertexTriangleStream_m3FC7DF3D1DA3F0D40025258E3B8FF5830EE7CE55(L_157, L_158, /*hidden argument*/NULL);
		// s_TempVerts.Clear();
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_159 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_s_TempVerts_9();
		NullCheck(L_159);
		List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB(L_159, /*hidden argument*/List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_RuntimeMethod_var);
		// }
		return;
	}

IL_03bd:
	{
		// int count = vh.currentVertCount;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_160 = ___vh0;
		NullCheck(L_160);
		int32_t L_161;
		L_161 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_160, /*hidden argument*/NULL);
		V_22 = L_161;
		// UIVertex vt = default(UIVertex);
		il2cpp_codegen_initobj((&V_23), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// for (int i = 0; i < count; i++)
		V_24 = 0;
		goto IL_0437;
	}

IL_03d2:
	{
		// vh.PopulateUIVertex(ref vt, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_162 = ___vh0;
		int32_t L_163 = V_24;
		NullCheck(L_162);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_162, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_23), L_163, /*hidden argument*/NULL);
		// Vector2 uv0 = vt.uv0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_164 = V_23;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_165 = L_164.get_uv0_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_166;
		L_166 = Vector4_op_Implicit_m3A59F157B9B8A3C2DD495B6F9B76F3C0D40BDFCC(L_165, /*hidden argument*/NULL);
		V_25 = L_166;
		// vt.uv0 = new Vector2(
		//     Packer.ToFloat((uv0.x + 0.5f) / 2f, (uv0.y + 0.5f) / 2f),
		//     normalizedIndex
		// );
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_167 = V_25;
		float L_168 = L_167.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_169 = V_25;
		float L_170 = L_169.get_y_1();
		float L_171;
		L_171 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(((float)((float)((float)il2cpp_codegen_add((float)L_168, (float)(0.5f)))/(float)(2.0f))), ((float)((float)((float)il2cpp_codegen_add((float)L_170, (float)(0.5f)))/(float)(2.0f))), /*hidden argument*/NULL);
		float L_172 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_173;
		memset((&L_173), 0, sizeof(L_173));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_173), L_171, L_172, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_174;
		L_174 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_173, /*hidden argument*/NULL);
		(&V_23)->set_uv0_4(L_174);
		// vh.SetUIVertex(vt, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_175 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_176 = V_23;
		int32_t L_177 = V_24;
		NullCheck(L_175);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_175, L_176, L_177, /*hidden argument*/NULL);
		// for (int i = 0; i < count; i++)
		int32_t L_178 = V_24;
		V_24 = ((int32_t)il2cpp_codegen_add((int32_t)L_178, (int32_t)1));
	}

IL_0437:
	{
		// for (int i = 0; i < count; i++)
		int32_t L_179 = V_24;
		int32_t L_180 = V_22;
		if ((((int32_t)L_179) < ((int32_t)L_180)))
		{
			goto IL_03d2;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIEffect::SetEffectParamsDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_SetEffectParamsDirty_mFAEDBE0F8F4804226CB2B62195BEBFEF996B2476 (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// paramTex.SetData(this, 0, m_EffectFactor); // param.x : effect factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_1 = __this->get_m_EffectFactor_15();
		NullCheck(L_0);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_0, __this, 0, L_1, /*hidden argument*/NULL);
		// paramTex.SetData(this, 1, m_ColorFactor); // param.y : color factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_2;
		L_2 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_3 = __this->get_m_ColorFactor_16();
		NullCheck(L_2);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_2, __this, 1, L_3, /*hidden argument*/NULL);
		// paramTex.SetData(this, 2, m_BlurFactor); // param.z : blur factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_4;
		L_4 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_5 = __this->get_m_BlurFactor_17();
		NullCheck(L_4);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_4, __this, 2, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIEffect::GetBounds(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Int32,System.Int32,UnityEngine.Rect&,UnityEngine.Rect&,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect_GetBounds_m35577243A9C58E39854A2538290A13D64C205BAD (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, int32_t ___start1, int32_t ___count2, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * ___posBounds3, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * ___uvBounds4, bool ___global5, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_5;
	memset((&V_5), 0, sizeof(V_5));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_6;
	memset((&V_6), 0, sizeof(V_6));
	{
		// Vector2 minPos = new Vector2(float.MaxValue, float.MaxValue);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_0), ((std::numeric_limits<float>::max)()), ((std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		// Vector2 maxPos = new Vector2(float.MinValue, float.MinValue);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_1), (-(std::numeric_limits<float>::max)()), (-(std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		// Vector2 minUV = new Vector2(float.MaxValue, float.MaxValue);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_2), ((std::numeric_limits<float>::max)()), ((std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		// Vector2 maxUV = new Vector2(float.MinValue, float.MinValue);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_3), (-(std::numeric_limits<float>::max)()), (-(std::numeric_limits<float>::max)()), /*hidden argument*/NULL);
		// for (int i = start; i < start + count; i++)
		int32_t L_0 = ___start1;
		V_4 = L_0;
		goto IL_0100;
	}

IL_004c:
	{
		// UIVertex vt = verts[i];
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_1 = ___verts0;
		int32_t L_2 = V_4;
		NullCheck(L_1);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_3;
		L_3 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_1, L_2, /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		// Vector2 uv = vt.uv0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_4 = L_3;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_5 = L_4.get_uv0_4();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Vector4_op_Implicit_m3A59F157B9B8A3C2DD495B6F9B76F3C0D40BDFCC(L_5, /*hidden argument*/NULL);
		V_5 = L_6;
		// Vector3 pos = vt.position;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = L_4.get_position_0();
		V_6 = L_7;
		// if (minPos.x >= pos.x && minPos.y >= pos.y)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = V_0;
		float L_9 = L_8.get_x_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_6;
		float L_11 = L_10.get_x_2();
		if ((!(((float)L_9) >= ((float)L_11))))
		{
			goto IL_0090;
		}
	}
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_12 = V_0;
		float L_13 = L_12.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = V_6;
		float L_15 = L_14.get_y_3();
		if ((!(((float)L_13) >= ((float)L_15))))
		{
			goto IL_0090;
		}
	}
	{
		// minPos = pos;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = V_6;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_17;
		L_17 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_16, /*hidden argument*/NULL);
		V_0 = L_17;
		// }
		goto IL_00b6;
	}

IL_0090:
	{
		// else if (maxPos.x <= pos.x && maxPos.y <= pos.y)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_18 = V_1;
		float L_19 = L_18.get_x_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = V_6;
		float L_21 = L_20.get_x_2();
		if ((!(((float)L_19) <= ((float)L_21))))
		{
			goto IL_00b6;
		}
	}
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_22 = V_1;
		float L_23 = L_22.get_y_1();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = V_6;
		float L_25 = L_24.get_y_3();
		if ((!(((float)L_23) <= ((float)L_25))))
		{
			goto IL_00b6;
		}
	}
	{
		// maxPos = pos;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_6;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_27;
		L_27 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_26, /*hidden argument*/NULL);
		V_1 = L_27;
	}

IL_00b6:
	{
		// if (minUV.x >= uv.x && minUV.y >= uv.y)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_28 = V_2;
		float L_29 = L_28.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_30 = V_5;
		float L_31 = L_30.get_x_0();
		if ((!(((float)L_29) >= ((float)L_31))))
		{
			goto IL_00d9;
		}
	}
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_32 = V_2;
		float L_33 = L_32.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_34 = V_5;
		float L_35 = L_34.get_y_1();
		if ((!(((float)L_33) >= ((float)L_35))))
		{
			goto IL_00d9;
		}
	}
	{
		// minUV = uv;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_36 = V_5;
		V_2 = L_36;
		// }
		goto IL_00fa;
	}

IL_00d9:
	{
		// else if (maxUV.x <= uv.x && maxUV.y <= uv.y)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_37 = V_3;
		float L_38 = L_37.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_39 = V_5;
		float L_40 = L_39.get_x_0();
		if ((!(((float)L_38) <= ((float)L_40))))
		{
			goto IL_00fa;
		}
	}
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_41 = V_3;
		float L_42 = L_41.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_43 = V_5;
		float L_44 = L_43.get_y_1();
		if ((!(((float)L_42) <= ((float)L_44))))
		{
			goto IL_00fa;
		}
	}
	{
		// maxUV = uv;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_45 = V_5;
		V_3 = L_45;
	}

IL_00fa:
	{
		// for (int i = start; i < start + count; i++)
		int32_t L_46 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_0100:
	{
		// for (int i = start; i < start + count; i++)
		int32_t L_47 = V_4;
		int32_t L_48 = ___start1;
		int32_t L_49 = ___count2;
		if ((((int32_t)L_47) < ((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)L_49)))))
		{
			goto IL_004c;
		}
	}
	{
		// posBounds.Set(minPos.x + 0.001f, minPos.y + 0.001f, maxPos.x - minPos.x - 0.002f,
		//     maxPos.y - minPos.y - 0.002f);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * L_50 = ___posBounds3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_51 = V_0;
		float L_52 = L_51.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_53 = V_0;
		float L_54 = L_53.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_55 = V_1;
		float L_56 = L_55.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_57 = V_0;
		float L_58 = L_57.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_59 = V_1;
		float L_60 = L_59.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_61 = V_0;
		float L_62 = L_61.get_y_1();
		Rect_Set_mACEFC5D6FC4E52EDE4480B222485B1FB49951F5B((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)L_50, ((float)il2cpp_codegen_add((float)L_52, (float)(0.00100000005f))), ((float)il2cpp_codegen_add((float)L_54, (float)(0.00100000005f))), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_56, (float)L_58)), (float)(0.00200000009f))), ((float)il2cpp_codegen_subtract((float)((float)il2cpp_codegen_subtract((float)L_60, (float)L_62)), (float)(0.00200000009f))), /*hidden argument*/NULL);
		// uvBounds.Set(minUV.x, minUV.y, maxUV.x - minUV.x, maxUV.y - minUV.y);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 * L_63 = ___uvBounds4;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_64 = V_2;
		float L_65 = L_64.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_66 = V_2;
		float L_67 = L_66.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_68 = V_3;
		float L_69 = L_68.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_70 = V_2;
		float L_71 = L_70.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_72 = V_3;
		float L_73 = L_72.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_74 = V_2;
		float L_75 = L_74.get_y_1();
		Rect_Set_mACEFC5D6FC4E52EDE4480B222485B1FB49951F5B((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)L_63, L_65, L_67, ((float)il2cpp_codegen_subtract((float)L_69, (float)L_71)), ((float)il2cpp_codegen_subtract((float)L_73, (float)L_75)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect__ctor_mEC430F1FC686DCDF70EE103A523A09F8E19DA37F (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// float m_EffectFactor = 1;
		__this->set_m_EffectFactor_15((1.0f));
		// float m_ColorFactor = 1;
		__this->set_m_ColorFactor_16((1.0f));
		// float m_BlurFactor = 1;
		__this->set_m_BlurFactor_17((1.0f));
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UIEffect::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIEffect__cctor_m0BEF19CA78CE673FB7B29883F6705793CEB2DB33 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly ParameterTexture s_ParamTex = new ParameterTexture(4, 1024, "_ParamTex");
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)il2cpp_codegen_object_new(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876(L_0, 4, ((int32_t)1024), _stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587, /*hidden argument*/NULL);
		((UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_StaticFields*)il2cpp_codegen_static_fields_for(UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_il2cpp_TypeInfo_var))->set_s_ParamTex_14(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Coffee.UIEffects.UIFlip::get_horizontal()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIFlip_get_horizontal_mF54C27E018057B8F9A36E7CA2E6FADA1D7743114 (UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Horizontal; }
		bool L_0 = __this->get_m_Horizontal_8();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIFlip::set_horizontal(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFlip_set_horizontal_m918D709FB4B6888F5E24F3712CEC77AA68DB5E37 (UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Horizontal == value) return;
		bool L_0 = __this->get_m_Horizontal_8();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_Horizontal == value) return;
		return;
	}

IL_000a:
	{
		// m_Horizontal = value;
		bool L_2 = ___value0;
		__this->set_m_Horizontal_8(L_2);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.UIFlip::get_vertical()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIFlip_get_vertical_m0250BFEECB51A1633A09D2CF5C705D1587C3D06A (UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Veritical; }
		bool L_0 = __this->get_m_Veritical_9();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIFlip::set_vertical(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFlip_set_vertical_m71A359FCC21F9D5DE5C9007DBC4A520FC4CA5F62 (UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Veritical == value) return;
		bool L_0 = __this->get_m_Veritical_9();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_Veritical == value) return;
		return;
	}

IL_000a:
	{
		// m_Veritical = value;
		bool L_2 = ___value0;
		__this->set_m_Veritical_9(L_2);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIFlip::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFlip_ModifyMesh_m79A6FC275515C150F5FCABE88B97ED4BD21A1645 (UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * G_B5_0 = NULL;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * G_B6_1 = NULL;
	float G_B8_0 = 0.0f;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * G_B8_1 = NULL;
	float G_B7_0 = 0.0f;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * G_B7_1 = NULL;
	float G_B9_0 = 0.0f;
	float G_B9_1 = 0.0f;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A * G_B9_2 = NULL;
	{
		// if (!isActiveAndEnabled) return;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!isActiveAndEnabled) return;
		return;
	}

IL_0009:
	{
		// var vt = default(UIVertex);
		il2cpp_codegen_initobj((&V_0), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// for (var i = 0; i < vh.currentVertCount; i++)
		V_1 = 0;
		goto IL_006b;
	}

IL_0015:
	{
		// vh.PopulateUIVertex(ref vt, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_1 = ___vh0;
		int32_t L_2 = V_1;
		NullCheck(L_1);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_1, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_0), L_2, /*hidden argument*/NULL);
		// var pos = vt.position;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_3 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = L_3.get_position_0();
		V_2 = L_4;
		// vt.position = new Vector3(
		//     m_Horizontal ? -pos.x : pos.x,
		//     m_Veritical ? -pos.y : pos.y
		// );
		bool L_5 = __this->get_m_Horizontal_8();
		G_B4_0 = (&V_0);
		if (L_5)
		{
			G_B5_0 = (&V_0);
			goto IL_0037;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = V_2;
		float L_7 = L_6.get_x_2();
		G_B6_0 = L_7;
		G_B6_1 = G_B4_0;
		goto IL_003e;
	}

IL_0037:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_2;
		float L_9 = L_8.get_x_2();
		G_B6_0 = ((-L_9));
		G_B6_1 = G_B5_0;
	}

IL_003e:
	{
		bool L_10 = __this->get_m_Veritical_9();
		G_B7_0 = G_B6_0;
		G_B7_1 = G_B6_1;
		if (L_10)
		{
			G_B8_0 = G_B6_0;
			G_B8_1 = G_B6_1;
			goto IL_004e;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = V_2;
		float L_12 = L_11.get_y_3();
		G_B9_0 = L_12;
		G_B9_1 = G_B7_0;
		G_B9_2 = G_B7_1;
		goto IL_0055;
	}

IL_004e:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_2;
		float L_14 = L_13.get_y_3();
		G_B9_0 = ((-L_14));
		G_B9_1 = G_B8_0;
		G_B9_2 = G_B8_1;
	}

IL_0055:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline((&L_15), G_B9_1, G_B9_0, /*hidden argument*/NULL);
		G_B9_2->set_position_0(L_15);
		// vh.SetUIVertex(vt, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_16 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_17 = V_0;
		int32_t L_18 = V_1;
		NullCheck(L_16);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_16, L_17, L_18, /*hidden argument*/NULL);
		// for (var i = 0; i < vh.currentVertCount; i++)
		int32_t L_19 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)1));
	}

IL_006b:
	{
		// for (var i = 0; i < vh.currentVertCount; i++)
		int32_t L_20 = V_1;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_21 = ___vh0;
		NullCheck(L_21);
		int32_t L_22;
		L_22 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_21, /*hidden argument*/NULL);
		if ((((int32_t)L_20) < ((int32_t)L_22)))
		{
			goto IL_0015;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIFlip::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIFlip__ctor_mD4CE3113E5EA38CB61767AD622C84063715A94E3 (UIFlip_tC43C62282079E40C0C48BDF3A07F49E3C42C9B7D * __this, const RuntimeMethod* method)
{
	{
		BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Coffee.UIEffects.UIGradient/Direction Coffee.UIEffects.UIGradient::get_direction()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIGradient_get_direction_m094F3EB30335D4C941FFE1C9CD92A1EA46FFD209 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Direction; }
		int32_t L_0 = __this->get_m_Direction_9();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_direction(Coffee.UIEffects.UIGradient/Direction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_direction_m978ABB95A3EE5868DC04D333CCA44D48CD79CA54 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Direction == value) return;
		int32_t L_0 = __this->get_m_Direction_9();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_Direction == value) return;
		return;
	}

IL_000a:
	{
		// m_Direction = value;
		int32_t L_2 = ___value0;
		__this->set_m_Direction_9(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Color Coffee.UIEffects.UIGradient::get_color1()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIGradient_get_color1_m423D956F1D7112DA243036673C5132976AAAAC38 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Color1; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color1_10();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_color1(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_color1_mE085DBC6446EFEF5659869CAB0A24388A22BC0F4 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Color1 == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color1_10();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_Color1 == value) return;
		return;
	}

IL_000f:
	{
		// m_Color1 = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_Color1_10(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Color Coffee.UIEffects.UIGradient::get_color2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIGradient_get_color2_mF8239D06170F4FB39247E60942112B738997056E (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Color2; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color2_11();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_color2(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_color2_mFCC6472DFDAF34B009A11A581EE40A2FDF698C24 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Color2 == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color2_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_Color2 == value) return;
		return;
	}

IL_000f:
	{
		// m_Color2 = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_Color2_11(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Color Coffee.UIEffects.UIGradient::get_color3()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIGradient_get_color3_m6A6ADF7604891CF1BE6BAFBAA652F1CA2E97C7FC (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Color3; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color3_12();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_color3(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_color3_m5866EAB3CF8D10559C80944A1CD53125CF994551 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Color3 == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color3_12();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_Color3 == value) return;
		return;
	}

IL_000f:
	{
		// m_Color3 = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_Color3_12(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Color Coffee.UIEffects.UIGradient::get_color4()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIGradient_get_color4_mA95E03D16F86A39D3003BC0390629E40E3859298 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Color4; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color4_13();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_color4(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_color4_m0A4217B2A704F895A15AD5745970225D7047C837 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Color4 == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_Color4_13();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_Color4 == value) return;
		return;
	}

IL_000f:
	{
		// m_Color4 = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_Color4_13(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIGradient::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIGradient_get_rotation_m77FADBF0C5FC07638849FBB32F7DC3739876108E (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// return m_Direction == Direction.Horizontal ? -90
		//     : m_Direction == Direction.Vertical ? 0
		//     : m_Rotation;
		int32_t L_0 = __this->get_m_Direction_9();
		if (!L_0)
		{
			goto IL_001e;
		}
	}
	{
		int32_t L_1 = __this->get_m_Direction_9();
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0018;
		}
	}
	{
		float L_2 = __this->get_m_Rotation_14();
		return L_2;
	}

IL_0018:
	{
		return (0.0f);
	}

IL_001e:
	{
		return (-90.0f);
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_rotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_rotation_m4581F1FA269346868CC5FFC6A716ACDAF59E45A7 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// if (Mathf.Approximately(m_Rotation, value)) return;
		float L_0 = __this->get_m_Rotation_14();
		float L_1 = ___value0;
		bool L_2;
		L_2 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (Mathf.Approximately(m_Rotation, value)) return;
		return;
	}

IL_000f:
	{
		// m_Rotation = value;
		float L_3 = ___value0;
		__this->set_m_Rotation_14(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIGradient::get_offset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIGradient_get_offset_mF798A5666929324378FD513ED51C988EE5091887 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Offset1; }
		float L_0 = __this->get_m_Offset1_15();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_offset(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_offset_m3F3A747D71F33B04448E557E6183FDD2E4849EEA (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// if (Mathf.Approximately(m_Offset1, value)) return;
		float L_0 = __this->get_m_Offset1_15();
		float L_1 = ___value0;
		bool L_2;
		L_2 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (Mathf.Approximately(m_Offset1, value)) return;
		return;
	}

IL_000f:
	{
		// m_Offset1 = value;
		float L_3 = ___value0;
		__this->set_m_Offset1_15(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Vector2 Coffee.UIEffects.UIGradient::get_offset2()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  UIGradient_get_offset2_m5B5F98FAADFFC9ACD34BC208000DCC0D0318507C (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return new Vector2(m_Offset2, m_Offset1); }
		float L_0 = __this->get_m_Offset2_16();
		float L_1 = __this->get_m_Offset1_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_offset2(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_offset2_m6941C3CAB75BFAD13296ED00F6E3A83865C830B2 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method)
{
	{
		// if (Mathf.Approximately(m_Offset1, value.y) && Mathf.Approximately(m_Offset2, value.x)) return;
		float L_0 = __this->get_m_Offset1_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1 = ___value0;
		float L_2 = L_1.get_y_1();
		bool L_3;
		L_3 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0027;
		}
	}
	{
		float L_4 = __this->get_m_Offset2_16();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = ___value0;
		float L_6 = L_5.get_x_0();
		bool L_7;
		L_7 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0027;
		}
	}
	{
		// if (Mathf.Approximately(m_Offset1, value.y) && Mathf.Approximately(m_Offset2, value.x)) return;
		return;
	}

IL_0027:
	{
		// m_Offset1 = value.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = ___value0;
		float L_9 = L_8.get_y_1();
		__this->set_m_Offset1_15(L_9);
		// m_Offset2 = value.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_10 = ___value0;
		float L_11 = L_10.get_x_0();
		__this->set_m_Offset2_16(L_11);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.UIGradient/GradientStyle Coffee.UIEffects.UIGradient::get_gradientStyle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIGradient_get_gradientStyle_mE294AE9A685ED8906035884704E0C7158764840D (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_GradientStyle; }
		int32_t L_0 = __this->get_m_GradientStyle_17();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_gradientStyle(Coffee.UIEffects.UIGradient/GradientStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_gradientStyle_m44B6AB97EA9E06EF31A34906E03AA0A41EB2D30B (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_GradientStyle == value) return;
		int32_t L_0 = __this->get_m_GradientStyle_17();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_GradientStyle == value) return;
		return;
	}

IL_000a:
	{
		// m_GradientStyle = value;
		int32_t L_2 = ___value0;
		__this->set_m_GradientStyle_17(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.ColorSpace Coffee.UIEffects.UIGradient::get_colorSpace()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIGradient_get_colorSpace_m7BC8625644E03D801B620B418B219676DB742020 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_ColorSpace; }
		int32_t L_0 = __this->get_m_ColorSpace_18();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_colorSpace(UnityEngine.ColorSpace)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_colorSpace_m2336F2412186992DBFFE0072A402C840E06FF989 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_ColorSpace == value) return;
		int32_t L_0 = __this->get_m_ColorSpace_18();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_ColorSpace == value) return;
		return;
	}

IL_000a:
	{
		// m_ColorSpace = value;
		int32_t L_2 = ___value0;
		__this->set_m_ColorSpace_18(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.UIGradient::get_ignoreAspectRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIGradient_get_ignoreAspectRatio_mD64D86E1D8FFD9BEC4606ED2C11257E9D9053AA2 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_IgnoreAspectRatio; }
		bool L_0 = __this->get_m_IgnoreAspectRatio_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIGradient::set_ignoreAspectRatio(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_set_ignoreAspectRatio_m1F52E4804269268E898C04B8ED1C618E313316C2 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_IgnoreAspectRatio == value) return;
		bool L_0 = __this->get_m_IgnoreAspectRatio_19();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_IgnoreAspectRatio == value) return;
		return;
	}

IL_000a:
	{
		// m_IgnoreAspectRatio = value;
		bool L_2 = ___value0;
		__this->set_m_IgnoreAspectRatio_19(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIGradient::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient_ModifyMesh_m23B0DAD902A385843C277C66D01B5359B84B40C6 (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_0;
	memset((&V_0), 0, sizeof(V_0));
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_3;
	memset((&V_3), 0, sizeof(V_3));
	Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  V_4;
	memset((&V_4), 0, sizeof(V_4));
	int32_t V_5 = 0;
	float V_6 = 0.0f;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_9;
	memset((&V_9), 0, sizeof(V_9));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_10;
	memset((&V_10), 0, sizeof(V_10));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B23_0;
	memset((&G_B23_0), 0, sizeof(G_B23_0));
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * G_B23_1 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B20_0;
	memset((&G_B20_0), 0, sizeof(G_B20_0));
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * G_B20_1 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B22_0;
	memset((&G_B22_0), 0, sizeof(G_B22_0));
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * G_B22_1 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B21_0;
	memset((&G_B21_0), 0, sizeof(G_B21_0));
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * G_B21_1 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B24_0;
	memset((&G_B24_0), 0, sizeof(G_B24_0));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  G_B24_1;
	memset((&G_B24_1), 0, sizeof(G_B24_1));
	Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * G_B24_2 = NULL;
	{
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var rect = default(Rect);
		il2cpp_codegen_initobj((&V_0), sizeof(Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 ));
		// var vertex = default(UIVertex);
		il2cpp_codegen_initobj((&V_1), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// switch (m_GradientStyle)
		int32_t L_1 = __this->get_m_GradientStyle_17();
		V_5 = L_1;
		int32_t L_2 = V_5;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0039;
			}
			case 1:
			{
				goto IL_006a;
			}
			case 2:
			{
				goto IL_004a;
			}
		}
	}
	{
		goto IL_0137;
	}

IL_0039:
	{
		// rect = graphic.rectTransform.rect;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_3 = ___graphic1;
		NullCheck(L_3);
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_4;
		L_4 = Graphic_get_rectTransform_m87D5A808474C6B71649CBB153DEBF5F268189EFF(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_5;
		L_5 = RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		// break;
		goto IL_0137;
	}

IL_004a:
	{
		// rect.Set(0, 0, 1, 1);
		Rect_Set_mACEFC5D6FC4E52EDE4480B222485B1FB49951F5B((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), (0.0f), (0.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		// break;
		goto IL_0137;
	}

IL_006a:
	{
		// rect.xMin = rect.yMin = float.MaxValue;
		float L_6 = ((std::numeric_limits<float>::max)());
		V_6 = L_6;
		Rect_set_yMin_mA2FDFF7C8C2361A4CF3F446BAB9A861F923F763A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_6, /*hidden argument*/NULL);
		float L_7 = V_6;
		Rect_set_xMin_mC91AC74347F8E3D537E8C5D70015E9B8EA872A3F((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_7, /*hidden argument*/NULL);
		// rect.xMax = rect.yMax = float.MinValue;
		float L_8 = (-(std::numeric_limits<float>::max)());
		V_6 = L_8;
		Rect_set_yMax_m4E7A7C5E88FA369D6ED022C939427F4895F6635D((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_8, /*hidden argument*/NULL);
		float L_9 = V_6;
		Rect_set_xMax_m4E466ED07B11CC5457BD62517418C493C0DDF2E3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_9, /*hidden argument*/NULL);
		// for (var i = 0; i < vh.currentVertCount; i++)
		V_7 = 0;
		goto IL_012a;
	}

IL_00a2:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_10 = ___vh0;
		int32_t L_11 = V_7;
		NullCheck(L_10);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_10, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_1), L_11, /*hidden argument*/NULL);
		// rect.xMin = Mathf.Min(rect.xMin, vertex.position.x);
		float L_12;
		L_12 = Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_13 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = L_13.get_position_0();
		float L_15 = L_14.get_x_2();
		float L_16;
		L_16 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_12, L_15, /*hidden argument*/NULL);
		Rect_set_xMin_mC91AC74347F8E3D537E8C5D70015E9B8EA872A3F((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_16, /*hidden argument*/NULL);
		// rect.yMin = Mathf.Min(rect.yMin, vertex.position.y);
		float L_17;
		L_17 = Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_18 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19 = L_18.get_position_0();
		float L_20 = L_19.get_y_3();
		float L_21;
		L_21 = Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14(L_17, L_20, /*hidden argument*/NULL);
		Rect_set_yMin_mA2FDFF7C8C2361A4CF3F446BAB9A861F923F763A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_21, /*hidden argument*/NULL);
		// rect.xMax = Mathf.Max(rect.xMax, vertex.position.x);
		float L_22;
		L_22 = Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_23 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24 = L_23.get_position_0();
		float L_25 = L_24.get_x_2();
		float L_26;
		L_26 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_22, L_25, /*hidden argument*/NULL);
		Rect_set_xMax_m4E466ED07B11CC5457BD62517418C493C0DDF2E3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_26, /*hidden argument*/NULL);
		// rect.yMax = Mathf.Max(rect.yMax, vertex.position.y);
		float L_27;
		L_27 = Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_28 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29 = L_28.get_position_0();
		float L_30 = L_29.get_y_3();
		float L_31;
		L_31 = Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775(L_27, L_30, /*hidden argument*/NULL);
		Rect_set_yMax_m4E7A7C5E88FA369D6ED022C939427F4895F6635D((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), L_31, /*hidden argument*/NULL);
		// for (var i = 0; i < vh.currentVertCount; i++)
		int32_t L_32 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_012a:
	{
		// for (var i = 0; i < vh.currentVertCount; i++)
		int32_t L_33 = V_7;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_34 = ___vh0;
		NullCheck(L_34);
		int32_t L_35;
		L_35 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_34, /*hidden argument*/NULL);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_00a2;
		}
	}

IL_0137:
	{
		// var rad = rotation * Mathf.Deg2Rad;
		float L_36;
		L_36 = UIGradient_get_rotation_m77FADBF0C5FC07638849FBB32F7DC3739876108E(__this, /*hidden argument*/NULL);
		V_2 = ((float)il2cpp_codegen_multiply((float)L_36, (float)(0.0174532924f)));
		// var dir = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));
		float L_37 = V_2;
		float L_38;
		L_38 = cosf(L_37);
		float L_39 = V_2;
		float L_40;
		L_40 = sinf(L_39);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_3), L_38, L_40, /*hidden argument*/NULL);
		// if (!m_IgnoreAspectRatio && Direction.Angle <= m_Direction)
		bool L_41 = __this->get_m_IgnoreAspectRatio_19();
		if (L_41)
		{
			goto IL_018a;
		}
	}
	{
		int32_t L_42 = __this->get_m_Direction_9();
		if ((((int32_t)2) > ((int32_t)L_42)))
		{
			goto IL_018a;
		}
	}
	{
		// dir.x *= rect.height / rect.width;
		float* L_43 = (&V_3)->get_address_of_x_0();
		float* L_44 = L_43;
		float L_45 = *((float*)L_44);
		float L_46;
		L_46 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		float L_47;
		L_47 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_0), /*hidden argument*/NULL);
		*((float*)L_44) = (float)((float)il2cpp_codegen_multiply((float)L_45, (float)((float)((float)L_46/(float)L_47))));
		// dir = dir.normalized;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_48;
		L_48 = Vector2_get_normalized_m1F7F7AA3B7AC2414F245395C3785880B847BF7F5((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_3), /*hidden argument*/NULL);
		V_3 = L_48;
	}

IL_018a:
	{
		// var localMatrix = new Matrix2x3(rect, dir.x, dir.y); // Get local matrix.
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_49 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_50 = V_3;
		float L_51 = L_50.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_52 = V_3;
		float L_53 = L_52.get_y_1();
		Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE((Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED *)(&V_4), L_49, L_51, L_53, /*hidden argument*/NULL);
		// for (var i = 0; i < vh.currentVertCount; i++)
		V_8 = 0;
		goto IL_02b4;
	}

IL_01a6:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_54 = ___vh0;
		int32_t L_55 = V_8;
		NullCheck(L_54);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_54, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_1), L_55, /*hidden argument*/NULL);
		// if (m_GradientStyle == GradientStyle.Split)
		int32_t L_56 = __this->get_m_GradientStyle_17();
		if ((!(((uint32_t)L_56) == ((uint32_t)2))))
		{
			goto IL_01dd;
		}
	}
	{
		// normalizedPos = localMatrix * s_SplitedCharacterPosition[i % 4] + offset2;
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_57 = V_4;
		IL2CPP_RUNTIME_CLASS_INIT(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_il2cpp_TypeInfo_var);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_58 = ((UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_StaticFields*)il2cpp_codegen_static_fields_for(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_il2cpp_TypeInfo_var))->get_s_SplitedCharacterPosition_8();
		int32_t L_59 = V_8;
		NullCheck(L_58);
		int32_t L_60 = ((int32_t)((int32_t)L_59%(int32_t)4));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_61 = (L_58)->GetAt(static_cast<il2cpp_array_size_t>(L_60));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_62;
		L_62 = Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0(L_57, L_61, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_63;
		L_63 = UIGradient_get_offset2_m5B5F98FAADFFC9ACD34BC208000DCC0D0318507C(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_64;
		L_64 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_62, L_63, /*hidden argument*/NULL);
		V_9 = L_64;
		// }
		goto IL_01fc;
	}

IL_01dd:
	{
		// normalizedPos = localMatrix * vertex.position + offset2;
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_65 = V_4;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_66 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_67 = L_66.get_position_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_68;
		L_68 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_67, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_69;
		L_69 = Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0(L_65, L_68, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_70;
		L_70 = UIGradient_get_offset2_m5B5F98FAADFFC9ACD34BC208000DCC0D0318507C(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_71;
		L_71 = Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline(L_69, L_70, /*hidden argument*/NULL);
		V_9 = L_71;
	}

IL_01fc:
	{
		// if (direction == Direction.Diagonal)
		int32_t L_72;
		L_72 = UIGradient_get_direction_m094F3EB30335D4C941FFE1C9CD92A1EA46FFD209_inline(__this, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_72) == ((uint32_t)3))))
		{
			goto IL_0245;
		}
	}
	{
		// color = Color.LerpUnclamped(
		//     Color.LerpUnclamped(m_Color1, m_Color2, normalizedPos.x),
		//     Color.LerpUnclamped(m_Color3, m_Color4, normalizedPos.x),
		//     normalizedPos.y);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_73 = __this->get_m_Color1_10();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_74 = __this->get_m_Color2_11();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_75 = V_9;
		float L_76 = L_75.get_x_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_77;
		L_77 = Color_LerpUnclamped_m04B5A0416E75DC33705FCD18B0B453A2ACD50793(L_73, L_74, L_76, /*hidden argument*/NULL);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_78 = __this->get_m_Color3_12();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_79 = __this->get_m_Color4_13();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_80 = V_9;
		float L_81 = L_80.get_x_0();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_82;
		L_82 = Color_LerpUnclamped_m04B5A0416E75DC33705FCD18B0B453A2ACD50793(L_78, L_79, L_81, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_83 = V_9;
		float L_84 = L_83.get_y_1();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_85;
		L_85 = Color_LerpUnclamped_m04B5A0416E75DC33705FCD18B0B453A2ACD50793(L_77, L_82, L_84, /*hidden argument*/NULL);
		V_10 = L_85;
		// }
		goto IL_025f;
	}

IL_0245:
	{
		// color = Color.LerpUnclamped(m_Color2, m_Color1, normalizedPos.y);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_86 = __this->get_m_Color2_11();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_87 = __this->get_m_Color1_10();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_88 = V_9;
		float L_89 = L_88.get_y_1();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_90;
		L_90 = Color_LerpUnclamped_m04B5A0416E75DC33705FCD18B0B453A2ACD50793(L_86, L_87, L_89, /*hidden argument*/NULL);
		V_10 = L_90;
	}

IL_025f:
	{
		// vertex.color *= (m_ColorSpace == ColorSpace.Gamma) ? color.gamma
		//     : (m_ColorSpace == ColorSpace.Linear) ? color.linear
		//     : color;
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * L_91 = (&V_1)->get_address_of_color_3();
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D * L_92 = L_91;
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_93 = (*(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D *)L_92);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_94;
		L_94 = Color32_op_Implicit_m63F14F1A14B1A9A3EE4D154413EE229D3E001623(L_93, /*hidden argument*/NULL);
		int32_t L_95 = __this->get_m_ColorSpace_18();
		G_B20_0 = L_94;
		G_B20_1 = L_92;
		if (!L_95)
		{
			G_B23_0 = L_94;
			G_B23_1 = L_92;
			goto IL_028f;
		}
	}
	{
		int32_t L_96 = __this->get_m_ColorSpace_18();
		G_B21_0 = G_B20_0;
		G_B21_1 = G_B20_1;
		if ((((int32_t)L_96) == ((int32_t)1)))
		{
			G_B22_0 = G_B20_0;
			G_B22_1 = G_B20_1;
			goto IL_0286;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_97 = V_10;
		G_B24_0 = L_97;
		G_B24_1 = G_B21_0;
		G_B24_2 = G_B21_1;
		goto IL_0296;
	}

IL_0286:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_98;
		L_98 = Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904((Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_10), /*hidden argument*/NULL);
		G_B24_0 = L_98;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		goto IL_0296;
	}

IL_028f:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_99;
		L_99 = Color_get_gamma_mB6C6DA08F57C698AAB65B93F16B58F7C3F8F7E16((Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 *)(&V_10), /*hidden argument*/NULL);
		G_B24_0 = L_99;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
	}

IL_0296:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_100;
		L_100 = Color_op_Multiply_mFD03CB228034C2D37F326B7AFF27C861E95447B7(G_B24_1, G_B24_0, /*hidden argument*/NULL);
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_101;
		L_101 = Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4(L_100, /*hidden argument*/NULL);
		*(Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D *)G_B24_2 = L_101;
		// vh.SetUIVertex(vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_102 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_103 = V_1;
		int32_t L_104 = V_8;
		NullCheck(L_102);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_102, L_103, L_104, /*hidden argument*/NULL);
		// for (var i = 0; i < vh.currentVertCount; i++)
		int32_t L_105 = V_8;
		V_8 = ((int32_t)il2cpp_codegen_add((int32_t)L_105, (int32_t)1));
	}

IL_02b4:
	{
		// for (var i = 0; i < vh.currentVertCount; i++)
		int32_t L_106 = V_8;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_107 = ___vh0;
		NullCheck(L_107);
		int32_t L_108;
		L_108 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_107, /*hidden argument*/NULL);
		if ((((int32_t)L_106) < ((int32_t)L_108)))
		{
			goto IL_01a6;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIGradient::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient__ctor_m9400C38CE25C5FA85BE64AA54384FC7F16F8FE6F (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// Color m_Color1 = Color.white;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		__this->set_m_Color1_10(L_0);
		// Color m_Color2 = Color.white;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1;
		L_1 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		__this->set_m_Color2_11(L_1);
		// Color m_Color3 = Color.white;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_2;
		L_2 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		__this->set_m_Color3_12(L_2);
		// Color m_Color4 = Color.white;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3;
		L_3 = Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E(/*hidden argument*/NULL);
		__this->set_m_Color4_13(L_3);
		// ColorSpace m_ColorSpace = ColorSpace.Uninitialized;
		__this->set_m_ColorSpace_18((-1));
		// bool m_IgnoreAspectRatio = true;
		__this->set_m_IgnoreAspectRatio_19((bool)1);
		BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UIGradient::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIGradient__cctor_mEAC8743086EA5920BBF96651F4F48D4BA01FB8B2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static readonly Vector2[] s_SplitedCharacterPosition = {Vector2.up, Vector2.one, Vector2.right, Vector2.zero};
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_0 = (Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA*)SZArrayNew(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA_il2cpp_TypeInfo_var, (uint32_t)4);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_1 = L_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		L_2 = Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C(/*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_2);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_3 = L_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		L_4 = Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42(/*hidden argument*/NULL);
		NullCheck(L_3);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_4);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_5 = L_3;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6;
		L_6 = Vector2_get_right_m42ED15112D219375D2B6879E62ED925D002F15AF(/*hidden argument*/NULL);
		NullCheck(L_5);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_6);
		Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* L_7 = L_5;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		L_8 = Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828(/*hidden argument*/NULL);
		NullCheck(L_7);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(3), (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 )L_8);
		((UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_StaticFields*)il2cpp_codegen_static_fields_for(UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813_il2cpp_TypeInfo_var))->set_s_SplitedCharacterPosition_8(L_7);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Color Coffee.UIEffects.UIHsvModifier::get_targetColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIHsvModifier_get_targetColor_mB8E7080EDD10DA0D149EF2017910EC4F13119D2E (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_TargetColor; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_TargetColor_15();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::set_targetColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_set_targetColor_m57B784581FD98B38E5EA7CD8CA7232B58D2D6CB0 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_TargetColor == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_TargetColor_15();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_TargetColor == value) return;
		return;
	}

IL_000f:
	{
		// m_TargetColor = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_TargetColor_15(L_3);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIHsvModifier::get_range()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIHsvModifier_get_range_m7C1D0EEBFBD69B2B32DD4C12943B56C36CD3DC5C (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Range; }
		float L_0 = __this->get_m_Range_16();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::set_range(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_set_range_m622FDB03F2E37F5D3832E501B66D9F4959CBF8E6 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Range, value)) return;
		float L_2 = __this->get_m_Range_16();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Range, value)) return;
		return;
	}

IL_0021:
	{
		// m_Range = value;
		float L_5 = ___value0;
		__this->set_m_Range_16(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIHsvModifier::get_saturation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIHsvModifier_get_saturation_mE5AD1F97E367095DBD1EA452DD24E46C2F037E17 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Saturation; }
		float L_0 = __this->get_m_Saturation_18();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::set_saturation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_set_saturation_m01D7FA3519D65959C9D084A452395571EAB02077 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, -0.5f, 0.5f);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (-0.5f), (0.5f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Saturation, value)) return;
		float L_2 = __this->get_m_Saturation_18();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Saturation, value)) return;
		return;
	}

IL_0021:
	{
		// m_Saturation = value;
		float L_5 = ___value0;
		__this->set_m_Saturation_18(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIHsvModifier::get_value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIHsvModifier_get_value_mE2043521CB9AA0AA3A812D9468DECDB25D2E801F (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Value; }
		float L_0 = __this->get_m_Value_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::set_value(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_set_value_m789131F4CD1C18A59D44F32AE3E29CC5BA34D732 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, -0.5f, 0.5f);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (-0.5f), (0.5f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Value, value)) return;
		float L_2 = __this->get_m_Value_19();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Value, value)) return;
		return;
	}

IL_0021:
	{
		// m_Value = value;
		float L_5 = ___value0;
		__this->set_m_Value_19(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIHsvModifier::get_hue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIHsvModifier_get_hue_m17FACAC80A9C5EF5AF4271A9274366B37DDA8174 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Hue; }
		float L_0 = __this->get_m_Hue_17();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::set_hue(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_set_hue_m7404ED8A0A744D41C1DC729258A4BF3D8E9D9D92 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, -0.5f, 0.5f);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (-0.5f), (0.5f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Hue, value)) return;
		float L_2 = __this->get_m_Hue_17();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Hue, value)) return;
		return;
	}

IL_0021:
	{
		// m_Hue = value;
		float L_5 = ___value0;
		__this->set_m_Hue_17(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIHsvModifier::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIHsvModifier_get_paramTex_m6B4568923620E4AE1805B1C133E1332D38D33B85 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return s_ParamTex; }
		IL2CPP_RUNTIME_CLASS_INIT(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_il2cpp_TypeInfo_var);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ((UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_StaticFields*)il2cpp_codegen_static_fields_for(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_il2cpp_TypeInfo_var))->get_s_ParamTex_14();
		return L_0;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.UIHsvModifier::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  UIHsvModifier_GetMaterialHash_m77225F452DCD129FA334029C9DC064B6935F4715 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!isActiveAndEnabled || !material || !material.shader)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___material0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}

IL_001d:
	{
		// return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_6 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_6;
	}

IL_0023:
	{
		// return new Hash128(
		//     (uint) material.GetInstanceID(),
		//     k_ShaderId,
		//     0,
		//     0
		// );
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = ___material0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_7, /*hidden argument*/NULL);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Hash128__ctor_m4F9101AF4B79AB2FBC395F48875C21A4E2101581((&L_9), L_8, ((int32_t)48), 0, 0, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_ModifyMaterial_m1944A64071017AB765A700C75EC6787B4CA43887 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAE03A8A39415A629D62FC97776BF380BC1C7564);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var connector = GraphicConnector.FindConnector(graphic);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic1;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1;
		L_1 = GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0(L_0, /*hidden argument*/NULL);
		// newMaterial.shader = Shader.Find(string.Format("Hidden/{0} (UIHsvModifier)", newMaterial.shader.name));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___newMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___newMaterial0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralCAE03A8A39415A629D62FC97776BF380BC1C7564, L_5, /*hidden argument*/NULL);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_7;
		L_7 = Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D(L_2, L_7, /*hidden argument*/NULL);
		// paramTex.RegisterMaterial(newMaterial);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_8;
		L_8 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_9 = ___newMaterial0;
		NullCheck(L_8);
		ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9(L_8, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_ModifyMesh_m224465843842D791A0E3A965E20C3F630DBE7B94 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var normalizedIndex = paramTex.GetNormalizedIndex(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		float L_2;
		L_2 = ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5(L_1, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		// var vertex = default(UIVertex);
		il2cpp_codegen_initobj((&V_1), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// var count = vh.currentVertCount;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_3 = ___vh0;
		NullCheck(L_3);
		int32_t L_4;
		L_4 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_3, /*hidden argument*/NULL);
		V_2 = L_4;
		// for (var i = 0; i < count; i++)
		V_3 = 0;
		goto IL_006b;
	}

IL_0029:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_5 = ___vh0;
		int32_t L_6 = V_3;
		NullCheck(L_5);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_5, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_1), L_6, /*hidden argument*/NULL);
		// vertex.uv0 = new Vector2(
		//     Packer.ToFloat(vertex.uv0.x, vertex.uv0.y),
		//     normalizedIndex
		// );
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_7 = V_1;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_8 = L_7.get_uv0_4();
		float L_9 = L_8.get_x_1();
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_10 = V_1;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_11 = L_10.get_uv0_4();
		float L_12 = L_11.get_y_2();
		float L_13;
		L_13 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_9, L_12, /*hidden argument*/NULL);
		float L_14 = V_0;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_15;
		memset((&L_15), 0, sizeof(L_15));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_15), L_13, L_14, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_16;
		L_16 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_15, /*hidden argument*/NULL);
		(&V_1)->set_uv0_4(L_16);
		// vh.SetUIVertex(vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_17 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_18 = V_1;
		int32_t L_19 = V_3;
		NullCheck(L_17);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_17, L_18, L_19, /*hidden argument*/NULL);
		// for (var i = 0; i < count; i++)
		int32_t L_20 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_006b:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_21 = V_3;
		int32_t L_22 = V_2;
		if ((((int32_t)L_21) < ((int32_t)L_22)))
		{
			goto IL_0029;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::SetEffectParamsDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier_SetEffectParamsDirty_m786AD408DE613467C527EAD7AA3DB1F3868B0409 (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		// Color.RGBToHSV(m_TargetColor, out h, out s, out v);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_TargetColor_15();
		Color_RGBToHSV_mDC3A14DCF9D4A898AF97613CD07D94BFF8402194(L_0, (float*)(&V_0), (float*)(&V_1), (float*)(&V_2), /*hidden argument*/NULL);
		// paramTex.SetData(this, 0, h); // param1.x : target hue
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_2 = V_0;
		NullCheck(L_1);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_1, __this, 0, L_2, /*hidden argument*/NULL);
		// paramTex.SetData(this, 1, s); // param1.y : target saturation
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_3;
		L_3 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_4 = V_1;
		NullCheck(L_3);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_3, __this, 1, L_4, /*hidden argument*/NULL);
		// paramTex.SetData(this, 2, v); // param1.z : target value
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_5;
		L_5 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_6 = V_2;
		NullCheck(L_5);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_5, __this, 2, L_6, /*hidden argument*/NULL);
		// paramTex.SetData(this, 3, m_Range); // param1.w : target range
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_7;
		L_7 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_8 = __this->get_m_Range_16();
		NullCheck(L_7);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_7, __this, 3, L_8, /*hidden argument*/NULL);
		// paramTex.SetData(this, 4, m_Hue + 0.5f); // param2.x : hue shift
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_9;
		L_9 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_10 = __this->get_m_Hue_17();
		NullCheck(L_9);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_9, __this, 4, ((float)il2cpp_codegen_add((float)L_10, (float)(0.5f))), /*hidden argument*/NULL);
		// paramTex.SetData(this, 5, m_Saturation + 0.5f); // param2.y : saturation shift
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_11;
		L_11 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_12 = __this->get_m_Saturation_18();
		NullCheck(L_11);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_11, __this, 5, ((float)il2cpp_codegen_add((float)L_12, (float)(0.5f))), /*hidden argument*/NULL);
		// paramTex.SetData(this, 6, m_Value + 0.5f); // param2.z : value shift
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_13;
		L_13 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_14 = __this->get_m_Value_19();
		NullCheck(L_13);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_13, __this, 6, ((float)il2cpp_codegen_add((float)L_14, (float)(0.5f))), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier__ctor_m96CF14648BB39439DEA95EC2DFEEE7946B9A822A (UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// Color m_TargetColor = Color.red;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		L_0 = Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8(/*hidden argument*/NULL);
		__this->set_m_TargetColor_15(L_0);
		// float m_Range = 0.1f;
		__this->set_m_Range_16((0.100000001f));
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UIHsvModifier::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIHsvModifier__cctor_mBFF73E20AFB72787C7E5240BAEEAF7B0366CE491 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly ParameterTexture s_ParamTex = new ParameterTexture(7, 128, "_ParamTex");
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)il2cpp_codegen_object_new(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876(L_0, 7, ((int32_t)128), _stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587, /*hidden argument*/NULL);
		((UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_StaticFields*)il2cpp_codegen_static_fields_for(UIHsvModifier_tD16CCDDA71B93AF72456871CB5E91E504D8481E2_il2cpp_TypeInfo_var))->set_s_ParamTex_14(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.Color Coffee.UIEffects.UIShadow::get_effectColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectColor; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_EffectColor_14();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_effectColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_effectColor_m6D7B016793A2C176AA66F257BD3C53BFD71297EC (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_EffectColor == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_EffectColor_14();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_EffectColor == value) return;
		return;
	}

IL_000f:
	{
		// m_EffectColor = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_EffectColor_14(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Vector2 Coffee.UIEffects.UIShadow::get_effectDistance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  UIShadow_get_effectDistance_m2AB1626DC40D34C0D91FF336342F938195B201EE (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectDistance; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_m_EffectDistance_15();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_effectDistance(UnityEngine.Vector2)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_effectDistance_m267278942B1CE1058C014208F2D79948626A99BC (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___value0, const RuntimeMethod* method)
{
	{
		// if (value.x > kMaxEffectDistance)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___value0;
		float L_1 = L_0.get_x_0();
		if ((!(((float)L_1) > ((float)(600.0f)))))
		{
			goto IL_0019;
		}
	}
	{
		// value.x = kMaxEffectDistance;
		(&___value0)->set_x_0((600.0f));
	}

IL_0019:
	{
		// if (value.x < -kMaxEffectDistance)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___value0;
		float L_3 = L_2.get_x_0();
		if ((!(((float)L_3) < ((float)(-600.0f)))))
		{
			goto IL_0032;
		}
	}
	{
		// value.x = -kMaxEffectDistance;
		(&___value0)->set_x_0((-600.0f));
	}

IL_0032:
	{
		// if (value.y > kMaxEffectDistance)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___value0;
		float L_5 = L_4.get_y_1();
		if ((!(((float)L_5) > ((float)(600.0f)))))
		{
			goto IL_004b;
		}
	}
	{
		// value.y = kMaxEffectDistance;
		(&___value0)->set_y_1((600.0f));
	}

IL_004b:
	{
		// if (value.y < -kMaxEffectDistance)
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___value0;
		float L_7 = L_6.get_y_1();
		if ((!(((float)L_7) < ((float)(-600.0f)))))
		{
			goto IL_0064;
		}
	}
	{
		// value.y = -kMaxEffectDistance;
		(&___value0)->set_y_1((-600.0f));
	}

IL_0064:
	{
		// if (m_EffectDistance == value) return;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8 = __this->get_m_EffectDistance_15();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = ___value0;
		bool L_10;
		L_10 = Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0073;
		}
	}
	{
		// if (m_EffectDistance == value) return;
		return;
	}

IL_0073:
	{
		// m_EffectDistance = value;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_11 = ___value0;
		__this->set_m_EffectDistance_15(L_11);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.UIShadow::get_useGraphicAlpha()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UIShadow_get_useGraphicAlpha_mEC09F1BCF8D368129B771A94C129F048C7C0CE1E (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_UseGraphicAlpha; }
		bool L_0 = __this->get_m_UseGraphicAlpha_16();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_useGraphicAlpha(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_useGraphicAlpha_m2A048380CA0E931D640C1B26A68B7C8E57798F7D (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_UseGraphicAlpha == value) return;
		bool L_0 = __this->get_m_UseGraphicAlpha_16();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_UseGraphicAlpha == value) return;
		return;
	}

IL_000a:
	{
		// m_UseGraphicAlpha = value;
		bool L_2 = ___value0;
		__this->set_m_UseGraphicAlpha_16(L_2);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIShadow::get_blurFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShadow_get_blurFactor_m5A7E95CE6D4AC039A6C84A95C9293FA23B7002E8 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_BlurFactor; }
		float L_0 = __this->get_m_BlurFactor_12();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_blurFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_blurFactor_mDCFC5282751C9A98A4D2163964AB5A688BE1D079 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 2);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (2.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_BlurFactor, value)) return;
		float L_2 = __this->get_m_BlurFactor_12();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_BlurFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_BlurFactor = value;
		float L_5 = ___value0;
		__this->set_m_BlurFactor_12(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.ShadowStyle Coffee.UIEffects.UIShadow::get_style()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIShadow_get_style_mCAA0D48FB84802EBB12777BD82587CF6B1A20379 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Style; }
		int32_t L_0 = __this->get_m_Style_13();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_style(Coffee.UIEffects.ShadowStyle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_style_m57011D0B6E5E8F3E16B1E6B24272D2348068C555 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_Style == value) return;
		int32_t L_0 = __this->get_m_Style_13();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_Style == value) return;
		return;
	}

IL_000a:
	{
		// m_Style = value;
		int32_t L_2 = ___value0;
		__this->set_m_Style_13(L_2);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Int32 Coffee.UIEffects.UIShadow::get_parameterIndex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIShadow_get_parameterIndex_m421844BF83F2BD63CB36E171DCA3A4173B1E0D94 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// public int parameterIndex { get; set; }
		int32_t L_0 = __this->get_U3CparameterIndexU3Ek__BackingField_18();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_parameterIndex(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_parameterIndex_mC140571AED150F3BB3FFCF5029F8D9878E54EE4A (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// public int parameterIndex { get; set; }
		int32_t L_0 = ___value0;
		__this->set_U3CparameterIndexU3Ek__BackingField_18(L_0);
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShadow::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// public ParameterTexture paramTex { get; private set; }
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = __this->get_U3CparamTexU3Ek__BackingField_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShadow::set_paramTex(Coffee.UIEffects.ParameterTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___value0, const RuntimeMethod* method)
{
	{
		// public ParameterTexture paramTex { get; private set; }
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ___value0;
		__this->set_U3CparamTexU3Ek__BackingField_19(L_0);
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_OnEnable_m60DEFD2F94DC112AE059AF3E9D47FB3A65D3125C (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnEnable();
		BaseMeshEffect_OnEnable_m900129F36764E412C7FCE57A7C00DB091F48F4C6(__this, /*hidden argument*/NULL);
		// _uiEffect = GetComponent<UIEffect>();
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_0;
		L_0 = Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13(__this, /*hidden argument*/Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13_RuntimeMethod_var);
		__this->set__uiEffect_11(L_0);
		// if (!_uiEffect) return;
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_1 = __this->get__uiEffect_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		// if (!_uiEffect) return;
		return;
	}

IL_0020:
	{
		// paramTex = _uiEffect.paramTex;
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_3 = __this->get__uiEffect_11();
		NullCheck(L_3);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_4;
		L_4 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, L_3);
		UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010_inline(__this, L_4, /*hidden argument*/NULL);
		// paramTex.Register(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_5;
		L_5 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		ParameterTexture_Register_m418624C23515BB36BFE2E30FDC66AB2DD855D62A(L_5, __this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_OnDisable_m86DD53B994C6902D25002910BFA026E3057E53BB (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// base.OnDisable();
		BaseMeshEffect_OnDisable_m714D32A1FD3155B874FD836C76954208747FED85(__this, /*hidden argument*/NULL);
		// _uiEffect = null;
		__this->set__uiEffect_11((UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 *)NULL);
		// if (paramTex == null) return;
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		// if (paramTex == null) return;
		return;
	}

IL_0016:
	{
		// paramTex.Unregister(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		ParameterTexture_Unregister_mC8283ADEEC7D0B15321727EC445C0909671918BC(L_1, __this, /*hidden argument*/NULL);
		// paramTex = null;
		UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010_inline(__this, (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)NULL, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_ModifyMesh_m99583022DC19BEAAD324A9F7E22FF313129EB38A (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponents_TisUIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_mE1D283405E85DB09AF57FA34A2F39D40B40FBC41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Clear_m75F5B96DFF821D80AF54DCCBF40A4C66163B3EE7_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361  V_0;
	memset((&V_0), 0, sizeof(V_0));
	UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * V_1 = NULL;
	Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361  V_2;
	memset((&V_2), 0, sizeof(V_2));
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 2> __leave_targets;
	UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * G_B19_0 = NULL;
	UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * G_B18_0 = NULL;
	UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * G_B20_0 = NULL;
	UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * G_B20_1 = NULL;
	{
		// if (!isActiveAndEnabled || vh.currentVertCount <= 0 || m_Style == ShadowStyle.None)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0019;
		}
	}
	{
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_1 = ___vh0;
		NullCheck(L_1);
		int32_t L_2;
		L_2 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_3 = __this->get_m_Style_13();
		if (L_3)
		{
			goto IL_001a;
		}
	}

IL_0019:
	{
		// return;
		return;
	}

IL_001a:
	{
		// vh.GetUIVertexStream(s_Verts);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_4 = ___vh0;
		IL2CPP_RUNTIME_CLASS_INIT(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_5 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
		NullCheck(L_4);
		VertexHelper_GetUIVertexStream_mA3E62A7B45BFFFC73D72BC7B8BFAD5388F8578BA(L_4, L_5, /*hidden argument*/NULL);
		// GetComponents<UIShadow>(tmpShadows);
		List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * L_6 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_tmpShadows_8();
		Component_GetComponents_TisUIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_mE1D283405E85DB09AF57FA34A2F39D40B40FBC41(__this, L_6, /*hidden argument*/Component_GetComponents_TisUIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_mE1D283405E85DB09AF57FA34A2F39D40B40FBC41_RuntimeMethod_var);
		// foreach (var s in tmpShadows)
		List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * L_7 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_tmpShadows_8();
		NullCheck(L_7);
		Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361  L_8;
		L_8 = List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462(L_7, /*hidden argument*/List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462_RuntimeMethod_var);
		V_0 = L_8;
	}

IL_003b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0092;
		}

IL_003d:
		{
			// foreach (var s in tmpShadows)
			UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * L_9;
			L_9 = Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_inline((Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *)(&V_0), /*hidden argument*/Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_RuntimeMethod_var);
			V_1 = L_9;
			// if (!s.isActiveAndEnabled) continue;
			UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * L_10 = V_1;
			NullCheck(L_10);
			bool L_11;
			L_11 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(L_10, /*hidden argument*/NULL);
			if (!L_11)
			{
				goto IL_0092;
			}
		}

IL_004d:
		{
			// if (s == this)
			UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * L_12 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			bool L_13;
			L_13 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_12, __this, /*hidden argument*/NULL);
			if (!L_13)
			{
				goto IL_009b;
			}
		}

IL_0056:
		{
			// foreach (var s2 in tmpShadows)
			IL2CPP_RUNTIME_CLASS_INIT(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
			List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * L_14 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_tmpShadows_8();
			NullCheck(L_14);
			Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361  L_15;
			L_15 = List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462(L_14, /*hidden argument*/List_1_GetEnumerator_mDDDA3E2A916342A871D4F884C6D1211D6E329462_RuntimeMethod_var);
			V_2 = L_15;
		}

IL_0061:
		try
		{ // begin try (depth: 2)
			{
				goto IL_0079;
			}

IL_0063:
			{
				// foreach (var s2 in tmpShadows)
				UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * L_16;
				L_16 = Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_inline((Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *)(&V_2), /*hidden argument*/Enumerator_get_Current_m60D73455A4C4D155484DC40876A629860AD843A2_RuntimeMethod_var);
				// s2._graphicVertexCount = s_Verts.Count;
				IL2CPP_RUNTIME_CLASS_INIT(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
				List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_17 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
				NullCheck(L_17);
				int32_t L_18;
				L_18 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_17, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
				NullCheck(L_16);
				L_16->set__graphicVertexCount_10(L_18);
			}

IL_0079:
			{
				// foreach (var s2 in tmpShadows)
				bool L_19;
				L_19 = Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932((Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *)(&V_2), /*hidden argument*/Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932_RuntimeMethod_var);
				if (L_19)
				{
					goto IL_0063;
				}
			}

IL_0082:
			{
				IL2CPP_LEAVE(0xAB, FINALLY_0084);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_0084;
		}

FINALLY_0084:
		{ // begin finally (depth: 2)
			Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75((Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *)(&V_2), /*hidden argument*/Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75_RuntimeMethod_var);
			IL2CPP_END_FINALLY(132)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(132)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0xAB, FINALLY_009d);
		}

IL_0092:
		{
			// foreach (var s in tmpShadows)
			bool L_20;
			L_20 = Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932((Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *)(&V_0), /*hidden argument*/Enumerator_MoveNext_m1DB25AF6DA04E0BD19AB54654A2D97256E53A932_RuntimeMethod_var);
			if (L_20)
			{
				goto IL_003d;
			}
		}

IL_009b:
		{
			IL2CPP_LEAVE(0xAB, FINALLY_009d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_009d;
	}

FINALLY_009d:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75((Enumerator_tC568F3E2E169EE348F1CEA809CFE2AC540388361 *)(&V_0), /*hidden argument*/Enumerator_Dispose_m5DB38CD57C955FF4661DC584A146B5ECF80F4F75_RuntimeMethod_var);
		IL2CPP_END_FINALLY(157)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(157)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0xAB, IL_00ab)
	}

IL_00ab:
	{
		// tmpShadows.Clear();
		IL2CPP_RUNTIME_CLASS_INIT(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
		List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * L_21 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_tmpShadows_8();
		NullCheck(L_21);
		List_1_Clear_m75F5B96DFF821D80AF54DCCBF40A4C66163B3EE7(L_21, /*hidden argument*/List_1_Clear_m75F5B96DFF821D80AF54DCCBF40A4C66163B3EE7_RuntimeMethod_var);
		// _uiEffect = _uiEffect ? _uiEffect : GetComponent<UIEffect>();
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_22 = __this->get__uiEffect_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_23;
		L_23 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_22, /*hidden argument*/NULL);
		G_B18_0 = __this;
		if (L_23)
		{
			G_B19_0 = __this;
			goto IL_00cb;
		}
	}
	{
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_24;
		L_24 = Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13(__this, /*hidden argument*/Component_GetComponent_TisUIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21_m184C10DA4FD48B176874DA00EF0AFAE1B58F2A13_RuntimeMethod_var);
		G_B20_0 = L_24;
		G_B20_1 = G_B18_0;
		goto IL_00d1;
	}

IL_00cb:
	{
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_25 = __this->get__uiEffect_11();
		G_B20_0 = L_25;
		G_B20_1 = G_B19_0;
	}

IL_00d1:
	{
		NullCheck(G_B20_1);
		G_B20_1->set__uiEffect_11(G_B20_0);
		// var start = s_Verts.Count - _graphicVertexCount;
		IL2CPP_RUNTIME_CLASS_INIT(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_26 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
		NullCheck(L_26);
		int32_t L_27;
		L_27 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_26, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		int32_t L_28 = __this->get__graphicVertexCount_10();
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)L_28));
		// var end = s_Verts.Count;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_29 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
		NullCheck(L_29);
		int32_t L_30;
		L_30 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_29, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		V_4 = L_30;
		// if (paramTex != null && _uiEffect && _uiEffect.isActiveAndEnabled)
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_31;
		L_31 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0153;
		}
	}
	{
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_32 = __this->get__uiEffect_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_33;
		L_33 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_32, /*hidden argument*/NULL);
		if (!L_33)
		{
			goto IL_0153;
		}
	}
	{
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_34 = __this->get__uiEffect_11();
		NullCheck(L_34);
		bool L_35;
		L_35 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(L_34, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_0153;
		}
	}
	{
		// paramTex.SetData(this, 0, _uiEffect.effectFactor); // param.x : effect factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_36;
		L_36 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_37 = __this->get__uiEffect_11();
		NullCheck(L_37);
		float L_38;
		L_38 = UIEffect_get_effectFactor_m00273C0BB3B80F99858764F8D80FD8D1BD7CB92E_inline(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_36, __this, 0, L_38, /*hidden argument*/NULL);
		// paramTex.SetData(this, 1, 255); // param.y : color factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_39;
		L_39 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		ParameterTexture_SetData_m3870BC4215A7F85A68A38D2E6B50DB27D65284BB(L_39, __this, 1, (uint8_t)((int32_t)255), /*hidden argument*/NULL);
		// paramTex.SetData(this, 2, m_BlurFactor); // param.z : blur factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_40;
		L_40 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		float L_41 = __this->get_m_BlurFactor_12();
		NullCheck(L_40);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_40, __this, 2, L_41, /*hidden argument*/NULL);
	}

IL_0153:
	{
		// ApplyShadow(s_Verts, effectColor, ref start, ref end, effectDistance, style, useGraphicAlpha);
		IL2CPP_RUNTIME_CLASS_INIT(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_42 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_43;
		L_43 = UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38_inline(__this, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_44;
		L_44 = UIShadow_get_effectDistance_m2AB1626DC40D34C0D91FF336342F938195B201EE_inline(__this, /*hidden argument*/NULL);
		int32_t L_45;
		L_45 = UIShadow_get_style_mCAA0D48FB84802EBB12777BD82587CF6B1A20379_inline(__this, /*hidden argument*/NULL);
		bool L_46;
		L_46 = UIShadow_get_useGraphicAlpha_mEC09F1BCF8D368129B771A94C129F048C7C0CE1E_inline(__this, /*hidden argument*/NULL);
		UIShadow_ApplyShadow_m5422EC048717AA600AD8A06FCD534E94350034E1(__this, L_42, L_43, (int32_t*)(&V_3), (int32_t*)(&V_4), L_44, L_45, L_46, /*hidden argument*/NULL);
		// vh.Clear();
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_47 = ___vh0;
		NullCheck(L_47);
		VertexHelper_Clear_mBF3FB3CEA5153F8F72C74FFD6006A7AFF62C18BA(L_47, /*hidden argument*/NULL);
		// vh.AddUIVertexTriangleStream(s_Verts);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_48 = ___vh0;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_49 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
		NullCheck(L_48);
		VertexHelper_AddUIVertexTriangleStream_m3FC7DF3D1DA3F0D40025258E3B8FF5830EE7CE55(L_48, L_49, /*hidden argument*/NULL);
		// s_Verts.Clear();
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_50 = ((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->get_s_Verts_9();
		NullCheck(L_50);
		List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB(L_50, /*hidden argument*/List_1_Clear_m4375DCA89C2759B409ABA84E601C40F72027BCBB_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color,System.Int32&,System.Int32&,UnityEngine.Vector2,Coffee.UIEffects.ShadowStyle,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_ApplyShadow_m5422EC048717AA600AD8A06FCD534E94350034E1 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color1, int32_t* ___start2, int32_t* ___end3, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___distance4, int32_t ___style5, bool ___alpha6, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// if (style == ShadowStyle.None || color.a <= 0)
		int32_t L_0 = ___style5;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___color1;
		float L_2 = L_1.get_a_3();
		if ((!(((float)L_2) <= ((float)(0.0f)))))
		{
			goto IL_0012;
		}
	}

IL_0011:
	{
		// return;
		return;
	}

IL_0012:
	{
		// var x = distance.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_3 = ___distance4;
		float L_4 = L_3.get_x_0();
		V_0 = L_4;
		// var y = distance.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = ___distance4;
		float L_6 = L_5.get_y_1();
		V_1 = L_6;
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, x, y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_7 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_8 = ___color1;
		int32_t* L_9 = ___start2;
		int32_t* L_10 = ___end3;
		float L_11 = V_0;
		float L_12 = V_1;
		bool L_13 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_7, L_8, (int32_t*)L_9, (int32_t*)L_10, L_11, L_12, L_13, /*hidden argument*/NULL);
		int32_t L_14 = ___style5;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_14, (int32_t)2)))
		{
			case 0:
			{
				goto IL_006e;
			}
			case 1:
			{
				goto IL_00a0;
			}
			case 2:
			{
				goto IL_0047;
			}
		}
	}
	{
		return;
	}

IL_0047:
	{
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, x, 0, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_15 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_16 = ___color1;
		int32_t* L_17 = ___start2;
		int32_t* L_18 = ___end3;
		float L_19 = V_0;
		bool L_20 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_15, L_16, (int32_t*)L_17, (int32_t*)L_18, L_19, (0.0f), L_20, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, 0, y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_21 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_22 = ___color1;
		int32_t* L_23 = ___start2;
		int32_t* L_24 = ___end3;
		float L_25 = V_1;
		bool L_26 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_21, L_22, (int32_t*)L_23, (int32_t*)L_24, (0.0f), L_25, L_26, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_006e:
	{
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, x, -y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_27 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_28 = ___color1;
		int32_t* L_29 = ___start2;
		int32_t* L_30 = ___end3;
		float L_31 = V_0;
		float L_32 = V_1;
		bool L_33 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_27, L_28, (int32_t*)L_29, (int32_t*)L_30, L_31, ((-L_32)), L_33, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, -x, y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_34 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_35 = ___color1;
		int32_t* L_36 = ___start2;
		int32_t* L_37 = ___end3;
		float L_38 = V_0;
		float L_39 = V_1;
		bool L_40 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_34, L_35, (int32_t*)L_36, (int32_t*)L_37, ((-L_38)), L_39, L_40, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, -x, -y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_41 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_42 = ___color1;
		int32_t* L_43 = ___start2;
		int32_t* L_44 = ___end3;
		float L_45 = V_0;
		float L_46 = V_1;
		bool L_47 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_41, L_42, (int32_t*)L_43, (int32_t*)L_44, ((-L_45)), ((-L_46)), L_47, /*hidden argument*/NULL);
		// break;
		return;
	}

IL_00a0:
	{
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, x, -y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_48 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_49 = ___color1;
		int32_t* L_50 = ___start2;
		int32_t* L_51 = ___end3;
		float L_52 = V_0;
		float L_53 = V_1;
		bool L_54 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_48, L_49, (int32_t*)L_50, (int32_t*)L_51, L_52, ((-L_53)), L_54, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, -x, y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_55 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_56 = ___color1;
		int32_t* L_57 = ___start2;
		int32_t* L_58 = ___end3;
		float L_59 = V_0;
		float L_60 = V_1;
		bool L_61 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_55, L_56, (int32_t*)L_57, (int32_t*)L_58, ((-L_59)), L_60, L_61, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, -x, -y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_62 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_63 = ___color1;
		int32_t* L_64 = ___start2;
		int32_t* L_65 = ___end3;
		float L_66 = V_0;
		float L_67 = V_1;
		bool L_68 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_62, L_63, (int32_t*)L_64, (int32_t*)L_65, ((-L_66)), ((-L_67)), L_68, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, -x, 0, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_69 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_70 = ___color1;
		int32_t* L_71 = ___start2;
		int32_t* L_72 = ___end3;
		float L_73 = V_0;
		bool L_74 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_69, L_70, (int32_t*)L_71, (int32_t*)L_72, ((-L_73)), (0.0f), L_74, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, 0, -y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_75 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_76 = ___color1;
		int32_t* L_77 = ___start2;
		int32_t* L_78 = ___end3;
		float L_79 = V_1;
		bool L_80 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_75, L_76, (int32_t*)L_77, (int32_t*)L_78, (0.0f), ((-L_79)), L_80, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, x, 0, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_81 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_82 = ___color1;
		int32_t* L_83 = ___start2;
		int32_t* L_84 = ___end3;
		float L_85 = V_0;
		bool L_86 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_81, L_82, (int32_t*)L_83, (int32_t*)L_84, L_85, (0.0f), L_86, /*hidden argument*/NULL);
		// ApplyShadowZeroAlloc(verts, color, ref start, ref end, 0, y, alpha);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_87 = ___verts0;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_88 = ___color1;
		int32_t* L_89 = ___start2;
		int32_t* L_90 = ___end3;
		float L_91 = V_1;
		bool L_92 = ___alpha6;
		UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D(__this, L_87, L_88, (int32_t*)L_89, (int32_t*)L_90, (0.0f), L_91, L_92, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::ApplyShadowZeroAlloc(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color,System.Int32&,System.Int32&,System.Single,System.Single,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * ___verts0, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color1, int32_t* ___start2, int32_t* ___end3, float ___x4, float ___y5, bool ___alpha6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	float V_2 = 0.0f;
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_3;
	memset((&V_3), 0, sizeof(V_3));
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_7;
	memset((&V_7), 0, sizeof(V_7));
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  V_8;
	memset((&V_8), 0, sizeof(V_8));
	float G_B7_0 = 0.0f;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * G_B16_0 = NULL;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * G_B15_0 = NULL;
	float G_B17_0 = 0.0f;
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * G_B17_1 = NULL;
	{
		// var count = end - start;
		int32_t* L_0 = ___end3;
		int32_t L_1 = *((int32_t*)L_0);
		int32_t* L_2 = ___start2;
		int32_t L_3 = *((int32_t*)L_2);
		V_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_3));
		// var neededCapacity = verts.Count + count;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_4 = ___verts0;
		NullCheck(L_4);
		int32_t L_5;
		L_5 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_4, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		int32_t L_6 = V_0;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)L_6));
		// if (verts.Capacity < neededCapacity)
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_7 = ___verts0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903(L_7, /*hidden argument*/List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903_RuntimeMethod_var);
		int32_t L_9 = V_1;
		if ((((int32_t)L_8) >= ((int32_t)L_9)))
		{
			goto IL_0027;
		}
	}
	{
		// verts.Capacity *= 2;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_10 = ___verts0;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_11 = L_10;
		NullCheck(L_11);
		int32_t L_12;
		L_12 = List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903(L_11, /*hidden argument*/List_1_get_Capacity_m8FCF1F96C4DC65526BBFD6A7954970BA5F95E903_RuntimeMethod_var);
		NullCheck(L_11);
		List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4(L_11, ((int32_t)il2cpp_codegen_multiply((int32_t)L_12, (int32_t)2)), /*hidden argument*/List_1_set_Capacity_m5F71210F8094C41745C86339C595A54F721D12A4_RuntimeMethod_var);
	}

IL_0027:
	{
		// var normalizedIndex = paramTex != null && _uiEffect && _uiEffect.isActiveAndEnabled
		//     ? paramTex.GetNormalizedIndex(this)
		//     : -1;
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_13;
		L_13 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0049;
		}
	}
	{
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_14 = __this->get__uiEffect_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_15;
		L_15 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_0049;
		}
	}
	{
		UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * L_16 = __this->get__uiEffect_11();
		NullCheck(L_16);
		bool L_17;
		L_17 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(L_16, /*hidden argument*/NULL);
		if (L_17)
		{
			goto IL_0050;
		}
	}

IL_0049:
	{
		G_B7_0 = (-1.0f);
		goto IL_005c;
	}

IL_0050:
	{
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_18;
		L_18 = UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		float L_19;
		L_19 = ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5(L_18, __this, /*hidden argument*/NULL);
		G_B7_0 = L_19;
	}

IL_005c:
	{
		V_2 = G_B7_0;
		// var vt = default(UIVertex);
		il2cpp_codegen_initobj((&V_3), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// for (var i = 0; i < count; i++)
		V_4 = 0;
		goto IL_0077;
	}

IL_006a:
	{
		// verts.Add(vt);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_20 = ___verts0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_21 = V_3;
		NullCheck(L_20);
		List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588(L_20, L_21, /*hidden argument*/List_1_Add_m021FBEB8D6A133F50D014CA1A2307D2FAADF7588_RuntimeMethod_var);
		// for (var i = 0; i < count; i++)
		int32_t L_22 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1));
	}

IL_0077:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_23 = V_4;
		int32_t L_24 = V_0;
		if ((((int32_t)L_23) < ((int32_t)L_24)))
		{
			goto IL_006a;
		}
	}
	{
		// for (var i = verts.Count - 1; count <= i; i--)
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_25 = ___verts0;
		NullCheck(L_25);
		int32_t L_26;
		L_26 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_25, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		V_5 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_26, (int32_t)1));
		goto IL_00a0;
	}

IL_0088:
	{
		// verts[i] = verts[i - count];
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_27 = ___verts0;
		int32_t L_28 = V_5;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_29 = ___verts0;
		int32_t L_30 = V_5;
		int32_t L_31 = V_0;
		NullCheck(L_29);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_32;
		L_32 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_29, ((int32_t)il2cpp_codegen_subtract((int32_t)L_30, (int32_t)L_31)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		NullCheck(L_27);
		List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3(L_27, L_28, L_32, /*hidden argument*/List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_RuntimeMethod_var);
		// for (var i = verts.Count - 1; count <= i; i--)
		int32_t L_33 = V_5;
		V_5 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_33, (int32_t)1));
	}

IL_00a0:
	{
		// for (var i = verts.Count - 1; count <= i; i--)
		int32_t L_34 = V_0;
		int32_t L_35 = V_5;
		if ((((int32_t)L_34) <= ((int32_t)L_35)))
		{
			goto IL_0088;
		}
	}
	{
		// for (var i = 0; i < count; ++i)
		V_6 = 0;
		goto IL_0160;
	}

IL_00ad:
	{
		// vt = verts[i + start + count];
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_36 = ___verts0;
		int32_t L_37 = V_6;
		int32_t* L_38 = ___start2;
		int32_t L_39 = *((int32_t*)L_38);
		int32_t L_40 = V_0;
		NullCheck(L_36);
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_41;
		L_41 = List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_inline(L_36, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_37, (int32_t)L_39)), (int32_t)L_40)), /*hidden argument*/List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_RuntimeMethod_var);
		V_3 = L_41;
		// var v = vt.position;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_42 = V_3;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43 = L_42.get_position_0();
		V_7 = L_43;
		// vt.position.Set(v.x + x, v.y + y, v.z);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_44 = (&V_3)->get_address_of_position_0();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = V_7;
		float L_46 = L_45.get_x_2();
		float L_47 = ___x4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_48 = V_7;
		float L_49 = L_48.get_y_3();
		float L_50 = ___y5;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_51 = V_7;
		float L_52 = L_51.get_z_4();
		Vector3_Set_m12EA2C6DF9F94ABD0462F422A20959A53EED90D7_inline((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_44, ((float)il2cpp_codegen_add((float)L_46, (float)L_47)), ((float)il2cpp_codegen_add((float)L_49, (float)L_50)), L_52, /*hidden argument*/NULL);
		// var vertColor = effectColor;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_53;
		L_53 = UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38_inline(__this, /*hidden argument*/NULL);
		V_8 = L_53;
		// vertColor.a = alpha ? color.a * vt.color.a / 255 : color.a;
		bool L_54 = ___alpha6;
		G_B15_0 = (&V_8);
		if (L_54)
		{
			G_B16_0 = (&V_8);
			goto IL_0100;
		}
	}
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_55 = ___color1;
		float L_56 = L_55.get_a_3();
		G_B17_0 = L_56;
		G_B17_1 = G_B15_0;
		goto IL_0119;
	}

IL_0100:
	{
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_57 = ___color1;
		float L_58 = L_57.get_a_3();
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_59 = V_3;
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_60 = L_59.get_color_3();
		uint8_t L_61 = L_60.get_a_4();
		G_B17_0 = ((float)((float)((float)il2cpp_codegen_multiply((float)L_58, (float)((float)((float)L_61))))/(float)(255.0f)));
		G_B17_1 = G_B16_0;
	}

IL_0119:
	{
		G_B17_1->set_a_3(G_B17_0);
		// vt.color = vertColor;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_62 = V_8;
		Color32_tDB54A78627878A7D2DE42BB028D64306A18E858D  L_63;
		L_63 = Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4(L_62, /*hidden argument*/NULL);
		(&V_3)->set_color_3(L_63);
		// if (0 <= normalizedIndex)
		float L_64 = V_2;
		if ((!(((float)(0.0f)) <= ((float)L_64))))
		{
			goto IL_0151;
		}
	}
	{
		// vt.uv0 = new Vector2(
		//     vt.uv0.x,
		//     normalizedIndex
		// );
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_65 = V_3;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_66 = L_65.get_uv0_4();
		float L_67 = L_66.get_x_1();
		float L_68 = V_2;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_69;
		memset((&L_69), 0, sizeof(L_69));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_69), L_67, L_68, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_70;
		L_70 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_69, /*hidden argument*/NULL);
		(&V_3)->set_uv0_4(L_70);
	}

IL_0151:
	{
		// verts[i] = vt;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_71 = ___verts0;
		int32_t L_72 = V_6;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_73 = V_3;
		NullCheck(L_71);
		List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3(L_71, L_72, L_73, /*hidden argument*/List_1_set_Item_m158C82C564E20DDC472D2322C72DACFA9984B2C3_RuntimeMethod_var);
		// for (var i = 0; i < count; ++i)
		int32_t L_74 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_74, (int32_t)1));
	}

IL_0160:
	{
		// for (var i = 0; i < count; ++i)
		int32_t L_75 = V_6;
		int32_t L_76 = V_0;
		if ((((int32_t)L_75) < ((int32_t)L_76)))
		{
			goto IL_00ad;
		}
	}
	{
		// start = end;
		int32_t* L_77 = ___start2;
		int32_t* L_78 = ___end3;
		int32_t L_79 = *((int32_t*)L_78);
		*((int32_t*)L_77) = (int32_t)L_79;
		// end = verts.Count;
		int32_t* L_80 = ___end3;
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_81 = ___verts0;
		NullCheck(L_81);
		int32_t L_82;
		L_82 = List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_inline(L_81, /*hidden argument*/List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_RuntimeMethod_var);
		*((int32_t*)L_80) = (int32_t)L_82;
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow__ctor_m0A21DE63CDED6A313DCAA7FCE15EA6F5D8EEBF48 (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// float m_BlurFactor = 1;
		__this->set_m_BlurFactor_12((1.0f));
		// ShadowStyle m_Style = ShadowStyle.Shadow;
		__this->set_m_Style_13(1);
		// [SerializeField] private Color m_EffectColor = new Color(0f, 0f, 0f, 0.5f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5((&L_0), (0.0f), (0.0f), (0.0f), (0.5f), /*hidden argument*/NULL);
		__this->set_m_EffectColor_14(L_0);
		// [SerializeField] private Vector2 m_EffectDistance = new Vector2(1f, -1f);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_1;
		memset((&L_1), 0, sizeof(L_1));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_1), (1.0f), (-1.0f), /*hidden argument*/NULL);
		__this->set_m_EffectDistance_15(L_1);
		// [SerializeField] private bool m_UseGraphicAlpha = true;
		__this->set_m_UseGraphicAlpha_16((bool)1);
		BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UIShadow::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShadow__cctor_m987303AB23CE53BB5BEF7B30A4A47D34B0E28CCD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m30A66B91842A107D2965D350964CD92B2D41584D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// static readonly List<UIShadow> tmpShadows = new List<UIShadow>();
		List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 * L_0 = (List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30 *)il2cpp_codegen_object_new(List_1_tD6BA83B935CAF0C7D297C40C2F9C40B58900DC30_il2cpp_TypeInfo_var);
		List_1__ctor_m30A66B91842A107D2965D350964CD92B2D41584D(L_0, /*hidden argument*/List_1__ctor_m30A66B91842A107D2965D350964CD92B2D41584D_RuntimeMethod_var);
		((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->set_tmpShadows_8(L_0);
		// static readonly List<UIVertex> s_Verts = new List<UIVertex>(4096);
		List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * L_1 = (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F *)il2cpp_codegen_object_new(List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F_il2cpp_TypeInfo_var);
		List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118(L_1, ((int32_t)4096), /*hidden argument*/List_1__ctor_m48440717C5233283529CCE706924A7C9A0082118_RuntimeMethod_var);
		((UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_StaticFields*)il2cpp_codegen_static_fields_for(UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB_il2cpp_TypeInfo_var))->set_s_Verts_9(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Coffee.UIEffects.UIShiny::get_effectFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShiny_get_effectFactor_m7CC24197EA6EC3D9E3F2FDF760FE7491D33D9DAE (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectFactor; }
		float L_0 = __this->get_m_EffectFactor_17();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_effectFactor_mDC9BF9072772D010621E6ECEEFA08070094F9C37 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		float L_2 = __this->get_m_EffectFactor_17();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_EffectFactor = value;
		float L_5 = ___value0;
		__this->set_m_EffectFactor_17(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIShiny::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShiny_get_width_mB3097400337647C1B974AC977F1EA8EF885FFA41 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Width; }
		float L_0 = __this->get_m_Width_18();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_width(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_width_mB05860895310CE75AB56D3A4C85C52705D4DEA01 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Width, value)) return;
		float L_2 = __this->get_m_Width_18();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Width, value)) return;
		return;
	}

IL_0021:
	{
		// m_Width = value;
		float L_5 = ___value0;
		__this->set_m_Width_18(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIShiny::get_softness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShiny_get_softness_mEA7CD28F61BAA8EDBE937D029CD6CDDFD8864F33 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Softness; }
		float L_0 = __this->get_m_Softness_20();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_softness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_softness_m918558EC9B443129ABA1765B7A6E443784FCB5E5 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0.01f, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.00999999978f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Softness, value)) return;
		float L_2 = __this->get_m_Softness_20();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Softness, value)) return;
		return;
	}

IL_0021:
	{
		// m_Softness = value;
		float L_5 = ___value0;
		__this->set_m_Softness_20(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIShiny::get_brightness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShiny_get_brightness_mFD1E509264A2332E79F52059EAFD7D0E98AC30B1 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Brightness; }
		float L_0 = __this->get_m_Brightness_21();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_brightness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_brightness_mBC5ED959615ABF2B95150577AF926CE66327F9BF (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Brightness, value)) return;
		float L_2 = __this->get_m_Brightness_21();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Brightness, value)) return;
		return;
	}

IL_0021:
	{
		// m_Brightness = value;
		float L_5 = ___value0;
		__this->set_m_Brightness_21(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIShiny::get_gloss()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShiny_get_gloss_mE1CC5FFDAB91B9EF04F663B0B903E5C2CDB835A0 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Gloss; }
		float L_0 = __this->get_m_Gloss_22();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_gloss(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_gloss_m17BA5F225B65CE3B9F678B8DB000D2EEB4B52F55 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_Gloss, value)) return;
		float L_2 = __this->get_m_Gloss_22();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_Gloss, value)) return;
		return;
	}

IL_0021:
	{
		// m_Gloss = value;
		float L_5 = ___value0;
		__this->set_m_Gloss_22(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UIShiny::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UIShiny_get_rotation_m987EBABE0F01A057E4B8252178B311B2AAF0ECA0 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Rotation; }
		float L_0 = __this->get_m_Rotation_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_rotation(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_rotation_m9F4B304CA1A09EED8163B83C662D685BC44F9C7C (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// if (Mathf.Approximately(m_Rotation, value)) return;
		float L_0 = __this->get_m_Rotation_19();
		float L_1 = ___value0;
		bool L_2;
		L_2 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (Mathf.Approximately(m_Rotation, value)) return;
		return;
	}

IL_000f:
	{
		// m_Rotation = value;
		float L_3 = ___value0;
		__this->set_m_Rotation_19(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.EffectArea Coffee.UIEffects.UIShiny::get_effectArea()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UIShiny_get_effectArea_mC4190ECC3E1AA96AE907892422ED41DDA89DA98F (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectArea; }
		int32_t L_0 = __this->get_m_EffectArea_23();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::set_effectArea(Coffee.UIEffects.EffectArea)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_set_effectArea_mD926F674DDFBD4147DA077CD829794E19D294D71 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_EffectArea == value) return;
		int32_t L_0 = __this->get_m_EffectArea_23();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_EffectArea == value) return;
		return;
	}

IL_000a:
	{
		// m_EffectArea = value;
		int32_t L_2 = ___value0;
		__this->set_m_EffectArea_23(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShiny::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIShiny_get_paramTex_m35E6A178B7C5D9112925E72BDFAA5E23AD8A35E2 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return s_ParamTex; }
		IL2CPP_RUNTIME_CLASS_INIT(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_il2cpp_TypeInfo_var);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ((UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_StaticFields*)il2cpp_codegen_static_fields_for(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_il2cpp_TypeInfo_var))->get_s_ParamTex_14();
		return L_0;
	}
}
// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIShiny::get_effectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * V_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B2_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B1_0 = NULL;
	{
		// get { return m_Player ?? (m_Player = new EffectPlayer()); }
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0 = __this->get_m_Player_24();
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_2 = (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C *)il2cpp_codegen_object_new(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE(L_2, /*hidden argument*/NULL);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_3 = L_2;
		V_0 = L_3;
		__this->set_m_Player_24(L_3);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_0018:
	{
		return G_B2_0;
	}
}
// System.Void Coffee.UIEffects.UIShiny::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_OnEnable_m9B87F17E22CFEB9A7660BA408933050482C806C6 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIShiny_U3COnEnableU3Eb__37_0_mD8C5F5D76DC15E78E11D122BB45ABD3C6A94E89E_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// base.OnEnable();
		BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE(__this, /*hidden argument*/NULL);
		// effectPlayer.OnEnable(f => effectFactor = f);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A(__this, /*hidden argument*/NULL);
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_1 = (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)il2cpp_codegen_object_new(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3(L_1, __this, (intptr_t)((intptr_t)UIShiny_U3COnEnableU3Eb__37_0_mD8C5F5D76DC15E78E11D122BB45ABD3C6A94E89E_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		NullCheck(L_0);
		EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_OnDisable_m2B6E97484F72EF1EB8CCF0A58804F8469ABDB982 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// base.OnDisable();
		BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580(__this, /*hidden argument*/NULL);
		// effectPlayer.OnDisable();
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.UIShiny::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  UIShiny_GetMaterialHash_m028145B948E2A4159F7C5071B0CA6816C893C865 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!isActiveAndEnabled || !material || !material.shader)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___material0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}

IL_001d:
	{
		// return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_6 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_6;
	}

IL_0023:
	{
		// return new Hash128(
		//     (uint) material.GetInstanceID(),
		//     k_ShaderId,
		//     0,
		//     0
		// );
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = ___material0;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_7, /*hidden argument*/NULL);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Hash128__ctor_m4F9101AF4B79AB2FBC395F48875C21A4E2101581((&L_9), L_8, 8, 0, 0, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Void Coffee.UIEffects.UIShiny::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_ModifyMaterial_m39A0C2725A242EBF52121665B15DD99F2B946B41 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral8366508A811BDFAE9F6D4C8A797C52DC11DF47A1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var connector = GraphicConnector.FindConnector(graphic);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic1;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1;
		L_1 = GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0(L_0, /*hidden argument*/NULL);
		// newMaterial.shader = Shader.Find(string.Format("Hidden/{0} (UIShiny)", newMaterial.shader.name));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___newMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___newMaterial0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteral8366508A811BDFAE9F6D4C8A797C52DC11DF47A1, L_5, /*hidden argument*/NULL);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_7;
		L_7 = Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D(L_2, L_7, /*hidden argument*/NULL);
		// paramTex.RegisterMaterial(newMaterial);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_8;
		L_8 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_9 = ___newMaterial0;
		NullCheck(L_8);
		ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9(L_8, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_ModifyMesh_m4AAC9C9885AC7A23843755180BA9D3EDBC53D74F (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_1;
	memset((&V_1), 0, sizeof(V_1));
	float V_2 = 0.0f;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_3;
	memset((&V_3), 0, sizeof(V_3));
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_4;
	memset((&V_4), 0, sizeof(V_4));
	Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  V_5;
	memset((&V_5), 0, sizeof(V_5));
	int32_t V_6 = 0;
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_7;
	memset((&V_7), 0, sizeof(V_7));
	{
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var normalizedIndex = paramTex.GetNormalizedIndex(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		float L_2;
		L_2 = ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5(L_1, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		// var rect = m_EffectArea.GetEffectArea(vh, rectTransform.rect);
		int32_t L_3 = __this->get_m_EffectArea_23();
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_4 = ___vh0;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_5;
		L_5 = BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_6;
		L_6 = RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_7;
		L_7 = EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7(L_3, L_4, L_6, (-1.0f), /*hidden argument*/NULL);
		V_1 = L_7;
		// var rad = m_Rotation * Mathf.Deg2Rad;
		float L_8 = __this->get_m_Rotation_19();
		V_2 = ((float)il2cpp_codegen_multiply((float)L_8, (float)(0.0174532924f)));
		// var dir = new Vector2(Mathf.Cos(rad), Mathf.Sin(rad));
		float L_9 = V_2;
		float L_10;
		L_10 = cosf(L_9);
		float L_11 = V_2;
		float L_12;
		L_12 = sinf(L_11);
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_3), L_10, L_12, /*hidden argument*/NULL);
		// dir.x *= rect.height / rect.width;
		float* L_13 = (&V_3)->get_address_of_x_0();
		float* L_14 = L_13;
		float L_15 = *((float*)L_14);
		float L_16;
		L_16 = Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		float L_17;
		L_17 = Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF((Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 *)(&V_1), /*hidden argument*/NULL);
		*((float*)L_14) = (float)((float)il2cpp_codegen_multiply((float)L_15, (float)((float)((float)L_16/(float)L_17))));
		// dir = dir.normalized;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_18;
		L_18 = Vector2_get_normalized_m1F7F7AA3B7AC2414F245395C3785880B847BF7F5((Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_3), /*hidden argument*/NULL);
		V_3 = L_18;
		// var vertex = default(UIVertex);
		il2cpp_codegen_initobj((&V_4), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// var localMatrix = new Matrix2x3(rect, dir.x, dir.y); // Get local matrix.
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_19 = V_1;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_20 = V_3;
		float L_21 = L_20.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_22 = V_3;
		float L_23 = L_22.get_y_1();
		Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE((Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED *)(&V_5), L_19, L_21, L_23, /*hidden argument*/NULL);
		// for (int i = 0; i < vh.currentVertCount; i++)
		V_6 = 0;
		goto IL_010e;
	}

IL_0096:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_24 = ___vh0;
		int32_t L_25 = V_6;
		NullCheck(L_24);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_24, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_4), L_25, /*hidden argument*/NULL);
		// connector.GetNormalizedFactor(m_EffectArea, i, localMatrix, vertex.position, out normalizedPos);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_26;
		L_26 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		int32_t L_27 = __this->get_m_EffectArea_23();
		int32_t L_28 = V_6;
		Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED  L_29 = V_5;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_30 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = L_30.get_position_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_32;
		L_32 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_31, /*hidden argument*/NULL);
		NullCheck(L_26);
		VirtActionInvoker5< int32_t, int32_t, Matrix2x3_t168A0107FACD8D92ED30E553C4CA397BF4BC6DED , Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * >::Invoke(15 /* System.Void Coffee.UIEffects.GraphicConnector::GetNormalizedFactor(Coffee.UIEffects.EffectArea,System.Int32,Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2,UnityEngine.Vector2&) */, L_26, L_27, L_28, L_29, L_32, (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 *)(&V_7));
		// vertex.uv0 = new Vector2(
		//     Packer.ToFloat(vertex.uv0.x, vertex.uv0.y),
		//     Packer.ToFloat(normalizedPos.y, normalizedIndex)
		// );
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_33 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_34 = L_33.get_uv0_4();
		float L_35 = L_34.get_x_1();
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_36 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_37 = L_36.get_uv0_4();
		float L_38 = L_37.get_y_2();
		float L_39;
		L_39 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_35, L_38, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_40 = V_7;
		float L_41 = L_40.get_y_1();
		float L_42 = V_0;
		float L_43;
		L_43 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_41, L_42, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_44;
		memset((&L_44), 0, sizeof(L_44));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_44), L_39, L_43, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_45;
		L_45 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_44, /*hidden argument*/NULL);
		(&V_4)->set_uv0_4(L_45);
		// vh.SetUIVertex(vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_46 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_47 = V_4;
		int32_t L_48 = V_6;
		NullCheck(L_46);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_46, L_47, L_48, /*hidden argument*/NULL);
		// for (int i = 0; i < vh.currentVertCount; i++)
		int32_t L_49 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_010e:
	{
		// for (int i = 0; i < vh.currentVertCount; i++)
		int32_t L_50 = V_6;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_51 = ___vh0;
		NullCheck(L_51);
		int32_t L_52;
		L_52 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_51, /*hidden argument*/NULL);
		if ((((int32_t)L_50) < ((int32_t)L_52)))
		{
			goto IL_0096;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::Play(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_Play_mD7A99CB880214409A701FC18D30E079BF900F9E0 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, bool ___reset0, const RuntimeMethod* method)
{
	{
		// effectPlayer.Play(reset);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A(__this, /*hidden argument*/NULL);
		bool L_1 = ___reset0;
		NullCheck(L_0);
		EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802(L_0, L_1, (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)NULL, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::Stop(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_Stop_mA1E3C7A37FDFD05D8ABD5D6BA62E448141FE2806 (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, bool ___reset0, const RuntimeMethod* method)
{
	{
		// effectPlayer.Stop(reset);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A(__this, /*hidden argument*/NULL);
		bool L_1 = ___reset0;
		NullCheck(L_0);
		EffectPlayer_Stop_mCDDB096B24E282D85C931BC545CE0C03FE4AF357(L_0, L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::SetEffectParamsDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_SetEffectParamsDirty_m263709BB893DDC8C9A4ADFBE9410B0EB4E518C8F (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// paramTex.SetData(this, 0, m_EffectFactor); // param1.x : location
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_1 = __this->get_m_EffectFactor_17();
		NullCheck(L_0);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_0, __this, 0, L_1, /*hidden argument*/NULL);
		// paramTex.SetData(this, 1, m_Width); // param1.y : width
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_2;
		L_2 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_3 = __this->get_m_Width_18();
		NullCheck(L_2);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_2, __this, 1, L_3, /*hidden argument*/NULL);
		// paramTex.SetData(this, 2, m_Softness); // param1.z : softness
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_4;
		L_4 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_5 = __this->get_m_Softness_20();
		NullCheck(L_4);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_4, __this, 2, L_5, /*hidden argument*/NULL);
		// paramTex.SetData(this, 3, m_Brightness); // param1.w : blightness
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_6;
		L_6 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_7 = __this->get_m_Brightness_21();
		NullCheck(L_6);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_6, __this, 3, L_7, /*hidden argument*/NULL);
		// paramTex.SetData(this, 4, m_Gloss); // param2.x : gloss
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_8;
		L_8 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_9 = __this->get_m_Gloss_22();
		NullCheck(L_8);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_8, __this, 4, L_9, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::SetVerticesDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_SetVerticesDirty_mBCDE768BFC79060F457CDA382D12A9792D7BE56C (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// base.SetVerticesDirty();
		BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9(__this, /*hidden argument*/NULL);
		// _lastRotation = m_Rotation;
		float L_0 = __this->get_m_Rotation_19();
		__this->set__lastRotation_15(L_0);
		// _lastEffectArea = m_EffectArea;
		int32_t L_1 = __this->get_m_EffectArea_23();
		__this->set__lastEffectArea_16(L_1);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::OnDidApplyAnimationProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_OnDidApplyAnimationProperties_m9CD7B1B017035892BD2D3134FAC9B9BD48FBD1DC (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	{
		// base.OnDidApplyAnimationProperties();
		BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E(__this, /*hidden argument*/NULL);
		// if (!Mathf.Approximately(_lastRotation, m_Rotation)
		//     || _lastEffectArea != m_EffectArea)
		float L_0 = __this->get__lastRotation_15();
		float L_1 = __this->get_m_Rotation_19();
		bool L_2;
		L_2 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0027;
		}
	}
	{
		int32_t L_3 = __this->get__lastEffectArea_16();
		int32_t L_4 = __this->get_m_EffectArea_23();
		if ((((int32_t)L_3) == ((int32_t)L_4)))
		{
			goto IL_002d;
		}
	}

IL_0027:
	{
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
	}

IL_002d:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny__ctor_m213C91AA2E259A559D0D2E0CDF8A5E953A4CE02E (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// float m_EffectFactor = 0.5f;
		__this->set_m_EffectFactor_17((0.5f));
		// float m_Width = 0.25f;
		__this->set_m_Width_18((0.25f));
		// float m_Rotation = 135;
		__this->set_m_Rotation_19((135.0f));
		// float m_Softness = 1f;
		__this->set_m_Softness_20((1.0f));
		// float m_Brightness = 1f;
		__this->set_m_Brightness_21((1.0f));
		// float m_Gloss = 1;
		__this->set_m_Gloss_22((1.0f));
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny__cctor_m236A3CDD15FBE0448E4E6F64467737B74CC7F227 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly ParameterTexture s_ParamTex = new ParameterTexture(8, 128, "_ParamTex");
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)il2cpp_codegen_object_new(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876(L_0, 8, ((int32_t)128), _stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587, /*hidden argument*/NULL);
		((UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_StaticFields*)il2cpp_codegen_static_fields_for(UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146_il2cpp_TypeInfo_var))->set_s_ParamTex_14(L_0);
		return;
	}
}
// System.Void Coffee.UIEffects.UIShiny::<OnEnable>b__37_0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UIShiny_U3COnEnableU3Eb__37_0_mD8C5F5D76DC15E78E11D122BB45ABD3C6A94E89E (UIShiny_tDE25BE47E9F056F76F4CA6A2E6A7438461A13146 * __this, float ___f0, const RuntimeMethod* method)
{
	{
		// effectPlayer.OnEnable(f => effectFactor = f);
		float L_0 = ___f0;
		UIShiny_set_effectFactor_mDC9BF9072772D010621E6ECEEFA08070094F9C37(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Coffee.UIEffects.BaseMeshEffect Coffee.UIEffects.UISyncEffect::get_targetEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return m_TargetEffect != this ? m_TargetEffect : null; }
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_0 = __this->get_m_TargetEffect_13();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, __this, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0010;
		}
	}
	{
		return (BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A *)NULL;
	}

IL_0010:
	{
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_2 = __this->get_m_TargetEffect_13();
		return L_2;
	}
}
// System.Void Coffee.UIEffects.UISyncEffect::set_targetEffect(Coffee.UIEffects.BaseMeshEffect)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISyncEffect_set_targetEffect_mCCFC727AC6494DA2150CCBA6F1B418AC20E7BA16 (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_TargetEffect == value) return;
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_0 = __this->get_m_TargetEffect_13();
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_TargetEffect == value) return;
		return;
	}

IL_000f:
	{
		// m_TargetEffect = value;
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_3 = ___value0;
		__this->set_m_TargetEffect_13(L_3);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UISyncEffect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISyncEffect_OnEnable_m6543A8A00A4E24DDA8709078CDD42B608BF9FFA6 (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m33273E2D2434DA63CFC046FA4864F2310CE5AAD3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (targetEffect)
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_0;
		L_0 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		// targetEffect.syncEffects.Add(this);
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_2;
		L_2 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * L_3 = L_2->get_syncEffects_7();
		NullCheck(L_3);
		List_1_Add_m33273E2D2434DA63CFC046FA4864F2310CE5AAD3(L_3, __this, /*hidden argument*/List_1_Add_m33273E2D2434DA63CFC046FA4864F2310CE5AAD3_RuntimeMethod_var);
	}

IL_001e:
	{
		// base.OnEnable();
		BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UISyncEffect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISyncEffect_OnDisable_m6B06EF4FE6D838E0F6A3AD34495AA050E116038B (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Remove_m905C6A86518BE8E31A16F5C7CAD8730B548CC554_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (targetEffect)
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_0;
		L_0 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001f;
		}
	}
	{
		// targetEffect.syncEffects.Remove(this);
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_2;
		L_2 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		List_1_t01BE5878AE01033101C05CE072C892141BBDA54F * L_3 = L_2->get_syncEffects_7();
		NullCheck(L_3);
		bool L_4;
		L_4 = List_1_Remove_m905C6A86518BE8E31A16F5C7CAD8730B548CC554(L_3, __this, /*hidden argument*/List_1_Remove_m905C6A86518BE8E31A16F5C7CAD8730B548CC554_RuntimeMethod_var);
	}

IL_001f:
	{
		// base.OnDisable();
		BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.UISyncEffect::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  UISyncEffect_GetMaterialHash_m606DFC3572F7EB6F5DFCA33847F2E316BA67CDC9 (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___baseMaterial0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * V_0 = NULL;
	{
		// if (!isActiveAndEnabled) return k_InvalidHash;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000e;
		}
	}
	{
		// if (!isActiveAndEnabled) return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_1 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_1;
	}

IL_000e:
	{
		// var matEffect = targetEffect as BaseMaterialEffect;
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_2;
		L_2 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		V_0 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 *)IsInstClass((RuntimeObject*)L_2, BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var));
		// if (!matEffect || !matEffect.isActiveAndEnabled) return k_InvalidHash;
		BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_002a;
		}
	}
	{
		BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * L_5 = V_0;
		NullCheck(L_5);
		bool L_6;
		L_6 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0030;
		}
	}

IL_002a:
	{
		// if (!matEffect || !matEffect.isActiveAndEnabled) return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_7 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_7;
	}

IL_0030:
	{
		// return matEffect.GetMaterialHash(baseMaterial);
		BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * L_8 = V_0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_9 = ___baseMaterial0;
		NullCheck(L_8);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_10;
		L_10 = VirtFuncInvoker1< Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A , Material_t8927C00353A72755313F046D0CE85178AE8218EE * >::Invoke(29 /* UnityEngine.Hash128 Coffee.UIEffects.BaseMaterialEffect::GetMaterialHash(UnityEngine.Material) */, L_8, L_9);
		return L_10;
	}
}
// System.Void Coffee.UIEffects.UISyncEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISyncEffect_ModifyMaterial_mDDD99F8EA072BF04EEB90855BAF954870BFFC251 (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * V_0 = NULL;
	{
		// if (!isActiveAndEnabled) return;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!isActiveAndEnabled) return;
		return;
	}

IL_0009:
	{
		// var matEffect = targetEffect as BaseMaterialEffect;
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_1;
		L_1 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		V_0 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 *)IsInstClass((RuntimeObject*)L_1, BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var));
		// if (!matEffect || !matEffect.isActiveAndEnabled) return;
		BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_3;
		L_3 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * L_4 = V_0;
		NullCheck(L_4);
		bool L_5;
		L_5 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0026;
		}
	}

IL_0025:
	{
		// if (!matEffect || !matEffect.isActiveAndEnabled) return;
		return;
	}

IL_0026:
	{
		// matEffect.ModifyMaterial(newMaterial, graphic);
		BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065 * L_6 = V_0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_7 = ___newMaterial0;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_8 = ___graphic1;
		NullCheck(L_6);
		VirtActionInvoker2< Material_t8927C00353A72755313F046D0CE85178AE8218EE *, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(31 /* System.Void Coffee.UIEffects.BaseMaterialEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic) */, L_6, L_7, L_8);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UISyncEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISyncEffect_ModifyMesh_m42AC1484094636F2EA42979A8F3F59822C22DC70 (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (!isActiveAndEnabled) return;
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// if (!isActiveAndEnabled) return;
		return;
	}

IL_0009:
	{
		// if (!targetEffect || !targetEffect.isActiveAndEnabled) return;
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_1;
		L_1 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_3;
		L_3 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4;
		L_4 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0024;
		}
	}

IL_0023:
	{
		// if (!targetEffect || !targetEffect.isActiveAndEnabled) return;
		return;
	}

IL_0024:
	{
		// targetEffect.ModifyMesh(vh, graphic);
		BaseMeshEffect_tB981157EDBB416DB2AD7F658947B2E8CE658E36A * L_5;
		L_5 = UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C(__this, /*hidden argument*/NULL);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_6 = ___vh0;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_7 = ___graphic1;
		NullCheck(L_5);
		VirtActionInvoker2< VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 *, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * >::Invoke(21 /* System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic) */, L_5, L_6, L_7);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UISyncEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISyncEffect__ctor_mBC4DE35774A9EB941735869ED682B8FD3BEB2F98 (UISyncEffect_tBF1333B10531A1D38F0C76E2F121B6F28302699E * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single Coffee.UIEffects.UITransitionEffect::get_effectFactor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UITransitionEffect_get_effectFactor_m632CBDEC96902006CF2EFB9D1E94F2A7CF65C1E6 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectFactor; }
		float L_0 = __this->get_m_EffectFactor_19();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_effectFactor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_effectFactor_m6316BCC7814AE16693E998498FE3F199DEFE6BC7 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		float L_2 = __this->get_m_EffectFactor_19();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_EffectFactor, value)) return;
		return;
	}

IL_0021:
	{
		// m_EffectFactor = value;
		float L_5 = ___value0;
		__this->set_m_EffectFactor_19(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::get_transitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return m_TransitionTexture
		//     ? m_TransitionTexture
		//     : defaultTransitionTexture;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = __this->get_m_TransitionTexture_20();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_2;
		L_2 = UITransitionEffect_get_defaultTransitionTexture_mB6C7677AF8930EA977477611EC747B8E44B9FE80(/*hidden argument*/NULL);
		return L_2;
	}

IL_0013:
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3 = __this->get_m_TransitionTexture_20();
		return L_3;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_transitionTexture(UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_transitionTexture_m8354B8733DD664EA8D8E5EC7DD60D39F9667DD31 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (m_TransitionTexture == value) return;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = __this->get_m_TransitionTexture_20();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_1 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_TransitionTexture == value) return;
		return;
	}

IL_000f:
	{
		// m_TransitionTexture = value;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3 = ___value0;
		__this->set_m_TransitionTexture_20(L_3);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::get_defaultTransitionTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * UITransitionEffect_get_defaultTransitionTexture_mB6C7677AF8930EA977477611EC747B8E44B9FE80 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral055F326E6AF2473ADEC1A21756F2574F6644221B);
		s_Il2CppMethodInitialized = true;
	}
	{
		// return _defaultTransitionTexture
		//     ? _defaultTransitionTexture
		//     : (_defaultTransitionTexture = Resources.Load<Texture>("Default-Transition"));
		IL2CPP_RUNTIME_CLASS_INIT(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_0 = ((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->get__defaultTransitionTexture_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001d;
		}
	}
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_2;
		L_2 = Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8(_stringLiteral055F326E6AF2473ADEC1A21756F2574F6644221B, /*hidden argument*/Resources_Load_TisTexture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE_m656D04F9019047B239DAE185817F2E1D7ED007B8_RuntimeMethod_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->set__defaultTransitionTexture_17(L_3);
		return L_3;
	}

IL_001d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_4 = ((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->get__defaultTransitionTexture_17();
		return L_4;
	}
}
// Coffee.UIEffects.UITransitionEffect/EffectMode Coffee.UIEffects.UITransitionEffect::get_effectMode()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t UITransitionEffect_get_effectMode_m791A11B214ACF5EC355A16F2E0C86E20116377F2 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectMode; }
		int32_t L_0 = __this->get_m_EffectMode_18();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_effectMode(Coffee.UIEffects.UITransitionEffect/EffectMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_effectMode_m03EA15F5C7F92D8A8A2EDB35E93A324E4B3E362A (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// if (m_EffectMode == value) return;
		int32_t L_0 = __this->get_m_EffectMode_18();
		int32_t L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_EffectMode == value) return;
		return;
	}

IL_000a:
	{
		// m_EffectMode = value;
		int32_t L_2 = ___value0;
		__this->set_m_EffectMode_18(L_2);
		// SetMaterialDirty();
		BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.UITransitionEffect::get_keepAspectRatio()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UITransitionEffect_get_keepAspectRatio_m6484BC9EBFF7A32D329BCD6950D42D4B4218637A (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_KeepAspectRatio; }
		bool L_0 = __this->get_m_KeepAspectRatio_22();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_keepAspectRatio(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_keepAspectRatio_m715D03A69C94555B3FD9AC4BF0E3E092CFCA6C6C (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// if (m_KeepAspectRatio == value) return;
		bool L_0 = __this->get_m_KeepAspectRatio_22();
		bool L_1 = ___value0;
		if ((!(((uint32_t)L_0) == ((uint32_t)L_1))))
		{
			goto IL_000a;
		}
	}
	{
		// if (m_KeepAspectRatio == value) return;
		return;
	}

IL_000a:
	{
		// m_KeepAspectRatio = value;
		bool L_2 = ___value0;
		__this->set_m_KeepAspectRatio_22(L_2);
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
		// }
		return;
	}
}
// Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UITransitionEffect::get_paramTex()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UITransitionEffect_get_paramTex_m8CCFCA1D815FC09F713F2F7853DCF75CC6C7EA35 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return s_ParamTex; }
		IL2CPP_RUNTIME_CLASS_INIT(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->get_s_ParamTex_15();
		return L_0;
	}
}
// System.Single Coffee.UIEffects.UITransitionEffect::get_dissolveWidth()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UITransitionEffect_get_dissolveWidth_m82A0BF713399790EF9AC1023E814B48E4BA962E4 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_DissolveWidth; }
		float L_0 = __this->get_m_DissolveWidth_23();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_dissolveWidth(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_dissolveWidth_m90D68E355B7F41A0A46C11DF2879403AEC673F77 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_DissolveWidth, value)) return;
		float L_2 = __this->get_m_DissolveWidth_23();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_DissolveWidth, value)) return;
		return;
	}

IL_0021:
	{
		// m_DissolveWidth = value;
		float L_5 = ___value0;
		__this->set_m_DissolveWidth_23(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Single Coffee.UIEffects.UITransitionEffect::get_dissolveSoftness()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float UITransitionEffect_get_dissolveSoftness_mFC4C5C7A6A76E00FC256964D4BFE617D3876CCC4 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_DissolveSoftness; }
		float L_0 = __this->get_m_DissolveSoftness_24();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_dissolveSoftness(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_dissolveSoftness_m7359FBFDF96454D92B6FDCF6F1035C9269935685 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, float ___value0, const RuntimeMethod* method)
{
	{
		// value = Mathf.Clamp(value, 0, 1);
		float L_0 = ___value0;
		float L_1;
		L_1 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_0, (0.0f), (1.0f), /*hidden argument*/NULL);
		___value0 = L_1;
		// if (Mathf.Approximately(m_DissolveSoftness, value)) return;
		float L_2 = __this->get_m_DissolveSoftness_24();
		float L_3 = ___value0;
		bool L_4;
		L_4 = Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55(L_2, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0021;
		}
	}
	{
		// if (Mathf.Approximately(m_DissolveSoftness, value)) return;
		return;
	}

IL_0021:
	{
		// m_DissolveSoftness = value;
		float L_5 = ___value0;
		__this->set_m_DissolveSoftness_24(L_5);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// UnityEngine.Color Coffee.UIEffects.UITransitionEffect::get_dissolveColor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UITransitionEffect_get_dissolveColor_m087996550F653B3DD57D5B7C4ED28A6700720583 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_DissolveColor; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_DissolveColor_25();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_dissolveColor(UnityEngine.Color)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_dissolveColor_mA08833DCB9133AA6C022C4FF48EB674BE5BB7863 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___value0, const RuntimeMethod* method)
{
	{
		// if (m_DissolveColor == value) return;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_DissolveColor_25();
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_1 = ___value0;
		bool L_2;
		L_2 = Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (m_DissolveColor == value) return;
		return;
	}

IL_000f:
	{
		// m_DissolveColor = value;
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_3 = ___value0;
		__this->set_m_DissolveColor_25(L_3);
		// SetEffectParamsDirty();
		VirtActionInvoker0::Invoke(23 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty() */, __this);
		// }
		return;
	}
}
// System.Boolean Coffee.UIEffects.UITransitionEffect::get_passRayOnHidden()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool UITransitionEffect_get_passRayOnHidden_m7F6FCC01F882D6DDD86E86FC477C2A14336A0272 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_PassRayOnHidden; }
		bool L_0 = __this->get_m_PassRayOnHidden_26();
		return L_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::set_passRayOnHidden(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_set_passRayOnHidden_m485D4113137975F3E7894291A68CC8DDD4CAC31E (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { m_PassRayOnHidden = value; }
		bool L_0 = ___value0;
		__this->set_m_PassRayOnHidden_26(L_0);
		// set { m_PassRayOnHidden = value; }
		return;
	}
}
// Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UITransitionEffect::get_effectPlayer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * V_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B2_0 = NULL;
	EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * G_B1_0 = NULL;
	{
		// get { return m_Player ?? (m_Player = new EffectPlayer()); }
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0 = __this->get_m_Player_27();
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0018;
		}
	}
	{
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_2 = (EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C *)il2cpp_codegen_object_new(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE(L_2, /*hidden argument*/NULL);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_3 = L_2;
		V_0 = L_3;
		__this->set_m_Player_27(L_3);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_4 = V_0;
		G_B2_0 = L_4;
	}

IL_0018:
	{
		return G_B2_0;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::Show(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_Show_m94DE5DA72ABAC6F4B4BD08238BDE9BA371C852D6 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, bool ___reset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_U3CShowU3Eb__46_0_m57FEB42ADD3994430AF16F786501D9B18B5ED71A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// effectPlayer.loop = false;
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_loop_3((bool)0);
		// effectPlayer.Play(reset, f => effectFactor = f);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_1;
		L_1 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		bool L_2 = ___reset0;
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_3 = (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)il2cpp_codegen_object_new(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3(L_3, __this, (intptr_t)((intptr_t)UITransitionEffect_U3CShowU3Eb__46_0_m57FEB42ADD3994430AF16F786501D9B18B5ED71A_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		NullCheck(L_1);
		EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802(L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::Hide(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_Hide_m1C9F1CB283F5DF13BAC04BC4FF43316133919DDB (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, bool ___reset0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_U3CHideU3Eb__47_0_mF3CCECEBADA02DE02348B5455B049D700C33B4B9_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// effectPlayer.loop = false;
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		L_0->set_loop_3((bool)0);
		// effectPlayer.Play(reset, f => effectFactor = 1 - f);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_1;
		L_1 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		bool L_2 = ___reset0;
		Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 * L_3 = (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)il2cpp_codegen_object_new(Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9_il2cpp_TypeInfo_var);
		Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3(L_3, __this, (intptr_t)((intptr_t)UITransitionEffect_U3CHideU3Eb__47_0_mF3CCECEBADA02DE02348B5455B049D700C33B4B9_RuntimeMethod_var), /*hidden argument*/Action_1__ctor_m7514CC492FC5E63D7FA62E0FB54CF5E5956D8EC3_RuntimeMethod_var);
		NullCheck(L_1);
		EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802(L_1, L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Hash128 Coffee.UIEffects.UITransitionEffect::GetMaterialHash(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  UITransitionEffect_GetMaterialHash_mA610989C67E64BF3A05096838EC7F381A777ED17 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___material0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	uint32_t V_1 = 0;
	{
		// if (!isActiveAndEnabled || !material || !material.shader)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_1 = ___material0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001d;
		}
	}
	{
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___material0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_4, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0023;
		}
	}

IL_001d:
	{
		// return k_InvalidHash;
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_6 = ((BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_StaticFields*)il2cpp_codegen_static_fields_for(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var))->get_k_InvalidHash_8();
		return L_6;
	}

IL_0023:
	{
		// var shaderVariantId = (uint) ((int) m_EffectMode << 6);
		int32_t L_7 = __this->get_m_EffectMode_18();
		V_0 = ((int32_t)((int32_t)L_7<<(int32_t)6));
		// var resourceId = (uint) transitionTexture.GetInstanceID();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_8;
		L_8 = UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9;
		L_9 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		// return new Hash128(
		//     (uint) material.GetInstanceID(),
		//     k_ShaderId + shaderVariantId,
		//     resourceId,
		//     0
		// );
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_10 = ___material0;
		NullCheck(L_10);
		int32_t L_11;
		L_11 = Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501(L_10, /*hidden argument*/NULL);
		uint32_t L_12 = V_0;
		uint32_t L_13 = V_1;
		Hash128_t1858EA099934FD6F2B769E5661C17A276A2AFE7A  L_14;
		memset((&L_14), 0, sizeof(L_14));
		Hash128__ctor_m4F9101AF4B79AB2FBC395F48875C21A4E2101581((&L_14), L_11, ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)40), (int32_t)L_12)), L_13, 0, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_ModifyMaterial_mF7DFA18AA710C04E11443F2B6DF29CDA25C3435F (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___newMaterial0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectMode_t81EABEB9816582A75850DECB66BA88C7C6BF1999_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF54472B8669373DE9BE5584699CBC7AEA9CEF091);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var connector = GraphicConnector.FindConnector(graphic);
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_0 = ___graphic1;
		IL2CPP_RUNTIME_CLASS_INIT(GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F_il2cpp_TypeInfo_var);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_1;
		L_1 = GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0(L_0, /*hidden argument*/NULL);
		// newMaterial.shader = Shader.Find(string.Format("Hidden/{0} (UITransition)", newMaterial.shader.name));
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = ___newMaterial0;
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_3 = ___newMaterial0;
		NullCheck(L_3);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_4;
		L_4 = Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		String_t* L_5;
		L_5 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_4, /*hidden argument*/NULL);
		String_t* L_6;
		L_6 = String_Format_mB3D38E5238C3164DB4D7D29339D9E225A4496D17(_stringLiteralF54472B8669373DE9BE5584699CBC7AEA9CEF091, L_5, /*hidden argument*/NULL);
		Shader_tB2355DC4F3CAF20B2F1AB5AABBF37C3555FFBC39 * L_7;
		L_7 = Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A(L_6, /*hidden argument*/NULL);
		NullCheck(L_2);
		Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D(L_2, L_7, /*hidden argument*/NULL);
		// SetShaderVariants(newMaterial, m_EffectMode);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_8 = ___newMaterial0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_10 = L_9;
		int32_t L_11 = __this->get_m_EffectMode_18();
		int32_t L_12 = L_11;
		RuntimeObject * L_13 = Box(EffectMode_t81EABEB9816582A75850DECB66BA88C7C6BF1999_il2cpp_TypeInfo_var, &L_12);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_13);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_13);
		BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1(__this, L_8, L_10, /*hidden argument*/NULL);
		// newMaterial.SetTexture(k_TransitionTexId, transitionTexture);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_14 = ___newMaterial0;
		IL2CPP_RUNTIME_CLASS_INIT(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		int32_t L_15 = ((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->get_k_TransitionTexId_14();
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_16;
		L_16 = UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60(L_14, L_15, L_16, /*hidden argument*/NULL);
		// paramTex.RegisterMaterial(newMaterial);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_17;
		L_17 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_18 = ___newMaterial0;
		NullCheck(L_17);
		ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9(L_17, L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_ModifyMesh_mE2925A2E4B63AB8BFB78E632089B124F84DE142C (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___vh0, Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___graphic1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * V_1 = NULL;
	float V_2 = 0.0f;
	Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  V_3;
	memset((&V_3), 0, sizeof(V_3));
	UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  V_4;
	memset((&V_4), 0, sizeof(V_4));
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	float G_B6_0 = 0.0f;
	{
		// if (!isActiveAndEnabled)
		bool L_0;
		L_0 = Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0009;
		}
	}
	{
		// return;
		return;
	}

IL_0009:
	{
		// var normalizedIndex = paramTex.GetNormalizedIndex(this);
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1;
		L_1 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		NullCheck(L_1);
		float L_2;
		L_2 = ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5(L_1, __this, /*hidden argument*/NULL);
		V_0 = L_2;
		// var tex = transitionTexture;
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_3;
		L_3 = UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		// var aspectRatio = m_KeepAspectRatio && tex ? ((float) tex.width) / tex.height : -1;
		bool L_4 = __this->get_m_KeepAspectRatio_22();
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_5, /*hidden argument*/NULL);
		if (L_6)
		{
			goto IL_0034;
		}
	}

IL_002d:
	{
		G_B6_0 = (-1.0f);
		goto IL_0043;
	}

IL_0034:
	{
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_7 = V_1;
		NullCheck(L_7);
		int32_t L_8;
		L_8 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_7);
		Texture_t9FE0218A1EEDF266E8C85879FE123265CACC95AE * L_9 = V_1;
		NullCheck(L_9);
		int32_t L_10;
		L_10 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_9);
		G_B6_0 = ((float)((float)((float)((float)L_8))/(float)((float)((float)L_10))));
	}

IL_0043:
	{
		V_2 = G_B6_0;
		// var rect = m_EffectArea.GetEffectArea(vh, rectTransform.rect, aspectRatio);
		int32_t L_11 = __this->get_m_EffectArea_21();
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_12 = ___vh0;
		RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * L_13;
		L_13 = BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_14;
		L_14 = RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2(L_13, /*hidden argument*/NULL);
		float L_15 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(EffectAreaExtensions_tDA101A6D56942F0A8872190849BBCC933900DE9B_il2cpp_TypeInfo_var);
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_16;
		L_16 = EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7(L_11, L_12, L_14, L_15, /*hidden argument*/NULL);
		V_3 = L_16;
		// var vertex = default(UIVertex);
		il2cpp_codegen_initobj((&V_4), sizeof(UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A ));
		// var count = vh.currentVertCount;
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_17 = ___vh0;
		NullCheck(L_17);
		int32_t L_18;
		L_18 = VertexHelper_get_currentVertCount_m4E9932F9BBCC9CB9636B3415A03454D6B7A92807(L_17, /*hidden argument*/NULL);
		V_5 = L_18;
		// for (var i = 0; i < count; i++)
		V_6 = 0;
		goto IL_00e8;
	}

IL_0072:
	{
		// vh.PopulateUIVertex(ref vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_19 = ___vh0;
		int32_t L_20 = V_6;
		NullCheck(L_19);
		VertexHelper_PopulateUIVertex_m540F0A80C1A55C7444259CEE118CAC61F198B555(L_19, (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A *)(&V_4), L_20, /*hidden argument*/NULL);
		// connector.GetPositionFactor(m_EffectArea, i, rect, vertex.position, out x, out y);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_21;
		L_21 = BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D(__this, /*hidden argument*/NULL);
		int32_t L_22 = __this->get_m_EffectArea_21();
		int32_t L_23 = V_6;
		Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878  L_24 = V_3;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_25 = V_4;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = L_25.get_position_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_27;
		L_27 = Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline(L_26, /*hidden argument*/NULL);
		NullCheck(L_21);
		VirtActionInvoker6< int32_t, int32_t, Rect_t7D9187DB6339DBA5741C09B6CCEF2F54F1966878 , Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 , float*, float* >::Invoke(12 /* System.Void Coffee.UIEffects.GraphicConnector::GetPositionFactor(Coffee.UIEffects.EffectArea,System.Int32,UnityEngine.Rect,UnityEngine.Vector2,System.Single&,System.Single&) */, L_21, L_22, L_23, L_24, L_27, (float*)(&V_7), (float*)(&V_8));
		// vertex.uv0 = new Vector2(
		//     Packer.ToFloat(vertex.uv0.x, vertex.uv0.y),
		//     Packer.ToFloat(x, y, normalizedIndex)
		// );
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_28 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_29 = L_28.get_uv0_4();
		float L_30 = L_29.get_x_1();
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_31 = V_4;
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_32 = L_31.get_uv0_4();
		float L_33 = L_32.get_y_2();
		float L_34;
		L_34 = Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662(L_30, L_33, /*hidden argument*/NULL);
		float L_35 = V_7;
		float L_36 = V_8;
		float L_37 = V_0;
		float L_38;
		L_38 = Packer_ToFloat_m7691266B0941E760FB34C506126EEAF8A8FBE1D7(L_35, L_36, L_37, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_39;
		memset((&L_39), 0, sizeof(L_39));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_39), L_34, L_38, /*hidden argument*/NULL);
		Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  L_40;
		L_40 = Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504(L_39, /*hidden argument*/NULL);
		(&V_4)->set_uv0_4(L_40);
		// vh.SetUIVertex(vertex, i);
		VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * L_41 = ___vh0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_42 = V_4;
		int32_t L_43 = V_6;
		NullCheck(L_41);
		VertexHelper_SetUIVertex_mE6E1BF09DA31C90FA922B6F96123D7C363A71D7E(L_41, L_42, L_43, /*hidden argument*/NULL);
		// for (var i = 0; i < count; i++)
		int32_t L_44 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_add((int32_t)L_44, (int32_t)1));
	}

IL_00e8:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_45 = V_6;
		int32_t L_46 = V_5;
		if ((((int32_t)L_45) < ((int32_t)L_46)))
		{
			goto IL_0072;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::OnEnable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_OnEnable_mA79771EC4DE69ED8844126AB7CF69C10CBD43CEF (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// base.OnEnable();
		BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE(__this, /*hidden argument*/NULL);
		// effectPlayer.OnEnable(null);
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502(L_0, (Action_1_t14225BCEFEF7A87B9836BA1CC40C611E313112D9 *)NULL, /*hidden argument*/NULL);
		// effectPlayer.loop = false;
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_1;
		L_1 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->set_loop_3((bool)0);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_OnDisable_m6BDD9389E4B91B0032D70E1016FD507B6C7A074F (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// base.OnDisable();
		BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580(__this, /*hidden argument*/NULL);
		// effectPlayer.OnDisable();
		EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C * L_0;
		L_0 = UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::SetEffectParamsDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_SetEffectParamsDirty_m4895AE69C72B203EFA443AC7EB3B87BF575908EE (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// paramTex.SetData(this, 0, m_EffectFactor); // param1.x : effect factor
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0;
		L_0 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_1 = __this->get_m_EffectFactor_19();
		NullCheck(L_0);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_0, __this, 0, L_1, /*hidden argument*/NULL);
		// if (m_EffectMode == EffectMode.Dissolve)
		int32_t L_2 = __this->get_m_EffectMode_18();
		if ((!(((uint32_t)L_2) == ((uint32_t)3))))
		{
			goto IL_008a;
		}
	}
	{
		// paramTex.SetData(this, 1, m_DissolveWidth); // param1.y : width
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_3;
		L_3 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_4 = __this->get_m_DissolveWidth_23();
		NullCheck(L_3);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_3, __this, 1, L_4, /*hidden argument*/NULL);
		// paramTex.SetData(this, 2, m_DissolveSoftness); // param1.z : softness
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_5;
		L_5 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		float L_6 = __this->get_m_DissolveSoftness_24();
		NullCheck(L_5);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_5, __this, 2, L_6, /*hidden argument*/NULL);
		// paramTex.SetData(this, 4, m_DissolveColor.r); // param2.x : red
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_7;
		L_7 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_8 = __this->get_address_of_m_DissolveColor_25();
		float L_9 = L_8->get_r_0();
		NullCheck(L_7);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_7, __this, 4, L_9, /*hidden argument*/NULL);
		// paramTex.SetData(this, 5, m_DissolveColor.g); // param2.y : green
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_10;
		L_10 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_11 = __this->get_address_of_m_DissolveColor_25();
		float L_12 = L_11->get_g_1();
		NullCheck(L_10);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_10, __this, 5, L_12, /*hidden argument*/NULL);
		// paramTex.SetData(this, 6, m_DissolveColor.b); // param2.z : blue
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_13;
		L_13 = VirtFuncInvoker0< ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * >::Invoke(28 /* Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex() */, __this);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * L_14 = __this->get_address_of_m_DissolveColor_25();
		float L_15 = L_14->get_b_2();
		NullCheck(L_13);
		ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F(L_13, __this, 6, L_15, /*hidden argument*/NULL);
	}

IL_008a:
	{
		// if (m_PassRayOnHidden)
		bool L_16 = __this->get_m_PassRayOnHidden_26();
		if (!L_16)
		{
			goto IL_00aa;
		}
	}
	{
		// graphic.raycastTarget = 0 < m_EffectFactor;
		Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * L_17;
		L_17 = BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB(__this, /*hidden argument*/NULL);
		float L_18 = __this->get_m_EffectFactor_19();
		NullCheck(L_17);
		VirtActionInvoker1< bool >::Invoke(25 /* System.Void UnityEngine.UI.Graphic::set_raycastTarget(System.Boolean) */, L_17, (bool)((((float)(0.0f)) < ((float)L_18))? 1 : 0));
	}

IL_00aa:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::SetVerticesDirty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_SetVerticesDirty_mCA6B03E3DF89E3B60C50556BA3FFE4403B4733A7 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// base.SetVerticesDirty();
		BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9(__this, /*hidden argument*/NULL);
		// _lastKeepAspectRatio = m_KeepAspectRatio;
		bool L_0 = __this->get_m_KeepAspectRatio_22();
		__this->set__lastKeepAspectRatio_16(L_0);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::OnDidApplyAnimationProperties()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_OnDidApplyAnimationProperties_mD7E02BB3378AAB63D843174CBD5D01D10DDD0423 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	{
		// base.OnDidApplyAnimationProperties();
		BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E(__this, /*hidden argument*/NULL);
		// if (_lastKeepAspectRatio != m_KeepAspectRatio)
		bool L_0 = __this->get__lastKeepAspectRatio_16();
		bool L_1 = __this->get_m_KeepAspectRatio_22();
		if ((((int32_t)L_0) == ((int32_t)L_1)))
		{
			goto IL_001a;
		}
	}
	{
		// SetVerticesDirty();
		VirtActionInvoker0::Invoke(22 /* System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty() */, __this);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect__ctor_mF067FB43A27FD374FCE4DE22891068F0419EC966 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// EffectMode m_EffectMode = EffectMode.Cutoff;
		__this->set_m_EffectMode_18(2);
		// float m_EffectFactor = 0.5f;
		__this->set_m_EffectFactor_19((0.5f));
		// float m_DissolveWidth = 0.5f;
		__this->set_m_DissolveWidth_23((0.5f));
		// float m_DissolveSoftness = 0.5f;
		__this->set_m_DissolveSoftness_24((0.5f));
		// Color m_DissolveColor = new Color(0.0f, 0.25f, 1.0f);
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0;
		memset((&L_0), 0, sizeof(L_0));
		Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494((&L_0), (0.0f), (0.25f), (1.0f), /*hidden argument*/NULL);
		__this->set_m_DissolveColor_25(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(BaseMaterialEffect_t54A21751E04550DD3C0BD5E6EFB4D6B7633C8065_il2cpp_TypeInfo_var);
		BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect__cctor_m29CED01502E9237C081448992B84557A9E6EEA58 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2786830AA9031CD84AE668243586F5295E0F9C48);
		s_Il2CppMethodInitialized = true;
	}
	{
		// private static readonly int k_TransitionTexId = Shader.PropertyToID("_TransitionTex");
		int32_t L_0;
		L_0 = Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F(_stringLiteral2786830AA9031CD84AE668243586F5295E0F9C48, /*hidden argument*/NULL);
		((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->set_k_TransitionTexId_14(L_0);
		// private static readonly ParameterTexture s_ParamTex = new ParameterTexture(8, 128, "_ParamTex");
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_1 = (ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 *)il2cpp_codegen_object_new(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876(L_1, 8, ((int32_t)128), _stringLiteral26B48FFCD5EAA5A4434A840945D2FD232B8A4587, /*hidden argument*/NULL);
		((UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_StaticFields*)il2cpp_codegen_static_fields_for(UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981_il2cpp_TypeInfo_var))->set_s_ParamTex_15(L_1);
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::<Show>b__46_0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_U3CShowU3Eb__46_0_m57FEB42ADD3994430AF16F786501D9B18B5ED71A (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, float ___f0, const RuntimeMethod* method)
{
	{
		// effectPlayer.Play(reset, f => effectFactor = f);
		float L_0 = ___f0;
		UITransitionEffect_set_effectFactor_m6316BCC7814AE16693E998498FE3F199DEFE6BC7(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.UITransitionEffect::<Hide>b__47_0(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UITransitionEffect_U3CHideU3Eb__47_0_mF3CCECEBADA02DE02348B5455B049D700C33B4B9 (UITransitionEffect_tD4E4819B8F4159A629C7CF083B1E14A59D5E9981 * __this, float ___f0, const RuntimeMethod* method)
{
	{
		// effectPlayer.Play(reset, f => effectFactor = 1 - f);
		float L_0 = ___f0;
		UITransitionEffect_set_effectFactor_m6316BCC7814AE16693E998498FE3F199DEFE6BC7(__this, ((float)il2cpp_codegen_subtract((float)(1.0f), (float)L_0)), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.BaseMaterialEffect/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m0B882E2EB74347504170994F853131BB520A2017 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * L_0 = (U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD *)il2cpp_codegen_object_new(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_mFFE62F74902632613ECF890912C74E4243194389(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Coffee.UIEffects.BaseMaterialEffect/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_mFFE62F74902632613ECF890912C74E4243194389 (U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Coffee.UIEffects.BaseMaterialEffect/<>c::<SetShaderVariants>b__15_0(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CU3Ec_U3CSetShaderVariantsU3Eb__15_0_m86A0BD07D9513900697A9A21089709A5AE45B974 (U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * __this, RuntimeObject * ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// var keywords = variants.Where(x => 0 < (int) x)
		RuntimeObject * L_0 = ___x0;
		return (bool)((((int32_t)0) < ((int32_t)((*(int32_t*)((int32_t*)UnBox(L_0, Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var))))))? 1 : 0);
	}
}
// System.String Coffee.UIEffects.BaseMaterialEffect/<>c::<SetShaderVariants>b__15_1(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* U3CU3Ec_U3CSetShaderVariantsU3Eb__15_1_mB4C34D6B741E7313073BF9AA37C633F2A40B3DB3 (U3CU3Ec_tFC58B12DC560B44942D8D4C9B7AB17D8C2590DDD * __this, RuntimeObject * ___x0, const RuntimeMethod* method)
{
	{
		// .Select(x => x.ToString().ToUpper())
		RuntimeObject * L_0 = ___x0;
		NullCheck(L_0);
		String_t* L_1;
		L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		NullCheck(L_1);
		String_t* L_2;
		L_2 = String_ToUpper_m4BC629F8059C3E0C4E3F7C7E04DB50EBB0C1A05A(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.EffectPlayer/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m95D50A34703A638FA30A294E31DA8E9AB89FD3D5 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * L_0 = (U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 *)il2cpp_codegen_object_new(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m6D644EF6639B2C8942C4F547E344568976E93B5C(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m6D644EF6639B2C8942C4F547E344568976E93B5C (U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.EffectPlayer/<>c::<OnEnable>b__7_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3COnEnableU3Eb__7_0_m03524910EABBB0BB877A6F4343AADDF0B2C8B8D2 (U3CU3Ec_t874B5ABEE2AB64D7B0FB9F2E78C982A30FDC0715 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// var count = s_UpdateActions.Count;
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = ((EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields*)il2cpp_codegen_static_fields_for(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var))->get_s_UpdateActions_6();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_inline(L_0, /*hidden argument*/List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_RuntimeMethod_var);
		V_0 = L_1;
		// for (int i = 0; i < count; i++)
		V_1 = 0;
		goto IL_0023;
	}

IL_000f:
	{
		// s_UpdateActions[i].Invoke();
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_2 = ((EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_StaticFields*)il2cpp_codegen_static_fields_for(EffectPlayer_tD45B45C192B086CDBADCB3E881DD000F5E11353C_il2cpp_TypeInfo_var))->get_s_UpdateActions_6();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_4;
		L_4 = List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_RuntimeMethod_var);
		NullCheck(L_4);
		Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_4, /*hidden argument*/NULL);
		// for (int i = 0; i < count; i++)
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0023:
	{
		// for (int i = 0; i < count; i++)
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_000f;
		}
	}
	{
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.GraphicConnector/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m296B372A3F55B194FA47324D2FD76C6B9F46F5C8 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * L_0 = (U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 *)il2cpp_codegen_object_new(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m827FF5CD8B256D37FC61A732312435C6F12041CE(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Coffee.UIEffects.GraphicConnector/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m827FF5CD8B256D37FC61A732312435C6F12041CE (U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Coffee.UIEffects.GraphicConnector/<>c::<AddConnector>b__4_0(Coffee.UIEffects.GraphicConnector,Coffee.UIEffects.GraphicConnector)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t U3CU3Ec_U3CAddConnectorU3Eb__4_0_mC99D9E9096B78F00A3509748193374ACB23CBB91 (U3CU3Ec_t6E1501E5C3B6B98A2056CFF7E567E1DEB13EBCA1 * __this, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___x0, GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * ___y1, const RuntimeMethod* method)
{
	{
		// s_Connectors.Sort((x, y) => y.priority - x.priority);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_0 = ___y1;
		NullCheck(L_0);
		int32_t L_1;
		L_1 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 Coffee.UIEffects.GraphicConnector::get_priority() */, L_0);
		GraphicConnector_t40A26758D7047C26D7AD41A4415A0397AF02EF6F * L_2 = ___x0;
		NullCheck(L_2);
		int32_t L_3;
		L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 Coffee.UIEffects.GraphicConnector::get_priority() */, L_2);
		return ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)L_3));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.MaterialCache/MaterialEntry::Release()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialEntry_Release_mB302360107E7F32A760184279DA7E3D24149498E (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (material)
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_0 = __this->get_material_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// UnityEngine.Object.DestroyImmediate(material, false);
		Material_t8927C00353A72755313F046D0CE85178AE8218EE * L_2 = __this->get_material_0();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_m69AA5B06236FACFF316DDFAD131B2622B397AC49(L_2, (bool)0, /*hidden argument*/NULL);
	}

IL_0019:
	{
		// material = null;
		__this->set_material_0((Material_t8927C00353A72755313F046D0CE85178AE8218EE *)NULL);
		// }
		return;
	}
}
// System.Void Coffee.UIEffects.MaterialCache/MaterialEntry::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialEntry__ctor_m409D7E4C408065F797EFAC708ADD6836DDAD7FC8 (MaterialEntry_t0CC5506BC482C61EDF460C3B0B9995F363AE6DF7 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Coffee.UIEffects.ParameterTexture/<>c::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mABB703F0252422D599BC010492D66F0782FFB526 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * L_0 = (U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B *)il2cpp_codegen_object_new(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m685DAAD421CE9C891A571D72713AD4D024D5B079(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture/<>c::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m685DAAD421CE9C891A571D72713AD4D024D5B079 (U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Coffee.UIEffects.ParameterTexture/<>c::<Initialize>b__16_0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec_U3CInitializeU3Eb__16_0_mD35535E6182BFEEE7882780DE18A230F2611C9E9 (U3CU3Ec_t18A82B87A48FDBD4336C76FC49C9929E4E048D4B * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// var count = updates.Count;
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_0 = ((ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields*)il2cpp_codegen_static_fields_for(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var))->get_updates_8();
		NullCheck(L_0);
		int32_t L_1;
		L_1 = List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_inline(L_0, /*hidden argument*/List_1_get_Count_m62C12145DD437794F8660D2396A00A7B2BA480C5_RuntimeMethod_var);
		V_0 = L_1;
		// for (int i = 0; i < count; i++)
		V_1 = 0;
		goto IL_0023;
	}

IL_000f:
	{
		// updates[i].Invoke();
		List_1_t458734AF850139150AB40DFB2B5D1BCE23178235 * L_2 = ((ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_StaticFields*)il2cpp_codegen_static_fields_for(ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1_il2cpp_TypeInfo_var))->get_updates_8();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * L_4;
		L_4 = List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_inline(L_2, L_3, /*hidden argument*/List_1_get_Item_m9F0A626A47DAE7452E139A6961335BE81507E551_RuntimeMethod_var);
		NullCheck(L_4);
		Action_Invoke_m3FFA5BE3D64F0FF8E1E1CB6F953913FADB5EB89E(L_4, /*hidden argument*/NULL);
		// for (int i = 0; i < count; i++)
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0023:
	{
		// for (int i = 0; i < count; i++)
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_000f;
		}
	}
	{
		// };
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___v0, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___v0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___v0;
		float L_3 = L_2.get_y_3();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_4), L_1, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0015;
	}

IL_0015:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_5 = V_0;
		return L_5;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UIEffect_get_advancedBlur_m37BF17E16B82618676E23CF6338093ECA5AEB4AD_inline (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_AdvancedBlur; }
		bool L_0 = __this->get_m_AdvancedBlur_21();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UIEffect_get_blurMode_m97D9F46E04E8B291EEB1E7ADCAE27842981FA6DD_inline (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_BlurMode; }
		int32_t L_0 = __this->get_m_BlurMode_20();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)((float)L_1/(float)L_2)), ((float)((float)L_4/(float)L_5)), ((float)((float)L_7/(float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_3)), ((float)il2cpp_codegen_multiply((float)L_5, (float)L_7)), ((float)il2cpp_codegen_multiply((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		__this->set_z_4((0.0f));
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___a0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___b1, const RuntimeMethod* method)
{
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___a0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___b1;
		float L_3 = L_2.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___a0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___b1;
		float L_7 = L_6.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_8;
		memset((&L_8), 0, sizeof(L_8));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_8), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0023;
	}

IL_0023:
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_9 = V_0;
		return L_9;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UIGradient_get_direction_m094F3EB30335D4C941FFE1C9CD92A1EA46FFD209_inline (UIGradient_tE0549A2C5DEF239DCFDF55DBBE3F760E95A5E813 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Direction; }
		int32_t L_0 = __this->get_m_Direction_9();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___lhs0, Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rhs1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = ___lhs0;
		float L_1 = L_0.get_x_0();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2 = ___rhs1;
		float L_3 = L_2.get_x_0();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3));
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_4 = ___lhs0;
		float L_5 = L_4.get_y_1();
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_6 = ___rhs1;
		float L_7 = L_6.get_y_1();
		V_1 = ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7));
		float L_8 = V_0;
		float L_9 = V_0;
		float L_10 = V_1;
		float L_11 = V_1;
		V_2 = (bool)((((float)((float)il2cpp_codegen_add((float)((float)il2cpp_codegen_multiply((float)L_8, (float)L_9)), (float)((float)il2cpp_codegen_multiply((float)L_10, (float)L_11))))) < ((float)(9.99999944E-11f)))? 1 : 0);
		goto IL_002e;
	}

IL_002e:
	{
		bool L_12 = V_2;
		return L_12;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * ___value0, const RuntimeMethod* method)
{
	{
		// public ParameterTexture paramTex { get; private set; }
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = ___value0;
		__this->set_U3CparamTexU3Ek__BackingField_19(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// public ParameterTexture paramTex { get; private set; }
		ParameterTexture_t6FB2DB42ED1413AB2693DA73306F5F83602867E1 * L_0 = __this->get_U3CparamTexU3Ek__BackingField_19();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float UIEffect_get_effectFactor_m00273C0BB3B80F99858764F8D80FD8D1BD7CB92E_inline (UIEffect_t3C48D70FA75BA4B6B51BFF7694BBEF207EDEBD21 * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectFactor; }
		float L_0 = __this->get_m_EffectFactor_15();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectColor; }
		Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  L_0 = __this->get_m_EffectColor_14();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  UIShadow_get_effectDistance_m2AB1626DC40D34C0D91FF336342F938195B201EE_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_EffectDistance; }
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_0 = __this->get_m_EffectDistance_15();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t UIShadow_get_style_mCAA0D48FB84802EBB12777BD82587CF6B1A20379_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_Style; }
		int32_t L_0 = __this->get_m_Style_13();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR bool UIShadow_get_useGraphicAlpha_mEC09F1BCF8D368129B771A94C129F048C7C0CE1E_inline (UIShadow_t144D12B910E68F2B57D1CAF9D831527EFEF22FEB * __this, const RuntimeMethod* method)
{
	{
		// get { return m_UseGraphicAlpha; }
		bool L_0 = __this->get_m_UseGraphicAlpha_16();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3_Set_m12EA2C6DF9F94ABD0462F422A20959A53EED90D7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___newX0, float ___newY1, float ___newZ2, const RuntimeMethod* method)
{
	{
		float L_0 = ___newX0;
		__this->set_x_2(L_0);
		float L_1 = ___newY1;
		__this->set_y_3(L_1);
		float L_2 = ___newZ2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return (RuntimeObject *)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Stack_1_get_Count_m2CC61A8B5F6A9FD28F252AABAC91172588412CA3_gshared_inline (Stack_1_tC6C298385D16F10F391B84280D21FE059A45CC55 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_1();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mE3884E9A0B85F35318E1993493702C464FE0C2C6_gshared_inline (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  List_1_get_Item_m391D3A419FD1FC4B147D9C35968DE52270549DEE_gshared_inline (List_1_t8907FD137E854241E2657BF53E6CEFF7370FAC5F * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* L_2 = (UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A*)__this->get__items_1();
		int32_t L_3 = ___index0;
		UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A*)L_2, (int32_t)L_3);
		return (UIVertex_tD94AAC5F0B42DBC441AAA8ADBFCFF9E5C320C03A )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m5D847939ABB9A78203B062CAFFE975792174D00F_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
