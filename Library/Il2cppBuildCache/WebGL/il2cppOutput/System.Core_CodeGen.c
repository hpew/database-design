﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Action`11::.ctor(System.Object,System.IntPtr)
// 0x00000003 System.Void System.Action`11::Invoke(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11)
// 0x00000004 System.IAsyncResult System.Action`11::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,System.AsyncCallback,System.Object)
// 0x00000005 System.Void System.Action`11::EndInvoke(System.IAsyncResult)
// 0x00000006 System.Void System.Func`12::.ctor(System.Object,System.IntPtr)
// 0x00000007 TResult System.Func`12::Invoke(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11)
// 0x00000008 System.IAsyncResult System.Func`12::BeginInvoke(T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,System.AsyncCallback,System.Object)
// 0x00000009 TResult System.Func`12::EndInvoke(System.IAsyncResult)
// 0x0000000A System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x0000000B System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x0000000C System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x0000000E System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x0000000F System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x00000010 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x00000011 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x00000012 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x00000014 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x00000016 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x00000017 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x0000001A System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x0000001B System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x0000001C System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x0000001E System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x00000020 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x00000021 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x00000022 System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x00000023 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x00000024 System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x00000025 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x00000026 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000028 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000029 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x0000002A System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x0000002B System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x0000002C System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x0000002D System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x0000002E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x0000002F System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x00000030 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x00000031 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x00000032 System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x00000033 System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x00000034 System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x00000035 System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x00000036 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000037 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000038 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mDB56C9FA4C344A86553D6AFB66D10B290302C53A (void);
// 0x00000039 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x0000003A System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x0000003B System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_m51A0560ABF374B66CF6D1208DAF27C4CBAD9AABA (void);
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x0000003E System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x0000003F System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000046 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000047 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000048 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000049 System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000004A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000004E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Except(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ExceptIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000050 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Reverse(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000051 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ReverseIterator(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000052 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000053 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000054 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000055 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x00000057 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x00000058 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000059 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x0000005A TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005B TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005C TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005D TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005E TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005F TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000060 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000061 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000062 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000063 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Repeat(TResult,System.Int32)
// 0x00000064 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::RepeatIterator(TResult,System.Int32)
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000066 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000067 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000068 System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000069 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000006A System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000006B System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000006C System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5 (void);
// 0x0000006D System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<System.Single>)
extern void Enumerable_Average_mCC66070D3714E5082DD32AAB83A2E6F158241BB6 (void);
// 0x0000006E System.Single System.Linq.Enumerable::Average(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Single>)
// 0x0000006F System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000070 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000071 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000072 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000073 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000074 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000075 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000076 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000077 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000078 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000079 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000007A System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000007B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000007C System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x0000007D System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x0000007E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000007F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000080 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000081 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000082 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000083 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000084 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000085 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000086 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000087 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000088 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000089 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000008A System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000008B System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x0000008C System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x0000008D System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000008E System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000008F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000090 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000091 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000092 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000093 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000094 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000095 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000096 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x00000097 System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000098 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000099 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000009A System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x0000009B System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000009C System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x0000009D TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000009E System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000009F System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x000000A0 System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x000000A1 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x000000A2 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x000000A3 TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000A4 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x000000A5 System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x000000A6 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000A7 System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A8 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x000000A9 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x000000AA System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x000000AB System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x000000AC TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000AD System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x000000AE System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x000000AF System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000B0 System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B1 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x000000B2 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x000000B3 System.Boolean System.Linq.Enumerable/<SkipIterator>d__31`1::MoveNext()
// 0x000000B4 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::<>m__Finally1()
// 0x000000B5 TSource System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000B6 System.Void System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x000000B7 System.Object System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x000000B8 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000B9 System.Collections.IEnumerator System.Linq.Enumerable/<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BA System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x000000BB System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x000000BC System.Boolean System.Linq.Enumerable/<ConcatIterator>d__59`1::MoveNext()
// 0x000000BD System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally1()
// 0x000000BE System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::<>m__Finally2()
// 0x000000BF TSource System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000C0 System.Void System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x000000C1 System.Object System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x000000C2 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000C3 System.Collections.IEnumerator System.Linq.Enumerable/<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C4 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x000000C5 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x000000C6 System.Boolean System.Linq.Enumerable/<DistinctIterator>d__68`1::MoveNext()
// 0x000000C7 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::<>m__Finally1()
// 0x000000C8 TSource System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000C9 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x000000CA System.Object System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x000000CB System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000CC System.Collections.IEnumerator System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CD System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::.ctor(System.Int32)
// 0x000000CE System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::System.IDisposable.Dispose()
// 0x000000CF System.Boolean System.Linq.Enumerable/<ExceptIterator>d__77`1::MoveNext()
// 0x000000D0 System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::<>m__Finally1()
// 0x000000D1 TSource System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000D2 System.Void System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.IEnumerator.Reset()
// 0x000000D3 System.Object System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.IEnumerator.get_Current()
// 0x000000D4 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000D5 System.Collections.IEnumerator System.Linq.Enumerable/<ExceptIterator>d__77`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D6 System.Void System.Linq.Enumerable/<ReverseIterator>d__79`1::.ctor(System.Int32)
// 0x000000D7 System.Void System.Linq.Enumerable/<ReverseIterator>d__79`1::System.IDisposable.Dispose()
// 0x000000D8 System.Boolean System.Linq.Enumerable/<ReverseIterator>d__79`1::MoveNext()
// 0x000000D9 TSource System.Linq.Enumerable/<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000DA System.Void System.Linq.Enumerable/<ReverseIterator>d__79`1::System.Collections.IEnumerator.Reset()
// 0x000000DB System.Object System.Linq.Enumerable/<ReverseIterator>d__79`1::System.Collections.IEnumerator.get_Current()
// 0x000000DC System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<ReverseIterator>d__79`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000DD System.Collections.IEnumerator System.Linq.Enumerable/<ReverseIterator>d__79`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000DE System.Void System.Linq.Enumerable/<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x000000DF System.Void System.Linq.Enumerable/<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x000000E0 System.Boolean System.Linq.Enumerable/<OfTypeIterator>d__97`1::MoveNext()
// 0x000000E1 System.Void System.Linq.Enumerable/<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x000000E2 TResult System.Linq.Enumerable/<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000E3 System.Void System.Linq.Enumerable/<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x000000E4 System.Object System.Linq.Enumerable/<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x000000E5 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000E6 System.Collections.IEnumerator System.Linq.Enumerable/<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000E7 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000E8 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000E9 System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x000000EA System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x000000EB TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000EC System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000ED System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000EE System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000EF System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F0 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::.ctor(System.Int32)
// 0x000000F1 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::System.IDisposable.Dispose()
// 0x000000F2 System.Boolean System.Linq.Enumerable/<RepeatIterator>d__117`1::MoveNext()
// 0x000000F3 TResult System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000F4 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerator.Reset()
// 0x000000F5 System.Object System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerator.get_Current()
// 0x000000F6 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000F7 System.Collections.IEnumerator System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F8 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000F9 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000FA System.Void System.Linq.IdentityFunction`1/<>c::.cctor()
// 0x000000FB System.Void System.Linq.IdentityFunction`1/<>c::.ctor()
// 0x000000FC TElement System.Linq.IdentityFunction`1/<>c::<get_Instance>b__1_0(TElement)
// 0x000000FD System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000FE TKey System.Linq.IGrouping`2::get_Key()
// 0x000000FF System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000100 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000101 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x00000102 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000103 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x00000104 System.Linq.Lookup`2/Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x00000105 System.Void System.Linq.Lookup`2::Resize()
// 0x00000106 System.Void System.Linq.Lookup`2/Grouping::Add(TElement)
// 0x00000107 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2/Grouping::GetEnumerator()
// 0x00000108 System.Collections.IEnumerator System.Linq.Lookup`2/Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x00000109 TKey System.Linq.Lookup`2/Grouping::get_Key()
// 0x0000010A System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x0000010B System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x0000010C System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x0000010D System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x0000010E System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x0000010F System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x00000110 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x00000111 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x00000112 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x00000113 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x00000114 TElement System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x00000115 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x00000116 System.Void System.Linq.Lookup`2/Grouping::.ctor()
// 0x00000117 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::.ctor(System.Int32)
// 0x00000118 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x00000119 System.Boolean System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::MoveNext()
// 0x0000011A TElement System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000011B System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x0000011C System.Object System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x0000011D System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::.ctor(System.Int32)
// 0x0000011E System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x0000011F System.Boolean System.Linq.Lookup`2/<GetEnumerator>d__12::MoveNext()
// 0x00000120 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x00000121 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x00000122 System.Object System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x00000123 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000124 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000125 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000126 System.Void System.Linq.Set`1::Resize()
// 0x00000127 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000128 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000129 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x0000012A System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x0000012B System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000012C System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000012D System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000012E System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000012F System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000130 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000131 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000132 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x00000133 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000134 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000135 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000136 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000137 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000138 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000139 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000013A System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000013B System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000013C System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000013D System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000013E System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x0000013F System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000140 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000141 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000142 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000143 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000144 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000145 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000146 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000147 System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x00000148 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000149 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000014A System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x0000014B System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000014C System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000014D System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000014E System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000014F System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000150 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000151 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000152 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000153 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000154 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000155 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000156 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000157 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000158 System.Int32 System.Collections.Generic.HashSet`1::RemoveWhere(System.Predicate`1<T>)
// 0x00000159 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000015A System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x0000015B System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x0000015C System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x0000015D System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x0000015E System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x0000015F System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000160 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000161 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x00000162 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000163 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000164 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000165 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000166 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000167 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[359] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneElement_mDB56C9FA4C344A86553D6AFB66D10B290302C53A,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	Error_NotSupported_m51A0560ABF374B66CF6D1208DAF27C4CBAD9AABA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5,
	Enumerable_Average_mCC66070D3714E5082DD32AAB83A2E6F158241BB6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[359] = 
{
	5172,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3786,
	3708,
	3723,
	3119,
	3723,
	3119,
	3708,
	3105,
	3708,
	3105,
	3708,
	3105,
	3723,
	1389,
	3723,
	1389,
	3142,
	3786,
	3786,
	3786,
	3786,
	3786,
	1389,
	1389,
	3723,
	3119,
	3723,
	3119,
	3708,
	3105,
	3708,
	3708,
	3105,
	3708,
	3105,
	3723,
	3723,
	3142,
	769,
	1847,
	2337,
	1131,
	1131,
	5339,
	5172,
	5172,
	5316,
	5316,
	5316,
	5316,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5113,
	5225,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[92] = 
{
	{ 0x0200000A, { 134, 4 } },
	{ 0x0200000B, { 138, 9 } },
	{ 0x0200000C, { 149, 7 } },
	{ 0x0200000D, { 158, 10 } },
	{ 0x0200000E, { 170, 11 } },
	{ 0x0200000F, { 184, 9 } },
	{ 0x02000010, { 196, 12 } },
	{ 0x02000011, { 211, 1 } },
	{ 0x02000012, { 212, 2 } },
	{ 0x02000013, { 214, 12 } },
	{ 0x02000014, { 226, 8 } },
	{ 0x02000015, { 234, 8 } },
	{ 0x02000016, { 242, 9 } },
	{ 0x02000017, { 251, 11 } },
	{ 0x02000018, { 262, 11 } },
	{ 0x02000019, { 273, 6 } },
	{ 0x0200001A, { 279, 6 } },
	{ 0x0200001B, { 285, 6 } },
	{ 0x0200001C, { 291, 4 } },
	{ 0x0200001D, { 295, 2 } },
	{ 0x0200001E, { 297, 4 } },
	{ 0x0200001F, { 301, 3 } },
	{ 0x02000022, { 304, 17 } },
	{ 0x02000023, { 325, 5 } },
	{ 0x02000024, { 330, 1 } },
	{ 0x02000026, { 331, 8 } },
	{ 0x02000028, { 339, 4 } },
	{ 0x02000029, { 343, 3 } },
	{ 0x0200002A, { 348, 5 } },
	{ 0x0200002B, { 353, 7 } },
	{ 0x0200002C, { 360, 3 } },
	{ 0x0200002D, { 363, 7 } },
	{ 0x0200002E, { 370, 4 } },
	{ 0x0200002F, { 374, 36 } },
	{ 0x02000031, { 410, 2 } },
	{ 0x0600003C, { 0, 10 } },
	{ 0x0600003D, { 10, 10 } },
	{ 0x0600003E, { 20, 5 } },
	{ 0x0600003F, { 25, 5 } },
	{ 0x06000040, { 30, 1 } },
	{ 0x06000041, { 31, 2 } },
	{ 0x06000042, { 33, 1 } },
	{ 0x06000043, { 34, 2 } },
	{ 0x06000044, { 36, 1 } },
	{ 0x06000045, { 37, 2 } },
	{ 0x06000046, { 39, 2 } },
	{ 0x06000047, { 41, 2 } },
	{ 0x06000048, { 43, 1 } },
	{ 0x06000049, { 44, 4 } },
	{ 0x0600004A, { 48, 1 } },
	{ 0x0600004B, { 49, 2 } },
	{ 0x0600004C, { 51, 1 } },
	{ 0x0600004D, { 52, 2 } },
	{ 0x0600004E, { 54, 1 } },
	{ 0x0600004F, { 55, 2 } },
	{ 0x06000050, { 57, 1 } },
	{ 0x06000051, { 58, 2 } },
	{ 0x06000052, { 60, 3 } },
	{ 0x06000053, { 63, 2 } },
	{ 0x06000054, { 65, 1 } },
	{ 0x06000055, { 66, 7 } },
	{ 0x06000056, { 73, 1 } },
	{ 0x06000057, { 74, 2 } },
	{ 0x06000058, { 76, 2 } },
	{ 0x06000059, { 78, 2 } },
	{ 0x0600005A, { 80, 4 } },
	{ 0x0600005B, { 84, 4 } },
	{ 0x0600005C, { 88, 3 } },
	{ 0x0600005D, { 91, 4 } },
	{ 0x0600005E, { 95, 4 } },
	{ 0x0600005F, { 99, 4 } },
	{ 0x06000060, { 103, 4 } },
	{ 0x06000061, { 107, 3 } },
	{ 0x06000062, { 110, 3 } },
	{ 0x06000063, { 113, 1 } },
	{ 0x06000064, { 114, 2 } },
	{ 0x06000065, { 116, 1 } },
	{ 0x06000066, { 117, 1 } },
	{ 0x06000067, { 118, 3 } },
	{ 0x06000068, { 121, 3 } },
	{ 0x06000069, { 124, 2 } },
	{ 0x0600006A, { 126, 2 } },
	{ 0x0600006B, { 128, 5 } },
	{ 0x0600006E, { 133, 1 } },
	{ 0x0600007E, { 147, 2 } },
	{ 0x06000083, { 156, 2 } },
	{ 0x06000088, { 168, 2 } },
	{ 0x0600008E, { 181, 3 } },
	{ 0x06000093, { 193, 3 } },
	{ 0x06000098, { 208, 3 } },
	{ 0x060000FF, { 321, 4 } },
	{ 0x0600012E, { 346, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[412] = 
{
	{ (Il2CppRGCTXDataType)2, 13351 },
	{ (Il2CppRGCTXDataType)3, 21977 },
	{ (Il2CppRGCTXDataType)2, 16966 },
	{ (Il2CppRGCTXDataType)2, 16259 },
	{ (Il2CppRGCTXDataType)3, 38067 },
	{ (Il2CppRGCTXDataType)2, 13546 },
	{ (Il2CppRGCTXDataType)2, 16266 },
	{ (Il2CppRGCTXDataType)3, 38227 },
	{ (Il2CppRGCTXDataType)2, 16261 },
	{ (Il2CppRGCTXDataType)3, 38105 },
	{ (Il2CppRGCTXDataType)2, 13352 },
	{ (Il2CppRGCTXDataType)3, 21978 },
	{ (Il2CppRGCTXDataType)2, 17021 },
	{ (Il2CppRGCTXDataType)2, 16279 },
	{ (Il2CppRGCTXDataType)3, 38294 },
	{ (Il2CppRGCTXDataType)2, 13572 },
	{ (Il2CppRGCTXDataType)2, 16287 },
	{ (Il2CppRGCTXDataType)3, 39366 },
	{ (Il2CppRGCTXDataType)2, 16283 },
	{ (Il2CppRGCTXDataType)3, 38782 },
	{ (Il2CppRGCTXDataType)2, 5997 },
	{ (Il2CppRGCTXDataType)3, 1691 },
	{ (Il2CppRGCTXDataType)3, 1692 },
	{ (Il2CppRGCTXDataType)2, 8638 },
	{ (Il2CppRGCTXDataType)3, 18144 },
	{ (Il2CppRGCTXDataType)2, 6019 },
	{ (Il2CppRGCTXDataType)3, 1835 },
	{ (Il2CppRGCTXDataType)3, 1836 },
	{ (Il2CppRGCTXDataType)2, 8719 },
	{ (Il2CppRGCTXDataType)3, 18161 },
	{ (Il2CppRGCTXDataType)3, 44391 },
	{ (Il2CppRGCTXDataType)2, 6159 },
	{ (Il2CppRGCTXDataType)3, 3041 },
	{ (Il2CppRGCTXDataType)3, 44415 },
	{ (Il2CppRGCTXDataType)2, 6171 },
	{ (Il2CppRGCTXDataType)3, 3114 },
	{ (Il2CppRGCTXDataType)3, 44410 },
	{ (Il2CppRGCTXDataType)2, 6166 },
	{ (Il2CppRGCTXDataType)3, 3073 },
	{ (Il2CppRGCTXDataType)2, 14801 },
	{ (Il2CppRGCTXDataType)3, 31809 },
	{ (Il2CppRGCTXDataType)2, 14802 },
	{ (Il2CppRGCTXDataType)3, 31810 },
	{ (Il2CppRGCTXDataType)3, 20099 },
	{ (Il2CppRGCTXDataType)3, 20184 },
	{ (Il2CppRGCTXDataType)2, 13292 },
	{ (Il2CppRGCTXDataType)2, 9462 },
	{ (Il2CppRGCTXDataType)3, 19627 },
	{ (Il2CppRGCTXDataType)3, 44196 },
	{ (Il2CppRGCTXDataType)2, 6073 },
	{ (Il2CppRGCTXDataType)3, 2474 },
	{ (Il2CppRGCTXDataType)3, 44225 },
	{ (Il2CppRGCTXDataType)2, 6083 },
	{ (Il2CppRGCTXDataType)3, 2518 },
	{ (Il2CppRGCTXDataType)3, 44237 },
	{ (Il2CppRGCTXDataType)2, 6089 },
	{ (Il2CppRGCTXDataType)3, 2569 },
	{ (Il2CppRGCTXDataType)3, 44307 },
	{ (Il2CppRGCTXDataType)2, 6151 },
	{ (Il2CppRGCTXDataType)3, 3004 },
	{ (Il2CppRGCTXDataType)2, 7125 },
	{ (Il2CppRGCTXDataType)3, 7814 },
	{ (Il2CppRGCTXDataType)3, 7815 },
	{ (Il2CppRGCTXDataType)2, 13547 },
	{ (Il2CppRGCTXDataType)3, 23412 },
	{ (Il2CppRGCTXDataType)3, 44455 },
	{ (Il2CppRGCTXDataType)2, 7590 },
	{ (Il2CppRGCTXDataType)3, 9972 },
	{ (Il2CppRGCTXDataType)2, 9950 },
	{ (Il2CppRGCTXDataType)2, 10267 },
	{ (Il2CppRGCTXDataType)3, 18159 },
	{ (Il2CppRGCTXDataType)3, 18160 },
	{ (Il2CppRGCTXDataType)3, 9973 },
	{ (Il2CppRGCTXDataType)3, 44262 },
	{ (Il2CppRGCTXDataType)2, 6107 },
	{ (Il2CppRGCTXDataType)3, 2752 },
	{ (Il2CppRGCTXDataType)2, 9839 },
	{ (Il2CppRGCTXDataType)3, 43986 },
	{ (Il2CppRGCTXDataType)2, 6067 },
	{ (Il2CppRGCTXDataType)3, 2433 },
	{ (Il2CppRGCTXDataType)2, 10929 },
	{ (Il2CppRGCTXDataType)2, 9547 },
	{ (Il2CppRGCTXDataType)2, 9863 },
	{ (Il2CppRGCTXDataType)2, 10252 },
	{ (Il2CppRGCTXDataType)2, 10930 },
	{ (Il2CppRGCTXDataType)2, 9548 },
	{ (Il2CppRGCTXDataType)2, 9864 },
	{ (Il2CppRGCTXDataType)2, 10253 },
	{ (Il2CppRGCTXDataType)2, 9865 },
	{ (Il2CppRGCTXDataType)2, 10254 },
	{ (Il2CppRGCTXDataType)3, 18145 },
	{ (Il2CppRGCTXDataType)2, 10931 },
	{ (Il2CppRGCTXDataType)2, 9549 },
	{ (Il2CppRGCTXDataType)2, 9866 },
	{ (Il2CppRGCTXDataType)2, 10255 },
	{ (Il2CppRGCTXDataType)2, 10932 },
	{ (Il2CppRGCTXDataType)2, 9550 },
	{ (Il2CppRGCTXDataType)2, 9867 },
	{ (Il2CppRGCTXDataType)2, 10256 },
	{ (Il2CppRGCTXDataType)2, 10933 },
	{ (Il2CppRGCTXDataType)2, 9551 },
	{ (Il2CppRGCTXDataType)2, 9868 },
	{ (Il2CppRGCTXDataType)2, 10257 },
	{ (Il2CppRGCTXDataType)2, 10934 },
	{ (Il2CppRGCTXDataType)2, 9552 },
	{ (Il2CppRGCTXDataType)2, 9869 },
	{ (Il2CppRGCTXDataType)2, 10258 },
	{ (Il2CppRGCTXDataType)2, 9870 },
	{ (Il2CppRGCTXDataType)2, 10259 },
	{ (Il2CppRGCTXDataType)3, 18146 },
	{ (Il2CppRGCTXDataType)2, 10928 },
	{ (Il2CppRGCTXDataType)2, 9862 },
	{ (Il2CppRGCTXDataType)2, 10251 },
	{ (Il2CppRGCTXDataType)3, 44301 },
	{ (Il2CppRGCTXDataType)2, 6149 },
	{ (Il2CppRGCTXDataType)3, 2993 },
	{ (Il2CppRGCTXDataType)2, 7983 },
	{ (Il2CppRGCTXDataType)2, 9835 },
	{ (Il2CppRGCTXDataType)2, 9836 },
	{ (Il2CppRGCTXDataType)2, 10249 },
	{ (Il2CppRGCTXDataType)3, 18143 },
	{ (Il2CppRGCTXDataType)2, 9834 },
	{ (Il2CppRGCTXDataType)2, 10248 },
	{ (Il2CppRGCTXDataType)3, 18142 },
	{ (Il2CppRGCTXDataType)2, 9546 },
	{ (Il2CppRGCTXDataType)2, 9860 },
	{ (Il2CppRGCTXDataType)2, 9545 },
	{ (Il2CppRGCTXDataType)3, 44206 },
	{ (Il2CppRGCTXDataType)3, 16335 },
	{ (Il2CppRGCTXDataType)2, 8106 },
	{ (Il2CppRGCTXDataType)2, 9838 },
	{ (Il2CppRGCTXDataType)2, 10250 },
	{ (Il2CppRGCTXDataType)2, 10478 },
	{ (Il2CppRGCTXDataType)3, 44310 },
	{ (Il2CppRGCTXDataType)3, 21979 },
	{ (Il2CppRGCTXDataType)3, 21981 },
	{ (Il2CppRGCTXDataType)2, 2236 },
	{ (Il2CppRGCTXDataType)3, 21980 },
	{ (Il2CppRGCTXDataType)3, 21989 },
	{ (Il2CppRGCTXDataType)2, 13355 },
	{ (Il2CppRGCTXDataType)2, 16262 },
	{ (Il2CppRGCTXDataType)3, 38106 },
	{ (Il2CppRGCTXDataType)3, 21990 },
	{ (Il2CppRGCTXDataType)2, 10017 },
	{ (Il2CppRGCTXDataType)2, 10314 },
	{ (Il2CppRGCTXDataType)3, 18180 },
	{ (Il2CppRGCTXDataType)3, 43989 },
	{ (Il2CppRGCTXDataType)2, 16284 },
	{ (Il2CppRGCTXDataType)3, 38783 },
	{ (Il2CppRGCTXDataType)3, 21982 },
	{ (Il2CppRGCTXDataType)2, 13354 },
	{ (Il2CppRGCTXDataType)2, 16260 },
	{ (Il2CppRGCTXDataType)3, 38068 },
	{ (Il2CppRGCTXDataType)3, 18179 },
	{ (Il2CppRGCTXDataType)3, 21983 },
	{ (Il2CppRGCTXDataType)3, 43988 },
	{ (Il2CppRGCTXDataType)2, 16280 },
	{ (Il2CppRGCTXDataType)3, 38295 },
	{ (Il2CppRGCTXDataType)3, 21996 },
	{ (Il2CppRGCTXDataType)2, 13356 },
	{ (Il2CppRGCTXDataType)2, 16267 },
	{ (Il2CppRGCTXDataType)3, 38228 },
	{ (Il2CppRGCTXDataType)3, 23552 },
	{ (Il2CppRGCTXDataType)3, 14638 },
	{ (Il2CppRGCTXDataType)3, 18181 },
	{ (Il2CppRGCTXDataType)3, 14637 },
	{ (Il2CppRGCTXDataType)3, 21997 },
	{ (Il2CppRGCTXDataType)3, 43990 },
	{ (Il2CppRGCTXDataType)2, 16288 },
	{ (Il2CppRGCTXDataType)3, 39367 },
	{ (Il2CppRGCTXDataType)3, 22010 },
	{ (Il2CppRGCTXDataType)2, 13358 },
	{ (Il2CppRGCTXDataType)2, 16286 },
	{ (Il2CppRGCTXDataType)3, 38785 },
	{ (Il2CppRGCTXDataType)3, 22011 },
	{ (Il2CppRGCTXDataType)2, 10020 },
	{ (Il2CppRGCTXDataType)2, 10317 },
	{ (Il2CppRGCTXDataType)3, 18185 },
	{ (Il2CppRGCTXDataType)3, 18184 },
	{ (Il2CppRGCTXDataType)2, 16264 },
	{ (Il2CppRGCTXDataType)3, 38108 },
	{ (Il2CppRGCTXDataType)3, 44011 },
	{ (Il2CppRGCTXDataType)2, 16285 },
	{ (Il2CppRGCTXDataType)3, 38784 },
	{ (Il2CppRGCTXDataType)3, 22003 },
	{ (Il2CppRGCTXDataType)2, 13357 },
	{ (Il2CppRGCTXDataType)2, 16282 },
	{ (Il2CppRGCTXDataType)3, 38297 },
	{ (Il2CppRGCTXDataType)3, 18183 },
	{ (Il2CppRGCTXDataType)3, 18182 },
	{ (Il2CppRGCTXDataType)3, 22004 },
	{ (Il2CppRGCTXDataType)2, 16263 },
	{ (Il2CppRGCTXDataType)3, 38107 },
	{ (Il2CppRGCTXDataType)3, 44010 },
	{ (Il2CppRGCTXDataType)2, 16281 },
	{ (Il2CppRGCTXDataType)3, 38296 },
	{ (Il2CppRGCTXDataType)3, 22017 },
	{ (Il2CppRGCTXDataType)2, 13359 },
	{ (Il2CppRGCTXDataType)2, 16290 },
	{ (Il2CppRGCTXDataType)3, 39369 },
	{ (Il2CppRGCTXDataType)3, 23553 },
	{ (Il2CppRGCTXDataType)3, 14640 },
	{ (Il2CppRGCTXDataType)3, 18187 },
	{ (Il2CppRGCTXDataType)3, 18186 },
	{ (Il2CppRGCTXDataType)3, 14639 },
	{ (Il2CppRGCTXDataType)3, 22018 },
	{ (Il2CppRGCTXDataType)2, 16265 },
	{ (Il2CppRGCTXDataType)3, 38109 },
	{ (Il2CppRGCTXDataType)3, 44012 },
	{ (Il2CppRGCTXDataType)2, 16289 },
	{ (Il2CppRGCTXDataType)3, 39368 },
	{ (Il2CppRGCTXDataType)3, 18176 },
	{ (Il2CppRGCTXDataType)3, 18177 },
	{ (Il2CppRGCTXDataType)3, 18220 },
	{ (Il2CppRGCTXDataType)3, 3044 },
	{ (Il2CppRGCTXDataType)3, 3043 },
	{ (Il2CppRGCTXDataType)2, 10006 },
	{ (Il2CppRGCTXDataType)2, 10306 },
	{ (Il2CppRGCTXDataType)3, 18178 },
	{ (Il2CppRGCTXDataType)2, 10114 },
	{ (Il2CppRGCTXDataType)2, 10388 },
	{ (Il2CppRGCTXDataType)3, 3046 },
	{ (Il2CppRGCTXDataType)2, 3554 },
	{ (Il2CppRGCTXDataType)2, 6160 },
	{ (Il2CppRGCTXDataType)3, 3042 },
	{ (Il2CppRGCTXDataType)3, 3045 },
	{ (Il2CppRGCTXDataType)3, 3116 },
	{ (Il2CppRGCTXDataType)2, 10012 },
	{ (Il2CppRGCTXDataType)2, 10310 },
	{ (Il2CppRGCTXDataType)3, 3118 },
	{ (Il2CppRGCTXDataType)2, 2234 },
	{ (Il2CppRGCTXDataType)2, 6172 },
	{ (Il2CppRGCTXDataType)3, 3115 },
	{ (Il2CppRGCTXDataType)3, 3117 },
	{ (Il2CppRGCTXDataType)3, 3075 },
	{ (Il2CppRGCTXDataType)2, 10009 },
	{ (Il2CppRGCTXDataType)2, 10308 },
	{ (Il2CppRGCTXDataType)3, 3077 },
	{ (Il2CppRGCTXDataType)2, 2232 },
	{ (Il2CppRGCTXDataType)2, 6167 },
	{ (Il2CppRGCTXDataType)3, 3074 },
	{ (Il2CppRGCTXDataType)3, 3076 },
	{ (Il2CppRGCTXDataType)3, 2476 },
	{ (Il2CppRGCTXDataType)3, 2477 },
	{ (Il2CppRGCTXDataType)2, 9992 },
	{ (Il2CppRGCTXDataType)2, 10297 },
	{ (Il2CppRGCTXDataType)3, 2479 },
	{ (Il2CppRGCTXDataType)2, 2218 },
	{ (Il2CppRGCTXDataType)2, 6074 },
	{ (Il2CppRGCTXDataType)3, 2475 },
	{ (Il2CppRGCTXDataType)3, 2478 },
	{ (Il2CppRGCTXDataType)3, 2520 },
	{ (Il2CppRGCTXDataType)2, 15342 },
	{ (Il2CppRGCTXDataType)3, 34094 },
	{ (Il2CppRGCTXDataType)2, 9995 },
	{ (Il2CppRGCTXDataType)2, 10299 },
	{ (Il2CppRGCTXDataType)3, 34095 },
	{ (Il2CppRGCTXDataType)3, 2522 },
	{ (Il2CppRGCTXDataType)2, 2220 },
	{ (Il2CppRGCTXDataType)2, 6084 },
	{ (Il2CppRGCTXDataType)3, 2519 },
	{ (Il2CppRGCTXDataType)3, 2521 },
	{ (Il2CppRGCTXDataType)3, 2571 },
	{ (Il2CppRGCTXDataType)2, 15344 },
	{ (Il2CppRGCTXDataType)3, 34096 },
	{ (Il2CppRGCTXDataType)2, 9998 },
	{ (Il2CppRGCTXDataType)2, 10301 },
	{ (Il2CppRGCTXDataType)3, 34097 },
	{ (Il2CppRGCTXDataType)3, 2573 },
	{ (Il2CppRGCTXDataType)2, 2222 },
	{ (Il2CppRGCTXDataType)2, 6090 },
	{ (Il2CppRGCTXDataType)3, 2570 },
	{ (Il2CppRGCTXDataType)3, 2572 },
	{ (Il2CppRGCTXDataType)2, 7126 },
	{ (Il2CppRGCTXDataType)3, 7816 },
	{ (Il2CppRGCTXDataType)2, 2229 },
	{ (Il2CppRGCTXDataType)2, 6152 },
	{ (Il2CppRGCTXDataType)3, 3005 },
	{ (Il2CppRGCTXDataType)3, 3006 },
	{ (Il2CppRGCTXDataType)3, 2754 },
	{ (Il2CppRGCTXDataType)2, 2224 },
	{ (Il2CppRGCTXDataType)3, 2756 },
	{ (Il2CppRGCTXDataType)2, 6108 },
	{ (Il2CppRGCTXDataType)3, 2753 },
	{ (Il2CppRGCTXDataType)3, 2755 },
	{ (Il2CppRGCTXDataType)3, 2435 },
	{ (Il2CppRGCTXDataType)2, 2216 },
	{ (Il2CppRGCTXDataType)3, 2437 },
	{ (Il2CppRGCTXDataType)2, 6068 },
	{ (Il2CppRGCTXDataType)3, 2434 },
	{ (Il2CppRGCTXDataType)3, 2436 },
	{ (Il2CppRGCTXDataType)2, 2226 },
	{ (Il2CppRGCTXDataType)2, 6150 },
	{ (Il2CppRGCTXDataType)3, 2994 },
	{ (Il2CppRGCTXDataType)3, 2995 },
	{ (Il2CppRGCTXDataType)2, 17037 },
	{ (Il2CppRGCTXDataType)2, 7984 },
	{ (Il2CppRGCTXDataType)2, 5567 },
	{ (Il2CppRGCTXDataType)3, 5 },
	{ (Il2CppRGCTXDataType)2, 8759 },
	{ (Il2CppRGCTXDataType)3, 18166 },
	{ (Il2CppRGCTXDataType)2, 5589 },
	{ (Il2CppRGCTXDataType)3, 27 },
	{ (Il2CppRGCTXDataType)2, 5589 },
	{ (Il2CppRGCTXDataType)2, 14084 },
	{ (Il2CppRGCTXDataType)3, 28002 },
	{ (Il2CppRGCTXDataType)3, 28004 },
	{ (Il2CppRGCTXDataType)3, 19637 },
	{ (Il2CppRGCTXDataType)3, 16394 },
	{ (Il2CppRGCTXDataType)2, 8130 },
	{ (Il2CppRGCTXDataType)2, 17115 },
	{ (Il2CppRGCTXDataType)2, 6102 },
	{ (Il2CppRGCTXDataType)3, 2705 },
	{ (Il2CppRGCTXDataType)3, 28003 },
	{ (Il2CppRGCTXDataType)2, 1675 },
	{ (Il2CppRGCTXDataType)2, 10503 },
	{ (Il2CppRGCTXDataType)3, 28005 },
	{ (Il2CppRGCTXDataType)3, 28006 },
	{ (Il2CppRGCTXDataType)2, 9464 },
	{ (Il2CppRGCTXDataType)3, 19636 },
	{ (Il2CppRGCTXDataType)2, 17107 },
	{ (Il2CppRGCTXDataType)2, 9918 },
	{ (Il2CppRGCTXDataType)2, 10266 },
	{ (Il2CppRGCTXDataType)3, 18149 },
	{ (Il2CppRGCTXDataType)3, 18150 },
	{ (Il2CppRGCTXDataType)3, 42832 },
	{ (Il2CppRGCTXDataType)2, 6104 },
	{ (Il2CppRGCTXDataType)3, 2719 },
	{ (Il2CppRGCTXDataType)3, 19638 },
	{ (Il2CppRGCTXDataType)3, 40350 },
	{ (Il2CppRGCTXDataType)2, 3772 },
	{ (Il2CppRGCTXDataType)3, 16407 },
	{ (Il2CppRGCTXDataType)2, 8135 },
	{ (Il2CppRGCTXDataType)2, 17171 },
	{ (Il2CppRGCTXDataType)3, 34091 },
	{ (Il2CppRGCTXDataType)3, 34092 },
	{ (Il2CppRGCTXDataType)2, 10514 },
	{ (Il2CppRGCTXDataType)3, 34093 },
	{ (Il2CppRGCTXDataType)2, 1861 },
	{ (Il2CppRGCTXDataType)3, 28007 },
	{ (Il2CppRGCTXDataType)2, 14086 },
	{ (Il2CppRGCTXDataType)3, 28008 },
	{ (Il2CppRGCTXDataType)3, 19628 },
	{ (Il2CppRGCTXDataType)2, 6101 },
	{ (Il2CppRGCTXDataType)3, 2650 },
	{ (Il2CppRGCTXDataType)3, 31772 },
	{ (Il2CppRGCTXDataType)2, 14803 },
	{ (Il2CppRGCTXDataType)3, 31811 },
	{ (Il2CppRGCTXDataType)2, 7128 },
	{ (Il2CppRGCTXDataType)3, 7817 },
	{ (Il2CppRGCTXDataType)3, 31778 },
	{ (Il2CppRGCTXDataType)3, 14563 },
	{ (Il2CppRGCTXDataType)2, 2533 },
	{ (Il2CppRGCTXDataType)3, 31773 },
	{ (Il2CppRGCTXDataType)2, 14798 },
	{ (Il2CppRGCTXDataType)3, 8529 },
	{ (Il2CppRGCTXDataType)2, 7309 },
	{ (Il2CppRGCTXDataType)2, 8045 },
	{ (Il2CppRGCTXDataType)3, 14593 },
	{ (Il2CppRGCTXDataType)3, 31774 },
	{ (Il2CppRGCTXDataType)3, 14558 },
	{ (Il2CppRGCTXDataType)3, 14559 },
	{ (Il2CppRGCTXDataType)3, 14557 },
	{ (Il2CppRGCTXDataType)3, 14560 },
	{ (Il2CppRGCTXDataType)2, 8041 },
	{ (Il2CppRGCTXDataType)2, 17103 },
	{ (Il2CppRGCTXDataType)3, 18164 },
	{ (Il2CppRGCTXDataType)3, 14562 },
	{ (Il2CppRGCTXDataType)2, 9698 },
	{ (Il2CppRGCTXDataType)3, 14561 },
	{ (Il2CppRGCTXDataType)2, 9566 },
	{ (Il2CppRGCTXDataType)2, 17030 },
	{ (Il2CppRGCTXDataType)2, 9952 },
	{ (Il2CppRGCTXDataType)2, 10269 },
	{ (Il2CppRGCTXDataType)3, 16384 },
	{ (Il2CppRGCTXDataType)2, 8126 },
	{ (Il2CppRGCTXDataType)3, 19685 },
	{ (Il2CppRGCTXDataType)3, 19686 },
	{ (Il2CppRGCTXDataType)2, 9485 },
	{ (Il2CppRGCTXDataType)3, 19689 },
	{ (Il2CppRGCTXDataType)2, 9485 },
	{ (Il2CppRGCTXDataType)3, 19690 },
	{ (Il2CppRGCTXDataType)2, 9573 },
	{ (Il2CppRGCTXDataType)3, 19694 },
	{ (Il2CppRGCTXDataType)3, 19699 },
	{ (Il2CppRGCTXDataType)3, 19698 },
	{ (Il2CppRGCTXDataType)2, 17169 },
	{ (Il2CppRGCTXDataType)3, 19688 },
	{ (Il2CppRGCTXDataType)3, 19687 },
	{ (Il2CppRGCTXDataType)3, 19695 },
	{ (Il2CppRGCTXDataType)2, 10500 },
	{ (Il2CppRGCTXDataType)3, 19692 },
	{ (Il2CppRGCTXDataType)3, 46358 },
	{ (Il2CppRGCTXDataType)2, 8048 },
	{ (Il2CppRGCTXDataType)3, 14631 },
	{ (Il2CppRGCTXDataType)1, 9691 },
	{ (Il2CppRGCTXDataType)2, 17043 },
	{ (Il2CppRGCTXDataType)3, 19691 },
	{ (Il2CppRGCTXDataType)1, 17043 },
	{ (Il2CppRGCTXDataType)1, 10500 },
	{ (Il2CppRGCTXDataType)2, 17169 },
	{ (Il2CppRGCTXDataType)2, 17043 },
	{ (Il2CppRGCTXDataType)2, 9961 },
	{ (Il2CppRGCTXDataType)2, 10274 },
	{ (Il2CppRGCTXDataType)3, 32203 },
	{ (Il2CppRGCTXDataType)3, 19696 },
	{ (Il2CppRGCTXDataType)3, 19697 },
	{ (Il2CppRGCTXDataType)3, 19693 },
	{ (Il2CppRGCTXDataType)3, 19700 },
	{ (Il2CppRGCTXDataType)2, 1543 },
	{ (Il2CppRGCTXDataType)3, 14653 },
	{ (Il2CppRGCTXDataType)2, 2385 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	359,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	92,
	s_rgctxIndices,
	412,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
