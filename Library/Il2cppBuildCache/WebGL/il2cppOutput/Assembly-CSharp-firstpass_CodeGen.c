﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void PyramidAnimation::Start()
extern void PyramidAnimation_Start_m9E3A8F7C90DC60781E9D3D7E0A8CB1E8F6B9857A (void);
// 0x00000002 System.Void PyramidAnimation::Animate()
extern void PyramidAnimation_Animate_m55DA2060EADF72524E5F83E74B330BF35A663268 (void);
// 0x00000003 System.Void PyramidAnimation::Update()
extern void PyramidAnimation_Update_mA210DB88B125D6E28BA008DC92B0C6270D8B375D (void);
// 0x00000004 System.Void PyramidAnimation::.ctor()
extern void PyramidAnimation__ctor_m32B0D3D7001776558E93D1B9E1690B78A47F80EE (void);
// 0x00000005 System.Void CandleChartFeed::Start()
extern void CandleChartFeed_Start_mFFCD83CFE3FFEA698E1ABA137E0C4C748121FC34 (void);
// 0x00000006 System.Void CandleChartFeed::Update()
extern void CandleChartFeed_Update_m99C28E006E14BEA77F1BAA3BFFFC557C7DB0CE07 (void);
// 0x00000007 System.Void CandleChartFeed::.ctor()
extern void CandleChartFeed__ctor_mBF450EB33D8F1D87AA953635ACFAEA361BB9A15E (void);
// 0x00000008 System.Void PyramidFeed::Start()
extern void PyramidFeed_Start_mF0C8196AC2A1B66421F0035FFF28B459FE650940 (void);
// 0x00000009 System.Void PyramidFeed::Update()
extern void PyramidFeed_Update_m94A84AD8034B3A8AA3D4D094A4154F9196764A41 (void);
// 0x0000000A System.Void PyramidFeed::.ctor()
extern void PyramidFeed__ctor_m9EA946F11C5ED29A30026DC6ECD52ECA5E26A81A (void);
// 0x0000000B System.Void BarContentFitter::Start()
extern void BarContentFitter_Start_m56A709082F6099D3166144ECF83EF75A4CB767B3 (void);
// 0x0000000C System.Void BarContentFitter::OnValidate()
extern void BarContentFitter_OnValidate_m062D09E63ED68147C8C8007C2607468993D3ABDC (void);
// 0x0000000D System.Void BarContentFitter::Match()
extern void BarContentFitter_Match_m6CD3744A2CA771813EB74B48D526BE5F422FA05F (void);
// 0x0000000E System.Void BarContentFitter::Update()
extern void BarContentFitter_Update_m58BB387F69F62C6814471E84BD5505A5938DE871 (void);
// 0x0000000F System.Void BarContentFitter::.ctor()
extern void BarContentFitter__ctor_m039CCCAF31FC3DC03BC020E1048684259D7962A1 (void);
// 0x00000010 System.Void customBarSelection::Start()
extern void customBarSelection_Start_m73E1DAD7F0C38DA1043ED1F44B92CB18736A8F97 (void);
// 0x00000011 System.Void customBarSelection::OnDestroy()
extern void customBarSelection_OnDestroy_m4A497F9566D7E24619C3810A1BA3BB7795A3AF25 (void);
// 0x00000012 System.Void customBarSelection::ToogleSelection(System.String,System.String)
extern void customBarSelection_ToogleSelection_m2C4F60C534ABB4A3DE7357F5F6882525D541BCF3 (void);
// 0x00000013 System.Void customBarSelection::SetSelection(System.String,System.String,System.Boolean)
extern void customBarSelection_SetSelection_mE205FFCE93B235BAD4E92553BB5BA3044F258E73 (void);
// 0x00000014 System.Void customBarSelection::.ctor()
extern void customBarSelection__ctor_m5A7189B8CB40E826024F10E3945F997E8AC58C09 (void);
// 0x00000015 System.Void masterBarSelection::Register(customBarSelection)
extern void masterBarSelection_Register_mF6B0C5A7E8DD251199075EEBD1531CD201E729F1 (void);
// 0x00000016 System.Void masterBarSelection::Unregister(customBarSelection)
extern void masterBarSelection_Unregister_m6757055C6E80348EA312AC86AB55457E89F9995B (void);
// 0x00000017 System.Void masterBarSelection::ToogleBar(System.String,System.String)
extern void masterBarSelection_ToogleBar_m4611FE7997C7FFAD306B4041017B180CB9022B42 (void);
// 0x00000018 System.Void masterBarSelection::SelectBar(System.String,System.String)
extern void masterBarSelection_SelectBar_mB698887406CAB68EF64E13998A70B0C3757A7C6D (void);
// 0x00000019 System.Void masterBarSelection::DeselectBar(System.String,System.String)
extern void masterBarSelection_DeselectBar_m25371DB70D930477CE94A54EEF9140013B6CB954 (void);
// 0x0000001A System.Void masterBarSelection::.ctor()
extern void masterBarSelection__ctor_m0BE374261F8252530FB1CCDF00A6223D39880E9D (void);
// 0x0000001B System.Void BarDataFiller::.cctor()
extern void BarDataFiller__cctor_mD3599F13797FC435E77C4C284372695B30669A64 (void);
// 0x0000001C System.Void BarDataFiller::EnsureCreateDataTypes()
extern void BarDataFiller_EnsureCreateDataTypes_m07D7B976F18BF4F391A29DE9EAC154D9DF59AE5D (void);
// 0x0000001D System.Double BarDataFiller::ParseItem(System.String,System.String)
extern void BarDataFiller_ParseItem_mA35CD85D0DA16A8009C629A98CAE490308BCDA18 (void);
// 0x0000001E System.Void BarDataFiller::LoadValueArray(BarDataFiller/CategoryData)
extern void BarDataFiller_LoadValueArray_mF343C7A768D3D6DEDC0C5B263E7E2022C59E3271 (void);
// 0x0000001F System.Void BarDataFiller::LoadObjectForEachElement(BarDataFiller/CategoryData)
extern void BarDataFiller_LoadObjectForEachElement_mC091368335B5031C3300B7062821180F6258A3B2 (void);
// 0x00000020 System.Void BarDataFiller::Start()
extern void BarDataFiller_Start_mD6E970115E46C74E8A2B3D73DE303529D8330E28 (void);
// 0x00000021 System.Void BarDataFiller::Fill()
extern void BarDataFiller_Fill_m633F58F9022016EFE1FB87F25A06FAFC34E59871 (void);
// 0x00000022 System.Void BarDataFiller::Fill(UnityEngine.WWWForm)
extern void BarDataFiller_Fill_mC11F6B735190D745DACFFFA38838E0369C0CB293 (void);
// 0x00000023 System.Void BarDataFiller::LoadCategoryVisualStyle(ChartAndGraph.BarChart)
extern void BarDataFiller_LoadCategoryVisualStyle_mBD1E913B85036E45B38644876D9E01310E617403 (void);
// 0x00000024 System.Void BarDataFiller::ApplyData(System.String)
extern void BarDataFiller_ApplyData_m6483A9ED1FDED1D1F829258DFE4461B5A95D2CF3 (void);
// 0x00000025 UnityEngine.Networking.UnityWebRequest BarDataFiller::CreateRequest(UnityEngine.WWWForm)
extern void BarDataFiller_CreateRequest_m008A16DECFA515370F6366DBBE00B3D41E7FB02D (void);
// 0x00000026 System.Collections.IEnumerator BarDataFiller::GetData(UnityEngine.WWWForm)
extern void BarDataFiller_GetData_mD41A3305539B936D9A51B5EF5F6EFF55107730C2 (void);
// 0x00000027 System.Void BarDataFiller::Update()
extern void BarDataFiller_Update_m2C5956D636139184C8D5A2392A39253CDB44D007 (void);
// 0x00000028 System.Void BarDataFiller::.ctor()
extern void BarDataFiller__ctor_mB0054DBC5AA02B744DDF14CF57B38069D381D87B (void);
// 0x00000029 System.Void BarDataFiller/CategoryData::.ctor()
extern void CategoryData__ctor_m0FFAAB2771823D79C287D84BB72BD2CA28D375B0 (void);
// 0x0000002A System.Void BarDataFiller/CategoryLoader::.ctor(System.Object,System.IntPtr)
extern void CategoryLoader__ctor_m69E2D50B997EDCD0E39E557496357FB1B90EC3A3 (void);
// 0x0000002B System.Void BarDataFiller/CategoryLoader::Invoke(BarDataFiller/CategoryData)
extern void CategoryLoader_Invoke_mE86E501C167E32E6E31C87DCC9094C6DB6586C06 (void);
// 0x0000002C System.IAsyncResult BarDataFiller/CategoryLoader::BeginInvoke(BarDataFiller/CategoryData,System.AsyncCallback,System.Object)
extern void CategoryLoader_BeginInvoke_m9FB207714A5C22C03E5B90E26E791F5DC12BB409 (void);
// 0x0000002D System.Void BarDataFiller/CategoryLoader::EndInvoke(System.IAsyncResult)
extern void CategoryLoader_EndInvoke_mCB375F7E76C9C279EC6644753F3C6733EE12B27E (void);
// 0x0000002E System.Void BarDataFiller/<GetData>d__24::.ctor(System.Int32)
extern void U3CGetDataU3Ed__24__ctor_m23346B90BF5520515FB01571AAD820E9056BC2AF (void);
// 0x0000002F System.Void BarDataFiller/<GetData>d__24::System.IDisposable.Dispose()
extern void U3CGetDataU3Ed__24_System_IDisposable_Dispose_mEC6A57765D641AA00A34DCFA6092965012FE2958 (void);
// 0x00000030 System.Boolean BarDataFiller/<GetData>d__24::MoveNext()
extern void U3CGetDataU3Ed__24_MoveNext_m4D7A405FCDFF6123206D8B1366C5D0675E5F6E3D (void);
// 0x00000031 System.Void BarDataFiller/<GetData>d__24::<>m__Finally1()
extern void U3CGetDataU3Ed__24_U3CU3Em__Finally1_m0B46925728F135D105CDDF531CA3DF43DCB02539 (void);
// 0x00000032 System.Object BarDataFiller/<GetData>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D8AD14E0C19EE312760EFFF80A2F14CBE757EFB (void);
// 0x00000033 System.Void BarDataFiller/<GetData>d__24::System.Collections.IEnumerator.Reset()
extern void U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_mB0F4ABD0744B078491FA16002ED4D4749F55AD9E (void);
// 0x00000034 System.Object BarDataFiller/<GetData>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_mCD2136265947A93966E384448D5C93D5B0D037D5 (void);
// 0x00000035 System.Void GraphDataFiller::.cctor()
extern void GraphDataFiller__cctor_m1034A158AE4CC9039019B3696786E1FE5E9C9CFE (void);
// 0x00000036 System.Void GraphDataFiller::EnsureCreateDataTypes()
extern void GraphDataFiller_EnsureCreateDataTypes_mA8AEA4D813A2EBB4E053F041A955AD82AE1C325D (void);
// 0x00000037 System.Void GraphDataFiller::CreateVectorFormats()
extern void GraphDataFiller_CreateVectorFormats_m53C262FFA98A221282D56786121FAC1EC373B508 (void);
// 0x00000038 System.Double GraphDataFiller::ParseItem(System.String,System.String)
extern void GraphDataFiller_ParseItem_m6888B64F7483853EBD6D0177F6D02A216AC11102 (void);
// 0x00000039 System.Void GraphDataFiller::LoadArrayForEachElement(GraphDataFiller/CategoryData)
extern void GraphDataFiller_LoadArrayForEachElement_mC51797EFA85A670E51059FE46DD951768C737B19 (void);
// 0x0000003A System.Void GraphDataFiller::LoadObjectArray(GraphDataFiller/CategoryData)
extern void GraphDataFiller_LoadObjectArray_mA1C6F1FD572C8F68224C3ED84370566B4C0D2A57 (void);
// 0x0000003B System.Void GraphDataFiller::LoadVectorArray(GraphDataFiller/CategoryData)
extern void GraphDataFiller_LoadVectorArray_m0030532864279D533B8E0558A856A03196DDCD58 (void);
// 0x0000003C System.Void GraphDataFiller::Start()
extern void GraphDataFiller_Start_m6684A6F7980FFCDBF0DA6D58622902EE19F4B1F8 (void);
// 0x0000003D System.Void GraphDataFiller::Fill()
extern void GraphDataFiller_Fill_m4CCB1B3968316276FBD8A542F5285669DA7C962E (void);
// 0x0000003E System.Void GraphDataFiller::Fill(UnityEngine.WWWForm)
extern void GraphDataFiller_Fill_m7677B6A5D9A669524DFB588810C9871858A41E42 (void);
// 0x0000003F System.Void GraphDataFiller::LoadCategoryVisualStyle(ChartAndGraph.GraphChartBase)
extern void GraphDataFiller_LoadCategoryVisualStyle_m35986A756D3D58885746CBB473334C2A2AB200EC (void);
// 0x00000040 System.Void GraphDataFiller::ApplyData(System.String)
extern void GraphDataFiller_ApplyData_m21DE343237A850448607067BB5614A9CA4285B1C (void);
// 0x00000041 UnityEngine.Networking.UnityWebRequest GraphDataFiller::CreateRequest(UnityEngine.WWWForm)
extern void GraphDataFiller_CreateRequest_m23A7BB28725B793E524C1D5C2F7F0E0635971468 (void);
// 0x00000042 System.Collections.IEnumerator GraphDataFiller::GetData(UnityEngine.WWWForm)
extern void GraphDataFiller_GetData_mDB9DBCC704135811FA58F24E972BEAF1918525C8 (void);
// 0x00000043 System.Void GraphDataFiller::Update()
extern void GraphDataFiller_Update_mE913AEE8A2B013EB0E9B6B345FCB005C384150A2 (void);
// 0x00000044 System.Void GraphDataFiller::.ctor()
extern void GraphDataFiller__ctor_m5AAE2D5F5A3B78649DD66F46AFC215D825FE6833 (void);
// 0x00000045 System.Void GraphDataFiller/VectorFormatData::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VectorFormatData__ctor_m8A07BEE0BD766F79EDF5A5B44B87A75BB85F5286 (void);
// 0x00000046 System.Void GraphDataFiller/CategoryData::.ctor()
extern void CategoryData__ctor_m59954DD6BC2C0405144B641FDB5A3696FA981F33 (void);
// 0x00000047 System.Void GraphDataFiller/CategoryLoader::.ctor(System.Object,System.IntPtr)
extern void CategoryLoader__ctor_m876ED039772EF03576826B125261A73D80C4D2DE (void);
// 0x00000048 System.Void GraphDataFiller/CategoryLoader::Invoke(GraphDataFiller/CategoryData)
extern void CategoryLoader_Invoke_m146DDAD28CCEF3F7A60D38A72648878B2233C0E7 (void);
// 0x00000049 System.IAsyncResult GraphDataFiller/CategoryLoader::BeginInvoke(GraphDataFiller/CategoryData,System.AsyncCallback,System.Object)
extern void CategoryLoader_BeginInvoke_m91E27CDAA4446FE6A1BC4C9C4C85AA8D90CBB5EA (void);
// 0x0000004A System.Void GraphDataFiller/CategoryLoader::EndInvoke(System.IAsyncResult)
extern void CategoryLoader_EndInvoke_mA19234C90E5A01148F3EE210E284C1CF56411390 (void);
// 0x0000004B System.Void GraphDataFiller/<GetData>d__29::.ctor(System.Int32)
extern void U3CGetDataU3Ed__29__ctor_m3C343F3EC10F96FC8639D32AA99C25FA12075BD5 (void);
// 0x0000004C System.Void GraphDataFiller/<GetData>d__29::System.IDisposable.Dispose()
extern void U3CGetDataU3Ed__29_System_IDisposable_Dispose_mE4AA5BFA3FC25D675CA57B4C8075D20B84A1830B (void);
// 0x0000004D System.Boolean GraphDataFiller/<GetData>d__29::MoveNext()
extern void U3CGetDataU3Ed__29_MoveNext_mCAC6F2B25682F73F1FD5EBFD8169E1A6A36BFA4D (void);
// 0x0000004E System.Void GraphDataFiller/<GetData>d__29::<>m__Finally1()
extern void U3CGetDataU3Ed__29_U3CU3Em__Finally1_m6CF9CEDD151D26CDA46D4134A1F916C3246A2BFF (void);
// 0x0000004F System.Object GraphDataFiller/<GetData>d__29::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDataU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F32C1C524518E6F04CCCCAD3771DAAE1665EC45 (void);
// 0x00000050 System.Void GraphDataFiller/<GetData>d__29::System.Collections.IEnumerator.Reset()
extern void U3CGetDataU3Ed__29_System_Collections_IEnumerator_Reset_m7DCBB1D02F477820DCE81C95EB63B1D2747AC323 (void);
// 0x00000051 System.Object GraphDataFiller/<GetData>d__29::System.Collections.IEnumerator.get_Current()
extern void U3CGetDataU3Ed__29_System_Collections_IEnumerator_get_Current_m628029FF6FC5324234A6D305A09351A288BF00D2 (void);
// 0x00000052 System.Void PieDataFiller::.cctor()
extern void PieDataFiller__cctor_m251B99C364DFE798BCC59B34C7E052A6285C9A7C (void);
// 0x00000053 System.Double PieDataFiller::ParseItem(System.String,System.String)
extern void PieDataFiller_ParseItem_m65D5329FBBEFF7169A16796DC8CB82C46A73884B (void);
// 0x00000054 System.Void PieDataFiller::LoadObjectforEachElement()
extern void PieDataFiller_LoadObjectforEachElement_m7EE0B4F1B06CFE167B0B58B170E41DD01E90012D (void);
// 0x00000055 System.Void PieDataFiller::LoadValueArray()
extern void PieDataFiller_LoadValueArray_m7B35C6F8D602022D256D7B9829BCE1E7D9F341F6 (void);
// 0x00000056 System.Void PieDataFiller::Start()
extern void PieDataFiller_Start_mC19BFD289B702BDAD3269065A857080F8317766C (void);
// 0x00000057 System.Void PieDataFiller::Fill()
extern void PieDataFiller_Fill_m3B2906674B8627EBDDC398723A35E16549C82F4C (void);
// 0x00000058 System.Void PieDataFiller::Fill(UnityEngine.WWWForm)
extern void PieDataFiller_Fill_mA572370E4C699805D5497EE8CC9013F1D7C3FF1A (void);
// 0x00000059 System.Void PieDataFiller::LoadCategoryVisualStyle(ChartAndGraph.PieChart)
extern void PieDataFiller_LoadCategoryVisualStyle_m0BC4B4DABB4987D433A471F685C73EE7853B173F (void);
// 0x0000005A System.Void PieDataFiller::ApplyData(System.String)
extern void PieDataFiller_ApplyData_m1154210329B43E30CB55F85C3C4B6126F960083F (void);
// 0x0000005B UnityEngine.Networking.UnityWebRequest PieDataFiller::CreateRequest(UnityEngine.WWWForm)
extern void PieDataFiller_CreateRequest_m293101FD2E3EFC977FCA4B8202D8E3FBE6491926 (void);
// 0x0000005C System.Collections.IEnumerator PieDataFiller::GetData(UnityEngine.WWWForm)
extern void PieDataFiller_GetData_m7AD75ED5136A7540B377D95CDC73B5156AE07BE0 (void);
// 0x0000005D System.Void PieDataFiller::Update()
extern void PieDataFiller_Update_mDB3426EE842E78B369FBCAF9F9B104BF0FB42DA3 (void);
// 0x0000005E System.Void PieDataFiller::.ctor()
extern void PieDataFiller__ctor_mCAAAB908614BD4C01C5A021B08C28995A657C7E8 (void);
// 0x0000005F System.Void PieDataFiller/<GetData>d__21::.ctor(System.Int32)
extern void U3CGetDataU3Ed__21__ctor_m5FDFF7FBE070B5D39B9735F93E54F91C4650592E (void);
// 0x00000060 System.Void PieDataFiller/<GetData>d__21::System.IDisposable.Dispose()
extern void U3CGetDataU3Ed__21_System_IDisposable_Dispose_m3690B5FEA5C363A68E5CF5491D67E4DAF05E0C08 (void);
// 0x00000061 System.Boolean PieDataFiller/<GetData>d__21::MoveNext()
extern void U3CGetDataU3Ed__21_MoveNext_m935AB22B609D60E1ABAAC0983DBDBA8C1F9CF42F (void);
// 0x00000062 System.Void PieDataFiller/<GetData>d__21::<>m__Finally1()
extern void U3CGetDataU3Ed__21_U3CU3Em__Finally1_m6A8B9DC5964ABD9A35F9F30D17C5B517601B74EA (void);
// 0x00000063 System.Object PieDataFiller/<GetData>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDataU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m162CF48EF0D446615C4EF6D006C692CDD0154710 (void);
// 0x00000064 System.Void PieDataFiller/<GetData>d__21::System.Collections.IEnumerator.Reset()
extern void U3CGetDataU3Ed__21_System_Collections_IEnumerator_Reset_m37DE13669B84A238C4A939A80966461EADE38B04 (void);
// 0x00000065 System.Object PieDataFiller/<GetData>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CGetDataU3Ed__21_System_Collections_IEnumerator_get_Current_m36AEC0B435EC47BE9E67D3BF1F81D07AE22C4C1E (void);
// 0x00000066 System.Void RadarDataFiller::.cctor()
extern void RadarDataFiller__cctor_m66EFBA774345BAF1422C23991AF9659EB79D3AE9 (void);
// 0x00000067 System.Void RadarDataFiller::EnsureCreateDataTypes()
extern void RadarDataFiller_EnsureCreateDataTypes_m45D9EDC47078D80CEB08110FCD24E7182F401F7D (void);
// 0x00000068 System.Double RadarDataFiller::ParseItem(System.String,System.String)
extern void RadarDataFiller_ParseItem_m7C8CE9B3929B8531623D1F4DE72C928049374124 (void);
// 0x00000069 System.Void RadarDataFiller::LoadValueArray(RadarDataFiller/CategoryData)
extern void RadarDataFiller_LoadValueArray_mCCB7D7C680BC47B7F16DC8AC900DB12E1782E0D5 (void);
// 0x0000006A System.Void RadarDataFiller::LoadObjectForEachElement(RadarDataFiller/CategoryData)
extern void RadarDataFiller_LoadObjectForEachElement_mEC3CD641B0FAF86E0C353342ABC01DF30AF30AFA (void);
// 0x0000006B System.Void RadarDataFiller::Start()
extern void RadarDataFiller_Start_mF8A2DCA86AD2EC1AAB1EA44C6CC75344B0F24923 (void);
// 0x0000006C System.Void RadarDataFiller::Fill()
extern void RadarDataFiller_Fill_mF5F43891081B0057F3B03CFEED56AB626C8C7E10 (void);
// 0x0000006D System.Void RadarDataFiller::Fill(UnityEngine.WWWForm)
extern void RadarDataFiller_Fill_mB4150671143F5AAC511109937C4EE9EDDA05F99D (void);
// 0x0000006E System.Void RadarDataFiller::LoadCategoryVisualStyle(ChartAndGraph.RadarChart)
extern void RadarDataFiller_LoadCategoryVisualStyle_m0F683906F52B72204D68BED4532E952AB5832CA2 (void);
// 0x0000006F System.Void RadarDataFiller::ApplyData(System.String)
extern void RadarDataFiller_ApplyData_m24ED2D99CD90B30E341B15E640AF01AD9C9F6125 (void);
// 0x00000070 UnityEngine.Networking.UnityWebRequest RadarDataFiller::CreateRequest(UnityEngine.WWWForm)
extern void RadarDataFiller_CreateRequest_m8485E82C1F007A0ABE3DB864912801DA181030D9 (void);
// 0x00000071 System.Collections.IEnumerator RadarDataFiller::GetData(UnityEngine.WWWForm)
extern void RadarDataFiller_GetData_mB7FDF2D44705CFB8C333109D80F1D6581A254D91 (void);
// 0x00000072 System.Void RadarDataFiller::Update()
extern void RadarDataFiller_Update_m67E9404482F3B5638C48CC604A5319DED389A073 (void);
// 0x00000073 System.Void RadarDataFiller::.ctor()
extern void RadarDataFiller__ctor_m0D038E37A46D07FC6FD8E492F2D55904FF007411 (void);
// 0x00000074 System.Void RadarDataFiller/CategoryData::.ctor()
extern void CategoryData__ctor_m60FF0D85483BC9E4A7FD5F7EDC74D1B331BF3A97 (void);
// 0x00000075 System.Void RadarDataFiller/CategoryLoader::.ctor(System.Object,System.IntPtr)
extern void CategoryLoader__ctor_mD695FAFDADFCBFD8AEEC66382670D7914E5C1604 (void);
// 0x00000076 System.Void RadarDataFiller/CategoryLoader::Invoke(RadarDataFiller/CategoryData)
extern void CategoryLoader_Invoke_m32736A9B36B7D980A120E13094E0EBAE07881209 (void);
// 0x00000077 System.IAsyncResult RadarDataFiller/CategoryLoader::BeginInvoke(RadarDataFiller/CategoryData,System.AsyncCallback,System.Object)
extern void CategoryLoader_BeginInvoke_m360D17E1441FC0D7D91B93EDE19D460C251E0B13 (void);
// 0x00000078 System.Void RadarDataFiller/CategoryLoader::EndInvoke(System.IAsyncResult)
extern void CategoryLoader_EndInvoke_m50D60D107EC4BC1004AE29BFBBFE74AE97A9A73D (void);
// 0x00000079 System.Void RadarDataFiller/<GetData>d__24::.ctor(System.Int32)
extern void U3CGetDataU3Ed__24__ctor_mE5AC81C7F88B9C90F79E72BE064CC18DD6783980 (void);
// 0x0000007A System.Void RadarDataFiller/<GetData>d__24::System.IDisposable.Dispose()
extern void U3CGetDataU3Ed__24_System_IDisposable_Dispose_m1682B988B3E5EC516BD0A4DFE5AA57A883EE6F51 (void);
// 0x0000007B System.Boolean RadarDataFiller/<GetData>d__24::MoveNext()
extern void U3CGetDataU3Ed__24_MoveNext_mFF4CF340570149CACE53CEF0CA4CC4A82572DC8B (void);
// 0x0000007C System.Void RadarDataFiller/<GetData>d__24::<>m__Finally1()
extern void U3CGetDataU3Ed__24_U3CU3Em__Finally1_m948049B8B8D63FEA5422B0C41C4F13E28ED87C28 (void);
// 0x0000007D System.Object RadarDataFiller/<GetData>d__24::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70B61A70670719AED94EC9A2B8C80E97A217024D (void);
// 0x0000007E System.Void RadarDataFiller/<GetData>d__24::System.Collections.IEnumerator.Reset()
extern void U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_m5F195A69B5832A7F5B608683EC834806914785E3 (void);
// 0x0000007F System.Object RadarDataFiller/<GetData>d__24::System.Collections.IEnumerator.get_Current()
extern void U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_m2101121EA708E368E26EAF98BE1B497878498183 (void);
// 0x00000080 UnityEngine.GameObject BillboardText::get_UIText()
extern void BillboardText_get_UIText_mAB7F8962EC45CF04171D3F0D4C2CF3BBE84DB0B0 (void);
// 0x00000081 System.Void BillboardText::set_UIText(UnityEngine.GameObject)
extern void BillboardText_set_UIText_m06B97F41D530290E48CC6D721F830DAA5C0380B7 (void);
// 0x00000082 System.Object BillboardText::get_UserData()
extern void BillboardText_get_UserData_mE921AB0E2FE52066A0EA5DA30DFA7D3F80157BE8 (void);
// 0x00000083 System.Void BillboardText::set_UserData(System.Object)
extern void BillboardText_set_UserData_m0E39DB9C7C06ADB39CF5E98F7B89C46D9B6D4571 (void);
// 0x00000084 System.Void BillboardText::SetVisible(System.Boolean)
extern void BillboardText_SetVisible_m651569C349FA5B2D76CF99D8D24A0D8BBDC5237A (void);
// 0x00000085 UnityEngine.RectTransform BillboardText::get_Rect()
extern void BillboardText_get_Rect_m4558C9D124D390BB4D033B8E4B71E25277D423D1 (void);
// 0x00000086 System.Void BillboardText::.ctor()
extern void BillboardText__ctor_m55630E20EA745D30AED5B838B79FF99C94FFD717 (void);
// 0x00000087 CategoryLabels/ChartCategoryLabelOptions CategoryLabels::get_VisibleLabels()
extern void CategoryLabels_get_VisibleLabels_mC722C3B72C0D82EE4645F92F9D9F646AA59D9F3D (void);
// 0x00000088 System.Void CategoryLabels::set_VisibleLabels(CategoryLabels/ChartCategoryLabelOptions)
extern void CategoryLabels_set_VisibleLabels_m04FCCFDF85C9D7EA790EF265F5F497FDB0F899DA (void);
// 0x00000089 System.Action`2<ChartAndGraph.IInternalUse,System.Boolean> CategoryLabels::get_Assign()
extern void CategoryLabels_get_Assign_mCF26C8B4B118F45DA6A41336AB8B43ADF2C6C108 (void);
// 0x0000008A System.Void CategoryLabels::.ctor()
extern void CategoryLabels__ctor_mF01D411028AF434818D5D1C215152F2B26FD9348 (void);
// 0x0000008B System.Void CategoryLabels::<get_Assign>b__6_0(ChartAndGraph.IInternalUse,System.Boolean)
extern void CategoryLabels_U3Cget_AssignU3Eb__6_0_m1E37F7A84B474AC9DE3E6366332A2BF2A757026C (void);
// 0x0000008C System.Void ChartText::Start()
extern void ChartText_Start_m801F662B72FA58F9BD1A30E6CFB0247C01CFD4EC (void);
// 0x0000008D System.Void ChartText::Update()
extern void ChartText_Update_m8830768A03FDB905C7FAA71AE99991AC1614DDB8 (void);
// 0x0000008E System.Void ChartText::.ctor()
extern void ChartText__ctor_m03E73200903719948D14794377743F8113EA123F (void);
// 0x0000008F ChartAndGraph.GroupLabelAlignment GroupLabels::get_Alignment()
extern void GroupLabels_get_Alignment_m87FE68F73B57BAD8176A583828313C559A1FBA6B (void);
// 0x00000090 System.Void GroupLabels::set_Alignment(ChartAndGraph.GroupLabelAlignment)
extern void GroupLabels_set_Alignment_m0570AB928A68CD1E3DF1A0B33BBFD6E943A7E7ED (void);
// 0x00000091 System.Action`2<ChartAndGraph.IInternalUse,System.Boolean> GroupLabels::get_Assign()
extern void GroupLabels_get_Assign_m20F4A1682DE6388C35E07002D15BCEABD40545F5 (void);
// 0x00000092 System.Void GroupLabels::.ctor()
extern void GroupLabels__ctor_mF0EBA35211AA4FD0A8D0683F01F7605E6BFAD313 (void);
// 0x00000093 System.Void GroupLabels::<get_Assign>b__5_0(ChartAndGraph.IInternalUse,System.Boolean)
extern void GroupLabels_U3Cget_AssignU3Eb__5_0_m8BB30C6A5D704B6E4463E5D6C87799990E5BAB9E (void);
// 0x00000094 UnityEngine.MonoBehaviour ItemLabelsBase::get_TextPrefab()
extern void ItemLabelsBase_get_TextPrefab_m6DEF999EFC827B840BC11E04D36F5D7526538197 (void);
// 0x00000095 System.Void ItemLabelsBase::set_TextPrefab(UnityEngine.MonoBehaviour)
extern void ItemLabelsBase_set_TextPrefab_m09DD0ED9D3CD75DC119BB86779CAC14E63D79EF5 (void);
// 0x00000096 System.Void ItemLabelsBase::.ctor()
extern void ItemLabelsBase__ctor_m142D9906D0A414AA2C92DD1CFEFC8F17D0BF7905 (void);
// 0x00000097 System.Void ItemLabelsBase::AddChildObjects()
extern void ItemLabelsBase_AddChildObjects_m018C2563AF1BD9871F77B00CBF42F59FEE2824E9 (void);
// 0x00000098 ChartAndGraph.TextFormatting ItemLabelsBase::get_TextFormat()
extern void ItemLabelsBase_get_TextFormat_mA82C77A99B358780D9278B5EFB04A38B2F35054D (void);
// 0x00000099 System.Void ItemLabelsBase::set_TextFormat(ChartAndGraph.TextFormatting)
extern void ItemLabelsBase_set_TextFormat_mE143EC246E2C7E2E75B2280B23420187CFC555AB (void);
// 0x0000009A System.Int32 ItemLabelsBase::get_FontSize()
extern void ItemLabelsBase_get_FontSize_m20C08741113190AE57A9395893B9820E9395BA27 (void);
// 0x0000009B System.Void ItemLabelsBase::set_FontSize(System.Int32)
extern void ItemLabelsBase_set_FontSize_mDFEEE48D891B1995D2B722954C0308F060454620 (void);
// 0x0000009C System.Single ItemLabelsBase::get_FontSharpness()
extern void ItemLabelsBase_get_FontSharpness_mD9C29353DBA75A7926CB6447F4584C8FA0A26D34 (void);
// 0x0000009D System.Void ItemLabelsBase::set_FontSharpness(System.Single)
extern void ItemLabelsBase_set_FontSharpness_m937EB7D8B875CCEF952A1D420237E4BCE75EACDD (void);
// 0x0000009E System.Single ItemLabelsBase::get_Seperation()
extern void ItemLabelsBase_get_Seperation_m3FC0BE49F743C9064B7DD004D384FD7C8A11F326 (void);
// 0x0000009F System.Void ItemLabelsBase::set_Seperation(System.Single)
extern void ItemLabelsBase_set_Seperation_mD69311D695C43B5C45D026F11BAEC22EC8C61C94 (void);
// 0x000000A0 System.Void ItemLabelsBase::ValidateProperties()
extern void ItemLabelsBase_ValidateProperties_m9EE0D8630657B1A74B7FC954CA67042166EE6651 (void);
// 0x000000A1 ChartAndGraph.ChartOrientedSize ItemLabelsBase::get_Location()
extern void ItemLabelsBase_get_Location_m17E0D42D404321BF2D878F21F5902A591B0580B7 (void);
// 0x000000A2 System.Void ItemLabelsBase::set_Location(ChartAndGraph.ChartOrientedSize)
extern void ItemLabelsBase_set_Location_mDCC9F138E0A1596C8BFE07FA5C4C60E2A9D61939 (void);
// 0x000000A3 System.Void ItemLabelsBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void ItemLabelsBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mB5A25B949CA84EB8ECA0946479CBFA4BC8F85CEA (void);
// 0x000000A4 System.Void ItemLabelsBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void ItemLabelsBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m109C00CA4D121C3F734312FDE1AF6855F0B7E51D (void);
// 0x000000A5 System.Collections.Generic.List`1<BillboardText> TextController::get_Text()
extern void TextController_get_Text_mFD0238C6C109AD916DE9C8379B15B88C359FA107 (void);
// 0x000000A6 System.Void TextController::SetInnerScale(System.Single)
extern void TextController_SetInnerScale_mFE4889429B9ED6829F076DADA795ADC11FEE3F15 (void);
// 0x000000A7 ChartAndGraph.AnyChart TextController::get_mParent()
extern void TextController_get_mParent_m33F259DA1763414EA5A9A288D8DBF1A4DA63C7C0 (void);
// 0x000000A8 System.Void TextController::set_mParent(ChartAndGraph.AnyChart)
extern void TextController_set_mParent_mFABEAB7D648EBAB3AB7C38A5F13323D1F7451D56 (void);
// 0x000000A9 System.Void TextController::Start()
extern void TextController_Start_mC0EEF24BD7CBF7C0E1935B2E8390DCA674A93D04 (void);
// 0x000000AA System.Void TextController::OnDestroy()
extern void TextController_OnDestroy_m1554DDF8F20062EBDDA51E3551D8016474C5DABF (void);
// 0x000000AB System.Void TextController::Canvas_willRenderCanvases()
extern void TextController_Canvas_willRenderCanvases_m7625506B363DB0B511ED8E46708B0DC61ABA6774 (void);
// 0x000000AC System.Void TextController::EnsureCanvas()
extern void TextController_EnsureCanvas_mEA25AFB8AE72C83C189F82B4163AC2C977B2FAC3 (void);
// 0x000000AD UnityEngine.Canvas TextController::get_SafeCanvas()
extern void TextController_get_SafeCanvas_m154826805B7DBE7E90EB2763F409EDAA13BFF238 (void);
// 0x000000AE System.Void TextController::OnDestory()
extern void TextController_OnDestory_m7536891FAB89E961AE4A4DDDB5072AE46430DE76 (void);
// 0x000000AF System.Void TextController::DestroyAll()
extern void TextController_DestroyAll_m52ABBB33C36136DBDA3C6187BC0FBFFED5652130 (void);
// 0x000000B0 System.Void TextController::AddText(BillboardText)
extern void TextController_AddText_m79C9AF8C77AEFA5ADA3D5BD576753A7D42F29D7A (void);
// 0x000000B1 UnityEngine.Camera TextController::EnsureCamera()
extern void TextController_EnsureCamera_m0B4707DB10A03DEFDC95959A0625817814894A81 (void);
// 0x000000B2 UnityEngine.Camera TextController::AssignCamera(UnityEngine.Camera)
extern void TextController_AssignCamera_m00BC4372C3CB1FB458D7C4B339B24CFB5BA2342C (void);
// 0x000000B3 System.Void TextController::Update()
extern void TextController_Update_m70F7AB07AA36CAA06091C83F38E4D00D3214A3D5 (void);
// 0x000000B4 UnityEngine.Vector3 TextController::ProjectPointOnPlane(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void TextController_ProjectPointOnPlane_mD09C89EA119BBF1D224D71030164E8355784B2F6 (void);
// 0x000000B5 System.Void TextController::CalculatePlane(UnityEngine.Camera,UnityEngine.RectTransform,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void TextController_CalculatePlane_m2AD8522C3A81ACD3A057F4DE3B4352BEDDD509EA (void);
// 0x000000B6 System.Void TextController::ApplyTextPosition()
extern void TextController_ApplyTextPosition_m0E3D96AB532953349DAB746DA0F93F07FA449083 (void);
// 0x000000B7 System.Void TextController::LateUpdate()
extern void TextController_LateUpdate_m17FD2A503AEA145AA3B1A37BDA1758DC91EC5741 (void);
// 0x000000B8 System.Void TextController::.ctor()
extern void TextController__ctor_m1C8A04AF790B9F1451CA5885ED58953C2F7989CD (void);
// 0x000000B9 System.Void TextController/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_mD6E461DD4146296306493E8B3FE5657EACC52F3B (void);
// 0x000000BA System.Boolean TextController/<>c__DisplayClass32_0::<ApplyTextPosition>b__0(BillboardText)
extern void U3CU3Ec__DisplayClass32_0_U3CApplyTextPositionU3Eb__0_mEAAC0C4EC1E513611879196879EB9DDD691FEEE3 (void);
// 0x000000BB System.Void BarAnimation::Start()
extern void BarAnimation_Start_m0888925527A0BF1E8A4AF3B815152718058F3D2E (void);
// 0x000000BC System.Void BarAnimation::Animate()
extern void BarAnimation_Animate_m37ED9C50B6BCA23841EBCE310DB3F40B159000DF (void);
// 0x000000BD System.Void BarAnimation::Update()
extern void BarAnimation_Update_m3DCAA30690F49343A27CA459F3EB1609A39CEE0B (void);
// 0x000000BE System.Void BarAnimation::.ctor()
extern void BarAnimation__ctor_m6365A477F47D1690BA9DAC4CF3C224945835C99B (void);
// 0x000000BF System.Void GraphAnimation::Start()
extern void GraphAnimation_Start_mB860DC47A57854C8E9F0B1556B7C552A8046EF26 (void);
// 0x000000C0 System.Boolean GraphAnimation::IsValidDouble(System.Double)
extern void GraphAnimation_IsValidDouble_m85358BC2CB0792B6FBB3ADFEDB39D816EE42BB59 (void);
// 0x000000C1 System.Void GraphAnimation::Animate(System.String,System.Collections.Generic.List`1<ChartAndGraph.DoubleVector2>,System.Single)
extern void GraphAnimation_Animate_m9981ABB5F8490D631F6305FCC4A0353BBE4C2BA3 (void);
// 0x000000C2 System.Void GraphAnimation::FixedUpdate()
extern void GraphAnimation_FixedUpdate_mD1535CEDAAD4ACC7F31695E51845C6087D7D010A (void);
// 0x000000C3 System.Void GraphAnimation::.ctor()
extern void GraphAnimation__ctor_mE6E4894238776443A9C925214DFA950EAF69C506 (void);
// 0x000000C4 System.Void GraphAnimation/InnerAnimation::Update(ChartAndGraph.GraphChartBase)
extern void InnerAnimation_Update_m5F13833FB4E240AE93428EC36B1F882759135A46 (void);
// 0x000000C5 System.Void GraphAnimation/InnerAnimation::.ctor()
extern void InnerAnimation__ctor_mF041B8218F00ABA0F7277F44C0BB25417D1D25F6 (void);
// 0x000000C6 System.Void AxisCoordinates::Start()
extern void AxisCoordinates_Start_m6918F4A65FE29709F11D665A0B53FF848458217D (void);
// 0x000000C7 System.Void AxisCoordinates::Update()
extern void AxisCoordinates_Update_m25BF46638BBF8FAFA21951F1C1BE8543FDF53D76 (void);
// 0x000000C8 System.Void AxisCoordinates::.ctor()
extern void AxisCoordinates__ctor_m20045517A731F8DF305F23B387EEFB78D63DC8CD (void);
// 0x000000C9 System.Void GraphDataVisualEditor::Start()
extern void GraphDataVisualEditor_Start_mB2152E60FA8C6D3CA194C9D6B67B30CD3A72E541 (void);
// 0x000000CA System.Void GraphDataVisualEditor::Update()
extern void GraphDataVisualEditor_Update_mE617AC8A0EE82823D0B3A1BAD2A7507820028921 (void);
// 0x000000CB System.Void GraphDataVisualEditor::.ctor()
extern void GraphDataVisualEditor__ctor_m29BD009617A67B49AB46539DD79F32677BEACEE6 (void);
// 0x000000CC System.Void HoverText::Start()
extern void HoverText_Start_m2274191EDF726AE518FAE164A815102ECEF5AAEA (void);
// 0x000000CD System.Void HoverText::NonHover()
extern void HoverText_NonHover_m11D481428A8F3C0869D455C131F59758650680CD (void);
// 0x000000CE System.Void HoverText::Update()
extern void HoverText_Update_m024AE9E58BE96987C0277C63CE9F5B1551072FD4 (void);
// 0x000000CF System.Collections.IEnumerator HoverText::SelectText(UnityEngine.UI.Text)
extern void HoverText_SelectText_mC859EF99FE3BA0697E61E7784454E77239FECEA9 (void);
// 0x000000D0 System.Void HoverText::RemoveText(UnityEngine.UI.Text)
extern void HoverText_RemoveText_mE4B225F5246ED56928E1A68FE1902B237FF71A92 (void);
// 0x000000D1 System.Void HoverText::PopText(System.String,UnityEngine.Vector3,System.Boolean)
extern void HoverText_PopText_m965EE5CDECB1927593035EA98A0B204823983BC3 (void);
// 0x000000D2 System.Void HoverText::BarHover(ChartAndGraph.BarChart/BarEventArgs)
extern void HoverText_BarHover_m0051D6CF0617FEB17C26B4BF40C807A8D3241EBA (void);
// 0x000000D3 System.Void HoverText::GraphHover(ChartAndGraph.GraphChartBase/GraphEventArgs)
extern void HoverText_GraphHover_mA609EDDDD81BEC5CD7676267751D23FDFD4A91CA (void);
// 0x000000D4 System.Void HoverText::.ctor()
extern void HoverText__ctor_mEAC46D46BDB48597C1673A4E4926B92CB806F883 (void);
// 0x000000D5 System.Void HoverText/<>c::.cctor()
extern void U3CU3Ec__cctor_m3971EBD9B2E9CBDEDD6D4E3A3EE9154196E05AB6 (void);
// 0x000000D6 System.Void HoverText/<>c::.ctor()
extern void U3CU3Ec__ctor_m13C72EBE32C43567CDF1AD532B52FABADFA9775F (void);
// 0x000000D7 System.Boolean HoverText/<>c::<Update>b__10_0(ChartAndGraph.CharItemEffectController)
extern void U3CU3Ec_U3CUpdateU3Eb__10_0_mFFB002A8FDAA17ECB08E696823961DC7F00FF057 (void);
// 0x000000D8 System.Void HoverText/<SelectText>d__11::.ctor(System.Int32)
extern void U3CSelectTextU3Ed__11__ctor_m97FF7574BECDA2356CE91AB79D964D437607610B (void);
// 0x000000D9 System.Void HoverText/<SelectText>d__11::System.IDisposable.Dispose()
extern void U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m0137CCE653C298077E28C9B537639ABF411CF179 (void);
// 0x000000DA System.Boolean HoverText/<SelectText>d__11::MoveNext()
extern void U3CSelectTextU3Ed__11_MoveNext_m55C44DC63F50B33156306DB53FFEBA4DD3A45FE7 (void);
// 0x000000DB System.Object HoverText/<SelectText>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD62DC4EDF7D813AD2A227E8A3997EC293D5F4D42 (void);
// 0x000000DC System.Void HoverText/<SelectText>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m4A0993E9C1AE47CF9B6BCABFB295969705F88EB8 (void);
// 0x000000DD System.Object HoverText/<SelectText>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m1D812B2A83110FF66329D8CF9E21A57F6D078C09 (void);
// 0x000000DE System.Void MultipleGraphDemo::Start()
extern void MultipleGraphDemo_Start_m755BE9F1BD7A103970BC765B58E16647588EB77F (void);
// 0x000000DF System.Void MultipleGraphDemo::.ctor()
extern void MultipleGraphDemo__ctor_mB180691B507CFECD43D6A681371B80E07D721DC2 (void);
// 0x000000E0 System.Void SimpleGraphDemo::Start()
extern void SimpleGraphDemo_Start_m1020866D3AF4B593D22EDDB0983376602E893926 (void);
// 0x000000E1 System.Void SimpleGraphDemo::Update()
extern void SimpleGraphDemo_Update_mC01EF2E4AF55144FD9E194D192528EAFA9BC7EBB (void);
// 0x000000E2 System.Void SimpleGraphDemo::.ctor()
extern void SimpleGraphDemo__ctor_mCA920101120C626ED2F51F62F7271B084ABB1A73 (void);
// 0x000000E3 System.Void testMarker::Start()
extern void testMarker_Start_mD705CD930485C3D340711377C94A2E973A77F457 (void);
// 0x000000E4 System.Void testMarker::Update()
extern void testMarker_Update_mEBDEE74F58D9845D91A1610E211797C38864529E (void);
// 0x000000E5 System.Void testMarker::.ctor()
extern void testMarker__ctor_mF617070254976CF3F3026AAFEE1F48315E8A69F1 (void);
// 0x000000E6 System.Void BarChartFeed::Start()
extern void BarChartFeed_Start_mCF5BB92B177EC0F36118B96992838567CFD64A70 (void);
// 0x000000E7 System.Void BarChartFeed::Update()
extern void BarChartFeed_Update_mF2484558CA8A9B5C9B4D82F529F0E6484785A289 (void);
// 0x000000E8 System.Void BarChartFeed::.ctor()
extern void BarChartFeed__ctor_mB27157699F062900B3D9A2274662E6B66D20BA86 (void);
// 0x000000E9 System.Void BarRunChart::Start()
extern void BarRunChart_Start_m3FD38EE32D5865E0AE23E88E1FEE45A22AE4DD8A (void);
// 0x000000EA System.Void BarRunChart::AddValuesToCategories()
extern void BarRunChart_AddValuesToCategories_m8D5E303BCF4D84E375874B4F3F1A8EC1D70A4222 (void);
// 0x000000EB System.Void BarRunChart::Update()
extern void BarRunChart_Update_mF12748D6C43CBFB9E06BD7F4A893CA191E187148 (void);
// 0x000000EC System.Void BarRunChart::.ctor()
extern void BarRunChart__ctor_mF85502A5D12C28A2ACC3C605E97B00CCEBE2E464 (void);
// 0x000000ED System.Void BarRunChart/RunChartEntry::.ctor(System.String,System.Double)
extern void RunChartEntry__ctor_m1D066248468841EA02363F3B2D018EB0B4829ED5 (void);
// 0x000000EE System.Void BarRunChart/<>c::.cctor()
extern void U3CU3Ec__cctor_mF08D3736BED3F89CF83438541DAC0B9E375C9B74 (void);
// 0x000000EF System.Void BarRunChart/<>c::.ctor()
extern void U3CU3Ec__ctor_m4353E711FBD0B7CD066369627C46E6DCB305C95E (void);
// 0x000000F0 System.Int32 BarRunChart/<>c::<Update>b__7_0(BarRunChart/RunChartEntry,BarRunChart/RunChartEntry)
extern void U3CU3Ec_U3CUpdateU3Eb__7_0_m109A386926080C03C01C5B55B5C77F1D48F9ED79 (void);
// 0x000000F1 System.Void ChangeOrder::Start()
extern void ChangeOrder_Start_mC11098E12352FAF34A7FFE13838DC8E3E70BF361 (void);
// 0x000000F2 System.Void ChangeOrder::Update()
extern void ChangeOrder_Update_m8BD6D11F5D89E4DEB22B879EA29C82ED0638629B (void);
// 0x000000F3 System.Void ChangeOrder::.ctor()
extern void ChangeOrder__ctor_mBA483888AAF9C227C5968455D551CE3B7E7019C4 (void);
// 0x000000F4 System.Void barMaterialSelection::Start()
extern void barMaterialSelection_Start_m158C5F8D46E3530CCCD2108BE31CCEC2602937D9 (void);
// 0x000000F5 System.Void barMaterialSelection::BarClicked(ChartAndGraph.BarChart/BarEventArgs)
extern void barMaterialSelection_BarClicked_mDD951E45FDD23973F7B2EA482AEB111FF5B32BAA (void);
// 0x000000F6 System.Void barMaterialSelection::Update()
extern void barMaterialSelection_Update_mC4E71B687410902314A56C4BC086D4E5E8B29624 (void);
// 0x000000F7 System.Void barMaterialSelection::.ctor()
extern void barMaterialSelection__ctor_mF65437273517EF6D80B3AAF5E65071CF153B6E18 (void);
// 0x000000F8 System.Void GraphChartFeed::Start()
extern void GraphChartFeed_Start_mC46517BBD16B1F372C0C8D0D06F2CC367A51CC94 (void);
// 0x000000F9 System.Collections.IEnumerator GraphChartFeed::ClearAll()
extern void GraphChartFeed_ClearAll_mFDDC6A3A71B5FBA7B10276AA0261B00A77514396 (void);
// 0x000000FA System.Void GraphChartFeed::.ctor()
extern void GraphChartFeed__ctor_m53607376CD2E57B66DAEF00FE00AE590AA647495 (void);
// 0x000000FB System.Void GraphChartFeed/<ClearAll>d__1::.ctor(System.Int32)
extern void U3CClearAllU3Ed__1__ctor_mC3D3B41DC95CA047E8CBB6D4EE7EE0A3C9B93887 (void);
// 0x000000FC System.Void GraphChartFeed/<ClearAll>d__1::System.IDisposable.Dispose()
extern void U3CClearAllU3Ed__1_System_IDisposable_Dispose_m9C5BBF85B5384B6AB898F8AE85C44DCC0AA54B93 (void);
// 0x000000FD System.Boolean GraphChartFeed/<ClearAll>d__1::MoveNext()
extern void U3CClearAllU3Ed__1_MoveNext_m2172EB52E8BE21C63076A06F011B9C1CCEC85BC0 (void);
// 0x000000FE System.Object GraphChartFeed/<ClearAll>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CClearAllU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BB8B95E41786ADB3903FAAE2B823A72A4E4C0CE (void);
// 0x000000FF System.Void GraphChartFeed/<ClearAll>d__1::System.Collections.IEnumerator.Reset()
extern void U3CClearAllU3Ed__1_System_Collections_IEnumerator_Reset_mFCAE984F92148CEAD61AFDC447B2B13371123DD3 (void);
// 0x00000100 System.Object GraphChartFeed/<ClearAll>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CClearAllU3Ed__1_System_Collections_IEnumerator_get_Current_mC9FDAC481477167131505D1A917E36F942CA3CD9 (void);
// 0x00000101 System.Void LargeDataFeed::AppendRealtimeWithDownSampling(System.Double,System.Double,System.Double)
extern void LargeDataFeed_AppendRealtimeWithDownSampling_mE0EEAE4C96CEEA9D77B9BD7EF64B1DDDC81563D8 (void);
// 0x00000102 System.Void LargeDataFeed::RestartDownSampleCount()
extern void LargeDataFeed_RestartDownSampleCount_m63C6E59B6A95239ECF4B3AACEDB04A2909417084 (void);
// 0x00000103 System.Void LargeDataFeed::AppendPointRealtime(System.Double,System.Double,System.Double)
extern void LargeDataFeed_AppendPointRealtime_mA62CB1D0137B7D9AE4F2431E08A608826945B4BA (void);
// 0x00000104 System.Void LargeDataFeed::Start()
extern void LargeDataFeed_Start_m76DD4C4F2B6691363FA41B511EE9662F4596BE18 (void);
// 0x00000105 ChartAndGraph.DoubleVector2 LargeDataFeed::GetLastPoint()
extern void LargeDataFeed_GetLastPoint_mB5D7BB8D4A8D5E24C7FA60300C265C2870DACF6F (void);
// 0x00000106 System.Void LargeDataFeed::SetInitialData()
extern void LargeDataFeed_SetInitialData_m00A49AA96A900D2CA09192FAFDE542BB5D2A31B4 (void);
// 0x00000107 System.Void LargeDataFeed::SaveToFile(System.String)
extern void LargeDataFeed_SaveToFile_m50BE8E45403FAC9B822EA6BF7394DF38C18438C3 (void);
// 0x00000108 System.Void LargeDataFeed::LoadFromFile(System.String)
extern void LargeDataFeed_LoadFromFile_m3810640434E9AFEB173A166825F1B358D20BFC2E (void);
// 0x00000109 System.Boolean LargeDataFeed::VerifySorted(System.Collections.Generic.List`1<ChartAndGraph.DoubleVector2>)
extern void LargeDataFeed_VerifySorted_mF05FA4896171594EBDD70619C4EDCF8BC5D94793 (void);
// 0x0000010A System.Void LargeDataFeed::OnDataLoaded()
extern void LargeDataFeed_OnDataLoaded_mFDB94465EE376F394C028C0C3D97C202C1817F7C (void);
// 0x0000010B System.Void LargeDataFeed::SetData(System.Collections.Generic.List`1<ChartAndGraph.DoubleVector2>)
extern void LargeDataFeed_SetData_mBC3C2E4EC425FA58837CFF7DE383360509887ECD (void);
// 0x0000010C System.Int32 LargeDataFeed::FindClosestIndex(System.Double)
extern void LargeDataFeed_FindClosestIndex_m6A3D0BD8423F1F5EF270B3059FF742283DB0E609 (void);
// 0x0000010D System.Double LargeDataFeed::get_PageSizeFactor()
extern void LargeDataFeed_get_PageSizeFactor_m8DAB8785A6ED21DFBBF5966D77F78B4B259D5684 (void);
// 0x0000010E System.Void LargeDataFeed::findPointsForPage(System.Double,System.Int32&,System.Int32&)
extern void LargeDataFeed_findPointsForPage_mD3C283F1279C1D260E1D0410CBA05107C55E984A (void);
// 0x0000010F System.Void LargeDataFeed::Update()
extern void LargeDataFeed_Update_m51C2C2AB1B9EDBB4FAC712A4D2F37A9F0FDA31B3 (void);
// 0x00000110 System.Void LargeDataFeed::LoadWithoutDownSampling(System.Int32,System.Int32)
extern void LargeDataFeed_LoadWithoutDownSampling_m19F74AAA12F7180BC674E2FE62BB1FA1CC80E2FC (void);
// 0x00000111 System.Void LargeDataFeed::LoadWithDownSampling(System.Int32,System.Int32)
extern void LargeDataFeed_LoadWithDownSampling_mCDA5EB0BFBD18B8E720034D6E9DFD47A61D32477 (void);
// 0x00000112 System.Void LargeDataFeed::LoadPage(System.Double)
extern void LargeDataFeed_LoadPage_mA282390F88AEE74C3DCD6FD633F52B15FC346FC2 (void);
// 0x00000113 System.Int32 LargeDataFeed::Compare(ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2)
extern void LargeDataFeed_Compare_m22E7EE022339655DD92F0A93AE5B287AEC912B34 (void);
// 0x00000114 System.Void LargeDataFeed::.ctor()
extern void LargeDataFeed__ctor_mB683B58D6D20001B2D4F29122D6AE9773AAAECE8 (void);
// 0x00000115 System.Void RealtimeAppend::Start()
extern void RealtimeAppend_Start_m757F71602AE019FBA3236C1F1C3DB51D1CD20F93 (void);
// 0x00000116 System.Void RealtimeAppend::Update()
extern void RealtimeAppend_Update_m2BCA69F6EEDA999A8D5B31705D9AEAF044F79FFC (void);
// 0x00000117 System.Void RealtimeAppend::.ctor()
extern void RealtimeAppend__ctor_m7E4215EC7EEB90620D719DB2798D6E732BEDCC93 (void);
// 0x00000118 System.Void Marker::Start()
extern void Marker_Start_m6A660AFF7D632ED278160FF9DD3A7342E9EADFAF (void);
// 0x00000119 System.Void Marker::Redraw()
extern void Marker_Redraw_m246E6B3C09D532542F35AC5B30E16CAED439498D (void);
// 0x0000011A System.Void Marker::Update()
extern void Marker_Update_mFACB0C819951C780128FD6B98D9C91154BA0866E (void);
// 0x0000011B System.Void Marker::.ctor()
extern void Marker__ctor_mBA175A38B7CB4A0F2876D1295CADB45D31D5AE81 (void);
// 0x0000011C System.Void MarkerSampleDataInitializer::Start()
extern void MarkerSampleDataInitializer_Start_mCFE52B6410A96944BACF8CA18A28871DFAEA0643 (void);
// 0x0000011D System.Void MarkerSampleDataInitializer::Update()
extern void MarkerSampleDataInitializer_Update_mDB86F4BAD15543184D23D74B423F379E404762EF (void);
// 0x0000011E System.Void MarkerSampleDataInitializer::.ctor()
extern void MarkerSampleDataInitializer__ctor_m34145AE40BB67D048C00402E05C8CD5B0F55A62B (void);
// 0x0000011F System.Void MarkerText::Start()
extern void MarkerText_Start_m8EE95DB6CF5050FFC960AB9A59C559D4171400DB (void);
// 0x00000120 System.Void MarkerText::Update()
extern void MarkerText_Update_m5942FFBC073F5735D0CB68B23CD0350F4FF9CD1A (void);
// 0x00000121 System.Void MarkerText::.ctor()
extern void MarkerText__ctor_m7604AC157CEE2ACB52DB0F4C0837D51C506D08AC (void);
// 0x00000122 System.Void MarkerTextLine::Start()
extern void MarkerTextLine_Start_m293CD9E5E04CF374A6EBCEB5EB01FCB38A376994 (void);
// 0x00000123 System.Void MarkerTextLine::Update()
extern void MarkerTextLine_Update_mC4F35EED56F4D1EE2A4F02F0114570A96D6933D7 (void);
// 0x00000124 System.Void MarkerTextLine::.ctor()
extern void MarkerTextLine__ctor_m36F15D21924079D9140DFABE7ED2635D65BCA54B (void);
// 0x00000125 System.Void PointHighlight::Start()
extern void PointHighlight_Start_mE83EEAA1E0C2BBD9B2F2477A8D46C83367E66BDD (void);
// 0x00000126 System.Void PointHighlight::Update()
extern void PointHighlight_Update_m492E83424CCE9CFB4475D12FB29E5858338E0F06 (void);
// 0x00000127 System.Collections.IEnumerator PointHighlight::SelectText(UnityEngine.UI.Text,UnityEngine.GameObject)
extern void PointHighlight_SelectText_m8A72511CDE4D7594F99C839614E4CD5C12AF43DE (void);
// 0x00000128 System.Void PointHighlight::ClearHighLight()
extern void PointHighlight_ClearHighLight_m6C71341422652AD7E13A6E70890CBBF8A90D0579 (void);
// 0x00000129 System.Void PointHighlight::RemoveText(PointHighlight/HighLight)
extern void PointHighlight_RemoveText_m11AE1F2D1C4CDB6369CF59D404C625E680EBF669 (void);
// 0x0000012A System.Void PointHighlight::PopText(System.String,UnityEngine.Vector3,System.Boolean)
extern void PointHighlight_PopText_m589FFCA748A2033ECDB2E24DC91AA1DC1C5FE454 (void);
// 0x0000012B System.Void PointHighlight::HighlightPoint(System.String,System.Int32)
extern void PointHighlight_HighlightPoint_mFED5373AE8C6DCBA1CEAA2E61AC3E9C3D6BDC1FF (void);
// 0x0000012C System.Void PointHighlight::.ctor()
extern void PointHighlight__ctor_m2C4255BDDF747D12F9A2C054A130834B19B99BB7 (void);
// 0x0000012D System.Void PointHighlight/HighLight::.ctor(UnityEngine.UI.Text,ChartAndGraph.ChartItemEffect)
extern void HighLight__ctor_mF2354A1A5BA3BB45548381281F7E565B30297233 (void);
// 0x0000012E System.Void PointHighlight/<>c::.cctor()
extern void U3CU3Ec__cctor_m592F40E01D57E4F774203188498C6888A853D6DD (void);
// 0x0000012F System.Void PointHighlight/<>c::.ctor()
extern void U3CU3Ec__ctor_m0D1AA3639356336DA13D4E0B840E69100C99E2EA (void);
// 0x00000130 System.Boolean PointHighlight/<>c::<Update>b__10_0(PointHighlight/HighLight)
extern void U3CU3Ec_U3CUpdateU3Eb__10_0_m173095F38045DD896ACC2D0177CEC4FA72A933A3 (void);
// 0x00000131 System.Void PointHighlight/<SelectText>d__11::.ctor(System.Int32)
extern void U3CSelectTextU3Ed__11__ctor_m3A5A14A90EDDBB09F4306CC9935BAB476EA0D3D0 (void);
// 0x00000132 System.Void PointHighlight/<SelectText>d__11::System.IDisposable.Dispose()
extern void U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m5D7B4F7984536AFA0FC52BED1C0B323B93EBB09A (void);
// 0x00000133 System.Boolean PointHighlight/<SelectText>d__11::MoveNext()
extern void U3CSelectTextU3Ed__11_MoveNext_mC6733F0CCDB24AB891E0C3AE239ACCF9E6D31EED (void);
// 0x00000134 System.Object PointHighlight/<SelectText>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF876D96FB9874E3A5498EBF30EDA4899359AB74C (void);
// 0x00000135 System.Void PointHighlight/<SelectText>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m9E9192BE9A6918BBBAE4E1CE01CE78D9250D66A1 (void);
// 0x00000136 System.Object PointHighlight/<SelectText>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m4FD7A47CBC202B781432238F82B2B019FDDC5878 (void);
// 0x00000137 System.Void TrackerLine::Start()
extern void TrackerLine_Start_m2ADA9CEED76770A873477E34C7A7619D8FEBE2FD (void);
// 0x00000138 System.Void TrackerLine::Redraw()
extern void TrackerLine_Redraw_m363BF67695D229ACFBE4A35B8D37A79C173ECFF0 (void);
// 0x00000139 System.Void TrackerLine::Update()
extern void TrackerLine_Update_m275952B7668A2F56972AAA4C5F57B65D0952DE55 (void);
// 0x0000013A System.Void TrackerLine::.ctor()
extern void TrackerLine__ctor_m7D7A21B1AD697005B2089E4689AC710F82028F31 (void);
// 0x0000013B System.Void highlightTest::Start()
extern void highlightTest_Start_m942E500F9ED5E4334B237A622F317A4ED147F12F (void);
// 0x0000013C System.Void highlightTest::Update()
extern void highlightTest_Update_m523180A6AA66C0C97D2AB7674559B933F710419F (void);
// 0x0000013D System.Void highlightTest::.ctor()
extern void highlightTest__ctor_mD5F4CD4618765C4046C15215F154D15E07F9A148 (void);
// 0x0000013E System.Void MixedCharts::Start()
extern void MixedCharts_Start_mCF02053BD18E51303EFF6CA09227CE7598AFF962 (void);
// 0x0000013F System.Collections.IEnumerator MixedCharts::FillGraphWait()
extern void MixedCharts_FillGraphWait_m4D334BD00C325E608B9863ACF308B1CFB44CDBF4 (void);
// 0x00000140 System.Void MixedCharts::FillGraph()
extern void MixedCharts_FillGraph_m5AC83EBB0AEB0BB7CC1FD2380D3CD370B9A16FB2 (void);
// 0x00000141 System.Void MixedCharts::Update()
extern void MixedCharts_Update_mA2AE0D5A046CEAB98A0A5ADAE870D8A706D658F3 (void);
// 0x00000142 System.Void MixedCharts::.ctor()
extern void MixedCharts__ctor_m9C343D27D4219352D21F64D7FD16074D8879E2B7 (void);
// 0x00000143 System.Void MixedCharts/<FillGraphWait>d__3::.ctor(System.Int32)
extern void U3CFillGraphWaitU3Ed__3__ctor_m605B8A9DF0DEF3C1D6455E9106738FEF84BB1D27 (void);
// 0x00000144 System.Void MixedCharts/<FillGraphWait>d__3::System.IDisposable.Dispose()
extern void U3CFillGraphWaitU3Ed__3_System_IDisposable_Dispose_m8252579F9A52AC98B6829F106FB8413592DB3A2A (void);
// 0x00000145 System.Boolean MixedCharts/<FillGraphWait>d__3::MoveNext()
extern void U3CFillGraphWaitU3Ed__3_MoveNext_mED58E3A42B5766194A71A83F5C35D42E15D68C51 (void);
// 0x00000146 System.Object MixedCharts/<FillGraphWait>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFillGraphWaitU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7564710146FBEBC473D30BAD1A81F231303B6F7 (void);
// 0x00000147 System.Void MixedCharts/<FillGraphWait>d__3::System.Collections.IEnumerator.Reset()
extern void U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_Reset_m5A91A4B97550476A7720F47010FEE9AB4FC11D8A (void);
// 0x00000148 System.Object MixedCharts/<FillGraphWait>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_get_Current_m349F3E4E8B051771D60B027E82B421A6949E0546 (void);
// 0x00000149 System.Void PieChartFeed::Start()
extern void PieChartFeed_Start_m2FAB703A1E5B06EC14B4ED209A238D6A32574A89 (void);
// 0x0000014A System.Void PieChartFeed::.ctor()
extern void PieChartFeed__ctor_mF02FD006D73FC177FE604D0C51864723707C76D2 (void);
// 0x0000014B System.Void RadarChartFeed::Start()
extern void RadarChartFeed_Start_m80026830A802376C7B2A1B6D500798AFF7C452E1 (void);
// 0x0000014C System.Void RadarChartFeed::Update()
extern void RadarChartFeed_Update_m94C77D25D55D9D6442009DF6FAF13646507BBD4E (void);
// 0x0000014D System.Void RadarChartFeed::.ctor()
extern void RadarChartFeed__ctor_mFB451A90D16F97B8C4A27610CF7B12C59C3A4181 (void);
// 0x0000014E System.Void RadarMaxValueCategory::Start()
extern void RadarMaxValueCategory_Start_m0FFD1C730DA015AFFDAA0D35CEA3DE8DAC9A3A13 (void);
// 0x0000014F System.Void RadarMaxValueCategory::Update()
extern void RadarMaxValueCategory_Update_m4F2E8F9BF8A8A37A0A141D214A75FE61EE6BE706 (void);
// 0x00000150 System.Void RadarMaxValueCategory::.ctor()
extern void RadarMaxValueCategory__ctor_m594CF6B8EA19D515F30C8E7EA70FDF85B78C25EF (void);
// 0x00000151 System.Void StreamingGraph::Start()
extern void StreamingGraph_Start_mD4A2B0315DEFC2F2CA28B7DA6FCE4490156EB999 (void);
// 0x00000152 System.Void StreamingGraph::Update()
extern void StreamingGraph_Update_mF656AE25CE7BDC3085CED169CF14D55086D1F358 (void);
// 0x00000153 System.Void StreamingGraph::.ctor()
extern void StreamingGraph__ctor_m2B8EC10B46F09D2CC5AB1C81F6A9C15510DBB805 (void);
// 0x00000154 System.Void WaveGraph::Start()
extern void WaveGraph_Start_mD2E2ECFE927A89ADA5822542D5F7E91BE61C58BD (void);
// 0x00000155 System.Void WaveGraph::Update()
extern void WaveGraph_Update_m1D74D5C07A8958747F6694657CAEE0ED9AD922EE (void);
// 0x00000156 System.Void WaveGraph::.ctor()
extern void WaveGraph__ctor_m4FCB3D355B36F108FA91CCA5BAAF79624C8FAA48 (void);
// 0x00000157 System.Void GraphRectZoom::Start()
extern void GraphRectZoom_Start_m534B0C62F408247F9E2DDF31E50F9A028CF56D5A (void);
// 0x00000158 System.Void GraphRectZoom::OnMarkerStart()
extern void GraphRectZoom_OnMarkerStart_m836CA5EDFDB91AAF2AF06A61A115DEDD03E4EB60 (void);
// 0x00000159 System.Void GraphRectZoom::OnMarkerMove()
extern void GraphRectZoom_OnMarkerMove_mFB5C7D98DC21435689BBECA36808C1BFAE8645D9 (void);
// 0x0000015A System.Void GraphRectZoom::OnMarkerEnd()
extern void GraphRectZoom_OnMarkerEnd_mB06D2925423EAFC0506A4D7A587D605F1D4DF92E (void);
// 0x0000015B System.Boolean GraphRectZoom::CheckBounds()
extern void GraphRectZoom_CheckBounds_m6A4DEC96C2364376EE8F4DBF05D770A7D6164128 (void);
// 0x0000015C System.Void GraphRectZoom::SetGraphZoom()
extern void GraphRectZoom_SetGraphZoom_m52CC2F738F74683AD95737D896FDE7179B76E238 (void);
// 0x0000015D System.Void GraphRectZoom::Update()
extern void GraphRectZoom_Update_m6B1B0E346B66C7646D64E68A6AE3CCCDD71BFD26 (void);
// 0x0000015E System.Void GraphRectZoom::.ctor()
extern void GraphRectZoom__ctor_mF8D23AE3521019A1DF4761F276EB96FC69834979 (void);
// 0x0000015F System.Void GraphZoom::Start()
extern void GraphZoom_Start_m941005FD417E54C5C3DA2DE6210B7A5C2D62A409 (void);
// 0x00000160 System.Void GraphZoom::OnEnable()
extern void GraphZoom_OnEnable_m1F4FF19C8E2C70D57A7CC464E7A33520FD2D7E7E (void);
// 0x00000161 System.Void GraphZoom::ResetZoomAnchor()
extern void GraphZoom_ResetZoomAnchor_mCC41534C5CADEF5EFC6585F5A67BF3A7433D7C69 (void);
// 0x00000162 System.Boolean GraphZoom::CompareWithError(UnityEngine.Vector3,UnityEngine.Vector3)
extern void GraphZoom_CompareWithError_m003C12026BDFFFA8A044FE7759F596C32E49CFDB (void);
// 0x00000163 System.Void GraphZoom::Update()
extern void GraphZoom_Update_mC06A6FF873100FCDCFBA2161563C6B35DFE99657 (void);
// 0x00000164 System.Void GraphZoom::.ctor()
extern void GraphZoom__ctor_m9E9E243260892F820508A31E107A1769E80B8A97 (void);
// 0x00000165 System.Void BasicSample::OnGUI()
extern void BasicSample_OnGUI_m6E672AACF828127DE47D9E00DD45A51B7F279CA2 (void);
// 0x00000166 System.Void BasicSample::WriteResult(System.String[])
extern void BasicSample_WriteResult_m32B9AB2FCE0D7A7176210712FB9FE91C27492954 (void);
// 0x00000167 System.Void BasicSample::WriteResult(System.String)
extern void BasicSample_WriteResult_m67D71AA635FF37335895929720C7FFA79F783F13 (void);
// 0x00000168 System.Void BasicSample::.ctor()
extern void BasicSample__ctor_m0836DDC774D6C525746E25A164BA323017461966 (void);
// 0x00000169 System.Void BasicSample::<OnGUI>b__1_0(System.String[])
extern void BasicSample_U3COnGUIU3Eb__1_0_m1A5B0705F6B217AA15C3D60BA3B8F957AE9A97DA (void);
// 0x0000016A System.Void BasicSample::<OnGUI>b__1_1(System.String[])
extern void BasicSample_U3COnGUIU3Eb__1_1_m854014CDE7FA4D215818F9CCE18CC1C06D8666BE (void);
// 0x0000016B System.Void BasicSample::<OnGUI>b__1_2(System.String)
extern void BasicSample_U3COnGUIU3Eb__1_2_m640031C29BC7B5CA6966D6C5A57C5FA0F42654C8 (void);
// 0x0000016C System.Void CanvasSampleOpenFileImage::UploadFile(System.String,System.String,System.String,System.Boolean)
extern void CanvasSampleOpenFileImage_UploadFile_m90C1945D5935BD89355A03E33F76A4236ABF3EFE (void);
// 0x0000016D System.Void CanvasSampleOpenFileImage::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSampleOpenFileImage_OnPointerDown_m7B12A59739CE20652F59FFE9D7AD1DDFB427B896 (void);
// 0x0000016E System.Void CanvasSampleOpenFileImage::OnFileUpload(System.String)
extern void CanvasSampleOpenFileImage_OnFileUpload_m5B739156016141EA135B533C32C19FF584884096 (void);
// 0x0000016F System.Collections.IEnumerator CanvasSampleOpenFileImage::OutputRoutine(System.String)
extern void CanvasSampleOpenFileImage_OutputRoutine_mC6AC6C7020F774BBBB5C1EC7D70113DAC9CE2AE1 (void);
// 0x00000170 System.Void CanvasSampleOpenFileImage::.ctor()
extern void CanvasSampleOpenFileImage__ctor_m502D7C88F0EAFFDDF1098646B4CBA5EC7EACCAC7 (void);
// 0x00000171 System.Void CanvasSampleOpenFileImage/<OutputRoutine>d__4::.ctor(System.Int32)
extern void U3COutputRoutineU3Ed__4__ctor_m98B984849785F14A4C8AB9C792DD30C7E2C217DA (void);
// 0x00000172 System.Void CanvasSampleOpenFileImage/<OutputRoutine>d__4::System.IDisposable.Dispose()
extern void U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m93AEB066101CFA725B073186A05AA61331643C68 (void);
// 0x00000173 System.Boolean CanvasSampleOpenFileImage/<OutputRoutine>d__4::MoveNext()
extern void U3COutputRoutineU3Ed__4_MoveNext_mE761C79A87795F9557283DB7D7552330836956CA (void);
// 0x00000174 System.Object CanvasSampleOpenFileImage/<OutputRoutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED88C4EFD5B495F4D2972BFFEDEDA8A4FA23A8BE (void);
// 0x00000175 System.Void CanvasSampleOpenFileImage/<OutputRoutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m5F856A6AF129DDDCD28467ABFEA0B6EC6E826F73 (void);
// 0x00000176 System.Object CanvasSampleOpenFileImage/<OutputRoutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m8F721991272364EAF9BEDDBBB389F230FF990E2A (void);
// 0x00000177 System.Void CanvasSampleOpenFileText::UploadFile(System.String,System.String,System.String,System.Boolean)
extern void CanvasSampleOpenFileText_UploadFile_m4E4B95184A67DCCA9DD2917CCC2DF9CEB29C208B (void);
// 0x00000178 System.Void CanvasSampleOpenFileText::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSampleOpenFileText_OnPointerDown_mB5C9CF79EBA459EF8242DA323855A69DE6911833 (void);
// 0x00000179 System.Void CanvasSampleOpenFileText::OnFileUpload(System.String)
extern void CanvasSampleOpenFileText_OnFileUpload_m962312A80189C05391D894DA031B283E66D52692 (void);
// 0x0000017A System.Collections.IEnumerator CanvasSampleOpenFileText::OutputRoutine(System.String)
extern void CanvasSampleOpenFileText_OutputRoutine_m6EC022ECFF5A958B50C9FD430032EE0A18972DD5 (void);
// 0x0000017B System.Void CanvasSampleOpenFileText::.ctor()
extern void CanvasSampleOpenFileText__ctor_mDCBA3E4C4721B3282B2809AA94E632BFAB1E26D5 (void);
// 0x0000017C System.Void CanvasSampleOpenFileText/<OutputRoutine>d__4::.ctor(System.Int32)
extern void U3COutputRoutineU3Ed__4__ctor_m6052F5D62A93EBD9C0E11383EB07CBD7660382CD (void);
// 0x0000017D System.Void CanvasSampleOpenFileText/<OutputRoutine>d__4::System.IDisposable.Dispose()
extern void U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m185B54E87BBFA49A5D93D56378C5D5B699C6D0E3 (void);
// 0x0000017E System.Boolean CanvasSampleOpenFileText/<OutputRoutine>d__4::MoveNext()
extern void U3COutputRoutineU3Ed__4_MoveNext_m705E3655AA67F21B4822796EFBCAEF8C1080EB74 (void);
// 0x0000017F System.Object CanvasSampleOpenFileText/<OutputRoutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD425B64024BB6BC1769FAD619E67F9F309A486D8 (void);
// 0x00000180 System.Void CanvasSampleOpenFileText/<OutputRoutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_mF82EE533AC3EFB9F1A356CA8603DE322C066D634 (void);
// 0x00000181 System.Object CanvasSampleOpenFileText/<OutputRoutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m95A29BAFA9A8371AEF30517414CE3A5CD502F8B9 (void);
// 0x00000182 System.Void CanvasSampleOpenFileTextMultiple::UploadFile(System.String,System.String,System.String,System.Boolean)
extern void CanvasSampleOpenFileTextMultiple_UploadFile_mA7D5D0A7F3A0745C9FF979546132225E1F4CC00B (void);
// 0x00000183 System.Void CanvasSampleOpenFileTextMultiple::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSampleOpenFileTextMultiple_OnPointerDown_mBA50A0847976B0736217F4D0794122C93597804B (void);
// 0x00000184 System.Void CanvasSampleOpenFileTextMultiple::OnFileUpload(System.String)
extern void CanvasSampleOpenFileTextMultiple_OnFileUpload_mA68667E8B5DB5196049132386BCE3B00D380E31E (void);
// 0x00000185 System.Collections.IEnumerator CanvasSampleOpenFileTextMultiple::OutputRoutine(System.String[])
extern void CanvasSampleOpenFileTextMultiple_OutputRoutine_mBC9DD3FC851968220AFE1A5B97FF7F0A81A830C2 (void);
// 0x00000186 System.Void CanvasSampleOpenFileTextMultiple::.ctor()
extern void CanvasSampleOpenFileTextMultiple__ctor_m8BDD97617CD22220F998809BF61AF929523D0235 (void);
// 0x00000187 System.Void CanvasSampleOpenFileTextMultiple/<OutputRoutine>d__4::.ctor(System.Int32)
extern void U3COutputRoutineU3Ed__4__ctor_mBDFF06C3C8D544BE9FBC7EA29B4790B7F3F288ED (void);
// 0x00000188 System.Void CanvasSampleOpenFileTextMultiple/<OutputRoutine>d__4::System.IDisposable.Dispose()
extern void U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_mEB828D2EA10D8A142380291F8820E27C7F743386 (void);
// 0x00000189 System.Boolean CanvasSampleOpenFileTextMultiple/<OutputRoutine>d__4::MoveNext()
extern void U3COutputRoutineU3Ed__4_MoveNext_mA793E0C183CAC70F902DCAD5155D5F37CD6BE7FB (void);
// 0x0000018A System.Object CanvasSampleOpenFileTextMultiple/<OutputRoutine>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57BAA75BD2D41E4F0A357D1679D79CF925EF1FD5 (void);
// 0x0000018B System.Void CanvasSampleOpenFileTextMultiple/<OutputRoutine>d__4::System.Collections.IEnumerator.Reset()
extern void U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m15778AB91FC3ABCC7AA558BFE3224F64EA05925E (void);
// 0x0000018C System.Object CanvasSampleOpenFileTextMultiple/<OutputRoutine>d__4::System.Collections.IEnumerator.get_Current()
extern void U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_mC1183837FBAE1B02788D12B358A1F890CE7D069F (void);
// 0x0000018D System.Void CanvasSampleSaveFileImage::Awake()
extern void CanvasSampleSaveFileImage_Awake_m8279E0A0C9F79F4705FA5D559052AB2A16B0BEF1 (void);
// 0x0000018E System.Void CanvasSampleSaveFileImage::DownloadFile(System.String,System.String,System.String,System.Byte[],System.Int32)
extern void CanvasSampleSaveFileImage_DownloadFile_mBBBD310465F04A8B8ABCDB1FBB5933904F08254D (void);
// 0x0000018F System.Void CanvasSampleSaveFileImage::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSampleSaveFileImage_OnPointerDown_m545F2AE468647734038CDE5278E5DD2ADB455B08 (void);
// 0x00000190 System.Void CanvasSampleSaveFileImage::OnFileDownload()
extern void CanvasSampleSaveFileImage_OnFileDownload_mBF87377B9C4E63F218254B1F9516E4E62AA6DE72 (void);
// 0x00000191 System.Void CanvasSampleSaveFileImage::.ctor()
extern void CanvasSampleSaveFileImage__ctor_m445DB33600F854C4C7A506D6AA806EB27E58258F (void);
// 0x00000192 System.Void CanvasSampleSaveFileText::DownloadFile(System.String,System.String,System.String,System.Byte[],System.Int32)
extern void CanvasSampleSaveFileText_DownloadFile_m19DA24D4A59805CB059BE18FA07007256499A4FE (void);
// 0x00000193 System.Void CanvasSampleSaveFileText::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CanvasSampleSaveFileText_OnPointerDown_mDAEA89215F6D4CF505D0BA6DCC292632834B981A (void);
// 0x00000194 System.Void CanvasSampleSaveFileText::OnFileDownload()
extern void CanvasSampleSaveFileText_OnFileDownload_m70504434EBFDA158BE2583629ED97C08D85604E3 (void);
// 0x00000195 System.Void CanvasSampleSaveFileText::.ctor()
extern void CanvasSampleSaveFileText__ctor_m12FE75537ECAF54983996FB25F589BBC106F19C1 (void);
// 0x00000196 System.String[] SFB.IStandaloneFileBrowser::OpenFilePanel(System.String,System.String,SFB.ExtensionFilter[],System.Boolean)
// 0x00000197 System.String[] SFB.IStandaloneFileBrowser::OpenFolderPanel(System.String,System.String,System.Boolean)
// 0x00000198 System.String SFB.IStandaloneFileBrowser::SaveFilePanel(System.String,System.String,System.String,SFB.ExtensionFilter[])
// 0x00000199 System.Void SFB.IStandaloneFileBrowser::OpenFilePanelAsync(System.String,System.String,SFB.ExtensionFilter[],System.Boolean,System.Action`1<System.String[]>)
// 0x0000019A System.Void SFB.IStandaloneFileBrowser::OpenFolderPanelAsync(System.String,System.String,System.Boolean,System.Action`1<System.String[]>)
// 0x0000019B System.Void SFB.IStandaloneFileBrowser::SaveFilePanelAsync(System.String,System.String,System.String,SFB.ExtensionFilter[],System.Action`1<System.String>)
// 0x0000019C System.Void SFB.ExtensionFilter::.ctor(System.String,System.String[])
extern void ExtensionFilter__ctor_m005AD482A024C782159F183FFD5579DBF4CD7887 (void);
// 0x0000019D System.Void SFB.StandaloneFileBrowser::.cctor()
extern void StandaloneFileBrowser__cctor_m2493A0EF5EBEADA117EA6563BF66748585BD9541 (void);
// 0x0000019E System.String[] SFB.StandaloneFileBrowser::OpenFilePanel(System.String,System.String,System.String,System.Boolean)
extern void StandaloneFileBrowser_OpenFilePanel_m2FE9F90D42A9265BBC2C374BD1DD3A49069B9098 (void);
// 0x0000019F System.String[] SFB.StandaloneFileBrowser::OpenFilePanel(System.String,System.String,SFB.ExtensionFilter[],System.Boolean)
extern void StandaloneFileBrowser_OpenFilePanel_m1206C5DCC43D26EC15F5037E21B1B9BB490BB45C (void);
// 0x000001A0 System.Void SFB.StandaloneFileBrowser::OpenFilePanelAsync(System.String,System.String,System.String,System.Boolean,System.Action`1<System.String[]>)
extern void StandaloneFileBrowser_OpenFilePanelAsync_m806F59EEE9E55C81803D1986A8F68B9D26000483 (void);
// 0x000001A1 System.Void SFB.StandaloneFileBrowser::OpenFilePanelAsync(System.String,System.String,SFB.ExtensionFilter[],System.Boolean,System.Action`1<System.String[]>)
extern void StandaloneFileBrowser_OpenFilePanelAsync_m39CA8A47A6C945AE7799D8CC901EE998F1CEFAC8 (void);
// 0x000001A2 System.String[] SFB.StandaloneFileBrowser::OpenFolderPanel(System.String,System.String,System.Boolean)
extern void StandaloneFileBrowser_OpenFolderPanel_mD17CCB7D2DDD75D9063D66E2D36D51272A388B79 (void);
// 0x000001A3 System.Void SFB.StandaloneFileBrowser::OpenFolderPanelAsync(System.String,System.String,System.Boolean,System.Action`1<System.String[]>)
extern void StandaloneFileBrowser_OpenFolderPanelAsync_mA7D50D60D47ACA560FE3C89DE40FEF6D859D5E72 (void);
// 0x000001A4 System.String SFB.StandaloneFileBrowser::SaveFilePanel(System.String,System.String,System.String,System.String)
extern void StandaloneFileBrowser_SaveFilePanel_m750A45BB58A0223D8F1F00E110FC8561733A9CA7 (void);
// 0x000001A5 System.String SFB.StandaloneFileBrowser::SaveFilePanel(System.String,System.String,System.String,SFB.ExtensionFilter[])
extern void StandaloneFileBrowser_SaveFilePanel_m7FE84762BA014DAE82BC87D9236272976C448B13 (void);
// 0x000001A6 System.Void SFB.StandaloneFileBrowser::SaveFilePanelAsync(System.String,System.String,System.String,System.String,System.Action`1<System.String>)
extern void StandaloneFileBrowser_SaveFilePanelAsync_mB1C994ED3411EE335514CBE4A29F8C9D28F4618C (void);
// 0x000001A7 System.Void SFB.StandaloneFileBrowser::SaveFilePanelAsync(System.String,System.String,System.String,SFB.ExtensionFilter[],System.Action`1<System.String>)
extern void StandaloneFileBrowser_SaveFilePanelAsync_m5BF3B698F3A0B2BBDB43DDC8C9CEDF1AFAB0B31A (void);
// 0x000001A8 System.Void SFB.StandaloneFileBrowser::.ctor()
extern void StandaloneFileBrowser__ctor_mC76A61DA78458B95631E10E194EB265C54179194 (void);
// 0x000001A9 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONNode::get_Tag()
// 0x000001AA GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m794CAD49418793072572FF0B76C678788DD70A5C (void);
// 0x000001AB System.Void GraphAndChartSimpleJSON.JSONNode::set_Item(System.Int32,GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_set_Item_m54443C1E880509EAF765765B8F2116AAD06F16D7 (void);
// 0x000001AC GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m2F810D89B8E8BC291F96EA31AA965F2CD56AAA52 (void);
// 0x000001AD System.Void GraphAndChartSimpleJSON.JSONNode::set_Item(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_set_Item_m71FFBF3F4D95690C885BD569ED4C9E32C9C616E1 (void);
// 0x000001AE System.String GraphAndChartSimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m2D883EA2A35F24E8ED1B543DD5B528B7EE1EEA77 (void);
// 0x000001AF System.Void GraphAndChartSimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_m0E14AE5ADF273C3B5007AB2283F8A149BB8EDF38 (void);
// 0x000001B0 System.Int32 GraphAndChartSimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m325702C7DA358809F2E67A55A5FC38D32CC6964D (void);
// 0x000001B1 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m47E1B8E1C9771B8484B0173F41B20AF62AF9E286 (void);
// 0x000001B2 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mDF6E7B0355D1B91D4255711CDA9030C31D915C0B (void);
// 0x000001B3 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mBC36FD7A7ABD4F62A0B447408A1486E37CE099DF (void);
// 0x000001B4 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m67FBFCDC6544EE60A9EF96502A52CED6FE4C3A59 (void);
// 0x000001B5 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m65F0BD407E70A32F9788DF0294D0BDC2E5BEBC2F (void);
// 0x000001B6 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mD22CEBA382CCB97D8920608BE73780F79209A000 (void);
// 0x000001B7 System.Boolean GraphAndChartSimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m6DC6F231D1C99C89D35DCA0E9CFB09C30F3211EE (void);
// 0x000001B8 System.Void GraphAndChartSimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_mFD2207D0BCDB10D9DB3A11CD30D40DC18B629DB8 (void);
// 0x000001B9 System.Void GraphAndChartSimpleJSON.JSONNode::Add(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_Add_mD86BF9C95349B6FE2867D0BD3DEEC2D7C87B7472 (void);
// 0x000001BA System.Void GraphAndChartSimpleJSON.JSONNode::Add(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_Add_m16EB0A81868783BC7600F795CA206B2243CADD67 (void);
// 0x000001BB GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m76E123309D5DEDB04B12443AB0C7FA821F18CD33 (void);
// 0x000001BC GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m234E9757857028D2185A26B1D125BFF0F55B112B (void);
// 0x000001BD GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::Remove(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_Remove_m2695928FBB81ACC4103A9D2481EBB4618ACCEB3E (void);
// 0x000001BE System.Collections.Generic.IEnumerable`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m9A419003FE3E6C582C581168325DFF2F916EA4E6 (void);
// 0x000001BF System.Collections.Generic.IEnumerable`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_mA4B063A992BD18F9254C2E4798DA93637ED05EEE (void);
// 0x000001C0 System.String GraphAndChartSimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_mB2B88FE44E1C28E46DA63108861FD2253915E3CF (void);
// 0x000001C1 System.String GraphAndChartSimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_mDCFC0B2C49FD5F86F8F9B87B901A798F35566880 (void);
// 0x000001C2 System.Void GraphAndChartSimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
// 0x000001C3 GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONNode::GetEnumerator()
// 0x000001C4 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,GraphAndChartSimpleJSON.JSONNode>> GraphAndChartSimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m034EE9CE2759B88857897900689A0DA38D2E0A88 (void);
// 0x000001C5 GraphAndChartSimpleJSON.JSONNode/KeyEnumerator GraphAndChartSimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_mEB33DEC4138BBDB5741ACC2707C371046102CB52 (void);
// 0x000001C6 GraphAndChartSimpleJSON.JSONNode/ValueEnumerator GraphAndChartSimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m14983FAA7ABFAA16F8D0833F5F69961D7A3775FE (void);
// 0x000001C7 System.Double GraphAndChartSimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m4FE7D6A1571A71C97F12B7199A200DAF6226822D (void);
// 0x000001C8 System.Void GraphAndChartSimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mD7BF0BF13A7DCA4BAF089D24BB96E4E18FCF5691 (void);
// 0x000001C9 System.Int32 GraphAndChartSimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_mC2BCD4318263CF5717BA098C4CEB6650F2462A6E (void);
// 0x000001CA System.Void GraphAndChartSimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m1E641BB048DC71F31439F97D7AC04F859C043669 (void);
// 0x000001CB System.Single GraphAndChartSimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m92DA2DA95D110DB99A8BEB14922E0C79E48F91A8 (void);
// 0x000001CC System.Void GraphAndChartSimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_mEB0ADE3AF3166894A27C91EF7442D99EBC7379F6 (void);
// 0x000001CD System.Boolean GraphAndChartSimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m6180FF46346F4B2FDB4676641483185606729746 (void);
// 0x000001CE System.Void GraphAndChartSimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_mA657BCFE82A9BCC2380C4725F6EFD697A54EFAC5 (void);
// 0x000001CF System.Int64 GraphAndChartSimpleJSON.JSONNode::get_AsLong()
extern void JSONNode_get_AsLong_mD41DE2D1B2C4922C0EACD72743CCF195877DF865 (void);
// 0x000001D0 System.Void GraphAndChartSimpleJSON.JSONNode::set_AsLong(System.Int64)
extern void JSONNode_set_AsLong_mE663F955EA3FC88B8D62D9D7178FDCA5A65D0B7C (void);
// 0x000001D1 GraphAndChartSimpleJSON.JSONArray GraphAndChartSimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m935EC207506B03E85E995C0E8281EB16D868E9EA (void);
// 0x000001D2 GraphAndChartSimpleJSON.JSONObject GraphAndChartSimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m3D777DBA0FE5C294C0B0FD3AAA95769700595C8F (void);
// 0x000001D3 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m1EA46A77855F3FC4A14E9EE9D71DDA4E212A4EA9 (void);
// 0x000001D4 System.String GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mB9E91405E9FB4D141BCAB7C0BB77156AF05EFF39 (void);
// 0x000001D5 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_mFEE3537E5F4B6B61E156471D4C9B144448483CF3 (void);
// 0x000001D6 System.Double GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m79BBDB2120CB96E6ADBC7FAAE8A935F087191987 (void);
// 0x000001D7 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_mC2C4DF10AB75CE2EAB0B550E4C94FA0974C02389 (void);
// 0x000001D8 System.Single GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m892C57C4F0D57F1FC33F3830D7F6D9DC516BA81F (void);
// 0x000001D9 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m12829D9BCEC10C160E56E670456EB34D0FD6F55B (void);
// 0x000001DA System.Int32 GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mF555AAFFA9ADEF6A0AFAB59E40EF53EDC2E13136 (void);
// 0x000001DB GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.Int64)
extern void JSONNode_op_Implicit_mCE3A6DC67C99802CD46822391901380ADEE56187 (void);
// 0x000001DC System.Int64 GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m6DE4662491A85E54BF525C11716CB121FC43E55A (void);
// 0x000001DD GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mC6864B10BE10BBE1A2B9BB7B13FCA926E01C00B4 (void);
// 0x000001DE System.Boolean GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m20BDA805DC04973A0B86DF2EEA0E1EC5294C2A80 (void);
// 0x000001DF GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,GraphAndChartSimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_mF66EA64CDA940B1CB6769E2311F01C56EEE7A669 (void);
// 0x000001E0 System.Boolean GraphAndChartSimpleJSON.JSONNode::op_Equality(GraphAndChartSimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_m4BADF8D78593A991CE67F962BF80C563709EF81F (void);
// 0x000001E1 System.Boolean GraphAndChartSimpleJSON.JSONNode::op_Inequality(GraphAndChartSimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m331AB33A8063DD8CF5A013EBEE1ACA8B36B8D82E (void);
// 0x000001E2 System.Boolean GraphAndChartSimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_m0DF2B3AC02B8FACD90E7199431586AE86E7881C9 (void);
// 0x000001E3 System.Int32 GraphAndChartSimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mC44AFA4860F2802B814A42AE17FDE50CD6A2CC2A (void);
// 0x000001E4 System.Text.StringBuilder GraphAndChartSimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_mFA295DED38D7E52FB2CC1E8239E9535ECF5C5818 (void);
// 0x000001E5 System.String GraphAndChartSimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_mCD71AA7D631E356B1221E932CD6B1B5B398A96C5 (void);
// 0x000001E6 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::ParseElement(System.String,System.Boolean)
extern void JSONNode_ParseElement_mB9BD86BAFDC3EA7D2B1749F9E4DD17414A333C5F (void);
// 0x000001E7 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_mB215C31C5F1D4FED0CA19BB8927328B0611FFD2A (void);
// 0x000001E8 System.Void GraphAndChartSimpleJSON.JSONNode::SerializeBinary(System.IO.BinaryWriter)
// 0x000001E9 System.Void GraphAndChartSimpleJSON.JSONNode::SaveToBinaryStream(System.IO.Stream)
extern void JSONNode_SaveToBinaryStream_m364D02A409828D427483CBD861175BADBEAD3E06 (void);
// 0x000001EA System.Void GraphAndChartSimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_mB3B0E7276E7311AB5617113BA6502AB9A9CE786F (void);
// 0x000001EB System.Void GraphAndChartSimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m926647312191674829BB64867DEFED85A98D777C (void);
// 0x000001EC System.String GraphAndChartSimpleJSON.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_m265B2EAEBF7C9392D363BD300A4455B838010CF5 (void);
// 0x000001ED System.Void GraphAndChartSimpleJSON.JSONNode::SaveToBinaryFile(System.String)
extern void JSONNode_SaveToBinaryFile_m1F93E968F938C66B9D70C39EAB4015E5B87C9869 (void);
// 0x000001EE System.String GraphAndChartSimpleJSON.JSONNode::SaveToBinaryBase64()
extern void JSONNode_SaveToBinaryBase64_m87A8FF3221C5C605927E2026169A7C2D8793871B (void);
// 0x000001EF GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::DeserializeBinary(System.IO.BinaryReader)
extern void JSONNode_DeserializeBinary_mD2AD63794CA1ED110E74BFA54DB2CA2BB64BDBBE (void);
// 0x000001F0 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m85F2969047E442D5A10478F80ED14E6DFC443224 (void);
// 0x000001F1 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_mAB76987DBDC8CA0EA873FE8FC2116CA91A71FDEE (void);
// 0x000001F2 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_m2D98F8A86854C743F1F0CB48D69872CB4CDB572F (void);
// 0x000001F3 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::LoadFromBinaryStream(System.IO.Stream)
extern void JSONNode_LoadFromBinaryStream_m0E1EB0742F235E299612B9B9D80067136D0D411F (void);
// 0x000001F4 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::LoadFromBinaryFile(System.String)
extern void JSONNode_LoadFromBinaryFile_m64AEB4673D02F096AD494E59CCE307A5BEDD6C3B (void);
// 0x000001F5 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::LoadFromBinaryBase64(System.String)
extern void JSONNode_LoadFromBinaryBase64_m5A8AC6DD582B02410615C4CC4FE3B9CFE33C7B44 (void);
// 0x000001F6 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::GetContainer(GraphAndChartSimpleJSON.JSONContainerType)
extern void JSONNode_GetContainer_m19833331A9B8CA83914B36966E0EE16DCD4F7E07 (void);
// 0x000001F7 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector2)
extern void JSONNode_op_Implicit_m8AE8B21A95048BF82C9461446DC5215E7E00057F (void);
// 0x000001F8 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector3)
extern void JSONNode_op_Implicit_m7088AB167559453B5CEDE102AE283D920CFE5D81 (void);
// 0x000001F9 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector4)
extern void JSONNode_op_Implicit_mD8FAEC77BB6747DDEF65E05BCAB8AE6ECADE37A5 (void);
// 0x000001FA GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(UnityEngine.Quaternion)
extern void JSONNode_op_Implicit_m92C7DCD9C1E79A6C9E7AF7DD108C3417C1402D37 (void);
// 0x000001FB GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(UnityEngine.Rect)
extern void JSONNode_op_Implicit_mB9D115AA85C0415A0310D55A3D865A48D14D594E (void);
// 0x000001FC GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::op_Implicit(UnityEngine.RectOffset)
extern void JSONNode_op_Implicit_m26E17AC2E55A65696F6730C97AE2915BF012E3F6 (void);
// 0x000001FD UnityEngine.Vector2 GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m8FFCBC62D03992E5237841E9031F21119DF1015B (void);
// 0x000001FE UnityEngine.Vector3 GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mCA03D4B5A8E753F9EDB1ECEBC49D6266889C4C55 (void);
// 0x000001FF UnityEngine.Vector4 GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mDC6A9D6E4B47E7624919689D8D168530F99D2666 (void);
// 0x00000200 UnityEngine.Quaternion GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mF76ED311632806F15586B03E77302A7962FB7248 (void);
// 0x00000201 UnityEngine.Rect GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m335CE1D01340558BA5E810898EA46700C139EF2C (void);
// 0x00000202 UnityEngine.RectOffset GraphAndChartSimpleJSON.JSONNode::op_Implicit(GraphAndChartSimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m3882CE159C237C946365B1DA6680CAB8B97B8D10 (void);
// 0x00000203 UnityEngine.Vector2 GraphAndChartSimpleJSON.JSONNode::ReadVector2(UnityEngine.Vector2)
extern void JSONNode_ReadVector2_m08534E2F214BE9E8247E07BF9B42D2E0E9213BAB (void);
// 0x00000204 UnityEngine.Vector2 GraphAndChartSimpleJSON.JSONNode::ReadVector2(System.String,System.String)
extern void JSONNode_ReadVector2_m91EEEC3B22770A79B7DE52BA9AC7DE248D5B0AFD (void);
// 0x00000205 UnityEngine.Vector2 GraphAndChartSimpleJSON.JSONNode::ReadVector2()
extern void JSONNode_ReadVector2_m0711D27FE35DABB91E76DC672653BFE32BEF15A9 (void);
// 0x00000206 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteVector2(UnityEngine.Vector2,System.String,System.String)
extern void JSONNode_WriteVector2_m2581E822B71A15A42BFE7E878E81AE7D07480B6C (void);
// 0x00000207 UnityEngine.Vector3 GraphAndChartSimpleJSON.JSONNode::ReadVector3(UnityEngine.Vector3)
extern void JSONNode_ReadVector3_m140E8784E3FF083C49AADA7CF7C1AAD6935D2ABD (void);
// 0x00000208 UnityEngine.Vector3 GraphAndChartSimpleJSON.JSONNode::ReadVector3(System.String,System.String,System.String)
extern void JSONNode_ReadVector3_mDC7EEA763492B0C0C37EF950A10736F206527D0F (void);
// 0x00000209 UnityEngine.Vector3 GraphAndChartSimpleJSON.JSONNode::ReadVector3()
extern void JSONNode_ReadVector3_mFDDA5015918CF9C6735B09ADBF27EB14A2572A5F (void);
// 0x0000020A GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteVector3(UnityEngine.Vector3,System.String,System.String,System.String)
extern void JSONNode_WriteVector3_m20BB306E4F882EA4DCCC37F590907B3E089362FB (void);
// 0x0000020B UnityEngine.Vector4 GraphAndChartSimpleJSON.JSONNode::ReadVector4(UnityEngine.Vector4)
extern void JSONNode_ReadVector4_mFBB8B7B9CE8BCB9F9477F753C1C558BD7CD2A61D (void);
// 0x0000020C UnityEngine.Vector4 GraphAndChartSimpleJSON.JSONNode::ReadVector4()
extern void JSONNode_ReadVector4_mCBE7BA3FC58F4DDB2964E84565B9AFAC0BD250AA (void);
// 0x0000020D GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteVector4(UnityEngine.Vector4)
extern void JSONNode_WriteVector4_mB812B63E5073CD324360D3064C05BE697F329455 (void);
// 0x0000020E UnityEngine.Quaternion GraphAndChartSimpleJSON.JSONNode::ReadQuaternion(UnityEngine.Quaternion)
extern void JSONNode_ReadQuaternion_m738AAA0E05CA15EDE900EBC18CA7F46B51B1BDF1 (void);
// 0x0000020F UnityEngine.Quaternion GraphAndChartSimpleJSON.JSONNode::ReadQuaternion()
extern void JSONNode_ReadQuaternion_m453397A50EF15F27D8A1214B6AFB1F819B6EE6D7 (void);
// 0x00000210 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteQuaternion(UnityEngine.Quaternion)
extern void JSONNode_WriteQuaternion_m883421C39E9D7AB9BB71786747025EC9E4D3DBFC (void);
// 0x00000211 UnityEngine.Rect GraphAndChartSimpleJSON.JSONNode::ReadRect(UnityEngine.Rect)
extern void JSONNode_ReadRect_m8344137C21516678BA98CF5F2C9FBF5F70759787 (void);
// 0x00000212 UnityEngine.Rect GraphAndChartSimpleJSON.JSONNode::ReadRect()
extern void JSONNode_ReadRect_mA43F4F30C279F39B71CA440A7D9E30B2050FB660 (void);
// 0x00000213 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteRect(UnityEngine.Rect)
extern void JSONNode_WriteRect_m79D99D1042AE7D7CA7F2BCFE9CFB90D812B8028D (void);
// 0x00000214 UnityEngine.RectOffset GraphAndChartSimpleJSON.JSONNode::ReadRectOffset(UnityEngine.RectOffset)
extern void JSONNode_ReadRectOffset_m78C6B5191BB6D70FC582CCA571E08D6F416A9389 (void);
// 0x00000215 UnityEngine.RectOffset GraphAndChartSimpleJSON.JSONNode::ReadRectOffset()
extern void JSONNode_ReadRectOffset_mFC531ED84BCA06A2E04BFAAA847F5BA45506B7E2 (void);
// 0x00000216 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteRectOffset(UnityEngine.RectOffset)
extern void JSONNode_WriteRectOffset_mE2786B33C174CEBE7A785AB871019EDBB72690A6 (void);
// 0x00000217 UnityEngine.Matrix4x4 GraphAndChartSimpleJSON.JSONNode::ReadMatrix()
extern void JSONNode_ReadMatrix_m16BED0622885EE7E8C2F8476570AAA5B99BFC3B2 (void);
// 0x00000218 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode::WriteMatrix(UnityEngine.Matrix4x4)
extern void JSONNode_WriteMatrix_mB5FE155E4F5D8AFE0FE59C1D2715BF4C43B0AD53 (void);
// 0x00000219 System.Void GraphAndChartSimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mAC243E2F0FCCE1CED0086918A168DA00553E37EF (void);
// 0x0000021A System.Void GraphAndChartSimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mEE1261316A49FF10A2C347B45C165513A07C1D8B (void);
// 0x0000021B System.Boolean GraphAndChartSimpleJSON.JSONNode/Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_m0DF87B70D0CB8C46C3B337005248A54B7C86979F (void);
// 0x0000021C System.Void GraphAndChartSimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.List`1/Enumerator<GraphAndChartSimpleJSON.JSONNode>)
extern void Enumerator__ctor_mF85148FE3692D3904C073278CFE8716EB9B4B0E2 (void);
// 0x0000021D System.Void GraphAndChartSimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,GraphAndChartSimpleJSON.JSONNode>)
extern void Enumerator__ctor_mA42ACFD81450787E450330C66FE5CBB9FD0644EC (void);
// 0x0000021E System.Collections.Generic.KeyValuePair`2<System.String,GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONNode/Enumerator::get_Current()
extern void Enumerator_get_Current_m80F00207F02904E3A63CA549AD1E34DB016C4A47 (void);
// 0x0000021F System.Boolean GraphAndChartSimpleJSON.JSONNode/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m9947F7EE949531A65662E0BA157E3534993C36BF (void);
// 0x00000220 System.Void GraphAndChartSimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<GraphAndChartSimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_mA049E3670B0C9E04EFFA9CD8702AA9A74DB9C718 (void);
// 0x00000221 System.Void GraphAndChartSimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,GraphAndChartSimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m65F47E21C6E8DBB2A363B8FCF67709863FF36C35 (void);
// 0x00000222 System.Void GraphAndChartSimpleJSON.JSONNode/ValueEnumerator::.ctor(GraphAndChartSimpleJSON.JSONNode/Enumerator)
extern void ValueEnumerator__ctor_mC832E5CC32931EE67681635AA27CF2ED262630ED (void);
// 0x00000223 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode/ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_m285C54105C3F05F978CD31F37025375C39C83542 (void);
// 0x00000224 System.Boolean GraphAndChartSimpleJSON.JSONNode/ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mEA96FAD65F2306BBC4B323984BD480FE6D2DB5AF (void);
// 0x00000225 GraphAndChartSimpleJSON.JSONNode/ValueEnumerator GraphAndChartSimpleJSON.JSONNode/ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_m44A09F9E247CBB2F7542237EF47BD0A985FC3663 (void);
// 0x00000226 System.Void GraphAndChartSimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<GraphAndChartSimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mD632C7FC751F302A79EC4073C7DFF80C496266F6 (void);
// 0x00000227 System.Void GraphAndChartSimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,GraphAndChartSimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m7D1CC885A67D308EED89D61D67DBD02D4D17DE79 (void);
// 0x00000228 System.Void GraphAndChartSimpleJSON.JSONNode/KeyEnumerator::.ctor(GraphAndChartSimpleJSON.JSONNode/Enumerator)
extern void KeyEnumerator__ctor_m7F24F46E4952C1FC737D3F638603E33208952562 (void);
// 0x00000229 System.String GraphAndChartSimpleJSON.JSONNode/KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_mF0ABCA5D5FEBDF2C7FC8BFD989D44026434E463B (void);
// 0x0000022A System.Boolean GraphAndChartSimpleJSON.JSONNode/KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_m063B0761C6BC5BB84B8A9ADEBE965D51E8958D2A (void);
// 0x0000022B GraphAndChartSimpleJSON.JSONNode/KeyEnumerator GraphAndChartSimpleJSON.JSONNode/KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_mA4473824DD3BB4D3421080111235E5A58757FF5D (void);
// 0x0000022C System.Void GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::.ctor(GraphAndChartSimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m52746D99CFC8343CD5EEB9C652BBE1C6D818155C (void);
// 0x0000022D System.Collections.Generic.KeyValuePair`2<System.String,GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_mFE8D852F1F4E3279DAA9518FBF7D940715746C06 (void);
// 0x0000022E System.Object GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m7D26432CB580B89B4C9023756070C7A31F15BC3C (void);
// 0x0000022F System.Boolean GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_mB0FFCA63B015A030053490951E34F49F79ABFB73 (void);
// 0x00000230 System.Void GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_m8E4D9BBBEED3B31C2CC1CAAA68A7215CCCFE2EDA (void);
// 0x00000231 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,GraphAndChartSimpleJSON.JSONNode>> GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_mBDFE8718EE63E4A1ACE83CF84503100810519E59 (void);
// 0x00000232 System.Void GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_mF72A212DEF6A83497140DAC59BD22C5EE70EC24C (void);
// 0x00000233 System.Collections.IEnumerator GraphAndChartSimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m8C2ACB3FBF5A13CEF75897520E969725DF5C6F64 (void);
// 0x00000234 System.Void GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__40__ctor_mD2664979A1B8AE875DDB2840B6156772FB3ECFDD (void);
// 0x00000235 System.Void GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m944D00543660D19F0E42EC6B6B139CB5D7EDFECC (void);
// 0x00000236 System.Boolean GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::MoveNext()
extern void U3Cget_ChildrenU3Ed__40_MoveNext_m453C570B96B371691FD54DB0A8DDE5E3943D4532 (void);
// 0x00000237 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::System.Collections.Generic.IEnumerator<GraphAndChartSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m4FC76B45630A04E0A0046A1409F00FB5FABE46B9 (void);
// 0x00000238 System.Void GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_m97CD00E06349E3DE3CAD68B058AE2EA97797AEDC (void);
// 0x00000239 System.Object GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_m8F61A110C435979412391A15BC54115CA7E0DEE5 (void);
// 0x0000023A System.Collections.Generic.IEnumerator`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::System.Collections.Generic.IEnumerable<GraphAndChartSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_mD91303D1AB77DD3B2593235773ED3B39DF1C2895 (void);
// 0x0000023B System.Collections.IEnumerator GraphAndChartSimpleJSON.JSONNode/<get_Children>d__40::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m8E4D404140934BAD769BAA96C71954DF437DBEB4 (void);
// 0x0000023C System.Void GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__42__ctor_m1AD4A760497B8C4CAB4E547E035B8ED4EBDDBFAB (void);
// 0x0000023D System.Void GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_m75E2DD336D0A67E1EA6196D218C1F2D9E26D2242 (void);
// 0x0000023E System.Boolean GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__42_MoveNext_m41D74C189B9AC160605BE456F1413E3553FB50DD (void);
// 0x0000023F System.Void GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally1_m17CEE5F8A4091B0148EF7B35CBCC74D44DBE889B (void);
// 0x00000240 System.Void GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally2_mF90971AD878C5B085E0FB7E610C4E0D66638E83D (void);
// 0x00000241 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.Generic.IEnumerator<GraphAndChartSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_mF52E7C66AC2CFE035C5422378EBA10E005BD16FD (void);
// 0x00000242 System.Void GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_m093AE80D972ACB3ED4CF25C53041441B789576A5 (void);
// 0x00000243 System.Object GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_mDC85D32E36F7E3EEA9C509E44985F3540F4659A4 (void);
// 0x00000244 System.Collections.Generic.IEnumerator`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.Generic.IEnumerable<GraphAndChartSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m3E11223B21486BF7B377ED4B07EA9557C32ED634 (void);
// 0x00000245 System.Collections.IEnumerator GraphAndChartSimpleJSON.JSONNode/<get_DeepChildren>d__42::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mB972D9FF70DEDEBBC7507696C8353787B73D448A (void);
// 0x00000246 System.Boolean GraphAndChartSimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mF3E98F07FB29748FF607D07002E1C6A5F655B661 (void);
// 0x00000247 System.Void GraphAndChartSimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_m6159E8B429821AFA42475DB06771F2912736C625 (void);
// 0x00000248 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_mC315B2AB653862B003EA9B2ED0107A5197C459F4 (void);
// 0x00000249 System.Boolean GraphAndChartSimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_mAB3875FA6030E364164F591576AD6A06A4184DEF (void);
// 0x0000024A GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_mDCA4E401C30BAFA976195E0EBA00FC512FB09F78 (void);
// 0x0000024B GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_m80D71FF8EF86521ECCBD31757FB9A95CAD530F7A (void);
// 0x0000024C System.Void GraphAndChartSimpleJSON.JSONArray::set_Item(System.Int32,GraphAndChartSimpleJSON.JSONNode)
extern void JSONArray_set_Item_m5A43B0FA2E75E4C41DD67AF7440FA1889A745037 (void);
// 0x0000024D GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_mAAFA9403173B73244F3E047CB922F13E97BCE76A (void);
// 0x0000024E System.Void GraphAndChartSimpleJSON.JSONArray::set_Item(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONArray_set_Item_mBA3DB6911937ED140443C9F48FD611585F5950C2 (void);
// 0x0000024F System.Int32 GraphAndChartSimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m98BA6144D0680CBA5A32FA14A45C27864E79932E (void);
// 0x00000250 System.Void GraphAndChartSimpleJSON.JSONArray::Add(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONArray_Add_m768ED67829CECDA6214620ECAEFBC5E5959E80EF (void);
// 0x00000251 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m541331871627A71D31DC30A0B29E4A380D81EEE4 (void);
// 0x00000252 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONArray::Remove(GraphAndChartSimpleJSON.JSONNode)
extern void JSONArray_Remove_mBEEFAA1049D8BF88BB5E132A8701E9D23E18FA7F (void);
// 0x00000253 System.Collections.Generic.IEnumerable`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_m7459647CAFE8FB57E0C4D2F757AA72BCA7749CEC (void);
// 0x00000254 System.Void GraphAndChartSimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_mA146E626CA9AFB66DDA87A6BCF7DB0F707207D3E (void);
// 0x00000255 System.Void GraphAndChartSimpleJSON.JSONArray::SerializeBinary(System.IO.BinaryWriter)
extern void JSONArray_SerializeBinary_mFBB242F279540C196367396678815DDCE69E1748 (void);
// 0x00000256 System.Void GraphAndChartSimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_mF4CAE833013E2A698568D4367805C2C51D0D25D2 (void);
// 0x00000257 System.Void GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m09E100DFDE814208000E288F38AACAA139288972 (void);
// 0x00000258 System.Void GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mFBB72D4FC09BA522703066660A1B6C84A2B2662E (void);
// 0x00000259 System.Boolean GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m5842B46A70689B08A17107B86E6DFBB3300F458F (void);
// 0x0000025A System.Void GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m3252C32520E6F43AFE392CB6919CFE1BC2B6F0E3 (void);
// 0x0000025B GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerator<GraphAndChartSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m0997099AAF452C670AC89A3DC8EB6F3F42EF8A2E (void);
// 0x0000025C System.Void GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mA44FDA37FE9633368C64D524CC7FFBD664058879 (void);
// 0x0000025D System.Object GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mDB53E925A7A6DC776A4F6BBB37B8E7AE9A7B4ACC (void);
// 0x0000025E System.Collections.Generic.IEnumerator`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerable<GraphAndChartSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m85636C898F51CA71FB68F27A5F0E3AE34CE40854 (void);
// 0x0000025F System.Collections.IEnumerator GraphAndChartSimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m2724CBA0FE19F63D89407542FD0085D90ACBEC0A (void);
// 0x00000260 System.Boolean GraphAndChartSimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_m9BCB66E97A2F88F44B775108A4B5E18F17888A2F (void);
// 0x00000261 System.Void GraphAndChartSimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_mC3F731FE78812DAA7E3A70CAE5A72EBC5758D8CC (void);
// 0x00000262 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m03F79226FA30F84478F294ECCB99AB694861CF4F (void);
// 0x00000263 System.Boolean GraphAndChartSimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m67321BA44A6C4D8BB5A1438A879E688A2E7DE28D (void);
// 0x00000264 GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m63EE29843A910F89E618BB31706D3854868BFFDE (void);
// 0x00000265 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_m72B96FA02E6771FD0C8E75C91BEDF9606634B7F5 (void);
// 0x00000266 System.Void GraphAndChartSimpleJSON.JSONObject::set_Item(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONObject_set_Item_mEF09124374182C135B41DB681FB7C415DB31C41D (void);
// 0x00000267 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_mF51497D1C688FBF842D2190AED82F139F5F78FE4 (void);
// 0x00000268 System.Void GraphAndChartSimpleJSON.JSONObject::set_Item(System.Int32,GraphAndChartSimpleJSON.JSONNode)
extern void JSONObject_set_Item_m322B2304A66474BFFC5D15FC609C1769E04EC673 (void);
// 0x00000269 System.Int32 GraphAndChartSimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m726D1A71A88913BA27964996BAC70CD2EBA8570B (void);
// 0x0000026A System.Void GraphAndChartSimpleJSON.JSONObject::Add(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONObject_Add_m6B4706D9DE2281401B6D81C078FD52D9AE761CE2 (void);
// 0x0000026B GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m0AAFED5BF3EBBD4C7FC06F2EB5F5732C7AE89506 (void);
// 0x0000026C GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m27288429F778385D9F072DB4595DFADED9C46C2F (void);
// 0x0000026D GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONObject::Remove(GraphAndChartSimpleJSON.JSONNode)
extern void JSONObject_Remove_m2F2F348D5A73770DBBB900718FF31C8FAC263EA5 (void);
// 0x0000026E System.Collections.Generic.IEnumerable`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_mBA3F815A9E4AA185FB9D3B41346F80D3D89FCEEB (void);
// 0x0000026F System.Void GraphAndChartSimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_mF7874C61E8E876BD5458DC7B33C6342873C3ABB6 (void);
// 0x00000270 System.Void GraphAndChartSimpleJSON.JSONObject::SerializeBinary(System.IO.BinaryWriter)
extern void JSONObject_SerializeBinary_m49377B90B1A9A0AC2B41C2B9972789F0DEEF79AF (void);
// 0x00000271 System.Void GraphAndChartSimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_mF4C8923987460A3B2D0B5B58CA337717B5D92899 (void);
// 0x00000272 System.Void GraphAndChartSimpleJSON.JSONObject/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mC5A02DDB9380C3E29DD7EE6239418532874371A1 (void);
// 0x00000273 System.Boolean GraphAndChartSimpleJSON.JSONObject/<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,GraphAndChartSimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m201C2480DFA39BCC11721101323F5BECBBA1E5E4 (void);
// 0x00000274 System.Void GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_m27FE9E533731FF76A4DA5A1495777DE4976EB2B5 (void);
// 0x00000275 System.Void GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m14D80EBCF6DEF404F017771F3E97B465C4E7B66A (void);
// 0x00000276 System.Boolean GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m3ECB5D4B30C8D5A240916179E1D34DAB38B6AB68 (void);
// 0x00000277 System.Void GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m90DD4395C8759994DE05DF05C9298C2B4F31A560 (void);
// 0x00000278 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerator<GraphAndChartSimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m406355CB8A2EB21F8264E1A23C80A1921AB1C795 (void);
// 0x00000279 System.Void GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mC76C5EF6AE752D074A3A428BEBAA2A1CCAB85232 (void);
// 0x0000027A System.Object GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m99340EAA456C373D91FF62F016A592BBCFDAEC01 (void);
// 0x0000027B System.Collections.Generic.IEnumerator`1<GraphAndChartSimpleJSON.JSONNode> GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerable<GraphAndChartSimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m1ADC7153B59830633F604BBAB11DC65F176AC157 (void);
// 0x0000027C System.Collections.IEnumerator GraphAndChartSimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m78AC7A7CE03612379624BE539ABBE50619F0149C (void);
// 0x0000027D GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_mCF53FBECB4BDEABC5574CF3439C685136FA99B9F (void);
// 0x0000027E System.Boolean GraphAndChartSimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mFD6A68DD25465F32A5EE6CCF96D6E454EBAEC5EA (void);
// 0x0000027F GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_mBFB6072AEFD5212172A37AB3E2357B6459511768 (void);
// 0x00000280 System.String GraphAndChartSimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_mA01365AAB55A95B641FA6BC66A9B3B23A47EB125 (void);
// 0x00000281 System.Void GraphAndChartSimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m4F50250EDF80AA623B85137C07068C3E1A010C8E (void);
// 0x00000282 System.Void GraphAndChartSimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_mB62055173E2C53E15EBB4D505F19FACE8A0BF37F (void);
// 0x00000283 System.Void GraphAndChartSimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_m159FDFF47242E33DD85F18C19AE7092397D18375 (void);
// 0x00000284 System.Boolean GraphAndChartSimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_m6BAB16DFE509FAD6FEE6AE480A036B7BDEA5D2A4 (void);
// 0x00000285 System.Int32 GraphAndChartSimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_mE751F12FDD59253CD819097906EAC9CF751BA10E (void);
// 0x00000286 System.Void GraphAndChartSimpleJSON.JSONString::SerializeBinary(System.IO.BinaryWriter)
extern void JSONString_SerializeBinary_m52D938CE59B95C44733F2403B81EAF780A30AB00 (void);
// 0x00000287 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m024BFC581B538629B7D2FAD15CEB077F2F9E3279 (void);
// 0x00000288 System.Boolean GraphAndChartSimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_mFB0F9F911212685A1310E8BA979644E88512E786 (void);
// 0x00000289 GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m7C9A36F1FB2609D397B11CBCF864720927F00A77 (void);
// 0x0000028A System.String GraphAndChartSimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m82E5E24487E19999083957C1AF260876F6EDA3F6 (void);
// 0x0000028B System.Void GraphAndChartSimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_mF166132B90DB5D575E20840B950FFE991C430728 (void);
// 0x0000028C System.Double GraphAndChartSimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m8B128C3B233ACBB22607C9D9CC55F90C8FFD4886 (void);
// 0x0000028D System.Void GraphAndChartSimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_mFB47CE3DD2520105AABB9D2DBCEB4AF98C564E36 (void);
// 0x0000028E System.Int64 GraphAndChartSimpleJSON.JSONNumber::get_AsLong()
extern void JSONNumber_get_AsLong_m245F4F4F46DD1873684A486D5A83F2D6D1B6EB88 (void);
// 0x0000028F System.Void GraphAndChartSimpleJSON.JSONNumber::set_AsLong(System.Int64)
extern void JSONNumber_set_AsLong_mC3399978ABB03B713E5E552F7878785AC0A56FB3 (void);
// 0x00000290 System.Void GraphAndChartSimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mF62A940B0E8E278BB2A3E96DE923AFF31B8E2FD1 (void);
// 0x00000291 System.Void GraphAndChartSimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m9A89C54F6EB8B7131AA1FB60D3CB47FEFB43E748 (void);
// 0x00000292 System.Void GraphAndChartSimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_mD0D48E0E46FED6E80C7F0FC038F385CE0EB23CEE (void);
// 0x00000293 System.Boolean GraphAndChartSimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mA7C80E2D8BAF67F3631BE93450FFFF552FFD0CEC (void);
// 0x00000294 System.Boolean GraphAndChartSimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mB1E8BD369ACF8A5330E5342061992567C6EB5B9A (void);
// 0x00000295 System.Int32 GraphAndChartSimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_mBCCC23C533A875C641F49F87CFCBD3E7A55EC6FF (void);
// 0x00000296 System.Void GraphAndChartSimpleJSON.JSONNumber::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNumber_SerializeBinary_m75AF22D10F227ABEF1E50B48AC9079C6BFDA295E (void);
// 0x00000297 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mFEC25FE31384718FCA71A25AFB90BD8FCFF82E7C (void);
// 0x00000298 System.Boolean GraphAndChartSimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m9FBCF0252BDDB22CFBB61A6A7E5DF716BFCC9DA3 (void);
// 0x00000299 GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mC419198BFED4A5878607F4977393029D43E7D462 (void);
// 0x0000029A System.String GraphAndChartSimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mAD379D54D1808097239F0BD40ECBDA0A1F2B41DF (void);
// 0x0000029B System.Void GraphAndChartSimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_m4C612D8CF5720CC1C69C66D5F4BF84325EF727FC (void);
// 0x0000029C System.Boolean GraphAndChartSimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_m6B3A77EE68C4EB21286885998407068F2BEEBA42 (void);
// 0x0000029D System.Void GraphAndChartSimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_mEFF054DBE90574E01A90AC547B21A8F645075B69 (void);
// 0x0000029E System.Void GraphAndChartSimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_m80476A3F2422A1617C1A7D5F1D0B0F05AB3793F4 (void);
// 0x0000029F System.Void GraphAndChartSimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_mBB081C7E3DE25C02EC0A7F3151CEF3E1ED297746 (void);
// 0x000002A0 System.Void GraphAndChartSimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_mD74EE78BFD28A1A78EEE832027DEC0ED92B93F3E (void);
// 0x000002A1 System.Boolean GraphAndChartSimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m22F59C35F0BC4174ADDAF3F54F48B0E845F8D167 (void);
// 0x000002A2 System.Int32 GraphAndChartSimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_mBF084C8ED32DA5C68C0FD033CEF8BBDF7B24A2E7 (void);
// 0x000002A3 System.Void GraphAndChartSimpleJSON.JSONBool::SerializeBinary(System.IO.BinaryWriter)
extern void JSONBool_SerializeBinary_m83EAFC1CDB6FAF99455803C977BBDB20DB70F12B (void);
// 0x000002A4 GraphAndChartSimpleJSON.JSONNull GraphAndChartSimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_m732F03FC4B1B326999FCC84E58DDBB4B46845B86 (void);
// 0x000002A5 System.Void GraphAndChartSimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_mE33227FE33336DA77DDF210DA89CE5A493A0D6B1 (void);
// 0x000002A6 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m1F12D6FC09303C5A73AB7B49BC174BE76F4216BF (void);
// 0x000002A7 System.Boolean GraphAndChartSimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_mC2E0E55DE67FCDDA18C01EF4B3A80DB8446A1C34 (void);
// 0x000002A8 GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m228222FB6E86CA2C4462C26608CF8D305960BE74 (void);
// 0x000002A9 System.String GraphAndChartSimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m4B46351EFCA2D41C2A136CECC211B0E7B95CF154 (void);
// 0x000002AA System.Void GraphAndChartSimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_m523880E340A52C95FB87F10A36222F42038818B8 (void);
// 0x000002AB System.Boolean GraphAndChartSimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_mC82D3992D82908950C4D5EF20D7F133653139D48 (void);
// 0x000002AC System.Void GraphAndChartSimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_mC92BCA0758D456516F83731B8A90A02C53F8F187 (void);
// 0x000002AD System.Boolean GraphAndChartSimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_mAED9044EF895937E717029B2466B6EAAA07D5283 (void);
// 0x000002AE System.Int32 GraphAndChartSimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m01DEEA5B857D405F4B9D7C523ADF1D10A8C4978F (void);
// 0x000002AF System.Void GraphAndChartSimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m599C035DD34051A49E96CBE32A948E557F3816DA (void);
// 0x000002B0 System.Void GraphAndChartSimpleJSON.JSONNull::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNull_SerializeBinary_m685B6B1CF8FB32ABD6CCF2E7664A9DB8C4FEB6FF (void);
// 0x000002B1 System.Void GraphAndChartSimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_mE6B7842FB044E55A54B9FC5803EFE681848E89FF (void);
// 0x000002B2 GraphAndChartSimpleJSON.JSONNodeType GraphAndChartSimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m53B8355F7E34B93B4FBE03E0DEA97ACC30A1CF0D (void);
// 0x000002B3 GraphAndChartSimpleJSON.JSONNode/Enumerator GraphAndChartSimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m6A362148ABC47D9D0E3AE58AC5429F0BF89295F3 (void);
// 0x000002B4 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::.ctor(GraphAndChartSimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m0206B2AD3CDF575600494DD3147D6600797E193C (void);
// 0x000002B5 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::.ctor(GraphAndChartSimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mE118EADFCC80752174DCBCD309862389F5A1A536 (void);
// 0x000002B6 T GraphAndChartSimpleJSON.JSONLazyCreator::Set(T)
// 0x000002B7 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_mCEF9DF7DE4A82B301CA3F1079E105B502C8D58FF (void);
// 0x000002B8 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_Item(System.Int32,GraphAndChartSimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m6F38849DAD672D158B3F3D0FE2287E341FDC1B5B (void);
// 0x000002B9 GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m9ABC4BCDD6712304064A7B03323E7F82CCE93DD5 (void);
// 0x000002BA System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_Item(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m9B0845ED7851163245504BEF8383024D10ADD343 (void);
// 0x000002BB System.Void GraphAndChartSimpleJSON.JSONLazyCreator::Add(GraphAndChartSimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m11466E9A4417AF2CDEB892F2A69C9847EBA7DB04 (void);
// 0x000002BC System.Void GraphAndChartSimpleJSON.JSONLazyCreator::Add(System.String,GraphAndChartSimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m9C55AE044732E0A31FD0DA3E66BA1F4718CB326D (void);
// 0x000002BD System.Boolean GraphAndChartSimpleJSON.JSONLazyCreator::op_Equality(GraphAndChartSimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mAB716C3C3AA936D1349287DDDC9BA2BC91ADED33 (void);
// 0x000002BE System.Boolean GraphAndChartSimpleJSON.JSONLazyCreator::op_Inequality(GraphAndChartSimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m898E7A136513B0849CFF6EDA7B0C8D64943F4B11 (void);
// 0x000002BF System.Boolean GraphAndChartSimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_mE85720D4743CF81DCEA41A4291EEE37F970C428A (void);
// 0x000002C0 System.Int32 GraphAndChartSimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_mF79437592680711E849C0080F66BA81E7E51EF12 (void);
// 0x000002C1 System.Int32 GraphAndChartSimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m1C73B790A7C3C5AE693EAFBA706F4D0A5C540632 (void);
// 0x000002C2 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m5A16A8383AF6DDDC8A5D2829DA5FE4EAB5644BFA (void);
// 0x000002C3 System.Single GraphAndChartSimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m97909CA726018D15C4FF72F1F1C7433C10201B10 (void);
// 0x000002C4 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m331F57597BC524EA61394EED0CF8B88EFB60E603 (void);
// 0x000002C5 System.Double GraphAndChartSimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_mBAE70F3DFCCE90D520D84AB402AEA5B95544F36E (void);
// 0x000002C6 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m2CDB70171988285F108CA0CCF97E0DA3C4BD9459 (void);
// 0x000002C7 System.Int64 GraphAndChartSimpleJSON.JSONLazyCreator::get_AsLong()
extern void JSONLazyCreator_get_AsLong_m4BCE83EF678D35AF8C8E790003018D8940A25988 (void);
// 0x000002C8 System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_AsLong(System.Int64)
extern void JSONLazyCreator_set_AsLong_m8466EAA77C575251F031F27AB836D1F25C83F96F (void);
// 0x000002C9 System.Boolean GraphAndChartSimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mB627E755CA8E9BF328A8328726F6779E80CA8FB1 (void);
// 0x000002CA System.Void GraphAndChartSimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_mFD07371E1640B276E9F05A8F1BDB5A4E561DBC52 (void);
// 0x000002CB GraphAndChartSimpleJSON.JSONArray GraphAndChartSimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mDC3B3A51F41F93743ABA027B349A38F8118EAAD0 (void);
// 0x000002CC GraphAndChartSimpleJSON.JSONObject GraphAndChartSimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m3152F723BAB90B8EBE72A8D8D89ED2941A3588BE (void);
// 0x000002CD System.Void GraphAndChartSimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,GraphAndChartSimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m09BD0EB0DDB7908638241F408E76A0CA6022E973 (void);
// 0x000002CE System.Void GraphAndChartSimpleJSON.JSONLazyCreator::SerializeBinary(System.IO.BinaryWriter)
extern void JSONLazyCreator_SerializeBinary_m38363CDF296E0870C9A84AEC5CDFB39430006E4A (void);
// 0x000002CF GraphAndChartSimpleJSON.JSONNode GraphAndChartSimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_m72FD3AB5C5FDA9B75DCAFE8CB48D5CA5CB0F5F1B (void);
// 0x000002D0 System.Boolean ChartAndGraph.ChartParser::SetPathRelativeTo(System.String)
// 0x000002D1 System.Object ChartAndGraph.ChartParser::GetObject(System.String)
// 0x000002D2 System.Object ChartAndGraph.ChartParser::GetChildObject(System.Object,System.String)
// 0x000002D3 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> ChartAndGraph.ChartParser::GetAllChildObjects(System.Object)
// 0x000002D4 System.String ChartAndGraph.ChartParser::GetChildObjectValue(System.Object,System.String)
// 0x000002D5 System.String ChartAndGraph.ChartParser::GetItem(System.Object,System.Int32)
// 0x000002D6 System.Object ChartAndGraph.ChartParser::GetItemObject(System.Object,System.Int32)
// 0x000002D7 System.Int32 ChartAndGraph.ChartParser::GetArraySize(System.Object)
// 0x000002D8 System.String ChartAndGraph.ChartParser::ObjectValue(System.Object)
// 0x000002D9 System.Void ChartAndGraph.ChartParser::.ctor()
extern void ChartParser__ctor_mFC87BF1464F23D6F3E3F271CBA8EB3E0F5131467 (void);
// 0x000002DA System.Void ChartAndGraph.JsonParser::.ctor(System.String)
extern void JsonParser__ctor_m3370E225292D26A54D9429E0C8A74F52D833D4F2 (void);
// 0x000002DB System.Object ChartAndGraph.JsonParser::GetObjectFromRoot(GraphAndChartSimpleJSON.JSONNode,System.String)
extern void JsonParser_GetObjectFromRoot_m1FAF3E8B1FBF861E1D01D41C4890AE54FA0E0963 (void);
// 0x000002DC System.Int32 ChartAndGraph.JsonParser::GetArraySize(System.Object)
extern void JsonParser_GetArraySize_m468DDADF873849630ABFDA65E978073DD737262A (void);
// 0x000002DD System.Object ChartAndGraph.JsonParser::GetChildObject(System.Object,System.String)
extern void JsonParser_GetChildObject_m6CE8DEFD882A4F8C021C4A3A05402EDC6ACF847F (void);
// 0x000002DE System.Boolean ChartAndGraph.JsonParser::SetPathRelativeTo(System.String)
extern void JsonParser_SetPathRelativeTo_mDFAB6EA6BC254C43865DAA8A56DECE101A487087 (void);
// 0x000002DF System.Object ChartAndGraph.JsonParser::GetObject(System.String)
extern void JsonParser_GetObject_mA1B881FE53685438DF67E1EFF6A4380D0CB6E818 (void);
// 0x000002E0 System.String ChartAndGraph.JsonParser::GetItem(System.Object,System.Int32)
extern void JsonParser_GetItem_mD37ECE8DFD5749000C5DBDC84CF19D3F04851BBA (void);
// 0x000002E1 System.Object ChartAndGraph.JsonParser::GetItemObject(System.Object,System.Int32)
extern void JsonParser_GetItemObject_mE7193D47CF776DBCBB8013282374DAB6FFBEE6EB (void);
// 0x000002E2 System.String ChartAndGraph.JsonParser::ObjectValue(System.Object)
extern void JsonParser_ObjectValue_m9C9517A85AD54752E721D2CB26924B153F28767C (void);
// 0x000002E3 System.String ChartAndGraph.JsonParser::GetChildObjectValue(System.Object,System.String)
extern void JsonParser_GetChildObjectValue_m0F455908D2B593C596F40F0A3D1F84344F08CCF7 (void);
// 0x000002E4 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> ChartAndGraph.JsonParser::GetAllChildObjects(System.Object)
extern void JsonParser_GetAllChildObjects_m64B3F53686284293C50CD18063C787EF61B153C2 (void);
// 0x000002E5 System.Void ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::.ctor(System.Int32)
extern void U3CGetAllChildObjectsU3Ed__12__ctor_m47139479FE975B344E47217D280FFC312E318714 (void);
// 0x000002E6 System.Void ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::System.IDisposable.Dispose()
extern void U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_m8BBB18472020BE742C8E39B81239FDACD74663BB (void);
// 0x000002E7 System.Boolean ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::MoveNext()
extern void U3CGetAllChildObjectsU3Ed__12_MoveNext_m75ECF193AAA94198E0494722EB8E9E3A2F8A8547 (void);
// 0x000002E8 System.Collections.Generic.KeyValuePair`2<System.String,System.Object> ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,System.Object>>.get_Current()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m24515EF29C62E5B28F804336D64C95621B543A8A (void);
// 0x000002E9 System.Void ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::System.Collections.IEnumerator.Reset()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_mD2374F91492CA89E5E1A6224468E705D910E38CC (void);
// 0x000002EA System.Object ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m3715E650CF1A0C57AAA0F132B149816F130815C2 (void);
// 0x000002EB System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.String,System.Object>>.GetEnumerator()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_mEC7AFF580DB38394CB779DFBD45E50A202643EF1 (void);
// 0x000002EC System.Collections.IEnumerator ChartAndGraph.JsonParser/<GetAllChildObjects>d__12::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE9A7F7C95471C247081CF66366D9A4DA207933EF (void);
// 0x000002ED System.Void ChartAndGraph.XMLParser::.ctor(System.String)
extern void XMLParser__ctor_mC2BD2EE105D8144DF44EBE297D4CB0E0366811A1 (void);
// 0x000002EE System.Int32 ChartAndGraph.XMLParser::GetArraySize(System.Object)
extern void XMLParser_GetArraySize_m7A880B43FF36FCF46B828EA6812341E88D5D481D (void);
// 0x000002EF System.Object ChartAndGraph.XMLParser::GetChildObject(System.Object,System.String)
extern void XMLParser_GetChildObject_m22F41552E6625205629E822B906F5E239154B4AC (void);
// 0x000002F0 System.Object ChartAndGraph.XMLParser::GetItemObject(System.Object,System.Int32)
extern void XMLParser_GetItemObject_m70D2A87E037F0BA55F700A78650AC6DADB764C41 (void);
// 0x000002F1 System.Object ChartAndGraph.XMLParser::GetObjectFromRoot(System.Xml.XmlElement,System.String)
extern void XMLParser_GetObjectFromRoot_mD8457DD1700A172276FAFDC2EE9228267DE87B63 (void);
// 0x000002F2 System.Object ChartAndGraph.XMLParser::GetObject(System.String)
extern void XMLParser_GetObject_m1E1EF741DA53BE600E11DA48E16E641EA7D1AC60 (void);
// 0x000002F3 System.Boolean ChartAndGraph.XMLParser::SetPathRelativeTo(System.String)
extern void XMLParser_SetPathRelativeTo_mC3E0EB5AB619DFDF14D52B81255DD6B6D30C9349 (void);
// 0x000002F4 System.String ChartAndGraph.XMLParser::GetChildObjectValue(System.Object,System.String)
extern void XMLParser_GetChildObjectValue_m605A3851D801EC974B78781DF5A4711E90493F21 (void);
// 0x000002F5 System.String ChartAndGraph.XMLParser::GetItem(System.Object,System.Int32)
extern void XMLParser_GetItem_mEE30A375F346E7F4A515F79E127CF81A2F9E4EFC (void);
// 0x000002F6 System.String ChartAndGraph.XMLParser::ObjectValue(System.Object)
extern void XMLParser_ObjectValue_mF0D45AFA6EC54B750A79EBC31EC902BFDF6969DB (void);
// 0x000002F7 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> ChartAndGraph.XMLParser::GetAllChildObjects(System.Object)
extern void XMLParser_GetAllChildObjects_mE2F140C257C62461EF9667E77BF1BD013EE8459B (void);
// 0x000002F8 System.Void ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::.ctor(System.Int32)
extern void U3CGetAllChildObjectsU3Ed__12__ctor_m5531B147F08A6B3597DD2C73FF2550DD018683FA (void);
// 0x000002F9 System.Void ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::System.IDisposable.Dispose()
extern void U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_mD6F2D843626DF724B8E34D5DF9B05FDD7D826D98 (void);
// 0x000002FA System.Boolean ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::MoveNext()
extern void U3CGetAllChildObjectsU3Ed__12_MoveNext_m117865738C1733E7C59C8A24969F3D5B54CA3B46 (void);
// 0x000002FB System.Void ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::<>m__Finally1()
extern void U3CGetAllChildObjectsU3Ed__12_U3CU3Em__Finally1_mA395032706EF755A2A81E576B142FB02F1C575A8 (void);
// 0x000002FC System.Collections.Generic.KeyValuePair`2<System.String,System.Object> ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::System.Collections.Generic.IEnumerator<System.Collections.Generic.KeyValuePair<System.String,System.Object>>.get_Current()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m5D93105895C6A0F0C06B8B11727EA7C8B91C8E8F (void);
// 0x000002FD System.Void ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::System.Collections.IEnumerator.Reset()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_m5FB1E2B73B3C5F5EA1DA48A97191F445FCB1CE0C (void);
// 0x000002FE System.Object ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m24F99202D95EB103F25D1F71E0FDC04BED1A8CCE (void);
// 0x000002FF System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<System.String,System.Object>>.GetEnumerator()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_m8F633C74F5985C71F8C1B945DF1A566EEF7DDDFE (void);
// 0x00000300 System.Collections.IEnumerator ChartAndGraph.XMLParser/<GetAllChildObjects>d__12::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m36887960DD50A7F7F13F9B101A78F4D3FCCA2741 (void);
// 0x00000301 ChartAndGraph.ChartSparseDataSource ChartAndGraph.IInternalPyramidData::get_InternalDataSource()
// 0x00000302 UnityEngine.GameObject ChartAndGraph.IPyramidGenerator::get_ContainerObject()
// 0x00000303 System.Void ChartAndGraph.IPyramidGenerator::SetParams(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
// 0x00000304 UnityEngine.Vector3 ChartAndGraph.IPyramidGenerator::GetTextPosition(ChartAndGraph.PyramidChart/JustificationType,System.Boolean)
// 0x00000305 System.Void ChartAndGraph.IPyramidGenerator::GetUpperBase(System.Single&,System.Single&)
// 0x00000306 System.Void ChartAndGraph.IPyramidGenerator::Generate()
// 0x00000307 System.Void ChartAndGraph.IPyramidGenerator::ApplyInfo(System.String,System.String,UnityEngine.Sprite,System.Single)
// 0x00000308 System.Void ChartAndGraph.IPyramidGenerator::SetAlpha(System.Single)
// 0x00000309 System.Void ChartAndGraph.PyramidCanvasGenerator::Start()
extern void PyramidCanvasGenerator_Start_m3FD5C291056718D4050620325B625045E7DF3BA7 (void);
// 0x0000030A UnityEngine.GameObject ChartAndGraph.PyramidCanvasGenerator::get_ContainerObject()
extern void PyramidCanvasGenerator_get_ContainerObject_mF8C4A7BEE596075328CAAFA304660C06E97D6874 (void);
// 0x0000030B System.Void ChartAndGraph.PyramidCanvasGenerator::SetAlpha(System.Single)
extern void PyramidCanvasGenerator_SetAlpha_m6D31F97B91C4F90B8CDCCEE69B38DAEBD2F0360C (void);
// 0x0000030C UnityEngine.Texture ChartAndGraph.PyramidCanvasGenerator::get_mainTexture()
extern void PyramidCanvasGenerator_get_mainTexture_m7689D2D85E95A5BA7AAF4F26A0F5E07E604C726D (void);
// 0x0000030D System.Void ChartAndGraph.PyramidCanvasGenerator::ApplyInfo(System.String,System.String,UnityEngine.Sprite,System.Single)
extern void PyramidCanvasGenerator_ApplyInfo_m46E514A59C53D3B241A3E7A7408B22E033D7007D (void);
// 0x0000030E System.Void ChartAndGraph.PyramidCanvasGenerator::Generate()
extern void PyramidCanvasGenerator_Generate_m61F52416A217571B83C5D1695A2BF90E06841752 (void);
// 0x0000030F UnityEngine.Vector3 ChartAndGraph.PyramidCanvasGenerator::GetTextPosition(ChartAndGraph.PyramidChart/JustificationType,System.Boolean)
extern void PyramidCanvasGenerator_GetTextPosition_mCBB29082C7CBE5CE08345E9370317FD00230F870 (void);
// 0x00000310 System.Void ChartAndGraph.PyramidCanvasGenerator::GetUpperBase(System.Single&,System.Single&)
extern void PyramidCanvasGenerator_GetUpperBase_mCDC8FC8388FD6D454B07A4EFB737EDAC8F4D6211 (void);
// 0x00000311 System.Void ChartAndGraph.PyramidCanvasGenerator::SetParams(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PyramidCanvasGenerator_SetParams_m72A8D7C514DE3E8F16E9049FAEE31B2BA70E0F5A (void);
// 0x00000312 System.Void ChartAndGraph.PyramidCanvasGenerator::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void PyramidCanvasGenerator_OnFillVBO_m4A0C9DBB6343FF5AE039327B564122F5AD8EEBEF (void);
// 0x00000313 System.Void ChartAndGraph.PyramidCanvasGenerator::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void PyramidCanvasGenerator_OnPopulateMesh_m906AF1F35339B3DE9EEE6473722C45DF8F341D4D (void);
// 0x00000314 System.Void ChartAndGraph.PyramidCanvasGenerator::FillChartMesh(ChartAndGraph.IChartMesh)
extern void PyramidCanvasGenerator_FillChartMesh_mC4C1E3ABFE0085955920E42D2CA974E6CA738E74 (void);
// 0x00000315 System.Void ChartAndGraph.PyramidCanvasGenerator::.ctor()
extern void PyramidCanvasGenerator__ctor_mCE0D1A7CFA6D38C2464DD6EBF858F4E38E311735 (void);
// 0x00000316 UnityEngine.Material ChartAndGraph.PyramidChart::get_BackMaterial()
extern void PyramidChart_get_BackMaterial_mA56FED0A62D50CCC1BA8FE6628F12DD6F155DDD4 (void);
// 0x00000317 System.Void ChartAndGraph.PyramidChart::set_BackMaterial(UnityEngine.Material)
extern void PyramidChart_set_BackMaterial_mD3C2503EBCF20CBE7C4B2872836CCBB51CB38158 (void);
// 0x00000318 System.Single ChartAndGraph.PyramidChart::get_Inset()
extern void PyramidChart_get_Inset_m41BC9867B37961D8350A4B65521C1159F27D0F22 (void);
// 0x00000319 System.Void ChartAndGraph.PyramidChart::set_Inset(System.Single)
extern void PyramidChart_set_Inset_m2B98A95C28A0F324C6B726C6807499918388E075 (void);
// 0x0000031A ChartAndGraph.PyramidChart/JustificationType ChartAndGraph.PyramidChart::get_Justification()
extern void PyramidChart_get_Justification_m87E5C87281A74D69FE117ECEA3F7950FFF7B33D2 (void);
// 0x0000031B System.Void ChartAndGraph.PyramidChart::set_Justification(ChartAndGraph.PyramidChart/JustificationType)
extern void PyramidChart_set_Justification_mF088405EBDD438388812A4FAB53B159230D22ADA (void);
// 0x0000031C ChartAndGraph.PyramidChart/SlopeType ChartAndGraph.PyramidChart::get_Slope()
extern void PyramidChart_get_Slope_mEF559480DC7E8088381B7FCDC5CDFE6EAE6E1135 (void);
// 0x0000031D System.Void ChartAndGraph.PyramidChart::set_Slope(ChartAndGraph.PyramidChart/SlopeType)
extern void PyramidChart_set_Slope_mF48C8331B5DDF56829D6BD035649D2D5A15BDB22 (void);
// 0x0000031E ChartAndGraph.PyramidCanvasGenerator ChartAndGraph.PyramidChart::get_Prefab()
extern void PyramidChart_get_Prefab_m037CE0A74EBF3D86928BDF562FFC0CE018D92CC8 (void);
// 0x0000031F System.Void ChartAndGraph.PyramidChart::set_Prefab(ChartAndGraph.PyramidCanvasGenerator)
extern void PyramidChart_set_Prefab_mC0A56F73E54ACF4F56B7F9884EB1EBB94ECAE30D (void);
// 0x00000320 ChartAndGraph.IChartData ChartAndGraph.PyramidChart::get_DataLink()
extern void PyramidChart_get_DataLink_m823270B2DE66C2CA1FDEE7C89F3FF7CFA79CA632 (void);
// 0x00000321 ChartAndGraph.PyramidData ChartAndGraph.PyramidChart::get_DataSource()
extern void PyramidChart_get_DataSource_mAB35BD540CF8E1E3D625AB03B9B4B2712F419135 (void);
// 0x00000322 System.Boolean ChartAndGraph.PyramidChart::get_SupportRealtimeGeneration()
extern void PyramidChart_get_SupportRealtimeGeneration_mAC47FB57D2B1B814FD2567B1B3CFF8429F12BD4C (void);
// 0x00000323 ChartAndGraph.LegenedData ChartAndGraph.PyramidChart::get_LegendInfo()
extern void PyramidChart_get_LegendInfo_mC2A52E5FB65AF2ED9F28B47F845FA3B4E4F273F9 (void);
// 0x00000324 System.Void ChartAndGraph.PyramidChart::.ctor()
extern void PyramidChart__ctor_m1ECF15F73E0F47D6A49CB71365C9C8589B168BF9 (void);
// 0x00000325 System.Void ChartAndGraph.PyramidChart::HookEvents()
extern void PyramidChart_HookEvents_mC0D642331413A4CA682B4B2B4B89CB15BE280EDF (void);
// 0x00000326 System.Void ChartAndGraph.PyramidChart::Data_RealtimeProperyUpdated()
extern void PyramidChart_Data_RealtimeProperyUpdated_m8DA02EB25812767C5DBA92CF5F7DA56B73E20B02 (void);
// 0x00000327 System.Void ChartAndGraph.PyramidChart::Data_ProperyUpdated()
extern void PyramidChart_Data_ProperyUpdated_m0E47011984476F715C5884037B9DB410FEB5532A (void);
// 0x00000328 System.Void ChartAndGraph.PyramidChart::QuickInvalidate()
extern void PyramidChart_QuickInvalidate_m88299B9CB7451106B08F8F8B7AEB9E8B1ED1F2C0 (void);
// 0x00000329 System.Void ChartAndGraph.PyramidChart::Invalidate()
extern void PyramidChart_Invalidate_m77A18AB00C5DA0D7F8F78159F48F8DEAB8CAEE4E (void);
// 0x0000032A System.Void ChartAndGraph.PyramidChart::MDataSource_DataValueChanged(System.Object,ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs)
extern void PyramidChart_MDataSource_DataValueChanged_m68E1556109EF4FF996560C8393EFEBAFFD843CCC (void);
// 0x0000032B System.Void ChartAndGraph.PyramidChart::MDataSource_DataStructureChanged(System.Object,System.EventArgs)
extern void PyramidChart_MDataSource_DataStructureChanged_m21380B60596C8753D6B9E4B7683E7B7FAEC20D63 (void);
// 0x0000032C System.Void ChartAndGraph.PyramidChart::Start()
extern void PyramidChart_Start_m836088CE367D839887D784312D025E3CA176BAE0 (void);
// 0x0000032D System.Void ChartAndGraph.PyramidChart::OnValidate()
extern void PyramidChart_OnValidate_mAE7FB632A76C2A537B6368A5B4E206CAEF8EA156 (void);
// 0x0000032E System.Void ChartAndGraph.PyramidChart::ClearChart()
extern void PyramidChart_ClearChart_m6A895DCE7183794A03B275CC2F765FEB45C34493 (void);
// 0x0000032F UnityEngine.Vector3 ChartAndGraph.PyramidChart::AlignTextPosition(ChartAndGraph.AlignedItemLabels,ChartAndGraph.PyramidChart/PyramidObject,ChartAndGraph.IPyramidGenerator,System.Single)
extern void PyramidChart_AlignTextPosition_m02188F7550276DBA58024FF433D2BCE09183A585 (void);
// 0x00000330 ChartAndGraph.IPyramidGenerator ChartAndGraph.PyramidChart::PreparePyramidObject(UnityEngine.GameObject&)
extern void PyramidChart_PreparePyramidObject_m8438E904C23C8FC45EB10F8B3B864E32D62E55CB (void);
// 0x00000331 System.Void ChartAndGraph.PyramidChart::GeneratePyramid(System.Boolean)
extern void PyramidChart_GeneratePyramid_mFFA8E14066A08D78094F9FCA24D106AEFF55392D (void);
// 0x00000332 System.Void ChartAndGraph.PyramidChart::OnLabelSettingChanged()
extern void PyramidChart_OnLabelSettingChanged_m9A4CFD3C3B20DE7933B212D1303F0EB17FD34CC3 (void);
// 0x00000333 System.Void ChartAndGraph.PyramidChart::OnLabelSettingsSet()
extern void PyramidChart_OnLabelSettingsSet_mDAD07C62AE2E5699A53D88126CC3477FE8348C81 (void);
// 0x00000334 System.Void ChartAndGraph.PyramidChart::InternalGenerateChart()
extern void PyramidChart_InternalGenerateChart_m269E27B790C02F6F5960733B708BF1D86896836B (void);
// 0x00000335 System.Boolean ChartAndGraph.PyramidChart::HasValues(ChartAndGraph.AxisBase)
extern void PyramidChart_HasValues_m26003D484862783E61C3BE483F915F0D564EB0EF (void);
// 0x00000336 System.Double ChartAndGraph.PyramidChart::MaxValue(ChartAndGraph.AxisBase)
extern void PyramidChart_MaxValue_m03294524BA8CA0AA3B103314C8D209524415EB6E (void);
// 0x00000337 System.Double ChartAndGraph.PyramidChart::MinValue(ChartAndGraph.AxisBase)
extern void PyramidChart_MinValue_m5A0B6C062736E98566BB09F087E8E6FD09F7A7E7 (void);
// 0x00000338 System.Void ChartAndGraph.PyramidChart::OnPropertyChanged()
extern void PyramidChart_OnPropertyChanged_m982A1C311EB0DC03A42ED8DD701263BBCAD66E76 (void);
// 0x00000339 System.Void ChartAndGraph.PyramidChart::OnPropertyUpdated()
extern void PyramidChart_OnPropertyUpdated_mC49FBBE06BEBA5EB523048FEC8392DE59C7AFF5C (void);
// 0x0000033A System.Void ChartAndGraph.PyramidChart::ValidateProperties()
extern void PyramidChart_ValidateProperties_m8D4ED101C8BA6DFF9E624C2F20789EDE9F1B6004 (void);
// 0x0000033B ChartAndGraph.PyramidChart/PyramidEventArgs ChartAndGraph.PyramidChart::userDataToEventArgs(System.Object)
extern void PyramidChart_userDataToEventArgs_m66E5A2C9B2B55EB785A26EDF7240BFF74291187C (void);
// 0x0000033C System.Void ChartAndGraph.PyramidChart::OnNonHoverted()
extern void PyramidChart_OnNonHoverted_m4DD3637FE918440E8751F6F57E9941C84205C8AA (void);
// 0x0000033D System.Void ChartAndGraph.PyramidChart::OnItemHoverted(System.Object)
extern void PyramidChart_OnItemHoverted_m8FF6354013197D34E98CCBFF082B7A5918BB166B (void);
// 0x0000033E System.Void ChartAndGraph.PyramidChart::OnItemSelected(System.Object)
extern void PyramidChart_OnItemSelected_mD99999AD64BB07EB544B9B080C20AF436D68EC59 (void);
// 0x0000033F System.Void ChartAndGraph.PyramidChart::Update()
extern void PyramidChart_Update_m1C89CDD141E5A1B82D4B061CE0BEF6AF4595794B (void);
// 0x00000340 System.Boolean ChartAndGraph.PyramidChart::get_SupportsCategoryLabels()
extern void PyramidChart_get_SupportsCategoryLabels_m07BD1390816F54C786D0239312370612E65E1566 (void);
// 0x00000341 System.Boolean ChartAndGraph.PyramidChart::get_SupportsGroupLables()
extern void PyramidChart_get_SupportsGroupLables_m0AA6B0921C3C6E3CE62E86B9B0F92F29DBFD25DD (void);
// 0x00000342 System.Boolean ChartAndGraph.PyramidChart::get_SupportsItemLabels()
extern void PyramidChart_get_SupportsItemLabels_mE68018377E0BF2C948BE68DB36F85F8A6DBC88A1 (void);
// 0x00000343 System.Boolean ChartAndGraph.PyramidChart::get_ShouldFitCanvas()
extern void PyramidChart_get_ShouldFitCanvas_m3FA92B1208892DB13117F79F69A7F09F52EA2C7B (void);
// 0x00000344 System.Single ChartAndGraph.PyramidChart::get_TotalDepthLink()
extern void PyramidChart_get_TotalDepthLink_m9BE4B0C8F1A56790AA32996B4346B0EC24E2614B (void);
// 0x00000345 System.Single ChartAndGraph.PyramidChart::get_TotalHeightLink()
extern void PyramidChart_get_TotalHeightLink_m420A6C084A59C3AF4D1506AC07537C7F50F9DAB5 (void);
// 0x00000346 System.Single ChartAndGraph.PyramidChart::get_TotalWidthLink()
extern void PyramidChart_get_TotalWidthLink_m31BD46131F31353F4EAF00C5B1301DDCFA480FB1 (void);
// 0x00000347 System.Boolean ChartAndGraph.PyramidChart::get_IsCanvas()
extern void PyramidChart_get_IsCanvas_m80278D306F23631C678400B0E2AF2D9C85871819 (void);
// 0x00000348 System.Void ChartAndGraph.PyramidChart/PyramidEventArgs::.ctor(System.String,System.String,System.String)
extern void PyramidEventArgs__ctor_mC1BB707334CF442DE1CB6201569427E0B0D41FF9 (void);
// 0x00000349 System.String ChartAndGraph.PyramidChart/PyramidEventArgs::get_Category()
extern void PyramidEventArgs_get_Category_m775875544DFBCAB3C0166BD745E20B325A86949D (void);
// 0x0000034A System.Void ChartAndGraph.PyramidChart/PyramidEventArgs::set_Category(System.String)
extern void PyramidEventArgs_set_Category_mDE1EDDF8E56E2BE4BFA785EC1BE243D6BFE91E47 (void);
// 0x0000034B System.String ChartAndGraph.PyramidChart/PyramidEventArgs::get_Title()
extern void PyramidEventArgs_get_Title_m9DFB5ADA82BE5AB49408925E61963B2A0FA8A5CF (void);
// 0x0000034C System.Void ChartAndGraph.PyramidChart/PyramidEventArgs::set_Title(System.String)
extern void PyramidEventArgs_set_Title_m6A6B22563451D13C04CA6969727FD453C5523235 (void);
// 0x0000034D System.String ChartAndGraph.PyramidChart/PyramidEventArgs::get_Text()
extern void PyramidEventArgs_get_Text_mE039C1B01C569C62FF5A03AAB2FB2E60C9697B13 (void);
// 0x0000034E System.Void ChartAndGraph.PyramidChart/PyramidEventArgs::set_Text(System.String)
extern void PyramidEventArgs_set_Text_mA128C4B843BCB63A903A1C4EF0CD6CF9168BCFBE (void);
// 0x0000034F System.Void ChartAndGraph.PyramidChart/PyramidEvent::.ctor()
extern void PyramidEvent__ctor_m0AFBEBE0C9DA12D1A59721BEF39B58351001D502 (void);
// 0x00000350 System.Void ChartAndGraph.PyramidChart/PyramidObject::.ctor()
extern void PyramidObject__ctor_m62D49387C8514901330AC3E3E39E174F7184C6F9 (void);
// 0x00000351 System.Void ChartAndGraph.PyramidData::.ctor()
extern void PyramidData__ctor_m5B8CD433B0A55C417C6C40D46AD8A7A7E6612EFD (void);
// 0x00000352 ChartAndGraph.ChartSparseDataSource ChartAndGraph.PyramidData::ChartAndGraph.IInternalPyramidData.get_InternalDataSource()
extern void PyramidData_ChartAndGraph_IInternalPyramidData_get_InternalDataSource_m5D75A460A521FCD5F1C69C18F71B26A63C0683F6 (void);
// 0x00000353 System.Int32 ChartAndGraph.PyramidData::get_TotalCategories()
extern void PyramidData_get_TotalCategories_m84D145E6E68EE264D7177B3EDE85B1F14AD2CA6B (void);
// 0x00000354 System.Void ChartAndGraph.PyramidData::Update()
extern void PyramidData_Update_m63E7D966C1327A5CBB63638AF4A555EEF8709F23 (void);
// 0x00000355 System.String ChartAndGraph.PyramidData::GetCategoryName(System.Int32)
extern void PyramidData_GetCategoryName_m4B9CCE041686BA7FDD1A1897443E5263C95152BF (void);
// 0x00000356 System.Void ChartAndGraph.PyramidData::OnBeforeSerialize()
extern void PyramidData_OnBeforeSerialize_m25AE45276AF2438D24034F094F74FB16D4DB4D0A (void);
// 0x00000357 System.Void ChartAndGraph.PyramidData::add_ProperyUpdated(System.Action)
extern void PyramidData_add_ProperyUpdated_m8DFF08F96D64766B67602D3E02BCD47F9E38C7C4 (void);
// 0x00000358 System.Void ChartAndGraph.PyramidData::remove_ProperyUpdated(System.Action)
extern void PyramidData_remove_ProperyUpdated_m6AD009FE63BD91431ECBEA9447D177E4A0A902AC (void);
// 0x00000359 System.Void ChartAndGraph.PyramidData::add_RealtimeProperyUpdated(System.Action)
extern void PyramidData_add_RealtimeProperyUpdated_m11C0E533367D009433E0BEAA414BF8FE6E37E88D (void);
// 0x0000035A System.Void ChartAndGraph.PyramidData::remove_RealtimeProperyUpdated(System.Action)
extern void PyramidData_remove_RealtimeProperyUpdated_mE3E870B58AC7B15C2FAF95507D56A16D56181C02 (void);
// 0x0000035B System.Void ChartAndGraph.PyramidData::RaiseRealtimePropertyUpdated()
extern void PyramidData_RaiseRealtimePropertyUpdated_m68B605C98C5B7586F979740D491C431E36535F80 (void);
// 0x0000035C System.Void ChartAndGraph.PyramidData::RaisePropertyUpdated()
extern void PyramidData_RaisePropertyUpdated_m4D7BA441A73F1B2FA8F67DD7F287DF0E0009F953 (void);
// 0x0000035D System.Boolean ChartAndGraph.PyramidData::HasCategory(System.String)
extern void PyramidData_HasCategory_m3D4E376362E7BAA071655FBEEE717E327CDBC488 (void);
// 0x0000035E System.Void ChartAndGraph.PyramidData::RenameCategory(System.String,System.String)
extern void PyramidData_RenameCategory_m11A4C747F4EABA80DEA34F341BD8275E2FD54996 (void);
// 0x0000035F System.Object ChartAndGraph.PyramidData::StoreCategory(System.String)
extern void PyramidData_StoreCategory_mDB746D21E9CC7B8961AF51F33E9C67344A54330F (void);
// 0x00000360 System.Void ChartAndGraph.PyramidData::RestoreCategory(System.String,System.Object)
extern void PyramidData_RestoreCategory_m2066AA8F45E53852D06D00C7E6A9A9FAB9CF25E1 (void);
// 0x00000361 System.Void ChartAndGraph.PyramidData::StartBatch()
extern void PyramidData_StartBatch_m2310DE39509776A0CACECE282F24703658BB5C71 (void);
// 0x00000362 System.Void ChartAndGraph.PyramidData::EndBatch()
extern void PyramidData_EndBatch_m47E44102D2B815451A5F9B08E8C2BF0FCFD5EEE1 (void);
// 0x00000363 System.Void ChartAndGraph.PyramidData::OnAfterDeserialize()
extern void PyramidData_OnAfterDeserialize_mD0D95924A23343A83968DFBE6F2E381B1A9BF3FA (void);
// 0x00000364 System.Void ChartAndGraph.PyramidData::AddGroup(System.String)
extern void PyramidData_AddGroup_m913471BFE1CCF039FF256F41C1DCBAAD7D4C2A2A (void);
// 0x00000365 System.Void ChartAndGraph.PyramidData::AddCategory(System.String,UnityEngine.Material,System.String,System.String,UnityEngine.Sprite)
extern void PyramidData_AddCategory_m54326A85E596AA466625709B976DA952AC8F8DFA (void);
// 0x00000366 System.Void ChartAndGraph.PyramidData::AddCategory(System.String,ChartAndGraph.ChartDynamicMaterial,System.String,System.String,UnityEngine.Sprite,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PyramidData_AddCategory_m03D37ED563B559DCD4D553F0C70E9A26EF99180B (void);
// 0x00000367 System.Void ChartAndGraph.PyramidData::SetCategoryInfo(System.String,System.String,System.String,UnityEngine.Sprite)
extern void PyramidData_SetCategoryInfo_mE46FC9BFE3E0FA6086E18AE56318D3070C328849 (void);
// 0x00000368 System.Void ChartAndGraph.PyramidData::SetCategoryAlpha(System.String,System.Single)
extern void PyramidData_SetCategoryAlpha_m1E21117FB6F3DECEA094A9E1365C4A2B08619367 (void);
// 0x00000369 System.Void ChartAndGraph.PyramidData::SetCategoryContentScale(System.String,System.Single)
extern void PyramidData_SetCategoryContentScale_m6AB737853A37E1F3D286DA4721920996146AF253 (void);
// 0x0000036A System.Void ChartAndGraph.PyramidData::SetCategoryOrientation(System.String,System.Single,System.Single,System.Single)
extern void PyramidData_SetCategoryOrientation_mD9D756454E1D432F9F4DD3110DBA20B4B6474D1D (void);
// 0x0000036B System.Void ChartAndGraph.PyramidData::SetCategorySlope(System.String,System.Single,System.Single)
extern void PyramidData_SetCategorySlope_m32B3F622DDA435A52A9AF755B6068C903C45168D (void);
// 0x0000036C System.Void ChartAndGraph.PyramidData::SetCategoryHeightRatio(System.String,System.Single)
extern void PyramidData_SetCategoryHeightRatio_m15F630E30B595AC5335BEC40832D3509D873E8E2 (void);
// 0x0000036D System.Void ChartAndGraph.PyramidData::Clear()
extern void PyramidData_Clear_m3A9530E66AF386C4CF97153FD5FFDAB3BF5B47AD (void);
// 0x0000036E System.Void ChartAndGraph.PyramidData::SetMaterial(System.String,UnityEngine.Material)
extern void PyramidData_SetMaterial_m8FA27AD0EFBAF2F7C4C6A7D687C096FDAD942744 (void);
// 0x0000036F ChartAndGraph.ChartDynamicMaterial ChartAndGraph.PyramidData::GetMaterial(System.String)
extern void PyramidData_GetMaterial_mB146DD3D5AF9DF8120DD7DE0C8E5A829AE9924ED (void);
// 0x00000370 System.Void ChartAndGraph.PyramidData::SetMaterial(System.String,ChartAndGraph.ChartDynamicMaterial)
extern void PyramidData_SetMaterial_m2E23B74418C28B076D3C64610862EFE78546E239 (void);
// 0x00000371 System.Void ChartAndGraph.PyramidData::RemoveCategory(System.String)
extern void PyramidData_RemoveCategory_mC22D3E2EA2643EDED436C510B90B69D1578D2118 (void);
// 0x00000372 System.Double ChartAndGraph.PyramidData::GetValue(System.String)
extern void PyramidData_GetValue_m46A9CD17E7C7B8D2C7B968921767ED8169AE85A6 (void);
// 0x00000373 System.Boolean ChartAndGraph.PyramidData::CheckAnimationEnded(System.Single,UnityEngine.AnimationCurve)
extern void PyramidData_CheckAnimationEnded_m3001F0CBB301A5FD7115017100FDA259D993D185 (void);
// 0x00000374 System.Object[] ChartAndGraph.PyramidData::StoreAllCategoriesinOrder()
extern void PyramidData_StoreAllCategoriesinOrder_mA4178F6CABDFF205BC46730EE25F3D66414801FB (void);
// 0x00000375 System.Void ChartAndGraph.PyramidData::FixEaseFunction(UnityEngine.AnimationCurve)
extern void PyramidData_FixEaseFunction_m9EEC19D62DC0E35D2679E50D8CE0A718A05EB4FB (void);
// 0x00000376 System.Void ChartAndGraph.PyramidData::SetValueInternal(System.String,System.String,System.Double)
extern void PyramidData_SetValueInternal_m0C982FFE4D2D235FBD9978F6802F0ADB7B7ED793 (void);
// 0x00000377 UnityEngine.Vector2 ChartAndGraph.PyramidData/CategoryData::get_Shift()
extern void CategoryData_get_Shift_m9B959B428B94380EBFA72C53506FBC2D58D826D8 (void);
// 0x00000378 System.Void ChartAndGraph.PyramidData/CategoryData::.ctor()
extern void CategoryData__ctor_m26F0237C886E996A106BBAC3FC2C36709B5D6B0D (void);
// 0x00000379 System.Void ChartAndGraph.PyramidData/DataEntry::.ctor()
extern void DataEntry__ctor_m8671E8075B39520C9C2FF4AD280C9462D276D114 (void);
// 0x0000037A System.Void ChartAndGraph.PyramidData/<>c::.cctor()
extern void U3CU3Ec__cctor_m34F51D6F14B1C7CE1935016769811162EEC21D5C (void);
// 0x0000037B System.Void ChartAndGraph.PyramidData/<>c::.ctor()
extern void U3CU3Ec__ctor_m5C4D72F7B31ABFC2DF210BE5BFF67D3050665D91 (void);
// 0x0000037C System.String ChartAndGraph.PyramidData/<>c::<Clear>b__38_0(ChartAndGraph.DataSource.ChartDataColumn)
extern void U3CU3Ec_U3CClearU3Eb__38_0_m79CFA4B1448C9EA6C06E396B8BF948B7B90010F5 (void);
// 0x0000037D System.Void ChartAndGraph.PyramidMesh::Generate2dMesh(UnityEngine.UIVertex[],UnityEngine.Vector2[],System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void PyramidMesh_Generate2dMesh_m8AB14E7E5672A2E8B4128749F71EC1FBFC28C5C9 (void);
// 0x0000037E System.Void ChartAndGraph.PyramidMesh::.ctor()
extern void PyramidMesh__ctor_mCD236F4200785C7C1F9A7C779618B448943F2333 (void);
// 0x0000037F System.Single ChartAndGraph.CandleChart::get_TotalHeightLink()
extern void CandleChart_get_TotalHeightLink_m598C73ED752261A7664E43B09571B9D47981A068 (void);
// 0x00000380 System.Single ChartAndGraph.CandleChart::get_TotalWidthLink()
extern void CandleChart_get_TotalWidthLink_mFF9B1DB7F8BF4AF033BA7D22D105E8AC094AF76D (void);
// 0x00000381 System.Void ChartAndGraph.CandleChart::FormatCandleValue(ChartAndGraph.CandleChartData/CandleValue,System.Int32,System.String&,System.String&,System.String&,System.String&)
extern void CandleChart_FormatCandleValue_mE3522EEDF8BCDFF301B676EEE0429137435B8129 (void);
// 0x00000382 System.Void ChartAndGraph.CandleChart::FormatCandleValue(ChartAndGraph.CandleChartData/CandleValue,System.Int32,System.String&,System.String&)
extern void CandleChart_FormatCandleValue_m6C06E5C9132129BE9D252A897EAD30FB2CEE88DC (void);
// 0x00000383 System.Void ChartAndGraph.CandleChart::FormatCandleValue(ChartAndGraph.CandleChartData/CandleValue,System.Int32,System.String&,System.String&,System.String&,System.String&,System.String&,System.String&)
extern void CandleChart_FormatCandleValue_m2DBACF487C6F696D3713364F47A3E0F954B7556C (void);
// 0x00000384 ChartAndGraph.CandleChart/CandleThicknessMode ChartAndGraph.CandleChart::get_ThicknessMode()
extern void CandleChart_get_ThicknessMode_m64DBC67B723E50CD56C4CC954FA40C7150570D0B (void);
// 0x00000385 System.Void ChartAndGraph.CandleChart::set_ThicknessMode(ChartAndGraph.CandleChart/CandleThicknessMode)
extern void CandleChart_set_ThicknessMode_m5F4585C9CB07B87B32704EE5EAC42B45788EFD8B (void);
// 0x00000386 ChartAndGraph.IChartData ChartAndGraph.CandleChart::get_DataLink()
extern void CandleChart_get_DataLink_m00FD68B034DC6139DD86EC1E48ED5C52E6952A27 (void);
// 0x00000387 System.Single ChartAndGraph.CandleChart::get_ThicknessConstant()
extern void CandleChart_get_ThicknessConstant_m8422009FEBC9441D7317CE265C4DFEDEBB2760EB (void);
// 0x00000388 System.Void ChartAndGraph.CandleChart::set_ThicknessConstant(System.Single)
extern void CandleChart_set_ThicknessConstant_m784DA40E173C4622E0E88D14A86232A84EEC6EB8 (void);
// 0x00000389 System.Single ChartAndGraph.CandleChart::GetScrollingRange(System.Int32)
extern void CandleChart_GetScrollingRange_m7EF73DF0DD8A16C4ABB7C54C766A7A9C0FD3DF87 (void);
// 0x0000038A System.String ChartAndGraph.CandleChart::get_ItemFormat()
extern void CandleChart_get_ItemFormat_m5EEE8919C00F565E1AE89494CE7AEFB9FDA172FE (void);
// 0x0000038B System.Void ChartAndGraph.CandleChart::set_ItemFormat(System.String)
extern void CandleChart_set_ItemFormat_m9213F4E419D7635EC8070C085B158975DA896A55 (void);
// 0x0000038C System.String ChartAndGraph.CandleChart::get_BodyFormat()
extern void CandleChart_get_BodyFormat_mC1A7B62E8F6F9CC380AC811AB61889114E78A7F1 (void);
// 0x0000038D System.Void ChartAndGraph.CandleChart::set_BodyFormat(System.String)
extern void CandleChart_set_BodyFormat_m6D32FA73067F33E5AC11517614ACD42EF97910D7 (void);
// 0x0000038E System.String ChartAndGraph.CandleChart::get_HighFormat()
extern void CandleChart_get_HighFormat_mE8A27E8B01B6892739CD541DDA674701FCB0350D (void);
// 0x0000038F System.Void ChartAndGraph.CandleChart::set_HighFormat(System.String)
extern void CandleChart_set_HighFormat_mDADEAADB405AA07B8F600333DE9CF2C1936D053A (void);
// 0x00000390 System.String ChartAndGraph.CandleChart::get_LowFormat()
extern void CandleChart_get_LowFormat_mFE54FB0627E93939329F94CDA029317DCB9EAF4A (void);
// 0x00000391 System.Void ChartAndGraph.CandleChart::set_LowFormat(System.String)
extern void CandleChart_set_LowFormat_m788CD1D7F584FDED174630885C8E2D2AE9248164 (void);
// 0x00000392 ChartAndGraph.CandleChartData ChartAndGraph.CandleChart::get_DataSource()
extern void CandleChart_get_DataSource_m0AB74C58AAE26714100C01C924601901A0213DBF (void);
// 0x00000393 System.Void ChartAndGraph.CandleChart::Start()
extern void CandleChart_Start_mF46EFFA901A7FFF36710F50B3EB64A22B0321C9A (void);
// 0x00000394 System.Void ChartAndGraph.CandleChart::OnValidate()
extern void CandleChart_OnValidate_m56672C9FAB940A4EC9F9FDFA637BDAB1EAF70999 (void);
// 0x00000395 System.Void ChartAndGraph.CandleChart::HookEvents()
extern void CandleChart_HookEvents_m6B6E4DAC69EB7562FE57716B8E4EA69540A87675 (void);
// 0x00000396 System.Void ChartAndGraph.CandleChart::CandleChart_InternalViewPortionChanged(System.Object,System.EventArgs)
extern void CandleChart_CandleChart_InternalViewPortionChanged_mA6B865845B98429DD9EBC51EE605B070F37953B5 (void);
// 0x00000397 System.Void ChartAndGraph.CandleChart::CandleChart_InternalRealTimeDataChanged(System.Int32,System.String)
extern void CandleChart_CandleChart_InternalRealTimeDataChanged_mDBFF0A2EB51D089078196909FB8DB204CF5C88C8 (void);
// 0x00000398 System.Void ChartAndGraph.CandleChart::CandleChart_InternalDataChanged(System.Object,System.EventArgs)
extern void CandleChart_CandleChart_InternalDataChanged_m4D73EFD7B463E1F7EA674DB120C5CCEFE4C45D84 (void);
// 0x00000399 System.Void ChartAndGraph.CandleChart::OnLabelSettingChanged()
extern void CandleChart_OnLabelSettingChanged_m6C8A4598ADE7656BBC7FACD99E98130A395EDF8A (void);
// 0x0000039A System.Void ChartAndGraph.CandleChart::OnAxisValuesChanged()
extern void CandleChart_OnAxisValuesChanged_m1C48F3A0F2874A6865799CC70187F5FDCD31E86B (void);
// 0x0000039B System.Void ChartAndGraph.CandleChart::OnLabelSettingsSet()
extern void CandleChart_OnLabelSettingsSet_m65E294BF05AE5344773202688B9BBB66ABF4B544 (void);
// 0x0000039C ChartAndGraph.LegenedData ChartAndGraph.CandleChart::get_LegendInfo()
extern void CandleChart_get_LegendInfo_m45FC5272E7730785237BE8F39EDF831F145E8273 (void);
// 0x0000039D System.Boolean ChartAndGraph.CandleChart::get_SupportsCategoryLabels()
extern void CandleChart_get_SupportsCategoryLabels_mD7C996FC5FBAD25CA590EE068D926AF9B91AE960 (void);
// 0x0000039E System.Boolean ChartAndGraph.CandleChart::get_SupportsGroupLables()
extern void CandleChart_get_SupportsGroupLables_m9B4F1F8D2247FB5BB844CCA08628CC346EFEC262 (void);
// 0x0000039F System.Boolean ChartAndGraph.CandleChart::get_SupportsItemLabels()
extern void CandleChart_get_SupportsItemLabels_m18B2A7E6CE430DEB2EE747B99F47457076C847D4 (void);
// 0x000003A0 System.Boolean ChartAndGraph.CandleChart::HasValues(ChartAndGraph.AxisBase)
extern void CandleChart_HasValues_mD0BED5956522BF501D630240CA6189990BE7F158 (void);
// 0x000003A1 System.Double ChartAndGraph.CandleChart::MaxValue(ChartAndGraph.AxisBase)
extern void CandleChart_MaxValue_m04277ADC4CAC6478F72BD66E189DAE5C4DE268CF (void);
// 0x000003A2 System.Double ChartAndGraph.CandleChart::MinValue(ChartAndGraph.AxisBase)
extern void CandleChart_MinValue_m98493586504E7EB11C80538EC6A7A62306857DAE (void);
// 0x000003A3 System.Void ChartAndGraph.CandleChart::Deflate(System.Double&,System.Double&,System.Double)
extern void CandleChart_Deflate_m9D2CC485CEFDF78276B26D025E3C485A559DB50C (void);
// 0x000003A4 ChartAndGraph.CandleChartData/CandleValue ChartAndGraph.CandleChart::NormalizeCandle(ChartAndGraph.CandleChartData/CandleValue,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void CandleChart_NormalizeCandle_m98E3DB6F3FDD364823DD396FC1E99680A408258D (void);
// 0x000003A5 ChartAndGraph.CandleChartData/CandleValue ChartAndGraph.CandleChart::InterpolateCandleInRect(ChartAndGraph.CandleChartData/CandleValue,UnityEngine.Rect)
extern void CandleChart_InterpolateCandleInRect_m97D7A1373D3BFAC2ADCAE0A5F190CAD777573496 (void);
// 0x000003A6 System.String ChartAndGraph.CandleChart::FormatItemWithFormat(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatItemWithFormat_mDBEEAE1C81C617711B40AC96DA71DE2409FDCB76 (void);
// 0x000003A7 System.String ChartAndGraph.CandleChart::FormatItem(System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatItem_mD53CE54C259E3E7AAC4E537A88C1B8BB0C39833C (void);
// 0x000003A8 System.String ChartAndGraph.CandleChart::FormatLow(System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatLow_m590FDD5765B9798269B34796FB02B2DE37B2FBC5 (void);
// 0x000003A9 System.String ChartAndGraph.CandleChart::FormatHigh(System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatHigh_m523529FB7E9EA95D4694E69B4A02557FE58DFA68 (void);
// 0x000003AA System.String ChartAndGraph.CandleChart::FormatBody(System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatBody_m8FBB46ACB7F278A0E9617141589AC13195FCEA0E (void);
// 0x000003AB System.Void ChartAndGraph.CandleChart::FormatItem(System.Text.StringBuilder,System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatItem_m0871F8AE5CD0D2CA1C9D09667AAFAEE6D1560A4C (void);
// 0x000003AC System.Void ChartAndGraph.CandleChart::FormatItemWithFormat(System.Text.StringBuilder,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void CandleChart_FormatItemWithFormat_m35E4421F3AFFAA329F1D72F387E2F80D2313E200 (void);
// 0x000003AD System.Void ChartAndGraph.CandleChart::TransformCandles(System.Collections.Generic.IList`1<ChartAndGraph.CandleChartData/CandleValue>,System.Collections.Generic.List`1<ChartAndGraph.CandleChartData/CandleValue>,UnityEngine.Rect,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void CandleChart_TransformCandles_m9F7FDAAB055207680D1C826D8EEB4AF72AD795C0 (void);
// 0x000003AE System.Int32 ChartAndGraph.CandleChart::ClipCandles(System.Collections.Generic.IList`1<ChartAndGraph.CandleChartData/CandleValue>,System.Collections.Generic.List`1<ChartAndGraph.CandleChartData/CandleValue>)
extern void CandleChart_ClipCandles_m54D130F6F6C943CFA23609EBBD5809D045FD3F8A (void);
// 0x000003AF System.Void ChartAndGraph.CandleChart::OnNonHoverted()
extern void CandleChart_OnNonHoverted_m84F7762745E03018F35FB17F3898A63A33187993 (void);
// 0x000003B0 System.Void ChartAndGraph.CandleChart::OnItemSelected(System.Object)
extern void CandleChart_OnItemSelected_m8A07A7EE9B634A3C17E4D4A1ED744B11C11C5B91 (void);
// 0x000003B1 System.Void ChartAndGraph.CandleChart::OnItemHoverted(System.Object)
extern void CandleChart_OnItemHoverted_m47467BDC246B715AEE6DBEEBC52B00948B37AC92 (void);
// 0x000003B2 System.Void ChartAndGraph.CandleChart::.ctor()
extern void CandleChart__ctor_m21A09CD5DCCEE77F1897C70AAECBAF126690FC7D (void);
// 0x000003B3 System.Void ChartAndGraph.CandleChart/CandleEvent::.ctor()
extern void CandleEvent__ctor_mB64B461DE26138E300986B96C4F26C566F2AF6AC (void);
// 0x000003B4 System.Void ChartAndGraph.CandleChart/CandleEventArgs::.ctor(System.Int32,System.Int32,UnityEngine.Rect,UnityEngine.Vector3,ChartAndGraph.CandleChartData/CandleValue,System.String)
extern void CandleEventArgs__ctor_m73F8D41CEC6FD422A0EAF91D77FE465B3076B4DC (void);
// 0x000003B5 System.Boolean ChartAndGraph.CandleChart/CandleEventArgs::get_IsHighEvent()
extern void CandleEventArgs_get_IsHighEvent_mAF15C317E3B14074D6B7B90AE10B3A2FF84A39AC (void);
// 0x000003B6 System.Boolean ChartAndGraph.CandleChart/CandleEventArgs::get_IsLowEvent()
extern void CandleEventArgs_get_IsLowEvent_mD5323C3CD4F2E624BD93106C7AE408D52374B57D (void);
// 0x000003B7 System.Boolean ChartAndGraph.CandleChart/CandleEventArgs::get_IsBodyEvent()
extern void CandleEventArgs_get_IsBodyEvent_m9D2112D0BDC23B7598683134F86982552D44FA1E (void);
// 0x000003B8 UnityEngine.Rect ChartAndGraph.CandleChart/CandleEventArgs::get_SelectionRect()
extern void CandleEventArgs_get_SelectionRect_m1BA91E7AC81BEF22E923E40B1B6F3D1A980EC63D (void);
// 0x000003B9 System.Void ChartAndGraph.CandleChart/CandleEventArgs::set_SelectionRect(UnityEngine.Rect)
extern void CandleEventArgs_set_SelectionRect_m335496950C896DF96106DA2CC0C7A5E2D9DFF041 (void);
// 0x000003BA UnityEngine.Vector3 ChartAndGraph.CandleChart/CandleEventArgs::get_Position()
extern void CandleEventArgs_get_Position_m4E369BBC39D884BFC6196F170705CE2F787B99A9 (void);
// 0x000003BB System.Void ChartAndGraph.CandleChart/CandleEventArgs::set_Position(UnityEngine.Vector3)
extern void CandleEventArgs_set_Position_m2F6E62EE3976061B158387AA04B8DBA38BB6F5F9 (void);
// 0x000003BC System.Int32 ChartAndGraph.CandleChart/CandleEventArgs::get_Index()
extern void CandleEventArgs_get_Index_m1124A9CE28D536D86334A6988DEDEA4CB97D7FC2 (void);
// 0x000003BD System.Void ChartAndGraph.CandleChart/CandleEventArgs::set_Index(System.Int32)
extern void CandleEventArgs_set_Index_mA835DEEF93ABB5FC3E5D025662A85A18AD07570E (void);
// 0x000003BE ChartAndGraph.CandleChartData/CandleValue ChartAndGraph.CandleChart/CandleEventArgs::get_CandleValue()
extern void CandleEventArgs_get_CandleValue_m8D6CA1E8F4F22BC926F537B47ED160F769992D43 (void);
// 0x000003BF System.Void ChartAndGraph.CandleChart/CandleEventArgs::set_CandleValue(ChartAndGraph.CandleChartData/CandleValue)
extern void CandleEventArgs_set_CandleValue_mD145E35AA00E49F70E63237F7F72853F916879D8 (void);
// 0x000003C0 System.String ChartAndGraph.CandleChart/CandleEventArgs::get_Category()
extern void CandleEventArgs_get_Category_m9ED5F70A96903ED3BAACD6717412E2D09596F354 (void);
// 0x000003C1 System.Void ChartAndGraph.CandleChart/CandleEventArgs::set_Category(System.String)
extern void CandleEventArgs_set_Category_m3D313A3E5EE3DA1262B5DEF2C06AD4711E3FDD8A (void);
// 0x000003C2 System.Void ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.add_InternalViewPortionChanged(System.EventHandler)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_add_InternalViewPortionChanged_m5DB4B47D9A257B0CCE65BDB91C1CC7F09358ADB5 (void);
// 0x000003C3 System.Void ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.remove_InternalViewPortionChanged(System.EventHandler)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_remove_InternalViewPortionChanged_mA81AB3679966958D93B0118B4CAC05524B2A6325 (void);
// 0x000003C4 System.Void ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.add_InternalDataChanged(System.EventHandler)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_add_InternalDataChanged_mCA6E008E7CD599161C420ABABD4ED06EBF942999 (void);
// 0x000003C5 System.Void ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.remove_InternalDataChanged(System.EventHandler)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_remove_InternalDataChanged_m50F18441D2EB04792AD44026B5C3789C7CDF256B (void);
// 0x000003C6 System.Void ChartAndGraph.CandleChartData::RenameCategory(System.String,System.String)
extern void CandleChartData_RenameCategory_m5443EC6E776EDB31FF5AA5F67716A0CEB13E4315 (void);
// 0x000003C7 System.Void ChartAndGraph.CandleChartData::AddCategory(System.String,ChartAndGraph.CandleChartData/CandleSettings,ChartAndGraph.CandleChartData/CandleSettings,System.Double)
extern void CandleChartData_AddCategory_m629D248CE33D6E503C288D75CDF9A8EC8462EC5A (void);
// 0x000003C8 System.Void ChartAndGraph.CandleChartData::AddCategory3DCandle(System.String,ChartAndGraph.CandleChartData/CandleSettings,ChartAndGraph.CandleChartData/CandleSettings,System.Double)
extern void CandleChartData_AddCategory3DCandle_mC501FE0232B3FC6644CAEC6637F09D46FF88F4D4 (void);
// 0x000003C9 System.Boolean ChartAndGraph.CandleChartData::RemoveCategory(System.String)
extern void CandleChartData_RemoveCategory_m26729DC323878BC3CD4A3E51B2CBB49097898DAE (void);
// 0x000003CA System.Void ChartAndGraph.CandleChartData::SetDownCandle(System.String,ChartAndGraph.CandleChartData/CandleSettings)
extern void CandleChartData_SetDownCandle_m0A025783536DEDA9863E0EF29E07364E62A3E0F4 (void);
// 0x000003CB System.Void ChartAndGraph.CandleChartData::SetUpCandle(System.String,ChartAndGraph.CandleChartData/CandleSettings)
extern void CandleChartData_SetUpCandle_mED3A65D321C1D08430FB651187E27B0B5184684F (void);
// 0x000003CC System.Void ChartAndGraph.CandleChartData::Set3DCategoryDepth(System.String,System.Double)
extern void CandleChartData_Set3DCategoryDepth_m74ECA8C993D752FBB2325C220FAC2ABE930177E7 (void);
// 0x000003CD System.Void ChartAndGraph.CandleChartData::ClearCategory(System.String)
extern void CandleChartData_ClearCategory_mA03C146855A083D94D1412F83509AEACD812E9AA (void);
// 0x000003CE System.Int32 ChartAndGraph.CandleChartData::GetCandleCount(System.String)
extern void CandleChartData_GetCandleCount_m3CF382A51CF4F867DEFCC616D365FB3F4D99D529 (void);
// 0x000003CF ChartAndGraph.CandleChartData/CandleValue ChartAndGraph.CandleChartData::GetCandle(System.String,System.Int32)
extern void CandleChartData_GetCandle_m28041F229BF56D7396C03A67FF0A08CDCAA62B3E (void);
// 0x000003D0 System.Int32 ChartAndGraph.CandleChartData::GetTotalCandlesInCategory(System.String)
extern void CandleChartData_GetTotalCandlesInCategory_mD98B4B7B729933DB02AD0413ED361A2916103E64 (void);
// 0x000003D1 System.Void ChartAndGraph.CandleChartData::ModifyCandleInCategory(System.String,System.Int32,System.Double,System.Double,System.Double,System.Double)
extern void CandleChartData_ModifyCandleInCategory_mC3E7DB4F888358234062F5DE5D749362FECDC921 (void);
// 0x000003D2 System.Void ChartAndGraph.CandleChartData::RemoveCandleInCategory(System.String,System.Int32)
extern void CandleChartData_RemoveCandleInCategory_m25CAA2D0D188F72B6EDF3E1E16A98824055E84C8 (void);
// 0x000003D3 System.Void ChartAndGraph.CandleChartData::ModifyLastCandleInCategory(System.String,System.Double,System.Double,System.Double,System.Double)
extern void CandleChartData_ModifyLastCandleInCategory_m6B8D03A191D146AAB393F5CA90B9E085F435FD95 (void);
// 0x000003D4 System.Void ChartAndGraph.CandleChartData::AddCandleToCategory(System.String,ChartAndGraph.CandleChartData/CandleValue,System.Single)
extern void CandleChartData_AddCandleToCategory_m2166CDA03AE3D0B9374BDF98BAA69287A1A0482A (void);
// 0x000003D5 System.Double ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.GetMaxValue(System.Int32,System.Boolean)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_GetMaxValue_mA504EA4923D0565CAB406C13E98D0C3C5183CAC1 (void);
// 0x000003D6 System.Double ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.GetMinValue(System.Int32,System.Boolean)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_GetMinValue_mF2DE9BCBDFFA4A64CBC2115809FE0FED9C991BFF (void);
// 0x000003D7 System.Void ChartAndGraph.CandleChartData::OnAfterDeserialize()
extern void CandleChartData_OnAfterDeserialize_m36F6F66026DEDDF4CAD578E93CF52918C5277444 (void);
// 0x000003D8 System.Void ChartAndGraph.CandleChartData::OnBeforeSerialize()
extern void CandleChartData_OnBeforeSerialize_m75717B8DCE716940C158D0EEC557FAFCA4B4EB5D (void);
// 0x000003D9 ChartAndGraph.BaseScrollableCategoryData ChartAndGraph.CandleChartData::GetDefaultCategory()
extern void CandleChartData_GetDefaultCategory_mB174FD9359B008B2BD6850D87E3168388BA20963 (void);
// 0x000003DA System.Void ChartAndGraph.CandleChartData::InnerClearCategory(System.String)
extern void CandleChartData_InnerClearCategory_mA708CA2B56A7FF418638F8ADFD76713B84299E49 (void);
// 0x000003DB System.Void ChartAndGraph.CandleChartData::AppendDatum(System.String,ChartAndGraph.MixedSeriesGenericValue)
extern void CandleChartData_AppendDatum_mF0EA4FF07FF954AC70AB303D826F3167E17773FB (void);
// 0x000003DC System.Void ChartAndGraph.CandleChartData::AppendDatum(System.String,System.Collections.Generic.IList`1<ChartAndGraph.MixedSeriesGenericValue>)
extern void CandleChartData_AppendDatum_m31AB4C52D2F277CFB135418586F6EE80D153FB43 (void);
// 0x000003DD System.Boolean ChartAndGraph.CandleChartData::AddCategory(System.String,ChartAndGraph.BaseScrollableCategoryData)
extern void CandleChartData_AddCategory_mEDDDDCCEF8EFB7FB353BC2A7D6E29B62257DB4C8 (void);
// 0x000003DE System.Int32 ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.get_TotalCategories()
extern void CandleChartData_ChartAndGraph_IInternalCandleData_get_TotalCategories_mDA7BCEA83CE1D6D92C7305E9EFC4004E08C66CA2 (void);
// 0x000003DF System.Void ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.add_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_add_InternalRealTimeDataChanged_m3D06FE180EF280777EF0B53FF1E397EF522E36D7 (void);
// 0x000003E0 System.Void ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.remove_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
extern void CandleChartData_ChartAndGraph_IInternalCandleData_remove_InternalRealTimeDataChanged_m73F41E021F43D95829EDC25F5AC65E527E58B374 (void);
// 0x000003E1 System.Collections.Generic.IEnumerable`1<ChartAndGraph.CandleChartData/CategoryData> ChartAndGraph.CandleChartData::ChartAndGraph.IInternalCandleData.get_Categories()
extern void CandleChartData_ChartAndGraph_IInternalCandleData_get_Categories_m0E407C38BDA68F4A5A8B24156E628DE688C3B4B9 (void);
// 0x000003E2 System.Void ChartAndGraph.CandleChartData::.ctor()
extern void CandleChartData__ctor_m601349095ECB37C0F45015E1492758972B22EAEC (void);
// 0x000003E3 System.Void ChartAndGraph.CandleChartData/CandleValue::.ctor(System.Double,System.Double,System.Double,System.Double,System.DateTime,System.TimeSpan)
extern void CandleValue__ctor_m3DF3507ACFD389F979CEA1D114D7455CE7B62BB5 (void);
// 0x000003E4 System.Void ChartAndGraph.CandleChartData/CandleValue::.ctor(System.Double,System.Double,System.Double,System.Double,System.Double,System.Double)
extern void CandleValue__ctor_m113D5BA7F228439CB190BA3CE617FB07BDDAFBBF (void);
// 0x000003E5 System.Boolean ChartAndGraph.CandleChartData/CandleValue::get_isUp()
extern void CandleValue_get_isUp_m327DC66B9BE3EFF66FF5299A41E200883479E80F (void);
// 0x000003E6 System.Double ChartAndGraph.CandleChartData/CandleValue::get_End()
extern void CandleValue_get_End_m2431E9875D452C1194BBC5FC3FB8668A4F9DAE52 (void);
// 0x000003E7 ChartAndGraph.DoubleVector2 ChartAndGraph.CandleChartData/CandleValue::get_MidPoint()
extern void CandleValue_get_MidPoint_m1C6D8595E8D75F3D474E520A562D457CCBD8232A (void);
// 0x000003E8 System.Double ChartAndGraph.CandleChartData/CandleValue::get_Max()
extern void CandleValue_get_Max_m35263E3FD9F3780330E691CC51D241D442D8A228 (void);
// 0x000003E9 System.Double ChartAndGraph.CandleChartData/CandleValue::get_LowBound()
extern void CandleValue_get_LowBound_m916E8CBB1CCE4211DAFE86888125AB79A57F329B (void);
// 0x000003EA System.Double ChartAndGraph.CandleChartData/CandleValue::get_HighBound()
extern void CandleValue_get_HighBound_m954D4BEC75899641B1CCB5E0792E9460F80BDCF0 (void);
// 0x000003EB System.Double ChartAndGraph.CandleChartData/CandleValue::get_Min()
extern void CandleValue_get_Min_m93794657F2D8ED00CB5B56EB649E5278FECEB04E (void);
// 0x000003EC System.Void ChartAndGraph.CandleChartData/CandleSettings::.ctor()
extern void CandleSettings__ctor_m8E130B523423C823992282CC919C895EC5C040A6 (void);
// 0x000003ED System.Void ChartAndGraph.CandleChartData/CategoryData::.ctor()
extern void CategoryData__ctor_m78FAEBAC0BBE1D80B2AEE1890A3772919F9B31A4 (void);
// 0x000003EE System.Int32 ChartAndGraph.CandleChartData/CandleComparer::Compare(ChartAndGraph.CandleChartData/CandleValue,ChartAndGraph.CandleChartData/CandleValue)
extern void CandleComparer_Compare_m2F2F55306F4CB0785F629EADEA3E1B9A22F01239 (void);
// 0x000003EF System.Void ChartAndGraph.CandleChartData/CandleComparer::.ctor()
extern void CandleComparer__ctor_m12219CAD5FBA888BEA34293CC31011F0147AB863 (void);
// 0x000003F0 System.Void ChartAndGraph.CandleChartData/SerializedCategory::.ctor()
extern void SerializedCategory__ctor_m7F8A603D480A2BDF2F8E9EC128E4408B185D9D4E (void);
// 0x000003F1 System.Void ChartAndGraph.CandleChartData/Slider::.ctor(ChartAndGraph.CandleChartData)
extern void Slider__ctor_m217D98E2A53A0B59E30B5B8D982EF7EA048BDF7B (void);
// 0x000003F2 ChartAndGraph.DoubleVector2 ChartAndGraph.CandleChartData/Slider::get_Max()
extern void Slider_get_Max_m8DF72945E0A7CB5EE23390EA8C90E532339BCAE1 (void);
// 0x000003F3 ChartAndGraph.DoubleVector2 ChartAndGraph.CandleChartData/Slider::get_Min()
extern void Slider_get_Min_m6071CE4E65E9B06F77BB91653EBB6394A11857AB (void);
// 0x000003F4 System.String ChartAndGraph.CandleChartData/Slider::get_Category()
extern void Slider_get_Category_m5B8D0FF74913A8ACF882846D87C34C470C181629 (void);
// 0x000003F5 System.Int32 ChartAndGraph.CandleChartData/Slider::get_MinIndex()
extern void Slider_get_MinIndex_mA5AD667371625A7F0DD2294FB03B357AEB949F59 (void);
// 0x000003F6 System.Boolean ChartAndGraph.CandleChartData/Slider::Update()
extern void Slider_Update_m217D54C3EBA3F7CE36F1639188249A0AC2CE22D0 (void);
// 0x000003F7 System.Void ChartAndGraph.CandleChartData/<>c::.cctor()
extern void U3CU3Ec__cctor_m1CD19550A4D019B320164A7C4754975EEEB77042 (void);
// 0x000003F8 System.Void ChartAndGraph.CandleChartData/<>c::.ctor()
extern void U3CU3Ec__ctor_m8A08FE628EDFF1251C7E01D6F373C3FE19867BF5 (void);
// 0x000003F9 System.Collections.Generic.KeyValuePair`2<System.String,ChartAndGraph.CandleChartData/CategoryData> ChartAndGraph.CandleChartData/<>c::<OnBeforeSerialize>b__32_0(System.Collections.Generic.KeyValuePair`2<System.String,ChartAndGraph.BaseScrollableCategoryData>)
extern void U3CU3Ec_U3COnBeforeSerializeU3Eb__32_0_mF5ECFB65AA3EC08FBE24CF23FE86C665C5408823 (void);
// 0x000003FA ChartAndGraph.CandleChartData/CategoryData ChartAndGraph.CandleChartData/<>c::<ChartAndGraph.IInternalCandleData.get_Categories>b__44_0(ChartAndGraph.BaseScrollableCategoryData)
extern void U3CU3Ec_U3CChartAndGraph_IInternalCandleData_get_CategoriesU3Eb__44_0_m0D7A86F25C103052E90B895D8C85767E1DE9026E (void);
// 0x000003FB System.Void ChartAndGraph.CanvasCandle::add_Hover(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CanvasCandle_add_Hover_m7A03F0024F862BDB44D4B95310A38BA14E6EC5FB (void);
// 0x000003FC System.Void ChartAndGraph.CanvasCandle::remove_Hover(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CanvasCandle_remove_Hover_m08E6A223A93FA472CB1946DC0298731133BF76EE (void);
// 0x000003FD System.Void ChartAndGraph.CanvasCandle::add_Click(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CanvasCandle_add_Click_m2955A032522633F04F7896963B5DE7E69FA672AA (void);
// 0x000003FE System.Void ChartAndGraph.CanvasCandle::remove_Click(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CanvasCandle_remove_Click_mB33B571C99D746F92B3E0A20ACE09828A18DC08E (void);
// 0x000003FF System.Void ChartAndGraph.CanvasCandle::add_Leave(System.Action)
extern void CanvasCandle_add_Leave_m473992F13C76B971C580B3B73D112C80F7F5136E (void);
// 0x00000400 System.Void ChartAndGraph.CanvasCandle::remove_Leave(System.Action)
extern void CanvasCandle_remove_Leave_mC622586CBA098291105A33A16BC79ABAAF382AB8 (void);
// 0x00000401 ChartAndGraph.CanvasCandleGraphic ChartAndGraph.CanvasCandle::CreateCandleGraphic()
extern void CanvasCandle_CreateCandleGraphic_m6D818B29A884A24CDFB8F6C593D9307D6C7567B2 (void);
// 0x00000402 System.Void ChartAndGraph.CanvasCandle::HookEventsForGraphic(ChartAndGraph.CanvasCandleGraphic)
extern void CanvasCandle_HookEventsForGraphic_m0822B3A2DA554DCD73752B4169D71115410D646D (void);
// 0x00000403 System.Void ChartAndGraph.CanvasCandle::Clear()
extern void CanvasCandle_Clear_mE83474357195AAEB0069FB26FD62CA14E5897DD2 (void);
// 0x00000404 System.Void ChartAndGraph.CanvasCandle::SetRefrenceIndex(System.Int32)
extern void CanvasCandle_SetRefrenceIndex_m3120303A07A05C2E680CF09F63EA20AD6651ABFD (void);
// 0x00000405 System.Void ChartAndGraph.CanvasCandle::Generate(ChartAndGraph.CandleChart,UnityEngine.Rect,System.Collections.Generic.IList`1<ChartAndGraph.CandleChartData/CandleValue>,ChartAndGraph.CandleChartData/CandleSettings)
extern void CanvasCandle_Generate_m1792E56658188CDE3034B312BACF1661F455A5EE (void);
// 0x00000406 System.Void ChartAndGraph.CanvasCandle::Candle_Leave(ChartAndGraph.CanvasCandleGraphic)
extern void CanvasCandle_Candle_Leave_m090252962C63213BDB7A49A0A36C9AE2BDD74B67 (void);
// 0x00000407 System.Void ChartAndGraph.CanvasCandle::Candle_Click(ChartAndGraph.CanvasCandleGraphic,System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void CanvasCandle_Candle_Click_mBE6CF0DD0F1961C2A356136EC82A591A363AB01E (void);
// 0x00000408 System.Void ChartAndGraph.CanvasCandle::Candle_Hover(ChartAndGraph.CanvasCandleGraphic,System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void CanvasCandle_Candle_Hover_m434A89E5A9F060530AC7E3CFC42F482A8C242532 (void);
// 0x00000409 System.Void ChartAndGraph.CanvasCandle::.ctor()
extern void CanvasCandle__ctor_mB9B73C3113FFBBF309717F2F19ED1810A02D6B72 (void);
// 0x0000040A System.Void ChartAndGraph.CanvasCandle/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mDCB6691267E39043D17389473F1F72CCE526ABAE (void);
// 0x0000040B System.Void ChartAndGraph.CanvasCandle/<>c__DisplayClass15_0::<HookEventsForGraphic>b__0(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CHookEventsForGraphicU3Eb__0_m832A25A3AE5FCA5B32980EEF62E9B115510AA830 (void);
// 0x0000040C System.Void ChartAndGraph.CanvasCandle/<>c__DisplayClass15_0::<HookEventsForGraphic>b__1(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass15_0_U3CHookEventsForGraphicU3Eb__1_m6E04460241ED47A7D8DE5990A445E6709A9BDA0A (void);
// 0x0000040D System.Void ChartAndGraph.CanvasCandle/<>c__DisplayClass15_0::<HookEventsForGraphic>b__2()
extern void U3CU3Ec__DisplayClass15_0_U3CHookEventsForGraphicU3Eb__2_mA5EA8B81455DB4F41C2CFE7CD038F3B7B49682D2 (void);
// 0x0000040E System.Boolean ChartAndGraph.CanvasCandleChart::get_FitToContainer()
extern void CanvasCandleChart_get_FitToContainer_m78DDCF1FB5E1EF0FBF05A1BEAAFAA93298BB29E2 (void);
// 0x0000040F System.Void ChartAndGraph.CanvasCandleChart::set_FitToContainer(System.Boolean)
extern void CanvasCandleChart_set_FitToContainer_m17DD522CD83A59B6598A1BCBBC4CF9CE59CEB322 (void);
// 0x00000410 ChartAndGraph.ChartMagin ChartAndGraph.CanvasCandleChart::get_FitMargin()
extern void CanvasCandleChart_get_FitMargin_mF480F838EF31E64558B85CED4723AADCF3915ED1 (void);
// 0x00000411 System.Void ChartAndGraph.CanvasCandleChart::set_FitMargin(ChartAndGraph.ChartMagin)
extern void CanvasCandleChart_set_FitMargin_m609531CD2DAF76857898466B335D9C6FBDDEDC1D (void);
// 0x00000412 System.Single ChartAndGraph.CanvasCandleChart::get_TotalDepthLink()
extern void CanvasCandleChart_get_TotalDepthLink_mE962D7B7181C9AB8D48862DD307C53E14AE956BD (void);
// 0x00000413 System.Boolean ChartAndGraph.CanvasCandleChart::get_SupportRealtimeGeneration()
extern void CanvasCandleChart_get_SupportRealtimeGeneration_m530BC202BCD75EC44B98791BE041FC23C0B76309 (void);
// 0x00000414 System.Boolean ChartAndGraph.CanvasCandleChart::get_IsCanvas()
extern void CanvasCandleChart_get_IsCanvas_mF648FC25D163B01F17E1E032856E88E6BB13D91E (void);
// 0x00000415 System.Boolean ChartAndGraph.CanvasCandleChart::get_ShouldFitCanvas()
extern void CanvasCandleChart_get_ShouldFitCanvas_m4482D620264E0E7BB7C6EA07507D3F9412FCB18C (void);
// 0x00000416 ChartAndGraph.AnyChart/FitType ChartAndGraph.CanvasCandleChart::get_FitAspectCanvas()
extern void CanvasCandleChart_get_FitAspectCanvas_m6370764F37B6D49595C5F800305A8A9F5ACE5832 (void);
// 0x00000417 ChartAndGraph.CanvasCandle ChartAndGraph.CanvasCandleChart::CreateDataObject(ChartAndGraph.CandleChartData/CategoryData,UnityEngine.GameObject)
extern void CanvasCandleChart_CreateDataObject_m07AED514070FAE243CC5992A902FE7A9E34563A9 (void);
// 0x00000418 System.Double ChartAndGraph.CanvasCandleChart::GetCategoryDepth(System.String)
extern void CanvasCandleChart_GetCategoryDepth_m5C56B05CF8D13EC72D1763C14E36548567C06C03 (void);
// 0x00000419 System.Void ChartAndGraph.CanvasCandleChart::GenerateRealtime()
extern void CanvasCandleChart_GenerateRealtime_m0BE2CC549DE07B1CB50375DDAAAF0B25589882CD (void);
// 0x0000041A System.Void ChartAndGraph.CanvasCandleChart::ClearChart()
extern void CanvasCandleChart_ClearChart_m2B5E7EF3884555E7B59E9CFCE905C4310A49DFC5 (void);
// 0x0000041B System.Void ChartAndGraph.CanvasCandleChart::GenerateItemLabels(System.Boolean,ChartAndGraph.CanvasCandleChart/CategoryObject,ChartAndGraph.CandleChartData/CategoryData,UnityEngine.Rect,System.Int32,System.Boolean)
extern void CanvasCandleChart_GenerateItemLabels_mB0D92746FFE2BE570FE2F5E18BB5550562DB00E2 (void);
// 0x0000041C System.Boolean ChartAndGraph.CanvasCandleChart::UpPredicate(ChartAndGraph.CandleChartData/CandleValue)
extern void CanvasCandleChart_UpPredicate_m7289FAA428D36710A98E5FFD0DCBDE659A2D939A (void);
// 0x0000041D System.Boolean ChartAndGraph.CanvasCandleChart::DownPredicate(ChartAndGraph.CandleChartData/CandleValue)
extern void CanvasCandleChart_DownPredicate_m14791FC0AD45966BA9A37FC0F0955034F48899A4 (void);
// 0x0000041E System.Void ChartAndGraph.CanvasCandleChart::FillCurrentSeries(System.Boolean,System.Int32)
extern void CanvasCandleChart_FillCurrentSeries_m0ECB5DF8D3B44F9466DCCA1A35CD3F4A9DBF9099 (void);
// 0x0000041F System.Int32 ChartAndGraph.CanvasCandleChart::MapIndex(System.Int32,System.Boolean)
extern void CanvasCandleChart_MapIndex_m984E5DDC6F8743096C4900B7196D126680E4062A (void);
// 0x00000420 System.Void ChartAndGraph.CanvasCandleChart::InternalGenerateChart()
extern void CanvasCandleChart_InternalGenerateChart_m394DAE54B3999BC64025023DA4ED38DB5173A2AF (void);
// 0x00000421 System.Void ChartAndGraph.CanvasCandleChart::Category_Hover(System.String,System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void CanvasCandleChart_Category_Hover_mBFFECBC534167A85491D006875DE75C7148B7788 (void);
// 0x00000422 System.Void ChartAndGraph.CanvasCandleChart::Category_Click(System.String,System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void CanvasCandleChart_Category_Click_mBE04F0962F2635ED907625CC48596603CA1FCADE (void);
// 0x00000423 System.Void ChartAndGraph.CanvasCandleChart::Category_Leave(System.String)
extern void CanvasCandleChart_Category_Leave_m3754EC24A17E89B2782CBF9A19086C7B856696F4 (void);
// 0x00000424 System.Void ChartAndGraph.CanvasCandleChart::OnItemHoverted(System.Object)
extern void CanvasCandleChart_OnItemHoverted_m0CB2E269DE6DE226FFA147C580F84EE5756ADEAB (void);
// 0x00000425 System.Void ChartAndGraph.CanvasCandleChart::OnItemSelected(System.Object)
extern void CanvasCandleChart_OnItemSelected_m12CFCA7EBC252168F7378CAD44049CE846685684 (void);
// 0x00000426 System.Void ChartAndGraph.CanvasCandleChart::OnItemLeave(System.Object,System.String)
extern void CanvasCandleChart_OnItemLeave_m4C076DDE2B8372616492E271C3FEB1777C224D08 (void);
// 0x00000427 System.Void ChartAndGraph.CanvasCandleChart::SetAsMixedSeries()
extern void CanvasCandleChart_SetAsMixedSeries_mD6BE6EDC0A22724667CB3A55DB67EAD0A26B63D4 (void);
// 0x00000428 System.Void ChartAndGraph.CanvasCandleChart::.ctor()
extern void CanvasCandleChart__ctor_m16AD589EAD93B119A7DDBD262FADDE1631ECA975 (void);
// 0x00000429 System.Boolean ChartAndGraph.CanvasCandleChart::<OnItemLeave>b__45_0(System.String)
extern void CanvasCandleChart_U3COnItemLeaveU3Eb__45_0_m811F170BE8838C17E66ADF78BEC2F32838A8F701 (void);
// 0x0000042A System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::add_Hover(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CategoryObject_add_Hover_m8BF66FBE73A4BB5FBD226C41DBB6F894B3BBF93E (void);
// 0x0000042B System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::remove_Hover(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CategoryObject_remove_Hover_m162A8BCC790D2A9BEB17971D675E30F13ACBA443 (void);
// 0x0000042C System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::add_Click(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CategoryObject_add_Click_m7131C9252F108EACEB73E742A113B1745186FC6A (void);
// 0x0000042D System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::remove_Click(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void CategoryObject_remove_Click_m704370212C1B5977C2B01D7EE3E5F7E3AA699F83 (void);
// 0x0000042E System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::add_Leave(System.Action)
extern void CategoryObject_add_Leave_mCDC72A0E33C950DC0EF06F5175A3728E3FE0782F (void);
// 0x0000042F System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::remove_Leave(System.Action)
extern void CategoryObject_remove_Leave_mD290676B1FF98DCE20CB2D9ADAF06D8C6FF87D98 (void);
// 0x00000430 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::.ctor(ChartAndGraph.CanvasCandleChart)
extern void CategoryObject__ctor_mF6D9BB15F76685910F603176A72365C68FA2A1D8 (void);
// 0x00000431 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::HookEvents()
extern void CategoryObject_HookEvents_mB82499160C614577AABD5ED7CB542C03D506011B (void);
// 0x00000432 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::HookEvents(ChartAndGraph.CanvasCandle,System.Boolean)
extern void CategoryObject_HookEvents_mC89109E38FEB1ABB15B3FAB431F8EEC43A5DA557 (void);
// 0x00000433 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::mClick(ChartAndGraph.CanvasCandle,System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void CategoryObject_mClick_m48C0B3C6274882BA90410BB87847F27C603DFC68 (void);
// 0x00000434 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::mHover(ChartAndGraph.CanvasCandle,System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void CategoryObject_mHover_m3432B525AC9ACA10E5C8661AC1D7B8BA228C3018 (void);
// 0x00000435 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject::mLeave(ChartAndGraph.CanvasCandle)
extern void CategoryObject_mLeave_mCC979BE9CE1116EC70C65D98B33200AFA054E038 (void);
// 0x00000436 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m616BFA074B2619DC77EC6B2B8E75A3F050E63DAC (void);
// 0x00000437 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject/<>c__DisplayClass17_0::<HookEvents>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CHookEventsU3Eb__0_m8074ACAB212ACED4651BB11B1B17E2FD2DB65359 (void);
// 0x00000438 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject/<>c__DisplayClass17_0::<HookEvents>b__1(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass17_0_U3CHookEventsU3Eb__1_m23A01997F35CBC50B814010C1BB81E768B1966B6 (void);
// 0x00000439 System.Void ChartAndGraph.CanvasCandleChart/CategoryObject/<>c__DisplayClass17_0::<HookEvents>b__2(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass17_0_U3CHookEventsU3Eb__2_mC9148D7AE85597D07B668F8382219D56A3BC74D0 (void);
// 0x0000043A System.Void ChartAndGraph.CanvasCandleChart/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m84340F8AC8AA05E6041229A77195B6ABD0E3C672 (void);
// 0x0000043B System.Void ChartAndGraph.CanvasCandleChart/<>c__DisplayClass39_0::<InternalGenerateChart>b__0(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass39_0_U3CInternalGenerateChartU3Eb__0_mE7141EEE08C8D4CBA26515FBA2819510D6BED4AC (void);
// 0x0000043C System.Void ChartAndGraph.CanvasCandleChart/<>c__DisplayClass39_0::<InternalGenerateChart>b__1(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass39_0_U3CInternalGenerateChartU3Eb__1_m56E16B616550F0B1CD7AF5D481F9161AE1341AF6 (void);
// 0x0000043D System.Void ChartAndGraph.CanvasCandleChart/<>c__DisplayClass39_0::<InternalGenerateChart>b__2()
extern void U3CU3Ec__DisplayClass39_0_U3CInternalGenerateChartU3Eb__2_mDD83B145E810D5DF9790BEAF5CCE0FAEB948A61E (void);
// 0x0000043E UnityEngine.Vector2 ChartAndGraph.CanvasCandleGraphic::get_Min()
extern void CanvasCandleGraphic_get_Min_m0E45D80B67B2687555ADF45A880180D3E7FA7538 (void);
// 0x0000043F UnityEngine.Vector2 ChartAndGraph.CanvasCandleGraphic::get_Max()
extern void CanvasCandleGraphic_get_Max_mDF8EFB4FDA38AC9BE8B841D6324F5C8A4F3A4A5D (void);
// 0x00000440 UnityEngine.Rect ChartAndGraph.CanvasCandleGraphic::RectFromIndex(System.Int32,System.Int32,UnityEngine.Rect)
extern void CanvasCandleGraphic_RectFromIndex_mEB3CE7AE0B48B1C8930C6A47BEE89FC5EF4C6FDB (void);
// 0x00000441 System.Void ChartAndGraph.CanvasCandleGraphic::SetUpHoverObject(ChartAndGraph.ChartItemEffect,System.Int32,System.Int32,System.Object)
extern void CanvasCandleGraphic_SetUpHoverObject_mF81B22171A137AD059E00EFBDC7B01253E3BB485 (void);
// 0x00000442 System.Void ChartAndGraph.CanvasCandleGraphic::PickLine(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
extern void CanvasCandleGraphic_PickLine_m35468513B5D434911D62FC2808B17DB89806CC12 (void);
// 0x00000443 System.Void ChartAndGraph.CanvasCandleGraphic::PickBody(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
extern void CanvasCandleGraphic_PickBody_mC7A02A490956B13263295FAAAEAB3303AA0C34AD (void);
// 0x00000444 System.Void ChartAndGraph.CanvasCandleGraphic::Pick(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
extern void CanvasCandleGraphic_Pick_mF5323F9EFA7E1B70289E3F0D913F426DF4D1F629 (void);
// 0x00000445 System.Single ChartAndGraph.CanvasCandleGraphic::get_MouseInThreshold()
extern void CanvasCandleGraphic_get_MouseInThreshold_mE1EBAEBAFD919FD7D9405872D697CF281A9A82C1 (void);
// 0x00000446 System.Void ChartAndGraph.CanvasCandleGraphic::ClearCandles()
extern void CanvasCandleGraphic_ClearCandles_m41D3D97972CB078966F997CEBDC12AC063F6B486 (void);
// 0x00000447 System.Void ChartAndGraph.CanvasCandleGraphic::SetCandle(System.Int32,System.Collections.Generic.IList`1<ChartAndGraph.CandleChartData/CandleValue>,ChartAndGraph.CandleChartData/CandleSettings)
extern void CanvasCandleGraphic_SetCandle_m8298FD9EE69478D92FEA303BE1956D61B485853C (void);
// 0x00000448 System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic::getOutline()
extern void CanvasCandleGraphic_getOutline_m798E8CB9D2C34E3562F6F0AC84703B1D56FA42C5 (void);
// 0x00000449 System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic::getCandle()
extern void CanvasCandleGraphic_getCandle_m3B6B00CD1C59B6EDD3CE21871A5804774A79B9CC (void);
// 0x0000044A System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic::getLine()
extern void CanvasCandleGraphic_getLine_mA140EF9183453BEC0C4FC21070BD37783E57AA89 (void);
// 0x0000044B System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic::getVerices()
extern void CanvasCandleGraphic_getVerices_m9154DAAB9013C0F21AC9DB498DCBFB4D0CECD3B4 (void);
// 0x0000044C System.Void ChartAndGraph.CanvasCandleGraphic::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void CanvasCandleGraphic_OnPopulateMesh_m868DFA12D36E9AB9E92231AEB30C77DB532D84E0 (void);
// 0x0000044D System.Void ChartAndGraph.CanvasCandleGraphic::.ctor()
extern void CanvasCandleGraphic__ctor_m29DFFFEC0FBB2292BEEC5B46967BEEB2D7713716 (void);
// 0x0000044E System.Void ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::.ctor(System.Int32)
extern void U3CgetOutlineU3Ed__19__ctor_mEFDBA2A898B7FBF0830DD5FB9F9B83F4BC10E633 (void);
// 0x0000044F System.Void ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::System.IDisposable.Dispose()
extern void U3CgetOutlineU3Ed__19_System_IDisposable_Dispose_m42DB601169ECF3C4049FF5FA8C7C40A82768B6DA (void);
// 0x00000450 System.Boolean ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::MoveNext()
extern void U3CgetOutlineU3Ed__19_MoveNext_mBE0A30E999C23E7A6F14B20D816EAA6C1B257F07 (void);
// 0x00000451 UnityEngine.UIVertex ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m13CD6F38EC403E61CC2DFA08B5F37CF5E297D386 (void);
// 0x00000452 System.Void ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::System.Collections.IEnumerator.Reset()
extern void U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_Reset_m8F76A0AA1EFF342B4F641587492819DF94693951 (void);
// 0x00000453 System.Object ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_get_Current_m8F0F3DDF253B4632CAAD0D24DFF88B1BA6D1E9BF (void);
// 0x00000454 System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m6D8E95C447218F47700C4C2495D423ACE17CB9AA (void);
// 0x00000455 System.Collections.IEnumerator ChartAndGraph.CanvasCandleGraphic/<getOutline>d__19::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetOutlineU3Ed__19_System_Collections_IEnumerable_GetEnumerator_mE3E50EE5A6C6DB0C705F3B7DDDD78C530B4DA2C8 (void);
// 0x00000456 System.Void ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::.ctor(System.Int32)
extern void U3CgetCandleU3Ed__20__ctor_mC82F7A257D54893E320CAA74A4A9F8E3C46F7A6D (void);
// 0x00000457 System.Void ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::System.IDisposable.Dispose()
extern void U3CgetCandleU3Ed__20_System_IDisposable_Dispose_m81A77A4E49DF47FE1ABDD3EB07FF558721202A35 (void);
// 0x00000458 System.Boolean ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::MoveNext()
extern void U3CgetCandleU3Ed__20_MoveNext_m312AD42BC93E2E9EBF2D2A5305CF1D8619A52C4B (void);
// 0x00000459 UnityEngine.UIVertex ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m4BD2F0D780FDE9F01E9498D7976DA81876228CEE (void);
// 0x0000045A System.Void ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::System.Collections.IEnumerator.Reset()
extern void U3CgetCandleU3Ed__20_System_Collections_IEnumerator_Reset_m6F0292D8486F07BDD361E3A7872B63C1961C1EC0 (void);
// 0x0000045B System.Object ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CgetCandleU3Ed__20_System_Collections_IEnumerator_get_Current_m9111EB1D61ACFBDCC6E9593D576936C79A24EF70 (void);
// 0x0000045C System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mB9B8A2839CD947E1084EEE1508B62501C9C9A98B (void);
// 0x0000045D System.Collections.IEnumerator ChartAndGraph.CanvasCandleGraphic/<getCandle>d__20::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetCandleU3Ed__20_System_Collections_IEnumerable_GetEnumerator_m9645F135E86563D363D9F1A8D234A86E607AC9F6 (void);
// 0x0000045E System.Void ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::.ctor(System.Int32)
extern void U3CgetLineU3Ed__21__ctor_m0326018FB282A0BA3FEC754F4D9D47FA72F54172 (void);
// 0x0000045F System.Void ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::System.IDisposable.Dispose()
extern void U3CgetLineU3Ed__21_System_IDisposable_Dispose_mA0D475FB96EB7B1DB8A497F5E7A45D331428E58D (void);
// 0x00000460 System.Boolean ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::MoveNext()
extern void U3CgetLineU3Ed__21_MoveNext_m6AEFBA63054F72D5335D56F54D14B32E4C2C46FA (void);
// 0x00000461 UnityEngine.UIVertex ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetLineU3Ed__21_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m5603D072D3ECD9E42D2013A333694F7E1C09C69A (void);
// 0x00000462 System.Void ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::System.Collections.IEnumerator.Reset()
extern void U3CgetLineU3Ed__21_System_Collections_IEnumerator_Reset_m01DC42B477E9802F36C385E8C991F6492566F885 (void);
// 0x00000463 System.Object ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CgetLineU3Ed__21_System_Collections_IEnumerator_get_Current_m8FE1DCA1D916AC6C8846759C6A879874BDDAD7FF (void);
// 0x00000464 System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetLineU3Ed__21_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m20622E0E1FDDEE6266F33E1492744F6A8F0045D6 (void);
// 0x00000465 System.Collections.IEnumerator ChartAndGraph.CanvasCandleGraphic/<getLine>d__21::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetLineU3Ed__21_System_Collections_IEnumerable_GetEnumerator_m2FDF32F5C4E66B3920C4557CBFF30D99422DDC2E (void);
// 0x00000466 System.Void ChartAndGraph.ICandleCreator::Generate(ChartAndGraph.CandleChart,UnityEngine.Rect,System.Collections.Generic.IList`1<ChartAndGraph.CandleChartData/CandleValue>,ChartAndGraph.CandleChartData/CandleSettings)
// 0x00000467 System.Double ChartAndGraph.IInternalCandleData::GetMinValue(System.Int32,System.Boolean)
// 0x00000468 System.Double ChartAndGraph.IInternalCandleData::GetMaxValue(System.Int32,System.Boolean)
// 0x00000469 System.Void ChartAndGraph.IInternalCandleData::OnBeforeSerialize()
// 0x0000046A System.Void ChartAndGraph.IInternalCandleData::OnAfterDeserialize()
// 0x0000046B System.Void ChartAndGraph.IInternalCandleData::add_InternalDataChanged(System.EventHandler)
// 0x0000046C System.Void ChartAndGraph.IInternalCandleData::remove_InternalDataChanged(System.EventHandler)
// 0x0000046D System.Void ChartAndGraph.IInternalCandleData::add_InternalViewPortionChanged(System.EventHandler)
// 0x0000046E System.Void ChartAndGraph.IInternalCandleData::remove_InternalViewPortionChanged(System.EventHandler)
// 0x0000046F System.Void ChartAndGraph.IInternalCandleData::add_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
// 0x00000470 System.Void ChartAndGraph.IInternalCandleData::remove_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
// 0x00000471 System.Int32 ChartAndGraph.IInternalCandleData::get_TotalCategories()
// 0x00000472 System.Collections.Generic.IEnumerable`1<ChartAndGraph.CandleChartData/CategoryData> ChartAndGraph.IInternalCandleData::get_Categories()
// 0x00000473 System.Void ChartAndGraph.InfoBox::CandleClicked(ChartAndGraph.CandleChart/CandleEventArgs)
extern void InfoBox_CandleClicked_mE8D20C2BC3D3178C1E4EA9CAD068CFBB5AFCC702 (void);
// 0x00000474 System.Void ChartAndGraph.InfoBox::CandleHovered(ChartAndGraph.CandleChart/CandleEventArgs)
extern void InfoBox_CandleHovered_mC7D3F1E37FA3754A0A78735A7624F3879B4F4FDD (void);
// 0x00000475 System.Void ChartAndGraph.InfoBox::BarHovered(ChartAndGraph.BarChart/BarEventArgs)
extern void InfoBox_BarHovered_mE6A5ED45536860E0E0DB16DCB86227AC1A431F4E (void);
// 0x00000476 System.Void ChartAndGraph.InfoBox::RadarHovered(ChartAndGraph.RadarChart/RadarEventArgs)
extern void InfoBox_RadarHovered_mC148FB5AB7CC5ADEFFF092340FB8F8ED9DAB44FE (void);
// 0x00000477 System.Void ChartAndGraph.InfoBox::GraphClicked(ChartAndGraph.GraphChartBase/GraphEventArgs)
extern void InfoBox_GraphClicked_m1BE72C5B5112845DF6401C3EFC8E28B265F4FA34 (void);
// 0x00000478 System.Void ChartAndGraph.InfoBox::GraphHoverd(ChartAndGraph.GraphChartBase/GraphEventArgs)
extern void InfoBox_GraphHoverd_m9B389831DC1C8B78CD8A4666A8896756FA9B8E98 (void);
// 0x00000479 System.Void ChartAndGraph.InfoBox::GraphLineClicked(ChartAndGraph.GraphChartBase/GraphEventArgs)
extern void InfoBox_GraphLineClicked_m866599CD5DBA81200F7B90FA2DE31FD001165F82 (void);
// 0x0000047A System.Void ChartAndGraph.InfoBox::GraphLineHoverd(ChartAndGraph.GraphChartBase/GraphEventArgs)
extern void InfoBox_GraphLineHoverd_m19CD91E5268C00C942D3ED2133D62A0FF41B9ED5 (void);
// 0x0000047B System.Void ChartAndGraph.InfoBox::PieHovered(ChartAndGraph.PieChart/PieEventArgs)
extern void InfoBox_PieHovered_m2FC55EBEA7AC0BB5E0DD65DF5F00541B75677299 (void);
// 0x0000047C System.Void ChartAndGraph.InfoBox::PyramidHovered(ChartAndGraph.PyramidChart/PyramidEventArgs)
extern void InfoBox_PyramidHovered_m6EFBF7D693B4EBCBD7B2A1063C89942D60DBFAB1 (void);
// 0x0000047D System.Void ChartAndGraph.InfoBox::NonHovered()
extern void InfoBox_NonHovered_mE2D527B087DCEF51B081791E08A9B80DDF5F052B (void);
// 0x0000047E System.Void ChartAndGraph.InfoBox::HookCandle()
extern void InfoBox_HookCandle_mE06AA12FB3E7A4A5E257FD17F1834690AB6A0A4E (void);
// 0x0000047F System.Void ChartAndGraph.InfoBox::HookChartEvents()
extern void InfoBox_HookChartEvents_mCC0FD2FA54C524C43D1D390AB57130124F0CFA86 (void);
// 0x00000480 System.Void ChartAndGraph.InfoBox::Start()
extern void InfoBox_Start_m77B9115D7D86257A71FF821A9FCB31A749AB2FB7 (void);
// 0x00000481 System.Void ChartAndGraph.InfoBox::Update()
extern void InfoBox_Update_mAA899B46178D3B292521450A75A12CA6EA164735 (void);
// 0x00000482 System.Void ChartAndGraph.InfoBox::.ctor()
extern void InfoBox__ctor_m941225ED15849B75B254D10679D6E0839816E9B9 (void);
// 0x00000483 System.Void ChartAndGraph.AbstractChartData::RemoveSliderForGroup(System.String)
extern void AbstractChartData_RemoveSliderForGroup_m9B3632ED8D7526CDD6B5FAD862E1E5B5D65ED893 (void);
// 0x00000484 System.Void ChartAndGraph.AbstractChartData::RemoveSliderForCategory(System.String)
extern void AbstractChartData_RemoveSliderForCategory_m47DC08F8284F49E10FF03920B02048D863519CE1 (void);
// 0x00000485 System.Void ChartAndGraph.AbstractChartData::RemoveSlider(System.String,System.String)
extern void AbstractChartData_RemoveSlider_mFE64540D9D48CB7B2E3DA9EA1BC33B68DF323FBA (void);
// 0x00000486 System.Boolean ChartAndGraph.AbstractChartData::DoSlider(ChartAndGraph.AbstractChartData/Slider)
extern void AbstractChartData_DoSlider_m00E9CB3D46262CB5D6C2CAF8AEC4C5456BE2B7A4 (void);
// 0x00000487 System.Void ChartAndGraph.AbstractChartData::UpdateSliders()
extern void AbstractChartData_UpdateSliders_m67DE90ADDAB7D774EB70BC4A625BBF9740C2623C (void);
// 0x00000488 System.Void ChartAndGraph.AbstractChartData::SetValueInternal(System.String,System.String,System.Double)
// 0x00000489 System.Void ChartAndGraph.AbstractChartData::.ctor()
extern void AbstractChartData__ctor_m42AF22840368227BA98CCF4662439FDE6BCE359F (void);
// 0x0000048A System.Boolean ChartAndGraph.AbstractChartData/Slider::UpdateSlider(ChartAndGraph.AbstractChartData)
extern void Slider_UpdateSlider_m1B7CB10E51D52BB02664132062F8A23373212D57 (void);
// 0x0000048B System.Void ChartAndGraph.AbstractChartData/Slider::.ctor()
extern void Slider__ctor_m56266ECDAFB9ED2C7351F4C99171A47A5264B54B (void);
// 0x0000048C System.Void ChartAndGraph.AbstractChartData/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mCBF7607CD96A908F01BC06E42777AC4AFE4EC3FB (void);
// 0x0000048D System.Boolean ChartAndGraph.AbstractChartData/<>c__DisplayClass2_0::<RemoveSliderForGroup>b__0(ChartAndGraph.AbstractChartData/Slider)
extern void U3CU3Ec__DisplayClass2_0_U3CRemoveSliderForGroupU3Eb__0_m70E074814A66DA329743708B457727200427C7D4 (void);
// 0x0000048E System.Void ChartAndGraph.AbstractChartData/<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_m077EC3EAAE51AF366935D410840FF2085FA5A1D8 (void);
// 0x0000048F System.Boolean ChartAndGraph.AbstractChartData/<>c__DisplayClass3_0::<RemoveSliderForCategory>b__0(ChartAndGraph.AbstractChartData/Slider)
extern void U3CU3Ec__DisplayClass3_0_U3CRemoveSliderForCategoryU3Eb__0_m4CACAD75D4CE289EFECBF0E16FB7D430AD003E32 (void);
// 0x00000490 System.Void ChartAndGraph.AbstractChartData/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mB9092D5830724BD4170E9D4CE09E90152B76BB2C (void);
// 0x00000491 System.Boolean ChartAndGraph.AbstractChartData/<>c__DisplayClass4_0::<RemoveSlider>b__0(ChartAndGraph.AbstractChartData/Slider)
extern void U3CU3Ec__DisplayClass4_0_U3CRemoveSliderU3Eb__0_m66A68EF9BAB3669BEA8248B0CC5464A5938274E4 (void);
// 0x00000492 System.String ChartAndGraph.AnyChart::get_CustomNumberFormat()
extern void AnyChart_get_CustomNumberFormat_m0AC11EBE27C7500614A0A3150704E9A9F0688873 (void);
// 0x00000493 System.Void ChartAndGraph.AnyChart::set_CustomNumberFormat(System.String)
extern void AnyChart_set_CustomNumberFormat_m0455D07B63986F28B53115F59CD8A4DB63B17581 (void);
// 0x00000494 System.String ChartAndGraph.AnyChart::get_CustomDateTimeFormat()
extern void AnyChart_get_CustomDateTimeFormat_mCB5464E6E7B3E977A4E9CE2FA2D55CD3B6F38B8A (void);
// 0x00000495 System.Void ChartAndGraph.AnyChart::set_CustomDateTimeFormat(System.String)
extern void AnyChart_set_CustomDateTimeFormat_m6A844AE03CACBB8A73E5B3026F3077831B931AE9 (void);
// 0x00000496 System.Collections.Generic.Dictionary`2<ChartAndGraph.DoubleVector3,System.Collections.Generic.KeyValuePair`2<System.String,System.String>> ChartAndGraph.AnyChart::get_VectorValueToStringMap()
extern void AnyChart_get_VectorValueToStringMap_mD76E6D652919102185A64AE23BC7162D01E75917 (void);
// 0x00000497 System.Collections.Generic.Dictionary`2<System.Double,System.String> ChartAndGraph.AnyChart::get_VerticalValueToStringMap()
extern void AnyChart_get_VerticalValueToStringMap_m1F17A15237DF71E6CEDD825B23E67364C8A6987C (void);
// 0x00000498 System.Collections.Generic.Dictionary`2<System.Double,System.String> ChartAndGraph.AnyChart::get_HorizontalValueToStringMap()
extern void AnyChart_get_HorizontalValueToStringMap_mBD0AFB84189A2B5CF469932E2B843A53FE234EEB (void);
// 0x00000499 UnityEngine.Camera ChartAndGraph.AnyChart::get_TextCameraLink()
extern void AnyChart_get_TextCameraLink_mAC8EF9B586EA2DB1103021BF2F0D5EF4870322F6 (void);
// 0x0000049A System.Single ChartAndGraph.AnyChart::get_TextIdleDistanceLink()
extern void AnyChart_get_TextIdleDistanceLink_m57876C1284E844484D59D03F21061DAB55EF5A40 (void);
// 0x0000049B System.Boolean ChartAndGraph.AnyChart::get_KeepOrthoSize()
extern void AnyChart_get_KeepOrthoSize_m2AB0597DA18D0255F4945F54ECC4BA79C70914C2 (void);
// 0x0000049C System.Void ChartAndGraph.AnyChart::set_KeepOrthoSize(System.Boolean)
extern void AnyChart_set_KeepOrthoSize_m6917A8606F8B3F199C6FCB4C643F147E6C0DCECD (void);
// 0x0000049D System.Boolean ChartAndGraph.AnyChart::get_PaperEffectText()
extern void AnyChart_get_PaperEffectText_mFDD282B68FB983674D303373159F4BC26E1DEC8C (void);
// 0x0000049E System.Void ChartAndGraph.AnyChart::set_PaperEffectText(System.Boolean)
extern void AnyChart_set_PaperEffectText_mF51D3B0744AA5F9673DFBCDCF62FEF16373A1E70 (void);
// 0x0000049F System.Boolean ChartAndGraph.AnyChart::get_VRSpaceText()
extern void AnyChart_get_VRSpaceText_m57E95CC4EA285C81630DA1C838FC429F36D1B90E (void);
// 0x000004A0 System.Void ChartAndGraph.AnyChart::set_VRSpaceText(System.Boolean)
extern void AnyChart_set_VRSpaceText_m4BFA3908FD701881BE4C80FF5C43F4C77A4D75FB (void);
// 0x000004A1 System.Single ChartAndGraph.AnyChart::get_VRSpaceScale()
extern void AnyChart_get_VRSpaceScale_m9F1CABDDAB2CFBD9BFEA6D83962B2A59CA3C22DD (void);
// 0x000004A2 System.Void ChartAndGraph.AnyChart::set_VRSpaceScale(System.Single)
extern void AnyChart_set_VRSpaceScale_mEB0E44EE23213D0D30B9BA46E66BF98F6E36AE08 (void);
// 0x000004A3 System.Boolean ChartAndGraph.AnyChart::get_MaintainLabelSize()
extern void AnyChart_get_MaintainLabelSize_m6362EA673AA4322BA6729FFFF7CB28FDEF69B121 (void);
// 0x000004A4 System.Void ChartAndGraph.AnyChart::set_MaintainLabelSize(System.Boolean)
extern void AnyChart_set_MaintainLabelSize_m49D1A27E0F05967938FCC168780D38363827F315 (void);
// 0x000004A5 System.Boolean ChartAndGraph.AnyChart::get_IsUnderCanvas()
extern void AnyChart_get_IsUnderCanvas_m195603F81F69BB55C721E932AE8A27457CE3C89C (void);
// 0x000004A6 System.Void ChartAndGraph.AnyChart::set_IsUnderCanvas(System.Boolean)
extern void AnyChart_set_IsUnderCanvas_mC1095C52AD717D93A0FF99BB0E5734571ED51BBF (void);
// 0x000004A7 System.Boolean ChartAndGraph.AnyChart::get_CanvasChanged()
extern void AnyChart_get_CanvasChanged_mC57ECBC1E94FE78A2BCB30A80D0A44302F8DBC6F (void);
// 0x000004A8 System.Void ChartAndGraph.AnyChart::set_CanvasChanged(System.Boolean)
extern void AnyChart_set_CanvasChanged_m12590C6FF3B6795685A5E876D7E9F4CB76D2A834 (void);
// 0x000004A9 System.Void ChartAndGraph.AnyChart::AxisChanged(System.Object,System.EventArgs)
extern void AnyChart_AxisChanged_mD7682E8E62D0FD8714021AE6A1396B959D004620 (void);
// 0x000004AA System.Void ChartAndGraph.AnyChart::OnPropertyUpdated()
extern void AnyChart_OnPropertyUpdated_mDF408648BFC3185F68AE7C898472E30ABDCC5391 (void);
// 0x000004AB System.Void ChartAndGraph.AnyChart::Labels_OnDataChanged(System.Object,System.EventArgs)
extern void AnyChart_Labels_OnDataChanged_m925FE6BB47837806BF43C0E2FD95DCD56480DE82 (void);
// 0x000004AC System.Void ChartAndGraph.AnyChart::Labels_OnDataUpdate(System.Object,System.EventArgs)
extern void AnyChart_Labels_OnDataUpdate_m47C3062CA67EC61F5649BC380511C854539EB3C2 (void);
// 0x000004AD System.Void ChartAndGraph.AnyChart::OnLabelSettingChanged()
extern void AnyChart_OnLabelSettingChanged_mC24544E7358D05B2B1B57F0CA76A8EA2CE405DD6 (void);
// 0x000004AE System.Single ChartAndGraph.AnyChart::get_TotalDepthLink()
// 0x000004AF System.Single ChartAndGraph.AnyChart::get_TotalHeightLink()
// 0x000004B0 System.Single ChartAndGraph.AnyChart::get_TotalWidthLink()
// 0x000004B1 ChartAndGraph.IChartData ChartAndGraph.AnyChart::get_DataLink()
// 0x000004B2 System.Boolean ChartAndGraph.AnyChart::get_IsCanvas()
// 0x000004B3 System.Single ChartAndGraph.AnyChart::get_TotalWidth()
extern void AnyChart_get_TotalWidth_m1011DAD4115030989BE370E758945608A6BE0D0F (void);
// 0x000004B4 System.Single ChartAndGraph.AnyChart::get_TotalHeight()
extern void AnyChart_get_TotalHeight_m54CC9006181ABAD472545C03E0235C9095239C88 (void);
// 0x000004B5 System.Single ChartAndGraph.AnyChart::get_TotalDepth()
extern void AnyChart_get_TotalDepth_m28634ED1897558FA8E91F7A1A482D5F3CD04EF1F (void);
// 0x000004B6 System.Double ChartAndGraph.AnyChart::GetScrollOffset(System.Int32)
extern void AnyChart_GetScrollOffset_m7768BD1DC02B8D0E3693A383A924E867CE0C3E03 (void);
// 0x000004B7 System.Boolean ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_HideHierarchy()
extern void AnyChart_ChartAndGraph_IInternalUse_get_HideHierarchy_mA9D7D1F262662D403AF1EAA00401C43D90CCCEAA (void);
// 0x000004B8 System.Void ChartAndGraph.AnyChart::FixAxisLabels()
extern void AnyChart_FixAxisLabels_m48ADDB3B77D0C24CAF64E4A99EED74AAD8E8696F (void);
// 0x000004B9 System.Void ChartAndGraph.AnyChart::OnAxisValuesChanged()
extern void AnyChart_OnAxisValuesChanged_m56C708D5108CF4F3D36FF66AB51CD28713F5BF2C (void);
// 0x000004BA System.Void ChartAndGraph.AnyChart::OnLabelSettingsSet()
extern void AnyChart_OnLabelSettingsSet_m242886560287205358EE8FD329BB39B5B66E6414 (void);
// 0x000004BB System.Void ChartAndGraph.AnyChart::Start()
extern void AnyChart_Start_mD4EF7C603D09C89FD31EF2AD29E096FD9D675E70 (void);
// 0x000004BC System.Void ChartAndGraph.AnyChart::DoCanvas(System.Boolean)
extern void AnyChart_DoCanvas_mE59B477599C638B6D17FF5F07C7DEC8275B9AC5F (void);
// 0x000004BD System.Void ChartAndGraph.AnyChart::CreateTextController()
extern void AnyChart_CreateTextController_m6A49D0C8D36C45F59969CAAA51C2B9362C9299E2 (void);
// 0x000004BE System.Void ChartAndGraph.AnyChart::EnsureTextController()
extern void AnyChart_EnsureTextController_m4747DF9972978764C351848CB85DCEBDE6F7DDE3 (void);
// 0x000004BF System.Boolean ChartAndGraph.AnyChart::get_Invalidating()
extern void AnyChart_get_Invalidating_m96DDF99F1701CCB0C4E00CD49651255CA4CED036 (void);
// 0x000004C0 System.Void ChartAndGraph.AnyChart::InvalidateRealtime()
extern void AnyChart_InvalidateRealtime_mC2749BD92D799EC92E5106216135E6CB1C35E986 (void);
// 0x000004C1 System.Void ChartAndGraph.AnyChart::Invalidate()
extern void AnyChart_Invalidate_m44A5F7B4CCA72ACAE8766BB82F5585A3D63D442F (void);
// 0x000004C2 System.Void ChartAndGraph.AnyChart::Awake()
extern void AnyChart_Awake_m18BB97712E88FCD794C5F16B5618B39F0AFCD684 (void);
// 0x000004C3 System.Void ChartAndGraph.AnyChart::Update()
extern void AnyChart_Update_m3E8127AC2DDDB4E1FFA780D2072C954BA47B04D6 (void);
// 0x000004C4 System.Void ChartAndGraph.AnyChart::LateUpdate()
extern void AnyChart_LateUpdate_m193D92CC347E7C824E519877B4AD0548B41A6D19 (void);
// 0x000004C5 System.Void ChartAndGraph.AnyChart::OnValidate()
extern void AnyChart_OnValidate_m79FB607D0777AFF6351217860D9B54A57DA66092 (void);
// 0x000004C6 TextController ChartAndGraph.AnyChart::get_TextController()
extern void AnyChart_get_TextController_m185F32490B09330CDA470FFBB491181C4EE32E45 (void);
// 0x000004C7 System.Void ChartAndGraph.AnyChart::set_TextController(TextController)
extern void AnyChart_set_TextController_m94231AA307FA3133BCC00C147C915DA65C77920E (void);
// 0x000004C8 ChartAndGraph.LegenedData ChartAndGraph.AnyChart::get_LegendInfo()
// 0x000004C9 System.Boolean ChartAndGraph.AnyChart::HasValues(ChartAndGraph.AxisBase)
// 0x000004CA System.Double ChartAndGraph.AnyChart::MaxValue(ChartAndGraph.AxisBase)
// 0x000004CB System.Double ChartAndGraph.AnyChart::MinValue(ChartAndGraph.AxisBase)
// 0x000004CC System.Void ChartAndGraph.AnyChart::OnEnable()
extern void AnyChart_OnEnable_mB12E9C209ED68AD071637A1B0B4CE7A2DE7D8F73 (void);
// 0x000004CD System.Void ChartAndGraph.AnyChart::OnDisable()
extern void AnyChart_OnDisable_m198E0682D61D297383C7B0DAC1F8FB840CCDF87C (void);
// 0x000004CE System.Boolean ChartAndGraph.AnyChart::get_SupportRealtimeGeneration()
// 0x000004CF System.Void ChartAndGraph.AnyChart::InvokeOnRedraw()
extern void AnyChart_InvokeOnRedraw_m00D6BD4CA67D1EE798C62D3C78B094FB7DF057AD (void);
// 0x000004D0 System.Void ChartAndGraph.AnyChart::GenerateRealtime()
extern void AnyChart_GenerateRealtime_mD1ED7FD86D4F2ACFF67C27C4D6AACD7376A309F6 (void);
// 0x000004D1 System.Void ChartAndGraph.AnyChart::GenerateChart()
extern void AnyChart_GenerateChart_m0C5EBF23B4B540E874FC63FDB564ED34DA6B58D0 (void);
// 0x000004D2 System.Void ChartAndGraph.AnyChart::InternalGenerateChart()
extern void AnyChart_InternalGenerateChart_m5C4AF6E633752CD5AB719A6998195EDA6EB79DC9 (void);
// 0x000004D3 System.Void ChartAndGraph.AnyChart::ClearChart()
extern void AnyChart_ClearChart_mEB0B744E8A300C7AB74DC2B4A27C1AFDF6095D24 (void);
// 0x000004D4 UnityEngine.GameObject ChartAndGraph.AnyChart::get_FixPosition()
extern void AnyChart_get_FixPosition_m5E0F5F20D3E2E4CAEAE63A2B324D48737A433651 (void);
// 0x000004D5 ChartAndGraph.ChartMagin ChartAndGraph.AnyChart::get_MarginLink()
extern void AnyChart_get_MarginLink_m338BB6284696A6A2F88321A1DE1B39CAC7F2570E (void);
// 0x000004D6 UnityEngine.Vector3 ChartAndGraph.AnyChart::get_CanvasFitOffset()
extern void AnyChart_get_CanvasFitOffset_mFDEC43D5BE0A310127300B33FC596FC12503FEFF (void);
// 0x000004D7 System.Boolean ChartAndGraph.AnyChart::get_ShouldFitCanvas()
extern void AnyChart_get_ShouldFitCanvas_m30CBD69BA4C21E7A3898BEB96B4C36CE5A1817AC (void);
// 0x000004D8 ChartAndGraph.AnyChart/FitType ChartAndGraph.AnyChart::get_FitAspectCanvas()
extern void AnyChart_get_FitAspectCanvas_m1959BCEBE2D42B8A7299D950868B522EDF8B3017 (void);
// 0x000004D9 ChartAndGraph.AnyChart/FitAlign ChartAndGraph.AnyChart::get_FitAlignCanvas()
extern void AnyChart_get_FitAlignCanvas_mC8A20FA4D96C2D1D865DBE78A020B7244DD1439F (void);
// 0x000004DA System.Void ChartAndGraph.AnyChart::FitCanvas()
extern void AnyChart_FitCanvas_mCE34828C0A5EB5665F90F89ACFF005785DB915D1 (void);
// 0x000004DB System.Void ChartAndGraph.AnyChart::OnNonHoverted()
extern void AnyChart_OnNonHoverted_m720A2E129AFF0F75D32BF47A01BA0ECF7244375B (void);
// 0x000004DC System.Void ChartAndGraph.AnyChart::OnItemLeave(System.Object,System.String)
extern void AnyChart_OnItemLeave_mC3B79D424DC36CE913A59514ADC770116B4F9A5F (void);
// 0x000004DD System.Void ChartAndGraph.AnyChart::OnItemSelected(System.Object)
extern void AnyChart_OnItemSelected_m0FBFC53D55542BF98A9F2FEC3DAE1D3BF8B741F1 (void);
// 0x000004DE System.Void ChartAndGraph.AnyChart::OnItemHoverted(System.Object)
extern void AnyChart_OnItemHoverted_m774F61937CDDEDEDCF18456914780D4AC85A041E (void);
// 0x000004DF ChartAndGraph.Axis.IAxisGenerator ChartAndGraph.AnyChart::InternalUpdateAxis(UnityEngine.GameObject&,ChartAndGraph.AxisBase,ChartAndGraph.ChartOrientation,System.Boolean,System.Boolean,System.Double)
extern void AnyChart_InternalUpdateAxis_m32CB03B8E04263D182D7F420B4F0B2A49A7F17B4 (void);
// 0x000004E0 System.Void ChartAndGraph.AnyChart::ValidateProperties()
extern void AnyChart_ValidateProperties_m108BB87D138E0F4E88228DF8F41E4B252D2369A8 (void);
// 0x000004E1 System.Void ChartAndGraph.AnyChart::GenerateAxis(System.Boolean)
extern void AnyChart_GenerateAxis_m6327549959BD854AA0C7967773F35080FCACB04B (void);
// 0x000004E2 System.Void ChartAndGraph.AnyChart::add_ChartGenerated(System.Action)
extern void AnyChart_add_ChartGenerated_mA7DB9A7B4BB163D50DECF59625C459C4C2095DA6 (void);
// 0x000004E3 System.Void ChartAndGraph.AnyChart::remove_ChartGenerated(System.Action)
extern void AnyChart_remove_ChartGenerated_m523F495E2E3E114810C67FBD82A5BC5909D7B5A1 (void);
// 0x000004E4 System.Void ChartAndGraph.AnyChart::RaiseChartGenerated()
extern void AnyChart_RaiseChartGenerated_mA17EA2EBA3E461CF5829355042ED56E5B770EBA1 (void);
// 0x000004E5 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.add_Generated(System.Action)
extern void AnyChart_ChartAndGraph_IInternalUse_add_Generated_m3BB659223245F2B8E60B89C269E9E400C0432105 (void);
// 0x000004E6 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.remove_Generated(System.Action)
extern void AnyChart_ChartAndGraph_IInternalUse_remove_Generated_mCEC58738425E8C2E7B739F694A45775900D8C7C6 (void);
// 0x000004E7 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.InternalItemSelected(System.Object)
extern void AnyChart_ChartAndGraph_IInternalUse_InternalItemSelected_mA1B3FF66FCC1F3EFE343457007D735493995CB50 (void);
// 0x000004E8 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.InternalItemLeave(System.Object)
extern void AnyChart_ChartAndGraph_IInternalUse_InternalItemLeave_m28452552121F4EF7A98121F734994576DBD803F3 (void);
// 0x000004E9 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.InternalItemHovered(System.Object)
extern void AnyChart_ChartAndGraph_IInternalUse_InternalItemHovered_mAB58ED8D7A297F310A5EEB1CA35EC65A4982F8B6 (void);
// 0x000004EA System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.CallOnValidate()
extern void AnyChart_ChartAndGraph_IInternalUse_CallOnValidate_m031FAB7D24FF2327FAFFF66E7FE313E3A448CCED (void);
// 0x000004EB ChartAndGraph.ItemLabels ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_ItemLabels()
extern void AnyChart_ChartAndGraph_IInternalUse_get_ItemLabels_mCC71FD6C2017E1212B03E48384D4D956F36F2D11 (void);
// 0x000004EC System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.set_ItemLabels(ChartAndGraph.ItemLabels)
extern void AnyChart_ChartAndGraph_IInternalUse_set_ItemLabels_mF4DD5FBA3FAB697D089B835F2C1BD1C1FE421DB7 (void);
// 0x000004ED ChartAndGraph.VerticalAxis ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_VerticalAxis()
extern void AnyChart_ChartAndGraph_IInternalUse_get_VerticalAxis_mDA255FDC3CE7495683718A4CC8898149891CA83A (void);
// 0x000004EE System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.set_VerticalAxis(ChartAndGraph.VerticalAxis)
extern void AnyChart_ChartAndGraph_IInternalUse_set_VerticalAxis_mEA479C4A594D772CFA6D2F9B8F8B26EF01DB78ED (void);
// 0x000004EF ChartAndGraph.HorizontalAxis ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_HorizontalAxis()
extern void AnyChart_ChartAndGraph_IInternalUse_get_HorizontalAxis_m0E5764FAF86003B6A24BE1B69B050786A4D2E0C9 (void);
// 0x000004F0 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.set_HorizontalAxis(ChartAndGraph.HorizontalAxis)
extern void AnyChart_ChartAndGraph_IInternalUse_set_HorizontalAxis_m8291830DFB70837ECE2E77329D24765B95917BCD (void);
// 0x000004F1 CategoryLabels ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_CategoryLabels()
extern void AnyChart_ChartAndGraph_IInternalUse_get_CategoryLabels_mCE0066BA618B0464B192968FFD2CEB5703681E53 (void);
// 0x000004F2 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.set_CategoryLabels(CategoryLabels)
extern void AnyChart_ChartAndGraph_IInternalUse_set_CategoryLabels_m5C39A0D69E8A311C20F6401E80C566064640952F (void);
// 0x000004F3 GroupLabels ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_GroupLabels()
extern void AnyChart_ChartAndGraph_IInternalUse_get_GroupLabels_m7346A32568692CC439E30932D3DDD8D43AD46630 (void);
// 0x000004F4 System.Void ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.set_GroupLabels(GroupLabels)
extern void AnyChart_ChartAndGraph_IInternalUse_set_GroupLabels_m8B826267B38D696095255B9EE22B541E6BD0E9F1 (void);
// 0x000004F5 TextController ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalTextController()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalTextController_m8E8734F6D08482D1F54295D5F9AF322D5ADD8146 (void);
// 0x000004F6 ChartAndGraph.LegenedData ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalLegendInfo()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalLegendInfo_mD583CFFAC25A9F57A0EAC19FEF39519357E866CA (void);
// 0x000004F7 UnityEngine.Camera ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalTextCamera()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalTextCamera_mC013A1DE61A533BEBF48F778E952A1264BFA5F4C (void);
// 0x000004F8 System.Single ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalTextIdleDistance()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalTextIdleDistance_m1582FBC4CCF0DEB1CF98098DD3EA13364A6D0A51 (void);
// 0x000004F9 System.Boolean ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.InternalHasValues(ChartAndGraph.AxisBase)
extern void AnyChart_ChartAndGraph_IInternalUse_InternalHasValues_m9294D407887BACD89E175F9D3044C235C64EDDF9 (void);
// 0x000004FA System.Double ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.InternalMaxValue(ChartAndGraph.AxisBase)
extern void AnyChart_ChartAndGraph_IInternalUse_InternalMaxValue_m1FF5C657FA190B3F99863EDBE50B67C391401511 (void);
// 0x000004FB System.Double ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.InternalMinValue(ChartAndGraph.AxisBase)
extern void AnyChart_ChartAndGraph_IInternalUse_InternalMinValue_m620653C10E56ADE4BE42733125D17937E06D9492 (void);
// 0x000004FC System.Void ChartAndGraph.AnyChart::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void AnyChart_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDE8102E45934BCF92E4FAFAE49D866BDC9819A00 (void);
// 0x000004FD System.Void ChartAndGraph.AnyChart::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void AnyChart_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m69FDA7001FAC6AC4775E9BA028D450748088B8F8 (void);
// 0x000004FE System.Void ChartAndGraph.AnyChart::OnBeforeSerializeEvent()
extern void AnyChart_OnBeforeSerializeEvent_mAB4DF8925D6F1305B474D0748C65B91E99B79241 (void);
// 0x000004FF System.Void ChartAndGraph.AnyChart::OnAfterDeserializeEvent()
extern void AnyChart_OnAfterDeserializeEvent_mF703D981F156BED1A5BFF5D79995D4BDAE707EC0 (void);
// 0x00000500 System.Single ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalTotalDepth()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalTotalDepth_mFC65A8DB3B2875426A33CB6BAD31ADEAA21679CD (void);
// 0x00000501 System.Single ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalTotalWidth()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalTotalWidth_m6CBB7F20BF1B505EF54A4D7B774973B982AE9019 (void);
// 0x00000502 System.Single ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalTotalHeight()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalTotalHeight_m98CB1F8F1819A26C73E3ACE4B0C8FCB02C5FDA4A (void);
// 0x00000503 System.Boolean ChartAndGraph.AnyChart::get_SupportsCategoryLabels()
// 0x00000504 System.Boolean ChartAndGraph.AnyChart::get_SupportsItemLabels()
// 0x00000505 System.Boolean ChartAndGraph.AnyChart::get_SupportsGroupLables()
// 0x00000506 System.Boolean ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalSupportsCategoryLables()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalSupportsCategoryLables_m19B03CA7EE3E59009BA352C6A6049317E0B15751 (void);
// 0x00000507 System.Boolean ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalSupportsGroupLabels()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalSupportsGroupLabels_m3A364DEA63E78B5CEDC3954EB77EE262E3399F11 (void);
// 0x00000508 System.Boolean ChartAndGraph.AnyChart::ChartAndGraph.IInternalUse.get_InternalSupportsItemLabels()
extern void AnyChart_ChartAndGraph_IInternalUse_get_InternalSupportsItemLabels_mDFCD01EAAF7220789F549B38497E20A4CD6E40FF (void);
// 0x00000509 System.Void ChartAndGraph.AnyChart::.ctor()
extern void AnyChart__ctor_m6096DAA40D067BACB31DB393771F3033461E0E5F (void);
// 0x0000050A System.Void ChartAndGraph.AnyChart::.cctor()
extern void AnyChart__cctor_m78EA190B7A711AAA196019DA9C0E9DB6913B0755 (void);
// 0x0000050B System.Void ChartAndGraph.CanvasAttribute::.ctor()
extern void CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299 (void);
// 0x0000050C System.Void ChartAndGraph.ChartFillerEditorAttribute::.ctor(GraphDataFiller/DataType)
extern void ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD (void);
// 0x0000050D System.Void ChartAndGraph.ChartFillerEditorAttribute::.ctor(BarDataFiller/DataType)
extern void ChartFillerEditorAttribute__ctor_mF182ABD485E7833635757C57D47433F2841475AA (void);
// 0x0000050E System.Void ChartAndGraph.ChartFillerEditorAttribute::.ctor(RadarDataFiller/DataType)
extern void ChartFillerEditorAttribute__ctor_mF6FDCA0F315DB5CB80A0D6C4B49E795AEE6A4927 (void);
// 0x0000050F System.Void ChartAndGraph.NonCanvasAttribute::.ctor()
extern void NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6 (void);
// 0x00000510 System.Void ChartAndGraph.SimpleAttribute::.ctor()
extern void SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D (void);
// 0x00000511 System.Void ChartAndGraph.AutoFloat::.ctor(System.Boolean,System.Single)
extern void AutoFloat__ctor_mFA106E4A358AB6F23F2C124C4C9BD3811B422C1A (void);
// 0x00000512 System.Boolean ChartAndGraph.AutoFloat::Equals(System.Object)
extern void AutoFloat_Equals_m33B82696A2015E983C87F70E380C9942593D0BFC (void);
// 0x00000513 System.Int32 ChartAndGraph.AutoFloat::GetHashCode()
extern void AutoFloat_GetHashCode_mBA93BDD7EA6E5F3DAF7F2B880B8DD0CA23539989 (void);
// 0x00000514 ChartAndGraph.AxisFormat ChartAndGraph.AxisBase::get_Format()
extern void AxisBase_get_Format_m70272E116015340CCB033EC0C18721ADE80A5380 (void);
// 0x00000515 System.Void ChartAndGraph.AxisBase::set_Format(ChartAndGraph.AxisFormat)
extern void AxisBase_set_Format_m983A7557BCC6978A1334B1556D05F2374446B1B8 (void);
// 0x00000516 ChartAndGraph.AutoFloat ChartAndGraph.AxisBase::get_Depth()
extern void AxisBase_get_Depth_mB1B72629F50E2C8A3D23E2400B2B5213E439C3A0 (void);
// 0x00000517 System.Void ChartAndGraph.AxisBase::set_Depth(ChartAndGraph.AutoFloat)
extern void AxisBase_set_Depth_m1DF88FD9376ED4AE6AEF5767AAB8E8A00439B64A (void);
// 0x00000518 ChartAndGraph.ChartMainDivisionInfo ChartAndGraph.AxisBase::get_MainDivisions()
extern void AxisBase_get_MainDivisions_mC0A8D25E60D1D1895C9D22E775C83EB2837B57C6 (void);
// 0x00000519 ChartAndGraph.ChartDivisionInfo ChartAndGraph.AxisBase::get_SubDivisions()
extern void AxisBase_get_SubDivisions_mFB99E1B27A5B244F974603CE5970F1C12DD0614A (void);
// 0x0000051A System.Void ChartAndGraph.AxisBase::ClearFormats()
extern void AxisBase_ClearFormats_mA654017A705ECFA2D3DB210AB6F904BD0DFC15DC (void);
// 0x0000051B System.Void ChartAndGraph.AxisBase::.ctor()
extern void AxisBase__ctor_m40EDD3B466B9BA93D96E92A2AE1ABDCC780F3956 (void);
// 0x0000051C System.Void ChartAndGraph.AxisBase::AddInnerItems()
extern void AxisBase_AddInnerItems_m6CF2EDEEC102EE7573164B4E0657C0B45DB46905 (void);
// 0x0000051D System.Void ChartAndGraph.AxisBase::ValidateProperties()
extern void AxisBase_ValidateProperties_mEE169138DFBE5E985C86CBAC0C730AF5BC403928 (void);
// 0x0000051E System.Void ChartAndGraph.AxisBase::GetStartEnd(ChartAndGraph.AnyChart,ChartAndGraph.ChartOrientation,System.Single,System.Single&,System.Single&)
extern void AxisBase_GetStartEnd_m73AD166A34801C224867E7DD2664B6C6E6541023 (void);
// 0x0000051F System.Void ChartAndGraph.AxisBase::SetMeshUv(ChartAndGraph.IChartMesh,System.Single,System.Single)
extern void AxisBase_SetMeshUv_m6FF6765D4179795A9FDEC8EE5C7C614300BA73B9 (void);
// 0x00000520 System.Void ChartAndGraph.AxisBase::DrawDivisions(System.Double,ChartAndGraph.AnyChart,UnityEngine.Transform,ChartAndGraph.ChartDivisionInfo,ChartAndGraph.IChartMesh,System.Int32,ChartAndGraph.ChartOrientation,System.Double,System.Boolean,System.Double)
extern void AxisBase_DrawDivisions_m6EDF6E3D1E65A3B46481C3310F0324F5A90BF4A7 (void);
// 0x00000521 System.Void ChartAndGraph.AxisBase::GetDirectionVectors(ChartAndGraph.AnyChart,ChartAndGraph.ChartDivisionInfo,ChartAndGraph.ChartOrientation,System.Single,System.Boolean,ChartAndGraph.DoubleVector3&,ChartAndGraph.DoubleVector3&,ChartAndGraph.DoubleVector3&)
extern void AxisBase_GetDirectionVectors_mA2936BD292DB886C3B08E2A2617F22CF3F075794 (void);
// 0x00000522 System.Void ChartAndGraph.AxisBase::AddSubdivisionToChartMesh(System.Double,ChartAndGraph.AnyChart,UnityEngine.Transform,ChartAndGraph.IChartMesh,ChartAndGraph.ChartOrientation)
extern void AxisBase_AddSubdivisionToChartMesh_m4284C97D4CB9B2731FEBC683B8F0FC4437A63990 (void);
// 0x00000523 System.Nullable`1<System.Double> ChartAndGraph.AxisBase::GetMainGap(ChartAndGraph.AnyChart,System.Double)
extern void AxisBase_GetMainGap_m056CCF93F4678F143603A0C72A75B950E486DFFA (void);
// 0x00000524 System.Void ChartAndGraph.AxisBase::AddMainDivisionToChartMesh(System.Double,ChartAndGraph.AnyChart,UnityEngine.Transform,ChartAndGraph.IChartMesh,ChartAndGraph.ChartOrientation)
extern void AxisBase_AddMainDivisionToChartMesh_m39F198436B2013AEC3229BE8A4994DBA7FF32D59 (void);
// 0x00000525 System.Void ChartAndGraph.AxisBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void AxisBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m149E8A13E39FE49CD6AE328440495767B8AF2A22 (void);
// 0x00000526 System.Void ChartAndGraph.AxisBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void AxisBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA4BAE054BD5AB67D47D4FA68D03A4EFF4FBF1463 (void);
// 0x00000527 System.Void ChartAndGraph.AxisBase/TextData::.ctor()
extern void TextData__ctor_m4216DCC59986D9093F0995CFE2D98D79E56A7D79 (void);
// 0x00000528 System.Void ChartAndGraph.AxisBase/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_mD617E10C9C6AB4F7494C3D0404931DB468EBBAA7 (void);
// 0x00000529 System.Double ChartAndGraph.AxisBase/<>c__DisplayClass24_0::<DrawDivisions>b__0(System.Double)
extern void U3CU3Ec__DisplayClass24_0_U3CDrawDivisionsU3Eb__0_m3D2EECC8B0445830AFE47CC25130FAD2125B7439 (void);
// 0x0000052A System.Void ChartAndGraph.ChartDivisionInfo::ValidateProperites()
extern void ChartDivisionInfo_ValidateProperites_mD93B05166C93F77CD4F42F671996F4E6C1552038 (void);
// 0x0000052B System.Single ChartAndGraph.ChartDivisionInfo::ValidateTotal(System.Single)
extern void ChartDivisionInfo_ValidateTotal_m2BF95482450BFF955B863413F85528FDDA2F43FA (void);
// 0x0000052C System.Int32 ChartAndGraph.ChartDivisionInfo::get_Total()
extern void ChartDivisionInfo_get_Total_m7BB758209A186C9033514BCB9EFE63C757FBDC9E (void);
// 0x0000052D System.Void ChartAndGraph.ChartDivisionInfo::set_Total(System.Int32)
extern void ChartDivisionInfo_set_Total_mD516998AD754475DC701DFA759A9009E70ACD3B7 (void);
// 0x0000052E UnityEngine.Material ChartAndGraph.ChartDivisionInfo::get_Material()
extern void ChartDivisionInfo_get_Material_mE2A755C7B0BEFBEBBDBDC5BF92888FB70E7AFCB0 (void);
// 0x0000052F System.Void ChartAndGraph.ChartDivisionInfo::set_Material(UnityEngine.Material)
extern void ChartDivisionInfo_set_Material_m745D8D197ECA328158F7725C7802D2CFC9B50EE9 (void);
// 0x00000530 ChartAndGraph.MaterialTiling ChartAndGraph.ChartDivisionInfo::get_MaterialTiling()
extern void ChartDivisionInfo_get_MaterialTiling_mF9A02376A3194C63E193203A524C4C3CC8676114 (void);
// 0x00000531 System.Void ChartAndGraph.ChartDivisionInfo::set_MaterialTiling(ChartAndGraph.MaterialTiling)
extern void ChartDivisionInfo_set_MaterialTiling_mAB9B54764EF3298C9F41FBCCD011517C8A3D6FCC (void);
// 0x00000532 ChartAndGraph.AutoFloat ChartAndGraph.ChartDivisionInfo::get_MarkBackLength()
extern void ChartDivisionInfo_get_MarkBackLength_m4BF971DBD25DF2FB74E2F6639F2752F52E6B4A2C (void);
// 0x00000533 System.Void ChartAndGraph.ChartDivisionInfo::set_MarkBackLength(ChartAndGraph.AutoFloat)
extern void ChartDivisionInfo_set_MarkBackLength_m5E30B9905A6E1F0148E4C4BD4382BE6B93ADA597 (void);
// 0x00000534 ChartAndGraph.AutoFloat ChartAndGraph.ChartDivisionInfo::get_MarkLength()
extern void ChartDivisionInfo_get_MarkLength_m3F4309A4FFAE198795690F0B2D7DD25197C978FE (void);
// 0x00000535 System.Void ChartAndGraph.ChartDivisionInfo::set_MarkLength(ChartAndGraph.AutoFloat)
extern void ChartDivisionInfo_set_MarkLength_mB2F426BF392CCCC44F220DC9838F1B5E6BEE3E8C (void);
// 0x00000536 ChartAndGraph.AutoFloat ChartAndGraph.ChartDivisionInfo::get_MarkDepth()
extern void ChartDivisionInfo_get_MarkDepth_m6F11558F65AB4EDE28C2B258BF417DBC194234A4 (void);
// 0x00000537 System.Void ChartAndGraph.ChartDivisionInfo::set_MarkDepth(ChartAndGraph.AutoFloat)
extern void ChartDivisionInfo_set_MarkDepth_m372D941BC4CC9043FF5E5A90077ECD57F28E9F76 (void);
// 0x00000538 System.Single ChartAndGraph.ChartDivisionInfo::get_MarkThickness()
extern void ChartDivisionInfo_get_MarkThickness_mA04DED009CD08FB63BFE7D8C2571C3DC6E5245AC (void);
// 0x00000539 System.Void ChartAndGraph.ChartDivisionInfo::set_MarkThickness(System.Single)
extern void ChartDivisionInfo_set_MarkThickness_m8E497798E1EC08ED8A1557D618B3A03B113F13A9 (void);
// 0x0000053A UnityEngine.MonoBehaviour ChartAndGraph.ChartDivisionInfo::get_TextPrefab()
extern void ChartDivisionInfo_get_TextPrefab_m7CB115E4D4CB080698E23D1044FD87D6E8EAEF27 (void);
// 0x0000053B System.Void ChartAndGraph.ChartDivisionInfo::set_TextPrefab(UnityEngine.MonoBehaviour)
extern void ChartDivisionInfo_set_TextPrefab_mE1CEDDEB6AA0A1E3968AEB8D043B1A743E990F6D (void);
// 0x0000053C System.String ChartAndGraph.ChartDivisionInfo::get_TextPrefix()
extern void ChartDivisionInfo_get_TextPrefix_m3B0CA6F1330AF91EDBB2DD006313056866B53CE5 (void);
// 0x0000053D System.Void ChartAndGraph.ChartDivisionInfo::set_TextPrefix(System.String)
extern void ChartDivisionInfo_set_TextPrefix_m56FA18F5893B1BEBD53BF0DD75257D2B2955DD80 (void);
// 0x0000053E System.String ChartAndGraph.ChartDivisionInfo::get_TextSuffix()
extern void ChartDivisionInfo_get_TextSuffix_m3CADA61268AD07AA687518E8FB7EC7DECED6B15F (void);
// 0x0000053F System.Void ChartAndGraph.ChartDivisionInfo::set_TextSuffix(System.String)
extern void ChartDivisionInfo_set_TextSuffix_m31E1AFB16EC911503BD4B469DDEC089CBBE9FFD0 (void);
// 0x00000540 System.Int32 ChartAndGraph.ChartDivisionInfo::get_FractionDigits()
extern void ChartDivisionInfo_get_FractionDigits_m51A270577AB840D4A6CA58C06D03E24BEBD81C2B (void);
// 0x00000541 System.Void ChartAndGraph.ChartDivisionInfo::set_FractionDigits(System.Int32)
extern void ChartDivisionInfo_set_FractionDigits_m7599C8B3FA890B8905B86D88ECCB37A639775474 (void);
// 0x00000542 System.Int32 ChartAndGraph.ChartDivisionInfo::get_FontSize()
extern void ChartDivisionInfo_get_FontSize_mA05ED2255799B7394183E729C861354C7564A1AF (void);
// 0x00000543 System.Void ChartAndGraph.ChartDivisionInfo::set_FontSize(System.Int32)
extern void ChartDivisionInfo_set_FontSize_m7757B5D592526A9A7DC62E244DE2BADCF0423BCC (void);
// 0x00000544 System.Single ChartAndGraph.ChartDivisionInfo::get_FontSharpness()
extern void ChartDivisionInfo_get_FontSharpness_m7EEC411FA1E2D77BAE3B2C8DF5A2BD68C0BFB0D4 (void);
// 0x00000545 System.Void ChartAndGraph.ChartDivisionInfo::set_FontSharpness(System.Single)
extern void ChartDivisionInfo_set_FontSharpness_mB02C95D91A79FA48E73E399A1BBC2295AFB6AF33 (void);
// 0x00000546 System.Single ChartAndGraph.ChartDivisionInfo::get_TextDepth()
extern void ChartDivisionInfo_get_TextDepth_m39B0EBE47E0F4B910CAEF035366997653E44F248 (void);
// 0x00000547 System.Void ChartAndGraph.ChartDivisionInfo::set_TextDepth(System.Single)
extern void ChartDivisionInfo_set_TextDepth_m57A2FF3D0F3DD645046FB1FF264983C011FF3439 (void);
// 0x00000548 System.Single ChartAndGraph.ChartDivisionInfo::get_TextSeperation()
extern void ChartDivisionInfo_get_TextSeperation_m68F561A9D3A5A570D063732EDD7EC4EA561637F2 (void);
// 0x00000549 System.Void ChartAndGraph.ChartDivisionInfo::set_TextSeperation(System.Single)
extern void ChartDivisionInfo_set_TextSeperation_m7454560344AEA16B5DEE0F01231E5089D00F1E23 (void);
// 0x0000054A ChartAndGraph.ChartDivisionAligment ChartAndGraph.ChartDivisionInfo::get_Alignment()
extern void ChartDivisionInfo_get_Alignment_m0532B71A29B449887AF923B5B3D4B289C8865BD8 (void);
// 0x0000054B System.Void ChartAndGraph.ChartDivisionInfo::set_Alignment(ChartAndGraph.ChartDivisionAligment)
extern void ChartDivisionInfo_set_Alignment_m3F775CB3DAE6ADB25C464BB9390A5C966D1C158A (void);
// 0x0000054C System.Void ChartAndGraph.ChartDivisionInfo::add_OnDataUpdate(System.EventHandler)
extern void ChartDivisionInfo_add_OnDataUpdate_mAC71E3C98BDF170D02D0AD5B2696655EC74FFD36 (void);
// 0x0000054D System.Void ChartAndGraph.ChartDivisionInfo::remove_OnDataUpdate(System.EventHandler)
extern void ChartDivisionInfo_remove_OnDataUpdate_mB943D54C9978DFF4D3502C4E7977E206BCCE64BA (void);
// 0x0000054E System.Void ChartAndGraph.ChartDivisionInfo::add_OnDataChanged(System.EventHandler)
extern void ChartDivisionInfo_add_OnDataChanged_m3D8BD87F819D255B2B010F2123A4EEC7F76A1A5B (void);
// 0x0000054F System.Void ChartAndGraph.ChartDivisionInfo::remove_OnDataChanged(System.EventHandler)
extern void ChartDivisionInfo_remove_OnDataChanged_m2287FF3D6D991ACCB9A6D84E73C352A9ADFD193D (void);
// 0x00000550 System.Void ChartAndGraph.ChartDivisionInfo::RaiseOnChanged()
extern void ChartDivisionInfo_RaiseOnChanged_m96598C18206F693726BC7ACACE735EFFEC79BB10 (void);
// 0x00000551 System.Void ChartAndGraph.ChartDivisionInfo::ChartAndGraph.IInternalSettings.add_InternalOnDataUpdate(System.EventHandler)
extern void ChartDivisionInfo_ChartAndGraph_IInternalSettings_add_InternalOnDataUpdate_m5A7281BD06A242A59A10B899F511579044917816 (void);
// 0x00000552 System.Void ChartAndGraph.ChartDivisionInfo::ChartAndGraph.IInternalSettings.remove_InternalOnDataUpdate(System.EventHandler)
extern void ChartDivisionInfo_ChartAndGraph_IInternalSettings_remove_InternalOnDataUpdate_m8D5C8082E73714CCB704EB87EBD39A6BF4DC41D9 (void);
// 0x00000553 System.Void ChartAndGraph.ChartDivisionInfo::ChartAndGraph.IInternalSettings.add_InternalOnDataChanged(System.EventHandler)
extern void ChartDivisionInfo_ChartAndGraph_IInternalSettings_add_InternalOnDataChanged_m276BBB7A9EB0CBA81CB692EA63BE25BE266A6FEC (void);
// 0x00000554 System.Void ChartAndGraph.ChartDivisionInfo::ChartAndGraph.IInternalSettings.remove_InternalOnDataChanged(System.EventHandler)
extern void ChartDivisionInfo_ChartAndGraph_IInternalSettings_remove_InternalOnDataChanged_m798E0365201A06C962332288F93F3C1B53FCB5F4 (void);
// 0x00000555 System.Void ChartAndGraph.ChartDivisionInfo::.ctor()
extern void ChartDivisionInfo__ctor_mAE48277429EE22F3095A9BD356BD01BDFAD23890 (void);
// 0x00000556 ChartAndGraph.ChartDivisionInfo/DivisionMessure ChartAndGraph.ChartMainDivisionInfo::get_Messure()
extern void ChartMainDivisionInfo_get_Messure_m3A745B8AFF8342228A85ED87E7461F3C29DF350B (void);
// 0x00000557 System.Void ChartAndGraph.ChartMainDivisionInfo::set_Messure(ChartAndGraph.ChartDivisionInfo/DivisionMessure)
extern void ChartMainDivisionInfo_set_Messure_mD5FE8E88B1E01656A50D23F11EB72444B27BB620 (void);
// 0x00000558 System.Single ChartAndGraph.ChartMainDivisionInfo::get_UnitsPerDivision()
extern void ChartMainDivisionInfo_get_UnitsPerDivision_mBAE6D6BE781F0775246686E15C013D456EE86BC1 (void);
// 0x00000559 System.Void ChartAndGraph.ChartMainDivisionInfo::set_UnitsPerDivision(System.Single)
extern void ChartMainDivisionInfo_set_UnitsPerDivision_mD65F104B210C644C441248572FCDEDC71863774F (void);
// 0x0000055A System.Void ChartAndGraph.ChartMainDivisionInfo::.ctor()
extern void ChartMainDivisionInfo__ctor_m36A0C51E13312389352CE4BE3C20BC4300145271 (void);
// 0x0000055B System.Single ChartAndGraph.ChartSubDivisionInfo::ValidateTotal(System.Single)
extern void ChartSubDivisionInfo_ValidateTotal_mAB4A7442E0EF979F30D93919FDDD4B59C26E197E (void);
// 0x0000055C System.Void ChartAndGraph.ChartSubDivisionInfo::.ctor()
extern void ChartSubDivisionInfo__ctor_m7E25D859786C5DD5A818E5396B3CBC9F4F660F5C (void);
// 0x0000055D System.Action`2<ChartAndGraph.IInternalUse,System.Boolean> ChartAndGraph.HorizontalAxis::get_Assign()
extern void HorizontalAxis_get_Assign_mB5D4219D8BF79A619838F02DA44746F619CFDC3C (void);
// 0x0000055E System.Void ChartAndGraph.HorizontalAxis::.ctor()
extern void HorizontalAxis__ctor_mDC1DCA585B8D54CCD7439E598E5C92AF9F3DC084 (void);
// 0x0000055F System.Void ChartAndGraph.HorizontalAxis::<get_Assign>b__1_0(ChartAndGraph.IInternalUse,System.Boolean)
extern void HorizontalAxis_U3Cget_AssignU3Eb__1_0_m6189D24F8E7B7616E51A82A16920AA7CAE45E1DA (void);
// 0x00000560 System.Action`2<ChartAndGraph.IInternalUse,System.Boolean> ChartAndGraph.VerticalAxis::get_Assign()
extern void VerticalAxis_get_Assign_m30C196DEBA9CE834E7CB196D40E5F28FB9AA433C (void);
// 0x00000561 System.Void ChartAndGraph.VerticalAxis::.ctor()
extern void VerticalAxis__ctor_mB2B5A009C7D1120860B60A7E31092B9A088522A7 (void);
// 0x00000562 System.Void ChartAndGraph.VerticalAxis::<get_Assign>b__1_0(ChartAndGraph.IInternalUse,System.Boolean)
extern void VerticalAxis_U3Cget_AssignU3Eb__1_0_m89177C3B9F17E4E1551B566302417DC94F34F3A4 (void);
// 0x00000563 System.Void ChartAndGraph.AxisChart::.ctor()
extern void AxisChart__ctor_mE8EC81B20982714816F0E5E988DC118345638B58 (void);
// 0x00000564 System.Void ChartAndGraph.BarChart::SetLabelOverride(System.String,System.String,System.String)
extern void BarChart_SetLabelOverride_m294414DAAF55862CD5FD223B152C52794851A0E5 (void);
// 0x00000565 System.Void ChartAndGraph.BarChart::RemoveLabelOverride(System.String,System.String)
extern void BarChart_RemoveLabelOverride_m259E01273E11FB832B180E55AE4C227085BA92CC (void);
// 0x00000566 System.Void ChartAndGraph.BarChart::ClearLabelOverride(System.String,System.String)
extern void BarChart_ClearLabelOverride_m8CCE08144E8B5E35E2A87941A1E768ACB864596E (void);
// 0x00000567 ChartAndGraph.IChartData ChartAndGraph.BarChart::get_DataLink()
extern void BarChart_get_DataLink_m085A1B141778BB69E13F3491697192CA80443A62 (void);
// 0x00000568 System.Void ChartAndGraph.BarChart::OnBeforeSerializeEvent()
extern void BarChart_OnBeforeSerializeEvent_m30FC1578E3BFA2106AD9268BA3865B8AE3AA1D71 (void);
// 0x00000569 System.Void ChartAndGraph.BarChart::OnAfterDeserializeEvent()
extern void BarChart_OnAfterDeserializeEvent_m2B8F8E903EC88922C1663FCFA0E3B2031C976D55 (void);
// 0x0000056A ChartAndGraph.BarChart/BarType ChartAndGraph.BarChart::get_ViewType()
extern void BarChart_get_ViewType_m7D2587E3CB9CE5782987C0952A31ECA5C5707D1C (void);
// 0x0000056B System.Void ChartAndGraph.BarChart::set_ViewType(ChartAndGraph.BarChart/BarType)
extern void BarChart_set_ViewType_m037C90F5FBB00732E708980C3E56F34A47913661 (void);
// 0x0000056C System.Boolean ChartAndGraph.BarChart::get_NegativeBars()
extern void BarChart_get_NegativeBars_m077E71C8B9BEAE206C77437E7F89D2906595E024 (void);
// 0x0000056D System.Void ChartAndGraph.BarChart::set_NegativeBars(System.Boolean)
extern void BarChart_set_NegativeBars_m032335B40D045C2EC8EA01D6AB9D79BC1D6EAD43 (void);
// 0x0000056E System.Boolean ChartAndGraph.BarChart::get_Stacked()
extern void BarChart_get_Stacked_mC462CD191145BE607CA526E9CA6AD341D0491140 (void);
// 0x0000056F System.Void ChartAndGraph.BarChart::set_Stacked(System.Boolean)
extern void BarChart_set_Stacked_m2195F482DB877599A93EE2573B0AE71D12CE52C3 (void);
// 0x00000570 System.Boolean ChartAndGraph.BarChart::get_ShouldFitCanvas()
extern void BarChart_get_ShouldFitCanvas_m60998B83C088913AD58633C2DAB06A4E8AAC2217 (void);
// 0x00000571 ChartAndGraph.BarData ChartAndGraph.BarChart::get_DataSource()
extern void BarChart_get_DataSource_mE818D5CEE1B81C3346661C3A60F34488763D8FEC (void);
// 0x00000572 UnityEngine.GameObject ChartAndGraph.BarChart::get_BarPrefabLink()
// 0x00000573 ChartAndGraph.ChartOrientedSize ChartAndGraph.BarChart::get_AxisSeperationLink()
// 0x00000574 ChartAndGraph.ChartOrientedSize ChartAndGraph.BarChart::get_BarSeperationLink()
// 0x00000575 ChartAndGraph.ChartOrientedSize ChartAndGraph.BarChart::get_GroupSeperationLink()
// 0x00000576 ChartAndGraph.ChartOrientedSize ChartAndGraph.BarChart::get_BarSizeLink()
// 0x00000577 System.Single ChartAndGraph.BarChart::get_TransitionTimeBetaFeature()
extern void BarChart_get_TransitionTimeBetaFeature_m2CB8DA93CA9B45D9773563ACDC95E157E626753E (void);
// 0x00000578 System.Void ChartAndGraph.BarChart::set_TransitionTimeBetaFeature(System.Single)
extern void BarChart_set_TransitionTimeBetaFeature_mAC0FA9B32D4859D9734E2437E199E7F3104E9156 (void);
// 0x00000579 System.Single ChartAndGraph.BarChart::get_HeightRatio()
extern void BarChart_get_HeightRatio_mD7B169039BD4012852CE55529BE9E64BE80264CB (void);
// 0x0000057A System.Void ChartAndGraph.BarChart::set_HeightRatio(System.Single)
extern void BarChart_set_HeightRatio_m33D77C10C4D9FBE26B40D6648B5FC439AE88C452 (void);
// 0x0000057B System.Single ChartAndGraph.BarChart::get_TotalDepthLink()
extern void BarChart_get_TotalDepthLink_mD78B79475A9CF469C5593EA02BD80E97EE900047 (void);
// 0x0000057C System.Single ChartAndGraph.BarChart::get_TotalHeightLink()
extern void BarChart_get_TotalHeightLink_m8C9CBE17D577AC9220EAB770F88E96E63D356BE8 (void);
// 0x0000057D System.Single ChartAndGraph.BarChart::get_TotalWidthLink()
extern void BarChart_get_TotalWidthLink_m21ACDD8A8B139A42A0DB88FC7F4926DB0BE3EDEC (void);
// 0x0000057E System.Void ChartAndGraph.BarChart::Start()
extern void BarChart_Start_mE2CCD4A20FFC0F083CC3C7929732EECF0AB621C0 (void);
// 0x0000057F System.Void ChartAndGraph.BarChart::ValidateProperties()
extern void BarChart_ValidateProperties_m0AA020A9567E980600725D8FC5C3E37F0E27A243 (void);
// 0x00000580 System.Void ChartAndGraph.BarChart::Update()
extern void BarChart_Update_m7D226A7789F53B654AF8E23EB4DD36F50D1BB396 (void);
// 0x00000581 System.Void ChartAndGraph.BarChart::UpdateAnimations()
extern void BarChart_UpdateAnimations_mAF3868196D432B9CD7C5857F50CC1C4EC47CF8DE (void);
// 0x00000582 System.Void ChartAndGraph.BarChart::OnValidate()
extern void BarChart_OnValidate_mEDED956F87D23C20A4946AB129E19BE6D68BE847 (void);
// 0x00000583 System.Void ChartAndGraph.BarChart::HookEvents()
extern void BarChart_HookEvents_mCCF779B67EDDB40EF7F10826A5EEC01CD94C4E01 (void);
// 0x00000584 System.Void ChartAndGraph.BarChart::InternalDataSource_ItemsReplaced(System.String,System.Int32,System.String,System.Int32)
extern void BarChart_InternalDataSource_ItemsReplaced_m8616CBCB2D04EABB1558B1DD3B711A82C2CA4626 (void);
// 0x00000585 System.Void ChartAndGraph.BarChart::Data_ProperyUpdated()
extern void BarChart_Data_ProperyUpdated_m580AE28485649AEA286A3997C2C14467EA12A380 (void);
// 0x00000586 System.Void ChartAndGraph.BarChart::MDataSource_DataValueChanged1(System.Object,ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs)
extern void BarChart_MDataSource_DataValueChanged1_mB08F479A4060C9A4ABFC255BD4A8AE5F94AF3D99 (void);
// 0x00000587 System.Void ChartAndGraph.BarChart::StackedStage1(System.Single&,System.Double)
extern void BarChart_StackedStage1_mD9822EE654B5DCAAC6CA26EE02F3EFCCABC6C480 (void);
// 0x00000588 System.Void ChartAndGraph.BarChart::StackedStage2(System.Double&,System.Double)
extern void BarChart_StackedStage2_m5CE7EEDE40F191E0445F76047F0CB80F38DE44D5 (void);
// 0x00000589 System.Void ChartAndGraph.BarChart::RefreshAllBars()
extern void BarChart_RefreshAllBars_mA3B7A7176074CB61D03D520C8DA9CC2122A1B863 (void);
// 0x0000058A System.Void ChartAndGraph.BarChart::FixBarLabels(ChartAndGraph.BarChart/BarObject)
extern void BarChart_FixBarLabels_mA71DD1699499493BF49C6E6C7DA8F95B4C47406F (void);
// 0x0000058B System.Void ChartAndGraph.BarChart::RefreshBar(ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs)
extern void BarChart_RefreshBar_m776D172F3409083554D0402779723BCB52D0AA44 (void);
// 0x0000058C System.Void ChartAndGraph.BarChart::MDataSource_DataStructureChanged(System.Object,System.EventArgs)
extern void BarChart_MDataSource_DataStructureChanged_m541AFE1E8E23893D4935C00C90F7CFA065964515 (void);
// 0x0000058D System.Boolean ChartAndGraph.BarChart::PointToWorldSpace(System.String,System.String,System.Double,System.Boolean,UnityEngine.Vector3&)
extern void BarChart_PointToWorldSpace_mA8E979ECA152E363C175DB4F41373EECA5E2F6AE (void);
// 0x0000058E System.Boolean ChartAndGraph.BarChart::GetBarBottomPosition(System.String,System.String,UnityEngine.Vector3&)
extern void BarChart_GetBarBottomPosition_m669E02A7AE6DE60929F7A23014CACC7E7DE9245F (void);
// 0x0000058F System.Boolean ChartAndGraph.BarChart::GetBarTrackPosition(System.String,System.String,UnityEngine.Vector3&)
extern void BarChart_GetBarTrackPosition_mC3064B48EAD1ECCBACFC806B3E730818B6925C8C (void);
// 0x00000590 UnityEngine.Vector3 ChartAndGraph.BarChart::AlignLabel(GroupLabels,UnityEngine.Vector3,System.Single)
extern void BarChart_AlignLabel_mD3DDA531B049ACA600FE049540C9A3F26E1A046D (void);
// 0x00000591 UnityEngine.Vector3 ChartAndGraph.BarChart::AlignLabel(ChartAndGraph.AlignedItemLabels,UnityEngine.Vector3,System.Single)
extern void BarChart_AlignLabel_m46E8B6C1FE7F8516431E877E27CC275463D5C096 (void);
// 0x00000592 System.Void ChartAndGraph.BarChart::UpdateTextLables()
extern void BarChart_UpdateTextLables_mC3B3CFC7C2EB7BDBE7698B09D4CB0F66FEEF170E (void);
// 0x00000593 UnityEngine.GameObject ChartAndGraph.BarChart::CreateBar(UnityEngine.Vector3,System.Double,System.Single,System.Single,System.Single,System.String,System.String,System.Int32,System.Int32)
extern void BarChart_CreateBar_m29CB7F4E466DCD46EC87DB2B138A1A625FFD0895 (void);
// 0x00000594 UnityEngine.Vector3 ChartAndGraph.BarChart::GetTopPosition(ChartAndGraph.BarChart/BarObject)
extern void BarChart_GetTopPosition_m9763D1F94C82DAFC7F207F500DF6EB496BFA4705 (void);
// 0x00000595 ChartAndGraph.BarChart/BarEventArgs ChartAndGraph.BarChart::BarObjToEventArgs(ChartAndGraph.BarChart/BarObject)
extern void BarChart_BarObjToEventArgs_mEDE9EF9F74A962F4C27DCF194D852CA9A2E2C85C (void);
// 0x00000596 ChartAndGraph.BarChart/BarEventArgs ChartAndGraph.BarChart::UserDataToEventArgs(System.Object)
extern void BarChart_UserDataToEventArgs_m589E2DB7ED2E8A497AEB6647F44869FEA51660D9 (void);
// 0x00000597 System.Boolean ChartAndGraph.BarChart::get_SupportRealtimeGeneration()
extern void BarChart_get_SupportRealtimeGeneration_m077E8988B961920DF1FE5FCB35CA4D99551614E4 (void);
// 0x00000598 System.Void ChartAndGraph.BarChart::OnItemHoverted(System.Object)
extern void BarChart_OnItemHoverted_m4E15759CB055C3BDDBF5094CE07E93C63E813426 (void);
// 0x00000599 System.Void ChartAndGraph.BarChart::OnNonHoverted()
extern void BarChart_OnNonHoverted_m05A0AB73063F68853B97614EEA177F6938C824EE (void);
// 0x0000059A System.Void ChartAndGraph.BarChart::OnItemSelected(System.Object)
extern void BarChart_OnItemSelected_m83086136E91ECF8F79CCE662FF0FFF671F583AF9 (void);
// 0x0000059B System.Void ChartAndGraph.BarChart::OnPropertyUpdated()
extern void BarChart_OnPropertyUpdated_m5976C15596FACB0215ED806F5AE7F5D6ADCB70F0 (void);
// 0x0000059C System.Void ChartAndGraph.BarChart::SetBarSize(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void BarChart_SetBarSize_m25D60A5794087E8F46D77FBC1542B7ECAA041AF2 (void);
// 0x0000059D System.Void ChartAndGraph.BarChart::AddBarToChart(System.Double,System.Double,System.Double,System.Double,System.Double,System.String,System.String,System.Int32,System.Int32,System.Double)
extern void BarChart_AddBarToChart_m194E18C4318234C4FE91F850BE881D0B1E1C963F (void);
// 0x0000059E System.Boolean ChartAndGraph.BarChart::HasValues(ChartAndGraph.AxisBase)
extern void BarChart_HasValues_mD38A53F80284BE06DA833EBB1FEEC081BDB69ED7 (void);
// 0x0000059F System.Double ChartAndGraph.BarChart::MinValue(ChartAndGraph.AxisBase)
extern void BarChart_MinValue_m21AB9B71D3F1F19496054A04AFBEFE82E84C6868 (void);
// 0x000005A0 System.Double ChartAndGraph.BarChart::MaxValue(ChartAndGraph.AxisBase)
extern void BarChart_MaxValue_m9F8A2CE3DC95D5C13AA19D92BD06ACBCCE8C828A (void);
// 0x000005A1 UnityEngine.Vector3 ChartAndGraph.BarChart::AlignGroupLabel(ChartAndGraph.BarChart/GroupObject)
extern void BarChart_AlignGroupLabel_m05A24C987A4E57E2DD3DED145B534CA21B35631A (void);
// 0x000005A2 System.Void ChartAndGraph.BarChart::StackedStage3(System.Double&,System.Double,System.Double,System.Double)
extern void BarChart_StackedStage3_m22553F4F512D7C2F5159773AA4F31966FB926F23 (void);
// 0x000005A3 System.Void ChartAndGraph.BarChart::AddRowToChartMesh(System.Double[0...,0...],System.Int32,System.Double,System.Double)
extern void BarChart_AddRowToChartMesh_mDEF7D4EF74727E15A39917033F6FB34D5201711B (void);
// 0x000005A4 System.Void ChartAndGraph.BarChart::ClearChart()
extern void BarChart_ClearChart_mEC5800F0BE5CBA4DBA9E95DA1522CE4C04C964A9 (void);
// 0x000005A5 System.Void ChartAndGraph.BarChart::OnLabelSettingChanged()
extern void BarChart_OnLabelSettingChanged_m8980A1D7559BEF60E522042F68CE6CFDEC7D8B09 (void);
// 0x000005A6 System.Void ChartAndGraph.BarChart::OnAxisValuesChanged()
extern void BarChart_OnAxisValuesChanged_m00C7112EDA9466C15B7A8694B1BB2D9EC51A997C (void);
// 0x000005A7 System.Void ChartAndGraph.BarChart::OnLabelSettingsSet()
extern void BarChart_OnLabelSettingsSet_m34CC22285E88025974DAC9C3F162C3449605776D (void);
// 0x000005A8 System.Single ChartAndGraph.BarChart::MessureWidth()
extern void BarChart_MessureWidth_m73AFC9F8AD30F3BA5D7BCF29E1651CF4433D7012 (void);
// 0x000005A9 System.Void ChartAndGraph.BarChart::InternalGenerateChart()
extern void BarChart_InternalGenerateChart_mC877303FD59FBA7166CF8793A671C6E9E0AC04A6 (void);
// 0x000005AA ChartAndGraph.LegenedData ChartAndGraph.BarChart::get_LegendInfo()
extern void BarChart_get_LegendInfo_m14FF6C2E093EB8F7536962FFBBE8B7F28B722DED (void);
// 0x000005AB System.Boolean ChartAndGraph.BarChart::get_SupportsCategoryLabels()
extern void BarChart_get_SupportsCategoryLabels_m5C3DCF476C28897B546746C0DBD9A9EA74BB193A (void);
// 0x000005AC System.Boolean ChartAndGraph.BarChart::get_SupportsGroupLables()
extern void BarChart_get_SupportsGroupLables_mB7628DC5B947A49CB22ED77F04F12A00CFEE409D (void);
// 0x000005AD System.Boolean ChartAndGraph.BarChart::get_SupportsItemLabels()
extern void BarChart_get_SupportsItemLabels_m0C5FA8519A044733D76BA5432E58577B67BB5028 (void);
// 0x000005AE System.Void ChartAndGraph.BarChart::.ctor()
extern void BarChart__ctor_mBDBF0E0952D22589FFF07D983F47911150643724 (void);
// 0x000005AF System.Void ChartAndGraph.BarChart::.cctor()
extern void BarChart__cctor_m673259ECD04A6F9BFA6C6AF3C7B57FDAD03BBF87 (void);
// 0x000005B0 System.Void ChartAndGraph.BarChart/BarEventArgs::.ctor(UnityEngine.Vector3,System.Double,System.String,System.String)
extern void BarEventArgs__ctor_m84F48C27C08BE4462D8BFE7C23A1112DED835F1B (void);
// 0x000005B1 UnityEngine.Vector3 ChartAndGraph.BarChart/BarEventArgs::get_TopPosition()
extern void BarEventArgs_get_TopPosition_mE31D84CAA751B3DF74913ACA6373FF46CEBD4013 (void);
// 0x000005B2 System.Void ChartAndGraph.BarChart/BarEventArgs::set_TopPosition(UnityEngine.Vector3)
extern void BarEventArgs_set_TopPosition_m41B22FE4648B00A7E795C1F56F6E1C686BFB43E2 (void);
// 0x000005B3 System.Double ChartAndGraph.BarChart/BarEventArgs::get_Value()
extern void BarEventArgs_get_Value_m06CD6D6E61DA6E762AEE2A011CD6B5B104FE331B (void);
// 0x000005B4 System.Void ChartAndGraph.BarChart/BarEventArgs::set_Value(System.Double)
extern void BarEventArgs_set_Value_mC913E728B00D39DDCDFBD63E75B9BC2932DE9B03 (void);
// 0x000005B5 System.String ChartAndGraph.BarChart/BarEventArgs::get_Category()
extern void BarEventArgs_get_Category_m3E31C561F7C77965215EF838112B706808754EC6 (void);
// 0x000005B6 System.Void ChartAndGraph.BarChart/BarEventArgs::set_Category(System.String)
extern void BarEventArgs_set_Category_mF8E4603293F4B61290C0AC030DF8C92605749AD1 (void);
// 0x000005B7 System.String ChartAndGraph.BarChart/BarEventArgs::get_Group()
extern void BarEventArgs_get_Group_mE173DA1B1DCD09DF12A094D76BAECE8CE0A1FCFD (void);
// 0x000005B8 System.Void ChartAndGraph.BarChart/BarEventArgs::set_Group(System.String)
extern void BarEventArgs_set_Group_mB3EA1BA0BFB6DDC8B50F8CC8A8C5038C2ACFA69A (void);
// 0x000005B9 System.Void ChartAndGraph.BarChart/SwitchAnimationEntry::.ctor()
extern void SwitchAnimationEntry__ctor_mB5E36853ADBCC71F59DAC8F6EF5234BFC88AEEE9 (void);
// 0x000005BA System.Void ChartAndGraph.BarChart/BarEvent::.ctor()
extern void BarEvent__ctor_m26C4A0E267E38335493679CC4232CE27AB526F2F (void);
// 0x000005BB System.Void ChartAndGraph.BarChart/BarObject::.ctor()
extern void BarObject__ctor_mBE53D24F981B184F73C0F1551FCDE6727F50A896 (void);
// 0x000005BC System.Void ChartAndGraph.BarChart/GroupObject::.ctor()
extern void GroupObject__ctor_mF35FBF00B2102156BFD71595F90A3C8C48F646C0 (void);
// 0x000005BD System.Void ChartAndGraph.BarChart/LabelPositionInfo::.ctor(ItemLabelsBase,ChartAndGraph.BarChart/BarObject)
extern void LabelPositionInfo__ctor_m9A08B018C1FB8E065DFA2B1B34E8C6148A8F5168 (void);
// 0x000005BE System.Void ChartAndGraph.BarChart/LabelPositionInfo::.ctor(ItemLabelsBase,ChartAndGraph.BarChart/GroupObject)
extern void LabelPositionInfo__ctor_mF62A3DF64E35F11CA1218B40AE99723F2636DE26 (void);
// 0x000005BF ChartAndGraph.ChartSparseDataSource ChartAndGraph.BarData::ChartAndGraph.IInternalBarData.get_InternalDataSource()
extern void BarData_ChartAndGraph_IInternalBarData_get_InternalDataSource_mB9A94792A3DA6E4C969CB8258D9BBA843B90B511 (void);
// 0x000005C0 System.Void ChartAndGraph.BarData::add_ProperyUpdated(System.Action)
extern void BarData_add_ProperyUpdated_m3BA9B92B6E412D8239F68AA155C0AC8109B79693 (void);
// 0x000005C1 System.Void ChartAndGraph.BarData::remove_ProperyUpdated(System.Action)
extern void BarData_remove_ProperyUpdated_mF53A3D39F62A6B39588F48F64E386BA2A7A29107 (void);
// 0x000005C2 System.Void ChartAndGraph.BarData::RaisePropertyUpdated()
extern void BarData_RaisePropertyUpdated_m531A88B715B3D9B09C48AA1CF93CC5D39B43B113 (void);
// 0x000005C3 System.Int32 ChartAndGraph.BarData::get_TotalCategories()
extern void BarData_get_TotalCategories_m8D7A20AF0D0696ABA9B5278F0EC5C5EBE2F2E6AE (void);
// 0x000005C4 System.Int32 ChartAndGraph.BarData::get_TotalGroups()
extern void BarData_get_TotalGroups_m2B4852C3EB01BBEAAF6C64934E14A5DE365481D2 (void);
// 0x000005C5 System.Void ChartAndGraph.BarData::Update()
extern void BarData_Update_m0798CF1E8580BA04A7DF49A34FEC9F6BFFE506CC (void);
// 0x000005C6 System.Void ChartAndGraph.BarData::RenameCategory(System.String,System.String)
extern void BarData_RenameCategory_m56EEE3DB12B836BFDE843FDD02E17EBA96477CC2 (void);
// 0x000005C7 System.Void ChartAndGraph.BarData::RenameGroup(System.String,System.String)
extern void BarData_RenameGroup_m67141774259FF3651B04253CF068F35BC069C62D (void);
// 0x000005C8 System.Boolean ChartAndGraph.BarData::get_AutomaticMaxValue()
extern void BarData_get_AutomaticMaxValue_m583863705121013C75414377A17956908CE2ED3F (void);
// 0x000005C9 System.Void ChartAndGraph.BarData::set_AutomaticMaxValue(System.Boolean)
extern void BarData_set_AutomaticMaxValue_mB91DB91E0C540459CDF85003CA15FDBD21A033C7 (void);
// 0x000005CA System.Double ChartAndGraph.BarData::get_MaxValue()
extern void BarData_get_MaxValue_mD6E8FF0722B6FCF2DBD5769D5A331BF9953BEE66 (void);
// 0x000005CB System.Void ChartAndGraph.BarData::set_MaxValue(System.Double)
extern void BarData_set_MaxValue_mF179732A9C2CC11EC9067B522F6EF848A424214B (void);
// 0x000005CC System.Boolean ChartAndGraph.BarData::HasGroup(System.String)
extern void BarData_HasGroup_m1E64B99CDBC2E330C0F160F88560EE4C5C1FD4B4 (void);
// 0x000005CD System.Boolean ChartAndGraph.BarData::HasCategory(System.String)
extern void BarData_HasCategory_m0D921031E131F02218D24E9F6E2CBBED14FAD40B (void);
// 0x000005CE System.Boolean ChartAndGraph.BarData::get_AutomaticMinValue()
extern void BarData_get_AutomaticMinValue_m1C951AEB21DC840AB2503894376F3E7BF567F513 (void);
// 0x000005CF System.Void ChartAndGraph.BarData::set_AutomaticMinValue(System.Boolean)
extern void BarData_set_AutomaticMinValue_mF51752411AB76C72C3F45976C60C8C362C6999E3 (void);
// 0x000005D0 System.Double ChartAndGraph.BarData::get_MinValue()
extern void BarData_get_MinValue_m85AEC54442AB41E522701321963FC52064AEC0B1 (void);
// 0x000005D1 System.Void ChartAndGraph.BarData::set_MinValue(System.Double)
extern void BarData_set_MinValue_m008A5E0195CE724472CC6A3A79B37189DE7BB47D (void);
// 0x000005D2 System.Void ChartAndGraph.BarData::ChartAndGraph.IInternalBarData.Update()
extern void BarData_ChartAndGraph_IInternalBarData_Update_m81B65DDD107E4F5D6BAFE099836475BCB725F813 (void);
// 0x000005D3 System.Void ChartAndGraph.BarData::StartBatch()
extern void BarData_StartBatch_m4F385F71BE659C7EE2D13BEAAC6B11DD9541CA07 (void);
// 0x000005D4 System.Void ChartAndGraph.BarData::EndBatch()
extern void BarData_EndBatch_m907319050E7F64A5A17F0BDD1FA86D241CE0C90F (void);
// 0x000005D5 System.Double ChartAndGraph.BarData::GetMinValue()
extern void BarData_GetMinValue_m169E40646FCCE40F26311B9578EEED8DA63CB4DB (void);
// 0x000005D6 System.Double ChartAndGraph.BarData::GetMaxValue()
extern void BarData_GetMaxValue_mC33E2474B4A7D2A8685879AE8F903CEBCF293D4E (void);
// 0x000005D7 System.String ChartAndGraph.BarData::GetCategoryName(System.Int32)
extern void BarData_GetCategoryName_m39F750B40A7173082132152F872DEEA2EAC2D5AF (void);
// 0x000005D8 System.String ChartAndGraph.BarData::GetGroupName(System.Int32)
extern void BarData_GetGroupName_mC1541D03DD6F44A2F21D7E9A34AFBC7779CBC610 (void);
// 0x000005D9 System.Object[] ChartAndGraph.BarData::StoreAllCategoriesinOrder()
extern void BarData_StoreAllCategoriesinOrder_m1F5AD79FC32E9D51A4B3B92A5F42006FBECA89EC (void);
// 0x000005DA System.Void ChartAndGraph.BarData::OnBeforeSerialize()
extern void BarData_OnBeforeSerialize_m2623BFDA1F9F352E4721B2720BA4F79540449A06 (void);
// 0x000005DB System.Void ChartAndGraph.BarData::OnAfterDeserialize()
extern void BarData_OnAfterDeserialize_m8EC126212D11D283B03A7BF879D1630C3DF4DD96 (void);
// 0x000005DC System.Void ChartAndGraph.BarData::AddCategory(System.String,UnityEngine.Material)
extern void BarData_AddCategory_m42FF55C7638AD1F15480A7F7B7693175F2B5B24A (void);
// 0x000005DD System.Void ChartAndGraph.BarData::ClearGroups()
extern void BarData_ClearGroups_mF23E7AF65B140EF9647F2A8EC8D0BB85411ED6E3 (void);
// 0x000005DE System.Void ChartAndGraph.BarData::ClearValues(System.Double)
extern void BarData_ClearValues_m9D18AA44F8E041ABBC48DE7A29A669F0501142D6 (void);
// 0x000005DF System.Void ChartAndGraph.BarData::ClearCategories()
extern void BarData_ClearCategories_m6A79A7E9A5C0B04F83A68DA64F66D88C0B5A23D1 (void);
// 0x000005E0 System.Void ChartAndGraph.BarData::AddCategory(System.String,ChartAndGraph.ChartDynamicMaterial,System.Int32)
extern void BarData_AddCategory_m2AE3077B9A8E88D02440C6C240F38D2BB9675C42 (void);
// 0x000005E1 System.Void ChartAndGraph.BarData::MoveCategory(System.String,System.Int32)
extern void BarData_MoveCategory_m4C84C891C6796F4155A0EF408C0029D7BB95EE32 (void);
// 0x000005E2 System.Void ChartAndGraph.BarData::SwitchCategoryPositions(System.String,System.String)
extern void BarData_SwitchCategoryPositions_mF9BAF22C0A71C0B1425C99BE35F7A046977A1216 (void);
// 0x000005E3 System.Void ChartAndGraph.BarData::AddCategory(System.String,ChartAndGraph.ChartDynamicMaterial)
extern void BarData_AddCategory_m1C0BF07095D6D78D1BC08EB4FCD0EAF63F2FC869 (void);
// 0x000005E4 System.Void ChartAndGraph.BarData::SetCategoryIndex(System.String,System.Int32)
extern void BarData_SetCategoryIndex_mFAD87119EBE321205C88A725201201E9C14297D8 (void);
// 0x000005E5 System.Void ChartAndGraph.BarData::SetMaterial(System.String,UnityEngine.Material)
extern void BarData_SetMaterial_mFA9601D6B951092A0C3F6835E46AD8EC619CC6D9 (void);
// 0x000005E6 ChartAndGraph.ChartDynamicMaterial ChartAndGraph.BarData::GetMaterial(System.String)
extern void BarData_GetMaterial_m3A24EBB9586D0575B3E074F84906D0E121770CC6 (void);
// 0x000005E7 System.Void ChartAndGraph.BarData::SetMaterial(System.String,ChartAndGraph.ChartDynamicMaterial)
extern void BarData_SetMaterial_mAF3A442C4C0A49B6ED6E7172B9C7DD6865F69626 (void);
// 0x000005E8 System.Void ChartAndGraph.BarData::RemoveCategory(System.String)
extern void BarData_RemoveCategory_mF4898735E1A3B47AD9D43D62BCF3B5E12D80BAE5 (void);
// 0x000005E9 System.Void ChartAndGraph.BarData::RemoveGroup(System.String)
extern void BarData_RemoveGroup_m6953B6993D0EC1839CA014221D587741D04A4859 (void);
// 0x000005EA System.Void ChartAndGraph.BarData::AddGroup(System.String)
extern void BarData_AddGroup_m2BDCF1952F58006910566AB29E674D7532E84C35 (void);
// 0x000005EB System.Double ChartAndGraph.BarData::GetValue(System.String,System.String)
extern void BarData_GetValue_m00A67EEFFEAFDD297C74004A13FFD2898B67B46E (void);
// 0x000005EC System.Boolean ChartAndGraph.BarData::CheckAnimationEnded(System.Single,UnityEngine.AnimationCurve)
extern void BarData_CheckAnimationEnded_mD0BBC1738E0966854A74DBEFC6CD10ACB15A49F9 (void);
// 0x000005ED System.Void ChartAndGraph.BarData::FixEaseFunction(UnityEngine.AnimationCurve)
extern void BarData_FixEaseFunction_m1280087C854FAC6F2E48DC2663511083ECDFA772 (void);
// 0x000005EE System.Void ChartAndGraph.BarData::RestoreCategory(System.String,System.Object)
extern void BarData_RestoreCategory_m782C89A3D9E455DD881C27531F573D32A99D9723 (void);
// 0x000005EF System.Void ChartAndGraph.BarData::SlideValue(System.String,System.String,System.Double,System.Single,UnityEngine.AnimationCurve)
extern void BarData_SlideValue_mB7667D8B22CAF115435F23619C7DA3377F676BDF (void);
// 0x000005F0 System.Void ChartAndGraph.BarData::SlideValue(System.String,System.String,System.Double,System.Single)
extern void BarData_SlideValue_m58CD5E30D4366C9ABDCB38C01C7E7461D1417415 (void);
// 0x000005F1 System.Void ChartAndGraph.BarData::SetValue(System.String,System.String,System.Double)
extern void BarData_SetValue_m93B3BE94FAE368EE150319187E2FB66A8116204C (void);
// 0x000005F2 System.Void ChartAndGraph.BarData::SetValueInternal(System.String,System.String,System.Double)
extern void BarData_SetValueInternal_mC9F39A66F2CFBFE422A25845C51F9F5CF7405099 (void);
// 0x000005F3 System.Void ChartAndGraph.BarData::.ctor()
extern void BarData__ctor_mBEB76DA2F3C502E3A15A701CA185F7529255371F (void);
// 0x000005F4 System.Void ChartAndGraph.BarData/CategoryData::.ctor()
extern void CategoryData__ctor_mF268FE6A4CB2996569B0A48B21A3B57FC402A29E (void);
// 0x000005F5 System.Void ChartAndGraph.BarData/DataEntry::.ctor()
extern void DataEntry__ctor_mDB9A0BE840FE814D2EECDA65BF125A3C255E3422 (void);
// 0x000005F6 System.Void ChartAndGraph.BarData/<>c::.cctor()
extern void U3CU3Ec__cctor_mD9051E904F4D4F371B45A8BB084683970AEAA9D4 (void);
// 0x000005F7 System.Void ChartAndGraph.BarData/<>c::.ctor()
extern void U3CU3Ec__ctor_mB90F5F7DC5B0071BE8A672161CA40C9059452EB5 (void);
// 0x000005F8 System.String ChartAndGraph.BarData/<>c::<ClearGroups>b__48_0(ChartAndGraph.DataSource.ChartDataRow)
extern void U3CU3Ec_U3CClearGroupsU3Eb__48_0_m7E7F6680AFCA52FEB76CD72C9A2A73D1AC9E52AB (void);
// 0x000005F9 System.String ChartAndGraph.BarData/<>c::<ClearValues>b__49_0(ChartAndGraph.DataSource.ChartDataColumn)
extern void U3CU3Ec_U3CClearValuesU3Eb__49_0_mE9FDADB32DF74D3B705AD381D05F310F9247617F (void);
// 0x000005FA System.String ChartAndGraph.BarData/<>c::<ClearValues>b__49_1(ChartAndGraph.DataSource.ChartDataRow)
extern void U3CU3Ec_U3CClearValuesU3Eb__49_1_m5BE4F428D726DC443AA57CDC0732ED7F506D919A (void);
// 0x000005FB System.String ChartAndGraph.BarData/<>c::<ClearCategories>b__50_0(ChartAndGraph.DataSource.ChartDataColumn)
extern void U3CU3Ec_U3CClearCategoriesU3Eb__50_0_m920FBBF0C83904577FCF36AEAD11B435BB90A2FB (void);
// 0x000005FC System.Void ChartAndGraph.BarGenerator::Generate(System.Single,System.Single)
// 0x000005FD System.Void ChartAndGraph.BarGenerator::Clear()
// 0x000005FE System.Void ChartAndGraph.BarGenerator::.ctor()
extern void BarGenerator__ctor_m4C54ACB7B5F4BFFB7182318224C318D142A4D922 (void);
// 0x000005FF ChartAndGraph.BarChart/BarObject ChartAndGraph.BarInfo::get_BarObject()
extern void BarInfo_get_BarObject_m7E3CC708841B7C5940E943A7111C56B722F90D5D (void);
// 0x00000600 System.Void ChartAndGraph.BarInfo::set_BarObject(ChartAndGraph.BarChart/BarObject)
extern void BarInfo_set_BarObject_m55485BFB5ABF5DE9B7ED25DC70E4DC6B63A4852E (void);
// 0x00000601 UnityEngine.GameObject ChartAndGraph.BarInfo::get_ItemLabel()
extern void BarInfo_get_ItemLabel_mA9559BC6E593E85EDAA3A4C499D6B769CD8F1D31 (void);
// 0x00000602 UnityEngine.GameObject ChartAndGraph.BarInfo::get_CategoryLabel()
extern void BarInfo_get_CategoryLabel_mD11D24E4CFB853331D318C9579FDAE77F946237F (void);
// 0x00000603 System.Double ChartAndGraph.BarInfo::get_Value()
extern void BarInfo_get_Value_m0CF1404F57C704D50DAF3E0BC216954878520AD7 (void);
// 0x00000604 System.String ChartAndGraph.BarInfo::get_Category()
extern void BarInfo_get_Category_m4294A194D8642AD1A72693F24CDA407E1BF9FB76 (void);
// 0x00000605 System.String ChartAndGraph.BarInfo::get_Group()
extern void BarInfo_get_Group_m0A3EF74DCAAB461D7EC0FA80AA8C40D1051F56D7 (void);
// 0x00000606 System.Void ChartAndGraph.BarInfo::.ctor()
extern void BarInfo__ctor_mF9DA48C6684929471DB7D28AB4505E1EF752B7D9 (void);
// 0x00000607 UnityEngine.Mesh ChartAndGraph.BarMesh::get_StrechMesh2D()
extern void BarMesh_get_StrechMesh2D_mB5B20B9D7D9AC60FDD62FCED6AFFF1E3F90CFE3B (void);
// 0x00000608 System.Void ChartAndGraph.BarMesh::Update2DMeshUv(UnityEngine.Mesh,System.Single)
extern void BarMesh_Update2DMeshUv_m1BC6E557D6E945866E973467F3F2C6AE21DCF45B (void);
// 0x00000609 UnityEngine.Mesh ChartAndGraph.BarMesh::Generate2DMesh(System.Single)
extern void BarMesh_Generate2DMesh_mBE5B1C316CD6007DD6B3F15C529A1A0360857DDA (void);
// 0x0000060A System.Void ChartAndGraph.BarMesh::WriteRect(System.Int32[],System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean)
extern void BarMesh_WriteRect_m922A4073B73DAF909599421FB448627506390C58 (void);
// 0x0000060B System.Void ChartAndGraph.BarMesh::WriteTringle(System.Int32[],System.Int32,System.Int32,System.Int32,System.Int32)
extern void BarMesh_WriteTringle_m55D7DED3B849F00665CF4B6CA2C7F63BD1B3F9F4 (void);
// 0x0000060C UnityEngine.Mesh ChartAndGraph.BarMesh::get_StrechMesh3D()
extern void BarMesh_get_StrechMesh3D_m49A164F2C3AFC2A0C40C46055201F2DAA8E928B5 (void);
// 0x0000060D System.Void ChartAndGraph.BarMesh::Update3DMeshUv(UnityEngine.Mesh,System.Single)
extern void BarMesh_Update3DMeshUv_m2AD274FE0F79FB8AF7AB9C8BBF411B07583D71FB (void);
// 0x0000060E UnityEngine.Mesh ChartAndGraph.BarMesh::Generate3DMesh(System.Single)
extern void BarMesh_Generate3DMesh_m8F8D3DCA18C0959EA6177180E587AA604E43906E (void);
// 0x0000060F System.Void ChartAndGraph.BarMesh::.ctor()
extern void BarMesh__ctor_m7CB9ACD811B612A75CFC5E7D1D4EDDE79B89FD51 (void);
// 0x00000610 System.Boolean ChartAndGraph.BarMeshGenerator::EnsureMeshFilter()
extern void BarMeshGenerator_EnsureMeshFilter_m629BF72229D41AC7648E9ED9D12277E75215D7D0 (void);
// 0x00000611 System.Void ChartAndGraph.BarMeshGenerator::Generate(System.Single,System.Single)
extern void BarMeshGenerator_Generate_mDFB8ED811667D1A94DD6BAB14CAA6B8348E3237D (void);
// 0x00000612 System.Void ChartAndGraph.BarMeshGenerator::Clear()
extern void BarMeshGenerator_Clear_mADD7FAC31910BE76957C7463C20E37314479B347 (void);
// 0x00000613 System.Void ChartAndGraph.BarMeshGenerator::OnDestroy()
extern void BarMeshGenerator_OnDestroy_m4F35D92BC00DAAA5C28AC0C5378E1BC2F5540497 (void);
// 0x00000614 System.Void ChartAndGraph.BarMeshGenerator::.ctor()
extern void BarMeshGenerator__ctor_m5CAC00DC73151B7645CDC8B20D8C6E49419FC524 (void);
// 0x00000615 System.Boolean ChartAndGraph.CanvasBarChart::get_FitToContainer()
extern void CanvasBarChart_get_FitToContainer_mC473D4570F11316ECD3E358249F7CB74CDB4D666 (void);
// 0x00000616 System.Void ChartAndGraph.CanvasBarChart::set_FitToContainer(System.Boolean)
extern void CanvasBarChart_set_FitToContainer_mAD4D6FE5C9151CCDAADC6926333C60C79B3DF15B (void);
// 0x00000617 ChartAndGraph.ChartMagin ChartAndGraph.CanvasBarChart::get_FitMargin()
extern void CanvasBarChart_get_FitMargin_mEFBFCBA33F7F73E0A984CE3CF2854832C108EE0B (void);
// 0x00000618 System.Void ChartAndGraph.CanvasBarChart::set_FitMargin(ChartAndGraph.ChartMagin)
extern void CanvasBarChart_set_FitMargin_m1E18E11B2AD52A7EB05DBEEEA29E742ED0294A12 (void);
// 0x00000619 ChartAndGraph.ChartMagin ChartAndGraph.CanvasBarChart::get_MarginLink()
extern void CanvasBarChart_get_MarginLink_mA6711E253B405A6C71E564D0BC6AFF4D7B159217 (void);
// 0x0000061A UnityEngine.CanvasRenderer ChartAndGraph.CanvasBarChart::get_BarPrefab()
extern void CanvasBarChart_get_BarPrefab_mDCCD475AE30A3839ED927E6884636EBA1B81AC76 (void);
// 0x0000061B System.Void ChartAndGraph.CanvasBarChart::set_BarPrefab(UnityEngine.CanvasRenderer)
extern void CanvasBarChart_set_BarPrefab_m0485790F05BA56B39B55AA084588F620EB581640 (void);
// 0x0000061C System.Boolean ChartAndGraph.CanvasBarChart::get_IsCanvas()
extern void CanvasBarChart_get_IsCanvas_mC83EC1FCAB5BF8102570B36A74FBCC40A309D965 (void);
// 0x0000061D System.Single ChartAndGraph.CanvasBarChart::get_TotalDepthLink()
extern void CanvasBarChart_get_TotalDepthLink_m16C4FE0C4564A0E6F0F9C4AAFD01E7378114C323 (void);
// 0x0000061E System.Single ChartAndGraph.CanvasBarChart::get_AxisSeperation()
extern void CanvasBarChart_get_AxisSeperation_mE89865474F4D2DBF58C890051E072324FC4F57F9 (void);
// 0x0000061F System.Void ChartAndGraph.CanvasBarChart::set_AxisSeperation(System.Single)
extern void CanvasBarChart_set_AxisSeperation_mF416F38FD96938FEF7989CBAF29A1210E649F00E (void);
// 0x00000620 System.Single ChartAndGraph.CanvasBarChart::get_BarSeperation()
extern void CanvasBarChart_get_BarSeperation_mDF4B31469CBFA598C62010A387A2B364A1372093 (void);
// 0x00000621 System.Void ChartAndGraph.CanvasBarChart::set_BarSeperation(System.Single)
extern void CanvasBarChart_set_BarSeperation_m898B3B085E6E6A1E6D4EAA44A14AFEA695A72D71 (void);
// 0x00000622 System.Single ChartAndGraph.CanvasBarChart::get_GroupSeperation()
extern void CanvasBarChart_get_GroupSeperation_m6488F1AE8A394C7D62087B03FB7C3DA179E29EA5 (void);
// 0x00000623 System.Void ChartAndGraph.CanvasBarChart::set_GroupSeperation(System.Single)
extern void CanvasBarChart_set_GroupSeperation_mD9D894198EA857D4C787BC72859EFA4918EB6BE8 (void);
// 0x00000624 System.Boolean ChartAndGraph.CanvasBarChart::get_SupportRealtimeGeneration()
extern void CanvasBarChart_get_SupportRealtimeGeneration_mCF54E2C6CA65CBC4764033CD1DB135D708BD51EB (void);
// 0x00000625 System.Single ChartAndGraph.CanvasBarChart::get_BarSize()
extern void CanvasBarChart_get_BarSize_m3C1DA959EB51037BE8D2CA2EAF4FF01A28807550 (void);
// 0x00000626 System.Void ChartAndGraph.CanvasBarChart::set_BarSize(System.Single)
extern void CanvasBarChart_set_BarSize_m84C620DB58861B71EB3F06E0D4A22DDCBC56040A (void);
// 0x00000627 ChartAndGraph.ChartOrientedSize ChartAndGraph.CanvasBarChart::get_AxisSeperationLink()
extern void CanvasBarChart_get_AxisSeperationLink_m42DB617962F047DE2002073B32341F3EEE1F2971 (void);
// 0x00000628 ChartAndGraph.ChartOrientedSize ChartAndGraph.CanvasBarChart::get_BarSeperationLink()
extern void CanvasBarChart_get_BarSeperationLink_m72CDB88938ED1C78E059A7E3E5C92F47590778DA (void);
// 0x00000629 ChartAndGraph.ChartOrientedSize ChartAndGraph.CanvasBarChart::get_GroupSeperationLink()
extern void CanvasBarChart_get_GroupSeperationLink_mDA7592C5CF2F59A907E597AF4A6228AA92A71ADF (void);
// 0x0000062A ChartAndGraph.ChartOrientedSize ChartAndGraph.CanvasBarChart::get_BarSizeLink()
extern void CanvasBarChart_get_BarSizeLink_mCC456A5FD458A041AF1F4CB074CD9E4E535BB9A3 (void);
// 0x0000062B System.Void ChartAndGraph.CanvasBarChart::SetBarSize(UnityEngine.GameObject,UnityEngine.Vector3,System.Single)
extern void CanvasBarChart_SetBarSize_m8327A929C1172D3B3F980FC7F54ED481EE790BE3 (void);
// 0x0000062C System.Void ChartAndGraph.CanvasBarChart::Update()
extern void CanvasBarChart_Update_m8A882D2E3B493F4B7D2C958434800AD2F888E129 (void);
// 0x0000062D ChartAndGraph.AnyChart/FitType ChartAndGraph.CanvasBarChart::get_BarFitType()
extern void CanvasBarChart_get_BarFitType_mFA58CCDAAB133B31133B78B5851CBB1D0610D563 (void);
// 0x0000062E System.Void ChartAndGraph.CanvasBarChart::set_BarFitType(ChartAndGraph.AnyChart/FitType)
extern void CanvasBarChart_set_BarFitType_mC6ABF38A919CA2B6B438801B261649CCC171F063 (void);
// 0x0000062F ChartAndGraph.AnyChart/FitAlign ChartAndGraph.CanvasBarChart::get_BarFitAlign()
extern void CanvasBarChart_get_BarFitAlign_m0433A5761C5A64074F916DF04D92235BA2054749 (void);
// 0x00000630 System.Void ChartAndGraph.CanvasBarChart::set_BarFitAlign(ChartAndGraph.AnyChart/FitAlign)
extern void CanvasBarChart_set_BarFitAlign_mF0C04A741D47AB24402A716B384991D56AB0A016 (void);
// 0x00000631 ChartAndGraph.AnyChart/FitType ChartAndGraph.CanvasBarChart::get_FitAspectCanvas()
extern void CanvasBarChart_get_FitAspectCanvas_mD7037D51296FD9FAB0EF95FF4988B6C8187C899E (void);
// 0x00000632 ChartAndGraph.AnyChart/FitAlign ChartAndGraph.CanvasBarChart::get_FitAlignCanvas()
extern void CanvasBarChart_get_FitAlignCanvas_mFD5780B5C6BE7F9489589E02B26D9754BB3A8A80 (void);
// 0x00000633 System.Void ChartAndGraph.CanvasBarChart::InternalGenerateChart()
extern void CanvasBarChart_InternalGenerateChart_m48A6475265177A377CD7A1590CE29B2E52E7470E (void);
// 0x00000634 UnityEngine.GameObject ChartAndGraph.CanvasBarChart::get_BarPrefabLink()
extern void CanvasBarChart_get_BarPrefabLink_mD61833CCFE22D2154BF01BC92C948FE393ABED48 (void);
// 0x00000635 System.Void ChartAndGraph.CanvasBarChart::.ctor()
extern void CanvasBarChart__ctor_mCDEE64AC43D04AF9AC3D00D333F8CB713996256E (void);
// 0x00000636 System.Void ChartAndGraph.IBarGenerator::Generate(System.Single,System.Single)
// 0x00000637 System.Void ChartAndGraph.IBarGenerator::Clear()
// 0x00000638 UnityEngine.Camera ChartAndGraph.WorldSpaceBarChart::get_TextCamera()
extern void WorldSpaceBarChart_get_TextCamera_m5200FC02DC4A0288FEB4B7105B526E3B056D851E (void);
// 0x00000639 System.Void ChartAndGraph.WorldSpaceBarChart::set_TextCamera(UnityEngine.Camera)
extern void WorldSpaceBarChart_set_TextCamera_m6F4E2D4FD6E8A4E4AB679F43036C2339689CF245 (void);
// 0x0000063A System.Boolean ChartAndGraph.WorldSpaceBarChart::get_IsCanvas()
extern void WorldSpaceBarChart_get_IsCanvas_m6D843ED098999650995345EABF749421072CF3A3 (void);
// 0x0000063B System.Single ChartAndGraph.WorldSpaceBarChart::get_TextIdleDistance()
extern void WorldSpaceBarChart_get_TextIdleDistance_mE9656FE551464171FC72AC702E64BEA69DB4F2B0 (void);
// 0x0000063C System.Void ChartAndGraph.WorldSpaceBarChart::set_TextIdleDistance(System.Single)
extern void WorldSpaceBarChart_set_TextIdleDistance_m6EDAAD2822FC4DE54AF6D081C251B8E585874710 (void);
// 0x0000063D UnityEngine.GameObject ChartAndGraph.WorldSpaceBarChart::get_BarPrefab()
extern void WorldSpaceBarChart_get_BarPrefab_mD7264BAE5BF76A6D4D7FC0E56C8611F17C4583F5 (void);
// 0x0000063E System.Void ChartAndGraph.WorldSpaceBarChart::set_BarPrefab(UnityEngine.GameObject)
extern void WorldSpaceBarChart_set_BarPrefab_mB94DF7E4979FB740147FCF7A24A166CDFD63DC8A (void);
// 0x0000063F ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_axisSeperation()
extern void WorldSpaceBarChart_get_axisSeperation_m9C90A90D80E986C192DC757B2D8F8D9BD56FCD8D (void);
// 0x00000640 System.Void ChartAndGraph.WorldSpaceBarChart::set_axisSeperation(ChartAndGraph.ChartOrientedSize)
extern void WorldSpaceBarChart_set_axisSeperation_m21CB675BC1FB5364481E1C52A5A9A3395B3D369E (void);
// 0x00000641 ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_BarSeperation()
extern void WorldSpaceBarChart_get_BarSeperation_m11A3B8CED9D4478AE8CF2810979CC6D1B28D3477 (void);
// 0x00000642 System.Void ChartAndGraph.WorldSpaceBarChart::set_BarSeperation(ChartAndGraph.ChartOrientedSize)
extern void WorldSpaceBarChart_set_BarSeperation_mB6CC1112E1E9FB7CB9A36B6F9B2EBF43BF86DFBA (void);
// 0x00000643 ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_GroupSeperation()
extern void WorldSpaceBarChart_get_GroupSeperation_m5D2E9D52827299240E6C99CB5FEDA45A6386EB62 (void);
// 0x00000644 System.Void ChartAndGraph.WorldSpaceBarChart::set_GroupSeperation(ChartAndGraph.ChartOrientedSize)
extern void WorldSpaceBarChart_set_GroupSeperation_mEE558F350DF60770E416ECD6522F9286CD1FB379 (void);
// 0x00000645 ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_BarSize()
extern void WorldSpaceBarChart_get_BarSize_m593850078A534DC91727D2D9B5FFE07BA337E94D (void);
// 0x00000646 System.Void ChartAndGraph.WorldSpaceBarChart::set_BarSize(ChartAndGraph.ChartOrientedSize)
extern void WorldSpaceBarChart_set_BarSize_mD96AF5A93C6D68B18614D454A6D3CD9D51F5C3AB (void);
// 0x00000647 ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_AxisSeperationLink()
extern void WorldSpaceBarChart_get_AxisSeperationLink_m8860AA8F523EB6FFF800596AF302CE2A59B3FAA0 (void);
// 0x00000648 ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_BarSeperationLink()
extern void WorldSpaceBarChart_get_BarSeperationLink_m916DF642C26428A7E05BDA792ED01ACE750DA795 (void);
// 0x00000649 ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_GroupSeperationLink()
extern void WorldSpaceBarChart_get_GroupSeperationLink_mE426E59E772A6513A7380E69899B5DFEF0DEB5E3 (void);
// 0x0000064A ChartAndGraph.ChartOrientedSize ChartAndGraph.WorldSpaceBarChart::get_BarSizeLink()
extern void WorldSpaceBarChart_get_BarSizeLink_mE9E9FF0A87DBED49157A52CF1D3B9CB6E4DCBCD3 (void);
// 0x0000064B UnityEngine.Camera ChartAndGraph.WorldSpaceBarChart::get_TextCameraLink()
extern void WorldSpaceBarChart_get_TextCameraLink_mB2F1FD1DD2493D85EEF79DEC9C0A8A603C6A49DD (void);
// 0x0000064C System.Single ChartAndGraph.WorldSpaceBarChart::get_TextIdleDistanceLink()
extern void WorldSpaceBarChart_get_TextIdleDistanceLink_m75F46ECBBE10E9D03A0E6109A6A521C76EFC8A96 (void);
// 0x0000064D UnityEngine.GameObject ChartAndGraph.WorldSpaceBarChart::get_BarPrefabLink()
extern void WorldSpaceBarChart_get_BarPrefabLink_m4408CA0CA91A0FE6EF6BF7710F7EBB696422B54E (void);
// 0x0000064E System.Void ChartAndGraph.WorldSpaceBarChart::ValidateProperties()
extern void WorldSpaceBarChart_ValidateProperties_mA196DC4D4C01B2045387DFC4F3E0DF137971F85F (void);
// 0x0000064F System.Void ChartAndGraph.WorldSpaceBarChart::.ctor()
extern void WorldSpaceBarChart__ctor_m2F3BFB7BE60C06874B611C431447027BE76B6E09 (void);
// 0x00000650 System.Single ChartAndGraph.CanvasLines::get_Tiling()
extern void CanvasLines_get_Tiling_mE65CEC6E26D8B194309BF763864A0CCE74BA6B17 (void);
// 0x00000651 System.Void ChartAndGraph.CanvasLines::set_Tiling(System.Single)
extern void CanvasLines_set_Tiling_m6083B7A5109191E770793EEDE23CD3C5F2144B42 (void);
// 0x00000652 System.Nullable`1<UnityEngine.Rect> ChartAndGraph.CanvasLines::get_ClipRect()
extern void CanvasLines_get_ClipRect_m474D9C89D9AE388D759504AAB85FBB2E5F6E253C (void);
// 0x00000653 System.Void ChartAndGraph.CanvasLines::set_ClipRect(System.Nullable`1<UnityEngine.Rect>)
extern void CanvasLines_set_ClipRect_m8016CC7BCDC55AA769103BAD3800B83F0EBFE926 (void);
// 0x00000654 System.Void ChartAndGraph.CanvasLines::MakePointRender(System.Single)
extern void CanvasLines_MakePointRender_mA4ADF69677C8E6E5A71C9F3BB45D0740D5BF4ACA (void);
// 0x00000655 System.Void ChartAndGraph.CanvasLines::SetFillZero(System.Single)
extern void CanvasLines_SetFillZero_m6E9D3754BD74B4379193DA9CE5900D1AB8F8FBF5 (void);
// 0x00000656 System.Void ChartAndGraph.CanvasLines::MakeFillRender(UnityEngine.Rect,System.Single,System.Boolean,System.Boolean)
extern void CanvasLines_MakeFillRender_mD3052B8AC5FD302664C83E2D189F548EC8DB1B66 (void);
// 0x00000657 System.Void ChartAndGraph.CanvasLines::.ctor()
extern void CanvasLines__ctor_m89CCDF01AD700DA84B8168806479C92DB342536F (void);
// 0x00000658 System.Void ChartAndGraph.CanvasLines::FindBoundingValues()
extern void CanvasLines_FindBoundingValues_mCA4E7C9399C94854EB978F2E995E674256872150 (void);
// 0x00000659 System.Void ChartAndGraph.CanvasLines::ModifyLines(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void CanvasLines_ModifyLines_mE282752721041452BE24199EB3A921D53379D212 (void);
// 0x0000065A System.Boolean ChartAndGraph.CanvasLines::get_EnableOptimization()
extern void CanvasLines_get_EnableOptimization_m202B20B5E9F00E4A1213D5ECCB945268B1593457 (void);
// 0x0000065B System.Void ChartAndGraph.CanvasLines::set_EnableOptimization(System.Boolean)
extern void CanvasLines_set_EnableOptimization_mCAC2A3AE52E5A5B3BF4C886BD10BF48960BA65FF (void);
// 0x0000065C System.Void ChartAndGraph.CanvasLines::SetLines(System.Collections.Generic.List`1<ChartAndGraph.CanvasLines/LineSegement>)
extern void CanvasLines_SetLines_m5AA7904B90F13C41387A172DE1FEE7E419F2C1EA (void);
// 0x0000065D System.Void ChartAndGraph.CanvasLines::UpdateMaterial()
extern void CanvasLines_UpdateMaterial_m66877253FD2A932E344F35206D7BD0496CE232A9 (void);
// 0x0000065E System.Void ChartAndGraph.CanvasLines::GetSide(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Single,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void CanvasLines_GetSide_m69A7F84C9228E81FBB63AA7A51F31A85BD3AED69 (void);
// 0x0000065F System.Void ChartAndGraph.CanvasLines::OnDestroy()
extern void CanvasLines_OnDestroy_m462631C63830C9F45DA9F55EF7AEBA4936881B5C (void);
// 0x00000660 System.Void ChartAndGraph.CanvasLines::OnDisable()
extern void CanvasLines_OnDisable_m0AEABF75331E5CA504364B353973AA5C8958700C (void);
// 0x00000661 UnityEngine.Material ChartAndGraph.CanvasLines::get_material()
extern void CanvasLines_get_material_mC498CFA5EA6C95524B9A93FDAC6190E679FF0567 (void);
// 0x00000662 System.Void ChartAndGraph.CanvasLines::set_material(UnityEngine.Material)
extern void CanvasLines_set_material_m243A75E3A4DDEBADF37A38BA7D042B42E0AED744 (void);
// 0x00000663 UnityEngine.Vector2 ChartAndGraph.CanvasLines::get_Min()
extern void CanvasLines_get_Min_m96A0B33AE7A51813596DF375DFA91121A5470565 (void);
// 0x00000664 UnityEngine.Vector2 ChartAndGraph.CanvasLines::get_Max()
extern void CanvasLines_get_Max_mA7A2DD721A200F63E8C9983C93041B6D33CEB893 (void);
// 0x00000665 System.Single ChartAndGraph.CanvasLines::get_MouseInThreshold()
extern void CanvasLines_get_MouseInThreshold_mD4DFF86633EFBCEA488C157198191ECD387D9A69 (void);
// 0x00000666 System.Void ChartAndGraph.CanvasLines::SetUpHoverObject(ChartAndGraph.ChartItemEffect,System.Int32,System.Int32,System.Object)
extern void CanvasLines_SetUpHoverObject_mFF285C1EC3299EBD22EA30CF6D26288EF2519848 (void);
// 0x00000667 System.Void ChartAndGraph.CanvasLines::Pick(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
extern void CanvasLines_Pick_m490C1779FDC2E791149FE75D1660B86C75D6A4B2 (void);
// 0x00000668 System.Void ChartAndGraph.CanvasLines::Update()
extern void CanvasLines_Update_m7F1E218E058908F594CC79F1EA2F824FB0351CC2 (void);
// 0x00000669 System.Void ChartAndGraph.CanvasLines::ProcesssPoint(UnityEngine.Vector4&,System.Single&)
extern void CanvasLines_ProcesssPoint_mF6D3700AD0C5E527CF7815E0963167C4E15282BE (void);
// 0x0000066A System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines::getDotVeritces()
extern void CanvasLines_getDotVeritces_m57F8AD391BB4EA247CE286F9B61901B445F0B5C5 (void);
// 0x0000066B UnityEngine.Vector2 ChartAndGraph.CanvasLines::TransformUv(UnityEngine.Vector2)
extern void CanvasLines_TransformUv_m544A7D54BDE58042C42E9D7F1F1056228679EE92 (void);
// 0x0000066C System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines::getFillVeritces()
extern void CanvasLines_getFillVeritces_mEBED888394C7BD35F69B03E2D32028DC50B5D971 (void);
// 0x0000066D System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines::getLineVertices()
extern void CanvasLines_getLineVertices_m3EE775FCB0F13DC4E61DE3FDE73ED596A0977B66 (void);
// 0x0000066E System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines::getVerices()
extern void CanvasLines_getVerices_m34D66F851E2599DBF9EB9E2153C72C6C3F7A1BB5 (void);
// 0x0000066F System.Void ChartAndGraph.CanvasLines::WriteTo(System.Collections.Generic.List`1<T>,System.Int32,T)
// 0x00000670 System.Void ChartAndGraph.CanvasLines::UpdateGeometry()
extern void CanvasLines_UpdateGeometry_mDACDCC2CC9DDC6CAE59C250567C83B3760DD8DD0 (void);
// 0x00000671 System.Void ChartAndGraph.CanvasLines::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void CanvasLines_OnPopulateMesh_m72917C3F9BA452191C4B0E37926403772DDB7D67 (void);
// 0x00000672 System.Void ChartAndGraph.CanvasLines::PickLine(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
extern void CanvasLines_PickLine_m78C8710B19E65A2E0E4FF3BEC40FDF2CD37FDB96 (void);
// 0x00000673 System.Void ChartAndGraph.CanvasLines::PickDot(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
extern void CanvasLines_PickDot_mC0DD68BC9446F2765E836456D3CD0111ED4818FE (void);
// 0x00000674 System.Void ChartAndGraph.CanvasLines::TrimItem(System.Single,System.Single,System.Single,System.Single,System.Boolean,System.Boolean,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern void CanvasLines_TrimItem_m788921BBDF2A54AE044ED03D445132EAB6BD5B9D (void);
// 0x00000675 System.Void ChartAndGraph.CanvasLines::TrimLine(UnityEngine.Rect,UnityEngine.Vector2&,UnityEngine.Vector2&)
extern void CanvasLines_TrimLine_m3EE97578F68CCA48E5C8C33857279D254A7E71C6 (void);
// 0x00000676 System.Void ChartAndGraph.CanvasLines/Line::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Boolean,System.Boolean)
extern void Line__ctor_m4389E71B32F966E8718C9FAFD8DC7C9F6E2110C7 (void);
// 0x00000677 System.Boolean ChartAndGraph.CanvasLines/Line::get_Degenerated()
extern void Line_get_Degenerated_mC903C588921C51C16C8A9EDE11AACCF1A4C759B6 (void);
// 0x00000678 System.Void ChartAndGraph.CanvasLines/Line::set_Degenerated(System.Boolean)
extern void Line_set_Degenerated_m378A5401A9BAA385E72BC9885DC3B53F1414B451 (void);
// 0x00000679 UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_P1()
extern void Line_get_P1_mC750288C96B6CE408C61A85DF57B29B463D10C0D (void);
// 0x0000067A System.Void ChartAndGraph.CanvasLines/Line::set_P1(UnityEngine.Vector3)
extern void Line_set_P1_mC38DFAC465CCB453B3B4AB7E97A447590CC89965 (void);
// 0x0000067B UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_P2()
extern void Line_get_P2_mB960A6BEF78A9B952DAE1A299D67CFC2012E2A1F (void);
// 0x0000067C System.Void ChartAndGraph.CanvasLines/Line::set_P2(UnityEngine.Vector3)
extern void Line_set_P2_m160F2F164EF038D1F32214DE28204AE9683D33EA (void);
// 0x0000067D UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_P3()
extern void Line_get_P3_m06A9A176422365C1575296A1F25FD5DEF4D99DDD (void);
// 0x0000067E System.Void ChartAndGraph.CanvasLines/Line::set_P3(UnityEngine.Vector3)
extern void Line_set_P3_mCAF01F4FED931AA69E7246B0FCBB544D303C7B70 (void);
// 0x0000067F UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_P4()
extern void Line_get_P4_m8283F3E3C3EFF22F76BDAE1934F53F5175BD2616 (void);
// 0x00000680 System.Void ChartAndGraph.CanvasLines/Line::set_P4(UnityEngine.Vector3)
extern void Line_set_P4_mC6DD8CC9AB985D8FB738267F457D4C34250B9E6D (void);
// 0x00000681 UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_From()
extern void Line_get_From_mE45AE1DDC8A9D9EBF61227F17C4266A2F2DE6E9B (void);
// 0x00000682 System.Void ChartAndGraph.CanvasLines/Line::set_From(UnityEngine.Vector3)
extern void Line_set_From_m592C4AE6BD4846DFCC12721665EE1B58E1D47F04 (void);
// 0x00000683 UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_To()
extern void Line_get_To_mD8FAC6076802B34205395DF694BDCE8314F1F2DA (void);
// 0x00000684 System.Void ChartAndGraph.CanvasLines/Line::set_To(UnityEngine.Vector3)
extern void Line_set_To_mB09CD554B116268CF79C72323AE421A1C6C0B1F3 (void);
// 0x00000685 UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_Dir()
extern void Line_get_Dir_mCD8563E867388D16CF2A859711F1868D2A1E77D0 (void);
// 0x00000686 System.Void ChartAndGraph.CanvasLines/Line::set_Dir(UnityEngine.Vector3)
extern void Line_set_Dir_m8099FE4EEA13DB6F851843C3258BC6931DE82B85 (void);
// 0x00000687 System.Single ChartAndGraph.CanvasLines/Line::get_Mag()
extern void Line_get_Mag_mFBC4B05934E2168B53EA8A64864446F3CE366AF5 (void);
// 0x00000688 System.Void ChartAndGraph.CanvasLines/Line::set_Mag(System.Single)
extern void Line_set_Mag_m8DF404321EC5B783086BAAB927F520F0B4FDE1EB (void);
// 0x00000689 UnityEngine.Vector3 ChartAndGraph.CanvasLines/Line::get_Normal()
extern void Line_get_Normal_m8F500F8704DEEF72A8F69A20608C2ACEE316146A (void);
// 0x0000068A System.Void ChartAndGraph.CanvasLines/Line::set_Normal(UnityEngine.Vector3)
extern void Line_set_Normal_mD0FB285540CA993D6648EA0A2ECED90968AEF099 (void);
// 0x0000068B System.Void ChartAndGraph.CanvasLines/LineSegement::.ctor(System.Collections.Generic.IList`1<UnityEngine.Vector3>)
extern void LineSegement__ctor_mC8F6BA6EEDD0553398CA7F326E32A932B778D0B5 (void);
// 0x0000068C System.Void ChartAndGraph.CanvasLines/LineSegement::.ctor(System.Collections.Generic.IList`1<UnityEngine.Vector4>)
extern void LineSegement__ctor_m59BA7FA62AB02CA4B1F910059229A5935542DACC (void);
// 0x0000068D System.Void ChartAndGraph.CanvasLines/LineSegement::ModifiyLines(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void LineSegement_ModifiyLines_m0A23D098CD43B5BD907B7237B82DC11B911375D7 (void);
// 0x0000068E System.Int32 ChartAndGraph.CanvasLines/LineSegement::get_PointCount()
extern void LineSegement_get_PointCount_mA0AED9D32FE2F9587749EBB3C9748391F2709BE5 (void);
// 0x0000068F System.Int32 ChartAndGraph.CanvasLines/LineSegement::get_LineCount()
extern void LineSegement_get_LineCount_m45BE633409E19733EB4262805AECEB0FF0145434 (void);
// 0x00000690 UnityEngine.Vector4 ChartAndGraph.CanvasLines/LineSegement::getPoint(System.Int32)
extern void LineSegement_getPoint_mC1566C4EDBA4C7107CDBD82ADA709D19E3AA426B (void);
// 0x00000691 System.Void ChartAndGraph.CanvasLines/LineSegement::GetLine(System.Int32,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void LineSegement_GetLine_m402749446A5EAB355A71D17F76B09D126125E83C (void);
// 0x00000692 ChartAndGraph.CanvasLines/Line ChartAndGraph.CanvasLines/LineSegement::GetLine(System.Int32,System.Single,System.Boolean,System.Boolean)
extern void LineSegement_GetLine_m1DB5640B2EE99E6C5D40D2E15476285F0BAF18BB (void);
// 0x00000693 System.Double ChartAndGraph.CanvasLines/LineSegement::GetLineMag(System.Int32)
extern void LineSegement_GetLineMag_mF41C5C1F747753C72426DA8FEC16959FC0FC7AA1 (void);
// 0x00000694 System.Void ChartAndGraph.CanvasLines/LineSegement/<>c::.cctor()
extern void U3CU3Ec__cctor_m88C99D6E184D80254FF1B1EC0FD438D0C1A6D275 (void);
// 0x00000695 System.Void ChartAndGraph.CanvasLines/LineSegement/<>c::.ctor()
extern void U3CU3Ec__ctor_m80401BB46A3F46478F980ED78A74C462464E0242 (void);
// 0x00000696 UnityEngine.Vector4 ChartAndGraph.CanvasLines/LineSegement/<>c::<.ctor>b__1_0(UnityEngine.Vector3)
extern void U3CU3Ec_U3C_ctorU3Eb__1_0_m5E1E42936DDD29BEE50C30FBF4AFD136C63D3A63 (void);
// 0x00000697 System.Void ChartAndGraph.CanvasLines/<getDotVeritces>d__54::.ctor(System.Int32)
extern void U3CgetDotVeritcesU3Ed__54__ctor_m4DB72EA868575928A2B878DB003FC69EC843CB41 (void);
// 0x00000698 System.Void ChartAndGraph.CanvasLines/<getDotVeritces>d__54::System.IDisposable.Dispose()
extern void U3CgetDotVeritcesU3Ed__54_System_IDisposable_Dispose_mD23F1E6AA65C6A057B3CB754453E5EF805A6D1C4 (void);
// 0x00000699 System.Boolean ChartAndGraph.CanvasLines/<getDotVeritces>d__54::MoveNext()
extern void U3CgetDotVeritcesU3Ed__54_MoveNext_m21D2E118869BC25ED2257E56C5DD7837FB02EFF2 (void);
// 0x0000069A UnityEngine.UIVertex ChartAndGraph.CanvasLines/<getDotVeritces>d__54::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mDE5152173FE2DDBBBEFBA74FF5D1003DB57329A5 (void);
// 0x0000069B System.Void ChartAndGraph.CanvasLines/<getDotVeritces>d__54::System.Collections.IEnumerator.Reset()
extern void U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_Reset_mB94C31961CCC9ED9E0C43BDA9EDAB2C8AEC8168E (void);
// 0x0000069C System.Object ChartAndGraph.CanvasLines/<getDotVeritces>d__54::System.Collections.IEnumerator.get_Current()
extern void U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_get_Current_m5EDAAE023B18488FB09268F34195242C38E00398 (void);
// 0x0000069D System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines/<getDotVeritces>d__54::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mDD7964939474EC93D2FE78BE7CC49AFBA792B7FE (void);
// 0x0000069E System.Collections.IEnumerator ChartAndGraph.CanvasLines/<getDotVeritces>d__54::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerable_GetEnumerator_mDD309DD0E06A30244A07398B3954C6D027892939 (void);
// 0x0000069F System.Void ChartAndGraph.CanvasLines/<getFillVeritces>d__56::.ctor(System.Int32)
extern void U3CgetFillVeritcesU3Ed__56__ctor_m84E7F14E607A4946C8501AB381A61E9073030C41 (void);
// 0x000006A0 System.Void ChartAndGraph.CanvasLines/<getFillVeritces>d__56::System.IDisposable.Dispose()
extern void U3CgetFillVeritcesU3Ed__56_System_IDisposable_Dispose_m24807AAE5C8A054491097465175CFE35CD1DEF66 (void);
// 0x000006A1 System.Boolean ChartAndGraph.CanvasLines/<getFillVeritces>d__56::MoveNext()
extern void U3CgetFillVeritcesU3Ed__56_MoveNext_mD455660F194F97BCC07CCE43B42A08F5BA7A28F4 (void);
// 0x000006A2 UnityEngine.UIVertex ChartAndGraph.CanvasLines/<getFillVeritces>d__56::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mD6B1F95D7EB23006D2F8D4AB387FDD8DBB18F306 (void);
// 0x000006A3 System.Void ChartAndGraph.CanvasLines/<getFillVeritces>d__56::System.Collections.IEnumerator.Reset()
extern void U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_Reset_mB82B5598EA4CE0F46906571B4E8D9DA77BC3B6FF (void);
// 0x000006A4 System.Object ChartAndGraph.CanvasLines/<getFillVeritces>d__56::System.Collections.IEnumerator.get_Current()
extern void U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_get_Current_m8EA14AB9919BD3DD944A510B246A8C9AD1E044BB (void);
// 0x000006A5 System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines/<getFillVeritces>d__56::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD742CD8DCFAD0905516F7BD5DC1F0EA2BC14A306 (void);
// 0x000006A6 System.Collections.IEnumerator ChartAndGraph.CanvasLines/<getFillVeritces>d__56::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerable_GetEnumerator_m16CFCBA5318010625AA9C47463B1FDECB73EF4FF (void);
// 0x000006A7 System.Void ChartAndGraph.CanvasLines/<getLineVertices>d__57::.ctor(System.Int32)
extern void U3CgetLineVerticesU3Ed__57__ctor_mEE477D5EAAE9411DFD0492352D6FB42078AC1569 (void);
// 0x000006A8 System.Void ChartAndGraph.CanvasLines/<getLineVertices>d__57::System.IDisposable.Dispose()
extern void U3CgetLineVerticesU3Ed__57_System_IDisposable_Dispose_mC8072E3A4ABA4613032B8D7E71DB91558C416482 (void);
// 0x000006A9 System.Boolean ChartAndGraph.CanvasLines/<getLineVertices>d__57::MoveNext()
extern void U3CgetLineVerticesU3Ed__57_MoveNext_m795B9D9B4534F2CBCE238BE166D83C3E8EFBBBFC (void);
// 0x000006AA UnityEngine.UIVertex ChartAndGraph.CanvasLines/<getLineVertices>d__57::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m32AE92CAB16E5053DB5BBCC59C1685CCFF523DE8 (void);
// 0x000006AB System.Void ChartAndGraph.CanvasLines/<getLineVertices>d__57::System.Collections.IEnumerator.Reset()
extern void U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_Reset_mEFF718EC377A4A2E4429F5E1E750125C21D55DB0 (void);
// 0x000006AC System.Object ChartAndGraph.CanvasLines/<getLineVertices>d__57::System.Collections.IEnumerator.get_Current()
extern void U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_get_Current_m6323FEDAA1579D420AE2EA6FB069041B77EEE869 (void);
// 0x000006AD System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLines/<getLineVertices>d__57::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m314F4C290F52F681EB509E6551875D62C947F5F4 (void);
// 0x000006AE System.Collections.IEnumerator ChartAndGraph.CanvasLines/<getLineVertices>d__57::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerable_GetEnumerator_m8D920BB8D684EEE193B70580501FDEF370F80984 (void);
// 0x000006AF System.Void ChartAndGraph.CanvasLinesHover::Init(System.Single)
extern void CanvasLinesHover_Init_mB3DC6FE954A3F49525416BFDD22405F8F818BE7A (void);
// 0x000006B0 System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLinesHover::getVerices()
extern void CanvasLinesHover_getVerices_m7234E00BE8E15C9BEEC7F63445BE89EB20DB7229 (void);
// 0x000006B1 System.Void ChartAndGraph.CanvasLinesHover::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void CanvasLinesHover_OnPopulateMesh_mF099508F65972C028394C082CA0A1E9EB6D7D10C (void);
// 0x000006B2 System.Void ChartAndGraph.CanvasLinesHover::.ctor()
extern void CanvasLinesHover__ctor_m1C3FCA3758D72D6CAB15088BE02E48978B8BA9EA (void);
// 0x000006B3 System.Void ChartAndGraph.CanvasLinesHover/<getVerices>d__3::.ctor(System.Int32)
extern void U3CgetVericesU3Ed__3__ctor_mFB4CB89B833F447622410D7F7A7CCAD4C98D70BF (void);
// 0x000006B4 System.Void ChartAndGraph.CanvasLinesHover/<getVerices>d__3::System.IDisposable.Dispose()
extern void U3CgetVericesU3Ed__3_System_IDisposable_Dispose_m746EFF4FE9C1FFE4D6A7070FB0CA043639F68F45 (void);
// 0x000006B5 System.Boolean ChartAndGraph.CanvasLinesHover/<getVerices>d__3::MoveNext()
extern void U3CgetVericesU3Ed__3_MoveNext_m89DA0703213B2918426B9C76BD3EE121A7D2B6DC (void);
// 0x000006B6 UnityEngine.UIVertex ChartAndGraph.CanvasLinesHover/<getVerices>d__3::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mF499DCABE17E18C34CA602E520367789C8BB8AD9 (void);
// 0x000006B7 System.Void ChartAndGraph.CanvasLinesHover/<getVerices>d__3::System.Collections.IEnumerator.Reset()
extern void U3CgetVericesU3Ed__3_System_Collections_IEnumerator_Reset_m5F6B721252ABAB165C6649E7E398EE50EDE376B7 (void);
// 0x000006B8 System.Object ChartAndGraph.CanvasLinesHover/<getVerices>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CgetVericesU3Ed__3_System_Collections_IEnumerator_get_Current_m17C9A5DE1F6758BF4F866CC0DD45A53E9E6D33D1 (void);
// 0x000006B9 System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.CanvasLinesHover/<getVerices>d__3::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD5851E834D32366D321671E68BFCB0CA31D1DE41 (void);
// 0x000006BA System.Collections.IEnumerator ChartAndGraph.CanvasLinesHover/<getVerices>d__3::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetVericesU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m76999D741D77F8EB58EFC799D0DBF597C38D6CFA (void);
// 0x000006BB System.Int32 ChartAndGraph.EventHandlingGraphic::get_refrenceIndex()
extern void EventHandlingGraphic_get_refrenceIndex_m74CA55213DF53D00162E676956F11D1311D84DB3 (void);
// 0x000006BC System.Void ChartAndGraph.EventHandlingGraphic::set_refrenceIndex(System.Int32)
extern void EventHandlingGraphic_set_refrenceIndex_mA31A5548EE2B4B2D2A76FBB107933ADF89B0EA09 (void);
// 0x000006BD System.Void ChartAndGraph.EventHandlingGraphic::SetRefrenceIndex(System.Int32)
extern void EventHandlingGraphic_SetRefrenceIndex_mCB3F59C440167FD68904130C54F91041F5C5E14E (void);
// 0x000006BE System.Single ChartAndGraph.EventHandlingGraphic::get_Sensitivity()
extern void EventHandlingGraphic_get_Sensitivity_m6066244F53D9D32CF6EBC441572A9CC5F0300122 (void);
// 0x000006BF System.Void ChartAndGraph.EventHandlingGraphic::ClearEvents()
extern void EventHandlingGraphic_ClearEvents_m88B6238533A9D5E3859B8A66DC83F05CBE02C06C (void);
// 0x000006C0 UnityEngine.Vector2 ChartAndGraph.EventHandlingGraphic::get_Min()
// 0x000006C1 UnityEngine.Vector2 ChartAndGraph.EventHandlingGraphic::get_Max()
// 0x000006C2 System.Void ChartAndGraph.EventHandlingGraphic::add_Hover(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void EventHandlingGraphic_add_Hover_m975682AE4DF3A55C09E90918447520BC78FB4C84 (void);
// 0x000006C3 System.Void ChartAndGraph.EventHandlingGraphic::remove_Hover(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void EventHandlingGraphic_remove_Hover_m83C03EF5E1E66A44170A92CFD02162EDA9F07CE7 (void);
// 0x000006C4 System.Void ChartAndGraph.EventHandlingGraphic::add_Click(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void EventHandlingGraphic_add_Click_m158BAA2833986B66A6D82E45FC0D81BD82F65289 (void);
// 0x000006C5 System.Void ChartAndGraph.EventHandlingGraphic::remove_Click(ChartAndGraph.EventHandlingGraphic/GraphicEvent)
extern void EventHandlingGraphic_remove_Click_m46285AF60CA8F9CC618C99DD50E77535486D011B (void);
// 0x000006C6 System.Void ChartAndGraph.EventHandlingGraphic::add_Leave(System.Action)
extern void EventHandlingGraphic_add_Leave_m3CEF09D689312D3288058607971942DA3A7E280E (void);
// 0x000006C7 System.Void ChartAndGraph.EventHandlingGraphic::remove_Leave(System.Action)
extern void EventHandlingGraphic_remove_Leave_mA0C44F388334C72624433637A97EE482B1A7D331 (void);
// 0x000006C8 System.Void ChartAndGraph.EventHandlingGraphic::SetViewRect(UnityEngine.Rect,UnityEngine.Rect)
extern void EventHandlingGraphic_SetViewRect_mC9080BA124E60101AC5FAB7AAC26BCB6127500E2 (void);
// 0x000006C9 System.Void ChartAndGraph.EventHandlingGraphic::HoverTransform(UnityEngine.Transform)
extern void EventHandlingGraphic_HoverTransform_m223EF0281BF374829863DD221C2625A3A4880AB5 (void);
// 0x000006CA System.Void ChartAndGraph.EventHandlingGraphic::SetHoverPrefab(ChartAndGraph.ChartItemEffect)
extern void EventHandlingGraphic_SetHoverPrefab_m789FB1014508B6CBCA14F7A76800717C237FF7BF (void);
// 0x000006CB System.Void ChartAndGraph.EventHandlingGraphic::RefreshInputs()
extern void EventHandlingGraphic_RefreshInputs_mE124118E0B26059766ACF7EECFB3A41540BC8645 (void);
// 0x000006CC System.Void ChartAndGraph.EventHandlingGraphic::Update()
extern void EventHandlingGraphic_Update_mA9ED966C414D2C31BF5BD0FFF406E366550C0F0C (void);
// 0x000006CD System.Void ChartAndGraph.EventHandlingGraphic::SetUpHoverObject(ChartAndGraph.ChartItemEffect,System.Int32,System.Int32,System.Object)
// 0x000006CE System.Single ChartAndGraph.EventHandlingGraphic::get_MouseInThreshold()
// 0x000006CF System.Void ChartAndGraph.EventHandlingGraphic::Pick(UnityEngine.Vector3,System.Int32&,System.Int32&,System.Object&)
// 0x000006D0 System.Void ChartAndGraph.EventHandlingGraphic::SetUpAllHoverObjects()
extern void EventHandlingGraphic_SetUpAllHoverObjects_m85F4FED218C93940F5563751C3AE2B7FE0A61480 (void);
// 0x000006D1 System.Void ChartAndGraph.EventHandlingGraphic::SetupHoverObjectToRect(ChartAndGraph.ChartItemEffect,System.Int32,System.Int32,UnityEngine.Rect)
extern void EventHandlingGraphic_SetupHoverObjectToRect_m131EF02087027565A5C9ACBC3A53894430653A4F (void);
// 0x000006D2 System.Void ChartAndGraph.EventHandlingGraphic::TriggerOut(ChartAndGraph.ChartItemEffect)
extern void EventHandlingGraphic_TriggerOut_m61D01C679776385C10027273CCAB92B61D6252D3 (void);
// 0x000006D3 System.Void ChartAndGraph.EventHandlingGraphic::TriggerIn(ChartAndGraph.ChartItemEffect)
extern void EventHandlingGraphic_TriggerIn_mF097DF471EA28EFBE75EC88B724B06A1AF8808E9 (void);
// 0x000006D4 ChartAndGraph.ChartItemEffect ChartAndGraph.EventHandlingGraphic::LockHoverObject(System.Int32,System.Int32,System.Object)
extern void EventHandlingGraphic_LockHoverObject_m375CE14E249E450F1803AA48E7A228F2374816DC (void);
// 0x000006D5 System.Void ChartAndGraph.EventHandlingGraphic::Effect_Deactivate(ChartAndGraph.ChartItemEffect)
extern void EventHandlingGraphic_Effect_Deactivate_mB36F12B889A87E511A50E0F28F398BE8CC117931 (void);
// 0x000006D6 System.Void ChartAndGraph.EventHandlingGraphic::SetUpHoverObject(ChartAndGraph.ChartItemEffect)
extern void EventHandlingGraphic_SetUpHoverObject_mF075E2B2F891460E978C4ADFA4773E96E1203D6C (void);
// 0x000006D7 System.Void ChartAndGraph.EventHandlingGraphic::DoMouse(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void EventHandlingGraphic_DoMouse_m0032911C7667DDD91C8F50DFA9ADFE7CF8D7C232 (void);
// 0x000006D8 System.Boolean ChartAndGraph.EventHandlingGraphic::HandleMouseMove(System.Boolean)
extern void EventHandlingGraphic_HandleMouseMove_m6AB9C576775784D8F64287D5113810858667FB64 (void);
// 0x000006D9 System.Void ChartAndGraph.EventHandlingGraphic::HandleMouseDown()
extern void EventHandlingGraphic_HandleMouseDown_mF4F4AA1559162FBA57E80455A5A46B802BDC3C15 (void);
// 0x000006DA System.Void ChartAndGraph.EventHandlingGraphic::.ctor()
extern void EventHandlingGraphic__ctor_mB2BB5F56FAFC4CD2B0E86EB5DD1EA280ED29DA43 (void);
// 0x000006DB System.Void ChartAndGraph.EventHandlingGraphic/GraphicEvent::.ctor(System.Object,System.IntPtr)
extern void GraphicEvent__ctor_m0DD5CD0FC7F5085311D1B6E1E9C52CC71D79B0AC (void);
// 0x000006DC System.Void ChartAndGraph.EventHandlingGraphic/GraphicEvent::Invoke(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void GraphicEvent_Invoke_m88B32CF8C4875117F435ADD8A2D2ED06566E3F18 (void);
// 0x000006DD System.IAsyncResult ChartAndGraph.EventHandlingGraphic/GraphicEvent::BeginInvoke(System.Int32,System.Int32,System.Object,UnityEngine.Vector2,System.AsyncCallback,System.Object)
extern void GraphicEvent_BeginInvoke_m45AE99ACD97CFD6CAE90D3EA4BB5C24A4ED3E007 (void);
// 0x000006DE System.Void ChartAndGraph.EventHandlingGraphic/GraphicEvent::EndInvoke(System.IAsyncResult)
extern void GraphicEvent_EndInvoke_m2BCCB8F1FC7075EF041FBBA21E9DF022DC1C60FC (void);
// 0x000006DF System.Void ChartAndGraph.SensitivityControl::.ctor()
extern void SensitivityControl__ctor_m4B727A7BE35E69CD262AB685BC203868F6CAC79C (void);
// 0x000006E0 ChartAndGraph.ChartAdancedSettings ChartAndGraph.ChartAdancedSettings::get_Instance()
extern void ChartAdancedSettings_get_Instance_m93A3FA4A26C18DCF5312E05C6AFA63B13E8BE20D (void);
// 0x000006E1 System.String ChartAndGraph.ChartAdancedSettings::InnerFormat(System.String,System.Double)
extern void ChartAdancedSettings_InnerFormat_m6A96887E59FC7AFB11B6A2ACA80A1895AA1B2D4B (void);
// 0x000006E2 System.String ChartAndGraph.ChartAdancedSettings::getFormat(System.Int32)
extern void ChartAdancedSettings_getFormat_m8F8F5D21FEDB1FC3E5EFE1E97D0B9CAA1914ED83 (void);
// 0x000006E3 System.String ChartAndGraph.ChartAdancedSettings::FormatFractionDigits(System.Int32,System.Double,System.String)
extern void ChartAdancedSettings_FormatFractionDigits_m154907F18D4D82F326D0406CC94CBD1739BFBA92 (void);
// 0x000006E4 System.Void ChartAndGraph.ChartAdancedSettings::.ctor()
extern void ChartAdancedSettings__ctor_mC0C2F645CD1F8D6FA6F756BA29F87E01AB8174A6 (void);
// 0x000006E5 System.Void ChartAndGraph.ChartAdancedSettings::.cctor()
extern void ChartAdancedSettings__cctor_m2D3F8CE33C5D3B6D31D71D5ADC2F49343FA2C3F8 (void);
// 0x000006E6 System.Void ChartAndGraph.ChartCommon::.cctor()
extern void ChartCommon__cctor_mAE6B2F155729FFCADA7907B9E157CF073EF66BC2 (void);
// 0x000006E7 System.Single ChartAndGraph.ChartCommon::SmoothLerp(System.Single,System.Single,System.Single)
extern void ChartCommon_SmoothLerp_m3DB7B51FBA7A1FCD82AAE0DFC0338D1B579543EA (void);
// 0x000006E8 UnityEngine.GameObject ChartAndGraph.ChartCommon::CreateCanvasChartItem()
extern void ChartCommon_CreateCanvasChartItem_m6367E70A97FDD247B08C0BBA9B3F85B677C4DB8C (void);
// 0x000006E9 System.Double ChartAndGraph.ChartCommon::Max(System.Nullable`1<System.Double>,System.Double)
extern void ChartCommon_Max_mC3FE068CC7153142E6690CA50A4DE95B023D84A2 (void);
// 0x000006EA System.Double ChartAndGraph.ChartCommon::Min(System.Nullable`1<System.Double>,System.Double)
extern void ChartCommon_Min_mE829F2ED8AA2DF44BAB0B467BA0997B4EAE0586E (void);
// 0x000006EB System.Double ChartAndGraph.ChartCommon::Clamp(System.Double)
extern void ChartCommon_Clamp_m7FA20092D4B7BDD2C456996EE4D2C9429EB27041 (void);
// 0x000006EC UnityEngine.GameObject ChartAndGraph.ChartCommon::CreateChartItem()
extern void ChartCommon_CreateChartItem_mBBD9BB97FF1448ED4C8F90E325CB1E63D00AFA3D (void);
// 0x000006ED System.Double ChartAndGraph.ChartCommon::normalizeInRangeX(System.Double,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void ChartCommon_normalizeInRangeX_m5A9FC8DB93377F29AA36EBF2FB995A0AE8A500BB (void);
// 0x000006EE System.Double ChartAndGraph.ChartCommon::normalizeInRangeY(System.Double,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void ChartCommon_normalizeInRangeY_m35A5D1F57761F02B18D4B7FE6C18CC4F3DA3DD60 (void);
// 0x000006EF System.Double ChartAndGraph.ChartCommon::normalizeInRange(System.Double,System.Double,System.Double)
extern void ChartCommon_normalizeInRange_mDA75C1F9FBFE69D76C41CFAD47A4B24A490229B0 (void);
// 0x000006F0 System.Double ChartAndGraph.ChartCommon::interpolateInRectX(UnityEngine.Rect,System.Double)
extern void ChartCommon_interpolateInRectX_m8AFD422B328D5E146F36497E790DE89313328F3C (void);
// 0x000006F1 System.Double ChartAndGraph.ChartCommon::interpolateInRectY(UnityEngine.Rect,System.Double)
extern void ChartCommon_interpolateInRectY_m31929A422085887AF05040282AD0AC4C9DFF64D4 (void);
// 0x000006F2 UnityEngine.Rect ChartAndGraph.ChartCommon::RectFromCenter(System.Single,System.Single,System.Single,System.Single)
extern void ChartCommon_RectFromCenter_m33AF4D2E3F82303914D512DD7D1B6B07843A8479 (void);
// 0x000006F3 ChartAndGraph.DoubleVector4 ChartAndGraph.ChartCommon::interpolateInRect(UnityEngine.Rect,ChartAndGraph.DoubleVector3)
extern void ChartCommon_interpolateInRect_m88DFBE7CC8DACF7FA54E788BBEBF182D29252364 (void);
// 0x000006F4 System.Void ChartAndGraph.ChartCommon::HideObjectEditor(UnityEngine.GameObject,System.Boolean)
extern void ChartCommon_HideObjectEditor_m5F1CE5AD96DBB15EA841A0E42D09BAE112D34E58 (void);
// 0x000006F5 System.Void ChartAndGraph.ChartCommon::HideObject(UnityEngine.GameObject,System.Boolean)
extern void ChartCommon_HideObject_m02029FFAFFD8E145E2A4023FD3BD15B06FA3BD92 (void);
// 0x000006F6 System.Single ChartAndGraph.ChartCommon::GetAutoLength(ChartAndGraph.AnyChart,ChartAndGraph.ChartOrientation)
extern void ChartCommon_GetAutoLength_mC1E9A8F65478C1B880C6EED6CFC12EE0FA2B270B (void);
// 0x000006F7 System.Single ChartAndGraph.ChartCommon::GetAutoDepth(ChartAndGraph.AnyChart,ChartAndGraph.ChartOrientation,ChartAndGraph.ChartDivisionInfo)
extern void ChartCommon_GetAutoDepth_m12DD417D9FD850CFC60154435AB17E7E2CFC2342 (void);
// 0x000006F8 System.Single ChartAndGraph.ChartCommon::GetAutoLength(ChartAndGraph.AnyChart,ChartAndGraph.ChartOrientation,ChartAndGraph.ChartDivisionInfo)
extern void ChartCommon_GetAutoLength_m244469F67A946AEF4422CC269BE15EEDF49DC29E (void);
// 0x000006F9 UnityEngine.Vector2 ChartAndGraph.ChartCommon::Perpendicular(UnityEngine.Vector2)
extern void ChartCommon_Perpendicular_m8984CB53060E190870D26D2FADA4D721C0E5A48A (void);
// 0x000006FA System.Boolean ChartAndGraph.ChartCommon::SegmentIntersection(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2&)
extern void ChartCommon_SegmentIntersection_m15BF96A23B5EFB4D182F2C73E00E84CD1F24A86E (void);
// 0x000006FB UnityEngine.Vector2 ChartAndGraph.ChartCommon::FromPolarRadians(System.Single,System.Single)
extern void ChartCommon_FromPolarRadians_mB05713735FE5CF63F9032FB1E118EF346B70448A (void);
// 0x000006FC UnityEngine.Vector2 ChartAndGraph.ChartCommon::FromPolar(System.Single,System.Single)
extern void ChartCommon_FromPolar_mC7C3BA4D189BB96C571550D023BABB7C26B74648 (void);
// 0x000006FD UnityEngine.Rect ChartAndGraph.ChartCommon::FixRect(UnityEngine.Rect)
extern void ChartCommon_FixRect_m0CF29336CD969BDAAA29B7C1C8DCCFE16AF5ED9A (void);
// 0x000006FE UnityEngine.Material ChartAndGraph.ChartCommon::get_DefaultMaterial()
extern void ChartCommon_get_DefaultMaterial_m1D33DE6E247F3311C5AA70A5A7DC61343A96F809 (void);
// 0x000006FF System.Boolean ChartAndGraph.ChartCommon::SafeAssignMaterial(UnityEngine.Renderer,UnityEngine.Material,UnityEngine.Material)
extern void ChartCommon_SafeAssignMaterial_m059805B45166DE800D2A048B890F1079396D193A (void);
// 0x00000700 System.Void ChartAndGraph.ChartCommon::CleanMesh(UnityEngine.Mesh,UnityEngine.Mesh&)
extern void ChartCommon_CleanMesh_m755E1C0824152425BD9276BB0ABD8109414DC964 (void);
// 0x00000701 System.Boolean ChartAndGraph.ChartCommon::get_IsInEditMode()
extern void ChartCommon_get_IsInEditMode_m0B9271E100A57E945F661EEDF0F86B6ACA13B0E4 (void);
// 0x00000702 System.Void ChartAndGraph.ChartCommon::SafeDestroy(UnityEngine.Object)
extern void ChartCommon_SafeDestroy_m58D9FA166A831CBEC3AD36A4F52968B940CEA112 (void);
// 0x00000703 UnityEngine.UIVertex ChartAndGraph.ChartCommon::CreateVertex(UnityEngine.Vector3,UnityEngine.Vector2)
extern void ChartCommon_CreateVertex_mFC195A32B6F4AD8DB1FF7A15ECD8A854CD8BE556 (void);
// 0x00000704 UnityEngine.UIVertex ChartAndGraph.ChartCommon::CreateVertex(UnityEngine.Vector3,UnityEngine.Vector2,System.Single)
extern void ChartCommon_CreateVertex_m1ADE199AC6F90175A82A6352698F88423360BAB0 (void);
// 0x00000705 System.Single ChartAndGraph.ChartCommon::GetTiling(ChartAndGraph.MaterialTiling)
extern void ChartCommon_GetTiling_m8E6D2702A7438ECB2F4607A904103FFD6723CB14 (void);
// 0x00000706 System.Void ChartAndGraph.ChartCommon::FixBillboardText(ItemLabelsBase,BillboardText)
extern void ChartCommon_FixBillboardText_m6D88EEC7627A9D263DFF27A743E18FDD5FD455CE (void);
// 0x00000707 T ChartAndGraph.ChartCommon::EnsureComponent(UnityEngine.GameObject)
// 0x00000708 System.Single ChartAndGraph.ChartCommon::DotProduct(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void ChartCommon_DotProduct_m5467DC6123D413FE21F1086FC990946230C1B1FC (void);
// 0x00000709 System.Single ChartAndGraph.ChartCommon::CrossProduct(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void ChartCommon_CrossProduct_mF90F396666234B55E717787B886C7E588C72E750 (void);
// 0x0000070A System.Single ChartAndGraph.ChartCommon::SegmentPointSqrDistance(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Vector2)
extern void ChartCommon_SegmentPointSqrDistance_m6F01504F9CA014E7BBAE1352CF4A88615EE35C1A (void);
// 0x0000070B UnityEngine.Vector3 ChartAndGraph.ChartCommon::LineCrossing(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ChartCommon_LineCrossing_m1CBED50D13323CF24F4AFD7F6D78FF27EB2D7D43 (void);
// 0x0000070C BillboardText ChartAndGraph.ChartCommon::UpdateBillboardText(BillboardText,UnityEngine.Transform,System.String,System.Single,System.Single,System.Single,System.Single,UnityEngine.Transform,System.Boolean,System.Boolean)
extern void ChartCommon_UpdateBillboardText_m14EF9B304498670F3C541C81DD77CFEDA7FB485B (void);
// 0x0000070D BillboardText ChartAndGraph.ChartCommon::CreateBillboardText(BillboardText,UnityEngine.MonoBehaviour,UnityEngine.Transform,System.String,System.Single,System.Single,System.Single,System.Single,UnityEngine.Transform,System.Boolean,System.Int32,System.Single)
extern void ChartCommon_CreateBillboardText_m6C6C6D21CF8003A39988A9846AD701D5AEC1F6F8 (void);
// 0x0000070E System.Void ChartAndGraph.ChartCommon::DoTextSignInternal(UnityEngine.MonoBehaviour,System.Double)
extern void ChartCommon_DoTextSignInternal_mDE8C01E9282B2853E664C8F786DCB3AF0FACE4D3 (void);
// 0x0000070F System.Void ChartAndGraph.ChartCommon::DoTextSign(UnityEngine.MonoBehaviour,System.Double)
extern void ChartCommon_DoTextSign_mD77A567B0F6B022FD728C6CE2B9375C82E7FDB5A (void);
// 0x00000710 System.Void ChartAndGraph.ChartCommon::GetTextInternal(UnityEngine.GameObject,System.String&)
extern void ChartCommon_GetTextInternal_mFC69F4FB31FEA49CA1D0C727739CEFD0D7A78608 (void);
// 0x00000711 System.String ChartAndGraph.ChartCommon::GetText(UnityEngine.GameObject)
extern void ChartCommon_GetText_m1B7ACA5C982205583B68EE99290163D37994B17B (void);
// 0x00000712 System.Void ChartAndGraph.ChartCommon::UpdateTextParamsInternal(UnityEngine.GameObject,System.String)
extern void ChartCommon_UpdateTextParamsInternal_m51D34A42A994C7D5AB49C0927CC9DF8BA031FC16 (void);
// 0x00000713 System.Void ChartAndGraph.ChartCommon::UpdateTextParams(UnityEngine.GameObject,System.String)
extern void ChartCommon_UpdateTextParams_m473E0338A8FAD950003826E2E42AE9BCD5399B0E (void);
// 0x00000714 System.Void ChartAndGraph.ChartCommon::MakeMaskable(UnityEngine.GameObject,System.Boolean)
extern void ChartCommon_MakeMaskable_m35BB02A85D2D3DFD5A50D83D394F9C2C5CE3CDA0 (void);
// 0x00000715 System.Void ChartAndGraph.ChartCommon::SetTextParamsInternal(UnityEngine.GameObject,System.String,System.Int32,System.Single,System.Boolean&)
extern void ChartCommon_SetTextParamsInternal_m5FCE550700149981A25C9D62F159E3E29B08B4E7 (void);
// 0x00000716 System.Boolean ChartAndGraph.ChartCommon::SetTextParams(UnityEngine.GameObject,System.String,System.Int32,System.Single)
extern void ChartCommon_SetTextParams_m6A082A38EE438F6A312445FED24CB245EB252839 (void);
// 0x00000717 System.Collections.Generic.IEqualityComparer`1<System.Int32> ChartAndGraph.ChartCommon::get_DefaultIntComparer()
extern void ChartCommon_get_DefaultIntComparer_mAA3284825DD7E0A551A6AC72786E3AF5704CC917 (void);
// 0x00000718 System.Void ChartAndGraph.ChartCommon::set_DefaultIntComparer(System.Collections.Generic.IEqualityComparer`1<System.Int32>)
extern void ChartCommon_set_DefaultIntComparer_m6C8736F71A022D13C54B0CAE41C075375F0804AE (void);
// 0x00000719 System.Collections.Generic.IEqualityComparer`1<System.Double> ChartAndGraph.ChartCommon::get_DefaultDoubleComparer()
extern void ChartCommon_get_DefaultDoubleComparer_m207B0BBC5438E7E4DFA79767A7ADE3057B2F4C29 (void);
// 0x0000071A System.Void ChartAndGraph.ChartCommon::set_DefaultDoubleComparer(System.Collections.Generic.IEqualityComparer`1<System.Double>)
extern void ChartCommon_set_DefaultDoubleComparer_m9E82EF5C50F5AEDDAF7CD4D6ABAAE35FDC6E5A7E (void);
// 0x0000071B System.Collections.Generic.IEqualityComparer`1<ChartAndGraph.DoubleVector3> ChartAndGraph.ChartCommon::get_DefaultDoubleVector3Comparer()
extern void ChartCommon_get_DefaultDoubleVector3Comparer_m3ACF2569E62983F10A1E0C4D60011A10D9997BBA (void);
// 0x0000071C System.Void ChartAndGraph.ChartCommon::set_DefaultDoubleVector3Comparer(System.Collections.Generic.IEqualityComparer`1<ChartAndGraph.DoubleVector3>)
extern void ChartCommon_set_DefaultDoubleVector3Comparer_m5E898325C6939EF7226EEFD7FAD89B3FDF7BA915 (void);
// 0x0000071D System.Void ChartAndGraph.ChartCommon::.ctor()
extern void ChartCommon__ctor_mC150ED618E2E9DC9F015022ABD0841D19D13A357 (void);
// 0x0000071E System.Boolean ChartAndGraph.ChartCommon/IntComparer::Equals(System.Int32,System.Int32)
extern void IntComparer_Equals_mCE5289036CDFEFE50B60C04CE44754C90F07C77E (void);
// 0x0000071F System.Int32 ChartAndGraph.ChartCommon/IntComparer::GetHashCode(System.Int32)
extern void IntComparer_GetHashCode_m0AEE9673DE40DBDB70B54466EA4530321B326CF6 (void);
// 0x00000720 System.Void ChartAndGraph.ChartCommon/IntComparer::.ctor()
extern void IntComparer__ctor_m914121068795F2529C12BF3F6E320E2171EB4781 (void);
// 0x00000721 System.Boolean ChartAndGraph.ChartCommon/DoubleComparer::Equals(System.Double,System.Double)
extern void DoubleComparer_Equals_m545DA4616136FD4C4781F743171E270A1A03134A (void);
// 0x00000722 System.Int32 ChartAndGraph.ChartCommon/DoubleComparer::GetHashCode(System.Double)
extern void DoubleComparer_GetHashCode_m0E63A72E1EBFCAD7F3712C2CADC9007CB7197F78 (void);
// 0x00000723 System.Void ChartAndGraph.ChartCommon/DoubleComparer::.ctor()
extern void DoubleComparer__ctor_mB598442B9A76F9E63F32BA24FB79836E6C0313A7 (void);
// 0x00000724 System.Boolean ChartAndGraph.ChartCommon/DoubleVector3Comparer::Equals(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3Comparer_Equals_m15019ABE09D8C7858F746371951C182E0B1A64FF (void);
// 0x00000725 System.Int32 ChartAndGraph.ChartCommon/DoubleVector3Comparer::GetHashCode(ChartAndGraph.DoubleVector3)
extern void DoubleVector3Comparer_GetHashCode_m9C95E96710E934B403932BFC2997F2E199E2E051 (void);
// 0x00000726 System.Void ChartAndGraph.ChartCommon/DoubleVector3Comparer::.ctor()
extern void DoubleVector3Comparer__ctor_m9CEA68B314D59872BA13BAE5ADC106329D068751 (void);
// 0x00000727 System.Void ChartAndGraph.BaseScrollableCategoryData::.ctor()
extern void BaseScrollableCategoryData__ctor_m2FA54FC47B3F343387495C326BC2D9813EA2535C (void);
// 0x00000728 System.Double ChartAndGraph.BaseSlider::get_Duration()
extern void BaseSlider_get_Duration_m5771749A0F1AFACA2997F1053A4502EC124BF094 (void);
// 0x00000729 System.Void ChartAndGraph.BaseSlider::set_Duration(System.Double)
extern void BaseSlider_set_Duration_m7CE1BAD2AADB23CD5710DDAECAFD59B478952C3D (void);
// 0x0000072A System.Double ChartAndGraph.BaseSlider::get_StartTime()
extern void BaseSlider_get_StartTime_mABAD31F5ABFF68C526320A9CA287BC29CDC73BAE (void);
// 0x0000072B System.Void ChartAndGraph.BaseSlider::set_StartTime(System.Double)
extern void BaseSlider_set_StartTime_m837A80AE1654C5A03634E81241483A31285604CA (void);
// 0x0000072C ChartAndGraph.DoubleVector2 ChartAndGraph.BaseSlider::get_Max()
// 0x0000072D ChartAndGraph.DoubleVector2 ChartAndGraph.BaseSlider::get_Min()
// 0x0000072E System.String ChartAndGraph.BaseSlider::get_Category()
// 0x0000072F System.Int32 ChartAndGraph.BaseSlider::get_MinIndex()
// 0x00000730 System.Boolean ChartAndGraph.BaseSlider::Update()
extern void BaseSlider_Update_mA24868A419012FA26D3CFAECB75BBFC201E6326D (void);
// 0x00000731 System.Void ChartAndGraph.BaseSlider::.ctor()
extern void BaseSlider__ctor_mCB4C62D22B35D27501FE4F75B40943425FBA07B3 (void);
// 0x00000732 System.Void ChartAndGraph.IChartData::Update()
// 0x00000733 System.Void ChartAndGraph.IChartData::OnAfterDeserialize()
// 0x00000734 System.Void ChartAndGraph.IChartData::OnBeforeSerialize()
// 0x00000735 System.Void ChartAndGraph.ScrollableChartData::add_DataChanged(System.EventHandler)
extern void ScrollableChartData_add_DataChanged_m53454856861790BB166459C847049FB880A090C9 (void);
// 0x00000736 System.Void ChartAndGraph.ScrollableChartData::remove_DataChanged(System.EventHandler)
extern void ScrollableChartData_remove_DataChanged_m7BA12D21F694D295693C39AE35D82029698C03F2 (void);
// 0x00000737 System.Void ChartAndGraph.ScrollableChartData::add_ViewPortionChanged(System.EventHandler)
extern void ScrollableChartData_add_ViewPortionChanged_mC7F6282B4113B254E79F20BFDEA4E2E735A8AC05 (void);
// 0x00000738 System.Void ChartAndGraph.ScrollableChartData::remove_ViewPortionChanged(System.EventHandler)
extern void ScrollableChartData_remove_ViewPortionChanged_m4F1A28D51F140E9964EAFC4E67761798659D59B3 (void);
// 0x00000739 System.Void ChartAndGraph.ScrollableChartData::add_RealtimeDataChanged(System.Action`2<System.Int32,System.String>)
extern void ScrollableChartData_add_RealtimeDataChanged_m0982CDB8E7798B3E606CA10F73634E4868CB64D1 (void);
// 0x0000073A System.Void ChartAndGraph.ScrollableChartData::remove_RealtimeDataChanged(System.Action`2<System.Int32,System.String>)
extern void ScrollableChartData_remove_RealtimeDataChanged_mDE9BB5466ABAA2D883C6E9726976621D6914CEAB (void);
// 0x0000073B System.Void ChartAndGraph.ScrollableChartData::StartBatch()
extern void ScrollableChartData_StartBatch_m753BE704EB0CE0B6A0214B75DFDEB46911631D1C (void);
// 0x0000073C System.Void ChartAndGraph.ScrollableChartData::EndBatch()
extern void ScrollableChartData_EndBatch_m6685BC6D3657A5322B478FD998599A8071112F19 (void);
// 0x0000073D System.Double ChartAndGraph.ScrollableChartData::get_AutomaticVerticallViewGap()
extern void ScrollableChartData_get_AutomaticVerticallViewGap_m1DC931258FC2271235E7F0C12E261BF3FC7055B8 (void);
// 0x0000073E System.Void ChartAndGraph.ScrollableChartData::set_AutomaticVerticallViewGap(System.Double)
extern void ScrollableChartData_set_AutomaticVerticallViewGap_mE6A43FC5C89F7ABE30C0481314DCBD746B11A60A (void);
// 0x0000073F System.Boolean ChartAndGraph.ScrollableChartData::get_AutomaticVerticallView()
extern void ScrollableChartData_get_AutomaticVerticallView_m42D15B487B45AB76AA291341AF18C4717986C014 (void);
// 0x00000740 System.Void ChartAndGraph.ScrollableChartData::set_AutomaticVerticallView(System.Boolean)
extern void ScrollableChartData_set_AutomaticVerticallView_m815C09C9827B8137FBFC6050ECEAE34C9F7EE8A9 (void);
// 0x00000741 System.Double ChartAndGraph.ScrollableChartData::get_AutomaticcHorizontaViewGap()
extern void ScrollableChartData_get_AutomaticcHorizontaViewGap_mAD22B2701D5F4B7B602BCEE8AC48A06E25605893 (void);
// 0x00000742 System.Void ChartAndGraph.ScrollableChartData::set_AutomaticcHorizontaViewGap(System.Double)
extern void ScrollableChartData_set_AutomaticcHorizontaViewGap_m35565A02DCBF18AF471E4F3556B431632A73C654 (void);
// 0x00000743 System.Boolean ChartAndGraph.ScrollableChartData::get_AutomaticHorizontalView()
extern void ScrollableChartData_get_AutomaticHorizontalView_mF51EC62BBE8006208ABB6DDADD936284F5B1B828 (void);
// 0x00000744 System.Void ChartAndGraph.ScrollableChartData::set_AutomaticHorizontalView(System.Boolean)
extern void ScrollableChartData_set_AutomaticHorizontalView_m203D43700DD4132599139AC7843B64BE3C428889 (void);
// 0x00000745 System.Double ChartAndGraph.ScrollableChartData::get_HorizontalViewSize()
extern void ScrollableChartData_get_HorizontalViewSize_mE3846AD3B190DE8F23B6115C95E7CDF75E568B49 (void);
// 0x00000746 System.Void ChartAndGraph.ScrollableChartData::set_HorizontalViewSize(System.Double)
extern void ScrollableChartData_set_HorizontalViewSize_m0F4FE5011D658285B43416FDE9C8B82515AA8155 (void);
// 0x00000747 System.Double ChartAndGraph.ScrollableChartData::get_HorizontalViewOrigin()
extern void ScrollableChartData_get_HorizontalViewOrigin_m79E3B1D9878D0595C1E5B6353EFDCA8BE904273D (void);
// 0x00000748 System.Void ChartAndGraph.ScrollableChartData::set_HorizontalViewOrigin(System.Double)
extern void ScrollableChartData_set_HorizontalViewOrigin_mB5C2AF750BFA50D568B07AB31F5D7CE4CBD4C22F (void);
// 0x00000749 System.Double ChartAndGraph.ScrollableChartData::get_VerticalViewSize()
extern void ScrollableChartData_get_VerticalViewSize_m64C2EFE321ACE1AD2EB8DF73542030E81F80134B (void);
// 0x0000074A System.Void ChartAndGraph.ScrollableChartData::set_VerticalViewSize(System.Double)
extern void ScrollableChartData_set_VerticalViewSize_mE720414B1C5FBFB5B903587BA4B58483E773512F (void);
// 0x0000074B System.Void ChartAndGraph.ScrollableChartData::Clear()
extern void ScrollableChartData_Clear_m76409556475B150DCE22E9E17895B681F72F6897 (void);
// 0x0000074C System.Double ChartAndGraph.ScrollableChartData::get_VerticalViewOrigin()
extern void ScrollableChartData_get_VerticalViewOrigin_m2AF691D4B0C0BD90BA214821C49912B5EADDCC3E (void);
// 0x0000074D System.Void ChartAndGraph.ScrollableChartData::set_VerticalViewOrigin(System.Double)
extern void ScrollableChartData_set_VerticalViewOrigin_mBB25EF7C00EDE722B87AA57174B9732FC642BDDC (void);
// 0x0000074E System.Void ChartAndGraph.ScrollableChartData::Update()
extern void ScrollableChartData_Update_m90A18043BBB0834B143678E5A00C6967F904DEFE (void);
// 0x0000074F System.Boolean ChartAndGraph.ScrollableChartData::HasCategory(System.String)
extern void ScrollableChartData_HasCategory_m90D552BAF0C3EEE9857FCB3A82EF824F0001E816 (void);
// 0x00000750 System.Void ChartAndGraph.ScrollableChartData::ModifyMinMax(ChartAndGraph.BaseScrollableCategoryData,ChartAndGraph.DoubleVector3)
extern void ScrollableChartData_ModifyMinMax_m44E3BC316E28094BC92A7ACA26125922F9BCE016 (void);
// 0x00000751 System.Double ChartAndGraph.ScrollableChartData::GetMaxValue(System.Int32,System.Boolean)
extern void ScrollableChartData_GetMaxValue_m2DE0E70B4A9F4AA4351F8C2CB1FD461C3213CD63 (void);
// 0x00000752 System.Double ChartAndGraph.ScrollableChartData::GetMinValue(System.Int32,System.Boolean)
extern void ScrollableChartData_GetMinValue_mF4006D0063CDE7411A14E5FB18276478C0B1CDBB (void);
// 0x00000753 System.Void ChartAndGraph.ScrollableChartData::RaiseRealtimeDataChanged(System.Int32,System.String)
extern void ScrollableChartData_RaiseRealtimeDataChanged_m5A94B2C27F0F9954476D9DA71167CFE570CBF735 (void);
// 0x00000754 System.Void ChartAndGraph.ScrollableChartData::RaiseViewPortionChanged()
extern void ScrollableChartData_RaiseViewPortionChanged_m150466DB78E2C9D348746B4E90EC4013D75E89E6 (void);
// 0x00000755 System.Void ChartAndGraph.ScrollableChartData::RaiseDataChanged()
extern void ScrollableChartData_RaiseDataChanged_m3065149898B001AF48C25F8EBD215AE6AFA4F81E (void);
// 0x00000756 System.Void ChartAndGraph.ScrollableChartData::RestoreDataValues()
extern void ScrollableChartData_RestoreDataValues_mC08159937DDC817D985D3BE25440CFD02008D5FF (void);
// 0x00000757 System.Void ChartAndGraph.ScrollableChartData::RestoreDataValues(System.Int32)
extern void ScrollableChartData_RestoreDataValues_mAE23047F02547B15FE2A7CB12520AD63E33C4B92 (void);
// 0x00000758 System.Void ChartAndGraph.ScrollableChartData::InnerClearCategory(System.String)
// 0x00000759 System.Boolean ChartAndGraph.ScrollableChartData::AddCategory(System.String,ChartAndGraph.BaseScrollableCategoryData)
// 0x0000075A System.Void ChartAndGraph.ScrollableChartData::AppendDatum(System.String,ChartAndGraph.MixedSeriesGenericValue)
// 0x0000075B System.Void ChartAndGraph.ScrollableChartData::AppendDatum(System.String,System.Collections.Generic.IList`1<ChartAndGraph.MixedSeriesGenericValue>)
// 0x0000075C ChartAndGraph.BaseScrollableCategoryData ChartAndGraph.ScrollableChartData::GetDefaultCategory()
// 0x0000075D System.Void ChartAndGraph.ScrollableChartData::OnAfterDeserialize()
// 0x0000075E System.Void ChartAndGraph.ScrollableChartData::OnBeforeSerialize()
// 0x0000075F System.Boolean ChartAndGraph.ScrollableChartData::ChartAndGraph.IMixedSeriesProxy.AddCategory(System.String,ChartAndGraph.BaseScrollableCategoryData)
extern void ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_AddCategory_mBACFB9E256A44B81A7A2CB1BCDC3D26A726CD83C (void);
// 0x00000760 System.Boolean ChartAndGraph.ScrollableChartData::ChartAndGraph.IMixedSeriesProxy.HasCategory(System.String)
extern void ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_HasCategory_mCB3C025D5035D28D1391E931E880180B6B02699C (void);
// 0x00000761 System.Void ChartAndGraph.ScrollableChartData::ChartAndGraph.IMixedSeriesProxy.ClearCategory(System.String)
extern void ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_ClearCategory_m790358592BA70062489A71FF4B8EE87F84212634 (void);
// 0x00000762 System.Void ChartAndGraph.ScrollableChartData::ChartAndGraph.IMixedSeriesProxy.AppendDatum(System.String,ChartAndGraph.MixedSeriesGenericValue)
extern void ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_AppendDatum_m7A49600482E18B2D9B291800CF1CD4A3CCF90031 (void);
// 0x00000763 System.Void ChartAndGraph.ScrollableChartData::ChartAndGraph.IMixedSeriesProxy.AppendDatum(System.String,System.Collections.Generic.IList`1<ChartAndGraph.MixedSeriesGenericValue>)
extern void ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_AppendDatum_m2DC78CFC42052D8D010A6AB506948FC00BEACE99 (void);
// 0x00000764 System.Void ChartAndGraph.ScrollableChartData::.ctor()
extern void ScrollableChartData__ctor_m3898019CFC0087D9FCCF25D9772ECA70A7977B2B (void);
// 0x00000765 System.Boolean ChartAndGraph.ScrollableChartData::<Update>b__47_0(ChartAndGraph.BaseSlider)
extern void ScrollableChartData_U3CUpdateU3Eb__47_0_mE8BFC1F2E09B15FAC7A0D7B71CD96F8B7C2105CE (void);
// 0x00000766 System.Double ChartAndGraph.ChartDateUtility::TimeSpanToValue(System.TimeSpan)
extern void ChartDateUtility_TimeSpanToValue_mD85D68BAA537194C060028AED8770291C9DA471D (void);
// 0x00000767 System.Double ChartAndGraph.ChartDateUtility::DateToValue(System.DateTime)
extern void ChartDateUtility_DateToValue_m547E8C8A33A2AB7168FA07D58170CF34914EDA6C (void);
// 0x00000768 System.String ChartAndGraph.ChartDateUtility::DateToTimeString(System.DateTime)
extern void ChartDateUtility_DateToTimeString_m0CD54CFEC440E4B60B3A759B94D5D93387C971F1 (void);
// 0x00000769 System.String ChartAndGraph.ChartDateUtility::DateToDateString(System.DateTime)
extern void ChartDateUtility_DateToDateString_mB8AC06BB0C6D4C47805321422F6A4DCECF759A2D (void);
// 0x0000076A System.String ChartAndGraph.ChartDateUtility::DateToDateTimeString(System.DateTime,System.String)
extern void ChartDateUtility_DateToDateTimeString_m7A7968FE192BC3552607BA61C43E19FE8101C86B (void);
// 0x0000076B System.DateTime ChartAndGraph.ChartDateUtility::ValueToDate(System.Double)
extern void ChartDateUtility_ValueToDate_m03E541A8CAE9CE7C2958BE375484432B3F6A449F (void);
// 0x0000076C System.Void ChartAndGraph.ChartDateUtility::.ctor()
extern void ChartDateUtility__ctor_m4297D4721F1BB503159B1E130A94124B797BE24E (void);
// 0x0000076D System.Void ChartAndGraph.ChartDateUtility::.cctor()
extern void ChartDateUtility__cctor_m2F4DDB7A3CE5001AA50D12B7E0650FD2BB4DE584 (void);
// 0x0000076E System.Void ChartAndGraph.ChartDynamicMaterial::.ctor()
extern void ChartDynamicMaterial__ctor_mC3379C8307242316AE5A02C4220C0B5391F0CDDD (void);
// 0x0000076F System.Void ChartAndGraph.ChartDynamicMaterial::.ctor(UnityEngine.Material)
extern void ChartDynamicMaterial__ctor_mD29B0B9C6654060E87FD8DC034702D28B88BB1B5 (void);
// 0x00000770 System.Void ChartAndGraph.ChartDynamicMaterial::.ctor(UnityEngine.Material,UnityEngine.Color,UnityEngine.Color)
extern void ChartDynamicMaterial__ctor_m70B79AA9472A38B4263A19EA51F17B5B58400BE9 (void);
// 0x00000771 System.Boolean ChartAndGraph.CharItemEffectController::get_WorkOnParent()
extern void CharItemEffectController_get_WorkOnParent_m903EB04E0D1C9340CF5714BBC942750CF46CAB3F (void);
// 0x00000772 System.Void ChartAndGraph.CharItemEffectController::set_WorkOnParent(System.Boolean)
extern void CharItemEffectController_set_WorkOnParent_mB59EB8AF35F52B149CDB79C484084DC7DD101D89 (void);
// 0x00000773 System.Boolean ChartAndGraph.CharItemEffectController::get_InitialScale()
extern void CharItemEffectController_get_InitialScale_m407352A2708523959EB60767979C30437DE9B6B7 (void);
// 0x00000774 System.Void ChartAndGraph.CharItemEffectController::set_InitialScale(System.Boolean)
extern void CharItemEffectController_set_InitialScale_m90D615D3B6E717032E683EEA0E1A9D7B27B8CD36 (void);
// 0x00000775 UnityEngine.Transform ChartAndGraph.CharItemEffectController::get_Parent()
extern void CharItemEffectController_get_Parent_m4E2198849F5E8F4461AA7240E2A1C6E2C791604C (void);
// 0x00000776 System.Void ChartAndGraph.CharItemEffectController::.ctor()
extern void CharItemEffectController__ctor_mAC6A6CFD7A1AA22A910A6ED0E4E3635750E70E7B (void);
// 0x00000777 System.Void ChartAndGraph.CharItemEffectController::Start()
extern void CharItemEffectController_Start_m42E8ADC4ECE6095C48403CC3EFD8676B6CF85706 (void);
// 0x00000778 System.Void ChartAndGraph.CharItemEffectController::OnTransformParentChanged()
extern void CharItemEffectController_OnTransformParentChanged_mA1024CE4DEF92E5E95A0A88205391BB16D793655 (void);
// 0x00000779 System.Void ChartAndGraph.CharItemEffectController::Update()
extern void CharItemEffectController_Update_mD46D24DC52B2E733F69160430C86E04101AD1065 (void);
// 0x0000077A System.Void ChartAndGraph.CharItemEffectController::Unregister(ChartAndGraph.ChartItemEffect)
extern void CharItemEffectController_Unregister_m57EFA9B3CDC178C118105FE52419B62156A862CE (void);
// 0x0000077B System.Void ChartAndGraph.CharItemEffectController::Register(ChartAndGraph.ChartItemEffect)
extern void CharItemEffectController_Register_mF0611912B318AE501CE3E0148DB51B4B66BDA98E (void);
// 0x0000077C System.Int32 ChartAndGraph.ChartItemEffect::get_ItemIndex()
extern void ChartItemEffect_get_ItemIndex_m4340775567B35B857FCA0AC7F9E145B82BCED8CA (void);
// 0x0000077D System.Void ChartAndGraph.ChartItemEffect::set_ItemIndex(System.Int32)
extern void ChartItemEffect_set_ItemIndex_m2690B655651FB9CC9E70E2966F476CCE9B072314 (void);
// 0x0000077E System.Int32 ChartAndGraph.ChartItemEffect::get_ItemType()
extern void ChartItemEffect_get_ItemType_m043287B6B47E2BC931B19B9F70D9F40B3EB08A0B (void);
// 0x0000077F System.Void ChartAndGraph.ChartItemEffect::set_ItemType(System.Int32)
extern void ChartItemEffect_set_ItemType_m99ACEB65F5F84DDE284947F6A251D9E3DD8EFD91 (void);
// 0x00000780 System.Object ChartAndGraph.ChartItemEffect::get_ItemData()
extern void ChartItemEffect_get_ItemData_m325E440723EC2C55FA58947F2D540423860308BA (void);
// 0x00000781 System.Void ChartAndGraph.ChartItemEffect::set_ItemData(System.Object)
extern void ChartItemEffect_set_ItemData_mB74438B594B425E6943D0A8E06078CBBC7A24227 (void);
// 0x00000782 ChartAndGraph.CharItemEffectController ChartAndGraph.ChartItemEffect::get_Controller()
extern void ChartItemEffect_get_Controller_m454FD7C44623E47515EBA523C68BD5A109D90AE4 (void);
// 0x00000783 System.Void ChartAndGraph.ChartItemEffect::add_Deactivate(System.Action`1<ChartAndGraph.ChartItemEffect>)
extern void ChartItemEffect_add_Deactivate_mCED293771E82E147D0842EAF25B06F06810864B7 (void);
// 0x00000784 System.Void ChartAndGraph.ChartItemEffect::remove_Deactivate(System.Action`1<ChartAndGraph.ChartItemEffect>)
extern void ChartItemEffect_remove_Deactivate_m3A9E4240833B71B8DBB7D2F4F51654FD1DCEFA2B (void);
// 0x00000785 System.Void ChartAndGraph.ChartItemEffect::RaiseDeactivated()
extern void ChartItemEffect_RaiseDeactivated_mFF0196CCB2545FC6F28EF2ADCFE715447ABD25B2 (void);
// 0x00000786 System.Void ChartAndGraph.ChartItemEffect::Register()
extern void ChartItemEffect_Register_mFAA04D751E14B227542F9B9C65E7A8FE6BC1C026 (void);
// 0x00000787 System.Void ChartAndGraph.ChartItemEffect::Unregister()
extern void ChartItemEffect_Unregister_mEC134C063511E588D322E7144198AE8B7BA0C146 (void);
// 0x00000788 System.Void ChartAndGraph.ChartItemEffect::OnDisable()
extern void ChartItemEffect_OnDisable_m486469A47AFCFB1CE6075F679AF090E00CB99F4C (void);
// 0x00000789 System.Void ChartAndGraph.ChartItemEffect::OnEnable()
extern void ChartItemEffect_OnEnable_m5B073C831E3ED7B3C28C7BEA3CF4891D2F6E1201 (void);
// 0x0000078A System.Void ChartAndGraph.ChartItemEffect::Start()
extern void ChartItemEffect_Start_m9451458A361B8C4303871AB087F99C63B085C4AD (void);
// 0x0000078B System.Void ChartAndGraph.ChartItemEffect::Destroy()
extern void ChartItemEffect_Destroy_mE8F4F293BB6F63D8559879BAD9DAFAAEE0CC146C (void);
// 0x0000078C System.Void ChartAndGraph.ChartItemEffect::TriggerIn(System.Boolean)
// 0x0000078D System.Void ChartAndGraph.ChartItemEffect::TriggerOut(System.Boolean)
// 0x0000078E UnityEngine.Vector3 ChartAndGraph.ChartItemEffect::get_ScaleMultiplier()
// 0x0000078F UnityEngine.Quaternion ChartAndGraph.ChartItemEffect::get_Rotation()
// 0x00000790 UnityEngine.Vector3 ChartAndGraph.ChartItemEffect::get_Translation()
// 0x00000791 System.Void ChartAndGraph.ChartItemEffect::.ctor()
extern void ChartItemEffect__ctor_m21A2EFA5839620F05EC02DAB1F02FBBE1F2ED56A (void);
// 0x00000792 ChartAndGraph.IInternalUse ChartAndGraph.ChartItemEvents::ChartAndGraph.InternalItemEvents.get_Parent()
extern void ChartItemEvents_ChartAndGraph_InternalItemEvents_get_Parent_mECA63081A5FE2EE69594C704534C160E314F5E14 (void);
// 0x00000793 System.Void ChartAndGraph.ChartItemEvents::ChartAndGraph.InternalItemEvents.set_Parent(ChartAndGraph.IInternalUse)
extern void ChartItemEvents_ChartAndGraph_InternalItemEvents_set_Parent_mED7B90B0ABF0D0AD54D210B6EF2EF321ED95169C (void);
// 0x00000794 System.Object ChartAndGraph.ChartItemEvents::ChartAndGraph.InternalItemEvents.get_UserData()
extern void ChartItemEvents_ChartAndGraph_InternalItemEvents_get_UserData_m6EF2B0D7B1D95E7A95C0DF1058DCDACF2CF8CC6E (void);
// 0x00000795 System.Void ChartAndGraph.ChartItemEvents::ChartAndGraph.InternalItemEvents.set_UserData(System.Object)
extern void ChartItemEvents_ChartAndGraph_InternalItemEvents_set_UserData_m621FB1CF8E06BB20C649620198743712D90D9F6F (void);
// 0x00000796 System.Void ChartAndGraph.ChartItemEvents::Start()
extern void ChartItemEvents_Start_mE542B9E82FE8C580A516D3173005D3FF5A917CF1 (void);
// 0x00000797 System.Void ChartAndGraph.ChartItemEvents::OnMouseEnter()
extern void ChartItemEvents_OnMouseEnter_m5A5461805B7058537205A9FDA98E540C15B65ED7 (void);
// 0x00000798 System.Void ChartAndGraph.ChartItemEvents::OnMouseExit()
extern void ChartItemEvents_OnMouseExit_m585425B7D3171A785BFA89829EE49C1551D1CE62 (void);
// 0x00000799 System.Void ChartAndGraph.ChartItemEvents::OnMouseDown()
extern void ChartItemEvents_OnMouseDown_mFEE27957BFBAD20A186C9B2B02A6A83B5A382B32 (void);
// 0x0000079A System.Void ChartAndGraph.ChartItemEvents::OnMouseUp()
extern void ChartItemEvents_OnMouseUp_mFCE6CB3CB398117A24F1708D54050C4BDB058D03 (void);
// 0x0000079B System.Void ChartAndGraph.ChartItemEvents::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void ChartItemEvents_OnPointerEnter_m390802A192708FD9B566664006C12DD1D1FF2B03 (void);
// 0x0000079C System.Void ChartAndGraph.ChartItemEvents::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void ChartItemEvents_OnPointerExit_m47A5C30AF5E7BBD5C48D6F720DB5202103E8B2F0 (void);
// 0x0000079D System.Void ChartAndGraph.ChartItemEvents::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ChartItemEvents_OnPointerDown_m94532FF8CC379E7E963B7B3F1D5F67BB7F2BE2CC (void);
// 0x0000079E System.Void ChartAndGraph.ChartItemEvents::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ChartItemEvents_OnPointerUp_mCAECD2912B1B5B6446AFC0C45EC2B487AFFD3B22 (void);
// 0x0000079F System.Void ChartAndGraph.ChartItemEvents::.ctor()
extern void ChartItemEvents__ctor_m956D57F6AA6A1293701D708FCD067087E7A738A2 (void);
// 0x000007A0 System.Void ChartAndGraph.ChartItemEvents/Event::.ctor()
extern void Event__ctor_m86A64E93F5FBBC5A1D212867A4540C1790CD8A45 (void);
// 0x000007A1 UnityEngine.Vector3 ChartAndGraph.ChartItemGrowEffect::get_ScaleMultiplier()
extern void ChartItemGrowEffect_get_ScaleMultiplier_m75CCFCC77307742DFC9143D61EAB0E056504CC1A (void);
// 0x000007A2 UnityEngine.Quaternion ChartAndGraph.ChartItemGrowEffect::get_Rotation()
extern void ChartItemGrowEffect_get_Rotation_m8E1E848F17AACB81BA4BFD8DFA729EBD4A68F0B2 (void);
// 0x000007A3 UnityEngine.Vector3 ChartAndGraph.ChartItemGrowEffect::get_Translation()
extern void ChartItemGrowEffect_get_Translation_mE5B4C7E8E687CBED8FEDB905D7A3D663839EB3A3 (void);
// 0x000007A4 System.Void ChartAndGraph.ChartItemGrowEffect::GrowAndShrink()
extern void ChartItemGrowEffect_GrowAndShrink_m01AAC4E599D8441664FA9586583F19ABA09F7A2D (void);
// 0x000007A5 System.Boolean ChartAndGraph.ChartItemGrowEffect::CheckAnimationEnded(System.Single,UnityEngine.AnimationCurve)
extern void ChartItemGrowEffect_CheckAnimationEnded_m1013DE42CEC05A3EE6FCAC26733A862C4493D3F2 (void);
// 0x000007A6 System.Void ChartAndGraph.ChartItemGrowEffect::FixEaseFunction(UnityEngine.AnimationCurve)
extern void ChartItemGrowEffect_FixEaseFunction_m7D49D1ACE24077326B30E4A085B80CF1548BE3CA (void);
// 0x000007A7 System.Void ChartAndGraph.ChartItemGrowEffect::Update()
extern void ChartItemGrowEffect_Update_m7037F22999F52D61304A0060F17334B71B371689 (void);
// 0x000007A8 System.Void ChartAndGraph.ChartItemGrowEffect::TriggerOut(System.Boolean)
extern void ChartItemGrowEffect_TriggerOut_m2785B42BE0C5C58EA0CE38E52B172E4561443527 (void);
// 0x000007A9 System.Void ChartAndGraph.ChartItemGrowEffect::TriggerIn(System.Boolean)
extern void ChartItemGrowEffect_TriggerIn_m6DCEA4FFEE8EA504031DBE1CDA509A0D60D22694 (void);
// 0x000007AA System.Void ChartAndGraph.ChartItemGrowEffect::Grow()
extern void ChartItemGrowEffect_Grow_m0D858BB1F561E19FB9B93DCB41AB49FE830324B5 (void);
// 0x000007AB System.Void ChartAndGraph.ChartItemGrowEffect::Shrink()
extern void ChartItemGrowEffect_Shrink_m136276F173A3A405E50E7D18F61F48DAD4A153D0 (void);
// 0x000007AC System.Void ChartAndGraph.ChartItemGrowEffect::.ctor()
extern void ChartItemGrowEffect__ctor_m7F877E0F472C81F23960361D3749B836AFFEE793 (void);
// 0x000007AD System.Void ChartAndGraph.ChartItemLerpEffect::Start()
extern void ChartItemLerpEffect_Start_m0DC1B7673756AFD7134570D30A15C9CD004182F2 (void);
// 0x000007AE System.Void ChartAndGraph.ChartItemLerpEffect::GrowAndShrink()
extern void ChartItemLerpEffect_GrowAndShrink_m153D418E580A229DD98DDEF1F98620C0B5BD6B5A (void);
// 0x000007AF System.Boolean ChartAndGraph.ChartItemLerpEffect::CheckAnimationEnded(System.Single,UnityEngine.AnimationCurve)
extern void ChartItemLerpEffect_CheckAnimationEnded_mE216026304EC9D113E47C196039D3CED5B83A4D6 (void);
// 0x000007B0 System.Void ChartAndGraph.ChartItemLerpEffect::FixEaseFunction(UnityEngine.AnimationCurve)
extern void ChartItemLerpEffect_FixEaseFunction_mBAF0478E17EB01F1D1F98F01859E732D1B64FAD9 (void);
// 0x000007B1 System.Void ChartAndGraph.ChartItemLerpEffect::ApplyLerp(System.Single)
// 0x000007B2 System.Single ChartAndGraph.ChartItemLerpEffect::GetStartValue()
// 0x000007B3 System.Void ChartAndGraph.ChartItemLerpEffect::Update()
extern void ChartItemLerpEffect_Update_mA7E24BCDD0A68A14FB720830715CE6D2EC3624E2 (void);
// 0x000007B4 System.Void ChartAndGraph.ChartItemLerpEffect::TriggerOut(System.Boolean)
extern void ChartItemLerpEffect_TriggerOut_mB55B91C9D1E281F55BF13351B3274683A06F94E4 (void);
// 0x000007B5 System.Void ChartAndGraph.ChartItemLerpEffect::TriggerIn(System.Boolean)
extern void ChartItemLerpEffect_TriggerIn_m2A0DF1A074E61AF8CF038E1A94E1C3004878A822 (void);
// 0x000007B6 System.Void ChartAndGraph.ChartItemLerpEffect::Grow()
extern void ChartItemLerpEffect_Grow_m043C7B11D33E747E6ADAC01FDCEE9F003A55C062 (void);
// 0x000007B7 System.Void ChartAndGraph.ChartItemLerpEffect::Shrink()
extern void ChartItemLerpEffect_Shrink_m67ABE2EA648C5C8706D13691F1D6FCACC9A03B09 (void);
// 0x000007B8 System.Void ChartAndGraph.ChartItemLerpEffect::.ctor()
extern void ChartItemLerpEffect__ctor_m433230ECF02C3C0EA75A18E52A5E78B7CDE0442F (void);
// 0x000007B9 System.Void ChartAndGraph.ChartItemMaterialLerpEffect::.ctor()
extern void ChartItemMaterialLerpEffect__ctor_m4DD636FDF3FD3D3FDA12B1D37BD6451CC4E0E17E (void);
// 0x000007BA System.Void ChartAndGraph.ChartItemTextBlend::Start()
extern void ChartItemTextBlend_Start_m3214B3E0224A33FF98F796B3B5E027F8194E887F (void);
// 0x000007BB UnityEngine.Quaternion ChartAndGraph.ChartItemTextBlend::get_Rotation()
extern void ChartItemTextBlend_get_Rotation_m4076F9EDBB3EF837A354F69D25873415E7A30758 (void);
// 0x000007BC UnityEngine.Vector3 ChartAndGraph.ChartItemTextBlend::get_ScaleMultiplier()
extern void ChartItemTextBlend_get_ScaleMultiplier_m136015C8A05CB9AE9547107665D6770AA8E19F1F (void);
// 0x000007BD UnityEngine.Vector3 ChartAndGraph.ChartItemTextBlend::get_Translation()
extern void ChartItemTextBlend_get_Translation_m5059C00742F62D592B1E5372701F0A00FAF73533 (void);
// 0x000007BE System.Single ChartAndGraph.ChartItemTextBlend::GetStartValue()
extern void ChartItemTextBlend_GetStartValue_mA549C99ED0C8F5B4B534C28F82B6AB1B5531C82F (void);
// 0x000007BF UnityEngine.CanvasRenderer ChartAndGraph.ChartItemTextBlend::EnsureRenderer()
extern void ChartItemTextBlend_EnsureRenderer_mBD0250B1BC76A6E7B6561AA0D61D191A3AAED13D (void);
// 0x000007C0 System.Void ChartAndGraph.ChartItemTextBlend::ApplyLerp(System.Single)
extern void ChartItemTextBlend_ApplyLerp_m8212C2E275DF4A4C99E8684C5413CCE9FB8BDA06 (void);
// 0x000007C1 System.Void ChartAndGraph.ChartItemTextBlend::.ctor()
extern void ChartItemTextBlend__ctor_m88335F931D17990F5161C6FD8E20F867ACDF4A62 (void);
// 0x000007C2 System.Single ChartAndGraph.ChartMagin::get_Left()
extern void ChartMagin_get_Left_m8CA3693F74477665EBC79AA26FF6FBFC4A00DF75 (void);
// 0x000007C3 System.Single ChartAndGraph.ChartMagin::get_Right()
extern void ChartMagin_get_Right_mD8761085F81F2AF1D421324FD92D98D146C52E2F (void);
// 0x000007C4 System.Single ChartAndGraph.ChartMagin::get_Top()
extern void ChartMagin_get_Top_m98A43BC03361E0A3E97E6DA5E85610206ABD3F10 (void);
// 0x000007C5 System.Single ChartAndGraph.ChartMagin::get_Bottom()
extern void ChartMagin_get_Bottom_m44249037E523659C0F58D7D5135FB1CE247D8B44 (void);
// 0x000007C6 System.Void ChartAndGraph.ChartMagin::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void ChartMagin__ctor_mEA93AF91A96ED580128F4E66524C2DF5DB4FAC5D (void);
// 0x000007C7 ChartAndGraph.ChartDynamicMaterial ChartAndGraph.ChartMaterialController::get_Materials()
extern void ChartMaterialController_get_Materials_m7EAB1C6A4854E373BA933345EA69308D019CC858 (void);
// 0x000007C8 System.Void ChartAndGraph.ChartMaterialController::set_Materials(ChartAndGraph.ChartDynamicMaterial)
extern void ChartMaterialController_set_Materials_m2B7C3E7A038C61336958F47508AAD1DD364C1051 (void);
// 0x000007C9 System.Void ChartAndGraph.ChartMaterialController::SetMaterial(UnityEngine.Material,UnityEngine.Material)
extern void ChartMaterialController_SetMaterial_m65A13F6A406ECC2FA670ADF3CA9FC0A3275427C5 (void);
// 0x000007CA System.Void ChartAndGraph.ChartMaterialController::WarnNullItem()
extern void ChartMaterialController_WarnNullItem_m69F7A0B523EC21E14DFDE60900A7ECE0E9CF0725 (void);
// 0x000007CB System.Void ChartAndGraph.ChartMaterialController::Start()
extern void ChartMaterialController_Start_m386114D5E91BFCD5FAE539C42981FFB10CF29D28 (void);
// 0x000007CC System.Int32 ChartAndGraph.ChartMaterialController::BaseColor()
extern void ChartMaterialController_BaseColor_m4B8E58F3ABF69566AEA9924A70CE06E93DD676CD (void);
// 0x000007CD System.Int32 ChartAndGraph.ChartMaterialController::Combine()
extern void ChartMaterialController_Combine_mE8583C0AEEA0C6D56432180AF81580C084BDAD73 (void);
// 0x000007CE System.Void ChartAndGraph.ChartMaterialController::TriggerOn()
extern void ChartMaterialController_TriggerOn_m07B21E0266927C4A1DDEE309A0ADC1B2A6856D2B (void);
// 0x000007CF System.Void ChartAndGraph.ChartMaterialController::TriggerOff()
extern void ChartMaterialController_TriggerOff_m2AE349CF58485061C1DCE395FF13CAC6F1451AC8 (void);
// 0x000007D0 System.Void ChartAndGraph.ChartMaterialController::OnMouseEnter()
extern void ChartMaterialController_OnMouseEnter_m4DFC3C2F6D1C343EB84730D59D62B80B461B9DA7 (void);
// 0x000007D1 System.Void ChartAndGraph.ChartMaterialController::OnMouseExit()
extern void ChartMaterialController_OnMouseExit_mAAFA7E23180D9047C4CBAA85CDCB738AF3CF4FC3 (void);
// 0x000007D2 System.Void ChartAndGraph.ChartMaterialController::OnMouseDown()
extern void ChartMaterialController_OnMouseDown_m2A331BC2A53E85FC231D7A874499A56499F6F533 (void);
// 0x000007D3 System.Void ChartAndGraph.ChartMaterialController::OnMouseUp()
extern void ChartMaterialController_OnMouseUp_m0F86B511344328B6E45587F58673F825743A91BE (void);
// 0x000007D4 System.Void ChartAndGraph.ChartMaterialController::Update()
extern void ChartMaterialController_Update_m950BF825350A09416A274A6A8983752829482A54 (void);
// 0x000007D5 UnityEngine.Color ChartAndGraph.ChartMaterialController::GetColorCombine(UnityEngine.Material)
extern void ChartMaterialController_GetColorCombine_mA3E9B5F8647844E6B7A5225CAB1718074508664A (void);
// 0x000007D6 System.Void ChartAndGraph.ChartMaterialController::SetColorCombine(UnityEngine.Material,UnityEngine.Color)
extern void ChartMaterialController_SetColorCombine_m980339BCC56F6ACB14773FEB7BB7565BD3C94EEF (void);
// 0x000007D7 System.Void ChartAndGraph.ChartMaterialController::SetRendererColor(UnityEngine.Color)
extern void ChartMaterialController_SetRendererColor_m1A3F4B95F49DDA77039BE038AAACFE09588F1C70 (void);
// 0x000007D8 System.Void ChartAndGraph.ChartMaterialController::SetColor(UnityEngine.Color)
extern void ChartMaterialController_SetColor_mABF0F402FA0B9A141F37DDC238FABA28B1DCD25C (void);
// 0x000007D9 System.Void ChartAndGraph.ChartMaterialController::OnDestroy()
extern void ChartMaterialController_OnDestroy_m34AAE9210F8F7A6C6F73499C97CFFD3F49823E1F (void);
// 0x000007DA System.Void ChartAndGraph.ChartMaterialController::Refresh()
extern void ChartMaterialController_Refresh_m824EF89EC19EC5A5E9BAF1EEAB11FBCE8F060167 (void);
// 0x000007DB System.Void ChartAndGraph.ChartMaterialController::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void ChartMaterialController_OnPointerEnter_m9470E5C6A0614561F054C72405D51D2B84A4101E (void);
// 0x000007DC System.Void ChartAndGraph.ChartMaterialController::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void ChartMaterialController_OnPointerExit_m30AE6EE053B099B2572A1F1EA55AA200C020F9B6 (void);
// 0x000007DD System.Void ChartAndGraph.ChartMaterialController::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void ChartMaterialController_OnPointerDown_m5A2933134BA392EA267DA288C789A519AC55E56A (void);
// 0x000007DE System.Void ChartAndGraph.ChartMaterialController::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void ChartMaterialController_OnPointerUp_mFC83BD22D26BDDBF8855DF1107B4EB74F0ED1533 (void);
// 0x000007DF System.Void ChartAndGraph.ChartMaterialController::.ctor()
extern void ChartMaterialController__ctor_m2B0DEC5EE987CF6D0CADF1602FD76C91E688D70E (void);
// 0x000007E0 System.Void ChartAndGraph.ChartMaterialController::.cctor()
extern void ChartMaterialController__cctor_m8FDAE74C2457745CA5BF9272A6C51C43BC551F11 (void);
// 0x000007E1 System.Void ChartAndGraph.ChartOrientedSize::.ctor()
extern void ChartOrientedSize__ctor_m98C035FAED28B3CE37A8F31CA1D2A8A248D418AE (void);
// 0x000007E2 System.Void ChartAndGraph.ChartOrientedSize::.ctor(System.Single)
extern void ChartOrientedSize__ctor_m56EDC540CC2E2022AC03BC84E8FB1CD8127962CE (void);
// 0x000007E3 System.Void ChartAndGraph.ChartOrientedSize::.ctor(System.Single,System.Single)
extern void ChartOrientedSize__ctor_mFD810D916A5E98DFC2B07834697E8FA86BB5E59C (void);
// 0x000007E4 System.Boolean ChartAndGraph.ChartOrientedSize::Equals(System.Object)
extern void ChartOrientedSize_Equals_m086B4EB35A945721AE66C5E1B402126C8D6727BA (void);
// 0x000007E5 System.Int32 ChartAndGraph.ChartOrientedSize::GetHashCode()
extern void ChartOrientedSize_GetHashCode_m1F9C3D954472412433C65756C25280F4CF3E01F9 (void);
// 0x000007E6 System.Object ChartAndGraph.ChartItem::get_TagData()
extern void ChartItem_get_TagData_mB309D7BE4D449337386A3F11CC5B1E5461622DA9 (void);
// 0x000007E7 System.Void ChartAndGraph.ChartItem::set_TagData(System.Object)
extern void ChartItem_set_TagData_m6EFCAD89C8BFBC7370A001CCDD1446856CEF193F (void);
// 0x000007E8 System.Void ChartAndGraph.ChartItem::.ctor()
extern void ChartItem__ctor_mA87CC828C72E990292B8C9FFBA71D9C7F68B9880 (void);
// 0x000007E9 System.Void ChartAndGraph.ChartItemNoDelete::.ctor()
extern void ChartItemNoDelete__ctor_mE4B12DDA0BFA41D20EA4403BE4EA7E118E12CC43 (void);
// 0x000007EA System.Void ChartAndGraph.DoubleRect::.ctor(System.Double,System.Double,System.Double,System.Double)
extern void DoubleRect__ctor_m1F984B01FA3C146F2E44687D00A5555DE0FB1788 (void);
// 0x000007EB System.String ChartAndGraph.DoubleRect::ToString()
extern void DoubleRect_ToString_m919CD47E3D21F81F9FBF3139392A42ADD1C14114 (void);
// 0x000007EC ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleRect::get_min()
extern void DoubleRect_get_min_m2DAC7EA9AF9EDCDC7C8AEE363E235019B55143BE (void);
// 0x000007ED ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleRect::get_max()
extern void DoubleRect_get_max_m9A803BD921EA1E942317AAA8E0D785412FEE7D1F (void);
// 0x000007EE System.Void ChartAndGraph.DoubleVector2::.ctor(UnityEngine.Vector2)
extern void DoubleVector2__ctor_mB07435411E7635EB6F0184A9CA371FDE8929F41A (void);
// 0x000007EF UnityEngine.Vector2 ChartAndGraph.DoubleVector2::ToVector2()
extern void DoubleVector2_ToVector2_m9F6043976FEBC38B285B63682A15F24AA6DF1393 (void);
// 0x000007F0 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector2::ToDoubleVector3()
extern void DoubleVector2_ToDoubleVector3_m89ED5CDEBCE31F542002701BEACBAE3002F8D9F7 (void);
// 0x000007F1 System.Void ChartAndGraph.DoubleVector2::.ctor(System.Double,System.Double)
extern void DoubleVector2__ctor_m49A34EB80CA262CEDACCA8A80EEC90973CBFFD0C (void);
// 0x000007F2 System.Double ChartAndGraph.DoubleVector3::get_Item(System.Int32)
extern void DoubleVector3_get_Item_mED8C18858451EA7064C2F46FA07E957C2644ED0D (void);
// 0x000007F3 System.Void ChartAndGraph.DoubleVector3::set_Item(System.Int32,System.Double)
extern void DoubleVector3_set_Item_m924522C3E1567A827FCD1CA497289495C98A0AED (void);
// 0x000007F4 UnityEngine.Vector2 ChartAndGraph.DoubleVector3::ToVector2()
extern void DoubleVector3_ToVector2_mCA8A08430A8A865321E15246F5843E2049991874 (void);
// 0x000007F5 UnityEngine.Vector3 ChartAndGraph.DoubleVector3::ToVector3()
extern void DoubleVector3_ToVector3_m536D90101A2050B94772A68DADC79B9EF6A76E95 (void);
// 0x000007F6 UnityEngine.Vector3 ChartAndGraph.DoubleVector3::ToVector4()
extern void DoubleVector3_ToVector4_m7E82F42A506E67800A6B46BAD15159908CD6F12C (void);
// 0x000007F7 ChartAndGraph.DoubleVector4 ChartAndGraph.DoubleVector3::ToDoubleVector4()
extern void DoubleVector3_ToDoubleVector4_m465CDD4A9232979D4AB4F55D349DC46C47A0D70C (void);
// 0x000007F8 ChartAndGraph.DoubleVector2 ChartAndGraph.DoubleVector3::ToDoubleVector2()
extern void DoubleVector3_ToDoubleVector2_mDF162BA352854D9C6A3CC52D9C504FEB4CA3F48F (void);
// 0x000007F9 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_normalized()
extern void DoubleVector3_get_normalized_m90F18F73D68A8B15215A499AF2C5639423485E5D (void);
// 0x000007FA System.Double ChartAndGraph.DoubleVector3::get_magnitude()
extern void DoubleVector3_get_magnitude_m5506849C53E5CEB754AE27A8BA2D19378010B921 (void);
// 0x000007FB System.Double ChartAndGraph.DoubleVector3::get_sqrMagnitude()
extern void DoubleVector3_get_sqrMagnitude_m88FEE9BC6FA36A803CB71EDC6EBA6AA1DBB88EFD (void);
// 0x000007FC ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_zero()
extern void DoubleVector3_get_zero_m4BA0DAEF6E6A5E374903A0D3E8938D8C5F1EA8F8 (void);
// 0x000007FD ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_one()
extern void DoubleVector3_get_one_m89706A68BF608B9F61D3B481D38C99077F4C2E95 (void);
// 0x000007FE ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_forward()
extern void DoubleVector3_get_forward_mA5EC8F787B4A77039CC8EF4D7530791FC6A54084 (void);
// 0x000007FF ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_back()
extern void DoubleVector3_get_back_m971380F28C21B3BC4CD39EFE1EB61E18E9C65ED5 (void);
// 0x00000800 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_up()
extern void DoubleVector3_get_up_m8E7B5BEB4E1F752F6FC39AC8449032CF0AA3BEAE (void);
// 0x00000801 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_down()
extern void DoubleVector3_get_down_m62F40AC480B022A58D0F1E4F8E1E8CDE4BD8E4E8 (void);
// 0x00000802 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_left()
extern void DoubleVector3_get_left_mFDF3F2E3E1462E22D86C717A53DD607B09E73AA6 (void);
// 0x00000803 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_right()
extern void DoubleVector3_get_right_m5AABED8AEBDF09F6A89403CE8E0E99F5C8C4A1DF (void);
// 0x00000804 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::get_fwd()
extern void DoubleVector3_get_fwd_m2B40844B46506803F63912F97875F840237E1A1F (void);
// 0x00000805 System.Void ChartAndGraph.DoubleVector3::.ctor(System.Double,System.Double,System.Double)
extern void DoubleVector3__ctor_mFB68746E3A4C58C0EFB00117654573FAA1486902 (void);
// 0x00000806 System.Void ChartAndGraph.DoubleVector3::.ctor(System.Double,System.Double)
extern void DoubleVector3__ctor_m7E898AB882BD1B76DA5C242BC4C1D752EFC275C9 (void);
// 0x00000807 System.Void ChartAndGraph.DoubleVector3::.ctor(UnityEngine.Vector3)
extern void DoubleVector3__ctor_m64CA35E9656B1D9020E4D0006D2F9182C3C8332A (void);
// 0x00000808 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Lerp(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3,System.Double)
extern void DoubleVector3_Lerp_m77D44244DAB2F1BCE647C226FA8F9CD07B992C6E (void);
// 0x00000809 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::LerpUnclamped(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3,System.Double)
extern void DoubleVector3_LerpUnclamped_mE0B451B62BE86B2011B4A389038798622BEB3910 (void);
// 0x0000080A ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::MoveTowards(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3,System.Double)
extern void DoubleVector3_MoveTowards_mE5FAA9C8DE0BD2E6F283578CDDC4D6D887F12319 (void);
// 0x0000080B System.Void ChartAndGraph.DoubleVector3::Set(System.Double,System.Double,System.Double)
extern void DoubleVector3_Set_mDD93E64C2D83554A00D45AEDA8E53BA8EEC9A461 (void);
// 0x0000080C ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Scale(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Scale_mB6007A14B1B464F5303C7E2FC2AB39A838E37D30 (void);
// 0x0000080D System.Void ChartAndGraph.DoubleVector3::Scale(ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Scale_mDE9D2DCDC9D7E9781D938FC8796B5E924F0EF440 (void);
// 0x0000080E ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Cross(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Cross_m2DCA01528D7E66FE3D4282BD8EB4F4557F0DEAD1 (void);
// 0x0000080F System.Int32 ChartAndGraph.DoubleVector3::GetHashCode()
extern void DoubleVector3_GetHashCode_m6B0B3D669BFEF329F363FACBABF80EACCC032811 (void);
// 0x00000810 System.Boolean ChartAndGraph.DoubleVector3::Equals(System.Object)
extern void DoubleVector3_Equals_m5C3BB2A3751484CE3672E5F49E9C554E9735D32C (void);
// 0x00000811 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Reflect(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Reflect_mA6272EFD7A04C90F9B19AFDADDDF1CBDD4E3D4DB (void);
// 0x00000812 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Normalize(ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Normalize_mEADC1A8785C72FB3EA37951B851CBFCF6D71EFEF (void);
// 0x00000813 System.Void ChartAndGraph.DoubleVector3::Normalize()
extern void DoubleVector3_Normalize_m6BAE2E860F8AF017BA5FA679637F17A5316186A8 (void);
// 0x00000814 System.Double ChartAndGraph.DoubleVector3::Dot(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Dot_m9C5D7B990969624D2575EB1CC92279FC95F52C86 (void);
// 0x00000815 System.Double ChartAndGraph.DoubleVector3::Distance(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Distance_m4FDA1C1EFE1C2011F69CC9937B550652B412F222 (void);
// 0x00000816 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::ClampMagnitude(ChartAndGraph.DoubleVector3,System.Double)
extern void DoubleVector3_ClampMagnitude_mB83103B06A4F891F1FE1D78FD553EF140E279A73 (void);
// 0x00000817 System.Double ChartAndGraph.DoubleVector3::Magnitude(ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Magnitude_m2367A75F8FF05DD750C6433929590962F381A29F (void);
// 0x00000818 System.Double ChartAndGraph.DoubleVector3::SqrMagnitude(ChartAndGraph.DoubleVector3)
extern void DoubleVector3_SqrMagnitude_m59ECB08B990EA531C4BF0D8D7415CDDA1384C767 (void);
// 0x00000819 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Min(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Min_m8A293E1EC95DDA10E7062878006594705DF96F2F (void);
// 0x0000081A ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::Max(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_Max_m8229C769E26645D5B1AEEA06EB446463FB0EBF85 (void);
// 0x0000081B ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::op_Addition(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_op_Addition_m33076677BD53CD9EAFB6593C813E8B9690789FAA (void);
// 0x0000081C ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::op_Subtraction(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_op_Subtraction_mDD74D64CAFD7EFE78E369115C4A566E01DBFCB13 (void);
// 0x0000081D ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::op_UnaryNegation(ChartAndGraph.DoubleVector3)
extern void DoubleVector3_op_UnaryNegation_mF11C867369B7D7DC7160AA0519878902DF6761E9 (void);
// 0x0000081E ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::op_Multiply(ChartAndGraph.DoubleVector3,System.Double)
extern void DoubleVector3_op_Multiply_m9285A5D4A034C4AF15DF67493B5D84E799366A8E (void);
// 0x0000081F ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::op_Multiply(System.Double,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_op_Multiply_m93BFC6CE511BA9CEE95BB59BAB48BDD7063A71DB (void);
// 0x00000820 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector3::op_Division(ChartAndGraph.DoubleVector3,System.Double)
extern void DoubleVector3_op_Division_m579F6EEE6580CE5B26C327845F66C12A3F98B279 (void);
// 0x00000821 System.Boolean ChartAndGraph.DoubleVector3::op_Equality(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_op_Equality_m086C68AA3377E5948901E2E2905842C2504C02B1 (void);
// 0x00000822 System.Boolean ChartAndGraph.DoubleVector3::op_Inequality(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void DoubleVector3_op_Inequality_m20100A2A0E8E9D59AE702050EB723F5BCEB6F0A2 (void);
// 0x00000823 System.String ChartAndGraph.DoubleVector3::ToString()
extern void DoubleVector3_ToString_mA764FD3E053E52D53B2B368E410DE1AE2544211B (void);
// 0x00000824 System.String ChartAndGraph.DoubleVector3::ToString(System.String)
extern void DoubleVector3_ToString_m4D85420392458F4FC660D1F6EAA01DAB3B0001F9 (void);
// 0x00000825 System.Void ChartAndGraph.DoubleVector4::.ctor(System.Double,System.Double,System.Double,System.Double)
extern void DoubleVector4__ctor_m1A8D3B2BCCF3B9F09BAA42BC481A1ED0B6E1F58D (void);
// 0x00000826 UnityEngine.Vector4 ChartAndGraph.DoubleVector4::ToVector4()
extern void DoubleVector4_ToVector4_m15954E35D363A12FE2A653263AE9AF726E3BC7FE (void);
// 0x00000827 UnityEngine.Vector3 ChartAndGraph.DoubleVector4::ToVector3()
extern void DoubleVector4_ToVector3_m89062AE3A11AF7CE2A02B01A75413B4401AC42C0 (void);
// 0x00000828 ChartAndGraph.DoubleVector3 ChartAndGraph.DoubleVector4::ToDoubleVector3()
extern void DoubleVector4_ToDoubleVector3_mE4450D748956E1BF8E4315E2D3658C2B1687C581 (void);
// 0x00000829 System.Void ChartAndGraph.GameObjectPool`1::RecycleObject(T)
// 0x0000082A T ChartAndGraph.GameObjectPool`1::TakeObject()
// 0x0000082B System.Void ChartAndGraph.GameObjectPool`1::DestoryAll()
// 0x0000082C System.Void ChartAndGraph.GameObjectPool`1::DeactivateObjects()
// 0x0000082D System.Void ChartAndGraph.GameObjectPool`1::.ctor()
// 0x0000082E System.Void ChartAndGraph.CustomChartPointer::Awake()
extern void CustomChartPointer_Awake_m052D5994DDD4D359FB7777FAC690F3AF39A13BEF (void);
// 0x0000082F System.Void ChartAndGraph.CustomChartPointer::UpdateGeometry()
extern void CustomChartPointer_UpdateGeometry_m2DAE70E5D1B8CAEA78122FB0A4330F38F3A73B7E (void);
// 0x00000830 System.Void ChartAndGraph.CustomChartPointer::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void CustomChartPointer_OnPointerClick_m265D470D918F34E39A15BB8ECAB2B9F2D65B778D (void);
// 0x00000831 System.Void ChartAndGraph.CustomChartPointer::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void CustomChartPointer_OnPointerDown_m3D27152BC8B90AC415D82AF4959F9A71893E4291 (void);
// 0x00000832 System.Void ChartAndGraph.CustomChartPointer::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void CustomChartPointer_OnPointerEnter_m36598D981FF59CDC8782271D11CBBBCF91E3A545 (void);
// 0x00000833 System.Void ChartAndGraph.CustomChartPointer::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void CustomChartPointer_OnPointerExit_m03DE6D6399EE4555B3AE8096D74C46A66A49A7C8 (void);
// 0x00000834 System.Void ChartAndGraph.CustomChartPointer::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void CustomChartPointer_OnPointerUp_m9D4D817FF7647F21B8CAC99B69FDA2A226612B8D (void);
// 0x00000835 System.Boolean ChartAndGraph.CustomChartPointer::Raycast(UnityEngine.Vector2,UnityEngine.Camera)
extern void CustomChartPointer_Raycast_m26C8BF8276F6D16A0A8A07F96F9B4351D16E140F (void);
// 0x00000836 System.Void ChartAndGraph.CustomChartPointer::LateUpdate()
extern void CustomChartPointer_LateUpdate_m06D699311DBFF394FD7B1790385C949B590BDADC (void);
// 0x00000837 System.Void ChartAndGraph.CustomChartPointer::.ctor()
extern void CustomChartPointer__ctor_mD5F5B26170AE9D0F6B829DF1250AC7B2351BF490 (void);
// 0x00000838 System.String ChartAndGraph.ChartDataSourceBaseCollection`1::get_ItemTypeName()
// 0x00000839 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::Add(T)
// 0x0000083A System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::SwitchPositions(System.String,System.String)
// 0x0000083B System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::SwitchPositions(System.Int32,System.Int32)
// 0x0000083C System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::Move(System.String,System.Int32)
// 0x0000083D System.Boolean ChartAndGraph.ChartDataSourceBaseCollection`1::TryGetIndexByName(System.String,System.Int32&)
// 0x0000083E System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::Insert(System.Int32,T)
// 0x0000083F System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::add_NameChanged(System.Action`2<System.String,ChartAndGraph.DataSource.IDataItem>)
// 0x00000840 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::remove_NameChanged(System.Action`2<System.String,ChartAndGraph.DataSource.IDataItem>)
// 0x00000841 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::add_OrderChanged(System.EventHandler)
// 0x00000842 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::remove_OrderChanged(System.EventHandler)
// 0x00000843 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::add_ItemRemoved(System.Action`1<T>)
// 0x00000844 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::remove_ItemRemoved(System.Action`1<T>)
// 0x00000845 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::add_ItemsReplaced(System.Action`4<System.String,System.Int32,System.String,System.Int32>)
// 0x00000846 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::remove_ItemsReplaced(System.Action`4<System.String,System.Int32,System.String,System.Int32>)
// 0x00000847 T ChartAndGraph.ChartDataSourceBaseCollection`1::get_Item(System.Int32)
// 0x00000848 T ChartAndGraph.ChartDataSourceBaseCollection`1::get_Item(System.String)
// 0x00000849 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::NameChangedHandler(System.String,ChartAndGraph.DataSource.IDataItem)
// 0x0000084A System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::Clear()
// 0x0000084B System.Boolean ChartAndGraph.ChartDataSourceBaseCollection`1::Contains(T)
// 0x0000084C System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::CopyTo(T[],System.Int32)
// 0x0000084D System.Int32 ChartAndGraph.ChartDataSourceBaseCollection`1::get_Count()
// 0x0000084E System.Boolean ChartAndGraph.ChartDataSourceBaseCollection`1::get_IsReadOnly()
// 0x0000084F System.Boolean ChartAndGraph.ChartDataSourceBaseCollection`1::Remove(T)
// 0x00000850 System.Collections.Generic.IEnumerator`1<T> ChartAndGraph.ChartDataSourceBaseCollection`1::GetEnumerator()
// 0x00000851 System.Collections.IEnumerator ChartAndGraph.ChartDataSourceBaseCollection`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000852 System.Void ChartAndGraph.ChartDataSourceBaseCollection`1::.ctor()
// 0x00000853 System.Void ChartAndGraph.ChartSparseDataSource::.ctor()
extern void ChartSparseDataSource__ctor_m5E17A577E752319F0CE300F62A9B376BCC207474 (void);
// 0x00000854 System.Void ChartAndGraph.ChartSparseDataSource::MColumns_ItemsReplaced(System.String,System.Int32,System.String,System.Int32)
extern void ChartSparseDataSource_MColumns_ItemsReplaced_mB60609E59B1DFBF54CE25B73B6BFAD42CAC5231D (void);
// 0x00000855 System.Void ChartAndGraph.ChartSparseDataSource::MRows_NameChanged(System.String,ChartAndGraph.DataSource.IDataItem)
extern void ChartSparseDataSource_MRows_NameChanged_m8023AE635F2E9F2CFF0F3110E3C7ABBEB4E6DE4C (void);
// 0x00000856 System.Void ChartAndGraph.ChartSparseDataSource::MColumns_NameChanged(System.String,ChartAndGraph.DataSource.IDataItem)
extern void ChartSparseDataSource_MColumns_NameChanged_m404ACDCA6662047A945517DD2B378C31BEB98911 (void);
// 0x00000857 System.Boolean ChartAndGraph.ChartSparseDataSource::get_SuspendEvents()
extern void ChartSparseDataSource_get_SuspendEvents_mD9C19B139AA3ACACAF5225680218715C53F8824B (void);
// 0x00000858 System.Void ChartAndGraph.ChartSparseDataSource::set_SuspendEvents(System.Boolean)
extern void ChartSparseDataSource_set_SuspendEvents_mA143F18FB951C3BADAC9CECC2714F1AACE315470 (void);
// 0x00000859 ChartAndGraph.DataSource.ChartColumnCollection ChartAndGraph.ChartSparseDataSource::get_Columns()
extern void ChartSparseDataSource_get_Columns_m2CF71F7D09116A06159513F6E25E7F1A9E9F7F94 (void);
// 0x0000085A ChartAndGraph.DataSource.ChartRowCollection ChartAndGraph.ChartSparseDataSource::get_Rows()
extern void ChartSparseDataSource_get_Rows_m587C7FE2A232468EAAD75680206636B8E4AFB959 (void);
// 0x0000085B System.Void ChartAndGraph.ChartSparseDataSource::OrderChanged(System.Object,System.EventArgs)
extern void ChartSparseDataSource_OrderChanged_m71BF74537D1BB2488BE75AF099B0EBFC56275DC8 (void);
// 0x0000085C System.Void ChartAndGraph.ChartSparseDataSource::FindMinMaxValue()
extern void ChartSparseDataSource_FindMinMaxValue_m746CAB84E6F22F442D6EFAF8593CCE93261BEAE2 (void);
// 0x0000085D System.Void ChartAndGraph.ChartSparseDataSource::Clear()
extern void ChartSparseDataSource_Clear_m4C5CD86FEB212BA9DB402B730A84FAD2982CF690 (void);
// 0x0000085E System.Void ChartAndGraph.ChartSparseDataSource::PrepareRawData()
extern void ChartSparseDataSource_PrepareRawData_mF1E8B75B50DCAD74CCD2D8E49C00CA83FD852319 (void);
// 0x0000085F System.Boolean ChartAndGraph.ChartSparseDataSource::HasZeroItems()
extern void ChartSparseDataSource_HasZeroItems_m54604D47DE9B10A3D8B64BA9E810EEBA266404BA (void);
// 0x00000860 System.Nullable`1<System.Double> ChartAndGraph.ChartSparseDataSource::getRawMaxValue()
extern void ChartSparseDataSource_getRawMaxValue_mA44A0F02039FF4515A440D1BDC21033C33F4773D (void);
// 0x00000861 System.Nullable`1<System.Double> ChartAndGraph.ChartSparseDataSource::getRawMinValue()
extern void ChartSparseDataSource_getRawMinValue_m0714C0882E54D9054B5587FA241C4562B59C59B5 (void);
// 0x00000862 System.Void ChartAndGraph.ChartSparseDataSource::ItemRemoved(ChartAndGraph.DataSource.IDataItem)
extern void ChartSparseDataSource_ItemRemoved_m0A443FC378C57DB4A0F4A67D1F2FE84AD3DF3709 (void);
// 0x00000863 System.Void ChartAndGraph.ChartSparseDataSource::Rows_ItemRemoved(ChartAndGraph.DataSource.ChartDataRow)
extern void ChartSparseDataSource_Rows_ItemRemoved_mC0E30E1A3D4BFA1B72EF486A1C4278F1D501BE59 (void);
// 0x00000864 System.Void ChartAndGraph.ChartSparseDataSource::Columns_ItemRemoved(ChartAndGraph.DataSource.ChartDataColumn)
extern void ChartSparseDataSource_Columns_ItemRemoved_m4A12626B6748F919F0D83557B14AA623D25BCDBB (void);
// 0x00000865 System.Void ChartAndGraph.ChartSparseDataSource::EnsureRawData()
extern void ChartSparseDataSource_EnsureRawData_mBA98C75670E5993115C984D47FAD367D59ADC134 (void);
// 0x00000866 System.Double[0...,0...] ChartAndGraph.ChartSparseDataSource::getRawData()
extern void ChartSparseDataSource_getRawData_mB2B3AA133EE32E6859090D0577C60A38B25205BA (void);
// 0x00000867 System.Boolean ChartAndGraph.ChartSparseDataSource::VerifyMinMaxValue(ChartAndGraph.ChartSparseDataSource/KeyElement,System.Double)
extern void ChartSparseDataSource_VerifyMinMaxValue_mB9A6B84E0177AF7BFC38A55FA5B416C00258128E (void);
// 0x00000868 System.Void ChartAndGraph.ChartSparseDataSource::InnerSetValue(ChartAndGraph.DataSource.ChartDataColumn,ChartAndGraph.DataSource.ChartDataRow,System.Double)
extern void ChartSparseDataSource_InnerSetValue_m1D9FDE998CCA943E5D9DEEECCA276D04F20C67E5 (void);
// 0x00000869 System.Double ChartAndGraph.ChartSparseDataSource::InnerGetValue(ChartAndGraph.DataSource.ChartDataColumn,ChartAndGraph.DataSource.ChartDataRow)
extern void ChartSparseDataSource_InnerGetValue_mB4440C8CD80E98FD7E98AC71D86E858754AC744C (void);
// 0x0000086A System.Double ChartAndGraph.ChartSparseDataSource::GetValue(System.String,System.String)
extern void ChartSparseDataSource_GetValue_m5A54768FB6583676D241FD7FEFAEE87E0D4B003E (void);
// 0x0000086B System.Double ChartAndGraph.ChartSparseDataSource::GetValue(System.String,System.Int32)
extern void ChartSparseDataSource_GetValue_mCFAAAC68FF595BEE04886E97168D223D292F2F50 (void);
// 0x0000086C System.Double ChartAndGraph.ChartSparseDataSource::GetValue(System.Int32,System.Int32)
extern void ChartSparseDataSource_GetValue_m3814BE95B7AD48D97B333C0B6C0DD9640D81B1DB (void);
// 0x0000086D System.Void ChartAndGraph.ChartSparseDataSource::AddLabel(System.String,System.Int32,System.String)
extern void ChartSparseDataSource_AddLabel_mE5212C0736692290BAE152CC533AB9A216F02681 (void);
// 0x0000086E System.Void ChartAndGraph.ChartSparseDataSource::SetValue(System.String,System.String,System.Double)
extern void ChartSparseDataSource_SetValue_m2867624C3DFCE779EA530B27C353A2E9AB440B8C (void);
// 0x0000086F System.Void ChartAndGraph.ChartSparseDataSource::SetValue(System.String,System.Int32,System.Double)
extern void ChartSparseDataSource_SetValue_m59518872614BD12DF9CDEF99AF167A7ED2BC7B7C (void);
// 0x00000870 System.Void ChartAndGraph.ChartSparseDataSource::SetValue(System.Int32,System.Int32,System.Double)
extern void ChartSparseDataSource_SetValue_mA8FF4E271E839E7C6BF41613754472DE8B74FFE0 (void);
// 0x00000871 System.Void ChartAndGraph.ChartSparseDataSource/KeyElement::.ctor(ChartAndGraph.DataSource.ChartDataRow,ChartAndGraph.DataSource.ChartDataColumn)
extern void KeyElement__ctor_mF495D25615988DEAF9D93439848D890FB358E5FA (void);
// 0x00000872 System.Boolean ChartAndGraph.ChartSparseDataSource/KeyElement::IsIn(ChartAndGraph.DataSource.IDataItem)
extern void KeyElement_IsIn_mB96974CD61E546BD694B850A130C84598FCAE0CE (void);
// 0x00000873 System.Boolean ChartAndGraph.ChartSparseDataSource/KeyElement::IsInRow(ChartAndGraph.DataSource.ChartDataRow)
extern void KeyElement_IsInRow_mDB9A5D4E921914DA9615454A3ADF273EB4B0CB4A (void);
// 0x00000874 System.Boolean ChartAndGraph.ChartSparseDataSource/KeyElement::IsInColumn(ChartAndGraph.DataSource.ChartDataColumn)
extern void KeyElement_IsInColumn_m37197E408CAD2E333AC1979A6FBE45DBCE5988E9 (void);
// 0x00000875 System.Boolean ChartAndGraph.ChartSparseDataSource/KeyElement::op_Equality(ChartAndGraph.ChartSparseDataSource/KeyElement,ChartAndGraph.ChartSparseDataSource/KeyElement)
extern void KeyElement_op_Equality_mB13C2C5C98BCD00182598A8061509860E4BFE523 (void);
// 0x00000876 System.Boolean ChartAndGraph.ChartSparseDataSource/KeyElement::op_Inequality(ChartAndGraph.ChartSparseDataSource/KeyElement,ChartAndGraph.ChartSparseDataSource/KeyElement)
extern void KeyElement_op_Inequality_m8BE683A6D0114E38378A9E9D3CC3CD149048DA84 (void);
// 0x00000877 System.Boolean ChartAndGraph.ChartSparseDataSource/KeyElement::Equals(System.Object)
extern void KeyElement_Equals_mB8633937E6EAD804F05153CFB62D57457E59F27A (void);
// 0x00000878 System.Int32 ChartAndGraph.ChartSparseDataSource/KeyElement::GetHashCode()
extern void KeyElement_GetHashCode_m68EFF0FDBC838A8B7E151CDED36F50DF849259A4 (void);
// 0x00000879 ChartAndGraph.DataSource.ChartDataRow ChartAndGraph.ChartSparseDataSource/KeyElement::get_Row()
extern void KeyElement_get_Row_m8E5B2536ABA3F7963BED8C00C37AD9DE246E4182 (void);
// 0x0000087A System.Void ChartAndGraph.ChartSparseDataSource/KeyElement::set_Row(ChartAndGraph.DataSource.ChartDataRow)
extern void KeyElement_set_Row_mA0B59C94D055895B004E338AD361C4228FC37AD0 (void);
// 0x0000087B ChartAndGraph.DataSource.ChartDataColumn ChartAndGraph.ChartSparseDataSource/KeyElement::get_Column()
extern void KeyElement_get_Column_m414AFFA313E39B3AEBAAC5B8DEA3A2E483F69999 (void);
// 0x0000087C System.Void ChartAndGraph.ChartSparseDataSource/KeyElement::set_Column(ChartAndGraph.DataSource.ChartDataColumn)
extern void KeyElement_set_Column_mFD48761D666FCDB501D3C5C79F3FCA47DEDF6D8C (void);
// 0x0000087D System.Void ChartAndGraph.GraphChart::OnLineSelected(System.Object)
extern void GraphChart_OnLineSelected_mA5EAEC051274D304B96E03D7B0C454B1F9B7128F (void);
// 0x0000087E System.Void ChartAndGraph.GraphChart::OnLineHovered(System.Object)
extern void GraphChart_OnLineHovered_m8A7098A585466DFD806D740B095B4B9D78E9F82C (void);
// 0x0000087F System.Boolean ChartAndGraph.GraphChart::get_FitToContainer()
extern void GraphChart_get_FitToContainer_m77A4781E39E69CBCFD0FE3FE97B57D7F0F88C53E (void);
// 0x00000880 System.Void ChartAndGraph.GraphChart::set_FitToContainer(System.Boolean)
extern void GraphChart_set_FitToContainer_m4A01B3112B8800ECC6040D7721FCC708F8C803E8 (void);
// 0x00000881 System.Boolean ChartAndGraph.GraphChart::get_NegativeFill()
extern void GraphChart_get_NegativeFill_m442C189CA82D329BC3B48DFE74CCE06DFFA3B653 (void);
// 0x00000882 System.Void ChartAndGraph.GraphChart::set_NegativeFill(System.Boolean)
extern void GraphChart_set_NegativeFill_m74832D2F59983EFE8CDDA53EB8E0A9C6B6083C12 (void);
// 0x00000883 ChartAndGraph.ChartMagin ChartAndGraph.GraphChart::get_FitMargin()
extern void GraphChart_get_FitMargin_mC29E8E2E64D13E60CF79B14001A2FFFD856ADCF6 (void);
// 0x00000884 System.Void ChartAndGraph.GraphChart::set_FitMargin(ChartAndGraph.ChartMagin)
extern void GraphChart_set_FitMargin_m6C859E6588F76E141F4DBB786B6CFCC3E5A2D47D (void);
// 0x00000885 System.Boolean ChartAndGraph.GraphChart::get_EnableBetaOptimization()
extern void GraphChart_get_EnableBetaOptimization_mB9776E81B83F789993D0DD9947767416BCDB62FD (void);
// 0x00000886 System.Void ChartAndGraph.GraphChart::set_EnableBetaOptimization(System.Boolean)
extern void GraphChart_set_EnableBetaOptimization_mDBD21CC52BAB9F25579CB6740074BFB8901065D2 (void);
// 0x00000887 ChartAndGraph.ChartMagin ChartAndGraph.GraphChart::get_MarginLink()
extern void GraphChart_get_MarginLink_m006682A84831559AD0EBCE004FBF0EC8C3A060D3 (void);
// 0x00000888 System.Boolean ChartAndGraph.GraphChart::get_IsCanvas()
extern void GraphChart_get_IsCanvas_m13979112AA7DAC2B4F81DA942F2A12DE7C7C6C84 (void);
// 0x00000889 System.Boolean ChartAndGraph.GraphChart::get_SupportRealtimeGeneration()
extern void GraphChart_get_SupportRealtimeGeneration_m8039BBD28EBC86B41DCB0A7D4C3C460A02E14BA2 (void);
// 0x0000088A System.Void ChartAndGraph.GraphChart::CenterObject(UnityEngine.GameObject,UnityEngine.RectTransform)
extern void GraphChart_CenterObject_mC1E8E6AE1A4CD77ED6A6C806734D34F368D5743E (void);
// 0x0000088B ChartAndGraph.CanvasLines ChartAndGraph.GraphChart::CreateDataObject(ChartAndGraph.GraphData/CategoryData,UnityEngine.GameObject,System.Boolean)
extern void GraphChart_CreateDataObject_m4DBD420AEB8476674DFF1025954738D633E71862 (void);
// 0x0000088C System.Void ChartAndGraph.GraphChart::Update()
extern void GraphChart_Update_m6DDF3DDCFEFCBA8562C9A15E8DA71BCE395C6B7E (void);
// 0x0000088D System.Void ChartAndGraph.GraphChart::ClearChart()
extern void GraphChart_ClearChart_mF2AB0835EDDC8CF4CEB0F039D3F0B8BCED8B95D7 (void);
// 0x0000088E System.Void ChartAndGraph.GraphChart::ClearCache()
extern void GraphChart_ClearCache_m6C0EB9BE07411F6693C04FA5F4076D66FF62FA01 (void);
// 0x0000088F System.Double ChartAndGraph.GraphChart::GetCategoryDepth(System.String)
extern void GraphChart_GetCategoryDepth_m49347F83748E35EF8BFFC8CDD0294E3E95F5525A (void);
// 0x00000890 System.Double ChartAndGraph.GraphChart::AddRadius(System.Double,System.Double,System.Double,System.Double)
extern void GraphChart_AddRadius_mD10AD3E4F8CFCB45FC9C620E8ADA8AE5BA63F951 (void);
// 0x00000891 System.Void ChartAndGraph.GraphChart::ViewPortionChanged()
extern void GraphChart_ViewPortionChanged_mA161DCA5BA702C5FB7DDA65A588AB59757D8B3B8 (void);
// 0x00000892 System.Void ChartAndGraph.GraphChart::GenerateRealtime()
extern void GraphChart_GenerateRealtime_m5F944C04426B29D8DF126D4F1D48F85472C14D9C (void);
// 0x00000893 System.Boolean ChartAndGraph.GraphChart::get_ShouldFitCanvas()
extern void GraphChart_get_ShouldFitCanvas_mAEE877B4DF762551CF746E62669E62ADA5D505FA (void);
// 0x00000894 ChartAndGraph.AnyChart/FitType ChartAndGraph.GraphChart::get_FitAspectCanvas()
extern void GraphChart_get_FitAspectCanvas_m5521185E681B053AC21AFF9F520ACE2AF4019BD4 (void);
// 0x00000895 System.Void ChartAndGraph.GraphChart::InternalGenerateChart()
extern void GraphChart_InternalGenerateChart_m2FE442227DAE4872202E848D0431E97C3EB75882 (void);
// 0x00000896 System.Void ChartAndGraph.GraphChart::Dots_Leave(System.String)
extern void GraphChart_Dots_Leave_m3BBE3E29DDD1A9A3859037BCAC75F82ED5F15108 (void);
// 0x00000897 System.Void ChartAndGraph.GraphChart::Lines_Leave(System.String)
extern void GraphChart_Lines_Leave_mA053E690FD3A938A438E81484E0E5CBB78B541CD (void);
// 0x00000898 System.Void ChartAndGraph.GraphChart::Dots_Click(System.String,System.Int32,UnityEngine.Vector2)
extern void GraphChart_Dots_Click_m24253BD37D184EFE55B2A2DD4C881700518CA62A (void);
// 0x00000899 System.Void ChartAndGraph.GraphChart::Lines_Clicked(System.String,System.Int32,UnityEngine.Vector2)
extern void GraphChart_Lines_Clicked_mA96C530C8ABDF5F0327ADDFF29E84C9DCB051C50 (void);
// 0x0000089A System.Void ChartAndGraph.GraphChart::Lines_Hover(System.String,System.Int32,UnityEngine.Vector2)
extern void GraphChart_Lines_Hover_m74A19A0F7FF1AF78F7A0C931BA8CEF1E0E7A2034 (void);
// 0x0000089B System.Void ChartAndGraph.GraphChart::Dots_Hover(System.String,System.Int32,UnityEngine.Vector2)
extern void GraphChart_Dots_Hover_m369B08B0BDB317C3BD668E3E50B33625E51BA4A5 (void);
// 0x0000089C System.Void ChartAndGraph.GraphChart::OnItemHoverted(System.Object)
extern void GraphChart_OnItemHoverted_mCDF48D540275EF8F9986F72C83A4D5128FB51EC6 (void);
// 0x0000089D System.Void ChartAndGraph.GraphChart::OnItemSelected(System.Object)
extern void GraphChart_OnItemSelected_m32393A801205F307B29910C921B0360C143FBBF2 (void);
// 0x0000089E System.Void ChartAndGraph.GraphChart::AddOccupiedCategory(System.String,System.String)
extern void GraphChart_AddOccupiedCategory_mBC04323715A1E22CDDD5BF5D0BB51EC261929401 (void);
// 0x0000089F System.Void ChartAndGraph.GraphChart::OnItemLeave(System.Object,System.String)
extern void GraphChart_OnItemLeave_m16D729ECD56E1B1CEB9802F7CAAE79E18BB4B4A8 (void);
// 0x000008A0 System.Void ChartAndGraph.GraphChart::SetAsMixedSeries()
extern void GraphChart_SetAsMixedSeries_mE9A526FAD07CCC6D4FBD3C80EC53B98870BC0701 (void);
// 0x000008A1 System.Void ChartAndGraph.GraphChart::.ctor()
extern void GraphChart__ctor_m3D127C0E87610FFC3230A47FC4FDF88F91089F11 (void);
// 0x000008A2 System.Boolean ChartAndGraph.GraphChart::<OnItemLeave>b__58_0(System.String)
extern void GraphChart_U3COnItemLeaveU3Eb__58_0_mA557CB16D6A2BA64937454DEDF3ADD5FECDB6FFF (void);
// 0x000008A3 System.Void ChartAndGraph.GraphChart/CategoryObject::.ctor()
extern void CategoryObject__ctor_m4107A0C15093D3A07B5BE2C8F6D7351EA1EBF0B4 (void);
// 0x000008A4 System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::.ctor()
extern void U3CU3Ec__DisplayClass48_0__ctor_m7D7B5519A450BA1B4BE387755552F027BD238EF9 (void);
// 0x000008A5 System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::<InternalGenerateChart>b__4(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__4_m78F30EF8FDBD89708A5D922A5895833CF4CD1ED1 (void);
// 0x000008A6 System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::<InternalGenerateChart>b__5(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__5_m6B7E32379767DF99C996EE88971B339F86ACED70 (void);
// 0x000008A7 System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::<InternalGenerateChart>b__6()
extern void U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__6_mA6CB1B1216F1A5A290E591B519121609917AFA01 (void);
// 0x000008A8 System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::<InternalGenerateChart>b__0(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__0_mE8CBB50E46CC939E724174C96F8F980A961E49CE (void);
// 0x000008A9 System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::<InternalGenerateChart>b__1(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__1_m14DF60523098941CE43D5387F88AFAA60B89A2B8 (void);
// 0x000008AA System.Void ChartAndGraph.GraphChart/<>c__DisplayClass48_0::<InternalGenerateChart>b__2()
extern void U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__2_mDDFB6B19924C3AC9169C0352DEC4E1463C297681 (void);
// 0x000008AB System.Void ChartAndGraph.GraphChart/<>c::.cctor()
extern void U3CU3Ec__cctor_m0C68B0F02946F73B550AD2B9C17E6EF00E2E42DC (void);
// 0x000008AC System.Void ChartAndGraph.GraphChart/<>c::.ctor()
extern void U3CU3Ec__ctor_m4AD90B5A3A238FDE194C669B988F51F97109C5BA (void);
// 0x000008AD UnityEngine.Vector4 ChartAndGraph.GraphChart/<>c::<InternalGenerateChart>b__48_3(ChartAndGraph.DoubleVector3)
extern void U3CU3Ec_U3CInternalGenerateChartU3Eb__48_3_mEA87D6D36DF69129E35B8148FEE9A84E9FEC8319 (void);
// 0x000008AE System.Single ChartAndGraph.GraphChartBase::get_HeightRatio()
extern void GraphChartBase_get_HeightRatio_m957D24B07EC9DD15BE62DB27B0C5D09DA69871D7 (void);
// 0x000008AF System.Void ChartAndGraph.GraphChartBase::set_HeightRatio(System.Single)
extern void GraphChartBase_set_HeightRatio_m3215A2F82486DD9536A79D7A637E918576B165D6 (void);
// 0x000008B0 System.Single ChartAndGraph.GraphChartBase::get_WidthRatio()
extern void GraphChartBase_get_WidthRatio_mB72FEA76D5FC92F56FDA7E236808DDDF4917F66E (void);
// 0x000008B1 System.Void ChartAndGraph.GraphChartBase::set_WidthRatio(System.Single)
extern void GraphChartBase_set_WidthRatio_m1C659060B32F8AA80985121F1836CACB6F33867E (void);
// 0x000008B2 System.String ChartAndGraph.GraphChartBase::get_ItemFormat()
extern void GraphChartBase_get_ItemFormat_mE9B7C0621416387221E63DC07378F42AB57E8B07 (void);
// 0x000008B3 System.Void ChartAndGraph.GraphChartBase::set_ItemFormat(System.String)
extern void GraphChartBase_set_ItemFormat_mA6181A9070016622898F15F9304B88682035E650 (void);
// 0x000008B4 ChartAndGraph.GraphData ChartAndGraph.GraphChartBase::get_DataSource()
extern void GraphChartBase_get_DataSource_m43FA715097E55E3413866C6AECE8D80B3581B80F (void);
// 0x000008B5 ChartAndGraph.LegenedData ChartAndGraph.GraphChartBase::get_LegendInfo()
extern void GraphChartBase_get_LegendInfo_m8DA296AE5601DD8F15298FFDFD32D1C56C4BE6D6 (void);
// 0x000008B6 ChartAndGraph.IChartData ChartAndGraph.GraphChartBase::get_DataLink()
extern void GraphChartBase_get_DataLink_m474A46C8258AB257282F1187D516C8F741D6016F (void);
// 0x000008B7 System.Void ChartAndGraph.GraphChartBase::ClearCache()
// 0x000008B8 System.Void ChartAndGraph.GraphChartBase::HookEvents()
extern void GraphChartBase_HookEvents_m6A2262685F100982489EB51E3E9D1E1ACBAC9125 (void);
// 0x000008B9 System.Void ChartAndGraph.GraphChartBase::GraphChartBase_InternalViewPortionChanged(System.Object,System.EventArgs)
extern void GraphChartBase_GraphChartBase_InternalViewPortionChanged_mDD6D74CA30A30A28CD459B65A5B0F250C506E724 (void);
// 0x000008BA System.Void ChartAndGraph.GraphChartBase::ViewPortionChanged()
// 0x000008BB System.Void ChartAndGraph.GraphChartBase::GraphChartBase_InternalRealTimeDataChanged(System.Int32,System.String)
extern void GraphChartBase_GraphChartBase_InternalRealTimeDataChanged_mE3DC6C52EBD03E1FD6151916755E7D2A07A7C00B (void);
// 0x000008BC System.Void ChartAndGraph.GraphChartBase::ClearRealtimeIndexdata()
extern void GraphChartBase_ClearRealtimeIndexdata_m4D5590BB504B284A40A2AC38C36D8A2797D42F2B (void);
// 0x000008BD System.Void ChartAndGraph.GraphChartBase::Invalidate()
extern void GraphChartBase_Invalidate_m74DFAF53F1AB5260A488CF4D6C2A6F6F1EACD6BC (void);
// 0x000008BE System.Void ChartAndGraph.GraphChartBase::GraphChart_InternalDataChanged(System.Object,System.EventArgs)
extern void GraphChartBase_GraphChart_InternalDataChanged_m28446CC735B69D4E20DFA04E092298A3ACB2CF67 (void);
// 0x000008BF System.Void ChartAndGraph.GraphChartBase::Start()
extern void GraphChartBase_Start_m2481135A37D1A74D317A4C64280F593B9D8BEF99 (void);
// 0x000008C0 System.Void ChartAndGraph.GraphChartBase::OnValidate()
extern void GraphChartBase_OnValidate_m5498B442A550D684E7EB10F96EDD46DC9416C5C5 (void);
// 0x000008C1 System.Void ChartAndGraph.GraphChartBase::OnLabelSettingChanged()
extern void GraphChartBase_OnLabelSettingChanged_m5D96A0DC501B63DBBEEDB68A3A0B0B38DDAE0E69 (void);
// 0x000008C2 System.Void ChartAndGraph.GraphChartBase::OnAxisValuesChanged()
extern void GraphChartBase_OnAxisValuesChanged_mBDD8DBC4831DC2B27ABCFC9E5872FF85D70FEA64 (void);
// 0x000008C3 System.Void ChartAndGraph.GraphChartBase::OnLabelSettingsSet()
extern void GraphChartBase_OnLabelSettingsSet_mF890CA0E077E38B634E45456F83B7572DF7CBBF2 (void);
// 0x000008C4 ChartAndGraph.DoubleVector4 ChartAndGraph.GraphChartBase::TransformPoint(UnityEngine.Rect,UnityEngine.Vector3,ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2)
extern void GraphChartBase_TransformPoint_mAB83EEF6C8FDFEEF57359D07D1A0779B02EF70D8 (void);
// 0x000008C5 System.Void ChartAndGraph.GraphChartBase::Update()
extern void GraphChartBase_Update_mF5E975021EE660FD9F57A98546E916CDE2E2F7FD (void);
// 0x000008C6 System.Void ChartAndGraph.GraphChartBase::UpdateMinMax(ChartAndGraph.DoubleVector3,System.Double&,System.Double&,System.Double&,System.Double&)
extern void GraphChartBase_UpdateMinMax_m7F1CF952ED57CBF089D75C1B6CE3FFFF2A07991C (void);
// 0x000008C7 System.Single ChartAndGraph.GraphChartBase::GetScrollingRange(System.Int32)
extern void GraphChartBase_GetScrollingRange_m2724ECDF4A1688763A428DE4A3333C2DFC269216 (void);
// 0x000008C8 UnityEngine.Rect ChartAndGraph.GraphChartBase::CreateUvRect(UnityEngine.Rect,UnityEngine.Rect)
extern void GraphChartBase_CreateUvRect_m0D1370A12B08E7A767475CF6F58FAE80C77A686B (void);
// 0x000008C9 System.Int32 ChartAndGraph.GraphChartBase::ClipPoints(System.Collections.Generic.IList`1<ChartAndGraph.DoubleVector3>,System.Collections.Generic.List`1<ChartAndGraph.DoubleVector4>,UnityEngine.Rect&)
extern void GraphChartBase_ClipPoints_m751E589D50E95062CFD1B08AA5B24A46218CD038 (void);
// 0x000008CA System.Void ChartAndGraph.GraphChartBase::TransformPoints(System.Collections.Generic.IList`1<ChartAndGraph.DoubleVector3>,UnityEngine.Rect,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void GraphChartBase_TransformPoints_mDE04BEEB8BA94163362DCE5C220CB6A836D9D286 (void);
// 0x000008CB System.Boolean ChartAndGraph.GraphChartBase::TransformPoints(System.Collections.Generic.IList`1<ChartAndGraph.DoubleVector4>,System.Collections.Generic.List`1<UnityEngine.Vector4>,UnityEngine.Rect,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void GraphChartBase_TransformPoints_m2420E7CA118A3EB2F3C5DFB645397AADCAF5374A (void);
// 0x000008CC System.Void ChartAndGraph.GraphChartBase::ValidateProperties()
extern void GraphChartBase_ValidateProperties_m71D044C9EBF998D5E0B47D70A3831AF64A424CFB (void);
// 0x000008CD System.Boolean ChartAndGraph.GraphChartBase::get_SupportsCategoryLabels()
extern void GraphChartBase_get_SupportsCategoryLabels_m05ABE914F22CDEB867B0D605AB4A9AAB5745D7E6 (void);
// 0x000008CE System.Boolean ChartAndGraph.GraphChartBase::get_SupportsGroupLables()
extern void GraphChartBase_get_SupportsGroupLables_m256FCF4262E98F92FF9D3EB8DE9436D14A6EF06B (void);
// 0x000008CF System.Boolean ChartAndGraph.GraphChartBase::get_SupportsItemLabels()
extern void GraphChartBase_get_SupportsItemLabels_m3E7AD6DAF4446140E99C81D009FD95136C0E5504 (void);
// 0x000008D0 System.Single ChartAndGraph.GraphChartBase::get_TotalHeightLink()
extern void GraphChartBase_get_TotalHeightLink_mC54F8C16C54624F33A4647B630320DBA6A410D00 (void);
// 0x000008D1 System.Single ChartAndGraph.GraphChartBase::get_TotalWidthLink()
extern void GraphChartBase_get_TotalWidthLink_mB27C39FB0E7AE90FFE4256FB8CBDEBBFDA3063BA (void);
// 0x000008D2 System.Single ChartAndGraph.GraphChartBase::get_TotalDepthLink()
extern void GraphChartBase_get_TotalDepthLink_mB370917376EA9AE75AB1079F9CC7973E9021248D (void);
// 0x000008D3 System.Boolean ChartAndGraph.GraphChartBase::HasValues(ChartAndGraph.AxisBase)
extern void GraphChartBase_HasValues_m94F574C967CF53BC734765753C053F1BFC5AA740 (void);
// 0x000008D4 System.Double ChartAndGraph.GraphChartBase::MaxValue(ChartAndGraph.AxisBase)
extern void GraphChartBase_MaxValue_m9C8A3CD7F373D6544B7F5E65EB2EB7A401269E44 (void);
// 0x000008D5 System.Double ChartAndGraph.GraphChartBase::MinValue(ChartAndGraph.AxisBase)
extern void GraphChartBase_MinValue_m5C70463DA41B056DB0B7F39BA1F742D0243808B7 (void);
// 0x000008D6 System.Void ChartAndGraph.GraphChartBase::OnItemHoverted(System.Object)
extern void GraphChartBase_OnItemHoverted_m49F6C63952C47E98E3D0DFA4227C524C10E41B61 (void);
// 0x000008D7 System.String ChartAndGraph.GraphChartBase::FormatItem(System.Double,System.Double)
extern void GraphChartBase_FormatItem_mF34CC108CE45F5958B19EC2F6392B4999990CD62 (void);
// 0x000008D8 System.String ChartAndGraph.GraphChartBase::FormatItem(System.String,System.String)
extern void GraphChartBase_FormatItem_m7B1AE4BB60B92ACAC6410F84CE3449CFC3ED1895 (void);
// 0x000008D9 System.Void ChartAndGraph.GraphChartBase::FormatItem(System.Text.StringBuilder,System.String,System.String)
extern void GraphChartBase_FormatItem_mB60D6DD8CED22475BB73A99CEBE9F79275B732B9 (void);
// 0x000008DA System.Void ChartAndGraph.GraphChartBase::OnItemSelected(System.Object)
extern void GraphChartBase_OnItemSelected_m2EF711ACB930629E595D45F8F634D33A91D646AB (void);
// 0x000008DB System.Void ChartAndGraph.GraphChartBase::.ctor()
extern void GraphChartBase__ctor_m842DAF976E645C350244C61CB5EBD797262197E3 (void);
// 0x000008DC System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::.ctor(System.Int32,UnityEngine.Vector3,ChartAndGraph.DoubleVector2,System.Single,System.String,System.String,System.String)
extern void GraphEventArgs__ctor_m3BA1F38D93D6F358D9EFB9A58916CA53BACD8ACD (void);
// 0x000008DD System.Single ChartAndGraph.GraphChartBase/GraphEventArgs::get_Magnitude()
extern void GraphEventArgs_get_Magnitude_m069084EB44CF3477283C3C24DFE227E060139C67 (void);
// 0x000008DE System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_Magnitude(System.Single)
extern void GraphEventArgs_set_Magnitude_mE0A9AC6DF6CF9D6532021ED8F40958FE33612C27 (void);
// 0x000008DF System.Int32 ChartAndGraph.GraphChartBase/GraphEventArgs::get_Index()
extern void GraphEventArgs_get_Index_m6E3E1833F3106A2C697659849B4D4DBB3EA3A834 (void);
// 0x000008E0 System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_Index(System.Int32)
extern void GraphEventArgs_set_Index_m779BF98A0027FBF3AE214D4225424BE1465B3330 (void);
// 0x000008E1 System.String ChartAndGraph.GraphChartBase/GraphEventArgs::get_XString()
extern void GraphEventArgs_get_XString_mD8983AC95262E528C933388971991F0ED7FBDB04 (void);
// 0x000008E2 System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_XString(System.String)
extern void GraphEventArgs_set_XString_mBE4A7E79289C442618EFF15D4863A9A764735C27 (void);
// 0x000008E3 System.String ChartAndGraph.GraphChartBase/GraphEventArgs::get_YString()
extern void GraphEventArgs_get_YString_mD55762099B346C4F5F0DB6C742BFE5659DB94074 (void);
// 0x000008E4 System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_YString(System.String)
extern void GraphEventArgs_set_YString_m1E0A09FE20ABD9A34C0361EFF8E294D558475401 (void);
// 0x000008E5 UnityEngine.Vector3 ChartAndGraph.GraphChartBase/GraphEventArgs::get_Position()
extern void GraphEventArgs_get_Position_m07FE6D33F9802F166E94A69452252688832F68FE (void);
// 0x000008E6 System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_Position(UnityEngine.Vector3)
extern void GraphEventArgs_set_Position_m1E11A04C49BB3B49F43F5CC58A11A64234E47E73 (void);
// 0x000008E7 ChartAndGraph.DoubleVector2 ChartAndGraph.GraphChartBase/GraphEventArgs::get_Value()
extern void GraphEventArgs_get_Value_mE3ED67FD7BE31E9EEA4FA9F1C24B3AAEDB363055 (void);
// 0x000008E8 System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_Value(ChartAndGraph.DoubleVector2)
extern void GraphEventArgs_set_Value_m2AEA5B671D29625A88182451F4D8DE567FD9353D (void);
// 0x000008E9 System.String ChartAndGraph.GraphChartBase/GraphEventArgs::get_Category()
extern void GraphEventArgs_get_Category_m132FB8CA4D23BBD5A6597E4C180168227A203D4A (void);
// 0x000008EA System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_Category(System.String)
extern void GraphEventArgs_set_Category_m7F525C2F77FBD1DF02655BB29922637BC97CC37D (void);
// 0x000008EB System.String ChartAndGraph.GraphChartBase/GraphEventArgs::get_Group()
extern void GraphEventArgs_get_Group_mEDE877E6A0267C6C014F063601F3A95873209B3E (void);
// 0x000008EC System.Void ChartAndGraph.GraphChartBase/GraphEventArgs::set_Group(System.String)
extern void GraphEventArgs_set_Group_mAF49E4F10D93415BBDB3437597F4A6C03D7FD22A (void);
// 0x000008ED System.Void ChartAndGraph.GraphChartBase/GraphEvent::.ctor()
extern void GraphEvent__ctor_m9FB528548A4729C3BF13DC90967E1EC87F4A556B (void);
// 0x000008EE System.Void ChartAndGraph.GraphData::AddCategory3DGraph(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Double,ChartAndGraph.MaterialTiling,ChartAndGraph.FillPathGenerator,UnityEngine.Material,System.Boolean,UnityEngine.GameObject,UnityEngine.Material,System.Double,System.Double,System.Boolean,System.Int32)
extern void GraphData_AddCategory3DGraph_m70581959401067B2717B860FB97DBD2F6166D4F6 (void);
// 0x000008EF System.Void ChartAndGraph.GraphData::Set3DCategoryPrefabs(System.String,ChartAndGraph.PathGenerator,ChartAndGraph.FillPathGenerator,UnityEngine.GameObject)
extern void GraphData_Set3DCategoryPrefabs_mA26193C759E79F326CFC57B81A71C179D9811602 (void);
// 0x000008F0 System.Void ChartAndGraph.GraphData::Set3DCategoryDepth(System.String,System.Double)
extern void GraphData_Set3DCategoryDepth_m1A5139512BB2778D1452A3F8850B0B981337DD23 (void);
// 0x000008F1 System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabelRealtime(ChartAndGraph.GraphChartBase,System.String,System.DateTime,System.Double,System.Double,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabelRealtime_m3B7FA31C9D93ECCD189FB38A2E2CCE239C0AFFAD (void);
// 0x000008F2 System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabelRealtime(ChartAndGraph.GraphChartBase,System.String,System.Double,System.DateTime,System.Double,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabelRealtime_mB5E1619495590901C1E50A18E9219FDB93DAC2B7 (void);
// 0x000008F3 System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabelRealtime(ChartAndGraph.GraphChartBase,System.String,System.DateTime,System.DateTime,System.Double,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabelRealtime_m1ED01A75FBB5172795C369F56916F07D7DAA85B9 (void);
// 0x000008F4 System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabelRealtime(ChartAndGraph.GraphChartBase,System.String,System.Double,System.Double,System.Double,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabelRealtime_m68DD81F1B6057B6CD5784510070B9F37E92FA7D9 (void);
// 0x000008F5 System.Void ChartAndGraph.GraphData::AddPointToCategoryRealtime(System.String,System.DateTime,System.DateTime,System.Double,System.Double)
extern void GraphData_AddPointToCategoryRealtime_mD79C08438F5B4EC8ACE6010FF73D0F03F6BE6D16 (void);
// 0x000008F6 System.Void ChartAndGraph.GraphData::AddPointToCategoryRealtime(System.String,System.DateTime,System.Double,System.Double,System.Double)
extern void GraphData_AddPointToCategoryRealtime_m3EB3B005E65F3C0FC5404081A447750DE6D22773 (void);
// 0x000008F7 System.Void ChartAndGraph.GraphData::AddPointToCategoryRealtime(System.String,System.Double,System.DateTime,System.Double,System.Double)
extern void GraphData_AddPointToCategoryRealtime_m3CBB7BD8F8B9AE1A94D5CFAAD8DC3097D9215B4D (void);
// 0x000008F8 System.Void ChartAndGraph.GraphData::SetCategoryArray(System.String,ChartAndGraph.DoubleVector3[],System.Int32,System.Int32)
extern void GraphData_SetCategoryArray_mCCC0A3787E0F0505719B1CF3453EDC7D4DDCF6D8 (void);
// 0x000008F9 System.Void ChartAndGraph.GraphData::UpdateLastPointInCategoryRealtime(System.String,System.Double,System.Double,System.Double,System.Double)
extern void GraphData_UpdateLastPointInCategoryRealtime_m12632BF5D28A545E44D16488960DB933F28F6FD9 (void);
// 0x000008FA System.Void ChartAndGraph.GraphData::AddPointToCategoryRealtime(System.String,System.Double,System.Double,System.Double,System.Double)
extern void GraphData_AddPointToCategoryRealtime_m1A37640184DC9DED0B58E355082CDA5B140E96EE (void);
// 0x000008FB System.Void ChartAndGraph.GraphData::CheckExtended(System.Boolean&)
extern void GraphData_CheckExtended_m7C0A0A87C42041BF12DBC35521F7C844703CEB8A (void);
// 0x000008FC System.Boolean ChartAndGraph.GraphData::get_IsExtended()
extern void GraphData_get_IsExtended_mC20FE3C5C023703211AB7B935E96272F06EA0EFB (void);
// 0x000008FD System.Void ChartAndGraph.GraphData::ClearAndMakeBezierCurve(System.String)
extern void GraphData_ClearAndMakeBezierCurve_m3A32C3FB2279E688A852D9734D2C4C109B3658FD (void);
// 0x000008FE System.Void ChartAndGraph.GraphData::ClearAndMakeLinear(System.String)
extern void GraphData_ClearAndMakeLinear_m43270963D3A09786A39FA648B11D642BB28E3ED7 (void);
// 0x000008FF System.Void ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.add_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
extern void GraphData_ChartAndGraph_IInternalGraphData_add_InternalRealTimeDataChanged_mD6DBC6B761BC9CDDEAFA2586D623786008C846C9 (void);
// 0x00000900 System.Void ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.remove_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
extern void GraphData_ChartAndGraph_IInternalGraphData_remove_InternalRealTimeDataChanged_m5C43AB570712151B5B4C80FD7A0F3EF02E8A47F7 (void);
// 0x00000901 System.Void ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.add_InternalViewPortionChanged(System.EventHandler)
extern void GraphData_ChartAndGraph_IInternalGraphData_add_InternalViewPortionChanged_m44CBE4ABA3B7507847D37543C1364A1C1C1E4B45 (void);
// 0x00000902 System.Void ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.remove_InternalViewPortionChanged(System.EventHandler)
extern void GraphData_ChartAndGraph_IInternalGraphData_remove_InternalViewPortionChanged_mF2781C9217F5BDB8F358F78558042771FBA3050B (void);
// 0x00000903 System.Void ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.add_InternalDataChanged(System.EventHandler)
extern void GraphData_ChartAndGraph_IInternalGraphData_add_InternalDataChanged_mB0BAECFE09A2289538D382AA9B24CEBD99768166 (void);
// 0x00000904 System.Void ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.remove_InternalDataChanged(System.EventHandler)
extern void GraphData_ChartAndGraph_IInternalGraphData_remove_InternalDataChanged_mD1109D60445B2C1B65330095A039669D9ADEA79D (void);
// 0x00000905 System.Void ChartAndGraph.GraphData::RenameCategory(System.String,System.String)
extern void GraphData_RenameCategory_m7DBF7FC7F78B86D4C698BC4B9109096E1C5CBE23 (void);
// 0x00000906 System.Void ChartAndGraph.GraphData::AddCategory(System.String,UnityEngine.Material,System.Double,ChartAndGraph.MaterialTiling,UnityEngine.Material,System.Boolean,UnityEngine.Material,System.Double,System.Boolean)
extern void GraphData_AddCategory_mD4A0A2E61345771585742A78831B6086017D1C7D (void);
// 0x00000907 System.Boolean ChartAndGraph.GraphData::isCategoryEnabled(System.String)
extern void GraphData_isCategoryEnabled_m5C0419EBA253AEBEB8C257A75B07594075B15968 (void);
// 0x00000908 System.Void ChartAndGraph.GraphData::SetCategoryEnabled(System.String,System.Boolean)
extern void GraphData_SetCategoryEnabled_m36849B69303A51BF1CD1052D92A855920D067462 (void);
// 0x00000909 System.Object ChartAndGraph.GraphData::StoreCategory(System.String)
extern void GraphData_StoreCategory_m876D0AE4C4560B07AABC5430B1D6DA1D9EDE8BC2 (void);
// 0x0000090A System.Void ChartAndGraph.GraphData::RestoreCategory(System.String,System.Object)
extern void GraphData_RestoreCategory_m778A8F078A3CF88354A41A276EF20C8B5DDF1C2F (void);
// 0x0000090B System.Object[] ChartAndGraph.GraphData::StoreAllCategoriesinOrder()
extern void GraphData_StoreAllCategoriesinOrder_m22349780531047B1B532BA3C3643D758F4E779E4 (void);
// 0x0000090C System.Void ChartAndGraph.GraphData::ClearAndSetAllowNonFunctions(System.String,System.Boolean)
extern void GraphData_ClearAndSetAllowNonFunctions_mA08A0EB867DA6AF5819A054FECED77B54AB5CEDE (void);
// 0x0000090D System.Void ChartAndGraph.GraphData::Set2DCategoryPrefabs(System.String,ChartAndGraph.ChartItemEffect,ChartAndGraph.ChartItemEffect)
extern void GraphData_Set2DCategoryPrefabs_m953A7F757FC0B719C47F1E01BA7D73E9679161E2 (void);
// 0x0000090E System.Void ChartAndGraph.GraphData::AddInnerCategoryGraph(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Double,ChartAndGraph.MaterialTiling,ChartAndGraph.FillPathGenerator,UnityEngine.Material,System.Boolean,UnityEngine.GameObject,UnityEngine.Material,System.Double,System.Double,System.Boolean,System.Int32,UnityEngine.Vector2[])
extern void GraphData_AddInnerCategoryGraph_mA3F29EBD76D93899E44063AFF220CAAF353798DB (void);
// 0x0000090F System.Void ChartAndGraph.GraphData::SetInitialData(System.String,UnityEngine.Vector2[],System.Boolean)
extern void GraphData_SetInitialData_m37501EA9B2B451E3EB41CF2992454D58427D0536 (void);
// 0x00000910 System.Void ChartAndGraph.GraphData::SetCategoryLine(System.String,UnityEngine.Material,System.Double,ChartAndGraph.MaterialTiling)
extern void GraphData_SetCategoryLine_mBEC80CBB964D92A28C3A65DB202DE8F09DF92808 (void);
// 0x00000911 System.Boolean ChartAndGraph.GraphData::RemoveCategory(System.String)
extern void GraphData_RemoveCategory_mDB5A5A8F928AF771956F8669181255853E5AC813 (void);
// 0x00000912 System.Void ChartAndGraph.GraphData::SetCategoryPoint(System.String,UnityEngine.Material,System.Double)
extern void GraphData_SetCategoryPoint_m7457DF540DCA794789A87CDD354ECEB7B6629F9E (void);
// 0x00000913 System.Void ChartAndGraph.GraphData::SetCategoryFill(System.String,UnityEngine.Material,System.Boolean)
extern void GraphData_SetCategoryFill_mB8D1DE3BF95449BA4D4ED7251213C833A67B236F (void);
// 0x00000914 System.Void ChartAndGraph.GraphData::ClearCategory(System.String)
extern void GraphData_ClearCategory_m08E3039A3C8D9EEA14899B689875DE88EA4B7A35 (void);
// 0x00000915 System.Void ChartAndGraph.GraphData::AddPointToCategory(System.String,System.DateTime,System.DateTime,System.Double)
extern void GraphData_AddPointToCategory_mBA22B51C9B4C172C0669C1DD7B7A723C2F1C470D (void);
// 0x00000916 System.Boolean ChartAndGraph.GraphData::GetLastPoint(System.String,ChartAndGraph.DoubleVector3&)
extern void GraphData_GetLastPoint_m2421808CDE31B5BDD11C110C9F26854F735C1B72 (void);
// 0x00000917 ChartAndGraph.DoubleVector3 ChartAndGraph.GraphData::GetPoint(System.String,System.Int32)
extern void GraphData_GetPoint_m72D84A8834B33534679F653EB61A4C85A8A6C70F (void);
// 0x00000918 System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabel(ChartAndGraph.GraphChartBase,System.String,System.DateTime,System.Double,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabel_mB882CCF027B42C295ED404BD17632A0F21DB403D (void);
// 0x00000919 System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabel(ChartAndGraph.GraphChartBase,System.String,System.Double,System.DateTime,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabel_m3506AFD00A4A3D397CA02EA7400161657EB7DA94 (void);
// 0x0000091A System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabel(ChartAndGraph.GraphChartBase,System.String,System.DateTime,System.DateTime,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabel_m88A62F64E8F1FFB1905FD4389E20E65DF40A692D (void);
// 0x0000091B System.Void ChartAndGraph.GraphData::AddPointToCategoryWithLabel(ChartAndGraph.GraphChartBase,System.String,System.Double,System.Double,System.Double,System.String,System.String)
extern void GraphData_AddPointToCategoryWithLabel_m2373B3FF0C1B4409592A20D1DF96E065B4C8DC84 (void);
// 0x0000091C System.Void ChartAndGraph.GraphData::AddPointToCategory(System.String,System.DateTime,System.Double,System.Double)
extern void GraphData_AddPointToCategory_m121AE6E76D8B015D8B5F0AF4C7DDF8812AA93D7F (void);
// 0x0000091D System.Void ChartAndGraph.GraphData::AddPointToCategory(System.String,System.Double,System.DateTime,System.Double)
extern void GraphData_AddPointToCategory_m06BDFA3BC5133DE5FC51DD181BDC461CC449F736 (void);
// 0x0000091E System.Void ChartAndGraph.GraphData::SetCurveInitialPoint(System.String,System.DateTime,System.Double,System.Double)
extern void GraphData_SetCurveInitialPoint_m132ACAAE5CC51CE8CB0C4BEA13CC92ABDAB2683C (void);
// 0x0000091F System.Void ChartAndGraph.GraphData::SetCurveInitialPoint(System.String,System.DateTime,System.DateTime,System.Double)
extern void GraphData_SetCurveInitialPoint_mD153FF86C0A814E79249B4FCEECBED8B2DB18CB3 (void);
// 0x00000920 System.Void ChartAndGraph.GraphData::SetCurveInitialPoint(System.String,System.Double,System.DateTime,System.Double)
extern void GraphData_SetCurveInitialPoint_mB93CEC7BA75B4D5B15B6C31318272B9FD8B60838 (void);
// 0x00000921 System.Void ChartAndGraph.GraphData::SetCategoryViewOrder(System.String,System.Int32)
extern void GraphData_SetCategoryViewOrder_mF65EA457658B0665E727D896CA77A75DC567B7AD (void);
// 0x00000922 System.Void ChartAndGraph.GraphData::SetCurveInitialPoint(System.String,System.Double,System.Double,System.Double)
extern void GraphData_SetCurveInitialPoint_m43E707E1654DC57F626B228049618B2B53E73F93 (void);
// 0x00000923 System.Double ChartAndGraph.GraphData::min3(System.Double,System.Double,System.Double)
extern void GraphData_min3_mEFABA74DA9EED0B253278640101A93239871DDE9 (void);
// 0x00000924 System.Double ChartAndGraph.GraphData::max3(System.Double,System.Double,System.Double)
extern void GraphData_max3_mDAEE5DCED8454A6764286ED27C24A962DCC831F1 (void);
// 0x00000925 ChartAndGraph.DoubleVector2 ChartAndGraph.GraphData::max3(ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2)
extern void GraphData_max3_m1D1D77940480A82B11AF3920420DBD715D661AFC (void);
// 0x00000926 ChartAndGraph.DoubleVector2 ChartAndGraph.GraphData::min3(ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2)
extern void GraphData_min3_m1238E49AA6581D063E368C91878818DE78AD3D4B (void);
// 0x00000927 System.Void ChartAndGraph.GraphData::MakeCurveCategorySmoothCubic(System.String)
extern void GraphData_MakeCurveCategorySmoothCubic_mE38178CF37FF8E5F29FB7DEABCCA092AF1A9D696 (void);
// 0x00000928 System.Void ChartAndGraph.GraphData::MakeCurveCategorySmooth(System.String,System.Single)
extern void GraphData_MakeCurveCategorySmooth_m8EA020E1D7048B34F0CE7D1A94658E374C0FF25F (void);
// 0x00000929 System.Void ChartAndGraph.GraphData::AddLinearCurveToCategory(System.String,ChartAndGraph.DoubleVector2,System.Double)
extern void GraphData_AddLinearCurveToCategory_mF8DDE07DBEEBA6A80CFFB8ECA374773BB74CD55C (void);
// 0x0000092A System.Void ChartAndGraph.GraphData::AddCurveToCategory(System.String,ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2,ChartAndGraph.DoubleVector2,System.Double)
extern void GraphData_AddCurveToCategory_m76ACB774CC44D3C8C05CF58FFEA391F125A779F2 (void);
// 0x0000092B System.Void ChartAndGraph.GraphData::AddPointToCategory(System.String,System.Double,System.Double,System.Double)
extern void GraphData_AddPointToCategory_mC08C597F4BD2BA3FB2F464B54A3998D2B843C1F4 (void);
// 0x0000092C System.Double ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.GetMaxValue(System.Int32,System.Boolean)
extern void GraphData_ChartAndGraph_IInternalGraphData_GetMaxValue_m697330585899A3E2813A803D63DF9650BCFA959C (void);
// 0x0000092D System.Double ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.GetMinValue(System.Int32,System.Boolean)
extern void GraphData_ChartAndGraph_IInternalGraphData_GetMinValue_m4029C8A74863FCC0CC3066C627006316884FEF1C (void);
// 0x0000092E System.Void ChartAndGraph.GraphData::OnAfterDeserialize()
extern void GraphData_OnAfterDeserialize_m3FC368A4F5DC5C324AEA3FBDE6A0EFF39F8881C4 (void);
// 0x0000092F System.Void ChartAndGraph.GraphData::OnBeforeSerialize()
extern void GraphData_OnBeforeSerialize_mCEB9E6863D16C2BF3953B08B2D35F2720175EEBC (void);
// 0x00000930 System.Void ChartAndGraph.GraphData::AppendDatum(System.String,ChartAndGraph.MixedSeriesGenericValue)
extern void GraphData_AppendDatum_mD94C70B001ED3505AB66D8692D94C7B3DEFD9866 (void);
// 0x00000931 System.Void ChartAndGraph.GraphData::InnerClearCategory(System.String)
extern void GraphData_InnerClearCategory_m70AE35FCC2B215C219842CAB2349C027C42FF599 (void);
// 0x00000932 System.Boolean ChartAndGraph.GraphData::AddCategory(System.String,ChartAndGraph.BaseScrollableCategoryData)
extern void GraphData_AddCategory_mEE61FFBCFC0E07F391B309470D1187186B218F75 (void);
// 0x00000933 System.Void ChartAndGraph.GraphData::AppendDatum(System.String,System.Collections.Generic.IList`1<ChartAndGraph.MixedSeriesGenericValue>)
extern void GraphData_AppendDatum_m7EC586B99638452FA11A3A00D87EEC709F36C268 (void);
// 0x00000934 ChartAndGraph.BaseScrollableCategoryData ChartAndGraph.GraphData::GetDefaultCategory()
extern void GraphData_GetDefaultCategory_m636547F9BFB7683C393E6EDE60A96F2B2FF39A7B (void);
// 0x00000935 System.Int32 ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.get_TotalCategories()
extern void GraphData_ChartAndGraph_IInternalGraphData_get_TotalCategories_m762C65184FFCB8264C51352937C9B16A9BA0A21E (void);
// 0x00000936 System.Collections.Generic.IEnumerable`1<ChartAndGraph.GraphData/CategoryData> ChartAndGraph.GraphData::ChartAndGraph.IInternalGraphData.get_Categories()
extern void GraphData_ChartAndGraph_IInternalGraphData_get_Categories_m418973E4C21679A279EECC9D0D2A156DBF81917A (void);
// 0x00000937 System.Void ChartAndGraph.GraphData::.ctor()
extern void GraphData__ctor_m0E510F0D5D5B48E1B496B5BB9D614A99BDA1C5A2 (void);
// 0x00000938 System.Boolean ChartAndGraph.GraphData/Slider::Update()
extern void Slider_Update_mE72393D0DCFA20404B3FA2A32988E92C6C21A521 (void);
// 0x00000939 System.String ChartAndGraph.GraphData/Slider::get_Category()
extern void Slider_get_Category_m7F8A2E06D5E487FE1EF19B4062E730DEF9199930 (void);
// 0x0000093A System.Void ChartAndGraph.GraphData/Slider::.ctor(ChartAndGraph.GraphData)
extern void Slider__ctor_m037FEDB60F5B69FE19E3762310936C171B2275F7 (void);
// 0x0000093B ChartAndGraph.DoubleVector2 ChartAndGraph.GraphData/Slider::get_Max()
extern void Slider_get_Max_m6D11BA28916F6F1A165200F59CDE86871B218DB6 (void);
// 0x0000093C System.Int32 ChartAndGraph.GraphData/Slider::get_MinIndex()
extern void Slider_get_MinIndex_m5C8CB3D796F08273435A87499C56434BFB00A9D4 (void);
// 0x0000093D ChartAndGraph.DoubleVector2 ChartAndGraph.GraphData/Slider::get_Min()
extern void Slider_get_Min_m10830B16365E28ED7646E872D4D086284A8FAED7 (void);
// 0x0000093E System.Collections.Generic.List`1<ChartAndGraph.DoubleVector3> ChartAndGraph.GraphData/CategoryData::getPoints()
extern void CategoryData_getPoints_m48DA92AA76B1111D6E69F5ED45F91A3E0148659C (void);
// 0x0000093F System.Void ChartAndGraph.GraphData/CategoryData::AddInnerCurve(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void CategoryData_AddInnerCurve_m7F8BC0A4329FD036F06FF95D3AD075021C7F92A1 (void);
// 0x00000940 System.Object ChartAndGraph.GraphData/CategoryData::Store()
extern void CategoryData_Store_m93410CDE47F7E9C484505064AF296B7E41D576B0 (void);
// 0x00000941 System.Void ChartAndGraph.GraphData/CategoryData::Restore(System.Object)
extern void CategoryData_Restore_m72D79257BA59FAF97B6366F32843B8828B0C5B9E (void);
// 0x00000942 System.Void ChartAndGraph.GraphData/CategoryData::.ctor()
extern void CategoryData__ctor_m8FBFEDB361EF081932EC41FAD2BEC457A4976A17 (void);
// 0x00000943 System.Void ChartAndGraph.GraphData/CategoryData::.cctor()
extern void CategoryData__cctor_mBF8497F2ADF47F05877958C3E7FF3C24EB548ACE (void);
// 0x00000944 System.Int32 ChartAndGraph.GraphData/VectorComparer::Compare(ChartAndGraph.DoubleVector3,ChartAndGraph.DoubleVector3)
extern void VectorComparer_Compare_m3A1FFB6B56AF278E7EE2FC25AB1B72088C614A35 (void);
// 0x00000945 System.Void ChartAndGraph.GraphData/VectorComparer::.ctor()
extern void VectorComparer__ctor_m15516D777433B1DCBFC0DFA5E229720786403068 (void);
// 0x00000946 System.Void ChartAndGraph.GraphData/SerializedCategory::.ctor()
extern void SerializedCategory__ctor_m76C2019F11D2A4E2951A3DE82F5BBDF8C502DC3D (void);
// 0x00000947 System.Void ChartAndGraph.GraphData/<>c::.cctor()
extern void U3CU3Ec__cctor_mC4375967E552D708E3D3131C00CA7FC3CB195001 (void);
// 0x00000948 System.Void ChartAndGraph.GraphData/<>c::.ctor()
extern void U3CU3Ec__ctor_m44855302AB891CBD33AA652D71D0C0CF2CE4B8A1 (void);
// 0x00000949 System.Boolean ChartAndGraph.GraphData/<>c::<StoreAllCategoriesinOrder>b__40_0(ChartAndGraph.BaseScrollableCategoryData)
extern void U3CU3Ec_U3CStoreAllCategoriesinOrderU3Eb__40_0_m46B873C4734B5D5A06E4B09DA78C2ADAAF938665 (void);
// 0x0000094A System.Int32 ChartAndGraph.GraphData/<>c::<StoreAllCategoriesinOrder>b__40_1(ChartAndGraph.BaseScrollableCategoryData)
extern void U3CU3Ec_U3CStoreAllCategoriesinOrderU3Eb__40_1_m00BFCAF4744FCC44EDF5E0A78940B85AA8B8BA05 (void);
// 0x0000094B System.Collections.Generic.KeyValuePair`2<System.String,ChartAndGraph.GraphData/CategoryData> ChartAndGraph.GraphData/<>c::<OnBeforeSerialize>b__76_1(System.Collections.Generic.KeyValuePair`2<System.String,ChartAndGraph.BaseScrollableCategoryData>)
extern void U3CU3Ec_U3COnBeforeSerializeU3Eb__76_1_m2545A2892DEA0B95A3ECAB275D327EA963B1A442 (void);
// 0x0000094C System.Int32 ChartAndGraph.GraphData/<>c::<OnBeforeSerialize>b__76_0(ChartAndGraph.GraphData/SerializedCategory)
extern void U3CU3Ec_U3COnBeforeSerializeU3Eb__76_0_m3B0A5A481D4A3E7AE7E5F16DE57633C88E4D6A74 (void);
// 0x0000094D ChartAndGraph.GraphData/CategoryData ChartAndGraph.GraphData/<>c::<ChartAndGraph.IInternalGraphData.get_Categories>b__85_0(ChartAndGraph.BaseScrollableCategoryData)
extern void U3CU3Ec_U3CChartAndGraph_IInternalGraphData_get_CategoriesU3Eb__85_0_mEE6BD649F7A20640674004D15FD6EABF90C90B99 (void);
// 0x0000094E System.Int32 ChartAndGraph.GraphData/<>c::<ChartAndGraph.IInternalGraphData.get_Categories>b__85_1(ChartAndGraph.GraphData/CategoryData)
extern void U3CU3Ec_U3CChartAndGraph_IInternalGraphData_get_CategoriesU3Eb__85_1_mC0E47F5FAE3FD40955B1ABD008A98589A026AB94 (void);
// 0x0000094F System.Void ChartAndGraph.GraphData/<>c__DisplayClass46_0::.ctor()
extern void U3CU3Ec__DisplayClass46_0__ctor_m967F06DCB10C8F0B5FA0CE5BE1D24A09AE2EAC92 (void);
// 0x00000950 System.Boolean ChartAndGraph.GraphData/<>c__DisplayClass46_0::<RemoveCategory>b__0(ChartAndGraph.BaseSlider)
extern void U3CU3Ec__DisplayClass46_0_U3CRemoveCategoryU3Eb__0_m55DBEB47F4B2F4DA80A571810BEE60BE53DE9463 (void);
// 0x00000951 System.Void ChartAndGraph.GraphData/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_m4411231EF5C353509BB9F0708E03CBFE847946BB (void);
// 0x00000952 System.Boolean ChartAndGraph.GraphData/<>c__DisplayClass49_0::<ClearCategory>b__0(ChartAndGraph.BaseSlider)
extern void U3CU3Ec__DisplayClass49_0_U3CClearCategoryU3Eb__0_mEF47F80A0195AED23C38A0CBF5F46ADBB80A358C (void);
// 0x00000953 UnityEngine.Camera ChartAndGraph.WorldSpaceGraphChart::get_TextCamera()
extern void WorldSpaceGraphChart_get_TextCamera_m23D7BEDBF926FBFE055DE82D21760DA409B612F1 (void);
// 0x00000954 System.Void ChartAndGraph.WorldSpaceGraphChart::set_TextCamera(UnityEngine.Camera)
extern void WorldSpaceGraphChart_set_TextCamera_m86CE6644AC1BFF1E8B6BA6582C6C4C2BE9D8940F (void);
// 0x00000955 System.Single ChartAndGraph.WorldSpaceGraphChart::get_TextIdleDistance()
extern void WorldSpaceGraphChart_get_TextIdleDistance_mCD510C55A0CEF8F1C94C5EB7E784D77DE802FF7C (void);
// 0x00000956 System.Void ChartAndGraph.WorldSpaceGraphChart::set_TextIdleDistance(System.Single)
extern void WorldSpaceGraphChart_set_TextIdleDistance_mCD1C03A8872AE1B481B115A886338E596FC68616 (void);
// 0x00000957 UnityEngine.Camera ChartAndGraph.WorldSpaceGraphChart::get_TextCameraLink()
extern void WorldSpaceGraphChart_get_TextCameraLink_m862D82F641CD5CBDEFC478D9B2C705726765DCB0 (void);
// 0x00000958 System.Single ChartAndGraph.WorldSpaceGraphChart::get_TextIdleDistanceLink()
extern void WorldSpaceGraphChart_get_TextIdleDistanceLink_m24EB84DE48FE8EB97F9730BF7442AC2D3F256950 (void);
// 0x00000959 System.Single ChartAndGraph.WorldSpaceGraphChart::get_TotalDepthLink()
extern void WorldSpaceGraphChart_get_TotalDepthLink_m015EE4161FD9D4BAF2FA716D6BE65CF572CB2CFA (void);
// 0x0000095A System.Boolean ChartAndGraph.WorldSpaceGraphChart::get_IsCanvas()
extern void WorldSpaceGraphChart_get_IsCanvas_m6B1E720B74BE80B5E50A40107B379A14F2E5F528 (void);
// 0x0000095B System.Boolean ChartAndGraph.WorldSpaceGraphChart::get_SupportRealtimeGeneration()
extern void WorldSpaceGraphChart_get_SupportRealtimeGeneration_m58EC4686F5C3285FDF19A5AFAA3D01A412F6C0B5 (void);
// 0x0000095C System.Void ChartAndGraph.WorldSpaceGraphChart::OnPropertyUpdated()
extern void WorldSpaceGraphChart_OnPropertyUpdated_m3AB2F7795A6EA7412509D9E1880954DB5E4CAF09 (void);
// 0x0000095D UnityEngine.GameObject ChartAndGraph.WorldSpaceGraphChart::CreatePointObject(ChartAndGraph.GraphData/CategoryData)
extern void WorldSpaceGraphChart_CreatePointObject_m144F164F0473A5A297535AF3F39420BB019DB7D1 (void);
// 0x0000095E ChartAndGraph.FillPathGenerator ChartAndGraph.WorldSpaceGraphChart::CreateFillObject(ChartAndGraph.GraphData/CategoryData)
extern void WorldSpaceGraphChart_CreateFillObject_m4474941A1FA34D3BCA97F7EA43A32A35CA1BB801 (void);
// 0x0000095F ChartAndGraph.PathGenerator ChartAndGraph.WorldSpaceGraphChart::CreateLineObject(ChartAndGraph.GraphData/CategoryData)
extern void WorldSpaceGraphChart_CreateLineObject_mAEDB4BF8B19712E1C4F736CC71A2FD9E870E830C (void);
// 0x00000960 System.Void ChartAndGraph.WorldSpaceGraphChart::OnNonHoverted()
extern void WorldSpaceGraphChart_OnNonHoverted_mA978B1AFBEED702AA4F938102C9E5B46FC818706 (void);
// 0x00000961 System.Double ChartAndGraph.WorldSpaceGraphChart::GetCategoryDepth(System.String)
extern void WorldSpaceGraphChart_GetCategoryDepth_m45E16E42001132A9F4CCD0FD284510DF862C0886 (void);
// 0x00000962 System.Void ChartAndGraph.WorldSpaceGraphChart::OnItemHoverted(System.Object)
extern void WorldSpaceGraphChart_OnItemHoverted_m926B0DBD6462622ED625D5C82FF9661F8D3F907C (void);
// 0x00000963 System.Void ChartAndGraph.WorldSpaceGraphChart::AddBillboardText(System.String,BillboardText)
extern void WorldSpaceGraphChart_AddBillboardText_mC910E28DCC2FD454E9F8EC358D76FEBFA9731A7B (void);
// 0x00000964 System.Void ChartAndGraph.WorldSpaceGraphChart::ClearChart()
extern void WorldSpaceGraphChart_ClearChart_mBE40D2D4234A6F96F3EE99EAD532319C55C8A4E5 (void);
// 0x00000965 System.Void ChartAndGraph.WorldSpaceGraphChart::ClearCache()
extern void WorldSpaceGraphChart_ClearCache_m5A63A3858EE2B14592144BBCB9F2E840E90AFE5B (void);
// 0x00000966 System.Void ChartAndGraph.WorldSpaceGraphChart::GenerateRealtime()
extern void WorldSpaceGraphChart_GenerateRealtime_m38F018228943D6E2086A9B7E7AEE906BBDCCBECC (void);
// 0x00000967 System.Void ChartAndGraph.WorldSpaceGraphChart::ViewPortionChanged()
extern void WorldSpaceGraphChart_ViewPortionChanged_m2F42AB998AE880C0B5C0C228E1029CA8164B988B (void);
// 0x00000968 System.Void ChartAndGraph.WorldSpaceGraphChart::InternalGenerateChart()
extern void WorldSpaceGraphChart_InternalGenerateChart_mD403B5EEC5B43DAD5F2A7617322A4B58BC7B7F19 (void);
// 0x00000969 System.Void ChartAndGraph.WorldSpaceGraphChart::SetAsMixedSeries()
extern void WorldSpaceGraphChart_SetAsMixedSeries_m33B086E7E24E8751030677CF1CB0B50A0EB7B52F (void);
// 0x0000096A System.Void ChartAndGraph.WorldSpaceGraphChart::.ctor()
extern void WorldSpaceGraphChart__ctor_m11FEE2638B36BEC38A52B727103083A0C9FA302F (void);
// 0x0000096B System.Void ChartAndGraph.WorldSpaceGraphChart/<>c__DisplayClass29_0::.ctor()
extern void U3CU3Ec__DisplayClass29_0__ctor_mF7F5C5109B96351894E4E44DE62842BEEB704A06 (void);
// 0x0000096C System.Boolean ChartAndGraph.WorldSpaceGraphChart/<>c__DisplayClass29_0::<GetCategoryDepth>b__0(ChartAndGraph.GraphData/CategoryData)
extern void U3CU3Ec__DisplayClass29_0_U3CGetCategoryDepthU3Eb__0_mB0A9C4AFEB4AD698C845788E90BA30C87F709DAE (void);
// 0x0000096D System.Void ChartAndGraph.WorldSpaceGraphChart/<>c::.cctor()
extern void U3CU3Ec__cctor_mFE42DFB7CBB94DE23643F029119FF14911ECF5C4 (void);
// 0x0000096E System.Void ChartAndGraph.WorldSpaceGraphChart/<>c::.ctor()
extern void U3CU3Ec__ctor_m7F323ACB0D421234BB1329DBE77F9ACCB0225F48 (void);
// 0x0000096F UnityEngine.Vector4 ChartAndGraph.WorldSpaceGraphChart/<>c::<InternalGenerateChart>b__36_1(ChartAndGraph.DoubleVector3)
extern void U3CU3Ec_U3CInternalGenerateChartU3Eb__36_1_mC5FE3C59D5AB982E5F6C65C11025A56BE23578A6 (void);
// 0x00000970 UnityEngine.Vector3 ChartAndGraph.WorldSpaceGraphChart/<>c::<InternalGenerateChart>b__36_0(UnityEngine.Vector4)
extern void U3CU3Ec_U3CInternalGenerateChartU3Eb__36_0_m6F13299ED82EB624BC2E90466101476CE880CFB2 (void);
// 0x00000971 System.Void ChartAndGraph.GraphFileManager::SaveGraphDataToFile(System.String,ChartAndGraph.GraphChartBase)
extern void GraphFileManager_SaveGraphDataToFile_mED864A8E42C6ED917D9D93894ACD4685B64B0C09 (void);
// 0x00000972 System.Void ChartAndGraph.GraphFileManager::LoadGraphDataFromFile(System.String,ChartAndGraph.GraphChartBase)
extern void GraphFileManager_LoadGraphDataFromFile_mBCB3B2D8328CA25D0E1E28105A173E059059EBAF (void);
// 0x00000973 System.Void ChartAndGraph.GraphFileManager::.ctor()
extern void GraphFileManager__ctor_m3186BD15EDA954B5399E2C85997821842A2BCD82 (void);
// 0x00000974 CategoryLabels ChartAndGraph.IInternalUse::get_CategoryLabels()
// 0x00000975 System.Void ChartAndGraph.IInternalUse::set_CategoryLabels(CategoryLabels)
// 0x00000976 ChartAndGraph.ItemLabels ChartAndGraph.IInternalUse::get_ItemLabels()
// 0x00000977 System.Void ChartAndGraph.IInternalUse::set_ItemLabels(ChartAndGraph.ItemLabels)
// 0x00000978 GroupLabels ChartAndGraph.IInternalUse::get_GroupLabels()
// 0x00000979 System.Void ChartAndGraph.IInternalUse::set_GroupLabels(GroupLabels)
// 0x0000097A ChartAndGraph.HorizontalAxis ChartAndGraph.IInternalUse::get_HorizontalAxis()
// 0x0000097B System.Void ChartAndGraph.IInternalUse::set_HorizontalAxis(ChartAndGraph.HorizontalAxis)
// 0x0000097C ChartAndGraph.VerticalAxis ChartAndGraph.IInternalUse::get_VerticalAxis()
// 0x0000097D System.Void ChartAndGraph.IInternalUse::set_VerticalAxis(ChartAndGraph.VerticalAxis)
// 0x0000097E UnityEngine.Camera ChartAndGraph.IInternalUse::get_InternalTextCamera()
// 0x0000097F System.Single ChartAndGraph.IInternalUse::get_InternalTextIdleDistance()
// 0x00000980 TextController ChartAndGraph.IInternalUse::get_InternalTextController()
// 0x00000981 ChartAndGraph.LegenedData ChartAndGraph.IInternalUse::get_InternalLegendInfo()
// 0x00000982 System.Boolean ChartAndGraph.IInternalUse::InternalHasValues(ChartAndGraph.AxisBase)
// 0x00000983 System.Double ChartAndGraph.IInternalUse::InternalMaxValue(ChartAndGraph.AxisBase)
// 0x00000984 System.Double ChartAndGraph.IInternalUse::InternalMinValue(ChartAndGraph.AxisBase)
// 0x00000985 System.Void ChartAndGraph.IInternalUse::InternalItemSelected(System.Object)
// 0x00000986 System.Void ChartAndGraph.IInternalUse::InternalItemLeave(System.Object)
// 0x00000987 System.Void ChartAndGraph.IInternalUse::InternalItemHovered(System.Object)
// 0x00000988 System.Void ChartAndGraph.IInternalUse::CallOnValidate()
// 0x00000989 System.Single ChartAndGraph.IInternalUse::get_InternalTotalWidth()
// 0x0000098A System.Single ChartAndGraph.IInternalUse::get_InternalTotalDepth()
// 0x0000098B System.Single ChartAndGraph.IInternalUse::get_InternalTotalHeight()
// 0x0000098C System.Boolean ChartAndGraph.IInternalUse::get_InternalSupportsItemLabels()
// 0x0000098D System.Boolean ChartAndGraph.IInternalUse::get_InternalSupportsCategoryLables()
// 0x0000098E System.Boolean ChartAndGraph.IInternalUse::get_InternalSupportsGroupLabels()
// 0x0000098F System.Void ChartAndGraph.IInternalUse::add_Generated(System.Action)
// 0x00000990 System.Void ChartAndGraph.IInternalUse::remove_Generated(System.Action)
// 0x00000991 System.Boolean ChartAndGraph.IInternalUse::get_HideHierarchy()
// 0x00000992 ChartAndGraph.ChartSparseDataSource ChartAndGraph.IInternalBarData::get_InternalDataSource()
// 0x00000993 System.Void ChartAndGraph.IInternalBarData::Update()
// 0x00000994 System.Double ChartAndGraph.IInternalBarData::GetMinValue()
// 0x00000995 System.Double ChartAndGraph.IInternalBarData::GetMaxValue()
// 0x00000996 System.Void ChartAndGraph.IInternalBarData::OnBeforeSerialize()
// 0x00000997 System.Void ChartAndGraph.IInternalBarData::OnAfterDeserialize()
// 0x00000998 System.Double ChartAndGraph.IInternalGraphData::GetMinValue(System.Int32,System.Boolean)
// 0x00000999 System.Double ChartAndGraph.IInternalGraphData::GetMaxValue(System.Int32,System.Boolean)
// 0x0000099A System.Void ChartAndGraph.IInternalGraphData::OnBeforeSerialize()
// 0x0000099B System.Void ChartAndGraph.IInternalGraphData::OnAfterDeserialize()
// 0x0000099C System.Void ChartAndGraph.IInternalGraphData::Update()
// 0x0000099D System.Void ChartAndGraph.IInternalGraphData::add_InternalDataChanged(System.EventHandler)
// 0x0000099E System.Void ChartAndGraph.IInternalGraphData::remove_InternalDataChanged(System.EventHandler)
// 0x0000099F System.Void ChartAndGraph.IInternalGraphData::add_InternalViewPortionChanged(System.EventHandler)
// 0x000009A0 System.Void ChartAndGraph.IInternalGraphData::remove_InternalViewPortionChanged(System.EventHandler)
// 0x000009A1 System.Void ChartAndGraph.IInternalGraphData::add_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
// 0x000009A2 System.Void ChartAndGraph.IInternalGraphData::remove_InternalRealTimeDataChanged(System.Action`2<System.Int32,System.String>)
// 0x000009A3 System.Int32 ChartAndGraph.IInternalGraphData::get_TotalCategories()
// 0x000009A4 System.Collections.Generic.IEnumerable`1<ChartAndGraph.GraphData/CategoryData> ChartAndGraph.IInternalGraphData::get_Categories()
// 0x000009A5 ChartAndGraph.ChartSparseDataSource ChartAndGraph.IInternalPieData::get_InternalDataSource()
// 0x000009A6 System.Void ChartAndGraph.IInternalSettings::add_InternalOnDataUpdate(System.EventHandler)
// 0x000009A7 System.Void ChartAndGraph.IInternalSettings::remove_InternalOnDataUpdate(System.EventHandler)
// 0x000009A8 System.Void ChartAndGraph.IInternalSettings::add_InternalOnDataChanged(System.EventHandler)
// 0x000009A9 System.Void ChartAndGraph.IInternalSettings::remove_InternalOnDataChanged(System.EventHandler)
// 0x000009AA ChartAndGraph.IInternalUse ChartAndGraph.InternalItemEvents::get_Parent()
// 0x000009AB System.Void ChartAndGraph.InternalItemEvents::set_Parent(ChartAndGraph.IInternalUse)
// 0x000009AC System.Object ChartAndGraph.InternalItemEvents::get_UserData()
// 0x000009AD System.Void ChartAndGraph.InternalItemEvents::set_UserData(System.Object)
// 0x000009AE System.Void ChartAndGraph.LegenedData::AddLegenedItem(ChartAndGraph.LegenedData/LegenedItem)
extern void LegenedData_AddLegenedItem_m0A4E213A1C7F5D3B95E468F42222875E8A87B96C (void);
// 0x000009AF System.Collections.Generic.IEnumerable`1<ChartAndGraph.LegenedData/LegenedItem> ChartAndGraph.LegenedData::get_Items()
extern void LegenedData_get_Items_m554485E6B26A46FB5B1A87CBC12DD399F9336DED (void);
// 0x000009B0 System.Void ChartAndGraph.LegenedData::.ctor()
extern void LegenedData__ctor_mEBAC6D44FDCDC65E9FAE701F5D84C49FCFA0D17A (void);
// 0x000009B1 System.Void ChartAndGraph.LegenedData/LegenedItem::.ctor()
extern void LegenedItem__ctor_m4B5DC2BE1D1F7245DFCF89F1C84103063A8C1F93 (void);
// 0x000009B2 System.Void ChartAndGraph.MaterialTiling::.ctor(System.Boolean,System.Single)
extern void MaterialTiling__ctor_m0B5FB49A293882E7A35FAEC1EB5DBA660AA07269 (void);
// 0x000009B3 System.Boolean ChartAndGraph.MaterialTiling::Equals(System.Object)
extern void MaterialTiling_Equals_m466A2E9815E17A86ABED36483492136634F64556 (void);
// 0x000009B4 System.Int32 ChartAndGraph.MaterialTiling::GetHashCode()
extern void MaterialTiling_GetHashCode_m31C922525DC3445EB61DCD90C8066015A8352338 (void);
// 0x000009B5 System.Void ChartAndGraph.CanvasChartMesh::.ctor(System.Boolean)
extern void CanvasChartMesh__ctor_mC8420991F60F32A4E3D92E3784E9D381995E2747 (void);
// 0x000009B6 System.Void ChartAndGraph.CanvasChartMesh::.ctor(UnityEngine.UI.VertexHelper)
extern void CanvasChartMesh__ctor_m90F9A5D93F0BBAF1CD19420289156CB538915D22 (void);
// 0x000009B7 System.Void ChartAndGraph.CanvasChartMesh::.ctor(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void CanvasChartMesh__ctor_mF6F9FC3F6A496692D8641E1A07B4882EB8B29CE4 (void);
// 0x000009B8 System.Void ChartAndGraph.CanvasChartMesh::WrapAround(UnityEngine.UI.VertexHelper)
extern void CanvasChartMesh_WrapAround_m0B9B647A396473DDAFAEAF7F4A963C07EFE4EDFA (void);
// 0x000009B9 System.Void ChartAndGraph.CanvasChartMesh::WrapAround(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void CanvasChartMesh_WrapAround_m38C56CD5B079B67EC813B4B1DCAD2518D4CDBB39 (void);
// 0x000009BA BillboardText ChartAndGraph.CanvasChartMesh::AddText(ChartAndGraph.AnyChart,UnityEngine.MonoBehaviour,UnityEngine.Transform,System.Int32,System.Single,System.String,System.Single,System.Single,System.Single,System.Single,System.Object)
extern void CanvasChartMesh_AddText_mF74190C7102D9387705DEB7D28067ACDD4E7FD88 (void);
// 0x000009BB UnityEngine.UIVertex ChartAndGraph.CanvasChartMesh::FloorVertex(UnityEngine.UIVertex)
extern void CanvasChartMesh_FloorVertex_mA7CFA0D10ABC5291C0DBD1278007F72964027A21 (void);
// 0x000009BC System.Void ChartAndGraph.CanvasChartMesh::AddQuad(UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex)
extern void CanvasChartMesh_AddQuad_m10058BC782160AB63D3E3B539B96C46B3A56B5E2 (void);
// 0x000009BD System.Void ChartAndGraph.CanvasChartMesh::AddXYRect(UnityEngine.Rect,System.Int32,System.Single)
extern void CanvasChartMesh_AddXYRect_mBFF9EDC1493756647C86B38EEADDEE7DE5388B75 (void);
// 0x000009BE System.Void ChartAndGraph.CanvasChartMesh::AddXZRect(UnityEngine.Rect,System.Int32,System.Single)
extern void CanvasChartMesh_AddXZRect_m88E01C307E6910DF3BC01315BBAFDA092292F6D7 (void);
// 0x000009BF System.Void ChartAndGraph.CanvasChartMesh::AddYZRect(UnityEngine.Rect,System.Int32,System.Single)
extern void CanvasChartMesh_AddYZRect_m18A6CB2741340D63F7097F704723DF9EE9FF2BA9 (void);
// 0x000009C0 ChartAndGraph.ChartOrientation ChartAndGraph.ChartMeshBase::get_Orientation()
extern void ChartMeshBase_get_Orientation_mD1C2C815A175E2BBEA3117ED8EBE1A7932640E87 (void);
// 0x000009C1 System.Void ChartAndGraph.ChartMeshBase::set_Orientation(ChartAndGraph.ChartOrientation)
extern void ChartMeshBase_set_Orientation_m87F834D65E5BED545ED69FE1C608E093643A02FE (void);
// 0x000009C2 System.Single ChartAndGraph.ChartMeshBase::get_Tile()
extern void ChartMeshBase_get_Tile_m325A1DA8DF1CE6A5201E16181CA3061C2B4F5079 (void);
// 0x000009C3 System.Void ChartAndGraph.ChartMeshBase::set_Tile(System.Single)
extern void ChartMeshBase_set_Tile_mAED56F5D771AC0946C07F3840834A44252736E44 (void);
// 0x000009C4 System.Single ChartAndGraph.ChartMeshBase::get_Offset()
extern void ChartMeshBase_get_Offset_m44B3F4D7B3492223D8C915497E194765BBCA2F02 (void);
// 0x000009C5 System.Void ChartAndGraph.ChartMeshBase::set_Offset(System.Single)
extern void ChartMeshBase_set_Offset_m0A620034DF859A689C768EEC52815E5F1DEB2B00 (void);
// 0x000009C6 System.Single ChartAndGraph.ChartMeshBase::get_Length()
extern void ChartMeshBase_get_Length_mF24BFEEE513BE43BF890161B869DAF8E5CA23FC6 (void);
// 0x000009C7 System.Void ChartAndGraph.ChartMeshBase::set_Length(System.Single)
extern void ChartMeshBase_set_Length_m05073A1B3BEFECD9F86DF14BFB79695824840418 (void);
// 0x000009C8 System.Boolean ChartAndGraph.ChartMeshBase::get_RecycleText()
extern void ChartMeshBase_get_RecycleText_mBF81839FE404AF6776B99355A86373744038D025 (void);
// 0x000009C9 System.Void ChartAndGraph.ChartMeshBase::set_RecycleText(System.Boolean)
extern void ChartMeshBase_set_RecycleText_mCFE3B45903FC5BABBCF566195FCED69F637A413B (void);
// 0x000009CA System.Void ChartAndGraph.ChartMeshBase::.ctor()
extern void ChartMeshBase__ctor_mC07028B8E9EAF4D95B3FC2F395EBB6123FD333AB (void);
// 0x000009CB System.Collections.Generic.List`1<BillboardText> ChartAndGraph.ChartMeshBase::get_CurrentTextObjects()
extern void ChartMeshBase_get_CurrentTextObjects_m84EEEB05795DEC2C619D2CF43EDFB6558061E538 (void);
// 0x000009CC System.Collections.Generic.List`1<BillboardText> ChartAndGraph.ChartMeshBase::get_TextObjects()
extern void ChartMeshBase_get_TextObjects_mD61A9AFBA1144FDA24681A92EE78A5F63ED8CE43 (void);
// 0x000009CD UnityEngine.Vector2[] ChartAndGraph.ChartMeshBase::GetUvs(UnityEngine.Rect)
extern void ChartMeshBase_GetUvs_m4B5310AE287908344D0F7C85B9AEC4429D3A11F4 (void);
// 0x000009CE System.Void ChartAndGraph.ChartMeshBase::Clear()
extern void ChartMeshBase_Clear_m6449E892337D4ADD7A7C1203209BC4B677186157 (void);
// 0x000009CF UnityEngine.Vector2[] ChartAndGraph.ChartMeshBase::GetUvs(UnityEngine.Rect,ChartAndGraph.ChartOrientation)
extern void ChartMeshBase_GetUvs_m00C4DC193BB5235A53273303F44F63912A12698A (void);
// 0x000009D0 System.Void ChartAndGraph.ChartMeshBase::DestoryBillboard(BillboardText)
extern void ChartMeshBase_DestoryBillboard_mFDEE9F997AC909905B9C678030E1D8003095E15C (void);
// 0x000009D1 System.Void ChartAndGraph.ChartMeshBase::DestoryRecycled()
extern void ChartMeshBase_DestoryRecycled_mEC94B72D27DC4C54A7B0664229FC207D059B7292 (void);
// 0x000009D2 BillboardText ChartAndGraph.ChartMeshBase::AddText(ChartAndGraph.AnyChart,UnityEngine.MonoBehaviour,UnityEngine.Transform,System.Int32,System.Single,System.String,System.Single,System.Single,System.Single,System.Single,System.Object)
extern void ChartMeshBase_AddText_mA665D9EA8727D70252D516570752AD2C99D15541 (void);
// 0x000009D3 System.Void ChartAndGraph.ChartMeshBase::AddQuad(UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex)
// 0x000009D4 System.Void ChartAndGraph.ChartMeshBase::AddXYRect(UnityEngine.Rect,System.Int32,System.Single)
// 0x000009D5 System.Void ChartAndGraph.ChartMeshBase::AddXZRect(UnityEngine.Rect,System.Int32,System.Single)
// 0x000009D6 System.Void ChartAndGraph.ChartMeshBase::AddYZRect(UnityEngine.Rect,System.Int32,System.Single)
// 0x000009D7 System.Collections.Generic.List`1<BillboardText> ChartAndGraph.IChartMesh::get_TextObjects()
// 0x000009D8 System.Single ChartAndGraph.IChartMesh::get_Tile()
// 0x000009D9 System.Void ChartAndGraph.IChartMesh::set_Tile(System.Single)
// 0x000009DA System.Single ChartAndGraph.IChartMesh::get_Offset()
// 0x000009DB System.Void ChartAndGraph.IChartMesh::set_Offset(System.Single)
// 0x000009DC System.Single ChartAndGraph.IChartMesh::get_Length()
// 0x000009DD System.Void ChartAndGraph.IChartMesh::set_Length(System.Single)
// 0x000009DE BillboardText ChartAndGraph.IChartMesh::AddText(ChartAndGraph.AnyChart,UnityEngine.MonoBehaviour,UnityEngine.Transform,System.Int32,System.Single,System.String,System.Single,System.Single,System.Single,System.Single,System.Object)
// 0x000009DF System.Void ChartAndGraph.IChartMesh::AddYZRect(UnityEngine.Rect,System.Int32,System.Single)
// 0x000009E0 System.Void ChartAndGraph.IChartMesh::AddXZRect(UnityEngine.Rect,System.Int32,System.Single)
// 0x000009E1 System.Void ChartAndGraph.IChartMesh::AddXYRect(UnityEngine.Rect,System.Int32,System.Single)
// 0x000009E2 System.Void ChartAndGraph.IChartMesh::AddQuad(UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex)
// 0x000009E3 System.Void ChartAndGraph.WorldSpaceChartMesh::.ctor(System.Boolean)
extern void WorldSpaceChartMesh__ctor_m2E3C6BC041022321A3D46B36EF5CF565FE2299ED (void);
// 0x000009E4 System.Void ChartAndGraph.WorldSpaceChartMesh::.ctor(System.Int32)
extern void WorldSpaceChartMesh__ctor_mA7D219705F46FCD9AD3D6BD1B19E4C551E352FC2 (void);
// 0x000009E5 System.Void ChartAndGraph.WorldSpaceChartMesh::ValidateMesh()
extern void WorldSpaceChartMesh_ValidateMesh_mD63C946E65F9BE74DB95ECE79CF7F9B22DAB669B (void);
// 0x000009E6 System.Int32 ChartAndGraph.WorldSpaceChartMesh::AddVertex(UnityEngine.UIVertex)
extern void WorldSpaceChartMesh_AddVertex_m7BC1BD2EEB63D113F3FFFC56C4B1118CF6D63CEA (void);
// 0x000009E7 BillboardText ChartAndGraph.WorldSpaceChartMesh::AddText(ChartAndGraph.AnyChart,UnityEngine.MonoBehaviour,UnityEngine.Transform,System.Int32,System.Single,System.String,System.Single,System.Single,System.Single,System.Single,System.Object)
extern void WorldSpaceChartMesh_AddText_mA022A04A643B695991733B1AB32D94F771EFA3B3 (void);
// 0x000009E8 System.Int32 ChartAndGraph.WorldSpaceChartMesh::AddVertex(UnityEngine.Vector3,UnityEngine.Vector2)
extern void WorldSpaceChartMesh_AddVertex_mB5DD6E333F49D0D05934B79F0CE9FDCAE9BB04B5 (void);
// 0x000009E9 System.Void ChartAndGraph.WorldSpaceChartMesh::AddTringle(System.Int32,System.Int32,System.Int32)
extern void WorldSpaceChartMesh_AddTringle_m657A6A980A3FFDFE2DBC4585F445AC77A5EF631B (void);
// 0x000009EA System.Void ChartAndGraph.WorldSpaceChartMesh::AddTringle(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32,System.Int32)
extern void WorldSpaceChartMesh_AddTringle_m1DCD78C509C68A93C50E75756B083565CCB49230 (void);
// 0x000009EB System.Void ChartAndGraph.WorldSpaceChartMesh::Clear()
extern void WorldSpaceChartMesh_Clear_m28F67D43065681BBEBAFB8F21B01CC12C45E2E50 (void);
// 0x000009EC System.Collections.Generic.List`1<System.Int32> ChartAndGraph.WorldSpaceChartMesh::GetTringlesForGroup(System.Int32)
extern void WorldSpaceChartMesh_GetTringlesForGroup_m4844214648411B602839321A9D2A0787B71E2984 (void);
// 0x000009ED System.Void ChartAndGraph.WorldSpaceChartMesh::AddYZRect(UnityEngine.Rect,System.Int32,System.Single)
extern void WorldSpaceChartMesh_AddYZRect_mAD45D502AA70369A86E50A6BAA91FA0EBB721D94 (void);
// 0x000009EE System.Void ChartAndGraph.WorldSpaceChartMesh::AddXZRect(UnityEngine.Rect,System.Int32,System.Single)
extern void WorldSpaceChartMesh_AddXZRect_m36B5C5463288ECD06BFDCE92FFB5BE9683834C5B (void);
// 0x000009EF System.Void ChartAndGraph.WorldSpaceChartMesh::AddXYRect(UnityEngine.Rect,System.Int32,System.Single)
extern void WorldSpaceChartMesh_AddXYRect_m351BB53A86264D8882FE0E97C46EB5D50BD578C8 (void);
// 0x000009F0 System.Void ChartAndGraph.WorldSpaceChartMesh::AddQuad(UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex,UnityEngine.UIVertex)
extern void WorldSpaceChartMesh_AddQuad_mB0D5BAF24B4CB90C056FF13EDD683814F75E920B (void);
// 0x000009F1 UnityEngine.Color[] ChartAndGraph.WorldSpaceChartMesh::GetColors()
extern void WorldSpaceChartMesh_GetColors_m5B7016EA5092703679F2D975117322F34FAF00A4 (void);
// 0x000009F2 System.Void ChartAndGraph.WorldSpaceChartMesh::ApplyToMesh(UnityEngine.Mesh)
extern void WorldSpaceChartMesh_ApplyToMesh_mDF81D371F336EF85E1769F6B9853999D628AF972 (void);
// 0x000009F3 UnityEngine.Mesh ChartAndGraph.WorldSpaceChartMesh::Generate(UnityEngine.Mesh)
extern void WorldSpaceChartMesh_Generate_m3EB581349019341221CC0016CFF0A97272D5805B (void);
// 0x000009F4 UnityEngine.Mesh ChartAndGraph.WorldSpaceChartMesh::Generate()
extern void WorldSpaceChartMesh_Generate_m55981EA45B0F0E0CE0EF3C2248D06CBEB52CCA3C (void);
// 0x000009F5 ChartAndGraph.ScrollableAxisChart ChartAndGraph.IMixedChartDelegate::CreateCategoryView(System.Type,ChartAndGraph.ScrollableAxisChart)
// 0x000009F6 System.Void ChartAndGraph.IMixedChartDelegate::SetData(System.Collections.Generic.Dictionary`2<System.String,ChartAndGraph.BaseScrollableCategoryData>)
// 0x000009F7 System.Void ChartAndGraph.IMixedChartDelegate::RealaseChart(ChartAndGraph.ScrollableAxisChart)
// 0x000009F8 System.Void ChartAndGraph.IMixedChartDelegate::DeactivateChart(ChartAndGraph.ScrollableAxisChart)
// 0x000009F9 System.Void ChartAndGraph.IMixedChartDelegate::ReactivateChart(ChartAndGraph.ScrollableAxisChart)
// 0x000009FA System.Boolean ChartAndGraph.IMixedSeriesProxy::AddCategory(System.String,ChartAndGraph.BaseScrollableCategoryData)
// 0x000009FB System.Boolean ChartAndGraph.IMixedSeriesProxy::HasCategory(System.String)
// 0x000009FC System.Void ChartAndGraph.IMixedSeriesProxy::ClearCategory(System.String)
// 0x000009FD System.Void ChartAndGraph.IMixedSeriesProxy::AppendDatum(System.String,ChartAndGraph.MixedSeriesGenericValue)
// 0x000009FE System.Void ChartAndGraph.IMixedSeriesProxy::AppendDatum(System.String,System.Collections.Generic.IList`1<ChartAndGraph.MixedSeriesGenericValue>)
// 0x000009FF System.Void ChartAndGraph.ChartSettingItemBase::add_OnDataUpdate(System.EventHandler)
extern void ChartSettingItemBase_add_OnDataUpdate_mDD4FC4303A073F4D8704C248857730055FAEF249 (void);
// 0x00000A00 System.Void ChartAndGraph.ChartSettingItemBase::remove_OnDataUpdate(System.EventHandler)
extern void ChartSettingItemBase_remove_OnDataUpdate_mA808D637C17F5F6E1ABD363FD47CBB39D899D061 (void);
// 0x00000A01 System.Void ChartAndGraph.ChartSettingItemBase::add_OnDataChanged(System.EventHandler)
extern void ChartSettingItemBase_add_OnDataChanged_mEFABE67C67B63454BF30E194741B5BA2B1F5D2AD (void);
// 0x00000A02 System.Void ChartAndGraph.ChartSettingItemBase::remove_OnDataChanged(System.EventHandler)
extern void ChartSettingItemBase_remove_OnDataChanged_m16893529ED8A5D7F8FE580F3107DD651AEEA77DD (void);
// 0x00000A03 System.Action`2<ChartAndGraph.IInternalUse,System.Boolean> ChartAndGraph.ChartSettingItemBase::get_Assign()
// 0x00000A04 ChartAndGraph.AnyChart ChartAndGraph.ChartSettingItemBase::get_SafeChart()
extern void ChartSettingItemBase_get_SafeChart_m3DEA3107DE28C2A19C172842D614C4261D2B26FC (void);
// 0x00000A05 System.Void ChartAndGraph.ChartSettingItemBase::AddInnerItem(ChartAndGraph.IInternalSettings)
extern void ChartSettingItemBase_AddInnerItem_m184A5C5296559360D74987D8F4B8EB1B3E6D29E1 (void);
// 0x00000A06 System.Void ChartAndGraph.ChartSettingItemBase::Item_InternalOnDataUpdate(System.Object,System.EventArgs)
extern void ChartSettingItemBase_Item_InternalOnDataUpdate_m1DC75A6A28470A2B8148FE3D394C91787FACCA6F (void);
// 0x00000A07 System.Void ChartAndGraph.ChartSettingItemBase::Item_InternalOnDataChanged(System.Object,System.EventArgs)
extern void ChartSettingItemBase_Item_InternalOnDataChanged_m3696A5FAE754C9EF520C0019FD8395146085DB78 (void);
// 0x00000A08 System.Void ChartAndGraph.ChartSettingItemBase::RaiseOnChanged()
extern void ChartSettingItemBase_RaiseOnChanged_m8A0FECF1EA9C891D7394298F246ED34DD7DAFCC4 (void);
// 0x00000A09 System.Void ChartAndGraph.ChartSettingItemBase::RaiseOnUpdate()
extern void ChartSettingItemBase_RaiseOnUpdate_m7A62F9F02DE1353DECEEB73D1374A5BB1702FB79 (void);
// 0x00000A0A System.Void ChartAndGraph.ChartSettingItemBase::SafeAssign(System.Boolean)
extern void ChartSettingItemBase_SafeAssign_mFB418A18CA26EF34EB9DA391290C0F77973C0FEE (void);
// 0x00000A0B System.Void ChartAndGraph.ChartSettingItemBase::OnEnable()
extern void ChartSettingItemBase_OnEnable_mCF6FBFBD2FFF1294FB71F15068E0A53D517E3427 (void);
// 0x00000A0C System.Void ChartAndGraph.ChartSettingItemBase::OnDisable()
extern void ChartSettingItemBase_OnDisable_m4A9359AE463435F63ECBB0DB87D0AF40F137339E (void);
// 0x00000A0D System.Void ChartAndGraph.ChartSettingItemBase::OnDestroy()
extern void ChartSettingItemBase_OnDestroy_mD1651F68B1676A67615C60360CA0EFA80602502C (void);
// 0x00000A0E System.Void ChartAndGraph.ChartSettingItemBase::OnValidate()
extern void ChartSettingItemBase_OnValidate_mAADB5BB1A37F9746EE0B40909B142E992CC6385F (void);
// 0x00000A0F System.Void ChartAndGraph.ChartSettingItemBase::Start()
extern void ChartSettingItemBase_Start_mDA47E2577247F4F9630FE38EDCA5E7B29EE60687 (void);
// 0x00000A10 System.Void ChartAndGraph.ChartSettingItemBase::ChartAndGraph.IInternalSettings.add_InternalOnDataUpdate(System.EventHandler)
extern void ChartSettingItemBase_ChartAndGraph_IInternalSettings_add_InternalOnDataUpdate_mBA788FFF61D365DCED995DE7749ADE00E6FD9D75 (void);
// 0x00000A11 System.Void ChartAndGraph.ChartSettingItemBase::ChartAndGraph.IInternalSettings.remove_InternalOnDataUpdate(System.EventHandler)
extern void ChartSettingItemBase_ChartAndGraph_IInternalSettings_remove_InternalOnDataUpdate_mB599FD2E02CF260112C4D9D8A6D0562EC19FFCB3 (void);
// 0x00000A12 System.Void ChartAndGraph.ChartSettingItemBase::ChartAndGraph.IInternalSettings.add_InternalOnDataChanged(System.EventHandler)
extern void ChartSettingItemBase_ChartAndGraph_IInternalSettings_add_InternalOnDataChanged_m8E7C41522B67DEF1D422E837208B49F951C236B1 (void);
// 0x00000A13 System.Void ChartAndGraph.ChartSettingItemBase::ChartAndGraph.IInternalSettings.remove_InternalOnDataChanged(System.EventHandler)
extern void ChartSettingItemBase_ChartAndGraph_IInternalSettings_remove_InternalOnDataChanged_mC440E3CB02974E01C9D2620EFCF3637CE7E4BBB7 (void);
// 0x00000A14 System.Void ChartAndGraph.ChartSettingItemBase::.ctor()
extern void ChartSettingItemBase__ctor_m3BE6C32887690E8537489E85A9FFB88C158AF7BE (void);
// 0x00000A15 System.Int32 ChartAndGraph.BoxPathGenerator::WriteBox(System.Single,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void BoxPathGenerator_WriteBox_m7EE3621B1FE61E0412F23A1848B25E7884037F97 (void);
// 0x00000A16 System.Void ChartAndGraph.BoxPathGenerator::AddTringles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32)
extern void BoxPathGenerator_AddTringles_mDB77D5DB9418BB02DD548B375F704D3885E9298A (void);
// 0x00000A17 System.Void ChartAndGraph.BoxPathGenerator::Generator(UnityEngine.Vector3[],System.Single,System.Boolean)
extern void BoxPathGenerator_Generator_m1BB38AAD5CAC72558CD051B7F47FE17F96AD40FA (void);
// 0x00000A18 System.Void ChartAndGraph.BoxPathGenerator::.ctor()
extern void BoxPathGenerator__ctor_mE3876F2A89613E614008E622B6E53EC421E8B4AA (void);
// 0x00000A19 System.Int32 ChartAndGraph.CustomPathGenerator::writeItem(UnityEngine.Quaternion,UnityEngine.Vector3,System.Single,System.Single)
extern void CustomPathGenerator_writeItem_m4608FA73D3C6ED5199C79FEC47BD4596B01FB8C0 (void);
// 0x00000A1A System.Void ChartAndGraph.CustomPathGenerator::AddTringles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32)
extern void CustomPathGenerator_AddTringles_mE7DDC474A3924357D0966E1B1F38D93A11E81D2B (void);
// 0x00000A1B System.Void ChartAndGraph.CustomPathGenerator::OfPath(UnityEngine.Vector3[])
extern void CustomPathGenerator_OfPath_m213D7FD2322D30BE947553826EF499E15812704C (void);
// 0x00000A1C UnityEngine.Quaternion ChartAndGraph.CustomPathGenerator::getAngle(System.Int32,UnityEngine.Quaternion)
extern void CustomPathGenerator_getAngle_m183A500DF1C65FF0805E5A1D18EAFB1831251693 (void);
// 0x00000A1D System.Single ChartAndGraph.CustomPathGenerator::getScale(System.Int32)
extern void CustomPathGenerator_getScale_m6611924BB1F9B0E58371D1B7F51B4CE43909CC9C (void);
// 0x00000A1E System.Void ChartAndGraph.CustomPathGenerator::Generator(UnityEngine.Vector3[],System.Single,System.Boolean)
extern void CustomPathGenerator_Generator_m3720613604D6CDB1BEAB5217C3C7F8D6365D4324 (void);
// 0x00000A1F System.Single ChartAndGraph.CustomPathGenerator::quickBlend(System.Single)
extern void CustomPathGenerator_quickBlend_m626A43A59874DCE6ED9A1E7D0110D69ABBA939D7 (void);
// 0x00000A20 System.Void ChartAndGraph.CustomPathGenerator::Generate(System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single)
extern void CustomPathGenerator_Generate_mA879B9F2476F7D11F31A5A51547A45699F07A305 (void);
// 0x00000A21 System.Void ChartAndGraph.CustomPathGenerator::ChartAndGraph.IBarGenerator.Generate(System.Single,System.Single)
extern void CustomPathGenerator_ChartAndGraph_IBarGenerator_Generate_m0F260C81AEDD379B854E0B1C4A0D6AA10C56707C (void);
// 0x00000A22 System.Void ChartAndGraph.CustomPathGenerator::ChartAndGraph.IBarGenerator.Clear()
extern void CustomPathGenerator_ChartAndGraph_IBarGenerator_Clear_m508E793EA9EB432445DFBA3437FB97AF01DC1098 (void);
// 0x00000A23 System.Void ChartAndGraph.CustomPathGenerator::.ctor()
extern void CustomPathGenerator__ctor_m57F797C7AEECDA022B4211138A7DE3CB0E966342 (void);
// 0x00000A24 System.Void ChartAndGraph.CylinderPathGenerator::EnsureCirlce()
extern void CylinderPathGenerator_EnsureCirlce_mD3C5AD52A226005D43535F9160D13E337769B010 (void);
// 0x00000A25 System.Int32 ChartAndGraph.CylinderPathGenerator::WriteCircle(System.Single,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void CylinderPathGenerator_WriteCircle_mB7571B9661A080808FE95776615186EE944451F6 (void);
// 0x00000A26 System.Void ChartAndGraph.CylinderPathGenerator::AddTringles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32)
extern void CylinderPathGenerator_AddTringles_m018F387507B830E374CF4A6D997113A9A2C40960 (void);
// 0x00000A27 System.Void ChartAndGraph.CylinderPathGenerator::Generator(UnityEngine.Vector3[],System.Single,System.Boolean)
extern void CylinderPathGenerator_Generator_m1DE0755CAA7C889FD69E4709E8A2FB8D011C7239 (void);
// 0x00000A28 System.Void ChartAndGraph.CylinderPathGenerator::.ctor()
extern void CylinderPathGenerator__ctor_mF52CF728E55F7345B0C28C611D4E0E905138F6E1 (void);
// 0x00000A29 System.Single ChartAndGraph.FillPathGenerator::get_JointSizeLink()
extern void FillPathGenerator_get_JointSizeLink_m575B9E75A2C595D25D0D4BBE9122EC3E4035D1CA (void);
// 0x00000A2A System.Int32 ChartAndGraph.FillPathGenerator::get_JointSmoothingLink()
extern void FillPathGenerator_get_JointSmoothingLink_mCB9270C160E3B49CCC7CAFE5924D15EEB0C9688F (void);
// 0x00000A2B System.Void ChartAndGraph.FillPathGenerator::SetLineSmoothing(System.Boolean,System.Int32,System.Single)
extern void FillPathGenerator_SetLineSmoothing_mC3101431F48A93A64BB009F8D51287BA33FDAC34 (void);
// 0x00000A2C System.Void ChartAndGraph.FillPathGenerator::SetStrechFill(System.Boolean)
extern void FillPathGenerator_SetStrechFill_mE60AD747B2BA3842C9C02F505762B440281184D4 (void);
// 0x00000A2D System.Void ChartAndGraph.FillPathGenerator::SetGraphBounds(System.Single,System.Single)
extern void FillPathGenerator_SetGraphBounds_m61780836DDE984366D3EBD53269729E2B1797231 (void);
// 0x00000A2E System.Int32 ChartAndGraph.FillPathGenerator::WriteVector(UnityEngine.Vector3,System.Single,System.Single)
extern void FillPathGenerator_WriteVector_mDF2D7A27BC22A3CE5CA646313C8FF75B9A7CD133 (void);
// 0x00000A2F System.Void ChartAndGraph.FillPathGenerator::AddTringles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32)
extern void FillPathGenerator_AddTringles_m0678A0768BB91D3A35D7314FDBC9E1C8352F7626 (void);
// 0x00000A30 System.Void ChartAndGraph.FillPathGenerator::Generator(UnityEngine.Vector3[],System.Single,System.Boolean)
extern void FillPathGenerator_Generator_m219C94C70D29A830B2BBD8CD098B03FCA1846B8A (void);
// 0x00000A31 System.Void ChartAndGraph.FillPathGenerator::.ctor()
extern void FillPathGenerator__ctor_m28EF7901FC93F3FF47684856905166DF80419AEB (void);
// 0x00000A32 System.Void ChartAndGraph.LineRendererPathGenerator::Start()
extern void LineRendererPathGenerator_Start_m4F6FC06348B0E6944A81F14E2F0116B2F911DB90 (void);
// 0x00000A33 System.Void ChartAndGraph.LineRendererPathGenerator::EnsureRenderer()
extern void LineRendererPathGenerator_EnsureRenderer_m1C98460ABC8DA8601F25A7D48BD0D01ED61AD80F (void);
// 0x00000A34 System.Void ChartAndGraph.LineRendererPathGenerator::Clear()
extern void LineRendererPathGenerator_Clear_m2E01D8922904F87793D9B13BAB68937628D13C19 (void);
// 0x00000A35 System.Void ChartAndGraph.LineRendererPathGenerator::Generator(UnityEngine.Vector3[],System.Single,System.Boolean)
extern void LineRendererPathGenerator_Generator_m8C16EEF8021D03B3A3E94BA1FE660D4CBDFB86B1 (void);
// 0x00000A36 System.Void ChartAndGraph.LineRendererPathGenerator::.ctor()
extern void LineRendererPathGenerator__ctor_m2389404C35FEFFF926B3A4C08FA5A2D7A9991E88 (void);
// 0x00000A37 System.Void ChartAndGraph.PathGenerator::Generator(UnityEngine.Vector3[],System.Single,System.Boolean)
// 0x00000A38 System.Void ChartAndGraph.PathGenerator::Clear()
// 0x00000A39 System.Void ChartAndGraph.PathGenerator::.ctor()
extern void PathGenerator__ctor_m30AB7C7D09A2ED528A50E0CFF5A2107CD62ED719 (void);
// 0x00000A3A UnityEngine.Quaternion ChartAndGraph.PathMultiplier::LookRotation(UnityEngine.Vector3)
extern void PathMultiplier_LookRotation_m78DF542B79BB3CE2CCE1A2620619B59A556605F5 (void);
// 0x00000A3B System.Void ChartAndGraph.PathMultiplier::AddJointSegments(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void PathMultiplier_AddJointSegments_mC74B9E4D0A5D7908D9F1B0A2D702BB74DC1529AF (void);
// 0x00000A3C System.Void ChartAndGraph.PathMultiplier::AddCenters(UnityEngine.Vector3,System.Single)
extern void PathMultiplier_AddCenters_mD0B90CD13135C2DB1884C198DA79C8FD1B8F644A (void);
// 0x00000A3D System.Void ChartAndGraph.PathMultiplier::ApplyToMesh(ChartAndGraph.WorldSpaceChartMesh)
extern void PathMultiplier_ApplyToMesh_mC8205718BA1D62EBE8F54D25B716778068F687FA (void);
// 0x00000A3E System.Void ChartAndGraph.PathMultiplier::MultiplyPath(UnityEngine.Vector3[])
extern void PathMultiplier_MultiplyPath_mC39857B7CCF198C040706E5D9A26993D4B99BCB0 (void);
// 0x00000A3F System.Void ChartAndGraph.PathMultiplier::ModifyPath(UnityEngine.Vector3[],System.Boolean)
extern void PathMultiplier_ModifyPath_mD09BFF9A47331B9B9673C366036766525EF21AF7 (void);
// 0x00000A40 System.Void ChartAndGraph.PathMultiplier::.ctor()
extern void PathMultiplier__ctor_m2E832E42E84A57D057ABF236735DC93A7EF346E0 (void);
// 0x00000A41 System.Int32 ChartAndGraph.SmoothPathGenerator::get_JointSmoothingLink()
extern void SmoothPathGenerator_get_JointSmoothingLink_m9C39609C277255F8748FFF0A27760F9DA7AF91E6 (void);
// 0x00000A42 System.Single ChartAndGraph.SmoothPathGenerator::get_JointSizeLink()
extern void SmoothPathGenerator_get_JointSizeLink_m6281A0339CB5D9D11D6794861595FDD85F46B6E4 (void);
// 0x00000A43 System.Void ChartAndGraph.SmoothPathGenerator::Clear()
extern void SmoothPathGenerator_Clear_m7E327B19334F2C37D984247CB022FED7B36A6FBF (void);
// 0x00000A44 System.Void ChartAndGraph.SmoothPathGenerator::OnDestroy()
extern void SmoothPathGenerator_OnDestroy_m8B10F4146409C968C1B13B1ADCC9B871BDBC27FD (void);
// 0x00000A45 System.Boolean ChartAndGraph.SmoothPathGenerator::EnsureMeshFilter()
extern void SmoothPathGenerator_EnsureMeshFilter_m579546E8AD41ADD2418B9F790087783F838CF754 (void);
// 0x00000A46 System.Void ChartAndGraph.SmoothPathGenerator::SetMesh(UnityEngine.Mesh)
extern void SmoothPathGenerator_SetMesh_m314D6469FDC81872A12BA0DAA1D68CB67352CBC9 (void);
// 0x00000A47 UnityEngine.Quaternion ChartAndGraph.SmoothPathGenerator::LookRotation(UnityEngine.Vector3)
extern void SmoothPathGenerator_LookRotation_mBA1E29179CA8A963DF317A5AF23716CD8E4BCB86 (void);
// 0x00000A48 System.Void ChartAndGraph.SmoothPathGenerator::AddJointSegments(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void SmoothPathGenerator_AddJointSegments_m78E8C49A087D0C5BB21CDCA7B9FDE65273FB38A3 (void);
// 0x00000A49 System.Void ChartAndGraph.SmoothPathGenerator::ModifyPath(UnityEngine.Vector3[],System.Boolean,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void SmoothPathGenerator_ModifyPath_mFD40AF348E9FB9C9462306D9FA4A1DC8F2FFD0A9 (void);
// 0x00000A4A System.Void ChartAndGraph.SmoothPathGenerator::ModifyPath(UnityEngine.Vector3[],System.Boolean)
extern void SmoothPathGenerator_ModifyPath_m6061F1F1ACCD736355CDA77F734130A73FD0D351 (void);
// 0x00000A4B System.Void ChartAndGraph.SmoothPathGenerator::.ctor()
extern void SmoothPathGenerator__ctor_m7F0080AA8521913262881666197000FB8AC321FA (void);
// 0x00000A4C System.Void ChartAndGraph.CanvasPieChart::.ctor()
extern void CanvasPieChart__ctor_m0D0D05DE6A15C40F5E2553CB536FDBE518C78173 (void);
// 0x00000A4D ChartAndGraph.PieCanvasGenerator ChartAndGraph.CanvasPieChart::get_Prefab()
extern void CanvasPieChart_get_Prefab_mFA7ADB31F8204A50439275DAB13766255D5E6D35 (void);
// 0x00000A4E System.Void ChartAndGraph.CanvasPieChart::set_Prefab(ChartAndGraph.PieCanvasGenerator)
extern void CanvasPieChart_set_Prefab_m854B47747293842413CCCA678988EA86952F7646 (void);
// 0x00000A4F System.Boolean ChartAndGraph.CanvasPieChart::get_IsCanvas()
extern void CanvasPieChart_get_IsCanvas_mB8A376FE32E803F0C0CDE2B5DA16EEC047EDF8BA (void);
// 0x00000A50 System.Single ChartAndGraph.CanvasPieChart::get_InnerDepthLink()
extern void CanvasPieChart_get_InnerDepthLink_mB82D6B0B206D5696BBE436C835F9A364C9007DB3 (void);
// 0x00000A51 System.Single ChartAndGraph.CanvasPieChart::get_OuterDepthLink()
extern void CanvasPieChart_get_OuterDepthLink_m3E4739B6FA30A523604F27A44C491D3AA06BB22D (void);
// 0x00000A52 UnityEngine.Material ChartAndGraph.CanvasPieChart::get_LineMaterialLink()
extern void CanvasPieChart_get_LineMaterialLink_m58FE291E0EE8BDD0730B000102E09F1BF531D0F0 (void);
// 0x00000A53 System.Single ChartAndGraph.CanvasPieChart::get_LineThicknessLink()
extern void CanvasPieChart_get_LineThicknessLink_m1B3A721D7D481539304779EA12884985259F28D3 (void);
// 0x00000A54 System.Single ChartAndGraph.CanvasPieChart::get_LineSpacingLink()
extern void CanvasPieChart_get_LineSpacingLink_m0E79B3CCED621279ED935C6F0123B0307282EBA0 (void);
// 0x00000A55 System.Single ChartAndGraph.CanvasPieChart::get_LineSpacing()
extern void CanvasPieChart_get_LineSpacing_m7BCE5141269D6A9428661207F7EB31C2E2524E30 (void);
// 0x00000A56 System.Void ChartAndGraph.CanvasPieChart::set_LineSpacing(System.Single)
extern void CanvasPieChart_set_LineSpacing_m746F61A226E58567491CDFFCE2D72BD6C9D88F45 (void);
// 0x00000A57 System.Void ChartAndGraph.CanvasPieChart::ValidateProperties()
extern void CanvasPieChart_ValidateProperties_mE96BB445D5C1FEB8F87A336FF8F6A9996E5338B4 (void);
// 0x00000A58 System.Single ChartAndGraph.CanvasPieChart::get_LineThickness()
extern void CanvasPieChart_get_LineThickness_mBD4AA35A14D6762E6D9C61BD8893E3CD34B0B7BF (void);
// 0x00000A59 System.Void ChartAndGraph.CanvasPieChart::set_LineThickness(System.Single)
extern void CanvasPieChart_set_LineThickness_m6D74D0952014C6064561817583814C3010B84B20 (void);
// 0x00000A5A UnityEngine.Material ChartAndGraph.CanvasPieChart::get_LineMaterial()
extern void CanvasPieChart_get_LineMaterial_m1FCED0C273C1843B57574130D7BD75DE57432C25 (void);
// 0x00000A5B System.Void ChartAndGraph.CanvasPieChart::set_LineMaterial(UnityEngine.Material)
extern void CanvasPieChart_set_LineMaterial_m61036643D6F18BD3C2EEF6BA8F9DA26D2869DCD2 (void);
// 0x00000A5C System.Void ChartAndGraph.CanvasPieChart::InternalGenerateChart()
extern void CanvasPieChart_InternalGenerateChart_m25D36C1384D34F13A60C3EE477A65114135BB5E1 (void);
// 0x00000A5D ChartAndGraph.IPieGenerator ChartAndGraph.CanvasPieChart::PreparePieObject(UnityEngine.GameObject&)
extern void CanvasPieChart_PreparePieObject_m5B1BC1D5508B5A7F0885C7F28ADDADA94472A3FE (void);
// 0x00000A5E System.Void ChartAndGraph.IPieGenerator::Generate(System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single)
// 0x00000A5F System.Void ChartAndGraph.PieCanvasGenerator::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void PieCanvasGenerator_OnFillVBO_m6342BD86F88ECFD2B03B0A3BC1E61FE4DF418DA0 (void);
// 0x00000A60 System.Void ChartAndGraph.PieCanvasGenerator::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void PieCanvasGenerator_OnPopulateMesh_m845364C1C0DC72BCB96236C8E0AD9B6BE1F4E7D3 (void);
// 0x00000A61 System.Void ChartAndGraph.PieCanvasGenerator::FillChartMesh(ChartAndGraph.IChartMesh)
extern void PieCanvasGenerator_FillChartMesh_mD2F9873596C7E21F2C66491DF63C8C4A2792D6E6 (void);
// 0x00000A62 System.Void ChartAndGraph.PieCanvasGenerator::Generate(System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single)
extern void PieCanvasGenerator_Generate_m91D79D36DC6F0CE3D68ED2120AE1B5AFA051104E (void);
// 0x00000A63 System.Boolean ChartAndGraph.PieCanvasGenerator::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern void PieCanvasGenerator_IsRaycastLocationValid_mB69275183754AF7DC0BAAC0063E99A2FD26A1FA0 (void);
// 0x00000A64 System.Void ChartAndGraph.PieCanvasGenerator::.ctor()
extern void PieCanvasGenerator__ctor_m6E34417FCAB752B174EC3210DB7AD7BFFFEF95C1 (void);
// 0x00000A65 System.Boolean ChartAndGraph.PieChart::get_ClockWise()
extern void PieChart_get_ClockWise_mFCB137777262AE9EF5E4083A51733E3FB723D24A (void);
// 0x00000A66 System.Void ChartAndGraph.PieChart::set_ClockWise(System.Boolean)
extern void PieChart_set_ClockWise_m2A4945A952537B1B60ACD3F0636AC845C34C7A94 (void);
// 0x00000A67 ChartAndGraph.IChartData ChartAndGraph.PieChart::get_DataLink()
extern void PieChart_get_DataLink_mB840DF74806D0E6D3193BAE23554B02D4C4007BC (void);
// 0x00000A68 ChartAndGraph.PieData ChartAndGraph.PieChart::get_DataSource()
extern void PieChart_get_DataSource_m2EA58A6ABAF519FBA7F87552870D7F0F5EE48BD2 (void);
// 0x00000A69 System.Single ChartAndGraph.PieChart::get_LineSpacingLink()
// 0x00000A6A System.Single ChartAndGraph.PieChart::get_LineThicknessLink()
// 0x00000A6B UnityEngine.Material ChartAndGraph.PieChart::get_LineMaterialLink()
// 0x00000A6C System.Single ChartAndGraph.PieChart::get_TotalDepthLink()
extern void PieChart_get_TotalDepthLink_m65C1778496C15643E0EBC9CE34D6105AC0F4CF32 (void);
// 0x00000A6D System.Single ChartAndGraph.PieChart::get_TotalHeightLink()
extern void PieChart_get_TotalHeightLink_m22FAD7AD18038CEE17305BB50890C04774B650C2 (void);
// 0x00000A6E System.Single ChartAndGraph.PieChart::get_TotalWidthLink()
extern void PieChart_get_TotalWidthLink_m34B6CA8BF1ADC35DA456F660DD25DE23CCEE4AAF (void);
// 0x00000A6F System.Int32 ChartAndGraph.PieChart::get_MeshSegements()
extern void PieChart_get_MeshSegements_m4A7289B0EF5936C1EF458F5E7DB153412A2065D1 (void);
// 0x00000A70 System.Void ChartAndGraph.PieChart::set_MeshSegements(System.Int32)
extern void PieChart_set_MeshSegements_mEB1CAB1880E6E11E9C21CC6B12DEA955C1F8A697 (void);
// 0x00000A71 System.Single ChartAndGraph.PieChart::get_AngleSpan()
extern void PieChart_get_AngleSpan_m73FAD22D7B1FAB8D98DABC375BD1EDF5A6D63FDD (void);
// 0x00000A72 System.Void ChartAndGraph.PieChart::set_AngleSpan(System.Single)
extern void PieChart_set_AngleSpan_m549F5491905F22C45E8C4BA87CBD8137E3576E0A (void);
// 0x00000A73 System.Single ChartAndGraph.PieChart::get_SpacingAngle()
extern void PieChart_get_SpacingAngle_mA7A171C614EF177CA9A2AB5646B03F394FBBD70F (void);
// 0x00000A74 System.Void ChartAndGraph.PieChart::set_SpacingAngle(System.Single)
extern void PieChart_set_SpacingAngle_m68F5777240098C29D308DEDFCAFFBE115E589D97 (void);
// 0x00000A75 System.Boolean ChartAndGraph.PieChart::get_SupportRealtimeGeneration()
extern void PieChart_get_SupportRealtimeGeneration_m0605EC1A3E165A6F120EC0A0D8C94B7C49D23E98 (void);
// 0x00000A76 System.Single ChartAndGraph.PieChart::get_Radius()
extern void PieChart_get_Radius_m9FA236E3D78F208734749FA6F75A15B74EB7F744 (void);
// 0x00000A77 System.Void ChartAndGraph.PieChart::set_Radius(System.Single)
extern void PieChart_set_Radius_m04EA3281954CE9D9F6A457D77D96BA143ECFC56B (void);
// 0x00000A78 System.Single ChartAndGraph.PieChart::get_TorusRadius()
extern void PieChart_get_TorusRadius_mA58D6DE6B8F5151780F92F9257E55D442827DCB8 (void);
// 0x00000A79 System.Void ChartAndGraph.PieChart::set_TorusRadius(System.Single)
extern void PieChart_set_TorusRadius_m4CD86EEE1A8D607B3A05DB6A69723A5CE900DD60 (void);
// 0x00000A7A System.Single ChartAndGraph.PieChart::get_StartAngle()
extern void PieChart_get_StartAngle_m45404C7A4E159368C869CD6AEE450B7648E9258E (void);
// 0x00000A7B System.Void ChartAndGraph.PieChart::set_StartAngle(System.Single)
extern void PieChart_set_StartAngle_m7CF0D4D475311FA754E80292BEB060095FB15BB0 (void);
// 0x00000A7C System.Single ChartAndGraph.PieChart::get_Extrusion()
extern void PieChart_get_Extrusion_m71CDD0B95441F39C90E9AD46F6032E3725C860A0 (void);
// 0x00000A7D System.Void ChartAndGraph.PieChart::set_Extrusion(System.Single)
extern void PieChart_set_Extrusion_mC02B66637137AB3DF9DCD6906E80C496164DF74B (void);
// 0x00000A7E System.Void ChartAndGraph.PieChart::OnDidApplyAnimationProperties()
extern void PieChart_OnDidApplyAnimationProperties_mAB75039DB9834C72B16695D3E1D3F08CA942A501 (void);
// 0x00000A7F System.Single ChartAndGraph.PieChart::get_InnerDepthLink()
// 0x00000A80 System.Single ChartAndGraph.PieChart::get_OuterDepthLink()
// 0x00000A81 ChartAndGraph.LegenedData ChartAndGraph.PieChart::get_LegendInfo()
extern void PieChart_get_LegendInfo_m478B6EF82793D6F0EEB2E4712B7020A3B4DA97ED (void);
// 0x00000A82 System.Void ChartAndGraph.PieChart::.ctor()
extern void PieChart__ctor_mE67D6F3E162C075707ECBCE49982923CEF1A544D (void);
// 0x00000A83 System.Void ChartAndGraph.PieChart::HookEvents()
extern void PieChart_HookEvents_m61E0478E234C901F3886381FB6BB451BD2E2ACB8 (void);
// 0x00000A84 System.Void ChartAndGraph.PieChart::Data_ProperyUpdated()
extern void PieChart_Data_ProperyUpdated_mF1472C937B837E54F3734BC761C9DFF57622E8AF (void);
// 0x00000A85 System.Void ChartAndGraph.PieChart::QuickInvalidate()
extern void PieChart_QuickInvalidate_mC15DE676DD48660F6806F7BF9367C1A66249A363 (void);
// 0x00000A86 System.Void ChartAndGraph.PieChart::Invalidate()
extern void PieChart_Invalidate_m7AC1DAF0081DAB36348F86C5AE97F696850DC851 (void);
// 0x00000A87 System.Void ChartAndGraph.PieChart::MDataSource_DataValueChanged(System.Object,ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs)
extern void PieChart_MDataSource_DataValueChanged_m7A8F9B03EC93F3A6681A59AECEFE43AF53A7C91A (void);
// 0x00000A88 System.Void ChartAndGraph.PieChart::MDataSource_DataStructureChanged(System.Object,System.EventArgs)
extern void PieChart_MDataSource_DataStructureChanged_m1FDDC6A28D391C8915CB6CBAC72068C7C47D2560 (void);
// 0x00000A89 System.Void ChartAndGraph.PieChart::Start()
extern void PieChart_Start_m9698F1A2547D85AE487C8A0970AD1DBCCA72243B (void);
// 0x00000A8A System.Void ChartAndGraph.PieChart::OnValidate()
extern void PieChart_OnValidate_m542DB0EFE4AB6AF93EABE99CDCCAE5392363B04E (void);
// 0x00000A8B System.Void ChartAndGraph.PieChart::ClearChart()
extern void PieChart_ClearChart_mE55068A075E7939BECCDFEF63152D2536E1A869A (void);
// 0x00000A8C UnityEngine.Vector3 ChartAndGraph.PieChart::AlignTextPosition(ChartAndGraph.AlignedItemLabels,ChartAndGraph.PieChart/PieObject,ChartAndGraph.CanvasLines/LineSegement&,System.Single)
extern void PieChart_AlignTextPosition_m5E2D84F9BBF0795184D343C6E69C56EBD44F5D85 (void);
// 0x00000A8D ChartAndGraph.CanvasLines ChartAndGraph.PieChart::AddLineRenderer(UnityEngine.GameObject,ChartAndGraph.CanvasLines/LineSegement)
extern void PieChart_AddLineRenderer_mCB039E1BF628DA170FBE81B167560EF95661D5E5 (void);
// 0x00000A8E System.Void ChartAndGraph.PieChart::GeneratePie(System.Boolean)
extern void PieChart_GeneratePie_m9DB5F22BB52F987E2F6CEC2C92DE8D2492B1466B (void);
// 0x00000A8F ChartAndGraph.IPieGenerator ChartAndGraph.PieChart::PreparePieObject(UnityEngine.GameObject&)
// 0x00000A90 System.Void ChartAndGraph.PieChart::OnLabelSettingChanged()
extern void PieChart_OnLabelSettingChanged_m8FC252D9ACE47F56BC94FCEC2844FEE9FA864B2D (void);
// 0x00000A91 System.Void ChartAndGraph.PieChart::OnLabelSettingsSet()
extern void PieChart_OnLabelSettingsSet_mC51CB0677BFCEDB73DEAD92C3CF242C0B765CA74 (void);
// 0x00000A92 System.Void ChartAndGraph.PieChart::InternalGenerateChart()
extern void PieChart_InternalGenerateChart_mE04E609DBD0647FAED3E39703B242A8D673261CF (void);
// 0x00000A93 System.Boolean ChartAndGraph.PieChart::HasValues(ChartAndGraph.AxisBase)
extern void PieChart_HasValues_m332A8670E759D3CA705FF52DFB8A1278A0B77434 (void);
// 0x00000A94 System.Double ChartAndGraph.PieChart::MaxValue(ChartAndGraph.AxisBase)
extern void PieChart_MaxValue_m2A4AB3A22C810118C1C3F8AE5393CC240DA8C76B (void);
// 0x00000A95 System.Double ChartAndGraph.PieChart::MinValue(ChartAndGraph.AxisBase)
extern void PieChart_MinValue_m71D76DF3FDA61B120AE739DEEDF360152C31A8B6 (void);
// 0x00000A96 System.Void ChartAndGraph.PieChart::OnPropertyChanged()
extern void PieChart_OnPropertyChanged_m126952AEE52DDF87B3947EC1811845B8D8F9ED70 (void);
// 0x00000A97 System.Void ChartAndGraph.PieChart::OnPropertyUpdated()
extern void PieChart_OnPropertyUpdated_m0F2A8917F537889299030D430F7ED4E12B543120 (void);
// 0x00000A98 System.Void ChartAndGraph.PieChart::ValidateProperties()
extern void PieChart_ValidateProperties_m7BB704663520A38C1773776DAD522025A9D0128B (void);
// 0x00000A99 ChartAndGraph.PieChart/PieEventArgs ChartAndGraph.PieChart::userDataToEventArgs(System.Object)
extern void PieChart_userDataToEventArgs_m34092B0D6D9296DD4FC369A944321104C9AD3E1E (void);
// 0x00000A9A System.Void ChartAndGraph.PieChart::OnNonHoverted()
extern void PieChart_OnNonHoverted_m9EC54D8DEBD959EE6CC78CC0FC60F203E05B5E6C (void);
// 0x00000A9B System.Void ChartAndGraph.PieChart::OnItemHoverted(System.Object)
extern void PieChart_OnItemHoverted_m5B85BE5F9A16330E42807B6119C3A425B78D77FA (void);
// 0x00000A9C System.Void ChartAndGraph.PieChart::OnItemSelected(System.Object)
extern void PieChart_OnItemSelected_mB52D340EB373FD245973AD2BD743A0FEBFB3E347 (void);
// 0x00000A9D System.Void ChartAndGraph.PieChart::Update()
extern void PieChart_Update_m1611C9840407E094EAB2A50DDFA29117AD70AFA7 (void);
// 0x00000A9E System.Boolean ChartAndGraph.PieChart::get_SupportsCategoryLabels()
extern void PieChart_get_SupportsCategoryLabels_m184227D8A7086D3D520F08F0C90CFB176882BDCD (void);
// 0x00000A9F System.Boolean ChartAndGraph.PieChart::get_SupportsGroupLables()
extern void PieChart_get_SupportsGroupLables_m9D4DBADBB5376CBB4E9B56F23009DF29D609E495 (void);
// 0x00000AA0 System.Boolean ChartAndGraph.PieChart::get_SupportsItemLabels()
extern void PieChart_get_SupportsItemLabels_mBBF43FED4601026C9702E7E5EB18D86F39F6493C (void);
// 0x00000AA1 System.Void ChartAndGraph.PieChart/PieEventArgs::.ctor(System.String,System.Double)
extern void PieEventArgs__ctor_m086F6E1E42D63DDE73276C08979BEF70A3120E70 (void);
// 0x00000AA2 System.Double ChartAndGraph.PieChart/PieEventArgs::get_Value()
extern void PieEventArgs_get_Value_m34E34DB40147B026905D90FF0579DF887371A9FF (void);
// 0x00000AA3 System.Void ChartAndGraph.PieChart/PieEventArgs::set_Value(System.Double)
extern void PieEventArgs_set_Value_m57932DB099DD42DDEE99D1F87CE10DB0B6C128C3 (void);
// 0x00000AA4 System.String ChartAndGraph.PieChart/PieEventArgs::get_Category()
extern void PieEventArgs_get_Category_m4104152AEE2A00AF85E1E806EDE4936580E24920 (void);
// 0x00000AA5 System.Void ChartAndGraph.PieChart/PieEventArgs::set_Category(System.String)
extern void PieEventArgs_set_Category_m1D0F3E34466A4EA1B70186116AF07992FC8D6206 (void);
// 0x00000AA6 System.Void ChartAndGraph.PieChart/PieEvent::.ctor()
extern void PieEvent__ctor_mE1F60C9925E2E45CD52A8449667CA6F1C69D1509 (void);
// 0x00000AA7 System.Void ChartAndGraph.PieChart/PieObject::.ctor()
extern void PieObject__ctor_mD178AD6BBBF5509CD0AEFE04441C7B3EFB063522 (void);
// 0x00000AA8 System.Void ChartAndGraph.PieData::.ctor()
extern void PieData__ctor_mFB43B6D6EDA8849D492EF0E10C748F189E7ACF17 (void);
// 0x00000AA9 ChartAndGraph.ChartSparseDataSource ChartAndGraph.PieData::ChartAndGraph.IInternalPieData.get_InternalDataSource()
extern void PieData_ChartAndGraph_IInternalPieData_get_InternalDataSource_mEF2702E9CAD9B8342106C4828DBCF0EBA58AFEA6 (void);
// 0x00000AAA System.Int32 ChartAndGraph.PieData::get_TotalCategories()
extern void PieData_get_TotalCategories_m03E19463EF6CF4ACA3E0B5B4F94C19E18B268233 (void);
// 0x00000AAB System.Void ChartAndGraph.PieData::Update()
extern void PieData_Update_m6AC15684466C0CFA5C809A942DF0E0AF4D850FF7 (void);
// 0x00000AAC System.String ChartAndGraph.PieData::GetCategoryName(System.Int32)
extern void PieData_GetCategoryName_mE5C421B17E2F9CFDE8EB3EA479F983C3BB7319CE (void);
// 0x00000AAD System.Void ChartAndGraph.PieData::OnBeforeSerialize()
extern void PieData_OnBeforeSerialize_mDCC2160261E78966599E79BDE008521E1F90A389 (void);
// 0x00000AAE System.Void ChartAndGraph.PieData::add_ProperyUpdated(System.Action)
extern void PieData_add_ProperyUpdated_m4960F7F4225FF6396F630FDDF953FC61F1E7C15B (void);
// 0x00000AAF System.Void ChartAndGraph.PieData::remove_ProperyUpdated(System.Action)
extern void PieData_remove_ProperyUpdated_m81A424A95A8D71D68AD29F6C4B7EE4103DFD65ED (void);
// 0x00000AB0 System.Void ChartAndGraph.PieData::RaisePropertyUpdated()
extern void PieData_RaisePropertyUpdated_m4593E7A6101DC1B1DA569479A7131D1E0D074C18 (void);
// 0x00000AB1 System.Boolean ChartAndGraph.PieData::HasCategory(System.String)
extern void PieData_HasCategory_m683486CE43763D5DB2AAA8E037FF39F125ED7D28 (void);
// 0x00000AB2 System.Void ChartAndGraph.PieData::RenameCategory(System.String,System.String)
extern void PieData_RenameCategory_m28E3CFDFA1F2EE3F5611609599CC985E6CD73C4F (void);
// 0x00000AB3 System.Object ChartAndGraph.PieData::StoreCategory(System.String)
extern void PieData_StoreCategory_m74BF28D255E9473FC7B3A8C98175227F0D0CBA34 (void);
// 0x00000AB4 System.Void ChartAndGraph.PieData::RestoreCategory(System.String,System.Object)
extern void PieData_RestoreCategory_m4A0D607C1E8C5B04260C4DFAFF18661F01F6C5DB (void);
// 0x00000AB5 System.Void ChartAndGraph.PieData::SetCateogryParams(System.String,System.Single,System.Single,System.Single)
extern void PieData_SetCateogryParams_m6D0D8ED467C333B64572DEC2E3135274C70355DE (void);
// 0x00000AB6 System.Void ChartAndGraph.PieData::StartBatch()
extern void PieData_StartBatch_m4E94F84E7F7F198FA65718237A566E461C1F6194 (void);
// 0x00000AB7 System.Void ChartAndGraph.PieData::EndBatch()
extern void PieData_EndBatch_mC9FC2C884D9D1C74ADCD63552347E436FC410566 (void);
// 0x00000AB8 System.Void ChartAndGraph.PieData::OnAfterDeserialize()
extern void PieData_OnAfterDeserialize_m90AF8B77FBB1E4ED71271C9DEE2E6C671B90E1B0 (void);
// 0x00000AB9 System.Void ChartAndGraph.PieData::AddGroup(System.String)
extern void PieData_AddGroup_mAA390186A7FA0F200934B71D3CE3812EF543DD52 (void);
// 0x00000ABA System.Void ChartAndGraph.PieData::AddCategory(System.String,UnityEngine.Material)
extern void PieData_AddCategory_mDDC8D602D08A6B3CE6D9CDC8D02A4E02E4AF1417 (void);
// 0x00000ABB System.Void ChartAndGraph.PieData::Clear()
extern void PieData_Clear_m8B295EAAC0D69A9F1C3AC8BD7790BA9E8B7D2A08 (void);
// 0x00000ABC System.Void ChartAndGraph.PieData::AddCategory(System.String,ChartAndGraph.ChartDynamicMaterial,System.Single,System.Single,System.Single)
extern void PieData_AddCategory_m1624DCC5FBF814970CBB52BFD9E5AFD1CB22B714 (void);
// 0x00000ABD System.Void ChartAndGraph.PieData::SetMaterial(System.String,UnityEngine.Material)
extern void PieData_SetMaterial_mB52EA8F4A2FBAE39E7B08125C5A19896249D223B (void);
// 0x00000ABE ChartAndGraph.ChartDynamicMaterial ChartAndGraph.PieData::GetMaterial(System.String)
extern void PieData_GetMaterial_mF1A6EDCB2734C520DDAAFA8C46CEC9D5DB1820A2 (void);
// 0x00000ABF System.Void ChartAndGraph.PieData::SetMaterial(System.String,ChartAndGraph.ChartDynamicMaterial)
extern void PieData_SetMaterial_mEC84FADB276984D3D041630C64ECC3A12A3D9C4B (void);
// 0x00000AC0 System.Void ChartAndGraph.PieData::RemoveCategory(System.String)
extern void PieData_RemoveCategory_m94E6DD67A6C2548DCD6AA37FA102343833B4E0E2 (void);
// 0x00000AC1 System.Double ChartAndGraph.PieData::GetValue(System.String)
extern void PieData_GetValue_mB5FEB9E58585E0A291EA6F871512826733A4C61B (void);
// 0x00000AC2 System.Boolean ChartAndGraph.PieData::CheckAnimationEnded(System.Single,UnityEngine.AnimationCurve)
extern void PieData_CheckAnimationEnded_m3F88C840661BB80E5482476FBF46E90EB67E7986 (void);
// 0x00000AC3 System.Object[] ChartAndGraph.PieData::StoreAllCategoriesinOrder()
extern void PieData_StoreAllCategoriesinOrder_mBDECEEF75E9D7C65749BA9F3397F5FDB3F266749 (void);
// 0x00000AC4 System.Void ChartAndGraph.PieData::FixEaseFunction(UnityEngine.AnimationCurve)
extern void PieData_FixEaseFunction_m50ECBDF8EC936BFFE013E122CFDCE99ECDE9B049 (void);
// 0x00000AC5 System.Void ChartAndGraph.PieData::SlideValue(System.String,System.Double,System.Single,UnityEngine.AnimationCurve)
extern void PieData_SlideValue_mD005484D571F83F853101E10170E0BE7A434FC6B (void);
// 0x00000AC6 System.Void ChartAndGraph.PieData::SlideValue(System.String,System.Double,System.Single)
extern void PieData_SlideValue_m8ADE6BDAE78DFE75AB32DCA0C15437A235B72693 (void);
// 0x00000AC7 System.Void ChartAndGraph.PieData::SetValue(System.String,System.Double)
extern void PieData_SetValue_m3C5DE50D73B97FBCE3DD0A2658A8B57B5295AB7A (void);
// 0x00000AC8 System.Void ChartAndGraph.PieData::SetValueInternal(System.String,System.String,System.Double)
extern void PieData_SetValueInternal_mA2A483296791D4C8D3AA657E6005D7103CBC2524 (void);
// 0x00000AC9 System.Void ChartAndGraph.PieData/CategoryData::.ctor()
extern void CategoryData__ctor_m9C63DA3E719D44CD498C63AB2946AFBDAB5A4569 (void);
// 0x00000ACA System.Void ChartAndGraph.PieData/DataEntry::.ctor()
extern void DataEntry__ctor_m322EBF74DDEAF4EB0AAEBC15ED17AA2879B1C375 (void);
// 0x00000ACB System.Void ChartAndGraph.PieData/<>c::.cctor()
extern void U3CU3Ec__cctor_mB3FE18FEE4CB7AECFD410090D4C840CFCB9BA1E9 (void);
// 0x00000ACC System.Void ChartAndGraph.PieData/<>c::.ctor()
extern void U3CU3Ec__ctor_mC9319E3F06175EB32FEDBA4B0BB1898654973860 (void);
// 0x00000ACD System.String ChartAndGraph.PieData/<>c::<Clear>b__28_0(ChartAndGraph.DataSource.ChartDataColumn)
extern void U3CU3Ec_U3CClearU3Eb__28_0_m44DCC4BBEBFAE4B5F1C57F8C5B386CAC57687191 (void);
// 0x00000ACE ChartAndGraph.PieChart/PieObject ChartAndGraph.PieInfo::get_pieObject()
extern void PieInfo_get_pieObject_mDBF95EBF9EF48A131C00E13C4BBC824636127571 (void);
// 0x00000ACF System.Void ChartAndGraph.PieInfo::set_pieObject(ChartAndGraph.PieChart/PieObject)
extern void PieInfo_set_pieObject_m94FFB57B6A64D14E3C5031D72DA5791AC030CC3F (void);
// 0x00000AD0 System.String ChartAndGraph.PieInfo::get_Category()
extern void PieInfo_get_Category_m67E84E4AFB994A57C04B7BD7423C004E8D5F5130 (void);
// 0x00000AD1 System.Void ChartAndGraph.PieInfo::.ctor()
extern void PieInfo__ctor_mA6C1E8F3EBA53EEB2A155EF9E080C7A2F6EBCC86 (void);
// 0x00000AD2 System.Void ChartAndGraph.PieMesh::Generate2dPath(System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Collections.Generic.List`1<System.Int32>,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern void PieMesh_Generate2dPath_m92E63EFC254B384A5D16839A7628E6EF35A2C28B (void);
// 0x00000AD3 System.Void ChartAndGraph.PieMesh::Generate2dMesh(ChartAndGraph.IChartMesh,System.Single,System.Single,System.Single,System.Single,System.Int32)
extern void PieMesh_Generate2dMesh_m2FFE004BFECE53EF9AA67292DA07F122783EA83F (void);
// 0x00000AD4 System.Void ChartAndGraph.PieMesh::Generate3dMesh(ChartAndGraph.WorldSpaceChartMesh,System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single)
extern void PieMesh_Generate3dMesh_m1A6485F1D48C1A27A2290170C4BA84D86D4CECBE (void);
// 0x00000AD5 System.Void ChartAndGraph.PieMesh::.ctor()
extern void PieMesh__ctor_m4FD1F62B7FD11F6193AE2D0BC1972ADBDFE5EF1A (void);
// 0x00000AD6 UnityEngine.Camera ChartAndGraph.WorldSpacePieChart::get_TextCamera()
extern void WorldSpacePieChart_get_TextCamera_mFDAE1CA423D5B2C52C03C83283C2506207B3393D (void);
// 0x00000AD7 System.Void ChartAndGraph.WorldSpacePieChart::set_TextCamera(UnityEngine.Camera)
extern void WorldSpacePieChart_set_TextCamera_m9C0EDE1FDCA1217F9400F1850A3A74FB6424B822 (void);
// 0x00000AD8 System.Single ChartAndGraph.WorldSpacePieChart::get_TextIdleDistance()
extern void WorldSpacePieChart_get_TextIdleDistance_mB1955EDBF33A6ED8635006278C07ADCF143C8712 (void);
// 0x00000AD9 System.Void ChartAndGraph.WorldSpacePieChart::set_TextIdleDistance(System.Single)
extern void WorldSpacePieChart_set_TextIdleDistance_mFD554F60F0C5B1FCC607F4508E46DB2AFB7B045D (void);
// 0x00000ADA UnityEngine.Camera ChartAndGraph.WorldSpacePieChart::get_TextCameraLink()
extern void WorldSpacePieChart_get_TextCameraLink_m65704EAF7E010E7A36D5DBCBBF4423D706E8F8A7 (void);
// 0x00000ADB System.Boolean ChartAndGraph.WorldSpacePieChart::get_IsCanvas()
extern void WorldSpacePieChart_get_IsCanvas_m6F6BE23848DFC45433FBB12515097D481CA6E25D (void);
// 0x00000ADC System.Single ChartAndGraph.WorldSpacePieChart::get_TextIdleDistanceLink()
extern void WorldSpacePieChart_get_TextIdleDistanceLink_m78335F11624737FBF314B83B125C6414C17C3FDD (void);
// 0x00000ADD System.Void ChartAndGraph.WorldSpacePieChart::.ctor()
extern void WorldSpacePieChart__ctor_m0E4D31440D9A8EB198D4C52B0DB862E9403566D7 (void);
// 0x00000ADE UnityEngine.GameObject ChartAndGraph.WorldSpacePieChart::get_Prefab()
extern void WorldSpacePieChart_get_Prefab_m238DD7272D86807CAFC38F47162AFA625237546C (void);
// 0x00000ADF System.Void ChartAndGraph.WorldSpacePieChart::set_Prefab(UnityEngine.GameObject)
extern void WorldSpacePieChart_set_Prefab_mBBD1FB33A5DF4D6F19C80814415577895F8C578F (void);
// 0x00000AE0 System.Single ChartAndGraph.WorldSpacePieChart::get_OuterDepth()
extern void WorldSpacePieChart_get_OuterDepth_mF1FF8B99C566AF38E191AD2E88BDA6405EBA1A52 (void);
// 0x00000AE1 System.Void ChartAndGraph.WorldSpacePieChart::set_OuterDepth(System.Single)
extern void WorldSpacePieChart_set_OuterDepth_mD111B6F36D13674799E14A36424BF7BD8564318F (void);
// 0x00000AE2 System.Single ChartAndGraph.WorldSpacePieChart::get_InnerDepth()
extern void WorldSpacePieChart_get_InnerDepth_mDEDCA1D569687C91AAB8A31DF332E2760A5C6FCA (void);
// 0x00000AE3 System.Void ChartAndGraph.WorldSpacePieChart::set_InnerDepth(System.Single)
extern void WorldSpacePieChart_set_InnerDepth_m81C1E7C36EEF2D5197882A6C001C05364EDF057E (void);
// 0x00000AE4 System.Single ChartAndGraph.WorldSpacePieChart::get_InnerDepthLink()
extern void WorldSpacePieChart_get_InnerDepthLink_m17B23B505994EDAC74AAF1C50E156F093327E316 (void);
// 0x00000AE5 System.Single ChartAndGraph.WorldSpacePieChart::get_OuterDepthLink()
extern void WorldSpacePieChart_get_OuterDepthLink_mF5BD4C4CA5C5F2B70655E881CBB6157CB981CA96 (void);
// 0x00000AE6 ChartAndGraph.IPieGenerator ChartAndGraph.WorldSpacePieChart::PreparePieObject(UnityEngine.GameObject&)
extern void WorldSpacePieChart_PreparePieObject_m2E769E29F3485BF001517AC5DF424A5BC2505574 (void);
// 0x00000AE7 System.Void ChartAndGraph.WorldSpacePieChart::ValidateProperties()
extern void WorldSpacePieChart_ValidateProperties_m32EB68138AC218546B9358B366F7B0A1A97CA783 (void);
// 0x00000AE8 UnityEngine.Material ChartAndGraph.WorldSpacePieChart::get_LineMaterialLink()
extern void WorldSpacePieChart_get_LineMaterialLink_mFCDCC6AF751C5DCCB8557318DF5D1EE709EDFAFD (void);
// 0x00000AE9 System.Single ChartAndGraph.WorldSpacePieChart::get_LineThicknessLink()
extern void WorldSpacePieChart_get_LineThicknessLink_m16A2FA5F79F15E401E2BCD76A58AD01CB4619996 (void);
// 0x00000AEA System.Single ChartAndGraph.WorldSpacePieChart::get_LineSpacingLink()
extern void WorldSpacePieChart_get_LineSpacingLink_mA029269FFF79DA98C773A3BFA395AB4ED0D2288A (void);
// 0x00000AEB System.Void ChartAndGraph.WorldSpacePieGenerator::Start()
extern void WorldSpacePieGenerator_Start_mD9FBF594E0BC4F5C91602276D4F6A6E30D044B5F (void);
// 0x00000AEC System.Void ChartAndGraph.WorldSpacePieGenerator::Generate(System.Single,System.Single,System.Single,System.Single,System.Int32,System.Single,System.Single)
extern void WorldSpacePieGenerator_Generate_mEC59AD4B1E9D550CB386E56A77BE23469D13607A (void);
// 0x00000AED System.Void ChartAndGraph.WorldSpacePieGenerator::OnDestroy()
extern void WorldSpacePieGenerator_OnDestroy_m194936688A6F10E673A93489D95D89C7F45DF215 (void);
// 0x00000AEE System.Void ChartAndGraph.WorldSpacePieGenerator::.ctor()
extern void WorldSpacePieGenerator__ctor_mACE9588E5BC015DD69DE0C6B454CA617E3F64DA4 (void);
// 0x00000AEF ChartAndGraph.RadarFill ChartAndGraph.CanvasRadarChart::CreateFillObject(UnityEngine.GameObject)
extern void CanvasRadarChart_CreateFillObject_mE84CCCD1A5203D55EF4F01042C98A4303C591ACB (void);
// 0x00000AF0 ChartAndGraph.CanvasLines ChartAndGraph.CanvasRadarChart::CreateLinesObject(UnityEngine.GameObject)
extern void CanvasRadarChart_CreateLinesObject_mC20E3CE7F2DB901967F6BCCE9C27372A5C5D9DF5 (void);
// 0x00000AF1 UnityEngine.GameObject ChartAndGraph.CanvasRadarChart::CreateAxisObject(System.Single,UnityEngine.Vector3[])
extern void CanvasRadarChart_CreateAxisObject_mE22109B3D0A6957881ECE8F8AAC8A72A5DA4449B (void);
// 0x00000AF2 System.Void ChartAndGraph.CanvasRadarChart::InternalGenerateChart()
extern void CanvasRadarChart_InternalGenerateChart_mEAA64B0BE56B0328AE6AE59E35C40B5596CF04EB (void);
// 0x00000AF3 UnityEngine.GameObject ChartAndGraph.CanvasRadarChart::CreateCategoryObject(UnityEngine.Vector3[],System.Int32)
extern void CanvasRadarChart_CreateCategoryObject_m960496039E2D5A02EAAE42577CC4E89D594CCD17 (void);
// 0x00000AF4 System.Void ChartAndGraph.CanvasRadarChart::OnEnable()
extern void CanvasRadarChart_OnEnable_m376B677255FC4D46DE92606D0F11ECA452AD9EAE (void);
// 0x00000AF5 System.Void ChartAndGraph.CanvasRadarChart::OnItemHoverted(System.Object)
extern void CanvasRadarChart_OnItemHoverted_mE93590D70158AFD9C25330537610705FC503732E (void);
// 0x00000AF6 System.Void ChartAndGraph.CanvasRadarChart::OnItemLeave(System.Object,System.String)
extern void CanvasRadarChart_OnItemLeave_m55DE371A6EF157669C3D8C57BB74C9F6B99F319C (void);
// 0x00000AF7 System.Boolean ChartAndGraph.CanvasRadarChart::get_IsCanvas()
extern void CanvasRadarChart_get_IsCanvas_mB56D16A567E4DFE9727CCC63C1A99582665BC5B9 (void);
// 0x00000AF8 System.Void ChartAndGraph.CanvasRadarChart::OnItemSelected(System.Object)
extern void CanvasRadarChart_OnItemSelected_m26E1B7280EE52944927DFCE3CF34E735BE753D20 (void);
// 0x00000AF9 System.Void ChartAndGraph.CanvasRadarChart::Points_Click(System.String,System.Int32,UnityEngine.Vector2)
extern void CanvasRadarChart_Points_Click_m01D15DD53BAAAB26740536B371BD1323AAD97984 (void);
// 0x00000AFA System.Void ChartAndGraph.CanvasRadarChart::Points_Leave(System.String)
extern void CanvasRadarChart_Points_Leave_m22182EF45BAE4418422697B3A2881A5DA0122E2E (void);
// 0x00000AFB System.Void ChartAndGraph.CanvasRadarChart::Points_Hover(System.String,System.Int32,UnityEngine.Vector2)
extern void CanvasRadarChart_Points_Hover_mB5F74DD1D87EFCB75EBFB025D6FE18CB858CF186 (void);
// 0x00000AFC System.Void ChartAndGraph.CanvasRadarChart::.ctor()
extern void CanvasRadarChart__ctor_m34838197BC7B999B2BBC8E95520B2E601C400AC3 (void);
// 0x00000AFD System.Void ChartAndGraph.CanvasRadarChart/<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mB156B14C76DE2DB071DF58E1B5D031517B8333B1 (void);
// 0x00000AFE System.Void ChartAndGraph.CanvasRadarChart/<>c__DisplayClass4_0::<CreateCategoryObject>b__0(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CCreateCategoryObjectU3Eb__0_mEDA2B44034101F032233300832FA34E59114FAA3 (void);
// 0x00000AFF System.Void ChartAndGraph.CanvasRadarChart/<>c__DisplayClass4_0::<CreateCategoryObject>b__1()
extern void U3CU3Ec__DisplayClass4_0_U3CCreateCategoryObjectU3Eb__1_mB1DA6A1138507F1F5C71530888BEA147C441DEA8 (void);
// 0x00000B00 System.Void ChartAndGraph.CanvasRadarChart/<>c__DisplayClass4_0::<CreateCategoryObject>b__2(System.Int32,System.Int32,System.Object,UnityEngine.Vector2)
extern void U3CU3Ec__DisplayClass4_0_U3CCreateCategoryObjectU3Eb__2_mEE3056D008A6D479D2A7B1D9F43EA6A877CBC7A6 (void);
// 0x00000B01 ChartAndGraph.ChartSparseDataSource ChartAndGraph.IInternalRadarData::get_InternalDataSource()
// 0x00000B02 System.Double ChartAndGraph.IInternalRadarData::GetMinValue()
// 0x00000B03 System.Double ChartAndGraph.IInternalRadarData::GetMaxValue()
// 0x00000B04 System.Void ChartAndGraph.IInternalRadarData::add_InternalDataChanged(System.EventHandler)
// 0x00000B05 System.Void ChartAndGraph.IInternalRadarData::remove_InternalDataChanged(System.EventHandler)
// 0x00000B06 ChartAndGraph.RadarChartData/CategoryData ChartAndGraph.IInternalRadarData::getCategoryData(System.Int32)
// 0x00000B07 System.Single ChartAndGraph.RadarChart::get_Radius()
extern void RadarChart_get_Radius_m8318721E7CE43B8B5BEC7DE06452938A10283365 (void);
// 0x00000B08 System.Void ChartAndGraph.RadarChart::set_Radius(System.Single)
extern void RadarChart_set_Radius_m527724F79BA03C6FEEDAD13E69A5B6074ED9CB0B (void);
// 0x00000B09 System.Single ChartAndGraph.RadarChart::get_Angle()
extern void RadarChart_get_Angle_m817D37185EA5005394BDDA0D026EAE5179B605BA (void);
// 0x00000B0A System.Void ChartAndGraph.RadarChart::set_Angle(System.Single)
extern void RadarChart_set_Angle_m64C56B7B8B36623FC97702C740C44AA2EA599676 (void);
// 0x00000B0B System.Void ChartAndGraph.RadarChart::AddBillboardText(System.String,BillboardText)
extern void RadarChart_AddBillboardText_m645DE699A39B06C86EDA30624FDF3BC7250B438B (void);
// 0x00000B0C UnityEngine.Material ChartAndGraph.RadarChart::get_AxisPointMaterial()
extern void RadarChart_get_AxisPointMaterial_mA7927B95DC8B39B5A9671304B3C850AD1A389DCB (void);
// 0x00000B0D System.Void ChartAndGraph.RadarChart::set_AxisPointMaterial(UnityEngine.Material)
extern void RadarChart_set_AxisPointMaterial_mEA25067B67FAEBF70CC7F8D36916F52B32E9238A (void);
// 0x00000B0E UnityEngine.Material ChartAndGraph.RadarChart::get_AxisLineMaterial()
extern void RadarChart_get_AxisLineMaterial_mF09A5C8950CF29FA4600B6395D86E38ED8507767 (void);
// 0x00000B0F System.Void ChartAndGraph.RadarChart::set_AxisLineMaterial(UnityEngine.Material)
extern void RadarChart_set_AxisLineMaterial_mE886F785E4B7C1E44369D46E3B624B6772F16289 (void);
// 0x00000B10 System.Void ChartAndGraph.RadarChart::Start()
extern void RadarChart_Start_m25ABEB8F0A1C9F4934CFBB8C9026BA7EDF7B0A99 (void);
// 0x00000B11 System.Single ChartAndGraph.RadarChart::get_AxisThickness()
extern void RadarChart_get_AxisThickness_m01E8C15C5708A2B60DF0573DB5B1791DDBF33D1E (void);
// 0x00000B12 System.Void ChartAndGraph.RadarChart::set_AxisThickness(System.Single)
extern void RadarChart_set_AxisThickness_mC4E4ED2C091B6A657AF6AD75D0E1EC8B6C97606B (void);
// 0x00000B13 System.Single ChartAndGraph.RadarChart::get_AxisPointSize()
extern void RadarChart_get_AxisPointSize_m9C002B1C338AD0DD46D9A1231DFABF42BCCC73A0 (void);
// 0x00000B14 System.Void ChartAndGraph.RadarChart::set_AxisPointSize(System.Single)
extern void RadarChart_set_AxisPointSize_mA084688B46DC33DD3CB045301B32D45422DE3C9B (void);
// 0x00000B15 System.Single ChartAndGraph.RadarChart::get_AxisAdd()
extern void RadarChart_get_AxisAdd_m42BF8E79E424CDC9EF4483FB9E4625CAE96BD6CA (void);
// 0x00000B16 System.Void ChartAndGraph.RadarChart::set_AxisAdd(System.Single)
extern void RadarChart_set_AxisAdd_m0E7F0A977DE3207E9627B8DA49151CC75B122C23 (void);
// 0x00000B17 System.Int32 ChartAndGraph.RadarChart::get_TotalAxisDevisions()
extern void RadarChart_get_TotalAxisDevisions_mDF0636265F972B9ED397CC241D57213E751ECE02 (void);
// 0x00000B18 System.Void ChartAndGraph.RadarChart::set_TotalAxisDevisions(System.Int32)
extern void RadarChart_set_TotalAxisDevisions_m888E549AB0A70C1F80BA778ABDCBBCA7871DA40F (void);
// 0x00000B19 ChartAndGraph.RadarChartData ChartAndGraph.RadarChart::get_DataSource()
extern void RadarChart_get_DataSource_mDDF96F479AA2B9C7FAF63914CF7653066688BA01 (void);
// 0x00000B1A System.Void ChartAndGraph.RadarChart::HookEvents()
extern void RadarChart_HookEvents_m306842507EF6A74AE348C9C9755A4CEA04F27BF6 (void);
// 0x00000B1B System.Void ChartAndGraph.RadarChart::ValueChanged(System.Object,ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs)
extern void RadarChart_ValueChanged_m89F84280DFAC3F7D94F55D902A339E4C1156534E (void);
// 0x00000B1C System.Void ChartAndGraph.RadarChart::StructureUpdated(System.Object,System.EventArgs)
extern void RadarChart_StructureUpdated_mA96622AF76BE9F0B8AF6C10CA6D8D77679E86DDA (void);
// 0x00000B1D System.Void ChartAndGraph.RadarChart::DataUpdated()
extern void RadarChart_DataUpdated_m659B39DC0C75A7A0C4B3646A0DA18AF349899B89 (void);
// 0x00000B1E System.Boolean ChartAndGraph.RadarChart::get_SupportRealtimeGeneration()
extern void RadarChart_get_SupportRealtimeGeneration_m0E533B667BD9ED2470E91D151706C0A3583E5BEF (void);
// 0x00000B1F ChartAndGraph.LegenedData ChartAndGraph.RadarChart::get_LegendInfo()
extern void RadarChart_get_LegendInfo_m0BDC4969FD87EE43962C6DFC9C8274DAB4072F24 (void);
// 0x00000B20 ChartAndGraph.IChartData ChartAndGraph.RadarChart::get_DataLink()
extern void RadarChart_get_DataLink_m065E1235AE5AC6F65EFE09B09C15D1C9984B6A03 (void);
// 0x00000B21 System.Boolean ChartAndGraph.RadarChart::get_SupportsCategoryLabels()
extern void RadarChart_get_SupportsCategoryLabels_m086B8AF3B826DF7E9DDB8AC7F995CE9712FC34F6 (void);
// 0x00000B22 System.Boolean ChartAndGraph.RadarChart::get_SupportsGroupLables()
extern void RadarChart_get_SupportsGroupLables_m7FB6D8ED10855F9134802486F889A93703D7A74D (void);
// 0x00000B23 System.Boolean ChartAndGraph.RadarChart::get_SupportsItemLabels()
extern void RadarChart_get_SupportsItemLabels_mD5385572E838D89C73AE38FCDAF53A6CB8AA65A6 (void);
// 0x00000B24 System.Single ChartAndGraph.RadarChart::get_TotalDepthLink()
extern void RadarChart_get_TotalDepthLink_m2F96E3D16046F2228D732F482568501FF273C712 (void);
// 0x00000B25 System.Single ChartAndGraph.RadarChart::get_TotalHeightLink()
extern void RadarChart_get_TotalHeightLink_m87AD917707F607F410FE471CAC8AFF663F9A937B (void);
// 0x00000B26 System.Single ChartAndGraph.RadarChart::get_TotalWidthLink()
extern void RadarChart_get_TotalWidthLink_mACE5094CEA324AA4306194915DFFBCAF616F2AAC (void);
// 0x00000B27 System.Void ChartAndGraph.RadarChart::Update()
extern void RadarChart_Update_mF6BD08A85BC940337A7A905BDBBDCF61F8164F5F (void);
// 0x00000B28 System.Void ChartAndGraph.RadarChart::OnValidate()
extern void RadarChart_OnValidate_mA76670232387C5623ED6A06A650CC2511011C426 (void);
// 0x00000B29 System.Void ChartAndGraph.RadarChart::ClearChart()
extern void RadarChart_ClearChart_mA148229BCABCEA41F146D9AD97EE79A587C0BEC7 (void);
// 0x00000B2A System.Void ChartAndGraph.RadarChart::LateUpdate()
extern void RadarChart_LateUpdate_m13B67BD9AB084BC67DB62F7D1CD2785DC26992C9 (void);
// 0x00000B2B System.Boolean ChartAndGraph.RadarChart::ItemToWorldPosition(System.String,System.Double,UnityEngine.Vector3&)
extern void RadarChart_ItemToWorldPosition_mCCEE8AD9270F3E41E22020E83E3C62747CBB3B5E (void);
// 0x00000B2C System.Boolean ChartAndGraph.RadarChart::SnapWorldPointToPosition(UnityEngine.Vector3,System.String&,System.Double&)
extern void RadarChart_SnapWorldPointToPosition_m69FC14A4012B47192ABBF3BA0996B398A1806ED4 (void);
// 0x00000B2D System.Void ChartAndGraph.RadarChart::InternalGenerateChart()
extern void RadarChart_InternalGenerateChart_m549DB27FABC19B671A2F36F479F9C1EBA62708E3 (void);
// 0x00000B2E System.Void ChartAndGraph.RadarChart::OnItemSelected(System.Object)
extern void RadarChart_OnItemSelected_mD2570E6F1DF882ED6EF4020C6BD3EB39D99AF959 (void);
// 0x00000B2F System.Void ChartAndGraph.RadarChart::OnItemLeave(System.Object,System.String)
extern void RadarChart_OnItemLeave_mCCCE4A876F76F3CAE2241CD8F97985423A56B42F (void);
// 0x00000B30 System.Void ChartAndGraph.RadarChart::OnItemHoverted(System.Object)
extern void RadarChart_OnItemHoverted_m983DE106593376BC2BB03D472E7E76A73E5AC162 (void);
// 0x00000B31 UnityEngine.GameObject ChartAndGraph.RadarChart::CreateCategoryObject(UnityEngine.Vector3[],System.Int32)
// 0x00000B32 UnityEngine.GameObject ChartAndGraph.RadarChart::CreateAxisObject(System.Single,UnityEngine.Vector3[])
// 0x00000B33 System.Boolean ChartAndGraph.RadarChart::HasValues(ChartAndGraph.AxisBase)
extern void RadarChart_HasValues_m0715B686F30DC04D091FE66B8F57FF9E87C4AB8F (void);
// 0x00000B34 System.Double ChartAndGraph.RadarChart::MaxValue(ChartAndGraph.AxisBase)
extern void RadarChart_MaxValue_m9312A80108F81298488BCA80D37D1035D7E3E6FC (void);
// 0x00000B35 System.Double ChartAndGraph.RadarChart::MinValue(ChartAndGraph.AxisBase)
extern void RadarChart_MinValue_mB31C6FDCE073B880377AB330355C2C63401387C9 (void);
// 0x00000B36 System.Void ChartAndGraph.RadarChart::.ctor()
extern void RadarChart__ctor_mAAE823A6B63E1D29BDA758759B969D8ED80817B6 (void);
// 0x00000B37 System.Boolean ChartAndGraph.RadarChart::<OnItemLeave>b__77_0(System.String)
extern void RadarChart_U3COnItemLeaveU3Eb__77_0_mCD5E6245CDA4737B40897F15288E0A869E9F23B7 (void);
// 0x00000B38 System.Void ChartAndGraph.RadarChart/RadarEventArgs::.ctor(System.String,System.String,System.Double,UnityEngine.Vector3,System.Int32)
extern void RadarEventArgs__ctor_mA1E00EF3B49C1667A98A9CBF751D99A2C196A34B (void);
// 0x00000B39 System.Int32 ChartAndGraph.RadarChart/RadarEventArgs::get_Index()
extern void RadarEventArgs_get_Index_m62007451B3AA6C66F095A4C35B8B7B8EC5E484A9 (void);
// 0x00000B3A System.Void ChartAndGraph.RadarChart/RadarEventArgs::set_Index(System.Int32)
extern void RadarEventArgs_set_Index_mA4261CF977A216CAF9751F99FC765CDAA82F8A59 (void);
// 0x00000B3B System.String ChartAndGraph.RadarChart/RadarEventArgs::get_Category()
extern void RadarEventArgs_get_Category_m9311857293FCD862890CAEC7153DFFF54AC0A15A (void);
// 0x00000B3C System.Void ChartAndGraph.RadarChart/RadarEventArgs::set_Category(System.String)
extern void RadarEventArgs_set_Category_m9A98ADC9A460C6B28C355DFF54D344DEB0BCFBAA (void);
// 0x00000B3D System.String ChartAndGraph.RadarChart/RadarEventArgs::get_Group()
extern void RadarEventArgs_get_Group_m0006840DB7F009CC5BE3E17EEEBD03B79AB7526D (void);
// 0x00000B3E System.Void ChartAndGraph.RadarChart/RadarEventArgs::set_Group(System.String)
extern void RadarEventArgs_set_Group_mE1D6C61197C2FB44AC097B9F052595AA93D9D60E (void);
// 0x00000B3F System.Double ChartAndGraph.RadarChart/RadarEventArgs::get_Value()
extern void RadarEventArgs_get_Value_mB6E37324C906F817C5B2E4755A1CA24293C5DB4D (void);
// 0x00000B40 System.Void ChartAndGraph.RadarChart/RadarEventArgs::set_Value(System.Double)
extern void RadarEventArgs_set_Value_mA19E1B1C45A5B260E50606F70636DB311AF02501 (void);
// 0x00000B41 UnityEngine.Vector3 ChartAndGraph.RadarChart/RadarEventArgs::get_Position()
extern void RadarEventArgs_get_Position_mB4AC59E9FCD59B2EE4E4F20F81778D7DFA6DD9BD (void);
// 0x00000B42 System.Void ChartAndGraph.RadarChart/RadarEventArgs::set_Position(UnityEngine.Vector3)
extern void RadarEventArgs_set_Position_mD64D46B5CEB08F082E4BC6D2B53A714228152CB9 (void);
// 0x00000B43 System.Void ChartAndGraph.RadarChart/RadarEvent::.ctor()
extern void RadarEvent__ctor_m9B10C5DDCECCD2F11C2978755F155C3F827A75BC (void);
// 0x00000B44 System.Void ChartAndGraph.RadarChartData::Add3DCategory(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Single,UnityEngine.GameObject,UnityEngine.Material,System.Single,UnityEngine.Material,System.Int32,System.Single,System.Single)
extern void RadarChartData_Add3DCategory_m130E06E50720F05B371D7751CBF383D40FCF4190 (void);
// 0x00000B45 System.Void ChartAndGraph.RadarChartData::Set3DCategoryOrientation(System.String,System.Single,System.Single)
extern void RadarChartData_Set3DCategoryOrientation_m3F7BCBD6AC90C3F56C3D687029889EC061880205 (void);
// 0x00000B46 System.Void ChartAndGraph.RadarChartData::Set3DCategoryFill(System.String,UnityEngine.Material,System.Int32)
extern void RadarChartData_Set3DCategoryFill_mADF46298559B857D171236FD1791951AD57B2B06 (void);
// 0x00000B47 System.Void ChartAndGraph.RadarChartData::Set3DCategoryLine(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Single)
extern void RadarChartData_Set3DCategoryLine_m336DEA8A3FF5440B17D58808C7030254D7FF37F3 (void);
// 0x00000B48 System.Void ChartAndGraph.RadarChartData::Set3DCategoryPoint(System.String,UnityEngine.GameObject,UnityEngine.Material,System.Single)
extern void RadarChartData_Set3DCategoryPoint_m876E582161714135D3D6D96A9B35C86CBF301AE3 (void);
// 0x00000B49 ChartAndGraph.ChartSparseDataSource ChartAndGraph.RadarChartData::ChartAndGraph.IInternalRadarData.get_InternalDataSource()
extern void RadarChartData_ChartAndGraph_IInternalRadarData_get_InternalDataSource_m6AC4C3114C247644A7BCF9875F27A89F84960265 (void);
// 0x00000B4A System.Void ChartAndGraph.RadarChartData::add_DataChanged(System.EventHandler)
extern void RadarChartData_add_DataChanged_m99D8EA4C37777EEE904B2472F067B27701E3DE25 (void);
// 0x00000B4B System.Void ChartAndGraph.RadarChartData::remove_DataChanged(System.EventHandler)
extern void RadarChartData_remove_DataChanged_m1E3D1F74E6E7B12E51D57747E9E72B85111CD90B (void);
// 0x00000B4C System.Void ChartAndGraph.RadarChartData::add_ProperyUpdated(System.Action)
extern void RadarChartData_add_ProperyUpdated_m94ED02CCC412FD825345AE1C6D9D000D3EC14005 (void);
// 0x00000B4D System.Void ChartAndGraph.RadarChartData::remove_ProperyUpdated(System.Action)
extern void RadarChartData_remove_ProperyUpdated_mCFABC49D427FE3F42604FE13456B70DFC4B01304 (void);
// 0x00000B4E System.Void ChartAndGraph.RadarChartData::RaisePropertyUpdated()
extern void RadarChartData_RaisePropertyUpdated_m85CBDCA8FA513E2E522A244EF08E8B57BEA94231 (void);
// 0x00000B4F System.Void ChartAndGraph.RadarChartData::RaiseDataChanged()
extern void RadarChartData_RaiseDataChanged_m913925A2ADF57160C1A7B601AD1BE8DAD8125CD3 (void);
// 0x00000B50 System.Int32 ChartAndGraph.RadarChartData::get_TotalCategories()
extern void RadarChartData_get_TotalCategories_m64459272A6219D14AF321A0E3CAE11909A42263A (void);
// 0x00000B51 System.Int32 ChartAndGraph.RadarChartData::get_TotalGroups()
extern void RadarChartData_get_TotalGroups_mE126A19EB3F10824DCC0FF3BD3638073A4F28038 (void);
// 0x00000B52 System.Void ChartAndGraph.RadarChartData::RenameCategory(System.String,System.String)
extern void RadarChartData_RenameCategory_m040D719A27246E2F7337C4B378903A237BC398D2 (void);
// 0x00000B53 System.Void ChartAndGraph.RadarChartData::RenameGroup(System.String,System.String)
extern void RadarChartData_RenameGroup_m55173E2CF110A173732B96F14C0B2900ABB77AE9 (void);
// 0x00000B54 System.Boolean ChartAndGraph.RadarChartData::get_AutomaticMaxValue()
extern void RadarChartData_get_AutomaticMaxValue_m348712965E81ACEF31462FE970A71560FAE19924 (void);
// 0x00000B55 System.Void ChartAndGraph.RadarChartData::set_AutomaticMaxValue(System.Boolean)
extern void RadarChartData_set_AutomaticMaxValue_m9EE5266D3C4C2FE011C4F6B44D2F3A7EB6ABDC49 (void);
// 0x00000B56 System.Double ChartAndGraph.RadarChartData::get_MaxValue()
extern void RadarChartData_get_MaxValue_m790303C375045BDF3033F35EF21BB79E5EFBF6B2 (void);
// 0x00000B57 System.Void ChartAndGraph.RadarChartData::set_MaxValue(System.Double)
extern void RadarChartData_set_MaxValue_mC7F8DAA096D703AC7E4B6FD1A7DCCBC3E61E4188 (void);
// 0x00000B58 System.Boolean ChartAndGraph.RadarChartData::get_AutomaticMinValue()
extern void RadarChartData_get_AutomaticMinValue_m49F9D64491C3605E417247C8FCF53D134B350453 (void);
// 0x00000B59 System.Void ChartAndGraph.RadarChartData::set_AutomaticMinValue(System.Boolean)
extern void RadarChartData_set_AutomaticMinValue_mCDDD54C26724124F243DE346E5F33A514FE8CE76 (void);
// 0x00000B5A System.Double ChartAndGraph.RadarChartData::get_MinValue()
extern void RadarChartData_get_MinValue_m921EAB67CF981900348A48CE09CFAE0A559AA0FC (void);
// 0x00000B5B System.Void ChartAndGraph.RadarChartData::set_MinValue(System.Double)
extern void RadarChartData_set_MinValue_m632CD37D5224E6E613AA9C34A86C9CD2806AF4F8 (void);
// 0x00000B5C System.Void ChartAndGraph.RadarChartData::Update()
extern void RadarChartData_Update_mBFC46C0D68C76A5AD53AAA706995E1BC84CD42EB (void);
// 0x00000B5D System.Void ChartAndGraph.RadarChartData::StartBatch()
extern void RadarChartData_StartBatch_mC70F5847873D3B2C1904FAF7F4BC575B95535F94 (void);
// 0x00000B5E System.Void ChartAndGraph.RadarChartData::EndBatch()
extern void RadarChartData_EndBatch_m9F7E8593991858075D5E242E68CA0C5C2532B8B5 (void);
// 0x00000B5F System.Double ChartAndGraph.RadarChartData::GetMinValue()
extern void RadarChartData_GetMinValue_m9C9F4851112F97B1A518AD6E29D5FA50F27678F3 (void);
// 0x00000B60 System.Boolean ChartAndGraph.RadarChartData::HasGroup(System.String)
extern void RadarChartData_HasGroup_m8695CE055ACD348AD36159DBA1F4898F15459FEA (void);
// 0x00000B61 System.Double ChartAndGraph.RadarChartData::GetMaxValue()
extern void RadarChartData_GetMaxValue_mEB8DB85D9F5B1BCB66EE509917E69B85DDF6A384 (void);
// 0x00000B62 System.Boolean ChartAndGraph.RadarChartData::HasCategory(System.String)
extern void RadarChartData_HasCategory_m01D7C8E1AB982ABA20AD06AC3E081C9D5BE2A7DF (void);
// 0x00000B63 System.String ChartAndGraph.RadarChartData::GetCategoryName(System.Int32)
extern void RadarChartData_GetCategoryName_m4A3B0335843AE3C2CBF123078375765BA2408567 (void);
// 0x00000B64 System.Double ChartAndGraph.RadarChartData::GetCategoryMaxValue(System.Int32)
extern void RadarChartData_GetCategoryMaxValue_m008D3C169C2FF87BC13725FB83FE1639C09CEBEB (void);
// 0x00000B65 System.Void ChartAndGraph.RadarChartData::SetCategoryMaxValue(System.String,System.Double)
extern void RadarChartData_SetCategoryMaxValue_m5CDFEFB1322EF5843222773D10DCE73946296950 (void);
// 0x00000B66 System.Int32 ChartAndGraph.RadarChartData::GetGroupIndex(System.String)
extern void RadarChartData_GetGroupIndex_m2D5768659D2B959A5328DD2C4DE981FFD46DB28B (void);
// 0x00000B67 System.String ChartAndGraph.RadarChartData::GetGroupName(System.Int32)
extern void RadarChartData_GetGroupName_mDD7F9D5330A0A19AECEB621E2F6303FB9F4786BD (void);
// 0x00000B68 System.Object[] ChartAndGraph.RadarChartData::StoreAllCategoriesinOrder()
extern void RadarChartData_StoreAllCategoriesinOrder_mD5A61B66E34BD21FF0FF5498283E1162590D5322 (void);
// 0x00000B69 System.Void ChartAndGraph.RadarChartData::OnBeforeSerialize()
extern void RadarChartData_OnBeforeSerialize_mEED2F3E6184CEA410AD18832AD3EC5C23E804077 (void);
// 0x00000B6A System.Void ChartAndGraph.RadarChartData::OnAfterDeserialize()
extern void RadarChartData_OnAfterDeserialize_m0C45500DB9E5900352C9DAC5CFE75F1C4967E0EB (void);
// 0x00000B6B System.Void ChartAndGraph.RadarChartData::AddCategory(System.String,ChartAndGraph.RadarChartData/CategoryData)
extern void RadarChartData_AddCategory_mE5CDBFD3F5AC1BE0A1D68EC8274091A8BAEAA0AA (void);
// 0x00000B6C System.Void ChartAndGraph.RadarChartData::ClearGroups()
extern void RadarChartData_ClearGroups_mFA3ECC13EA8F0E9E6D2D429505214DD865CEBC0F (void);
// 0x00000B6D System.Void ChartAndGraph.RadarChartData::ClearCategories()
extern void RadarChartData_ClearCategories_mB51296D801643E4D1655A36D181185A7B412F8DB (void);
// 0x00000B6E System.Void ChartAndGraph.RadarChartData::AddCategory(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Single,UnityEngine.GameObject,UnityEngine.Material,System.Single,UnityEngine.Material)
extern void RadarChartData_AddCategory_mE8074171A469325092AC49FC59D1BF2C00B76ECA (void);
// 0x00000B6F System.Void ChartAndGraph.RadarChartData::AddInnerCategory(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Single,UnityEngine.GameObject,UnityEngine.Material,System.Single,UnityEngine.Material,System.Int32,System.Single,System.Single)
extern void RadarChartData_AddInnerCategory_mE94BC7FC9215A338F7E1D7D6610F95FD2E461B26 (void);
// 0x00000B70 System.Void ChartAndGraph.RadarChartData::SetCategoryHover(System.String,ChartAndGraph.ChartItemEffect,ChartAndGraph.ChartItemEffect)
extern void RadarChartData_SetCategoryHover_m7EA49FCD93E367C16662ACD8CD32A2E5ACE1EBCE (void);
// 0x00000B71 System.Void ChartAndGraph.RadarChartData::SetInnerCategoryFill(System.String,UnityEngine.Material,System.Int32)
extern void RadarChartData_SetInnerCategoryFill_mC9601A1BFDA8448DDF46765B0966FF1F082AA95E (void);
// 0x00000B72 System.Void ChartAndGraph.RadarChartData::SetInnerCategoryLine(System.String,ChartAndGraph.PathGenerator,UnityEngine.Material,System.Single)
extern void RadarChartData_SetInnerCategoryLine_m9FDC2510CDFCA4449BCB19E7528D094EA3A7C811 (void);
// 0x00000B73 System.Void ChartAndGraph.RadarChartData::SetInnerCategoryPoint(System.String,UnityEngine.GameObject,UnityEngine.Material,System.Single)
extern void RadarChartData_SetInnerCategoryPoint_m81E11EDE8B768EE225813DCCF3743151C3D97BE4 (void);
// 0x00000B74 System.Void ChartAndGraph.RadarChartData::SetCategoryFill(System.String,UnityEngine.Material)
extern void RadarChartData_SetCategoryFill_m48C4D1A12E900C13414192E4F4531F0195C02C1D (void);
// 0x00000B75 System.Void ChartAndGraph.RadarChartData::SetCategoryLine(System.String,UnityEngine.Material,System.Single)
extern void RadarChartData_SetCategoryLine_m3FF12F349A37BC994A5B07DBC020A162A9794200 (void);
// 0x00000B76 System.Void ChartAndGraph.RadarChartData::SetCategoryPoint(System.String,UnityEngine.Material,System.Single)
extern void RadarChartData_SetCategoryPoint_m626597C9774A604D5EF11923520D927F440F44AF (void);
// 0x00000B77 System.Void ChartAndGraph.RadarChartData::SetMaterial(System.String,UnityEngine.Material)
extern void RadarChartData_SetMaterial_mB358824E191056E603B0FD16713D7951DD2D650C (void);
// 0x00000B78 ChartAndGraph.ChartDynamicMaterial ChartAndGraph.RadarChartData::GetMaterial(System.String)
extern void RadarChartData_GetMaterial_mEE8559CBC26738273C3AC5B9D8E8F852E4419828 (void);
// 0x00000B79 System.Void ChartAndGraph.RadarChartData::SetMaterial(System.String,ChartAndGraph.ChartDynamicMaterial)
extern void RadarChartData_SetMaterial_mA4B2759040229EBBA347ED2736F39187AE48350A (void);
// 0x00000B7A System.Void ChartAndGraph.RadarChartData::RemoveCategory(System.String)
extern void RadarChartData_RemoveCategory_m9D0A3334267E96F3E4919F469261A39EDB1C1327 (void);
// 0x00000B7B System.Void ChartAndGraph.RadarChartData::RestoreCategory(System.String,System.Object)
extern void RadarChartData_RestoreCategory_mAC5189FF3298E0B9A9D69FAE5A222C6C99E63539 (void);
// 0x00000B7C System.Void ChartAndGraph.RadarChartData::RemoveGroup(System.String)
extern void RadarChartData_RemoveGroup_m924DC8293C1D58D47E016D0AB40D1C6F9C8B79F9 (void);
// 0x00000B7D System.Void ChartAndGraph.RadarChartData::AddGroup(System.String)
extern void RadarChartData_AddGroup_m1F74100E9748C2D4003A034BFA6F6292AB880904 (void);
// 0x00000B7E System.Double ChartAndGraph.RadarChartData::GetValue(System.String,System.String)
extern void RadarChartData_GetValue_m002560F54544DF76554D66C4D38FE6B9E30BA69E (void);
// 0x00000B7F System.Boolean ChartAndGraph.RadarChartData::CheckAnimationEnded(System.Single,UnityEngine.AnimationCurve)
extern void RadarChartData_CheckAnimationEnded_mAD0C5D51EB0F3904B6512C28EEF207AC7F6E1A94 (void);
// 0x00000B80 System.Void ChartAndGraph.RadarChartData::FixEaseFunction(UnityEngine.AnimationCurve)
extern void RadarChartData_FixEaseFunction_m6D500A3AB6A269DC71B5822077C634AA14CDDD43 (void);
// 0x00000B81 System.Void ChartAndGraph.RadarChartData::SlideValue(System.String,System.String,System.Double,System.Single,UnityEngine.AnimationCurve)
extern void RadarChartData_SlideValue_mD20632B9B3E6ACCACD827E9DAA5300E1333A8F68 (void);
// 0x00000B82 System.Void ChartAndGraph.RadarChartData::SlideValue(System.String,System.String,System.Double,System.Single)
extern void RadarChartData_SlideValue_mF88966F4D0C9CCC81E6C3FEFC880C1ED01355089 (void);
// 0x00000B83 System.Void ChartAndGraph.RadarChartData::SetValue(System.String,System.String,System.Double)
extern void RadarChartData_SetValue_m3B44AF655063A77B53C29A048352EE3B085AB470 (void);
// 0x00000B84 System.Void ChartAndGraph.RadarChartData::SetValueInternal(System.String,System.String,System.Double)
extern void RadarChartData_SetValueInternal_m9DB16C8DF97FB4D4CC5EB5F879F5BEDD239B8584 (void);
// 0x00000B85 ChartAndGraph.RadarChartData/CategoryData ChartAndGraph.RadarChartData::ChartAndGraph.IInternalRadarData.getCategoryData(System.Int32)
extern void RadarChartData_ChartAndGraph_IInternalRadarData_getCategoryData_mCC4D8AE683FED0FDCDF1404B74FA1429D920E998 (void);
// 0x00000B86 System.Void ChartAndGraph.RadarChartData::ChartAndGraph.IInternalRadarData.add_InternalDataChanged(System.EventHandler)
extern void RadarChartData_ChartAndGraph_IInternalRadarData_add_InternalDataChanged_m741C827E0D695F9C3CC703B400352E69AF3FA5BB (void);
// 0x00000B87 System.Void ChartAndGraph.RadarChartData::ChartAndGraph.IInternalRadarData.remove_InternalDataChanged(System.EventHandler)
extern void RadarChartData_ChartAndGraph_IInternalRadarData_remove_InternalDataChanged_mE513DEBBBF0B81FE4172E3532AE52D65E9B3FAD2 (void);
// 0x00000B88 System.Void ChartAndGraph.RadarChartData::.ctor()
extern void RadarChartData__ctor_m32273BD441DD9474CB345D14AEAD3900201EF0F9 (void);
// 0x00000B89 System.Void ChartAndGraph.RadarChartData/CategoryData::.ctor()
extern void CategoryData__ctor_m9832E3233589FF77B473687D603FB448CCE6B536 (void);
// 0x00000B8A System.Void ChartAndGraph.RadarChartData/DataEntry::.ctor()
extern void DataEntry__ctor_mB775C8234DB7D698783D05015656305277BF74BE (void);
// 0x00000B8B System.Void ChartAndGraph.RadarChartData/<>c::.cctor()
extern void U3CU3Ec__cctor_m2AD9301513291F7B2B6528A5F2CA72BC540816A3 (void);
// 0x00000B8C System.Void ChartAndGraph.RadarChartData/<>c::.ctor()
extern void U3CU3Ec__ctor_m3DEFE7B4A55719CD1DAE2C06DD154E76E2BC9CD4 (void);
// 0x00000B8D System.String ChartAndGraph.RadarChartData/<>c::<ClearGroups>b__59_0(ChartAndGraph.DataSource.ChartDataRow)
extern void U3CU3Ec_U3CClearGroupsU3Eb__59_0_m54484CBB1E8A76B71B9D97DA61F6BEAA6818D0CF (void);
// 0x00000B8E System.String ChartAndGraph.RadarChartData/<>c::<ClearCategories>b__60_0(ChartAndGraph.DataSource.ChartDataColumn)
extern void U3CU3Ec_U3CClearCategoriesU3Eb__60_0_m0A8F4F4E4299B0CAFAAFADA47E48F49FD6593ED7 (void);
// 0x00000B8F System.Void ChartAndGraph.RadarFill::.ctor()
extern void RadarFill__ctor_m0313F6B94057C40CBFB2FC6C109451B9DA25C4EC (void);
// 0x00000B90 System.Void ChartAndGraph.RadarFill::SetPath(UnityEngine.Vector3[],System.Single)
extern void RadarFill_SetPath_m233FEF3BF93EF2D5D833174646306BCE352B03D8 (void);
// 0x00000B91 System.Void ChartAndGraph.RadarFill::UpdateMaterial()
extern void RadarFill_UpdateMaterial_mC9982E2583E6F5E1A0937155D68E649E4C182746 (void);
// 0x00000B92 System.Void ChartAndGraph.RadarFill::OnDisable()
extern void RadarFill_OnDisable_mF0C072E47D293E8E2B1F5EEA6A61C42640B5444E (void);
// 0x00000B93 UnityEngine.Material ChartAndGraph.RadarFill::get_material()
extern void RadarFill_get_material_mE06D8FE51309D3A6AA0C35C4A07B86232657271A (void);
// 0x00000B94 System.Void ChartAndGraph.RadarFill::set_material(UnityEngine.Material)
extern void RadarFill_set_material_m3314AD9E5E8660D7B7D268B4B7A26D64CB32EC7D (void);
// 0x00000B95 UnityEngine.Vector2 ChartAndGraph.RadarFill::InterpolateInViewRect(UnityEngine.Vector3)
extern void RadarFill_InterpolateInViewRect_m7D35B6BFE5E16239827A14160871D278B7FB9E7F (void);
// 0x00000B96 System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.RadarFill::getVerices()
extern void RadarFill_getVerices_m93B92F5B9E70EC783CF194DC2AAAD21F7DC12707 (void);
// 0x00000B97 System.Void ChartAndGraph.RadarFill::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void RadarFill_OnPopulateMesh_m723D5D656F46320E0B6218121035CC29F593714F (void);
// 0x00000B98 System.Void ChartAndGraph.RadarFill/<getVerices>d__11::.ctor(System.Int32)
extern void U3CgetVericesU3Ed__11__ctor_m95EAA0E125642D785F6170A26A63A985795114EC (void);
// 0x00000B99 System.Void ChartAndGraph.RadarFill/<getVerices>d__11::System.IDisposable.Dispose()
extern void U3CgetVericesU3Ed__11_System_IDisposable_Dispose_mA707AEC25B6F42499A916D5625F27CC57C387E70 (void);
// 0x00000B9A System.Boolean ChartAndGraph.RadarFill/<getVerices>d__11::MoveNext()
extern void U3CgetVericesU3Ed__11_MoveNext_mC280799E2CD850DF504CF0A846FBACC3AD8D96EF (void);
// 0x00000B9B UnityEngine.UIVertex ChartAndGraph.RadarFill/<getVerices>d__11::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m0ABBFDCEE2D7145EBE071FCFE6684F6BAFEC3AF9 (void);
// 0x00000B9C System.Void ChartAndGraph.RadarFill/<getVerices>d__11::System.Collections.IEnumerator.Reset()
extern void U3CgetVericesU3Ed__11_System_Collections_IEnumerator_Reset_mE28A7F9BD47AA43584DAFA502888ACF0235FB4F3 (void);
// 0x00000B9D System.Object ChartAndGraph.RadarFill/<getVerices>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CgetVericesU3Ed__11_System_Collections_IEnumerator_get_Current_m53326F4FE9B852D2151D3849221DE6279144AAFF (void);
// 0x00000B9E System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.RadarFill/<getVerices>d__11::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mFE0741653FF30F25813799307B8423F0B24C7C53 (void);
// 0x00000B9F System.Collections.IEnumerator ChartAndGraph.RadarFill/<getVerices>d__11::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetVericesU3Ed__11_System_Collections_IEnumerable_GetEnumerator_mC4C94D6FA75874BC56F1392AE45985B0A0F0F00E (void);
// 0x00000BA0 System.Boolean ChartAndGraph.RadarFillGenerator::EnsureMeshFilter()
extern void RadarFillGenerator_EnsureMeshFilter_m708F04B71F0292D732B6467115059A2D251FC95A (void);
// 0x00000BA1 UnityEngine.Vector2 ChartAndGraph.RadarFillGenerator::InterpolateInViewRect(UnityEngine.Vector3)
extern void RadarFillGenerator_InterpolateInViewRect_m219D9F3BF7B6C39BC7DEE426D764A02D23E87F0B (void);
// 0x00000BA2 UnityEngine.Vector3 ChartAndGraph.RadarFillGenerator::curve(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void RadarFillGenerator_curve_m9CE5144398806A9D062F105693339CE7475F293C (void);
// 0x00000BA3 System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex> ChartAndGraph.RadarFillGenerator::getVerices()
extern void RadarFillGenerator_getVerices_mF7C8A6EBFC00C77EDB871799777143548EC570EF (void);
// 0x00000BA4 System.Void ChartAndGraph.RadarFillGenerator::Generate(UnityEngine.Vector3[],System.Single,System.Single)
extern void RadarFillGenerator_Generate_mD633997BF5ADC452ECB5E159DFEC57BB6F2DA55B (void);
// 0x00000BA5 System.Void ChartAndGraph.RadarFillGenerator::Clear()
extern void RadarFillGenerator_Clear_mB624624C76C84837D8FB75FBE6CDCF259EB40213 (void);
// 0x00000BA6 System.Void ChartAndGraph.RadarFillGenerator::OnDestroy()
extern void RadarFillGenerator_OnDestroy_m7DFC9D02618E463B3FFA68C2312938FC77D72134 (void);
// 0x00000BA7 System.Void ChartAndGraph.RadarFillGenerator::.ctor()
extern void RadarFillGenerator__ctor_m2AA33BDD3FD11DAB3D9AEA0AA87CB1A1E44CBE7A (void);
// 0x00000BA8 System.Void ChartAndGraph.RadarFillGenerator/<getVerices>d__9::.ctor(System.Int32)
extern void U3CgetVericesU3Ed__9__ctor_mAC242DA1B337EB73A1ACF54B5008C8982895E7BE (void);
// 0x00000BA9 System.Void ChartAndGraph.RadarFillGenerator/<getVerices>d__9::System.IDisposable.Dispose()
extern void U3CgetVericesU3Ed__9_System_IDisposable_Dispose_mA5B4335205957942C2C19D8CF215EE867ADE91DA (void);
// 0x00000BAA System.Boolean ChartAndGraph.RadarFillGenerator/<getVerices>d__9::MoveNext()
extern void U3CgetVericesU3Ed__9_MoveNext_mA19A6718F5D410AA11B9E4A2742E0CA0FCE2ADC1 (void);
// 0x00000BAB UnityEngine.UIVertex ChartAndGraph.RadarFillGenerator/<getVerices>d__9::System.Collections.Generic.IEnumerator<UnityEngine.UIVertex>.get_Current()
extern void U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m26748669262E86E2C5B4B00464074D5101846F44 (void);
// 0x00000BAC System.Void ChartAndGraph.RadarFillGenerator/<getVerices>d__9::System.Collections.IEnumerator.Reset()
extern void U3CgetVericesU3Ed__9_System_Collections_IEnumerator_Reset_m2806ECA59982D7E816ABAEE95594A91198113387 (void);
// 0x00000BAD System.Object ChartAndGraph.RadarFillGenerator/<getVerices>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CgetVericesU3Ed__9_System_Collections_IEnumerator_get_Current_mEC742CBC4BFF915F4437AFE99135CE5D95A38D2C (void);
// 0x00000BAE System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex> ChartAndGraph.RadarFillGenerator/<getVerices>d__9::System.Collections.Generic.IEnumerable<UnityEngine.UIVertex>.GetEnumerator()
extern void U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m18B10F0B05365FD3906E1B6F7909DDAB07663928 (void);
// 0x00000BAF System.Collections.IEnumerator ChartAndGraph.RadarFillGenerator/<getVerices>d__9::System.Collections.IEnumerable.GetEnumerator()
extern void U3CgetVericesU3Ed__9_System_Collections_IEnumerable_GetEnumerator_mE68D27E149F9F1E558455CA8B7C987FB5485CD25 (void);
// 0x00000BB0 ChartAndGraph.PathGenerator ChartAndGraph.WorldSpaceRadarChart::get_AxisPrefab()
extern void WorldSpaceRadarChart_get_AxisPrefab_m1B090BDD3151E89FDB6E10141B1BFD962F9E7A47 (void);
// 0x00000BB1 System.Void ChartAndGraph.WorldSpaceRadarChart::set_AxisPrefab(ChartAndGraph.PathGenerator)
extern void WorldSpaceRadarChart_set_AxisPrefab_m6858D3C6CD8F6D7C3BECD45CE4BDE0C44CBC651C (void);
// 0x00000BB2 UnityEngine.GameObject ChartAndGraph.WorldSpaceRadarChart::get_AxisPointPrefab()
extern void WorldSpaceRadarChart_get_AxisPointPrefab_mF1BF25C84798B3AB9E7E8519F27E3BDA4C6190FC (void);
// 0x00000BB3 System.Void ChartAndGraph.WorldSpaceRadarChart::set_AxisPointPrefab(UnityEngine.GameObject)
extern void WorldSpaceRadarChart_set_AxisPointPrefab_m3784BCAC3AAC49DFA8F0E52B4F26603C12A5DB31 (void);
// 0x00000BB4 UnityEngine.GameObject ChartAndGraph.WorldSpaceRadarChart::CreateAxisObject(System.Single,UnityEngine.Vector3[])
extern void WorldSpaceRadarChart_CreateAxisObject_m364FE323E71618652FBFA481DB43CD32F70E5782 (void);
// 0x00000BB5 UnityEngine.GameObject ChartAndGraph.WorldSpaceRadarChart::CreatePrefab(UnityEngine.GameObject,UnityEngine.GameObject)
extern void WorldSpaceRadarChart_CreatePrefab_m15318BAA6D75AB9D4261B44DECA9327AE3E9B144 (void);
// 0x00000BB6 ChartAndGraph.RadarFillGenerator ChartAndGraph.WorldSpaceRadarChart::CreateFillObject(UnityEngine.GameObject)
extern void WorldSpaceRadarChart_CreateFillObject_mC638A0CCDC17755DD65A6B5835BDE6CEDF984A46 (void);
// 0x00000BB7 System.Boolean ChartAndGraph.WorldSpaceRadarChart::get_IsCanvas()
extern void WorldSpaceRadarChart_get_IsCanvas_m56F7FE3420454551D10E54FA7FFE4CD64736309C (void);
// 0x00000BB8 UnityEngine.GameObject ChartAndGraph.WorldSpaceRadarChart::CreateCategoryObject(UnityEngine.Vector3[],System.Int32)
extern void WorldSpaceRadarChart_CreateCategoryObject_m67D55F775C49BF3014B09775CAA1202CC50A1AF4 (void);
// 0x00000BB9 System.Void ChartAndGraph.WorldSpaceRadarChart::.ctor()
extern void WorldSpaceRadarChart__ctor_mA274F974380017450FFD8B7D1092F18A8472D93E (void);
// 0x00000BBA ChartAndGraph.ScrollableChartData ChartAndGraph.ScrollableAxisChart::get_ScrollableData()
extern void ScrollableAxisChart_get_ScrollableData_mF6B505446F40A825D01C5D05B9C0B276181BBBBD (void);
// 0x00000BBB System.Boolean ChartAndGraph.ScrollableAxisChart::get_AutoScrollHorizontally()
extern void ScrollableAxisChart_get_AutoScrollHorizontally_m0119650CF15A68143E03351A5CDB8EC6BE591744 (void);
// 0x00000BBC System.Void ChartAndGraph.ScrollableAxisChart::set_AutoScrollHorizontally(System.Boolean)
extern void ScrollableAxisChart_set_AutoScrollHorizontally_m5ED34138D96AF142165479908982263DBA0D7469 (void);
// 0x00000BBD System.Void ChartAndGraph.ScrollableAxisChart::Update()
extern void ScrollableAxisChart_Update_mB88F71FF8803E8007544F908D203FA0A6E79CC93 (void);
// 0x00000BBE System.Void ChartAndGraph.ScrollableAxisChart::GenerateRealtime()
extern void ScrollableAxisChart_GenerateRealtime_m6CD2F771D39503CD10FAA0B2672CFE408CD6D066 (void);
// 0x00000BBF System.Double ChartAndGraph.ScrollableAxisChart::GetScrollOffset(System.Int32)
extern void ScrollableAxisChart_GetScrollOffset_m6AA9D13986C5D2E3C46EDAF92AAF042CA819E7AD (void);
// 0x00000BC0 System.Void ChartAndGraph.ScrollableAxisChart::AddBillboardText(System.String,System.Int32,BillboardText)
extern void ScrollableAxisChart_AddBillboardText_m26DA501F3E62CCF8AB9A1C86845444E567E4A462 (void);
// 0x00000BC1 System.Void ChartAndGraph.ScrollableAxisChart::ClearBillboard()
extern void ScrollableAxisChart_ClearBillboard_m5BC2AE0997B7E6590F993BCA2A0062212794662D (void);
// 0x00000BC2 System.Void ChartAndGraph.ScrollableAxisChart::ClearBillboardCategories()
extern void ScrollableAxisChart_ClearBillboardCategories_m2AD60E7521A0CCFD9B44DA4A9BBACA734B42012B (void);
// 0x00000BC3 System.Boolean ChartAndGraph.ScrollableAxisChart::get_Scrollable()
extern void ScrollableAxisChart_get_Scrollable_m20D6DDF3AC3C161A0D93610B3244C1A7ABAE7257 (void);
// 0x00000BC4 System.Void ChartAndGraph.ScrollableAxisChart::set_Scrollable(System.Boolean)
extern void ScrollableAxisChart_set_Scrollable_m9A8BDC045D11031CA97020A4D65682F676C72C2C (void);
// 0x00000BC5 System.Double ChartAndGraph.ScrollableAxisChart::get_HorizontalScrolling()
extern void ScrollableAxisChart_get_HorizontalScrolling_m89B95AEC4C79BEF0F7D03B12E72EA090097ADEB9 (void);
// 0x00000BC6 System.Void ChartAndGraph.ScrollableAxisChart::set_HorizontalScrolling(System.Double)
extern void ScrollableAxisChart_set_HorizontalScrolling_mCDA5CCBF57D9502DCA231D3B40FDB1704A29877B (void);
// 0x00000BC7 System.Boolean ChartAndGraph.ScrollableAxisChart::get_AutoScrollVertically()
extern void ScrollableAxisChart_get_AutoScrollVertically_mF27BB4C25493A8D4B866E53E93563721D980C005 (void);
// 0x00000BC8 System.Void ChartAndGraph.ScrollableAxisChart::set_AutoScrollVertically(System.Boolean)
extern void ScrollableAxisChart_set_AutoScrollVertically_m6FB6FB491B56531D6461AA9ECC6B278BF3E8E947 (void);
// 0x00000BC9 System.Double ChartAndGraph.ScrollableAxisChart::get_VerticalScrolling()
extern void ScrollableAxisChart_get_VerticalScrolling_m79372E57FD6B44B33DBB5744CF5C555B476E906B (void);
// 0x00000BCA System.Void ChartAndGraph.ScrollableAxisChart::set_VerticalScrolling(System.Double)
extern void ScrollableAxisChart_set_VerticalScrolling_m6271CE083E9237183DE9B58EF8C3CB2AE49F39A3 (void);
// 0x00000BCB System.Boolean ChartAndGraph.ScrollableAxisChart::get_RaycastTarget()
extern void ScrollableAxisChart_get_RaycastTarget_mEED1B5750596D66777103EFA63B16209B744E9C9 (void);
// 0x00000BCC System.Void ChartAndGraph.ScrollableAxisChart::set_RaycastTarget(System.Boolean)
extern void ScrollableAxisChart_set_RaycastTarget_mCCC2BA8FCCF02988A16205E5B23E43C6522DFD34 (void);
// 0x00000BCD System.Boolean ChartAndGraph.ScrollableAxisChart::PointToClient(UnityEngine.Vector3,System.Double&,System.DateTime&)
extern void ScrollableAxisChart_PointToClient_m92EAB332DDDA386A003208EBF3E7BE906FF3B9FB (void);
// 0x00000BCE System.Boolean ChartAndGraph.ScrollableAxisChart::PointToClient(UnityEngine.Vector3,System.DateTime&,System.DateTime&)
extern void ScrollableAxisChart_PointToClient_m3777F4C5CD77CDA447AF211F98198CFFA307D7C5 (void);
// 0x00000BCF System.Boolean ChartAndGraph.ScrollableAxisChart::PointToClient(UnityEngine.Vector3,System.DateTime&,System.Double&)
extern void ScrollableAxisChart_PointToClient_m723CA78010FD72D2CA6FFEDB5865F60D06BC2454 (void);
// 0x00000BD0 System.Boolean ChartAndGraph.ScrollableAxisChart::PointToWorldSpace(UnityEngine.Vector3&,System.DateTime,System.Double,System.String)
extern void ScrollableAxisChart_PointToWorldSpace_m3914970E17E7F0527513EFFB60A30C352088BCC5 (void);
// 0x00000BD1 System.Boolean ChartAndGraph.ScrollableAxisChart::PointToWorldSpace(UnityEngine.Vector3&,System.Double,System.DateTime,System.String)
extern void ScrollableAxisChart_PointToWorldSpace_mDC31114BA9343ED423C8B60D327F25724A5619E3 (void);
// 0x00000BD2 System.Boolean ChartAndGraph.ScrollableAxisChart::PointToWorldSpace(UnityEngine.Vector3&,System.DateTime,System.DateTime,System.String)
extern void ScrollableAxisChart_PointToWorldSpace_m3D8ACB12C8D0AB38D9B2CA4CAC17C6A33BD5C697 (void);
// 0x00000BD3 System.Double ChartAndGraph.ScrollableAxisChart::GetCategoryDepth(System.String)
// 0x00000BD4 System.Void ChartAndGraph.ScrollableAxisChart::SetAsMixedSeries()
// 0x00000BD5 ChartAndGraph.DoubleVector3 ChartAndGraph.ScrollableAxisChart::PointToNormalized(System.Double,System.Double)
extern void ScrollableAxisChart_PointToNormalized_mA13C59E2931C28D63CF24669AAFEE1A51E69DF8E (void);
// 0x00000BD6 ChartAndGraph.DoubleVector3 ChartAndGraph.ScrollableAxisChart::NormalizedToPoint(System.Double,System.Double)
extern void ScrollableAxisChart_NormalizedToPoint_m3214950D806AF4830E2597271BFC39C381FCC736 (void);
// 0x00000BD7 UnityEngine.Vector3 ChartAndGraph.ScrollableAxisChart::get_PointShift()
extern void ScrollableAxisChart_get_PointShift_mAD73FA814A124CFD102CC0E0CE0D093CF3841386 (void);
// 0x00000BD8 UnityEngine.Vector3 ChartAndGraph.ScrollableAxisChart::get_CanvasFitOffset()
extern void ScrollableAxisChart_get_CanvasFitOffset_m10E03FAD4C331CC26EB009F8F37097CDCBEC6FB9 (void);
// 0x00000BD9 System.Boolean ChartAndGraph.ScrollableAxisChart::PointToWorldSpace(UnityEngine.Vector3&,System.Double,System.Double,System.String)
extern void ScrollableAxisChart_PointToWorldSpace_m7EEDDAFC797F298F8586FAA7DE548336B0807D11 (void);
// 0x00000BDA System.Boolean ChartAndGraph.ScrollableAxisChart::MouseToClient(System.Double&,System.Double&)
extern void ScrollableAxisChart_MouseToClient_mA8EE4656273AD78AF80ACA4380E4C42AED8020D0 (void);
// 0x00000BDB System.Boolean ChartAndGraph.ScrollableAxisChart::PointToClient(UnityEngine.Vector3,System.Double&,System.Double&)
extern void ScrollableAxisChart_PointToClient_m45A693188B6BBB8DCBD9FD1B442F877ADC93B33E (void);
// 0x00000BDC System.Boolean ChartAndGraph.ScrollableAxisChart::TrimRect(ChartAndGraph.DoubleRect,ChartAndGraph.DoubleRect&)
extern void ScrollableAxisChart_TrimRect_mB40DAD04B18B238E3343D647A858FD7A4624CC78 (void);
// 0x00000BDD System.Boolean ChartAndGraph.ScrollableAxisChart::IsRectVisible(ChartAndGraph.DoubleRect)
extern void ScrollableAxisChart_IsRectVisible_m9D14A660695D71226B59964D49DD774ED185D744 (void);
// 0x00000BDE System.Boolean ChartAndGraph.ScrollableAxisChart::RectToCanvas(UnityEngine.RectTransform,ChartAndGraph.DoubleRect,System.String)
extern void ScrollableAxisChart_RectToCanvas_m68D2BFA24F1847EE1B56CD609A6552EC54999AB2 (void);
// 0x00000BDF System.Single ChartAndGraph.ScrollableAxisChart::GetScrollingRange(System.Int32)
// 0x00000BE0 System.Boolean ChartAndGraph.ScrollableAxisChart::get_HorizontalPanning()
extern void ScrollableAxisChart_get_HorizontalPanning_m4D2E6510CC1061CE22D3EF0F6F36F88B5756D7F1 (void);
// 0x00000BE1 System.Void ChartAndGraph.ScrollableAxisChart::set_HorizontalPanning(System.Boolean)
extern void ScrollableAxisChart_set_HorizontalPanning_m11D6AD0F91299ADDCB2DE627430C13093D3B4E71 (void);
// 0x00000BE2 System.Boolean ChartAndGraph.ScrollableAxisChart::get_VerticalPanning()
extern void ScrollableAxisChart_get_VerticalPanning_m4D040CD390A540F7F11D3402267319887DEA756C (void);
// 0x00000BE3 System.Void ChartAndGraph.ScrollableAxisChart::set_VerticalPanning(System.Boolean)
extern void ScrollableAxisChart_set_VerticalPanning_mB37E5FBCDE3448752DF6DF516433F01B9DDD15CD (void);
// 0x00000BE4 UnityEngine.GameObject ChartAndGraph.ScrollableAxisChart::CreateRectMask(UnityEngine.Rect)
extern void ScrollableAxisChart_CreateRectMask_m337026D0A97ADE0A0162371636D7B2A05297B58D (void);
// 0x00000BE5 System.Void ChartAndGraph.ScrollableAxisChart::ClearChart()
extern void ScrollableAxisChart_ClearChart_m11BB4672C0CD5DF042764411C7675B4FAA86E768 (void);
// 0x00000BE6 System.String ChartAndGraph.ScrollableAxisChart::StringFromAxisFormat(ChartAndGraph.DoubleVector3,ChartAndGraph.AxisBase,System.Boolean)
extern void ScrollableAxisChart_StringFromAxisFormat_mD391FEFB4D49311A8A505EE7B940481B09F81C3A (void);
// 0x00000BE7 System.String ChartAndGraph.ScrollableAxisChart::StringFromAxisFormat(ChartAndGraph.DoubleVector3,ChartAndGraph.AxisBase,System.Int32,System.Boolean)
extern void ScrollableAxisChart_StringFromAxisFormat_m459D838D430A4E89026F3E64B51FB4908821892F (void);
// 0x00000BE8 System.Void ChartAndGraph.ScrollableAxisChart::GetScrollParams(System.Double&,System.Double&,System.Double&,System.Double&,System.Double&,System.Double&,System.Double&,System.Double&,System.Double&)
extern void ScrollableAxisChart_GetScrollParams_mC50B26C21345FBFB4AA4E16AF0E277A468CC25F1 (void);
// 0x00000BE9 System.Void ChartAndGraph.ScrollableAxisChart::MouseDraged(UnityEngine.Vector2)
extern void ScrollableAxisChart_MouseDraged_mA0C6908F207EC8FBF64855B4B1D82746B085280D (void);
// 0x00000BEA System.Void ChartAndGraph.ScrollableAxisChart::HandleMouseDrag()
extern void ScrollableAxisChart_HandleMouseDrag_m73A25D0D90F58AD361FC68DC62C9B809EA014E2D (void);
// 0x00000BEB System.Void ChartAndGraph.ScrollableAxisChart::TriggerActiveTextsOut()
extern void ScrollableAxisChart_TriggerActiveTextsOut_m81E5F13BCC7C7FCFF62E97018B9D19858D012B14 (void);
// 0x00000BEC System.Void ChartAndGraph.ScrollableAxisChart::AddActiveText(BillboardText)
extern void ScrollableAxisChart_AddActiveText_mAEBB053AEE6A03A4A79F8789B418A9FF09D1870F (void);
// 0x00000BED System.Void ChartAndGraph.ScrollableAxisChart::SelectActiveText(BillboardText)
extern void ScrollableAxisChart_SelectActiveText_m174B8A7A926499B5374DA1C9E546D80BDBB45B77 (void);
// 0x00000BEE System.Void ChartAndGraph.ScrollableAxisChart::.ctor()
extern void ScrollableAxisChart__ctor_m267104CD7AF077C36E087B856ABD8125157C45F7 (void);
// 0x00000BEF ChartLabelAlignment ChartAndGraph.AlignedItemLabels::get_Alignment()
extern void AlignedItemLabels_get_Alignment_mECB6C3B817CB67F8F417EB5A99BA99B0AF5BD451 (void);
// 0x00000BF0 System.Void ChartAndGraph.AlignedItemLabels::set_Alignment(ChartLabelAlignment)
extern void AlignedItemLabels_set_Alignment_m831FC445E8263B1C4F7C327F834634A853D50E7D (void);
// 0x00000BF1 System.Void ChartAndGraph.AlignedItemLabels::.ctor()
extern void AlignedItemLabels__ctor_mFA74C6A2F31A81643C7A400FA5685A3279997809 (void);
// 0x00000BF2 System.Int32 ChartAndGraph.ItemLabels::get_FractionDigits()
extern void ItemLabels_get_FractionDigits_m82BF3FF9409EF8305A14022754B7857ADF6DB566 (void);
// 0x00000BF3 System.Void ChartAndGraph.ItemLabels::set_FractionDigits(System.Int32)
extern void ItemLabels_set_FractionDigits_m7B02640144572DA0EC5C7C927E8FADAE316B59A6 (void);
// 0x00000BF4 System.Action`2<ChartAndGraph.IInternalUse,System.Boolean> ChartAndGraph.ItemLabels::get_Assign()
extern void ItemLabels_get_Assign_m088722D6792ED742B2537A592938FE18ACA27283 (void);
// 0x00000BF5 System.Void ChartAndGraph.ItemLabels::.ctor()
extern void ItemLabels__ctor_m7F365D10C7DC8760ACAE0AB08ABEA2B50098F698 (void);
// 0x00000BF6 System.Void ChartAndGraph.ItemLabels::<get_Assign>b__5_0(ChartAndGraph.IInternalUse,System.Boolean)
extern void ItemLabels_U3Cget_AssignU3Eb__5_0_m961E93586578FB887F59CD9628AEC9CE40E8C929 (void);
// 0x00000BF7 System.Void ChartAndGraph.TextDirection::Start()
extern void TextDirection_Start_m52920200EF741582D800E1FBE7637BC988D3B040 (void);
// 0x00000BF8 System.Void ChartAndGraph.TextDirection::SetTextController(TextController)
extern void TextDirection_SetTextController_mA891F1CAD539228F24B23B187786B0B5AFBD3EA4 (void);
// 0x00000BF9 System.Void ChartAndGraph.TextDirection::SetRelativeTo(UnityEngine.Transform,UnityEngine.Transform)
extern void TextDirection_SetRelativeTo_mA1BF2964DA4802337856175D7E014381B1C6A5E3 (void);
// 0x00000BFA System.Void ChartAndGraph.TextDirection::LateUpdate()
extern void TextDirection_LateUpdate_m681E8C5668E9C3A25F1C42F708E76A5C34900CE1 (void);
// 0x00000BFB System.Void ChartAndGraph.TextDirection::SetDirection(System.Single)
extern void TextDirection_SetDirection_m9202893CE4705CA57CA37251F35ED252E3AC120A (void);
// 0x00000BFC System.Void ChartAndGraph.TextDirection::SetDirection(UnityEngine.Vector3)
extern void TextDirection_SetDirection_mA61EAE1203BB917D55BA197CC1A877358EEC8916 (void);
// 0x00000BFD System.Void ChartAndGraph.TextDirection::.ctor()
extern void TextDirection__ctor_m25491B9E3CCC345F953E9DBC6E477DF736747987 (void);
// 0x00000BFE System.String ChartAndGraph.TextFormatting::get_Suffix()
extern void TextFormatting_get_Suffix_mD60BF477085B8EF4BA141764BA3BB306814E98A8 (void);
// 0x00000BFF System.Void ChartAndGraph.TextFormatting::set_Suffix(System.String)
extern void TextFormatting_set_Suffix_mA100B03ABD880619CDAB53DF6ABE006B71F3E0E1 (void);
// 0x00000C00 System.String ChartAndGraph.TextFormatting::get_Prefix()
extern void TextFormatting_get_Prefix_m3D4865298EF50EE7DD09C32F5251126951C25AAE (void);
// 0x00000C01 System.Void ChartAndGraph.TextFormatting::set_Prefix(System.String)
extern void TextFormatting_set_Prefix_m8F85B66321B6FB246514F0FA9D5C2D60CBA684E2 (void);
// 0x00000C02 System.String ChartAndGraph.TextFormatting::get_CustomFormat()
extern void TextFormatting_get_CustomFormat_mD981569C543D767A4E3D64EC98B1CC5C29E03CE8 (void);
// 0x00000C03 System.Void ChartAndGraph.TextFormatting::set_CustomFormat(System.String)
extern void TextFormatting_set_CustomFormat_m9666A3CD1B30F98D3828D40681C43D09DE5B73F2 (void);
// 0x00000C04 System.Void ChartAndGraph.TextFormatting::add_OnDataUpdate(System.EventHandler)
extern void TextFormatting_add_OnDataUpdate_m2F65466175314C9D5ED7FE225FE1BD80ADBEDBBB (void);
// 0x00000C05 System.Void ChartAndGraph.TextFormatting::remove_OnDataUpdate(System.EventHandler)
extern void TextFormatting_remove_OnDataUpdate_mA5F909354E837998B946D9345C788F83D4CDF9B9 (void);
// 0x00000C06 System.Void ChartAndGraph.TextFormatting::add_OnDataChanged(System.EventHandler)
extern void TextFormatting_add_OnDataChanged_m8C1F23C5AD0504F224A32812FA950812A484F1AA (void);
// 0x00000C07 System.Void ChartAndGraph.TextFormatting::remove_OnDataChanged(System.EventHandler)
extern void TextFormatting_remove_OnDataChanged_m1FCA55929D4EFD9908E2D572D8240F8069F0497F (void);
// 0x00000C08 System.Void ChartAndGraph.TextFormatting::RaiseOnChanged()
extern void TextFormatting_RaiseOnChanged_m869D7B0A2965421425FD54F74D68F175DD3D4422 (void);
// 0x00000C09 System.Void ChartAndGraph.TextFormatting::RaiseOnUpdate()
extern void TextFormatting_RaiseOnUpdate_m0632B9BD9178D8DBAB5254C9E84ECFE9D0E46136 (void);
// 0x00000C0A System.Void ChartAndGraph.TextFormatting::ChartAndGraph.IInternalSettings.add_InternalOnDataUpdate(System.EventHandler)
extern void TextFormatting_ChartAndGraph_IInternalSettings_add_InternalOnDataUpdate_mE1B274D666F185B5481859D4A53C65A21CEC5549 (void);
// 0x00000C0B System.Void ChartAndGraph.TextFormatting::ChartAndGraph.IInternalSettings.remove_InternalOnDataUpdate(System.EventHandler)
extern void TextFormatting_ChartAndGraph_IInternalSettings_remove_InternalOnDataUpdate_m063CF339F9C62982533F5369DF08872AF9708A97 (void);
// 0x00000C0C System.Void ChartAndGraph.TextFormatting::ChartAndGraph.IInternalSettings.add_InternalOnDataChanged(System.EventHandler)
extern void TextFormatting_ChartAndGraph_IInternalSettings_add_InternalOnDataChanged_m45291A45F01E4DD7688D1E6546506ABF12D1E007 (void);
// 0x00000C0D System.Void ChartAndGraph.TextFormatting::ChartAndGraph.IInternalSettings.remove_InternalOnDataChanged(System.EventHandler)
extern void TextFormatting_ChartAndGraph_IInternalSettings_remove_InternalOnDataChanged_m6A4E4E2D0A8593DD2B2111D927D80A92E343F0DB (void);
// 0x00000C0E System.String ChartAndGraph.TextFormatting::FormatKeywords(System.String,System.String,System.String)
extern void TextFormatting_FormatKeywords_m6DE37BFDFC22EEB565DBD764002D5352FF1C0237 (void);
// 0x00000C0F System.Void ChartAndGraph.TextFormatting::FormatKeywords(System.Text.StringBuilder,System.String,System.String)
extern void TextFormatting_FormatKeywords_mC8218DC10A38C97C40A0DB73D503EEE5E5C9A652 (void);
// 0x00000C10 System.String ChartAndGraph.TextFormatting::ValidString(System.String)
extern void TextFormatting_ValidString_m1A0C04C7C125E882D77A7AA1408E96BCBB6AF836 (void);
// 0x00000C11 System.Void ChartAndGraph.TextFormatting::Format(System.Text.StringBuilder,System.String,System.String,System.String)
extern void TextFormatting_Format_m84D20D445C001711330E560E1109B6B1F8E89AC1 (void);
// 0x00000C12 System.String ChartAndGraph.TextFormatting::Format(System.String,System.String,System.String)
extern void TextFormatting_Format_mC7F6EF9E7F6F13F2FB2024C76244ED0679F21EC6 (void);
// 0x00000C13 System.Void ChartAndGraph.TextFormatting::.ctor()
extern void TextFormatting__ctor_m25E5FC8D31710F0A9BC49FDD34BC8BB86B38AF0E (void);
// 0x00000C14 System.Void ChartAndGraph.PieAnimation::Start()
extern void PieAnimation_Start_m8B31A7265C85482C8A431B865A81316540955B35 (void);
// 0x00000C15 System.Void ChartAndGraph.PieAnimation::OnEnable()
extern void PieAnimation_OnEnable_m9D4B16130434DF8A08DB82FBB4BF0DB8EB66A185 (void);
// 0x00000C16 System.Void ChartAndGraph.PieAnimation::Animate()
extern void PieAnimation_Animate_mDD9ABEA9092357464BD61530DA1E471FC8E44A20 (void);
// 0x00000C17 System.Void ChartAndGraph.PieAnimation::Update()
extern void PieAnimation_Update_m26C407791F9536275651E92FDDE20DE333DD8E9B (void);
// 0x00000C18 System.Void ChartAndGraph.PieAnimation::.ctor()
extern void PieAnimation__ctor_m05BA7F8B9535B821DA5C8AB1332F13A22D9C6168 (void);
// 0x00000C19 System.Void ChartAndGraph.SelectScene::Start()
extern void SelectScene_Start_m28D362FCD02A06755ABC5219946A66055A2EAA38 (void);
// 0x00000C1A System.Void ChartAndGraph.SelectScene::ChangeCanvas()
extern void SelectScene_ChangeCanvas_m0EABB3FF2CD8616219670C6A03E48BC415A52F51 (void);
// 0x00000C1B System.Void ChartAndGraph.SelectScene::Select(System.String)
extern void SelectScene_Select_mBDB0B21D990C0124D7C8165E4A852BD415EF162E (void);
// 0x00000C1C System.Void ChartAndGraph.SelectScene::SelectMain()
extern void SelectScene_SelectMain_m7B23C7E5C250046326542829CDA7ECE32BA37749 (void);
// 0x00000C1D System.Void ChartAndGraph.SelectScene::.ctor()
extern void SelectScene__ctor_mAD684180B1324A8ACC6C896AB8357975FBCC9053 (void);
// 0x00000C1E System.Void ChartAndGraph.SelectScene/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mD7F0B3A3E430A93B3DE0DD0272749EFB146F2F8C (void);
// 0x00000C1F System.Void ChartAndGraph.SelectScene/<>c__DisplayClass6_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CStartU3Eb__0_m6094D067135F047261703D726A6AAC79533E633A (void);
// 0x00000C20 System.Void ChartAndGraph.BubbleGraphFeed::Start()
extern void BubbleGraphFeed_Start_m47505E34E07E5518CAFEB7A3371313536AD25382 (void);
// 0x00000C21 System.Void ChartAndGraph.BubbleGraphFeed::.ctor()
extern void BubbleGraphFeed__ctor_m117A1C9DF31730D9FBD3DD33B15A7E48E8EADAC6 (void);
// 0x00000C22 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Texture2D> ChartAndGraph.Legened.CanvasLegend::CreateimageDictionary()
extern void CanvasLegend_CreateimageDictionary_m43DFF4711EFF207CE0F0897C8A33A376D6AA0157 (void);
// 0x00000C23 System.Int32 ChartAndGraph.Legened.CanvasLegend::get_FontSize()
extern void CanvasLegend_get_FontSize_m11FC61224312844E27302F5372FA7A107BB01751 (void);
// 0x00000C24 System.Void ChartAndGraph.Legened.CanvasLegend::set_FontSize(System.Int32)
extern void CanvasLegend_set_FontSize_m5BA01C235A686523E10B3846C7C7F30E28CBBA74 (void);
// 0x00000C25 ChartAndGraph.Legened.CanvasLegendItem ChartAndGraph.Legened.CanvasLegend::get_LegenedItemPrefab()
extern void CanvasLegend_get_LegenedItemPrefab_m22A1217E0A1B0B27AEC45CDF5751DDCF296AAA2A (void);
// 0x00000C26 System.Void ChartAndGraph.Legened.CanvasLegend::set_LegenedItemPrefab(ChartAndGraph.Legened.CanvasLegendItem)
extern void CanvasLegend_set_LegenedItemPrefab_mFFA536B2CECD0FE6A658B84A1580EC0EAABFE970 (void);
// 0x00000C27 ChartAndGraph.AnyChart ChartAndGraph.Legened.CanvasLegend::get_Chart()
extern void CanvasLegend_get_Chart_mFDF2B73F82DD7EFC3719D707CB43E82DD1697E27 (void);
// 0x00000C28 System.Void ChartAndGraph.Legened.CanvasLegend::set_Chart(ChartAndGraph.AnyChart)
extern void CanvasLegend_set_Chart_m2F990939D41CE727B4B0806CFC49E6CCC3FCFE07 (void);
// 0x00000C29 System.Void ChartAndGraph.Legened.CanvasLegend::Start()
extern void CanvasLegend_Start_mAE84920B6784218D10B29F572B484AE7A890255A (void);
// 0x00000C2A System.Void ChartAndGraph.Legened.CanvasLegend::OnEnable()
extern void CanvasLegend_OnEnable_m87D5E1E5D20CD36798C6DC797F0802B3AE173350 (void);
// 0x00000C2B System.Void ChartAndGraph.Legened.CanvasLegend::OnDisable()
extern void CanvasLegend_OnDisable_mAC95861AAD6C5D4F124773BEFF074C214D4B7C87 (void);
// 0x00000C2C System.Void ChartAndGraph.Legened.CanvasLegend::OnDestory()
extern void CanvasLegend_OnDestory_m8669EF0C7388E33C5A09221FEDF3F30605E78AF8 (void);
// 0x00000C2D System.Void ChartAndGraph.Legened.CanvasLegend::CanvasLegend_Generated()
extern void CanvasLegend_CanvasLegend_Generated_mD793D8281E062F41E21E007E112C59CDFAD61958 (void);
// 0x00000C2E System.Void ChartAndGraph.Legened.CanvasLegend::OnValidate()
extern void CanvasLegend_OnValidate_m9AAFDDB05D385011F0288325CE2F498112B3A01F (void);
// 0x00000C2F System.Void ChartAndGraph.Legened.CanvasLegend::PropertyChanged()
extern void CanvasLegend_PropertyChanged_mA71794893BF1F896DA64A4071E90068C37E702B4 (void);
// 0x00000C30 System.Void ChartAndGraph.Legened.CanvasLegend::Clear()
extern void CanvasLegend_Clear_m607799FC14B73F07176AF7DE2323B9273341E734 (void);
// 0x00000C31 System.Boolean ChartAndGraph.Legened.CanvasLegend::isGradientShader(UnityEngine.Material)
extern void CanvasLegend_isGradientShader_m84DDD48676C5B89E2FE672F6EF04AF6D3F78A431 (void);
// 0x00000C32 UnityEngine.Sprite ChartAndGraph.Legened.CanvasLegend::CreateSpriteFromTexture(UnityEngine.Texture2D)
extern void CanvasLegend_CreateSpriteFromTexture_mE32C880FB78FC7DA4A39F3BBE0E7DEE46D086A9B (void);
// 0x00000C33 UnityEngine.Material ChartAndGraph.Legened.CanvasLegend::CreateCanvasGradient(UnityEngine.Material)
extern void CanvasLegend_CreateCanvasGradient_m4DDBBF0B228DD6CBBADE960FB9AFF8C50932ABAA (void);
// 0x00000C34 System.Void ChartAndGraph.Legened.CanvasLegend::Generate()
extern void CanvasLegend_Generate_m5F8E99B064158CFF9AF591C65FFB9E6F66D03F64 (void);
// 0x00000C35 System.Void ChartAndGraph.Legened.CanvasLegend::Update()
extern void CanvasLegend_Update_m5411E860A625AE7AEBD2E8AEFF4D7CE62B8455BA (void);
// 0x00000C36 System.Void ChartAndGraph.Legened.CanvasLegend::InnerGenerate()
extern void CanvasLegend_InnerGenerate_mE0FCEB937FDE2871D578EE38AE76CFCF63DA0C0C (void);
// 0x00000C37 System.Void ChartAndGraph.Legened.CanvasLegend::.ctor()
extern void CanvasLegend__ctor_m0A3BD0E8C09083359C9F47CE082649421F24EDEA (void);
// 0x00000C38 System.Void ChartAndGraph.Legened.CanvasLegend/ImageOverride::.ctor()
extern void ImageOverride__ctor_mCAC69304E42166E716B9C50BC7562040B20D1CE8 (void);
// 0x00000C39 System.Void ChartAndGraph.Legened.CanvasLegendItem::.ctor()
extern void CanvasLegendItem__ctor_m58E47F56D9CAB3B1F84094D65CF9F7A66F1CC107 (void);
// 0x00000C3A System.Void ChartAndGraph.Exceptions.ChartDuplicateItemException::.ctor(System.String)
extern void ChartDuplicateItemException__ctor_m472095D9E1987B99C83EC00395BD02E289A63985 (void);
// 0x00000C3B System.Void ChartAndGraph.Exceptions.ChartException::.ctor(System.String)
extern void ChartException__ctor_m10F2DA5E37A9D03341BD0FF2DA2F8439C1FBD429 (void);
// 0x00000C3C System.Void ChartAndGraph.Exceptions.ChartItemNotExistException::.ctor(System.String)
extern void ChartItemNotExistException__ctor_m0CD84B036E6213139DFEEF95DF4E859BDF8C97ED (void);
// 0x00000C3D System.String ChartAndGraph.DataSource.ChartColumnCollection::get_ItemTypeName()
extern void ChartColumnCollection_get_ItemTypeName_mCEACE72AD02A54F3E2EA6C7DDB6EE50E70B4D165 (void);
// 0x00000C3E System.Void ChartAndGraph.DataSource.ChartColumnCollection::.ctor()
extern void ChartColumnCollection__ctor_m10B7EB73DC78BDF6223E8DDBCEA4BFAE73C1EB18 (void);
// 0x00000C3F System.Void ChartAndGraph.DataSource.ChartDataColumn::.ctor(System.String)
extern void ChartDataColumn__ctor_m9CAE894BFD62A6C4BF4BEF87E6A2DD084EAAF935 (void);
// 0x00000C40 System.String ChartAndGraph.DataSource.ChartDataItemBase::get_Name()
extern void ChartDataItemBase_get_Name_m1A4BDABA4661B4EDF30DB1E4FCC970E161467C1D (void);
// 0x00000C41 System.Void ChartAndGraph.DataSource.ChartDataItemBase::set_Name(System.String)
extern void ChartDataItemBase_set_Name_mAD91AA6A39FFC10A2A708166E3EF6C70E6203926 (void);
// 0x00000C42 System.Object ChartAndGraph.DataSource.ChartDataItemBase::get_UserData()
extern void ChartDataItemBase_get_UserData_m7366DD757A7150DDD8A9BED5BD84833CF439A79E (void);
// 0x00000C43 System.Void ChartAndGraph.DataSource.ChartDataItemBase::set_UserData(System.Object)
extern void ChartDataItemBase_set_UserData_m68BB91284256A958A1965AD38A7F65970958CFCA (void);
// 0x00000C44 ChartAndGraph.ChartDynamicMaterial ChartAndGraph.DataSource.ChartDataItemBase::get_Material()
extern void ChartDataItemBase_get_Material_m884D84994D5DFB145F7F80D646ED047675946B82 (void);
// 0x00000C45 System.Void ChartAndGraph.DataSource.ChartDataItemBase::set_Material(ChartAndGraph.ChartDynamicMaterial)
extern void ChartDataItemBase_set_Material_m556B6A8463CE116F5C41CB9A7C82CD866F333A43 (void);
// 0x00000C46 System.Void ChartAndGraph.DataSource.ChartDataItemBase::.ctor(System.String)
extern void ChartDataItemBase__ctor_mC1F6B7DCAEB6418FF77AAA8B260FD35C6B583979 (void);
// 0x00000C47 System.Void ChartAndGraph.DataSource.ChartDataItemBase::add_NameChanged(System.Action`2<System.String,ChartAndGraph.DataSource.IDataItem>)
extern void ChartDataItemBase_add_NameChanged_mAB59E2935A5636D41ACCABCA450D3AC205B6BCDE (void);
// 0x00000C48 System.Void ChartAndGraph.DataSource.ChartDataItemBase::remove_NameChanged(System.Action`2<System.String,ChartAndGraph.DataSource.IDataItem>)
extern void ChartDataItemBase_remove_NameChanged_m16AFE4B4FF6A7A5DB09EE7E5FAAF7AE835A3E030 (void);
// 0x00000C49 System.Void ChartAndGraph.DataSource.ChartDataItemBase::CancelNameChange()
extern void ChartDataItemBase_CancelNameChange_mDC7C7FA5119AA0B4B50D01E084A8948AD1645F6C (void);
// 0x00000C4A System.Void ChartAndGraph.DataSource.ChartDataRow::.ctor(System.String)
extern void ChartDataRow__ctor_m170E0D453D698E5B865DB80B05E95815509719A6 (void);
// 0x00000C4B System.Void ChartAndGraph.DataSource.ChartDataSourceBase::add_DataStructureChanged(System.EventHandler)
extern void ChartDataSourceBase_add_DataStructureChanged_m3DFE5025CFA79F2777ACF301467AEE3227CFCD43 (void);
// 0x00000C4C System.Void ChartAndGraph.DataSource.ChartDataSourceBase::remove_DataStructureChanged(System.EventHandler)
extern void ChartDataSourceBase_remove_DataStructureChanged_m8AAA481D8DF9741946B75D75912507CD4D14179D (void);
// 0x00000C4D System.Void ChartAndGraph.DataSource.ChartDataSourceBase::add_ItemsReplaced(System.Action`4<System.String,System.Int32,System.String,System.Int32>)
extern void ChartDataSourceBase_add_ItemsReplaced_mFA7A223CBFD7512E5E929AE0E0A717DE475AFFBA (void);
// 0x00000C4E System.Void ChartAndGraph.DataSource.ChartDataSourceBase::remove_ItemsReplaced(System.Action`4<System.String,System.Int32,System.String,System.Int32>)
extern void ChartDataSourceBase_remove_ItemsReplaced_m8102FBC9B601DCBA582A2DD9B1B9F41D9F21B9FD (void);
// 0x00000C4F System.Void ChartAndGraph.DataSource.ChartDataSourceBase::add_DataValueChanged(System.EventHandler`1<ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs>)
extern void ChartDataSourceBase_add_DataValueChanged_m46AFF31FDDA9040F4A0E8FEDBB8AA5F618F72F6B (void);
// 0x00000C50 System.Void ChartAndGraph.DataSource.ChartDataSourceBase::remove_DataValueChanged(System.EventHandler`1<ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs>)
extern void ChartDataSourceBase_remove_DataValueChanged_m2B90A93D01B42AB10A91FA075051E2B995B982DC (void);
// 0x00000C51 System.Void ChartAndGraph.DataSource.ChartDataSourceBase::OnDataStructureChanged()
extern void ChartDataSourceBase_OnDataStructureChanged_m0860BC4844E190490E025D7C856611D81C0DD1F3 (void);
// 0x00000C52 System.Void ChartAndGraph.DataSource.ChartDataSourceBase::OnItemsReplaced(System.String,System.Int32,System.String,System.Int32)
extern void ChartDataSourceBase_OnItemsReplaced_mDDA31C181C7CE7CBE46DAEC17B32869CBBA27AD7 (void);
// 0x00000C53 System.Void ChartAndGraph.DataSource.ChartDataSourceBase::OnDataValueChanged(ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs)
extern void ChartDataSourceBase_OnDataValueChanged_mAFC19B6C9432BB5F12BFE77BBA965AFA597DB4D2 (void);
// 0x00000C54 System.Double[0...,0...] ChartAndGraph.DataSource.ChartDataSourceBase::getRawData()
// 0x00000C55 ChartAndGraph.DataSource.ChartColumnCollection ChartAndGraph.DataSource.ChartDataSourceBase::get_Columns()
// 0x00000C56 ChartAndGraph.DataSource.ChartRowCollection ChartAndGraph.DataSource.ChartDataSourceBase::get_Rows()
// 0x00000C57 System.Void ChartAndGraph.DataSource.ChartDataSourceBase::.ctor()
extern void ChartDataSourceBase__ctor_mFE3E99C34B9C46A1ECEE154058F410D176CA3386 (void);
// 0x00000C58 ChartAndGraph.Common.ChartItemIndex ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::get_ItemIndex()
extern void DataValueChangedEventArgs_get_ItemIndex_m567F892ACB8F7484334FE772FAD6772D47BA3168 (void);
// 0x00000C59 System.Void ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::set_ItemIndex(ChartAndGraph.Common.ChartItemIndex)
extern void DataValueChangedEventArgs_set_ItemIndex_m3F4E84261B9A7E33423D30D574AE5CA803D1C070 (void);
// 0x00000C5A System.Double ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::get_OldValue()
extern void DataValueChangedEventArgs_get_OldValue_m0982C5004334F1A50D175D9FE9164DA2A80F9C45 (void);
// 0x00000C5B System.Void ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::set_OldValue(System.Double)
extern void DataValueChangedEventArgs_set_OldValue_m3017E9DAA2BC8F3144A12C5623B7EEABC4F0C32B (void);
// 0x00000C5C System.Double ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::get_NewValue()
extern void DataValueChangedEventArgs_get_NewValue_m58B94C77C099C4EE56C72968A6FAD8A45F9A9F5A (void);
// 0x00000C5D System.Void ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::set_NewValue(System.Double)
extern void DataValueChangedEventArgs_set_NewValue_m9560AAB634ECBA050B2023D8CF4F96D05E99ED1B (void);
// 0x00000C5E System.Boolean ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::get_MinMaxChanged()
extern void DataValueChangedEventArgs_get_MinMaxChanged_m52927FBE8B2DCCE7C6C5F22AA9559C6EDFE56FDC (void);
// 0x00000C5F System.Void ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::set_MinMaxChanged(System.Boolean)
extern void DataValueChangedEventArgs_set_MinMaxChanged_mE75097F31ECA36DF1DED4BD67CF3BEF9CA8E1541 (void);
// 0x00000C60 System.Void ChartAndGraph.DataSource.ChartDataSourceBase/DataValueChangedEventArgs::.ctor(System.Int32,System.Int32,System.Double,System.Double,System.Boolean)
extern void DataValueChangedEventArgs__ctor_mE9A533BF6AFACA01151D246BDB5D9B1CACDA5D64 (void);
// 0x00000C61 System.String ChartAndGraph.DataSource.ChartRowCollection::get_ItemTypeName()
extern void ChartRowCollection_get_ItemTypeName_m9AAD9DC6F3644EC040171ADBCD178401CEA766CB (void);
// 0x00000C62 System.Void ChartAndGraph.DataSource.ChartRowCollection::.ctor()
extern void ChartRowCollection__ctor_m5B54347CF12CD85575586913DB873AA56006ABBB (void);
// 0x00000C63 System.String ChartAndGraph.DataSource.IDataItem::get_Name()
// 0x00000C64 System.Void ChartAndGraph.DataSource.IDataItem::set_Name(System.String)
// 0x00000C65 System.Void ChartAndGraph.DataSource.IDataItem::CancelNameChange()
// 0x00000C66 System.Void ChartAndGraph.DataSource.IDataItem::add_NameChanged(System.Action`2<System.String,ChartAndGraph.DataSource.IDataItem>)
// 0x00000C67 System.Void ChartAndGraph.DataSource.IDataItem::remove_NameChanged(System.Action`2<System.String,ChartAndGraph.DataSource.IDataItem>)
// 0x00000C68 System.Void ChartAndGraph.Common.ChartItemIndex::.ctor(System.Int32,System.Int32)
extern void ChartItemIndex__ctor_m4941151A043FAF2B807F6DA558F40770D70AA68C (void);
// 0x00000C69 System.Boolean ChartAndGraph.Common.ChartItemIndex::Equals(System.Object)
extern void ChartItemIndex_Equals_m3ED9D2A4A198B737C7A5D0AF25A359CF6582C6A9 (void);
// 0x00000C6A System.Int32 ChartAndGraph.Common.ChartItemIndex::GetHashCode()
extern void ChartItemIndex_GetHashCode_m34B5BD4640C9BFF87E9B100043FA0936960CE5F4 (void);
// 0x00000C6B System.Int32 ChartAndGraph.Common.ChartItemIndex::get_Group()
extern void ChartItemIndex_get_Group_mBBD268E7B2942B2B9F44E76CF11463EB2247B92B (void);
// 0x00000C6C System.Void ChartAndGraph.Common.ChartItemIndex::set_Group(System.Int32)
extern void ChartItemIndex_set_Group_m1FD2ECC0C5E3DE8D62E22CF59AB13BB2C63E940B (void);
// 0x00000C6D System.Int32 ChartAndGraph.Common.ChartItemIndex::get_Category()
extern void ChartItemIndex_get_Category_m65B6C653CB412F5D8B8A8E8E34759FE50B81C3A5 (void);
// 0x00000C6E System.Void ChartAndGraph.Common.ChartItemIndex::set_Category(System.Int32)
extern void ChartItemIndex_set_Category_mF159D3DEB39D5C73D7ECE7448170A56B9488F5E4 (void);
// 0x00000C6F System.Void ChartAndGraph.Axis.AxisGenerator::InnerFixLabels(ChartAndGraph.AnyChart)
extern void AxisGenerator_InnerFixLabels_m04B10787F75A30F78CDAFD2BB2D484C646E70CE8 (void);
// 0x00000C70 System.Void ChartAndGraph.Axis.AxisGenerator::InnerSetAxis(System.Double,ChartAndGraph.AnyChart,ChartAndGraph.AxisBase,ChartAndGraph.ChartOrientation,System.Boolean)
extern void AxisGenerator_InnerSetAxis_m965951C8051950098B7B51A49ED807D62CC2E236 (void);
// 0x00000C71 System.Void ChartAndGraph.Axis.AxisGenerator::FixLabels(ChartAndGraph.AnyChart)
extern void AxisGenerator_FixLabels_m6F2D034508501859E8B5CD23D92AB20EB62F7EA3 (void);
// 0x00000C72 System.Void ChartAndGraph.Axis.AxisGenerator::SetAxis(System.Double,ChartAndGraph.AnyChart,ChartAndGraph.AxisBase,ChartAndGraph.ChartOrientation,System.Boolean)
extern void AxisGenerator_SetAxis_mF18009916DBA9522B0AF968E05EF11FBA6EB4F51 (void);
// 0x00000C73 UnityEngine.Object ChartAndGraph.Axis.AxisGenerator::This()
extern void AxisGenerator_This_m542C8ED27846766C425D159D20BD38E739CBD239 (void);
// 0x00000C74 UnityEngine.GameObject ChartAndGraph.Axis.AxisGenerator::GetGameObject()
extern void AxisGenerator_GetGameObject_m36B5AC2E1F5EE76A4AC78D731E0363D3E1FF48F0 (void);
// 0x00000C75 System.Void ChartAndGraph.Axis.AxisGenerator::Start()
extern void AxisGenerator_Start_m41BC4CEEF0F59B141AC681D2A997A8194B8C8EC2 (void);
// 0x00000C76 System.Void ChartAndGraph.Axis.AxisGenerator::OnDestroy()
extern void AxisGenerator_OnDestroy_mA052767C7A8ADDA4310774E19CA9BFD3962A8E03 (void);
// 0x00000C77 System.Single ChartAndGraph.Axis.AxisGenerator::GetTiling(ChartAndGraph.AnyChart,ChartAndGraph.ChartOrientation,ChartAndGraph.ChartDivisionInfo)
extern void AxisGenerator_GetTiling_m5FB69574A741BB7DBBF1A4CA08FDE78A4176F772 (void);
// 0x00000C78 System.Void ChartAndGraph.Axis.AxisGenerator::Update()
extern void AxisGenerator_Update_m087962844BAA3E28F061DD5375C9FF0EE21C1CC8 (void);
// 0x00000C79 System.Void ChartAndGraph.Axis.AxisGenerator::.ctor()
extern void AxisGenerator__ctor_mA7EA9076FE53A1D5BC76AC31183173988AE25065 (void);
// 0x00000C7A System.Void ChartAndGraph.Axis.CanvasAxisGenerator::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void CanvasAxisGenerator_OnPopulateMesh_m492525484E4BE7E2D2D2F7D386E0E81B879FEDAA (void);
// 0x00000C7B System.Void ChartAndGraph.Axis.CanvasAxisGenerator::AddToCanvasChartMesh(ChartAndGraph.CanvasChartMesh)
extern void CanvasAxisGenerator_AddToCanvasChartMesh_m42E2453727FAA2EAEFE79DD2C0D38E4BEA96936C (void);
// 0x00000C7C System.Void ChartAndGraph.Axis.CanvasAxisGenerator::OnPopulateMesh(UnityEngine.Mesh)
extern void CanvasAxisGenerator_OnPopulateMesh_mF3EC87C795A2C4BED0312F0647BBE17A058EDA1C (void);
// 0x00000C7D System.Void ChartAndGraph.Axis.CanvasAxisGenerator::FixLabels(ChartAndGraph.AnyChart)
extern void CanvasAxisGenerator_FixLabels_mECCA8953C761E8844939FF15A74924FE672CF872 (void);
// 0x00000C7E System.Void ChartAndGraph.Axis.CanvasAxisGenerator::UpdateMaterial()
extern void CanvasAxisGenerator_UpdateMaterial_m11D57D722B4534D79425E68062D346B37D95C789 (void);
// 0x00000C7F System.Void ChartAndGraph.Axis.CanvasAxisGenerator::OnDestroy()
extern void CanvasAxisGenerator_OnDestroy_m20642C4884C7DC636B0B84C82C174D6E37D54A2D (void);
// 0x00000C80 System.Single ChartAndGraph.Axis.CanvasAxisGenerator::GetTiling(ChartAndGraph.MaterialTiling)
extern void CanvasAxisGenerator_GetTiling_m74F9BA48027EEA8A3C947D901B6202255445384D (void);
// 0x00000C81 System.Void ChartAndGraph.Axis.CanvasAxisGenerator::SetAxis(System.Double,ChartAndGraph.AnyChart,ChartAndGraph.AxisBase,ChartAndGraph.ChartOrientation,System.Boolean)
extern void CanvasAxisGenerator_SetAxis_m2782DC9E983468DD535AF3F39A640D90E33854B6 (void);
// 0x00000C82 System.Void ChartAndGraph.Axis.CanvasAxisGenerator::Update()
extern void CanvasAxisGenerator_Update_mD8A6742AB2128ED6469AE00A2FE99D607ACFE785 (void);
// 0x00000C83 UnityEngine.GameObject ChartAndGraph.Axis.CanvasAxisGenerator::GetGameObject()
extern void CanvasAxisGenerator_GetGameObject_mD0DF412ED4B864F1102293CFFC94EEB6D739C5AA (void);
// 0x00000C84 UnityEngine.Object ChartAndGraph.Axis.CanvasAxisGenerator::This()
extern void CanvasAxisGenerator_This_mF13963B6497D6E628B9BB7CA2B5423324096DBEA (void);
// 0x00000C85 System.Void ChartAndGraph.Axis.CanvasAxisGenerator::.ctor()
extern void CanvasAxisGenerator__ctor_mE31297F392611291BCC38579703DF7FAE7813372 (void);
// 0x00000C86 UnityEngine.Object ChartAndGraph.Axis.IAxisGenerator::This()
// 0x00000C87 UnityEngine.GameObject ChartAndGraph.Axis.IAxisGenerator::GetGameObject()
// 0x00000C88 System.Void ChartAndGraph.Axis.IAxisGenerator::FixLabels(ChartAndGraph.AnyChart)
// 0x00000C89 System.Void ChartAndGraph.Axis.IAxisGenerator::SetAxis(System.Double,ChartAndGraph.AnyChart,ChartAndGraph.AxisBase,ChartAndGraph.ChartOrientation,System.Boolean)
static Il2CppMethodPointer s_methodPointers[3209] = 
{
	PyramidAnimation_Start_m9E3A8F7C90DC60781E9D3D7E0A8CB1E8F6B9857A,
	PyramidAnimation_Animate_m55DA2060EADF72524E5F83E74B330BF35A663268,
	PyramidAnimation_Update_mA210DB88B125D6E28BA008DC92B0C6270D8B375D,
	PyramidAnimation__ctor_m32B0D3D7001776558E93D1B9E1690B78A47F80EE,
	CandleChartFeed_Start_mFFCD83CFE3FFEA698E1ABA137E0C4C748121FC34,
	CandleChartFeed_Update_m99C28E006E14BEA77F1BAA3BFFFC557C7DB0CE07,
	CandleChartFeed__ctor_mBF450EB33D8F1D87AA953635ACFAEA361BB9A15E,
	PyramidFeed_Start_mF0C8196AC2A1B66421F0035FFF28B459FE650940,
	PyramidFeed_Update_m94A84AD8034B3A8AA3D4D094A4154F9196764A41,
	PyramidFeed__ctor_m9EA946F11C5ED29A30026DC6ECD52ECA5E26A81A,
	BarContentFitter_Start_m56A709082F6099D3166144ECF83EF75A4CB767B3,
	BarContentFitter_OnValidate_m062D09E63ED68147C8C8007C2607468993D3ABDC,
	BarContentFitter_Match_m6CD3744A2CA771813EB74B48D526BE5F422FA05F,
	BarContentFitter_Update_m58BB387F69F62C6814471E84BD5505A5938DE871,
	BarContentFitter__ctor_m039CCCAF31FC3DC03BC020E1048684259D7962A1,
	customBarSelection_Start_m73E1DAD7F0C38DA1043ED1F44B92CB18736A8F97,
	customBarSelection_OnDestroy_m4A497F9566D7E24619C3810A1BA3BB7795A3AF25,
	customBarSelection_ToogleSelection_m2C4F60C534ABB4A3DE7357F5F6882525D541BCF3,
	customBarSelection_SetSelection_mE205FFCE93B235BAD4E92553BB5BA3044F258E73,
	customBarSelection__ctor_m5A7189B8CB40E826024F10E3945F997E8AC58C09,
	masterBarSelection_Register_mF6B0C5A7E8DD251199075EEBD1531CD201E729F1,
	masterBarSelection_Unregister_m6757055C6E80348EA312AC86AB55457E89F9995B,
	masterBarSelection_ToogleBar_m4611FE7997C7FFAD306B4041017B180CB9022B42,
	masterBarSelection_SelectBar_mB698887406CAB68EF64E13998A70B0C3757A7C6D,
	masterBarSelection_DeselectBar_m25371DB70D930477CE94A54EEF9140013B6CB954,
	masterBarSelection__ctor_m0BE374261F8252530FB1CCDF00A6223D39880E9D,
	BarDataFiller__cctor_mD3599F13797FC435E77C4C284372695B30669A64,
	BarDataFiller_EnsureCreateDataTypes_m07D7B976F18BF4F391A29DE9EAC154D9DF59AE5D,
	BarDataFiller_ParseItem_mA35CD85D0DA16A8009C629A98CAE490308BCDA18,
	BarDataFiller_LoadValueArray_mF343C7A768D3D6DEDC0C5B263E7E2022C59E3271,
	BarDataFiller_LoadObjectForEachElement_mC091368335B5031C3300B7062821180F6258A3B2,
	BarDataFiller_Start_mD6E970115E46C74E8A2B3D73DE303529D8330E28,
	BarDataFiller_Fill_m633F58F9022016EFE1FB87F25A06FAFC34E59871,
	BarDataFiller_Fill_mC11F6B735190D745DACFFFA38838E0369C0CB293,
	BarDataFiller_LoadCategoryVisualStyle_mBD1E913B85036E45B38644876D9E01310E617403,
	BarDataFiller_ApplyData_m6483A9ED1FDED1D1F829258DFE4461B5A95D2CF3,
	BarDataFiller_CreateRequest_m008A16DECFA515370F6366DBBE00B3D41E7FB02D,
	BarDataFiller_GetData_mD41A3305539B936D9A51B5EF5F6EFF55107730C2,
	BarDataFiller_Update_m2C5956D636139184C8D5A2392A39253CDB44D007,
	BarDataFiller__ctor_mB0054DBC5AA02B744DDF14CF57B38069D381D87B,
	CategoryData__ctor_m0FFAAB2771823D79C287D84BB72BD2CA28D375B0,
	CategoryLoader__ctor_m69E2D50B997EDCD0E39E557496357FB1B90EC3A3,
	CategoryLoader_Invoke_mE86E501C167E32E6E31C87DCC9094C6DB6586C06,
	CategoryLoader_BeginInvoke_m9FB207714A5C22C03E5B90E26E791F5DC12BB409,
	CategoryLoader_EndInvoke_mCB375F7E76C9C279EC6644753F3C6733EE12B27E,
	U3CGetDataU3Ed__24__ctor_m23346B90BF5520515FB01571AAD820E9056BC2AF,
	U3CGetDataU3Ed__24_System_IDisposable_Dispose_mEC6A57765D641AA00A34DCFA6092965012FE2958,
	U3CGetDataU3Ed__24_MoveNext_m4D7A405FCDFF6123206D8B1366C5D0675E5F6E3D,
	U3CGetDataU3Ed__24_U3CU3Em__Finally1_m0B46925728F135D105CDDF531CA3DF43DCB02539,
	U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D8AD14E0C19EE312760EFFF80A2F14CBE757EFB,
	U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_mB0F4ABD0744B078491FA16002ED4D4749F55AD9E,
	U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_mCD2136265947A93966E384448D5C93D5B0D037D5,
	GraphDataFiller__cctor_m1034A158AE4CC9039019B3696786E1FE5E9C9CFE,
	GraphDataFiller_EnsureCreateDataTypes_mA8AEA4D813A2EBB4E053F041A955AD82AE1C325D,
	GraphDataFiller_CreateVectorFormats_m53C262FFA98A221282D56786121FAC1EC373B508,
	GraphDataFiller_ParseItem_m6888B64F7483853EBD6D0177F6D02A216AC11102,
	GraphDataFiller_LoadArrayForEachElement_mC51797EFA85A670E51059FE46DD951768C737B19,
	GraphDataFiller_LoadObjectArray_mA1C6F1FD572C8F68224C3ED84370566B4C0D2A57,
	GraphDataFiller_LoadVectorArray_m0030532864279D533B8E0558A856A03196DDCD58,
	GraphDataFiller_Start_m6684A6F7980FFCDBF0DA6D58622902EE19F4B1F8,
	GraphDataFiller_Fill_m4CCB1B3968316276FBD8A542F5285669DA7C962E,
	GraphDataFiller_Fill_m7677B6A5D9A669524DFB588810C9871858A41E42,
	GraphDataFiller_LoadCategoryVisualStyle_m35986A756D3D58885746CBB473334C2A2AB200EC,
	GraphDataFiller_ApplyData_m21DE343237A850448607067BB5614A9CA4285B1C,
	GraphDataFiller_CreateRequest_m23A7BB28725B793E524C1D5C2F7F0E0635971468,
	GraphDataFiller_GetData_mDB9DBCC704135811FA58F24E972BEAF1918525C8,
	GraphDataFiller_Update_mE913AEE8A2B013EB0E9B6B345FCB005C384150A2,
	GraphDataFiller__ctor_m5AAE2D5F5A3B78649DD66F46AFC215D825FE6833,
	VectorFormatData__ctor_m8A07BEE0BD766F79EDF5A5B44B87A75BB85F5286,
	CategoryData__ctor_m59954DD6BC2C0405144B641FDB5A3696FA981F33,
	CategoryLoader__ctor_m876ED039772EF03576826B125261A73D80C4D2DE,
	CategoryLoader_Invoke_m146DDAD28CCEF3F7A60D38A72648878B2233C0E7,
	CategoryLoader_BeginInvoke_m91E27CDAA4446FE6A1BC4C9C4C85AA8D90CBB5EA,
	CategoryLoader_EndInvoke_mA19234C90E5A01148F3EE210E284C1CF56411390,
	U3CGetDataU3Ed__29__ctor_m3C343F3EC10F96FC8639D32AA99C25FA12075BD5,
	U3CGetDataU3Ed__29_System_IDisposable_Dispose_mE4AA5BFA3FC25D675CA57B4C8075D20B84A1830B,
	U3CGetDataU3Ed__29_MoveNext_mCAC6F2B25682F73F1FD5EBFD8169E1A6A36BFA4D,
	U3CGetDataU3Ed__29_U3CU3Em__Finally1_m6CF9CEDD151D26CDA46D4134A1F916C3246A2BFF,
	U3CGetDataU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F32C1C524518E6F04CCCCAD3771DAAE1665EC45,
	U3CGetDataU3Ed__29_System_Collections_IEnumerator_Reset_m7DCBB1D02F477820DCE81C95EB63B1D2747AC323,
	U3CGetDataU3Ed__29_System_Collections_IEnumerator_get_Current_m628029FF6FC5324234A6D305A09351A288BF00D2,
	PieDataFiller__cctor_m251B99C364DFE798BCC59B34C7E052A6285C9A7C,
	PieDataFiller_ParseItem_m65D5329FBBEFF7169A16796DC8CB82C46A73884B,
	PieDataFiller_LoadObjectforEachElement_m7EE0B4F1B06CFE167B0B58B170E41DD01E90012D,
	PieDataFiller_LoadValueArray_m7B35C6F8D602022D256D7B9829BCE1E7D9F341F6,
	PieDataFiller_Start_mC19BFD289B702BDAD3269065A857080F8317766C,
	PieDataFiller_Fill_m3B2906674B8627EBDDC398723A35E16549C82F4C,
	PieDataFiller_Fill_mA572370E4C699805D5497EE8CC9013F1D7C3FF1A,
	PieDataFiller_LoadCategoryVisualStyle_m0BC4B4DABB4987D433A471F685C73EE7853B173F,
	PieDataFiller_ApplyData_m1154210329B43E30CB55F85C3C4B6126F960083F,
	PieDataFiller_CreateRequest_m293101FD2E3EFC977FCA4B8202D8E3FBE6491926,
	PieDataFiller_GetData_m7AD75ED5136A7540B377D95CDC73B5156AE07BE0,
	PieDataFiller_Update_mDB3426EE842E78B369FBCAF9F9B104BF0FB42DA3,
	PieDataFiller__ctor_mCAAAB908614BD4C01C5A021B08C28995A657C7E8,
	U3CGetDataU3Ed__21__ctor_m5FDFF7FBE070B5D39B9735F93E54F91C4650592E,
	U3CGetDataU3Ed__21_System_IDisposable_Dispose_m3690B5FEA5C363A68E5CF5491D67E4DAF05E0C08,
	U3CGetDataU3Ed__21_MoveNext_m935AB22B609D60E1ABAAC0983DBDBA8C1F9CF42F,
	U3CGetDataU3Ed__21_U3CU3Em__Finally1_m6A8B9DC5964ABD9A35F9F30D17C5B517601B74EA,
	U3CGetDataU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m162CF48EF0D446615C4EF6D006C692CDD0154710,
	U3CGetDataU3Ed__21_System_Collections_IEnumerator_Reset_m37DE13669B84A238C4A939A80966461EADE38B04,
	U3CGetDataU3Ed__21_System_Collections_IEnumerator_get_Current_m36AEC0B435EC47BE9E67D3BF1F81D07AE22C4C1E,
	RadarDataFiller__cctor_m66EFBA774345BAF1422C23991AF9659EB79D3AE9,
	RadarDataFiller_EnsureCreateDataTypes_m45D9EDC47078D80CEB08110FCD24E7182F401F7D,
	RadarDataFiller_ParseItem_m7C8CE9B3929B8531623D1F4DE72C928049374124,
	RadarDataFiller_LoadValueArray_mCCB7D7C680BC47B7F16DC8AC900DB12E1782E0D5,
	RadarDataFiller_LoadObjectForEachElement_mEC3CD641B0FAF86E0C353342ABC01DF30AF30AFA,
	RadarDataFiller_Start_mF8A2DCA86AD2EC1AAB1EA44C6CC75344B0F24923,
	RadarDataFiller_Fill_mF5F43891081B0057F3B03CFEED56AB626C8C7E10,
	RadarDataFiller_Fill_mB4150671143F5AAC511109937C4EE9EDDA05F99D,
	RadarDataFiller_LoadCategoryVisualStyle_m0F683906F52B72204D68BED4532E952AB5832CA2,
	RadarDataFiller_ApplyData_m24ED2D99CD90B30E341B15E640AF01AD9C9F6125,
	RadarDataFiller_CreateRequest_m8485E82C1F007A0ABE3DB864912801DA181030D9,
	RadarDataFiller_GetData_mB7FDF2D44705CFB8C333109D80F1D6581A254D91,
	RadarDataFiller_Update_m67E9404482F3B5638C48CC604A5319DED389A073,
	RadarDataFiller__ctor_m0D038E37A46D07FC6FD8E492F2D55904FF007411,
	CategoryData__ctor_m60FF0D85483BC9E4A7FD5F7EDC74D1B331BF3A97,
	CategoryLoader__ctor_mD695FAFDADFCBFD8AEEC66382670D7914E5C1604,
	CategoryLoader_Invoke_m32736A9B36B7D980A120E13094E0EBAE07881209,
	CategoryLoader_BeginInvoke_m360D17E1441FC0D7D91B93EDE19D460C251E0B13,
	CategoryLoader_EndInvoke_m50D60D107EC4BC1004AE29BFBBFE74AE97A9A73D,
	U3CGetDataU3Ed__24__ctor_mE5AC81C7F88B9C90F79E72BE064CC18DD6783980,
	U3CGetDataU3Ed__24_System_IDisposable_Dispose_m1682B988B3E5EC516BD0A4DFE5AA57A883EE6F51,
	U3CGetDataU3Ed__24_MoveNext_mFF4CF340570149CACE53CEF0CA4CC4A82572DC8B,
	U3CGetDataU3Ed__24_U3CU3Em__Finally1_m948049B8B8D63FEA5422B0C41C4F13E28ED87C28,
	U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70B61A70670719AED94EC9A2B8C80E97A217024D,
	U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_m5F195A69B5832A7F5B608683EC834806914785E3,
	U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_m2101121EA708E368E26EAF98BE1B497878498183,
	BillboardText_get_UIText_mAB7F8962EC45CF04171D3F0D4C2CF3BBE84DB0B0,
	BillboardText_set_UIText_m06B97F41D530290E48CC6D721F830DAA5C0380B7,
	BillboardText_get_UserData_mE921AB0E2FE52066A0EA5DA30DFA7D3F80157BE8,
	BillboardText_set_UserData_m0E39DB9C7C06ADB39CF5E98F7B89C46D9B6D4571,
	BillboardText_SetVisible_m651569C349FA5B2D76CF99D8D24A0D8BBDC5237A,
	BillboardText_get_Rect_m4558C9D124D390BB4D033B8E4B71E25277D423D1,
	BillboardText__ctor_m55630E20EA745D30AED5B838B79FF99C94FFD717,
	CategoryLabels_get_VisibleLabels_mC722C3B72C0D82EE4645F92F9D9F646AA59D9F3D,
	CategoryLabels_set_VisibleLabels_m04FCCFDF85C9D7EA790EF265F5F497FDB0F899DA,
	CategoryLabels_get_Assign_mCF26C8B4B118F45DA6A41336AB8B43ADF2C6C108,
	CategoryLabels__ctor_mF01D411028AF434818D5D1C215152F2B26FD9348,
	CategoryLabels_U3Cget_AssignU3Eb__6_0_m1E37F7A84B474AC9DE3E6366332A2BF2A757026C,
	ChartText_Start_m801F662B72FA58F9BD1A30E6CFB0247C01CFD4EC,
	ChartText_Update_m8830768A03FDB905C7FAA71AE99991AC1614DDB8,
	ChartText__ctor_m03E73200903719948D14794377743F8113EA123F,
	GroupLabels_get_Alignment_m87FE68F73B57BAD8176A583828313C559A1FBA6B,
	GroupLabels_set_Alignment_m0570AB928A68CD1E3DF1A0B33BBFD6E943A7E7ED,
	GroupLabels_get_Assign_m20F4A1682DE6388C35E07002D15BCEABD40545F5,
	GroupLabels__ctor_mF0EBA35211AA4FD0A8D0683F01F7605E6BFAD313,
	GroupLabels_U3Cget_AssignU3Eb__5_0_m8BB30C6A5D704B6E4463E5D6C87799990E5BAB9E,
	ItemLabelsBase_get_TextPrefab_m6DEF999EFC827B840BC11E04D36F5D7526538197,
	ItemLabelsBase_set_TextPrefab_m09DD0ED9D3CD75DC119BB86779CAC14E63D79EF5,
	ItemLabelsBase__ctor_m142D9906D0A414AA2C92DD1CFEFC8F17D0BF7905,
	ItemLabelsBase_AddChildObjects_m018C2563AF1BD9871F77B00CBF42F59FEE2824E9,
	ItemLabelsBase_get_TextFormat_mA82C77A99B358780D9278B5EFB04A38B2F35054D,
	ItemLabelsBase_set_TextFormat_mE143EC246E2C7E2E75B2280B23420187CFC555AB,
	ItemLabelsBase_get_FontSize_m20C08741113190AE57A9395893B9820E9395BA27,
	ItemLabelsBase_set_FontSize_mDFEEE48D891B1995D2B722954C0308F060454620,
	ItemLabelsBase_get_FontSharpness_mD9C29353DBA75A7926CB6447F4584C8FA0A26D34,
	ItemLabelsBase_set_FontSharpness_m937EB7D8B875CCEF952A1D420237E4BCE75EACDD,
	ItemLabelsBase_get_Seperation_m3FC0BE49F743C9064B7DD004D384FD7C8A11F326,
	ItemLabelsBase_set_Seperation_mD69311D695C43B5C45D026F11BAEC22EC8C61C94,
	ItemLabelsBase_ValidateProperties_m9EE0D8630657B1A74B7FC954CA67042166EE6651,
	ItemLabelsBase_get_Location_m17E0D42D404321BF2D878F21F5902A591B0580B7,
	ItemLabelsBase_set_Location_mDCC9F138E0A1596C8BFE07FA5C4C60E2A9D61939,
	ItemLabelsBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mB5A25B949CA84EB8ECA0946479CBFA4BC8F85CEA,
	ItemLabelsBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m109C00CA4D121C3F734312FDE1AF6855F0B7E51D,
	TextController_get_Text_mFD0238C6C109AD916DE9C8379B15B88C359FA107,
	TextController_SetInnerScale_mFE4889429B9ED6829F076DADA795ADC11FEE3F15,
	TextController_get_mParent_m33F259DA1763414EA5A9A288D8DBF1A4DA63C7C0,
	TextController_set_mParent_mFABEAB7D648EBAB3AB7C38A5F13323D1F7451D56,
	TextController_Start_mC0EEF24BD7CBF7C0E1935B2E8390DCA674A93D04,
	TextController_OnDestroy_m1554DDF8F20062EBDDA51E3551D8016474C5DABF,
	TextController_Canvas_willRenderCanvases_m7625506B363DB0B511ED8E46708B0DC61ABA6774,
	TextController_EnsureCanvas_mEA25AFB8AE72C83C189F82B4163AC2C977B2FAC3,
	TextController_get_SafeCanvas_m154826805B7DBE7E90EB2763F409EDAA13BFF238,
	TextController_OnDestory_m7536891FAB89E961AE4A4DDDB5072AE46430DE76,
	TextController_DestroyAll_m52ABBB33C36136DBDA3C6187BC0FBFFED5652130,
	TextController_AddText_m79C9AF8C77AEFA5ADA3D5BD576753A7D42F29D7A,
	TextController_EnsureCamera_m0B4707DB10A03DEFDC95959A0625817814894A81,
	TextController_AssignCamera_m00BC4372C3CB1FB458D7C4B339B24CFB5BA2342C,
	TextController_Update_m70F7AB07AA36CAA06091C83F38E4D00D3214A3D5,
	TextController_ProjectPointOnPlane_mD09C89EA119BBF1D224D71030164E8355784B2F6,
	TextController_CalculatePlane_m2AD8522C3A81ACD3A057F4DE3B4352BEDDD509EA,
	TextController_ApplyTextPosition_m0E3D96AB532953349DAB746DA0F93F07FA449083,
	TextController_LateUpdate_m17FD2A503AEA145AA3B1A37BDA1758DC91EC5741,
	TextController__ctor_m1C8A04AF790B9F1451CA5885ED58953C2F7989CD,
	U3CU3Ec__DisplayClass32_0__ctor_mD6E461DD4146296306493E8B3FE5657EACC52F3B,
	U3CU3Ec__DisplayClass32_0_U3CApplyTextPositionU3Eb__0_mEAAC0C4EC1E513611879196879EB9DDD691FEEE3,
	BarAnimation_Start_m0888925527A0BF1E8A4AF3B815152718058F3D2E,
	BarAnimation_Animate_m37ED9C50B6BCA23841EBCE310DB3F40B159000DF,
	BarAnimation_Update_m3DCAA30690F49343A27CA459F3EB1609A39CEE0B,
	BarAnimation__ctor_m6365A477F47D1690BA9DAC4CF3C224945835C99B,
	GraphAnimation_Start_mB860DC47A57854C8E9F0B1556B7C552A8046EF26,
	GraphAnimation_IsValidDouble_m85358BC2CB0792B6FBB3ADFEDB39D816EE42BB59,
	GraphAnimation_Animate_m9981ABB5F8490D631F6305FCC4A0353BBE4C2BA3,
	GraphAnimation_FixedUpdate_mD1535CEDAAD4ACC7F31695E51845C6087D7D010A,
	GraphAnimation__ctor_mE6E4894238776443A9C925214DFA950EAF69C506,
	InnerAnimation_Update_m5F13833FB4E240AE93428EC36B1F882759135A46,
	InnerAnimation__ctor_mF041B8218F00ABA0F7277F44C0BB25417D1D25F6,
	AxisCoordinates_Start_m6918F4A65FE29709F11D665A0B53FF848458217D,
	AxisCoordinates_Update_m25BF46638BBF8FAFA21951F1C1BE8543FDF53D76,
	AxisCoordinates__ctor_m20045517A731F8DF305F23B387EEFB78D63DC8CD,
	GraphDataVisualEditor_Start_mB2152E60FA8C6D3CA194C9D6B67B30CD3A72E541,
	GraphDataVisualEditor_Update_mE617AC8A0EE82823D0B3A1BAD2A7507820028921,
	GraphDataVisualEditor__ctor_m29BD009617A67B49AB46539DD79F32677BEACEE6,
	HoverText_Start_m2274191EDF726AE518FAE164A815102ECEF5AAEA,
	HoverText_NonHover_m11D481428A8F3C0869D455C131F59758650680CD,
	HoverText_Update_m024AE9E58BE96987C0277C63CE9F5B1551072FD4,
	HoverText_SelectText_mC859EF99FE3BA0697E61E7784454E77239FECEA9,
	HoverText_RemoveText_mE4B225F5246ED56928E1A68FE1902B237FF71A92,
	HoverText_PopText_m965EE5CDECB1927593035EA98A0B204823983BC3,
	HoverText_BarHover_m0051D6CF0617FEB17C26B4BF40C807A8D3241EBA,
	HoverText_GraphHover_mA609EDDDD81BEC5CD7676267751D23FDFD4A91CA,
	HoverText__ctor_mEAC46D46BDB48597C1673A4E4926B92CB806F883,
	U3CU3Ec__cctor_m3971EBD9B2E9CBDEDD6D4E3A3EE9154196E05AB6,
	U3CU3Ec__ctor_m13C72EBE32C43567CDF1AD532B52FABADFA9775F,
	U3CU3Ec_U3CUpdateU3Eb__10_0_mFFB002A8FDAA17ECB08E696823961DC7F00FF057,
	U3CSelectTextU3Ed__11__ctor_m97FF7574BECDA2356CE91AB79D964D437607610B,
	U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m0137CCE653C298077E28C9B537639ABF411CF179,
	U3CSelectTextU3Ed__11_MoveNext_m55C44DC63F50B33156306DB53FFEBA4DD3A45FE7,
	U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD62DC4EDF7D813AD2A227E8A3997EC293D5F4D42,
	U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m4A0993E9C1AE47CF9B6BCABFB295969705F88EB8,
	U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m1D812B2A83110FF66329D8CF9E21A57F6D078C09,
	MultipleGraphDemo_Start_m755BE9F1BD7A103970BC765B58E16647588EB77F,
	MultipleGraphDemo__ctor_mB180691B507CFECD43D6A681371B80E07D721DC2,
	SimpleGraphDemo_Start_m1020866D3AF4B593D22EDDB0983376602E893926,
	SimpleGraphDemo_Update_mC01EF2E4AF55144FD9E194D192528EAFA9BC7EBB,
	SimpleGraphDemo__ctor_mCA920101120C626ED2F51F62F7271B084ABB1A73,
	testMarker_Start_mD705CD930485C3D340711377C94A2E973A77F457,
	testMarker_Update_mEBDEE74F58D9845D91A1610E211797C38864529E,
	testMarker__ctor_mF617070254976CF3F3026AAFEE1F48315E8A69F1,
	BarChartFeed_Start_mCF5BB92B177EC0F36118B96992838567CFD64A70,
	BarChartFeed_Update_mF2484558CA8A9B5C9B4D82F529F0E6484785A289,
	BarChartFeed__ctor_mB27157699F062900B3D9A2274662E6B66D20BA86,
	BarRunChart_Start_m3FD38EE32D5865E0AE23E88E1FEE45A22AE4DD8A,
	BarRunChart_AddValuesToCategories_m8D5E303BCF4D84E375874B4F3F1A8EC1D70A4222,
	BarRunChart_Update_mF12748D6C43CBFB9E06BD7F4A893CA191E187148,
	BarRunChart__ctor_mF85502A5D12C28A2ACC3C605E97B00CCEBE2E464,
	RunChartEntry__ctor_m1D066248468841EA02363F3B2D018EB0B4829ED5,
	U3CU3Ec__cctor_mF08D3736BED3F89CF83438541DAC0B9E375C9B74,
	U3CU3Ec__ctor_m4353E711FBD0B7CD066369627C46E6DCB305C95E,
	U3CU3Ec_U3CUpdateU3Eb__7_0_m109A386926080C03C01C5B55B5C77F1D48F9ED79,
	ChangeOrder_Start_mC11098E12352FAF34A7FFE13838DC8E3E70BF361,
	ChangeOrder_Update_m8BD6D11F5D89E4DEB22B879EA29C82ED0638629B,
	ChangeOrder__ctor_mBA483888AAF9C227C5968455D551CE3B7E7019C4,
	barMaterialSelection_Start_m158C5F8D46E3530CCCD2108BE31CCEC2602937D9,
	barMaterialSelection_BarClicked_mDD951E45FDD23973F7B2EA482AEB111FF5B32BAA,
	barMaterialSelection_Update_mC4E71B687410902314A56C4BC086D4E5E8B29624,
	barMaterialSelection__ctor_mF65437273517EF6D80B3AAF5E65071CF153B6E18,
	GraphChartFeed_Start_mC46517BBD16B1F372C0C8D0D06F2CC367A51CC94,
	GraphChartFeed_ClearAll_mFDDC6A3A71B5FBA7B10276AA0261B00A77514396,
	GraphChartFeed__ctor_m53607376CD2E57B66DAEF00FE00AE590AA647495,
	U3CClearAllU3Ed__1__ctor_mC3D3B41DC95CA047E8CBB6D4EE7EE0A3C9B93887,
	U3CClearAllU3Ed__1_System_IDisposable_Dispose_m9C5BBF85B5384B6AB898F8AE85C44DCC0AA54B93,
	U3CClearAllU3Ed__1_MoveNext_m2172EB52E8BE21C63076A06F011B9C1CCEC85BC0,
	U3CClearAllU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BB8B95E41786ADB3903FAAE2B823A72A4E4C0CE,
	U3CClearAllU3Ed__1_System_Collections_IEnumerator_Reset_mFCAE984F92148CEAD61AFDC447B2B13371123DD3,
	U3CClearAllU3Ed__1_System_Collections_IEnumerator_get_Current_mC9FDAC481477167131505D1A917E36F942CA3CD9,
	LargeDataFeed_AppendRealtimeWithDownSampling_mE0EEAE4C96CEEA9D77B9BD7EF64B1DDDC81563D8,
	LargeDataFeed_RestartDownSampleCount_m63C6E59B6A95239ECF4B3AACEDB04A2909417084,
	LargeDataFeed_AppendPointRealtime_mA62CB1D0137B7D9AE4F2431E08A608826945B4BA,
	LargeDataFeed_Start_m76DD4C4F2B6691363FA41B511EE9662F4596BE18,
	LargeDataFeed_GetLastPoint_mB5D7BB8D4A8D5E24C7FA60300C265C2870DACF6F,
	LargeDataFeed_SetInitialData_m00A49AA96A900D2CA09192FAFDE542BB5D2A31B4,
	LargeDataFeed_SaveToFile_m50BE8E45403FAC9B822EA6BF7394DF38C18438C3,
	LargeDataFeed_LoadFromFile_m3810640434E9AFEB173A166825F1B358D20BFC2E,
	LargeDataFeed_VerifySorted_mF05FA4896171594EBDD70619C4EDCF8BC5D94793,
	LargeDataFeed_OnDataLoaded_mFDB94465EE376F394C028C0C3D97C202C1817F7C,
	LargeDataFeed_SetData_mBC3C2E4EC425FA58837CFF7DE383360509887ECD,
	LargeDataFeed_FindClosestIndex_m6A3D0BD8423F1F5EF270B3059FF742283DB0E609,
	LargeDataFeed_get_PageSizeFactor_m8DAB8785A6ED21DFBBF5966D77F78B4B259D5684,
	LargeDataFeed_findPointsForPage_mD3C283F1279C1D260E1D0410CBA05107C55E984A,
	LargeDataFeed_Update_m51C2C2AB1B9EDBB4FAC712A4D2F37A9F0FDA31B3,
	LargeDataFeed_LoadWithoutDownSampling_m19F74AAA12F7180BC674E2FE62BB1FA1CC80E2FC,
	LargeDataFeed_LoadWithDownSampling_mCDA5EB0BFBD18B8E720034D6E9DFD47A61D32477,
	LargeDataFeed_LoadPage_mA282390F88AEE74C3DCD6FD633F52B15FC346FC2,
	LargeDataFeed_Compare_m22E7EE022339655DD92F0A93AE5B287AEC912B34,
	LargeDataFeed__ctor_mB683B58D6D20001B2D4F29122D6AE9773AAAECE8,
	RealtimeAppend_Start_m757F71602AE019FBA3236C1F1C3DB51D1CD20F93,
	RealtimeAppend_Update_m2BCA69F6EEDA999A8D5B31705D9AEAF044F79FFC,
	RealtimeAppend__ctor_m7E4215EC7EEB90620D719DB2798D6E732BEDCC93,
	Marker_Start_m6A660AFF7D632ED278160FF9DD3A7342E9EADFAF,
	Marker_Redraw_m246E6B3C09D532542F35AC5B30E16CAED439498D,
	Marker_Update_mFACB0C819951C780128FD6B98D9C91154BA0866E,
	Marker__ctor_mBA175A38B7CB4A0F2876D1295CADB45D31D5AE81,
	MarkerSampleDataInitializer_Start_mCFE52B6410A96944BACF8CA18A28871DFAEA0643,
	MarkerSampleDataInitializer_Update_mDB86F4BAD15543184D23D74B423F379E404762EF,
	MarkerSampleDataInitializer__ctor_m34145AE40BB67D048C00402E05C8CD5B0F55A62B,
	MarkerText_Start_m8EE95DB6CF5050FFC960AB9A59C559D4171400DB,
	MarkerText_Update_m5942FFBC073F5735D0CB68B23CD0350F4FF9CD1A,
	MarkerText__ctor_m7604AC157CEE2ACB52DB0F4C0837D51C506D08AC,
	MarkerTextLine_Start_m293CD9E5E04CF374A6EBCEB5EB01FCB38A376994,
	MarkerTextLine_Update_mC4F35EED56F4D1EE2A4F02F0114570A96D6933D7,
	MarkerTextLine__ctor_m36F15D21924079D9140DFABE7ED2635D65BCA54B,
	PointHighlight_Start_mE83EEAA1E0C2BBD9B2F2477A8D46C83367E66BDD,
	PointHighlight_Update_m492E83424CCE9CFB4475D12FB29E5858338E0F06,
	PointHighlight_SelectText_m8A72511CDE4D7594F99C839614E4CD5C12AF43DE,
	PointHighlight_ClearHighLight_m6C71341422652AD7E13A6E70890CBBF8A90D0579,
	PointHighlight_RemoveText_m11AE1F2D1C4CDB6369CF59D404C625E680EBF669,
	PointHighlight_PopText_m589FFCA748A2033ECDB2E24DC91AA1DC1C5FE454,
	PointHighlight_HighlightPoint_mFED5373AE8C6DCBA1CEAA2E61AC3E9C3D6BDC1FF,
	PointHighlight__ctor_m2C4255BDDF747D12F9A2C054A130834B19B99BB7,
	HighLight__ctor_mF2354A1A5BA3BB45548381281F7E565B30297233,
	U3CU3Ec__cctor_m592F40E01D57E4F774203188498C6888A853D6DD,
	U3CU3Ec__ctor_m0D1AA3639356336DA13D4E0B840E69100C99E2EA,
	U3CU3Ec_U3CUpdateU3Eb__10_0_m173095F38045DD896ACC2D0177CEC4FA72A933A3,
	U3CSelectTextU3Ed__11__ctor_m3A5A14A90EDDBB09F4306CC9935BAB476EA0D3D0,
	U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m5D7B4F7984536AFA0FC52BED1C0B323B93EBB09A,
	U3CSelectTextU3Ed__11_MoveNext_mC6733F0CCDB24AB891E0C3AE239ACCF9E6D31EED,
	U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF876D96FB9874E3A5498EBF30EDA4899359AB74C,
	U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m9E9192BE9A6918BBBAE4E1CE01CE78D9250D66A1,
	U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m4FD7A47CBC202B781432238F82B2B019FDDC5878,
	TrackerLine_Start_m2ADA9CEED76770A873477E34C7A7619D8FEBE2FD,
	TrackerLine_Redraw_m363BF67695D229ACFBE4A35B8D37A79C173ECFF0,
	TrackerLine_Update_m275952B7668A2F56972AAA4C5F57B65D0952DE55,
	TrackerLine__ctor_m7D7A21B1AD697005B2089E4689AC710F82028F31,
	highlightTest_Start_m942E500F9ED5E4334B237A622F317A4ED147F12F,
	highlightTest_Update_m523180A6AA66C0C97D2AB7674559B933F710419F,
	highlightTest__ctor_mD5F4CD4618765C4046C15215F154D15E07F9A148,
	MixedCharts_Start_mCF02053BD18E51303EFF6CA09227CE7598AFF962,
	MixedCharts_FillGraphWait_m4D334BD00C325E608B9863ACF308B1CFB44CDBF4,
	MixedCharts_FillGraph_m5AC83EBB0AEB0BB7CC1FD2380D3CD370B9A16FB2,
	MixedCharts_Update_mA2AE0D5A046CEAB98A0A5ADAE870D8A706D658F3,
	MixedCharts__ctor_m9C343D27D4219352D21F64D7FD16074D8879E2B7,
	U3CFillGraphWaitU3Ed__3__ctor_m605B8A9DF0DEF3C1D6455E9106738FEF84BB1D27,
	U3CFillGraphWaitU3Ed__3_System_IDisposable_Dispose_m8252579F9A52AC98B6829F106FB8413592DB3A2A,
	U3CFillGraphWaitU3Ed__3_MoveNext_mED58E3A42B5766194A71A83F5C35D42E15D68C51,
	U3CFillGraphWaitU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7564710146FBEBC473D30BAD1A81F231303B6F7,
	U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_Reset_m5A91A4B97550476A7720F47010FEE9AB4FC11D8A,
	U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_get_Current_m349F3E4E8B051771D60B027E82B421A6949E0546,
	PieChartFeed_Start_m2FAB703A1E5B06EC14B4ED209A238D6A32574A89,
	PieChartFeed__ctor_mF02FD006D73FC177FE604D0C51864723707C76D2,
	RadarChartFeed_Start_m80026830A802376C7B2A1B6D500798AFF7C452E1,
	RadarChartFeed_Update_m94C77D25D55D9D6442009DF6FAF13646507BBD4E,
	RadarChartFeed__ctor_mFB451A90D16F97B8C4A27610CF7B12C59C3A4181,
	RadarMaxValueCategory_Start_m0FFD1C730DA015AFFDAA0D35CEA3DE8DAC9A3A13,
	RadarMaxValueCategory_Update_m4F2E8F9BF8A8A37A0A141D214A75FE61EE6BE706,
	RadarMaxValueCategory__ctor_m594CF6B8EA19D515F30C8E7EA70FDF85B78C25EF,
	StreamingGraph_Start_mD4A2B0315DEFC2F2CA28B7DA6FCE4490156EB999,
	StreamingGraph_Update_mF656AE25CE7BDC3085CED169CF14D55086D1F358,
	StreamingGraph__ctor_m2B8EC10B46F09D2CC5AB1C81F6A9C15510DBB805,
	WaveGraph_Start_mD2E2ECFE927A89ADA5822542D5F7E91BE61C58BD,
	WaveGraph_Update_m1D74D5C07A8958747F6694657CAEE0ED9AD922EE,
	WaveGraph__ctor_m4FCB3D355B36F108FA91CCA5BAAF79624C8FAA48,
	GraphRectZoom_Start_m534B0C62F408247F9E2DDF31E50F9A028CF56D5A,
	GraphRectZoom_OnMarkerStart_m836CA5EDFDB91AAF2AF06A61A115DEDD03E4EB60,
	GraphRectZoom_OnMarkerMove_mFB5C7D98DC21435689BBECA36808C1BFAE8645D9,
	GraphRectZoom_OnMarkerEnd_mB06D2925423EAFC0506A4D7A587D605F1D4DF92E,
	GraphRectZoom_CheckBounds_m6A4DEC96C2364376EE8F4DBF05D770A7D6164128,
	GraphRectZoom_SetGraphZoom_m52CC2F738F74683AD95737D896FDE7179B76E238,
	GraphRectZoom_Update_m6B1B0E346B66C7646D64E68A6AE3CCCDD71BFD26,
	GraphRectZoom__ctor_mF8D23AE3521019A1DF4761F276EB96FC69834979,
	GraphZoom_Start_m941005FD417E54C5C3DA2DE6210B7A5C2D62A409,
	GraphZoom_OnEnable_m1F4FF19C8E2C70D57A7CC464E7A33520FD2D7E7E,
	GraphZoom_ResetZoomAnchor_mCC41534C5CADEF5EFC6585F5A67BF3A7433D7C69,
	GraphZoom_CompareWithError_m003C12026BDFFFA8A044FE7759F596C32E49CFDB,
	GraphZoom_Update_mC06A6FF873100FCDCFBA2161563C6B35DFE99657,
	GraphZoom__ctor_m9E9E243260892F820508A31E107A1769E80B8A97,
	BasicSample_OnGUI_m6E672AACF828127DE47D9E00DD45A51B7F279CA2,
	BasicSample_WriteResult_m32B9AB2FCE0D7A7176210712FB9FE91C27492954,
	BasicSample_WriteResult_m67D71AA635FF37335895929720C7FFA79F783F13,
	BasicSample__ctor_m0836DDC774D6C525746E25A164BA323017461966,
	BasicSample_U3COnGUIU3Eb__1_0_m1A5B0705F6B217AA15C3D60BA3B8F957AE9A97DA,
	BasicSample_U3COnGUIU3Eb__1_1_m854014CDE7FA4D215818F9CCE18CC1C06D8666BE,
	BasicSample_U3COnGUIU3Eb__1_2_m640031C29BC7B5CA6966D6C5A57C5FA0F42654C8,
	CanvasSampleOpenFileImage_UploadFile_m90C1945D5935BD89355A03E33F76A4236ABF3EFE,
	CanvasSampleOpenFileImage_OnPointerDown_m7B12A59739CE20652F59FFE9D7AD1DDFB427B896,
	CanvasSampleOpenFileImage_OnFileUpload_m5B739156016141EA135B533C32C19FF584884096,
	CanvasSampleOpenFileImage_OutputRoutine_mC6AC6C7020F774BBBB5C1EC7D70113DAC9CE2AE1,
	CanvasSampleOpenFileImage__ctor_m502D7C88F0EAFFDDF1098646B4CBA5EC7EACCAC7,
	U3COutputRoutineU3Ed__4__ctor_m98B984849785F14A4C8AB9C792DD30C7E2C217DA,
	U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m93AEB066101CFA725B073186A05AA61331643C68,
	U3COutputRoutineU3Ed__4_MoveNext_mE761C79A87795F9557283DB7D7552330836956CA,
	U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED88C4EFD5B495F4D2972BFFEDEDA8A4FA23A8BE,
	U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m5F856A6AF129DDDCD28467ABFEA0B6EC6E826F73,
	U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m8F721991272364EAF9BEDDBBB389F230FF990E2A,
	CanvasSampleOpenFileText_UploadFile_m4E4B95184A67DCCA9DD2917CCC2DF9CEB29C208B,
	CanvasSampleOpenFileText_OnPointerDown_mB5C9CF79EBA459EF8242DA323855A69DE6911833,
	CanvasSampleOpenFileText_OnFileUpload_m962312A80189C05391D894DA031B283E66D52692,
	CanvasSampleOpenFileText_OutputRoutine_m6EC022ECFF5A958B50C9FD430032EE0A18972DD5,
	CanvasSampleOpenFileText__ctor_mDCBA3E4C4721B3282B2809AA94E632BFAB1E26D5,
	U3COutputRoutineU3Ed__4__ctor_m6052F5D62A93EBD9C0E11383EB07CBD7660382CD,
	U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m185B54E87BBFA49A5D93D56378C5D5B699C6D0E3,
	U3COutputRoutineU3Ed__4_MoveNext_m705E3655AA67F21B4822796EFBCAEF8C1080EB74,
	U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD425B64024BB6BC1769FAD619E67F9F309A486D8,
	U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_mF82EE533AC3EFB9F1A356CA8603DE322C066D634,
	U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m95A29BAFA9A8371AEF30517414CE3A5CD502F8B9,
	CanvasSampleOpenFileTextMultiple_UploadFile_mA7D5D0A7F3A0745C9FF979546132225E1F4CC00B,
	CanvasSampleOpenFileTextMultiple_OnPointerDown_mBA50A0847976B0736217F4D0794122C93597804B,
	CanvasSampleOpenFileTextMultiple_OnFileUpload_mA68667E8B5DB5196049132386BCE3B00D380E31E,
	CanvasSampleOpenFileTextMultiple_OutputRoutine_mBC9DD3FC851968220AFE1A5B97FF7F0A81A830C2,
	CanvasSampleOpenFileTextMultiple__ctor_m8BDD97617CD22220F998809BF61AF929523D0235,
	U3COutputRoutineU3Ed__4__ctor_mBDFF06C3C8D544BE9FBC7EA29B4790B7F3F288ED,
	U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_mEB828D2EA10D8A142380291F8820E27C7F743386,
	U3COutputRoutineU3Ed__4_MoveNext_mA793E0C183CAC70F902DCAD5155D5F37CD6BE7FB,
	U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57BAA75BD2D41E4F0A357D1679D79CF925EF1FD5,
	U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m15778AB91FC3ABCC7AA558BFE3224F64EA05925E,
	U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_mC1183837FBAE1B02788D12B358A1F890CE7D069F,
	CanvasSampleSaveFileImage_Awake_m8279E0A0C9F79F4705FA5D559052AB2A16B0BEF1,
	CanvasSampleSaveFileImage_DownloadFile_mBBBD310465F04A8B8ABCDB1FBB5933904F08254D,
	CanvasSampleSaveFileImage_OnPointerDown_m545F2AE468647734038CDE5278E5DD2ADB455B08,
	CanvasSampleSaveFileImage_OnFileDownload_mBF87377B9C4E63F218254B1F9516E4E62AA6DE72,
	CanvasSampleSaveFileImage__ctor_m445DB33600F854C4C7A506D6AA806EB27E58258F,
	CanvasSampleSaveFileText_DownloadFile_m19DA24D4A59805CB059BE18FA07007256499A4FE,
	CanvasSampleSaveFileText_OnPointerDown_mDAEA89215F6D4CF505D0BA6DCC292632834B981A,
	CanvasSampleSaveFileText_OnFileDownload_m70504434EBFDA158BE2583629ED97C08D85604E3,
	CanvasSampleSaveFileText__ctor_m12FE75537ECAF54983996FB25F589BBC106F19C1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExtensionFilter__ctor_m005AD482A024C782159F183FFD5579DBF4CD7887,
	StandaloneFileBrowser__cctor_m2493A0EF5EBEADA117EA6563BF66748585BD9541,
	StandaloneFileBrowser_OpenFilePanel_m2FE9F90D42A9265BBC2C374BD1DD3A49069B9098,
	StandaloneFileBrowser_OpenFilePanel_m1206C5DCC43D26EC15F5037E21B1B9BB490BB45C,
	StandaloneFileBrowser_OpenFilePanelAsync_m806F59EEE9E55C81803D1986A8F68B9D26000483,
	StandaloneFileBrowser_OpenFilePanelAsync_m39CA8A47A6C945AE7799D8CC901EE998F1CEFAC8,
	StandaloneFileBrowser_OpenFolderPanel_mD17CCB7D2DDD75D9063D66E2D36D51272A388B79,
	StandaloneFileBrowser_OpenFolderPanelAsync_mA7D50D60D47ACA560FE3C89DE40FEF6D859D5E72,
	StandaloneFileBrowser_SaveFilePanel_m750A45BB58A0223D8F1F00E110FC8561733A9CA7,
	StandaloneFileBrowser_SaveFilePanel_m7FE84762BA014DAE82BC87D9236272976C448B13,
	StandaloneFileBrowser_SaveFilePanelAsync_mB1C994ED3411EE335514CBE4A29F8C9D28F4618C,
	StandaloneFileBrowser_SaveFilePanelAsync_m5BF3B698F3A0B2BBDB43DDC8C9CEDF1AFAB0B31A,
	StandaloneFileBrowser__ctor_mC76A61DA78458B95631E10E194EB265C54179194,
	NULL,
	JSONNode_get_Item_m794CAD49418793072572FF0B76C678788DD70A5C,
	JSONNode_set_Item_m54443C1E880509EAF765765B8F2116AAD06F16D7,
	JSONNode_get_Item_m2F810D89B8E8BC291F96EA31AA965F2CD56AAA52,
	JSONNode_set_Item_m71FFBF3F4D95690C885BD569ED4C9E32C9C616E1,
	JSONNode_get_Value_m2D883EA2A35F24E8ED1B543DD5B528B7EE1EEA77,
	JSONNode_set_Value_m0E14AE5ADF273C3B5007AB2283F8A149BB8EDF38,
	JSONNode_get_Count_m325702C7DA358809F2E67A55A5FC38D32CC6964D,
	JSONNode_get_IsNumber_m47E1B8E1C9771B8484B0173F41B20AF62AF9E286,
	JSONNode_get_IsString_mDF6E7B0355D1B91D4255711CDA9030C31D915C0B,
	JSONNode_get_IsBoolean_mBC36FD7A7ABD4F62A0B447408A1486E37CE099DF,
	JSONNode_get_IsNull_m67FBFCDC6544EE60A9EF96502A52CED6FE4C3A59,
	JSONNode_get_IsArray_m65F0BD407E70A32F9788DF0294D0BDC2E5BEBC2F,
	JSONNode_get_IsObject_mD22CEBA382CCB97D8920608BE73780F79209A000,
	JSONNode_get_Inline_m6DC6F231D1C99C89D35DCA0E9CFB09C30F3211EE,
	JSONNode_set_Inline_mFD2207D0BCDB10D9DB3A11CD30D40DC18B629DB8,
	JSONNode_Add_mD86BF9C95349B6FE2867D0BD3DEEC2D7C87B7472,
	JSONNode_Add_m16EB0A81868783BC7600F795CA206B2243CADD67,
	JSONNode_Remove_m76E123309D5DEDB04B12443AB0C7FA821F18CD33,
	JSONNode_Remove_m234E9757857028D2185A26B1D125BFF0F55B112B,
	JSONNode_Remove_m2695928FBB81ACC4103A9D2481EBB4618ACCEB3E,
	JSONNode_get_Children_m9A419003FE3E6C582C581168325DFF2F916EA4E6,
	JSONNode_get_DeepChildren_mA4B063A992BD18F9254C2E4798DA93637ED05EEE,
	JSONNode_ToString_mB2B88FE44E1C28E46DA63108861FD2253915E3CF,
	JSONNode_ToString_mDCFC0B2C49FD5F86F8F9B87B901A798F35566880,
	NULL,
	NULL,
	JSONNode_get_Linq_m034EE9CE2759B88857897900689A0DA38D2E0A88,
	JSONNode_get_Keys_mEB33DEC4138BBDB5741ACC2707C371046102CB52,
	JSONNode_get_Values_m14983FAA7ABFAA16F8D0833F5F69961D7A3775FE,
	JSONNode_get_AsDouble_m4FE7D6A1571A71C97F12B7199A200DAF6226822D,
	JSONNode_set_AsDouble_mD7BF0BF13A7DCA4BAF089D24BB96E4E18FCF5691,
	JSONNode_get_AsInt_mC2BCD4318263CF5717BA098C4CEB6650F2462A6E,
	JSONNode_set_AsInt_m1E641BB048DC71F31439F97D7AC04F859C043669,
	JSONNode_get_AsFloat_m92DA2DA95D110DB99A8BEB14922E0C79E48F91A8,
	JSONNode_set_AsFloat_mEB0ADE3AF3166894A27C91EF7442D99EBC7379F6,
	JSONNode_get_AsBool_m6180FF46346F4B2FDB4676641483185606729746,
	JSONNode_set_AsBool_mA657BCFE82A9BCC2380C4725F6EFD697A54EFAC5,
	JSONNode_get_AsLong_mD41DE2D1B2C4922C0EACD72743CCF195877DF865,
	JSONNode_set_AsLong_mE663F955EA3FC88B8D62D9D7178FDCA5A65D0B7C,
	JSONNode_get_AsArray_m935EC207506B03E85E995C0E8281EB16D868E9EA,
	JSONNode_get_AsObject_m3D777DBA0FE5C294C0B0FD3AAA95769700595C8F,
	JSONNode_op_Implicit_m1EA46A77855F3FC4A14E9EE9D71DDA4E212A4EA9,
	JSONNode_op_Implicit_mB9E91405E9FB4D141BCAB7C0BB77156AF05EFF39,
	JSONNode_op_Implicit_mFEE3537E5F4B6B61E156471D4C9B144448483CF3,
	JSONNode_op_Implicit_m79BBDB2120CB96E6ADBC7FAAE8A935F087191987,
	JSONNode_op_Implicit_mC2C4DF10AB75CE2EAB0B550E4C94FA0974C02389,
	JSONNode_op_Implicit_m892C57C4F0D57F1FC33F3830D7F6D9DC516BA81F,
	JSONNode_op_Implicit_m12829D9BCEC10C160E56E670456EB34D0FD6F55B,
	JSONNode_op_Implicit_mF555AAFFA9ADEF6A0AFAB59E40EF53EDC2E13136,
	JSONNode_op_Implicit_mCE3A6DC67C99802CD46822391901380ADEE56187,
	JSONNode_op_Implicit_m6DE4662491A85E54BF525C11716CB121FC43E55A,
	JSONNode_op_Implicit_mC6864B10BE10BBE1A2B9BB7B13FCA926E01C00B4,
	JSONNode_op_Implicit_m20BDA805DC04973A0B86DF2EEA0E1EC5294C2A80,
	JSONNode_op_Implicit_mF66EA64CDA940B1CB6769E2311F01C56EEE7A669,
	JSONNode_op_Equality_m4BADF8D78593A991CE67F962BF80C563709EF81F,
	JSONNode_op_Inequality_m331AB33A8063DD8CF5A013EBEE1ACA8B36B8D82E,
	JSONNode_Equals_m0DF2B3AC02B8FACD90E7199431586AE86E7881C9,
	JSONNode_GetHashCode_mC44AFA4860F2802B814A42AE17FDE50CD6A2CC2A,
	JSONNode_get_EscapeBuilder_mFA295DED38D7E52FB2CC1E8239E9535ECF5C5818,
	JSONNode_Escape_mCD71AA7D631E356B1221E932CD6B1B5B398A96C5,
	JSONNode_ParseElement_mB9BD86BAFDC3EA7D2B1749F9E4DD17414A333C5F,
	JSONNode_Parse_mB215C31C5F1D4FED0CA19BB8927328B0611FFD2A,
	NULL,
	JSONNode_SaveToBinaryStream_m364D02A409828D427483CBD861175BADBEAD3E06,
	JSONNode_SaveToCompressedStream_mB3B0E7276E7311AB5617113BA6502AB9A9CE786F,
	JSONNode_SaveToCompressedFile_m926647312191674829BB64867DEFED85A98D777C,
	JSONNode_SaveToCompressedBase64_m265B2EAEBF7C9392D363BD300A4455B838010CF5,
	JSONNode_SaveToBinaryFile_m1F93E968F938C66B9D70C39EAB4015E5B87C9869,
	JSONNode_SaveToBinaryBase64_m87A8FF3221C5C605927E2026169A7C2D8793871B,
	JSONNode_DeserializeBinary_mD2AD63794CA1ED110E74BFA54DB2CA2BB64BDBBE,
	JSONNode_LoadFromCompressedFile_m85F2969047E442D5A10478F80ED14E6DFC443224,
	JSONNode_LoadFromCompressedStream_mAB76987DBDC8CA0EA873FE8FC2116CA91A71FDEE,
	JSONNode_LoadFromCompressedBase64_m2D98F8A86854C743F1F0CB48D69872CB4CDB572F,
	JSONNode_LoadFromBinaryStream_m0E1EB0742F235E299612B9B9D80067136D0D411F,
	JSONNode_LoadFromBinaryFile_m64AEB4673D02F096AD494E59CCE307A5BEDD6C3B,
	JSONNode_LoadFromBinaryBase64_m5A8AC6DD582B02410615C4CC4FE3B9CFE33C7B44,
	JSONNode_GetContainer_m19833331A9B8CA83914B36966E0EE16DCD4F7E07,
	JSONNode_op_Implicit_m8AE8B21A95048BF82C9461446DC5215E7E00057F,
	JSONNode_op_Implicit_m7088AB167559453B5CEDE102AE283D920CFE5D81,
	JSONNode_op_Implicit_mD8FAEC77BB6747DDEF65E05BCAB8AE6ECADE37A5,
	JSONNode_op_Implicit_m92C7DCD9C1E79A6C9E7AF7DD108C3417C1402D37,
	JSONNode_op_Implicit_mB9D115AA85C0415A0310D55A3D865A48D14D594E,
	JSONNode_op_Implicit_m26E17AC2E55A65696F6730C97AE2915BF012E3F6,
	JSONNode_op_Implicit_m8FFCBC62D03992E5237841E9031F21119DF1015B,
	JSONNode_op_Implicit_mCA03D4B5A8E753F9EDB1ECEBC49D6266889C4C55,
	JSONNode_op_Implicit_mDC6A9D6E4B47E7624919689D8D168530F99D2666,
	JSONNode_op_Implicit_mF76ED311632806F15586B03E77302A7962FB7248,
	JSONNode_op_Implicit_m335CE1D01340558BA5E810898EA46700C139EF2C,
	JSONNode_op_Implicit_m3882CE159C237C946365B1DA6680CAB8B97B8D10,
	JSONNode_ReadVector2_m08534E2F214BE9E8247E07BF9B42D2E0E9213BAB,
	JSONNode_ReadVector2_m91EEEC3B22770A79B7DE52BA9AC7DE248D5B0AFD,
	JSONNode_ReadVector2_m0711D27FE35DABB91E76DC672653BFE32BEF15A9,
	JSONNode_WriteVector2_m2581E822B71A15A42BFE7E878E81AE7D07480B6C,
	JSONNode_ReadVector3_m140E8784E3FF083C49AADA7CF7C1AAD6935D2ABD,
	JSONNode_ReadVector3_mDC7EEA763492B0C0C37EF950A10736F206527D0F,
	JSONNode_ReadVector3_mFDDA5015918CF9C6735B09ADBF27EB14A2572A5F,
	JSONNode_WriteVector3_m20BB306E4F882EA4DCCC37F590907B3E089362FB,
	JSONNode_ReadVector4_mFBB8B7B9CE8BCB9F9477F753C1C558BD7CD2A61D,
	JSONNode_ReadVector4_mCBE7BA3FC58F4DDB2964E84565B9AFAC0BD250AA,
	JSONNode_WriteVector4_mB812B63E5073CD324360D3064C05BE697F329455,
	JSONNode_ReadQuaternion_m738AAA0E05CA15EDE900EBC18CA7F46B51B1BDF1,
	JSONNode_ReadQuaternion_m453397A50EF15F27D8A1214B6AFB1F819B6EE6D7,
	JSONNode_WriteQuaternion_m883421C39E9D7AB9BB71786747025EC9E4D3DBFC,
	JSONNode_ReadRect_m8344137C21516678BA98CF5F2C9FBF5F70759787,
	JSONNode_ReadRect_mA43F4F30C279F39B71CA440A7D9E30B2050FB660,
	JSONNode_WriteRect_m79D99D1042AE7D7CA7F2BCFE9CFB90D812B8028D,
	JSONNode_ReadRectOffset_m78C6B5191BB6D70FC582CCA571E08D6F416A9389,
	JSONNode_ReadRectOffset_mFC531ED84BCA06A2E04BFAAA847F5BA45506B7E2,
	JSONNode_WriteRectOffset_mE2786B33C174CEBE7A785AB871019EDBB72690A6,
	JSONNode_ReadMatrix_m16BED0622885EE7E8C2F8476570AAA5B99BFC3B2,
	JSONNode_WriteMatrix_mB5FE155E4F5D8AFE0FE59C1D2715BF4C43B0AD53,
	JSONNode__ctor_mAC243E2F0FCCE1CED0086918A168DA00553E37EF,
	JSONNode__cctor_mEE1261316A49FF10A2C347B45C165513A07C1D8B,
	Enumerator_get_IsValid_m0DF87B70D0CB8C46C3B337005248A54B7C86979F,
	Enumerator__ctor_mF85148FE3692D3904C073278CFE8716EB9B4B0E2,
	Enumerator__ctor_mA42ACFD81450787E450330C66FE5CBB9FD0644EC,
	Enumerator_get_Current_m80F00207F02904E3A63CA549AD1E34DB016C4A47,
	Enumerator_MoveNext_m9947F7EE949531A65662E0BA157E3534993C36BF,
	ValueEnumerator__ctor_mA049E3670B0C9E04EFFA9CD8702AA9A74DB9C718,
	ValueEnumerator__ctor_m65F47E21C6E8DBB2A363B8FCF67709863FF36C35,
	ValueEnumerator__ctor_mC832E5CC32931EE67681635AA27CF2ED262630ED,
	ValueEnumerator_get_Current_m285C54105C3F05F978CD31F37025375C39C83542,
	ValueEnumerator_MoveNext_mEA96FAD65F2306BBC4B323984BD480FE6D2DB5AF,
	ValueEnumerator_GetEnumerator_m44A09F9E247CBB2F7542237EF47BD0A985FC3663,
	KeyEnumerator__ctor_mD632C7FC751F302A79EC4073C7DFF80C496266F6,
	KeyEnumerator__ctor_m7D1CC885A67D308EED89D61D67DBD02D4D17DE79,
	KeyEnumerator__ctor_m7F24F46E4952C1FC737D3F638603E33208952562,
	KeyEnumerator_get_Current_mF0ABCA5D5FEBDF2C7FC8BFD989D44026434E463B,
	KeyEnumerator_MoveNext_m063B0761C6BC5BB84B8A9ADEBE965D51E8958D2A,
	KeyEnumerator_GetEnumerator_mA4473824DD3BB4D3421080111235E5A58757FF5D,
	LinqEnumerator__ctor_m52746D99CFC8343CD5EEB9C652BBE1C6D818155C,
	LinqEnumerator_get_Current_mFE8D852F1F4E3279DAA9518FBF7D940715746C06,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m7D26432CB580B89B4C9023756070C7A31F15BC3C,
	LinqEnumerator_MoveNext_mB0FFCA63B015A030053490951E34F49F79ABFB73,
	LinqEnumerator_Dispose_m8E4D9BBBEED3B31C2CC1CAAA68A7215CCCFE2EDA,
	LinqEnumerator_GetEnumerator_mBDFE8718EE63E4A1ACE83CF84503100810519E59,
	LinqEnumerator_Reset_mF72A212DEF6A83497140DAC59BD22C5EE70EC24C,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m8C2ACB3FBF5A13CEF75897520E969725DF5C6F64,
	U3Cget_ChildrenU3Ed__40__ctor_mD2664979A1B8AE875DDB2840B6156772FB3ECFDD,
	U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m944D00543660D19F0E42EC6B6B139CB5D7EDFECC,
	U3Cget_ChildrenU3Ed__40_MoveNext_m453C570B96B371691FD54DB0A8DDE5E3943D4532,
	U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m4FC76B45630A04E0A0046A1409F00FB5FABE46B9,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_m97CD00E06349E3DE3CAD68B058AE2EA97797AEDC,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_m8F61A110C435979412391A15BC54115CA7E0DEE5,
	U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_mD91303D1AB77DD3B2593235773ED3B39DF1C2895,
	U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m8E4D404140934BAD769BAA96C71954DF437DBEB4,
	U3Cget_DeepChildrenU3Ed__42__ctor_m1AD4A760497B8C4CAB4E547E035B8ED4EBDDBFAB,
	U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_m75E2DD336D0A67E1EA6196D218C1F2D9E26D2242,
	U3Cget_DeepChildrenU3Ed__42_MoveNext_m41D74C189B9AC160605BE456F1413E3553FB50DD,
	U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally1_m17CEE5F8A4091B0148EF7B35CBCC74D44DBE889B,
	U3Cget_DeepChildrenU3Ed__42_U3CU3Em__Finally2_mF90971AD878C5B085E0FB7E610C4E0D66638E83D,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_mF52E7C66AC2CFE035C5422378EBA10E005BD16FD,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_m093AE80D972ACB3ED4CF25C53041441B789576A5,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_mDC85D32E36F7E3EEA9C509E44985F3540F4659A4,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m3E11223B21486BF7B377ED4B07EA9557C32ED634,
	U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mB972D9FF70DEDEBBC7507696C8353787B73D448A,
	JSONArray_get_Inline_mF3E98F07FB29748FF607D07002E1C6A5F655B661,
	JSONArray_set_Inline_m6159E8B429821AFA42475DB06771F2912736C625,
	JSONArray_get_Tag_mC315B2AB653862B003EA9B2ED0107A5197C459F4,
	JSONArray_get_IsArray_mAB3875FA6030E364164F591576AD6A06A4184DEF,
	JSONArray_GetEnumerator_mDCA4E401C30BAFA976195E0EBA00FC512FB09F78,
	JSONArray_get_Item_m80D71FF8EF86521ECCBD31757FB9A95CAD530F7A,
	JSONArray_set_Item_m5A43B0FA2E75E4C41DD67AF7440FA1889A745037,
	JSONArray_get_Item_mAAFA9403173B73244F3E047CB922F13E97BCE76A,
	JSONArray_set_Item_mBA3DB6911937ED140443C9F48FD611585F5950C2,
	JSONArray_get_Count_m98BA6144D0680CBA5A32FA14A45C27864E79932E,
	JSONArray_Add_m768ED67829CECDA6214620ECAEFBC5E5959E80EF,
	JSONArray_Remove_m541331871627A71D31DC30A0B29E4A380D81EEE4,
	JSONArray_Remove_mBEEFAA1049D8BF88BB5E132A8701E9D23E18FA7F,
	JSONArray_get_Children_m7459647CAFE8FB57E0C4D2F757AA72BCA7749CEC,
	JSONArray_WriteToStringBuilder_mA146E626CA9AFB66DDA87A6BCF7DB0F707207D3E,
	JSONArray_SerializeBinary_mFBB242F279540C196367396678815DDCE69E1748,
	JSONArray__ctor_mF4CAE833013E2A698568D4367805C2C51D0D25D2,
	U3Cget_ChildrenU3Ed__22__ctor_m09E100DFDE814208000E288F38AACAA139288972,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mFBB72D4FC09BA522703066660A1B6C84A2B2662E,
	U3Cget_ChildrenU3Ed__22_MoveNext_m5842B46A70689B08A17107B86E6DFBB3300F458F,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_m3252C32520E6F43AFE392CB6919CFE1BC2B6F0E3,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m0997099AAF452C670AC89A3DC8EB6F3F42EF8A2E,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mA44FDA37FE9633368C64D524CC7FFBD664058879,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mDB53E925A7A6DC776A4F6BBB37B8E7AE9A7B4ACC,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m85636C898F51CA71FB68F27A5F0E3AE34CE40854,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m2724CBA0FE19F63D89407542FD0085D90ACBEC0A,
	JSONObject_get_Inline_m9BCB66E97A2F88F44B775108A4B5E18F17888A2F,
	JSONObject_set_Inline_mC3F731FE78812DAA7E3A70CAE5A72EBC5758D8CC,
	JSONObject_get_Tag_m03F79226FA30F84478F294ECCB99AB694861CF4F,
	JSONObject_get_IsObject_m67321BA44A6C4D8BB5A1438A879E688A2E7DE28D,
	JSONObject_GetEnumerator_m63EE29843A910F89E618BB31706D3854868BFFDE,
	JSONObject_get_Item_m72B96FA02E6771FD0C8E75C91BEDF9606634B7F5,
	JSONObject_set_Item_mEF09124374182C135B41DB681FB7C415DB31C41D,
	JSONObject_get_Item_mF51497D1C688FBF842D2190AED82F139F5F78FE4,
	JSONObject_set_Item_m322B2304A66474BFFC5D15FC609C1769E04EC673,
	JSONObject_get_Count_m726D1A71A88913BA27964996BAC70CD2EBA8570B,
	JSONObject_Add_m6B4706D9DE2281401B6D81C078FD52D9AE761CE2,
	JSONObject_Remove_m0AAFED5BF3EBBD4C7FC06F2EB5F5732C7AE89506,
	JSONObject_Remove_m27288429F778385D9F072DB4595DFADED9C46C2F,
	JSONObject_Remove_m2F2F348D5A73770DBBB900718FF31C8FAC263EA5,
	JSONObject_get_Children_mBA3F815A9E4AA185FB9D3B41346F80D3D89FCEEB,
	JSONObject_WriteToStringBuilder_mF7874C61E8E876BD5458DC7B33C6342873C3ABB6,
	JSONObject_SerializeBinary_m49377B90B1A9A0AC2B41C2B9972789F0DEEF79AF,
	JSONObject__ctor_mF4C8923987460A3B2D0B5B58CA337717B5D92899,
	U3CU3Ec__DisplayClass21_0__ctor_mC5A02DDB9380C3E29DD7EE6239418532874371A1,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m201C2480DFA39BCC11721101323F5BECBBA1E5E4,
	U3Cget_ChildrenU3Ed__23__ctor_m27FE9E533731FF76A4DA5A1495777DE4976EB2B5,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m14D80EBCF6DEF404F017771F3E97B465C4E7B66A,
	U3Cget_ChildrenU3Ed__23_MoveNext_m3ECB5D4B30C8D5A240916179E1D34DAB38B6AB68,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m90DD4395C8759994DE05DF05C9298C2B4F31A560,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m406355CB8A2EB21F8264E1A23C80A1921AB1C795,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mC76C5EF6AE752D074A3A428BEBAA2A1CCAB85232,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m99340EAA456C373D91FF62F016A592BBCFDAEC01,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m1ADC7153B59830633F604BBAB11DC65F176AC157,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m78AC7A7CE03612379624BE539ABBE50619F0149C,
	JSONString_get_Tag_mCF53FBECB4BDEABC5574CF3439C685136FA99B9F,
	JSONString_get_IsString_mFD6A68DD25465F32A5EE6CCF96D6E454EBAEC5EA,
	JSONString_GetEnumerator_mBFB6072AEFD5212172A37AB3E2357B6459511768,
	JSONString_get_Value_mA01365AAB55A95B641FA6BC66A9B3B23A47EB125,
	JSONString_set_Value_m4F50250EDF80AA623B85137C07068C3E1A010C8E,
	JSONString__ctor_mB62055173E2C53E15EBB4D505F19FACE8A0BF37F,
	JSONString_WriteToStringBuilder_m159FDFF47242E33DD85F18C19AE7092397D18375,
	JSONString_Equals_m6BAB16DFE509FAD6FEE6AE480A036B7BDEA5D2A4,
	JSONString_GetHashCode_mE751F12FDD59253CD819097906EAC9CF751BA10E,
	JSONString_SerializeBinary_m52D938CE59B95C44733F2403B81EAF780A30AB00,
	JSONNumber_get_Tag_m024BFC581B538629B7D2FAD15CEB077F2F9E3279,
	JSONNumber_get_IsNumber_mFB0F9F911212685A1310E8BA979644E88512E786,
	JSONNumber_GetEnumerator_m7C9A36F1FB2609D397B11CBCF864720927F00A77,
	JSONNumber_get_Value_m82E5E24487E19999083957C1AF260876F6EDA3F6,
	JSONNumber_set_Value_mF166132B90DB5D575E20840B950FFE991C430728,
	JSONNumber_get_AsDouble_m8B128C3B233ACBB22607C9D9CC55F90C8FFD4886,
	JSONNumber_set_AsDouble_mFB47CE3DD2520105AABB9D2DBCEB4AF98C564E36,
	JSONNumber_get_AsLong_m245F4F4F46DD1873684A486D5A83F2D6D1B6EB88,
	JSONNumber_set_AsLong_mC3399978ABB03B713E5E552F7878785AC0A56FB3,
	JSONNumber__ctor_mF62A940B0E8E278BB2A3E96DE923AFF31B8E2FD1,
	JSONNumber__ctor_m9A89C54F6EB8B7131AA1FB60D3CB47FEFB43E748,
	JSONNumber_WriteToStringBuilder_mD0D48E0E46FED6E80C7F0FC038F385CE0EB23CEE,
	JSONNumber_IsNumeric_mA7C80E2D8BAF67F3631BE93450FFFF552FFD0CEC,
	JSONNumber_Equals_mB1E8BD369ACF8A5330E5342061992567C6EB5B9A,
	JSONNumber_GetHashCode_mBCCC23C533A875C641F49F87CFCBD3E7A55EC6FF,
	JSONNumber_SerializeBinary_m75AF22D10F227ABEF1E50B48AC9079C6BFDA295E,
	JSONBool_get_Tag_mFEC25FE31384718FCA71A25AFB90BD8FCFF82E7C,
	JSONBool_get_IsBoolean_m9FBCF0252BDDB22CFBB61A6A7E5DF716BFCC9DA3,
	JSONBool_GetEnumerator_mC419198BFED4A5878607F4977393029D43E7D462,
	JSONBool_get_Value_mAD379D54D1808097239F0BD40ECBDA0A1F2B41DF,
	JSONBool_set_Value_m4C612D8CF5720CC1C69C66D5F4BF84325EF727FC,
	JSONBool_get_AsBool_m6B3A77EE68C4EB21286885998407068F2BEEBA42,
	JSONBool_set_AsBool_mEFF054DBE90574E01A90AC547B21A8F645075B69,
	JSONBool__ctor_m80476A3F2422A1617C1A7D5F1D0B0F05AB3793F4,
	JSONBool__ctor_mBB081C7E3DE25C02EC0A7F3151CEF3E1ED297746,
	JSONBool_WriteToStringBuilder_mD74EE78BFD28A1A78EEE832027DEC0ED92B93F3E,
	JSONBool_Equals_m22F59C35F0BC4174ADDAF3F54F48B0E845F8D167,
	JSONBool_GetHashCode_mBF084C8ED32DA5C68C0FD033CEF8BBDF7B24A2E7,
	JSONBool_SerializeBinary_m83EAFC1CDB6FAF99455803C977BBDB20DB70F12B,
	JSONNull_CreateOrGet_m732F03FC4B1B326999FCC84E58DDBB4B46845B86,
	JSONNull__ctor_mE33227FE33336DA77DDF210DA89CE5A493A0D6B1,
	JSONNull_get_Tag_m1F12D6FC09303C5A73AB7B49BC174BE76F4216BF,
	JSONNull_get_IsNull_mC2E0E55DE67FCDDA18C01EF4B3A80DB8446A1C34,
	JSONNull_GetEnumerator_m228222FB6E86CA2C4462C26608CF8D305960BE74,
	JSONNull_get_Value_m4B46351EFCA2D41C2A136CECC211B0E7B95CF154,
	JSONNull_set_Value_m523880E340A52C95FB87F10A36222F42038818B8,
	JSONNull_get_AsBool_mC82D3992D82908950C4D5EF20D7F133653139D48,
	JSONNull_set_AsBool_mC92BCA0758D456516F83731B8A90A02C53F8F187,
	JSONNull_Equals_mAED9044EF895937E717029B2466B6EAAA07D5283,
	JSONNull_GetHashCode_m01DEEA5B857D405F4B9D7C523ADF1D10A8C4978F,
	JSONNull_WriteToStringBuilder_m599C035DD34051A49E96CBE32A948E557F3816DA,
	JSONNull_SerializeBinary_m685B6B1CF8FB32ABD6CCF2E7664A9DB8C4FEB6FF,
	JSONNull__cctor_mE6B7842FB044E55A54B9FC5803EFE681848E89FF,
	JSONLazyCreator_get_Tag_m53B8355F7E34B93B4FBE03E0DEA97ACC30A1CF0D,
	JSONLazyCreator_GetEnumerator_m6A362148ABC47D9D0E3AE58AC5429F0BF89295F3,
	JSONLazyCreator__ctor_m0206B2AD3CDF575600494DD3147D6600797E193C,
	JSONLazyCreator__ctor_mE118EADFCC80752174DCBCD309862389F5A1A536,
	NULL,
	JSONLazyCreator_get_Item_mCEF9DF7DE4A82B301CA3F1079E105B502C8D58FF,
	JSONLazyCreator_set_Item_m6F38849DAD672D158B3F3D0FE2287E341FDC1B5B,
	JSONLazyCreator_get_Item_m9ABC4BCDD6712304064A7B03323E7F82CCE93DD5,
	JSONLazyCreator_set_Item_m9B0845ED7851163245504BEF8383024D10ADD343,
	JSONLazyCreator_Add_m11466E9A4417AF2CDEB892F2A69C9847EBA7DB04,
	JSONLazyCreator_Add_m9C55AE044732E0A31FD0DA3E66BA1F4718CB326D,
	JSONLazyCreator_op_Equality_mAB716C3C3AA936D1349287DDDC9BA2BC91ADED33,
	JSONLazyCreator_op_Inequality_m898E7A136513B0849CFF6EDA7B0C8D64943F4B11,
	JSONLazyCreator_Equals_mE85720D4743CF81DCEA41A4291EEE37F970C428A,
	JSONLazyCreator_GetHashCode_mF79437592680711E849C0080F66BA81E7E51EF12,
	JSONLazyCreator_get_AsInt_m1C73B790A7C3C5AE693EAFBA706F4D0A5C540632,
	JSONLazyCreator_set_AsInt_m5A16A8383AF6DDDC8A5D2829DA5FE4EAB5644BFA,
	JSONLazyCreator_get_AsFloat_m97909CA726018D15C4FF72F1F1C7433C10201B10,
	JSONLazyCreator_set_AsFloat_m331F57597BC524EA61394EED0CF8B88EFB60E603,
	JSONLazyCreator_get_AsDouble_mBAE70F3DFCCE90D520D84AB402AEA5B95544F36E,
	JSONLazyCreator_set_AsDouble_m2CDB70171988285F108CA0CCF97E0DA3C4BD9459,
	JSONLazyCreator_get_AsLong_m4BCE83EF678D35AF8C8E790003018D8940A25988,
	JSONLazyCreator_set_AsLong_m8466EAA77C575251F031F27AB836D1F25C83F96F,
	JSONLazyCreator_get_AsBool_mB627E755CA8E9BF328A8328726F6779E80CA8FB1,
	JSONLazyCreator_set_AsBool_mFD07371E1640B276E9F05A8F1BDB5A4E561DBC52,
	JSONLazyCreator_get_AsArray_mDC3B3A51F41F93743ABA027B349A38F8118EAAD0,
	JSONLazyCreator_get_AsObject_m3152F723BAB90B8EBE72A8D8D89ED2941A3588BE,
	JSONLazyCreator_WriteToStringBuilder_m09BD0EB0DDB7908638241F408E76A0CA6022E973,
	JSONLazyCreator_SerializeBinary_m38363CDF296E0870C9A84AEC5CDFB39430006E4A,
	JSON_Parse_m72FD3AB5C5FDA9B75DCAFE8CB48D5CA5CB0F5F1B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChartParser__ctor_mFC87BF1464F23D6F3E3F271CBA8EB3E0F5131467,
	JsonParser__ctor_m3370E225292D26A54D9429E0C8A74F52D833D4F2,
	JsonParser_GetObjectFromRoot_m1FAF3E8B1FBF861E1D01D41C4890AE54FA0E0963,
	JsonParser_GetArraySize_m468DDADF873849630ABFDA65E978073DD737262A,
	JsonParser_GetChildObject_m6CE8DEFD882A4F8C021C4A3A05402EDC6ACF847F,
	JsonParser_SetPathRelativeTo_mDFAB6EA6BC254C43865DAA8A56DECE101A487087,
	JsonParser_GetObject_mA1B881FE53685438DF67E1EFF6A4380D0CB6E818,
	JsonParser_GetItem_mD37ECE8DFD5749000C5DBDC84CF19D3F04851BBA,
	JsonParser_GetItemObject_mE7193D47CF776DBCBB8013282374DAB6FFBEE6EB,
	JsonParser_ObjectValue_m9C9517A85AD54752E721D2CB26924B153F28767C,
	JsonParser_GetChildObjectValue_m0F455908D2B593C596F40F0A3D1F84344F08CCF7,
	JsonParser_GetAllChildObjects_m64B3F53686284293C50CD18063C787EF61B153C2,
	U3CGetAllChildObjectsU3Ed__12__ctor_m47139479FE975B344E47217D280FFC312E318714,
	U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_m8BBB18472020BE742C8E39B81239FDACD74663BB,
	U3CGetAllChildObjectsU3Ed__12_MoveNext_m75ECF193AAA94198E0494722EB8E9E3A2F8A8547,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m24515EF29C62E5B28F804336D64C95621B543A8A,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_mD2374F91492CA89E5E1A6224468E705D910E38CC,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m3715E650CF1A0C57AAA0F132B149816F130815C2,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_mEC7AFF580DB38394CB779DFBD45E50A202643EF1,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE9A7F7C95471C247081CF66366D9A4DA207933EF,
	XMLParser__ctor_mC2BD2EE105D8144DF44EBE297D4CB0E0366811A1,
	XMLParser_GetArraySize_m7A880B43FF36FCF46B828EA6812341E88D5D481D,
	XMLParser_GetChildObject_m22F41552E6625205629E822B906F5E239154B4AC,
	XMLParser_GetItemObject_m70D2A87E037F0BA55F700A78650AC6DADB764C41,
	XMLParser_GetObjectFromRoot_mD8457DD1700A172276FAFDC2EE9228267DE87B63,
	XMLParser_GetObject_m1E1EF741DA53BE600E11DA48E16E641EA7D1AC60,
	XMLParser_SetPathRelativeTo_mC3E0EB5AB619DFDF14D52B81255DD6B6D30C9349,
	XMLParser_GetChildObjectValue_m605A3851D801EC974B78781DF5A4711E90493F21,
	XMLParser_GetItem_mEE30A375F346E7F4A515F79E127CF81A2F9E4EFC,
	XMLParser_ObjectValue_mF0D45AFA6EC54B750A79EBC31EC902BFDF6969DB,
	XMLParser_GetAllChildObjects_mE2F140C257C62461EF9667E77BF1BD013EE8459B,
	U3CGetAllChildObjectsU3Ed__12__ctor_m5531B147F08A6B3597DD2C73FF2550DD018683FA,
	U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_mD6F2D843626DF724B8E34D5DF9B05FDD7D826D98,
	U3CGetAllChildObjectsU3Ed__12_MoveNext_m117865738C1733E7C59C8A24969F3D5B54CA3B46,
	U3CGetAllChildObjectsU3Ed__12_U3CU3Em__Finally1_mA395032706EF755A2A81E576B142FB02F1C575A8,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m5D93105895C6A0F0C06B8B11727EA7C8B91C8E8F,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_m5FB1E2B73B3C5F5EA1DA48A97191F445FCB1CE0C,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m24F99202D95EB103F25D1F71E0FDC04BED1A8CCE,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_m8F633C74F5985C71F8C1B945DF1A566EEF7DDDFE,
	U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m36887960DD50A7F7F13F9B101A78F4D3FCCA2741,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PyramidCanvasGenerator_Start_m3FD5C291056718D4050620325B625045E7DF3BA7,
	PyramidCanvasGenerator_get_ContainerObject_mF8C4A7BEE596075328CAAFA304660C06E97D6874,
	PyramidCanvasGenerator_SetAlpha_m6D31F97B91C4F90B8CDCCEE69B38DAEBD2F0360C,
	PyramidCanvasGenerator_get_mainTexture_m7689D2D85E95A5BA7AAF4F26A0F5E07E604C726D,
	PyramidCanvasGenerator_ApplyInfo_m46E514A59C53D3B241A3E7A7408B22E033D7007D,
	PyramidCanvasGenerator_Generate_m61F52416A217571B83C5D1695A2BF90E06841752,
	PyramidCanvasGenerator_GetTextPosition_mCBB29082C7CBE5CE08345E9370317FD00230F870,
	PyramidCanvasGenerator_GetUpperBase_mCDC8FC8388FD6D454B07A4EFB737EDAC8F4D6211,
	PyramidCanvasGenerator_SetParams_m72A8D7C514DE3E8F16E9049FAEE31B2BA70E0F5A,
	PyramidCanvasGenerator_OnFillVBO_m4A0C9DBB6343FF5AE039327B564122F5AD8EEBEF,
	PyramidCanvasGenerator_OnPopulateMesh_m906AF1F35339B3DE9EEE6473722C45DF8F341D4D,
	PyramidCanvasGenerator_FillChartMesh_mC4C1E3ABFE0085955920E42D2CA974E6CA738E74,
	PyramidCanvasGenerator__ctor_mCE0D1A7CFA6D38C2464DD6EBF858F4E38E311735,
	PyramidChart_get_BackMaterial_mA56FED0A62D50CCC1BA8FE6628F12DD6F155DDD4,
	PyramidChart_set_BackMaterial_mD3C2503EBCF20CBE7C4B2872836CCBB51CB38158,
	PyramidChart_get_Inset_m41BC9867B37961D8350A4B65521C1159F27D0F22,
	PyramidChart_set_Inset_m2B98A95C28A0F324C6B726C6807499918388E075,
	PyramidChart_get_Justification_m87E5C87281A74D69FE117ECEA3F7950FFF7B33D2,
	PyramidChart_set_Justification_mF088405EBDD438388812A4FAB53B159230D22ADA,
	PyramidChart_get_Slope_mEF559480DC7E8088381B7FCDC5CDFE6EAE6E1135,
	PyramidChart_set_Slope_mF48C8331B5DDF56829D6BD035649D2D5A15BDB22,
	PyramidChart_get_Prefab_m037CE0A74EBF3D86928BDF562FFC0CE018D92CC8,
	PyramidChart_set_Prefab_mC0A56F73E54ACF4F56B7F9884EB1EBB94ECAE30D,
	PyramidChart_get_DataLink_m823270B2DE66C2CA1FDEE7C89F3FF7CFA79CA632,
	PyramidChart_get_DataSource_mAB35BD540CF8E1E3D625AB03B9B4B2712F419135,
	PyramidChart_get_SupportRealtimeGeneration_mAC47FB57D2B1B814FD2567B1B3CFF8429F12BD4C,
	PyramidChart_get_LegendInfo_mC2A52E5FB65AF2ED9F28B47F845FA3B4E4F273F9,
	PyramidChart__ctor_m1ECF15F73E0F47D6A49CB71365C9C8589B168BF9,
	PyramidChart_HookEvents_mC0D642331413A4CA682B4B2B4B89CB15BE280EDF,
	PyramidChart_Data_RealtimeProperyUpdated_m8DA02EB25812767C5DBA92CF5F7DA56B73E20B02,
	PyramidChart_Data_ProperyUpdated_m0E47011984476F715C5884037B9DB410FEB5532A,
	PyramidChart_QuickInvalidate_m88299B9CB7451106B08F8F8B7AEB9E8B1ED1F2C0,
	PyramidChart_Invalidate_m77A18AB00C5DA0D7F8F78159F48F8DEAB8CAEE4E,
	PyramidChart_MDataSource_DataValueChanged_m68E1556109EF4FF996560C8393EFEBAFFD843CCC,
	PyramidChart_MDataSource_DataStructureChanged_m21380B60596C8753D6B9E4B7683E7B7FAEC20D63,
	PyramidChart_Start_m836088CE367D839887D784312D025E3CA176BAE0,
	PyramidChart_OnValidate_mAE7FB632A76C2A537B6368A5B4E206CAEF8EA156,
	PyramidChart_ClearChart_m6A895DCE7183794A03B275CC2F765FEB45C34493,
	PyramidChart_AlignTextPosition_m02188F7550276DBA58024FF433D2BCE09183A585,
	PyramidChart_PreparePyramidObject_m8438E904C23C8FC45EB10F8B3B864E32D62E55CB,
	PyramidChart_GeneratePyramid_mFFA8E14066A08D78094F9FCA24D106AEFF55392D,
	PyramidChart_OnLabelSettingChanged_m9A4CFD3C3B20DE7933B212D1303F0EB17FD34CC3,
	PyramidChart_OnLabelSettingsSet_mDAD07C62AE2E5699A53D88126CC3477FE8348C81,
	PyramidChart_InternalGenerateChart_m269E27B790C02F6F5960733B708BF1D86896836B,
	PyramidChart_HasValues_m26003D484862783E61C3BE483F915F0D564EB0EF,
	PyramidChart_MaxValue_m03294524BA8CA0AA3B103314C8D209524415EB6E,
	PyramidChart_MinValue_m5A0B6C062736E98566BB09F087E8E6FD09F7A7E7,
	PyramidChart_OnPropertyChanged_m982A1C311EB0DC03A42ED8DD701263BBCAD66E76,
	PyramidChart_OnPropertyUpdated_mC49FBBE06BEBA5EB523048FEC8392DE59C7AFF5C,
	PyramidChart_ValidateProperties_m8D4ED101C8BA6DFF9E624C2F20789EDE9F1B6004,
	PyramidChart_userDataToEventArgs_m66E5A2C9B2B55EB785A26EDF7240BFF74291187C,
	PyramidChart_OnNonHoverted_m4DD3637FE918440E8751F6F57E9941C84205C8AA,
	PyramidChart_OnItemHoverted_m8FF6354013197D34E98CCBFF082B7A5918BB166B,
	PyramidChart_OnItemSelected_mD99999AD64BB07EB544B9B080C20AF436D68EC59,
	PyramidChart_Update_m1C89CDD141E5A1B82D4B061CE0BEF6AF4595794B,
	PyramidChart_get_SupportsCategoryLabels_m07BD1390816F54C786D0239312370612E65E1566,
	PyramidChart_get_SupportsGroupLables_m0AA6B0921C3C6E3CE62E86B9B0F92F29DBFD25DD,
	PyramidChart_get_SupportsItemLabels_mE68018377E0BF2C948BE68DB36F85F8A6DBC88A1,
	PyramidChart_get_ShouldFitCanvas_m3FA92B1208892DB13117F79F69A7F09F52EA2C7B,
	PyramidChart_get_TotalDepthLink_m9BE4B0C8F1A56790AA32996B4346B0EC24E2614B,
	PyramidChart_get_TotalHeightLink_m420A6C084A59C3AF4D1506AC07537C7F50F9DAB5,
	PyramidChart_get_TotalWidthLink_m31BD46131F31353F4EAF00C5B1301DDCFA480FB1,
	PyramidChart_get_IsCanvas_m80278D306F23631C678400B0E2AF2D9C85871819,
	PyramidEventArgs__ctor_mC1BB707334CF442DE1CB6201569427E0B0D41FF9,
	PyramidEventArgs_get_Category_m775875544DFBCAB3C0166BD745E20B325A86949D,
	PyramidEventArgs_set_Category_mDE1EDDF8E56E2BE4BFA785EC1BE243D6BFE91E47,
	PyramidEventArgs_get_Title_m9DFB5ADA82BE5AB49408925E61963B2A0FA8A5CF,
	PyramidEventArgs_set_Title_m6A6B22563451D13C04CA6969727FD453C5523235,
	PyramidEventArgs_get_Text_mE039C1B01C569C62FF5A03AAB2FB2E60C9697B13,
	PyramidEventArgs_set_Text_mA128C4B843BCB63A903A1C4EF0CD6CF9168BCFBE,
	PyramidEvent__ctor_m0AFBEBE0C9DA12D1A59721BEF39B58351001D502,
	PyramidObject__ctor_m62D49387C8514901330AC3E3E39E174F7184C6F9,
	PyramidData__ctor_m5B8CD433B0A55C417C6C40D46AD8A7A7E6612EFD,
	PyramidData_ChartAndGraph_IInternalPyramidData_get_InternalDataSource_m5D75A460A521FCD5F1C69C18F71B26A63C0683F6,
	PyramidData_get_TotalCategories_m84D145E6E68EE264D7177B3EDE85B1F14AD2CA6B,
	PyramidData_Update_m63E7D966C1327A5CBB63638AF4A555EEF8709F23,
	PyramidData_GetCategoryName_m4B9CCE041686BA7FDD1A1897443E5263C95152BF,
	PyramidData_OnBeforeSerialize_m25AE45276AF2438D24034F094F74FB16D4DB4D0A,
	PyramidData_add_ProperyUpdated_m8DFF08F96D64766B67602D3E02BCD47F9E38C7C4,
	PyramidData_remove_ProperyUpdated_m6AD009FE63BD91431ECBEA9447D177E4A0A902AC,
	PyramidData_add_RealtimeProperyUpdated_m11C0E533367D009433E0BEAA414BF8FE6E37E88D,
	PyramidData_remove_RealtimeProperyUpdated_mE3E870B58AC7B15C2FAF95507D56A16D56181C02,
	PyramidData_RaiseRealtimePropertyUpdated_m68B605C98C5B7586F979740D491C431E36535F80,
	PyramidData_RaisePropertyUpdated_m4D7BA441A73F1B2FA8F67DD7F287DF0E0009F953,
	PyramidData_HasCategory_m3D4E376362E7BAA071655FBEEE717E327CDBC488,
	PyramidData_RenameCategory_m11A4C747F4EABA80DEA34F341BD8275E2FD54996,
	PyramidData_StoreCategory_mDB746D21E9CC7B8961AF51F33E9C67344A54330F,
	PyramidData_RestoreCategory_m2066AA8F45E53852D06D00C7E6A9A9FAB9CF25E1,
	PyramidData_StartBatch_m2310DE39509776A0CACECE282F24703658BB5C71,
	PyramidData_EndBatch_m47E44102D2B815451A5F9B08E8C2BF0FCFD5EEE1,
	PyramidData_OnAfterDeserialize_mD0D95924A23343A83968DFBE6F2E381B1A9BF3FA,
	PyramidData_AddGroup_m913471BFE1CCF039FF256F41C1DCBAAD7D4C2A2A,
	PyramidData_AddCategory_m54326A85E596AA466625709B976DA952AC8F8DFA,
	PyramidData_AddCategory_m03D37ED563B559DCD4D553F0C70E9A26EF99180B,
	PyramidData_SetCategoryInfo_mE46FC9BFE3E0FA6086E18AE56318D3070C328849,
	PyramidData_SetCategoryAlpha_m1E21117FB6F3DECEA094A9E1365C4A2B08619367,
	PyramidData_SetCategoryContentScale_m6AB737853A37E1F3D286DA4721920996146AF253,
	PyramidData_SetCategoryOrientation_mD9D756454E1D432F9F4DD3110DBA20B4B6474D1D,
	PyramidData_SetCategorySlope_m32B3F622DDA435A52A9AF755B6068C903C45168D,
	PyramidData_SetCategoryHeightRatio_m15F630E30B595AC5335BEC40832D3509D873E8E2,
	PyramidData_Clear_m3A9530E66AF386C4CF97153FD5FFDAB3BF5B47AD,
	PyramidData_SetMaterial_m8FA27AD0EFBAF2F7C4C6A7D687C096FDAD942744,
	PyramidData_GetMaterial_mB146DD3D5AF9DF8120DD7DE0C8E5A829AE9924ED,
	PyramidData_SetMaterial_m2E23B74418C28B076D3C64610862EFE78546E239,
	PyramidData_RemoveCategory_mC22D3E2EA2643EDED436C510B90B69D1578D2118,
	PyramidData_GetValue_m46A9CD17E7C7B8D2C7B968921767ED8169AE85A6,
	PyramidData_CheckAnimationEnded_m3001F0CBB301A5FD7115017100FDA259D993D185,
	PyramidData_StoreAllCategoriesinOrder_mA4178F6CABDFF205BC46730EE25F3D66414801FB,
	PyramidData_FixEaseFunction_m9EEC19D62DC0E35D2679E50D8CE0A718A05EB4FB,
	PyramidData_SetValueInternal_m0C982FFE4D2D235FBD9978F6802F0ADB7B7ED793,
	CategoryData_get_Shift_m9B959B428B94380EBFA72C53506FBC2D58D826D8,
	CategoryData__ctor_m26F0237C886E996A106BBAC3FC2C36709B5D6B0D,
	DataEntry__ctor_m8671E8075B39520C9C2FF4AD280C9462D276D114,
	U3CU3Ec__cctor_m34F51D6F14B1C7CE1935016769811162EEC21D5C,
	U3CU3Ec__ctor_m5C4D72F7B31ABFC2DF210BE5BFF67D3050665D91,
	U3CU3Ec_U3CClearU3Eb__38_0_m79CFA4B1448C9EA6C06E396B8BF948B7B90010F5,
	PyramidMesh_Generate2dMesh_m8AB14E7E5672A2E8B4128749F71EC1FBFC28C5C9,
	PyramidMesh__ctor_mCD236F4200785C7C1F9A7C779618B448943F2333,
	CandleChart_get_TotalHeightLink_m598C73ED752261A7664E43B09571B9D47981A068,
	CandleChart_get_TotalWidthLink_mFF9B1DB7F8BF4AF033BA7D22D105E8AC094AF76D,
	CandleChart_FormatCandleValue_mE3522EEDF8BCDFF301B676EEE0429137435B8129,
	CandleChart_FormatCandleValue_m6C06E5C9132129BE9D252A897EAD30FB2CEE88DC,
	CandleChart_FormatCandleValue_m2DBACF487C6F696D3713364F47A3E0F954B7556C,
	CandleChart_get_ThicknessMode_m64DBC67B723E50CD56C4CC954FA40C7150570D0B,
	CandleChart_set_ThicknessMode_m5F4585C9CB07B87B32704EE5EAC42B45788EFD8B,
	CandleChart_get_DataLink_m00FD68B034DC6139DD86EC1E48ED5C52E6952A27,
	CandleChart_get_ThicknessConstant_m8422009FEBC9441D7317CE265C4DFEDEBB2760EB,
	CandleChart_set_ThicknessConstant_m784DA40E173C4622E0E88D14A86232A84EEC6EB8,
	CandleChart_GetScrollingRange_m7EF73DF0DD8A16C4ABB7C54C766A7A9C0FD3DF87,
	CandleChart_get_ItemFormat_m5EEE8919C00F565E1AE89494CE7AEFB9FDA172FE,
	CandleChart_set_ItemFormat_m9213F4E419D7635EC8070C085B158975DA896A55,
	CandleChart_get_BodyFormat_mC1A7B62E8F6F9CC380AC811AB61889114E78A7F1,
	CandleChart_set_BodyFormat_m6D32FA73067F33E5AC11517614ACD42EF97910D7,
	CandleChart_get_HighFormat_mE8A27E8B01B6892739CD541DDA674701FCB0350D,
	CandleChart_set_HighFormat_mDADEAADB405AA07B8F600333DE9CF2C1936D053A,
	CandleChart_get_LowFormat_mFE54FB0627E93939329F94CDA029317DCB9EAF4A,
	CandleChart_set_LowFormat_m788CD1D7F584FDED174630885C8E2D2AE9248164,
	CandleChart_get_DataSource_m0AB74C58AAE26714100C01C924601901A0213DBF,
	CandleChart_Start_mF46EFFA901A7FFF36710F50B3EB64A22B0321C9A,
	CandleChart_OnValidate_m56672C9FAB940A4EC9F9FDFA637BDAB1EAF70999,
	CandleChart_HookEvents_m6B6E4DAC69EB7562FE57716B8E4EA69540A87675,
	CandleChart_CandleChart_InternalViewPortionChanged_mA6B865845B98429DD9EBC51EE605B070F37953B5,
	CandleChart_CandleChart_InternalRealTimeDataChanged_mDBFF0A2EB51D089078196909FB8DB204CF5C88C8,
	CandleChart_CandleChart_InternalDataChanged_m4D73EFD7B463E1F7EA674DB120C5CCEFE4C45D84,
	CandleChart_OnLabelSettingChanged_m6C8A4598ADE7656BBC7FACD99E98130A395EDF8A,
	CandleChart_OnAxisValuesChanged_m1C48F3A0F2874A6865799CC70187F5FDCD31E86B,
	CandleChart_OnLabelSettingsSet_m65E294BF05AE5344773202688B9BBB66ABF4B544,
	CandleChart_get_LegendInfo_m45FC5272E7730785237BE8F39EDF831F145E8273,
	CandleChart_get_SupportsCategoryLabels_mD7C996FC5FBAD25CA590EE068D926AF9B91AE960,
	CandleChart_get_SupportsGroupLables_m9B4F1F8D2247FB5BB844CCA08628CC346EFEC262,
	CandleChart_get_SupportsItemLabels_m18B2A7E6CE430DEB2EE747B99F47457076C847D4,
	CandleChart_HasValues_mD0BED5956522BF501D630240CA6189990BE7F158,
	CandleChart_MaxValue_m04277ADC4CAC6478F72BD66E189DAE5C4DE268CF,
	CandleChart_MinValue_m98493586504E7EB11C80538EC6A7A62306857DAE,
	CandleChart_Deflate_m9D2CC485CEFDF78276B26D025E3C485A559DB50C,
	CandleChart_NormalizeCandle_m98E3DB6F3FDD364823DD396FC1E99680A408258D,
	CandleChart_InterpolateCandleInRect_m97D7A1373D3BFAC2ADCAE0A5F190CAD777573496,
	CandleChart_FormatItemWithFormat_mDBEEAE1C81C617711B40AC96DA71DE2409FDCB76,
	CandleChart_FormatItem_mD53CE54C259E3E7AAC4E537A88C1B8BB0C39833C,
	CandleChart_FormatLow_m590FDD5765B9798269B34796FB02B2DE37B2FBC5,
	CandleChart_FormatHigh_m523529FB7E9EA95D4694E69B4A02557FE58DFA68,
	CandleChart_FormatBody_m8FBB46ACB7F278A0E9617141589AC13195FCEA0E,
	CandleChart_FormatItem_m0871F8AE5CD0D2CA1C9D09667AAFAEE6D1560A4C,
	CandleChart_FormatItemWithFormat_m35E4421F3AFFAA329F1D72F387E2F80D2313E200,
	CandleChart_TransformCandles_m9F7FDAAB055207680D1C826D8EEB4AF72AD795C0,
	CandleChart_ClipCandles_m54D130F6F6C943CFA23609EBBD5809D045FD3F8A,
	CandleChart_OnNonHoverted_m84F7762745E03018F35FB17F3898A63A33187993,
	CandleChart_OnItemSelected_m8A07A7EE9B634A3C17E4D4A1ED744B11C11C5B91,
	CandleChart_OnItemHoverted_m47467BDC246B715AEE6DBEEBC52B00948B37AC92,
	CandleChart__ctor_m21A09CD5DCCEE77F1897C70AAECBAF126690FC7D,
	CandleEvent__ctor_mB64B461DE26138E300986B96C4F26C566F2AF6AC,
	CandleEventArgs__ctor_m73F8D41CEC6FD422A0EAF91D77FE465B3076B4DC,
	CandleEventArgs_get_IsHighEvent_mAF15C317E3B14074D6B7B90AE10B3A2FF84A39AC,
	CandleEventArgs_get_IsLowEvent_mD5323C3CD4F2E624BD93106C7AE408D52374B57D,
	CandleEventArgs_get_IsBodyEvent_m9D2112D0BDC23B7598683134F86982552D44FA1E,
	CandleEventArgs_get_SelectionRect_m1BA91E7AC81BEF22E923E40B1B6F3D1A980EC63D,
	CandleEventArgs_set_SelectionRect_m335496950C896DF96106DA2CC0C7A5E2D9DFF041,
	CandleEventArgs_get_Position_m4E369BBC39D884BFC6196F170705CE2F787B99A9,
	CandleEventArgs_set_Position_m2F6E62EE3976061B158387AA04B8DBA38BB6F5F9,
	CandleEventArgs_get_Index_m1124A9CE28D536D86334A6988DEDEA4CB97D7FC2,
	CandleEventArgs_set_Index_mA835DEEF93ABB5FC3E5D025662A85A18AD07570E,
	CandleEventArgs_get_CandleValue_m8D6CA1E8F4F22BC926F537B47ED160F769992D43,
	CandleEventArgs_set_CandleValue_mD145E35AA00E49F70E63237F7F72853F916879D8,
	CandleEventArgs_get_Category_m9ED5F70A96903ED3BAACD6717412E2D09596F354,
	CandleEventArgs_set_Category_m3D313A3E5EE3DA1262B5DEF2C06AD4711E3FDD8A,
	CandleChartData_ChartAndGraph_IInternalCandleData_add_InternalViewPortionChanged_m5DB4B47D9A257B0CCE65BDB91C1CC7F09358ADB5,
	CandleChartData_ChartAndGraph_IInternalCandleData_remove_InternalViewPortionChanged_mA81AB3679966958D93B0118B4CAC05524B2A6325,
	CandleChartData_ChartAndGraph_IInternalCandleData_add_InternalDataChanged_mCA6E008E7CD599161C420ABABD4ED06EBF942999,
	CandleChartData_ChartAndGraph_IInternalCandleData_remove_InternalDataChanged_m50F18441D2EB04792AD44026B5C3789C7CDF256B,
	CandleChartData_RenameCategory_m5443EC6E776EDB31FF5AA5F67716A0CEB13E4315,
	CandleChartData_AddCategory_m629D248CE33D6E503C288D75CDF9A8EC8462EC5A,
	CandleChartData_AddCategory3DCandle_mC501FE0232B3FC6644CAEC6637F09D46FF88F4D4,
	CandleChartData_RemoveCategory_m26729DC323878BC3CD4A3E51B2CBB49097898DAE,
	CandleChartData_SetDownCandle_m0A025783536DEDA9863E0EF29E07364E62A3E0F4,
	CandleChartData_SetUpCandle_mED3A65D321C1D08430FB651187E27B0B5184684F,
	CandleChartData_Set3DCategoryDepth_m74ECA8C993D752FBB2325C220FAC2ABE930177E7,
	CandleChartData_ClearCategory_mA03C146855A083D94D1412F83509AEACD812E9AA,
	CandleChartData_GetCandleCount_m3CF382A51CF4F867DEFCC616D365FB3F4D99D529,
	CandleChartData_GetCandle_m28041F229BF56D7396C03A67FF0A08CDCAA62B3E,
	CandleChartData_GetTotalCandlesInCategory_mD98B4B7B729933DB02AD0413ED361A2916103E64,
	CandleChartData_ModifyCandleInCategory_mC3E7DB4F888358234062F5DE5D749362FECDC921,
	CandleChartData_RemoveCandleInCategory_m25CAA2D0D188F72B6EDF3E1E16A98824055E84C8,
	CandleChartData_ModifyLastCandleInCategory_m6B8D03A191D146AAB393F5CA90B9E085F435FD95,
	CandleChartData_AddCandleToCategory_m2166CDA03AE3D0B9374BDF98BAA69287A1A0482A,
	CandleChartData_ChartAndGraph_IInternalCandleData_GetMaxValue_mA504EA4923D0565CAB406C13E98D0C3C5183CAC1,
	CandleChartData_ChartAndGraph_IInternalCandleData_GetMinValue_mF2DE9BCBDFFA4A64CBC2115809FE0FED9C991BFF,
	CandleChartData_OnAfterDeserialize_m36F6F66026DEDDF4CAD578E93CF52918C5277444,
	CandleChartData_OnBeforeSerialize_m75717B8DCE716940C158D0EEC557FAFCA4B4EB5D,
	CandleChartData_GetDefaultCategory_mB174FD9359B008B2BD6850D87E3168388BA20963,
	CandleChartData_InnerClearCategory_mA708CA2B56A7FF418638F8ADFD76713B84299E49,
	CandleChartData_AppendDatum_mF0EA4FF07FF954AC70AB303D826F3167E17773FB,
	CandleChartData_AppendDatum_m31AB4C52D2F277CFB135418586F6EE80D153FB43,
	CandleChartData_AddCategory_mEDDDDCCEF8EFB7FB353BC2A7D6E29B62257DB4C8,
	CandleChartData_ChartAndGraph_IInternalCandleData_get_TotalCategories_mDA7BCEA83CE1D6D92C7305E9EFC4004E08C66CA2,
	CandleChartData_ChartAndGraph_IInternalCandleData_add_InternalRealTimeDataChanged_m3D06FE180EF280777EF0B53FF1E397EF522E36D7,
	CandleChartData_ChartAndGraph_IInternalCandleData_remove_InternalRealTimeDataChanged_m73F41E021F43D95829EDC25F5AC65E527E58B374,
	CandleChartData_ChartAndGraph_IInternalCandleData_get_Categories_m0E407C38BDA68F4A5A8B24156E628DE688C3B4B9,
	CandleChartData__ctor_m601349095ECB37C0F45015E1492758972B22EAEC,
	CandleValue__ctor_m3DF3507ACFD389F979CEA1D114D7455CE7B62BB5,
	CandleValue__ctor_m113D5BA7F228439CB190BA3CE617FB07BDDAFBBF,
	CandleValue_get_isUp_m327DC66B9BE3EFF66FF5299A41E200883479E80F,
	CandleValue_get_End_m2431E9875D452C1194BBC5FC3FB8668A4F9DAE52,
	CandleValue_get_MidPoint_m1C6D8595E8D75F3D474E520A562D457CCBD8232A,
	CandleValue_get_Max_m35263E3FD9F3780330E691CC51D241D442D8A228,
	CandleValue_get_LowBound_m916E8CBB1CCE4211DAFE86888125AB79A57F329B,
	CandleValue_get_HighBound_m954D4BEC75899641B1CCB5E0792E9460F80BDCF0,
	CandleValue_get_Min_m93794657F2D8ED00CB5B56EB649E5278FECEB04E,
	CandleSettings__ctor_m8E130B523423C823992282CC919C895EC5C040A6,
	CategoryData__ctor_m78FAEBAC0BBE1D80B2AEE1890A3772919F9B31A4,
	CandleComparer_Compare_m2F2F55306F4CB0785F629EADEA3E1B9A22F01239,
	CandleComparer__ctor_m12219CAD5FBA888BEA34293CC31011F0147AB863,
	SerializedCategory__ctor_m7F8A603D480A2BDF2F8E9EC128E4408B185D9D4E,
	Slider__ctor_m217D98E2A53A0B59E30B5B8D982EF7EA048BDF7B,
	Slider_get_Max_m8DF72945E0A7CB5EE23390EA8C90E532339BCAE1,
	Slider_get_Min_m6071CE4E65E9B06F77BB91653EBB6394A11857AB,
	Slider_get_Category_m5B8D0FF74913A8ACF882846D87C34C470C181629,
	Slider_get_MinIndex_mA5AD667371625A7F0DD2294FB03B357AEB949F59,
	Slider_Update_m217D54C3EBA3F7CE36F1639188249A0AC2CE22D0,
	U3CU3Ec__cctor_m1CD19550A4D019B320164A7C4754975EEEB77042,
	U3CU3Ec__ctor_m8A08FE628EDFF1251C7E01D6F373C3FE19867BF5,
	U3CU3Ec_U3COnBeforeSerializeU3Eb__32_0_mF5ECFB65AA3EC08FBE24CF23FE86C665C5408823,
	U3CU3Ec_U3CChartAndGraph_IInternalCandleData_get_CategoriesU3Eb__44_0_m0D7A86F25C103052E90B895D8C85767E1DE9026E,
	CanvasCandle_add_Hover_m7A03F0024F862BDB44D4B95310A38BA14E6EC5FB,
	CanvasCandle_remove_Hover_m08E6A223A93FA472CB1946DC0298731133BF76EE,
	CanvasCandle_add_Click_m2955A032522633F04F7896963B5DE7E69FA672AA,
	CanvasCandle_remove_Click_mB33B571C99D746F92B3E0A20ACE09828A18DC08E,
	CanvasCandle_add_Leave_m473992F13C76B971C580B3B73D112C80F7F5136E,
	CanvasCandle_remove_Leave_mC622586CBA098291105A33A16BC79ABAAF382AB8,
	CanvasCandle_CreateCandleGraphic_m6D818B29A884A24CDFB8F6C593D9307D6C7567B2,
	CanvasCandle_HookEventsForGraphic_m0822B3A2DA554DCD73752B4169D71115410D646D,
	CanvasCandle_Clear_mE83474357195AAEB0069FB26FD62CA14E5897DD2,
	CanvasCandle_SetRefrenceIndex_m3120303A07A05C2E680CF09F63EA20AD6651ABFD,
	CanvasCandle_Generate_m1792E56658188CDE3034B312BACF1661F455A5EE,
	CanvasCandle_Candle_Leave_m090252962C63213BDB7A49A0A36C9AE2BDD74B67,
	CanvasCandle_Candle_Click_mBE6CF0DD0F1961C2A356136EC82A591A363AB01E,
	CanvasCandle_Candle_Hover_m434A89E5A9F060530AC7E3CFC42F482A8C242532,
	CanvasCandle__ctor_mB9B73C3113FFBBF309717F2F19ED1810A02D6B72,
	U3CU3Ec__DisplayClass15_0__ctor_mDCB6691267E39043D17389473F1F72CCE526ABAE,
	U3CU3Ec__DisplayClass15_0_U3CHookEventsForGraphicU3Eb__0_m832A25A3AE5FCA5B32980EEF62E9B115510AA830,
	U3CU3Ec__DisplayClass15_0_U3CHookEventsForGraphicU3Eb__1_m6E04460241ED47A7D8DE5990A445E6709A9BDA0A,
	U3CU3Ec__DisplayClass15_0_U3CHookEventsForGraphicU3Eb__2_mA5EA8B81455DB4F41C2CFE7CD038F3B7B49682D2,
	CanvasCandleChart_get_FitToContainer_m78DDCF1FB5E1EF0FBF05A1BEAAFAA93298BB29E2,
	CanvasCandleChart_set_FitToContainer_m17DD522CD83A59B6598A1BCBBC4CF9CE59CEB322,
	CanvasCandleChart_get_FitMargin_mF480F838EF31E64558B85CED4723AADCF3915ED1,
	CanvasCandleChart_set_FitMargin_m609531CD2DAF76857898466B335D9C6FBDDEDC1D,
	CanvasCandleChart_get_TotalDepthLink_mE962D7B7181C9AB8D48862DD307C53E14AE956BD,
	CanvasCandleChart_get_SupportRealtimeGeneration_m530BC202BCD75EC44B98791BE041FC23C0B76309,
	CanvasCandleChart_get_IsCanvas_mF648FC25D163B01F17E1E032856E88E6BB13D91E,
	CanvasCandleChart_get_ShouldFitCanvas_m4482D620264E0E7BB7C6EA07507D3F9412FCB18C,
	CanvasCandleChart_get_FitAspectCanvas_m6370764F37B6D49595C5F800305A8A9F5ACE5832,
	CanvasCandleChart_CreateDataObject_m07AED514070FAE243CC5992A902FE7A9E34563A9,
	CanvasCandleChart_GetCategoryDepth_m5C56B05CF8D13EC72D1763C14E36548567C06C03,
	CanvasCandleChart_GenerateRealtime_m0BE2CC549DE07B1CB50375DDAAAF0B25589882CD,
	CanvasCandleChart_ClearChart_m2B5E7EF3884555E7B59E9CFCE905C4310A49DFC5,
	CanvasCandleChart_GenerateItemLabels_mB0D92746FFE2BE570FE2F5E18BB5550562DB00E2,
	CanvasCandleChart_UpPredicate_m7289FAA428D36710A98E5FFD0DCBDE659A2D939A,
	CanvasCandleChart_DownPredicate_m14791FC0AD45966BA9A37FC0F0955034F48899A4,
	CanvasCandleChart_FillCurrentSeries_m0ECB5DF8D3B44F9466DCCA1A35CD3F4A9DBF9099,
	CanvasCandleChart_MapIndex_m984E5DDC6F8743096C4900B7196D126680E4062A,
	CanvasCandleChart_InternalGenerateChart_m394DAE54B3999BC64025023DA4ED38DB5173A2AF,
	CanvasCandleChart_Category_Hover_mBFFECBC534167A85491D006875DE75C7148B7788,
	CanvasCandleChart_Category_Click_mBE04F0962F2635ED907625CC48596603CA1FCADE,
	CanvasCandleChart_Category_Leave_m3754EC24A17E89B2782CBF9A19086C7B856696F4,
	CanvasCandleChart_OnItemHoverted_m0CB2E269DE6DE226FFA147C580F84EE5756ADEAB,
	CanvasCandleChart_OnItemSelected_m12CFCA7EBC252168F7378CAD44049CE846685684,
	CanvasCandleChart_OnItemLeave_m4C076DDE2B8372616492E271C3FEB1777C224D08,
	CanvasCandleChart_SetAsMixedSeries_mD6BE6EDC0A22724667CB3A55DB67EAD0A26B63D4,
	CanvasCandleChart__ctor_m16AD589EAD93B119A7DDBD262FADDE1631ECA975,
	CanvasCandleChart_U3COnItemLeaveU3Eb__45_0_m811F170BE8838C17E66ADF78BEC2F32838A8F701,
	CategoryObject_add_Hover_m8BF66FBE73A4BB5FBD226C41DBB6F894B3BBF93E,
	CategoryObject_remove_Hover_m162A8BCC790D2A9BEB17971D675E30F13ACBA443,
	CategoryObject_add_Click_m7131C9252F108EACEB73E742A113B1745186FC6A,
	CategoryObject_remove_Click_m704370212C1B5977C2B01D7EE3E5F7E3AA699F83,
	CategoryObject_add_Leave_mCDC72A0E33C950DC0EF06F5175A3728E3FE0782F,
	CategoryObject_remove_Leave_mD290676B1FF98DCE20CB2D9ADAF06D8C6FF87D98,
	CategoryObject__ctor_mF6D9BB15F76685910F603176A72365C68FA2A1D8,
	CategoryObject_HookEvents_mB82499160C614577AABD5ED7CB542C03D506011B,
	CategoryObject_HookEvents_mC89109E38FEB1ABB15B3FAB431F8EEC43A5DA557,
	CategoryObject_mClick_m48C0B3C6274882BA90410BB87847F27C603DFC68,
	CategoryObject_mHover_m3432B525AC9ACA10E5C8661AC1D7B8BA228C3018,
	CategoryObject_mLeave_mCC979BE9CE1116EC70C65D98B33200AFA054E038,
	U3CU3Ec__DisplayClass17_0__ctor_m616BFA074B2619DC77EC6B2B8E75A3F050E63DAC,
	U3CU3Ec__DisplayClass17_0_U3CHookEventsU3Eb__0_m8074ACAB212ACED4651BB11B1B17E2FD2DB65359,
	U3CU3Ec__DisplayClass17_0_U3CHookEventsU3Eb__1_m23A01997F35CBC50B814010C1BB81E768B1966B6,
	U3CU3Ec__DisplayClass17_0_U3CHookEventsU3Eb__2_mC9148D7AE85597D07B668F8382219D56A3BC74D0,
	U3CU3Ec__DisplayClass39_0__ctor_m84340F8AC8AA05E6041229A77195B6ABD0E3C672,
	U3CU3Ec__DisplayClass39_0_U3CInternalGenerateChartU3Eb__0_mE7141EEE08C8D4CBA26515FBA2819510D6BED4AC,
	U3CU3Ec__DisplayClass39_0_U3CInternalGenerateChartU3Eb__1_m56E16B616550F0B1CD7AF5D481F9161AE1341AF6,
	U3CU3Ec__DisplayClass39_0_U3CInternalGenerateChartU3Eb__2_mDD83B145E810D5DF9790BEAF5CCE0FAEB948A61E,
	CanvasCandleGraphic_get_Min_m0E45D80B67B2687555ADF45A880180D3E7FA7538,
	CanvasCandleGraphic_get_Max_mDF8EFB4FDA38AC9BE8B841D6324F5C8A4F3A4A5D,
	CanvasCandleGraphic_RectFromIndex_mEB3CE7AE0B48B1C8930C6A47BEE89FC5EF4C6FDB,
	CanvasCandleGraphic_SetUpHoverObject_mF81B22171A137AD059E00EFBDC7B01253E3BB485,
	CanvasCandleGraphic_PickLine_m35468513B5D434911D62FC2808B17DB89806CC12,
	CanvasCandleGraphic_PickBody_mC7A02A490956B13263295FAAAEAB3303AA0C34AD,
	CanvasCandleGraphic_Pick_mF5323F9EFA7E1B70289E3F0D913F426DF4D1F629,
	CanvasCandleGraphic_get_MouseInThreshold_mE1EBAEBAFD919FD7D9405872D697CF281A9A82C1,
	CanvasCandleGraphic_ClearCandles_m41D3D97972CB078966F997CEBDC12AC063F6B486,
	CanvasCandleGraphic_SetCandle_m8298FD9EE69478D92FEA303BE1956D61B485853C,
	CanvasCandleGraphic_getOutline_m798E8CB9D2C34E3562F6F0AC84703B1D56FA42C5,
	CanvasCandleGraphic_getCandle_m3B6B00CD1C59B6EDD3CE21871A5804774A79B9CC,
	CanvasCandleGraphic_getLine_mA140EF9183453BEC0C4FC21070BD37783E57AA89,
	CanvasCandleGraphic_getVerices_m9154DAAB9013C0F21AC9DB498DCBFB4D0CECD3B4,
	CanvasCandleGraphic_OnPopulateMesh_m868DFA12D36E9AB9E92231AEB30C77DB532D84E0,
	CanvasCandleGraphic__ctor_m29DFFFEC0FBB2292BEEC5B46967BEEB2D7713716,
	U3CgetOutlineU3Ed__19__ctor_mEFDBA2A898B7FBF0830DD5FB9F9B83F4BC10E633,
	U3CgetOutlineU3Ed__19_System_IDisposable_Dispose_m42DB601169ECF3C4049FF5FA8C7C40A82768B6DA,
	U3CgetOutlineU3Ed__19_MoveNext_mBE0A30E999C23E7A6F14B20D816EAA6C1B257F07,
	U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m13CD6F38EC403E61CC2DFA08B5F37CF5E297D386,
	U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_Reset_m8F76A0AA1EFF342B4F641587492819DF94693951,
	U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_get_Current_m8F0F3DDF253B4632CAAD0D24DFF88B1BA6D1E9BF,
	U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m6D8E95C447218F47700C4C2495D423ACE17CB9AA,
	U3CgetOutlineU3Ed__19_System_Collections_IEnumerable_GetEnumerator_mE3E50EE5A6C6DB0C705F3B7DDDD78C530B4DA2C8,
	U3CgetCandleU3Ed__20__ctor_mC82F7A257D54893E320CAA74A4A9F8E3C46F7A6D,
	U3CgetCandleU3Ed__20_System_IDisposable_Dispose_m81A77A4E49DF47FE1ABDD3EB07FF558721202A35,
	U3CgetCandleU3Ed__20_MoveNext_m312AD42BC93E2E9EBF2D2A5305CF1D8619A52C4B,
	U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m4BD2F0D780FDE9F01E9498D7976DA81876228CEE,
	U3CgetCandleU3Ed__20_System_Collections_IEnumerator_Reset_m6F0292D8486F07BDD361E3A7872B63C1961C1EC0,
	U3CgetCandleU3Ed__20_System_Collections_IEnumerator_get_Current_m9111EB1D61ACFBDCC6E9593D576936C79A24EF70,
	U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mB9B8A2839CD947E1084EEE1508B62501C9C9A98B,
	U3CgetCandleU3Ed__20_System_Collections_IEnumerable_GetEnumerator_m9645F135E86563D363D9F1A8D234A86E607AC9F6,
	U3CgetLineU3Ed__21__ctor_m0326018FB282A0BA3FEC754F4D9D47FA72F54172,
	U3CgetLineU3Ed__21_System_IDisposable_Dispose_mA0D475FB96EB7B1DB8A497F5E7A45D331428E58D,
	U3CgetLineU3Ed__21_MoveNext_m6AEFBA63054F72D5335D56F54D14B32E4C2C46FA,
	U3CgetLineU3Ed__21_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m5603D072D3ECD9E42D2013A333694F7E1C09C69A,
	U3CgetLineU3Ed__21_System_Collections_IEnumerator_Reset_m01DC42B477E9802F36C385E8C991F6492566F885,
	U3CgetLineU3Ed__21_System_Collections_IEnumerator_get_Current_m8FE1DCA1D916AC6C8846759C6A879874BDDAD7FF,
	U3CgetLineU3Ed__21_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m20622E0E1FDDEE6266F33E1492744F6A8F0045D6,
	U3CgetLineU3Ed__21_System_Collections_IEnumerable_GetEnumerator_m2FDF32F5C4E66B3920C4557CBFF30D99422DDC2E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InfoBox_CandleClicked_mE8D20C2BC3D3178C1E4EA9CAD068CFBB5AFCC702,
	InfoBox_CandleHovered_mC7D3F1E37FA3754A0A78735A7624F3879B4F4FDD,
	InfoBox_BarHovered_mE6A5ED45536860E0E0DB16DCB86227AC1A431F4E,
	InfoBox_RadarHovered_mC148FB5AB7CC5ADEFFF092340FB8F8ED9DAB44FE,
	InfoBox_GraphClicked_m1BE72C5B5112845DF6401C3EFC8E28B265F4FA34,
	InfoBox_GraphHoverd_m9B389831DC1C8B78CD8A4666A8896756FA9B8E98,
	InfoBox_GraphLineClicked_m866599CD5DBA81200F7B90FA2DE31FD001165F82,
	InfoBox_GraphLineHoverd_m19CD91E5268C00C942D3ED2133D62A0FF41B9ED5,
	InfoBox_PieHovered_m2FC55EBEA7AC0BB5E0DD65DF5F00541B75677299,
	InfoBox_PyramidHovered_m6EFBF7D693B4EBCBD7B2A1063C89942D60DBFAB1,
	InfoBox_NonHovered_mE2D527B087DCEF51B081791E08A9B80DDF5F052B,
	InfoBox_HookCandle_mE06AA12FB3E7A4A5E257FD17F1834690AB6A0A4E,
	InfoBox_HookChartEvents_mCC0FD2FA54C524C43D1D390AB57130124F0CFA86,
	InfoBox_Start_m77B9115D7D86257A71FF821A9FCB31A749AB2FB7,
	InfoBox_Update_mAA899B46178D3B292521450A75A12CA6EA164735,
	InfoBox__ctor_m941225ED15849B75B254D10679D6E0839816E9B9,
	AbstractChartData_RemoveSliderForGroup_m9B3632ED8D7526CDD6B5FAD862E1E5B5D65ED893,
	AbstractChartData_RemoveSliderForCategory_m47DC08F8284F49E10FF03920B02048D863519CE1,
	AbstractChartData_RemoveSlider_mFE64540D9D48CB7B2E3DA9EA1BC33B68DF323FBA,
	AbstractChartData_DoSlider_m00E9CB3D46262CB5D6C2CAF8AEC4C5456BE2B7A4,
	AbstractChartData_UpdateSliders_m67DE90ADDAB7D774EB70BC4A625BBF9740C2623C,
	NULL,
	AbstractChartData__ctor_m42AF22840368227BA98CCF4662439FDE6BCE359F,
	Slider_UpdateSlider_m1B7CB10E51D52BB02664132062F8A23373212D57,
	Slider__ctor_m56266ECDAFB9ED2C7351F4C99171A47A5264B54B,
	U3CU3Ec__DisplayClass2_0__ctor_mCBF7607CD96A908F01BC06E42777AC4AFE4EC3FB,
	U3CU3Ec__DisplayClass2_0_U3CRemoveSliderForGroupU3Eb__0_m70E074814A66DA329743708B457727200427C7D4,
	U3CU3Ec__DisplayClass3_0__ctor_m077EC3EAAE51AF366935D410840FF2085FA5A1D8,
	U3CU3Ec__DisplayClass3_0_U3CRemoveSliderForCategoryU3Eb__0_m4CACAD75D4CE289EFECBF0E16FB7D430AD003E32,
	U3CU3Ec__DisplayClass4_0__ctor_mB9092D5830724BD4170E9D4CE09E90152B76BB2C,
	U3CU3Ec__DisplayClass4_0_U3CRemoveSliderU3Eb__0_m66A68EF9BAB3669BEA8248B0CC5464A5938274E4,
	AnyChart_get_CustomNumberFormat_m0AC11EBE27C7500614A0A3150704E9A9F0688873,
	AnyChart_set_CustomNumberFormat_m0455D07B63986F28B53115F59CD8A4DB63B17581,
	AnyChart_get_CustomDateTimeFormat_mCB5464E6E7B3E977A4E9CE2FA2D55CD3B6F38B8A,
	AnyChart_set_CustomDateTimeFormat_m6A844AE03CACBB8A73E5B3026F3077831B931AE9,
	AnyChart_get_VectorValueToStringMap_mD76E6D652919102185A64AE23BC7162D01E75917,
	AnyChart_get_VerticalValueToStringMap_m1F17A15237DF71E6CEDD825B23E67364C8A6987C,
	AnyChart_get_HorizontalValueToStringMap_mBD0AFB84189A2B5CF469932E2B843A53FE234EEB,
	AnyChart_get_TextCameraLink_mAC8EF9B586EA2DB1103021BF2F0D5EF4870322F6,
	AnyChart_get_TextIdleDistanceLink_m57876C1284E844484D59D03F21061DAB55EF5A40,
	AnyChart_get_KeepOrthoSize_m2AB0597DA18D0255F4945F54ECC4BA79C70914C2,
	AnyChart_set_KeepOrthoSize_m6917A8606F8B3F199C6FCB4C643F147E6C0DCECD,
	AnyChart_get_PaperEffectText_mFDD282B68FB983674D303373159F4BC26E1DEC8C,
	AnyChart_set_PaperEffectText_mF51D3B0744AA5F9673DFBCDCF62FEF16373A1E70,
	AnyChart_get_VRSpaceText_m57E95CC4EA285C81630DA1C838FC429F36D1B90E,
	AnyChart_set_VRSpaceText_m4BFA3908FD701881BE4C80FF5C43F4C77A4D75FB,
	AnyChart_get_VRSpaceScale_m9F1CABDDAB2CFBD9BFEA6D83962B2A59CA3C22DD,
	AnyChart_set_VRSpaceScale_mEB0E44EE23213D0D30B9BA46E66BF98F6E36AE08,
	AnyChart_get_MaintainLabelSize_m6362EA673AA4322BA6729FFFF7CB28FDEF69B121,
	AnyChart_set_MaintainLabelSize_m49D1A27E0F05967938FCC168780D38363827F315,
	AnyChart_get_IsUnderCanvas_m195603F81F69BB55C721E932AE8A27457CE3C89C,
	AnyChart_set_IsUnderCanvas_mC1095C52AD717D93A0FF99BB0E5734571ED51BBF,
	AnyChart_get_CanvasChanged_mC57ECBC1E94FE78A2BCB30A80D0A44302F8DBC6F,
	AnyChart_set_CanvasChanged_m12590C6FF3B6795685A5E876D7E9F4CB76D2A834,
	AnyChart_AxisChanged_mD7682E8E62D0FD8714021AE6A1396B959D004620,
	AnyChart_OnPropertyUpdated_mDF408648BFC3185F68AE7C898472E30ABDCC5391,
	AnyChart_Labels_OnDataChanged_m925FE6BB47837806BF43C0E2FD95DCD56480DE82,
	AnyChart_Labels_OnDataUpdate_m47C3062CA67EC61F5649BC380511C854539EB3C2,
	AnyChart_OnLabelSettingChanged_mC24544E7358D05B2B1B57F0CA76A8EA2CE405DD6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AnyChart_get_TotalWidth_m1011DAD4115030989BE370E758945608A6BE0D0F,
	AnyChart_get_TotalHeight_m54CC9006181ABAD472545C03E0235C9095239C88,
	AnyChart_get_TotalDepth_m28634ED1897558FA8E91F7A1A482D5F3CD04EF1F,
	AnyChart_GetScrollOffset_m7768BD1DC02B8D0E3693A383A924E867CE0C3E03,
	AnyChart_ChartAndGraph_IInternalUse_get_HideHierarchy_mA9D7D1F262662D403AF1EAA00401C43D90CCCEAA,
	AnyChart_FixAxisLabels_m48ADDB3B77D0C24CAF64E4A99EED74AAD8E8696F,
	AnyChart_OnAxisValuesChanged_m56C708D5108CF4F3D36FF66AB51CD28713F5BF2C,
	AnyChart_OnLabelSettingsSet_m242886560287205358EE8FD329BB39B5B66E6414,
	AnyChart_Start_mD4EF7C603D09C89FD31EF2AD29E096FD9D675E70,
	AnyChart_DoCanvas_mE59B477599C638B6D17FF5F07C7DEC8275B9AC5F,
	AnyChart_CreateTextController_m6A49D0C8D36C45F59969CAAA51C2B9362C9299E2,
	AnyChart_EnsureTextController_m4747DF9972978764C351848CB85DCEBDE6F7DDE3,
	AnyChart_get_Invalidating_m96DDF99F1701CCB0C4E00CD49651255CA4CED036,
	AnyChart_InvalidateRealtime_mC2749BD92D799EC92E5106216135E6CB1C35E986,
	AnyChart_Invalidate_m44A5F7B4CCA72ACAE8766BB82F5585A3D63D442F,
	AnyChart_Awake_m18BB97712E88FCD794C5F16B5618B39F0AFCD684,
	AnyChart_Update_m3E8127AC2DDDB4E1FFA780D2072C954BA47B04D6,
	AnyChart_LateUpdate_m193D92CC347E7C824E519877B4AD0548B41A6D19,
	AnyChart_OnValidate_m79FB607D0777AFF6351217860D9B54A57DA66092,
	AnyChart_get_TextController_m185F32490B09330CDA470FFBB491181C4EE32E45,
	AnyChart_set_TextController_m94231AA307FA3133BCC00C147C915DA65C77920E,
	NULL,
	NULL,
	NULL,
	NULL,
	AnyChart_OnEnable_mB12E9C209ED68AD071637A1B0B4CE7A2DE7D8F73,
	AnyChart_OnDisable_m198E0682D61D297383C7B0DAC1F8FB840CCDF87C,
	NULL,
	AnyChart_InvokeOnRedraw_m00D6BD4CA67D1EE798C62D3C78B094FB7DF057AD,
	AnyChart_GenerateRealtime_mD1ED7FD86D4F2ACFF67C27C4D6AACD7376A309F6,
	AnyChart_GenerateChart_m0C5EBF23B4B540E874FC63FDB564ED34DA6B58D0,
	AnyChart_InternalGenerateChart_m5C4AF6E633752CD5AB719A6998195EDA6EB79DC9,
	AnyChart_ClearChart_mEB0B744E8A300C7AB74DC2B4A27C1AFDF6095D24,
	AnyChart_get_FixPosition_m5E0F5F20D3E2E4CAEAE63A2B324D48737A433651,
	AnyChart_get_MarginLink_m338BB6284696A6A2F88321A1DE1B39CAC7F2570E,
	AnyChart_get_CanvasFitOffset_mFDEC43D5BE0A310127300B33FC596FC12503FEFF,
	AnyChart_get_ShouldFitCanvas_m30CBD69BA4C21E7A3898BEB96B4C36CE5A1817AC,
	AnyChart_get_FitAspectCanvas_m1959BCEBE2D42B8A7299D950868B522EDF8B3017,
	AnyChart_get_FitAlignCanvas_mC8A20FA4D96C2D1D865DBE78A020B7244DD1439F,
	AnyChart_FitCanvas_mCE34828C0A5EB5665F90F89ACFF005785DB915D1,
	AnyChart_OnNonHoverted_m720A2E129AFF0F75D32BF47A01BA0ECF7244375B,
	AnyChart_OnItemLeave_mC3B79D424DC36CE913A59514ADC770116B4F9A5F,
	AnyChart_OnItemSelected_m0FBFC53D55542BF98A9F2FEC3DAE1D3BF8B741F1,
	AnyChart_OnItemHoverted_m774F61937CDDEDEDCF18456914780D4AC85A041E,
	AnyChart_InternalUpdateAxis_m32CB03B8E04263D182D7F420B4F0B2A49A7F17B4,
	AnyChart_ValidateProperties_m108BB87D138E0F4E88228DF8F41E4B252D2369A8,
	AnyChart_GenerateAxis_m6327549959BD854AA0C7967773F35080FCACB04B,
	AnyChart_add_ChartGenerated_mA7DB9A7B4BB163D50DECF59625C459C4C2095DA6,
	AnyChart_remove_ChartGenerated_m523F495E2E3E114810C67FBD82A5BC5909D7B5A1,
	AnyChart_RaiseChartGenerated_mA17EA2EBA3E461CF5829355042ED56E5B770EBA1,
	AnyChart_ChartAndGraph_IInternalUse_add_Generated_m3BB659223245F2B8E60B89C269E9E400C0432105,
	AnyChart_ChartAndGraph_IInternalUse_remove_Generated_mCEC58738425E8C2E7B739F694A45775900D8C7C6,
	AnyChart_ChartAndGraph_IInternalUse_InternalItemSelected_mA1B3FF66FCC1F3EFE343457007D735493995CB50,
	AnyChart_ChartAndGraph_IInternalUse_InternalItemLeave_m28452552121F4EF7A98121F734994576DBD803F3,
	AnyChart_ChartAndGraph_IInternalUse_InternalItemHovered_mAB58ED8D7A297F310A5EEB1CA35EC65A4982F8B6,
	AnyChart_ChartAndGraph_IInternalUse_CallOnValidate_m031FAB7D24FF2327FAFFF66E7FE313E3A448CCED,
	AnyChart_ChartAndGraph_IInternalUse_get_ItemLabels_mCC71FD6C2017E1212B03E48384D4D956F36F2D11,
	AnyChart_ChartAndGraph_IInternalUse_set_ItemLabels_mF4DD5FBA3FAB697D089B835F2C1BD1C1FE421DB7,
	AnyChart_ChartAndGraph_IInternalUse_get_VerticalAxis_mDA255FDC3CE7495683718A4CC8898149891CA83A,
	AnyChart_ChartAndGraph_IInternalUse_set_VerticalAxis_mEA479C4A594D772CFA6D2F9B8F8B26EF01DB78ED,
	AnyChart_ChartAndGraph_IInternalUse_get_HorizontalAxis_m0E5764FAF86003B6A24BE1B69B050786A4D2E0C9,
	AnyChart_ChartAndGraph_IInternalUse_set_HorizontalAxis_m8291830DFB70837ECE2E77329D24765B95917BCD,
	AnyChart_ChartAndGraph_IInternalUse_get_CategoryLabels_mCE0066BA618B0464B192968FFD2CEB5703681E53,
	AnyChart_ChartAndGraph_IInternalUse_set_CategoryLabels_m5C39A0D69E8A311C20F6401E80C566064640952F,
	AnyChart_ChartAndGraph_IInternalUse_get_GroupLabels_m7346A32568692CC439E30932D3DDD8D43AD46630,
	AnyChart_ChartAndGraph_IInternalUse_set_GroupLabels_m8B826267B38D696095255B9EE22B541E6BD0E9F1,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalTextController_m8E8734F6D08482D1F54295D5F9AF322D5ADD8146,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalLegendInfo_mD583CFFAC25A9F57A0EAC19FEF39519357E866CA,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalTextCamera_mC013A1DE61A533BEBF48F778E952A1264BFA5F4C,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalTextIdleDistance_m1582FBC4CCF0DEB1CF98098DD3EA13364A6D0A51,
	AnyChart_ChartAndGraph_IInternalUse_InternalHasValues_m9294D407887BACD89E175F9D3044C235C64EDDF9,
	AnyChart_ChartAndGraph_IInternalUse_InternalMaxValue_m1FF5C657FA190B3F99863EDBE50B67C391401511,
	AnyChart_ChartAndGraph_IInternalUse_InternalMinValue_m620653C10E56ADE4BE42733125D17937E06D9492,
	AnyChart_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDE8102E45934BCF92E4FAFAE49D866BDC9819A00,
	AnyChart_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m69FDA7001FAC6AC4775E9BA028D450748088B8F8,
	AnyChart_OnBeforeSerializeEvent_mAB4DF8925D6F1305B474D0748C65B91E99B79241,
	AnyChart_OnAfterDeserializeEvent_mF703D981F156BED1A5BFF5D79995D4BDAE707EC0,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalTotalDepth_mFC65A8DB3B2875426A33CB6BAD31ADEAA21679CD,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalTotalWidth_m6CBB7F20BF1B505EF54A4D7B774973B982AE9019,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalTotalHeight_m98CB1F8F1819A26C73E3ACE4B0C8FCB02C5FDA4A,
	NULL,
	NULL,
	NULL,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalSupportsCategoryLables_m19B03CA7EE3E59009BA352C6A6049317E0B15751,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalSupportsGroupLabels_m3A364DEA63E78B5CEDC3954EB77EE262E3399F11,
	AnyChart_ChartAndGraph_IInternalUse_get_InternalSupportsItemLabels_mDFCD01EAAF7220789F549B38497E20A4CD6E40FF,
	AnyChart__ctor_m6096DAA40D067BACB31DB393771F3033461E0E5F,
	AnyChart__cctor_m78EA190B7A711AAA196019DA9C0E9DB6913B0755,
	CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299,
	ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD,
	ChartFillerEditorAttribute__ctor_mF182ABD485E7833635757C57D47433F2841475AA,
	ChartFillerEditorAttribute__ctor_mF6FDCA0F315DB5CB80A0D6C4B49E795AEE6A4927,
	NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6,
	SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D,
	AutoFloat__ctor_mFA106E4A358AB6F23F2C124C4C9BD3811B422C1A,
	AutoFloat_Equals_m33B82696A2015E983C87F70E380C9942593D0BFC,
	AutoFloat_GetHashCode_mBA93BDD7EA6E5F3DAF7F2B880B8DD0CA23539989,
	AxisBase_get_Format_m70272E116015340CCB033EC0C18721ADE80A5380,
	AxisBase_set_Format_m983A7557BCC6978A1334B1556D05F2374446B1B8,
	AxisBase_get_Depth_mB1B72629F50E2C8A3D23E2400B2B5213E439C3A0,
	AxisBase_set_Depth_m1DF88FD9376ED4AE6AEF5767AAB8E8A00439B64A,
	AxisBase_get_MainDivisions_mC0A8D25E60D1D1895C9D22E775C83EB2837B57C6,
	AxisBase_get_SubDivisions_mFB99E1B27A5B244F974603CE5970F1C12DD0614A,
	AxisBase_ClearFormats_mA654017A705ECFA2D3DB210AB6F904BD0DFC15DC,
	AxisBase__ctor_m40EDD3B466B9BA93D96E92A2AE1ABDCC780F3956,
	AxisBase_AddInnerItems_m6CF2EDEEC102EE7573164B4E0657C0B45DB46905,
	AxisBase_ValidateProperties_mEE169138DFBE5E985C86CBAC0C730AF5BC403928,
	AxisBase_GetStartEnd_m73AD166A34801C224867E7DD2664B6C6E6541023,
	AxisBase_SetMeshUv_m6FF6765D4179795A9FDEC8EE5C7C614300BA73B9,
	AxisBase_DrawDivisions_m6EDF6E3D1E65A3B46481C3310F0324F5A90BF4A7,
	AxisBase_GetDirectionVectors_mA2936BD292DB886C3B08E2A2617F22CF3F075794,
	AxisBase_AddSubdivisionToChartMesh_m4284C97D4CB9B2731FEBC683B8F0FC4437A63990,
	AxisBase_GetMainGap_m056CCF93F4678F143603A0C72A75B950E486DFFA,
	AxisBase_AddMainDivisionToChartMesh_m39F198436B2013AEC3229BE8A4994DBA7FF32D59,
	AxisBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m149E8A13E39FE49CD6AE328440495767B8AF2A22,
	AxisBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA4BAE054BD5AB67D47D4FA68D03A4EFF4FBF1463,
	TextData__ctor_m4216DCC59986D9093F0995CFE2D98D79E56A7D79,
	U3CU3Ec__DisplayClass24_0__ctor_mD617E10C9C6AB4F7494C3D0404931DB468EBBAA7,
	U3CU3Ec__DisplayClass24_0_U3CDrawDivisionsU3Eb__0_m3D2EECC8B0445830AFE47CC25130FAD2125B7439,
	ChartDivisionInfo_ValidateProperites_mD93B05166C93F77CD4F42F671996F4E6C1552038,
	ChartDivisionInfo_ValidateTotal_m2BF95482450BFF955B863413F85528FDDA2F43FA,
	ChartDivisionInfo_get_Total_m7BB758209A186C9033514BCB9EFE63C757FBDC9E,
	ChartDivisionInfo_set_Total_mD516998AD754475DC701DFA759A9009E70ACD3B7,
	ChartDivisionInfo_get_Material_mE2A755C7B0BEFBEBBDBDC5BF92888FB70E7AFCB0,
	ChartDivisionInfo_set_Material_m745D8D197ECA328158F7725C7802D2CFC9B50EE9,
	ChartDivisionInfo_get_MaterialTiling_mF9A02376A3194C63E193203A524C4C3CC8676114,
	ChartDivisionInfo_set_MaterialTiling_mAB9B54764EF3298C9F41FBCCD011517C8A3D6FCC,
	ChartDivisionInfo_get_MarkBackLength_m4BF971DBD25DF2FB74E2F6639F2752F52E6B4A2C,
	ChartDivisionInfo_set_MarkBackLength_m5E30B9905A6E1F0148E4C4BD4382BE6B93ADA597,
	ChartDivisionInfo_get_MarkLength_m3F4309A4FFAE198795690F0B2D7DD25197C978FE,
	ChartDivisionInfo_set_MarkLength_mB2F426BF392CCCC44F220DC9838F1B5E6BEE3E8C,
	ChartDivisionInfo_get_MarkDepth_m6F11558F65AB4EDE28C2B258BF417DBC194234A4,
	ChartDivisionInfo_set_MarkDepth_m372D941BC4CC9043FF5E5A90077ECD57F28E9F76,
	ChartDivisionInfo_get_MarkThickness_mA04DED009CD08FB63BFE7D8C2571C3DC6E5245AC,
	ChartDivisionInfo_set_MarkThickness_m8E497798E1EC08ED8A1557D618B3A03B113F13A9,
	ChartDivisionInfo_get_TextPrefab_m7CB115E4D4CB080698E23D1044FD87D6E8EAEF27,
	ChartDivisionInfo_set_TextPrefab_mE1CEDDEB6AA0A1E3968AEB8D043B1A743E990F6D,
	ChartDivisionInfo_get_TextPrefix_m3B0CA6F1330AF91EDBB2DD006313056866B53CE5,
	ChartDivisionInfo_set_TextPrefix_m56FA18F5893B1BEBD53BF0DD75257D2B2955DD80,
	ChartDivisionInfo_get_TextSuffix_m3CADA61268AD07AA687518E8FB7EC7DECED6B15F,
	ChartDivisionInfo_set_TextSuffix_m31E1AFB16EC911503BD4B469DDEC089CBBE9FFD0,
	ChartDivisionInfo_get_FractionDigits_m51A270577AB840D4A6CA58C06D03E24BEBD81C2B,
	ChartDivisionInfo_set_FractionDigits_m7599C8B3FA890B8905B86D88ECCB37A639775474,
	ChartDivisionInfo_get_FontSize_mA05ED2255799B7394183E729C861354C7564A1AF,
	ChartDivisionInfo_set_FontSize_m7757B5D592526A9A7DC62E244DE2BADCF0423BCC,
	ChartDivisionInfo_get_FontSharpness_m7EEC411FA1E2D77BAE3B2C8DF5A2BD68C0BFB0D4,
	ChartDivisionInfo_set_FontSharpness_mB02C95D91A79FA48E73E399A1BBC2295AFB6AF33,
	ChartDivisionInfo_get_TextDepth_m39B0EBE47E0F4B910CAEF035366997653E44F248,
	ChartDivisionInfo_set_TextDepth_m57A2FF3D0F3DD645046FB1FF264983C011FF3439,
	ChartDivisionInfo_get_TextSeperation_m68F561A9D3A5A570D063732EDD7EC4EA561637F2,
	ChartDivisionInfo_set_TextSeperation_m7454560344AEA16B5DEE0F01231E5089D00F1E23,
	ChartDivisionInfo_get_Alignment_m0532B71A29B449887AF923B5B3D4B289C8865BD8,
	ChartDivisionInfo_set_Alignment_m3F775CB3DAE6ADB25C464BB9390A5C966D1C158A,
	ChartDivisionInfo_add_OnDataUpdate_mAC71E3C98BDF170D02D0AD5B2696655EC74FFD36,
	ChartDivisionInfo_remove_OnDataUpdate_mB943D54C9978DFF4D3502C4E7977E206BCCE64BA,
	ChartDivisionInfo_add_OnDataChanged_m3D8BD87F819D255B2B010F2123A4EEC7F76A1A5B,
	ChartDivisionInfo_remove_OnDataChanged_m2287FF3D6D991ACCB9A6D84E73C352A9ADFD193D,
	ChartDivisionInfo_RaiseOnChanged_m96598C18206F693726BC7ACACE735EFFEC79BB10,
	ChartDivisionInfo_ChartAndGraph_IInternalSettings_add_InternalOnDataUpdate_m5A7281BD06A242A59A10B899F511579044917816,
	ChartDivisionInfo_ChartAndGraph_IInternalSettings_remove_InternalOnDataUpdate_m8D5C8082E73714CCB704EB87EBD39A6BF4DC41D9,
	ChartDivisionInfo_ChartAndGraph_IInternalSettings_add_InternalOnDataChanged_m276BBB7A9EB0CBA81CB692EA63BE25BE266A6FEC,
	ChartDivisionInfo_ChartAndGraph_IInternalSettings_remove_InternalOnDataChanged_m798E0365201A06C962332288F93F3C1B53FCB5F4,
	ChartDivisionInfo__ctor_mAE48277429EE22F3095A9BD356BD01BDFAD23890,
	ChartMainDivisionInfo_get_Messure_m3A745B8AFF8342228A85ED87E7461F3C29DF350B,
	ChartMainDivisionInfo_set_Messure_mD5FE8E88B1E01656A50D23F11EB72444B27BB620,
	ChartMainDivisionInfo_get_UnitsPerDivision_mBAE6D6BE781F0775246686E15C013D456EE86BC1,
	ChartMainDivisionInfo_set_UnitsPerDivision_mD65F104B210C644C441248572FCDEDC71863774F,
	ChartMainDivisionInfo__ctor_m36A0C51E13312389352CE4BE3C20BC4300145271,
	ChartSubDivisionInfo_ValidateTotal_mAB4A7442E0EF979F30D93919FDDD4B59C26E197E,
	ChartSubDivisionInfo__ctor_m7E25D859786C5DD5A818E5396B3CBC9F4F660F5C,
	HorizontalAxis_get_Assign_mB5D4219D8BF79A619838F02DA44746F619CFDC3C,
	HorizontalAxis__ctor_mDC1DCA585B8D54CCD7439E598E5C92AF9F3DC084,
	HorizontalAxis_U3Cget_AssignU3Eb__1_0_m6189D24F8E7B7616E51A82A16920AA7CAE45E1DA,
	VerticalAxis_get_Assign_m30C196DEBA9CE834E7CB196D40E5F28FB9AA433C,
	VerticalAxis__ctor_mB2B5A009C7D1120860B60A7E31092B9A088522A7,
	VerticalAxis_U3Cget_AssignU3Eb__1_0_m89177C3B9F17E4E1551B566302417DC94F34F3A4,
	AxisChart__ctor_mE8EC81B20982714816F0E5E988DC118345638B58,
	BarChart_SetLabelOverride_m294414DAAF55862CD5FD223B152C52794851A0E5,
	BarChart_RemoveLabelOverride_m259E01273E11FB832B180E55AE4C227085BA92CC,
	BarChart_ClearLabelOverride_m8CCE08144E8B5E35E2A87941A1E768ACB864596E,
	BarChart_get_DataLink_m085A1B141778BB69E13F3491697192CA80443A62,
	BarChart_OnBeforeSerializeEvent_m30FC1578E3BFA2106AD9268BA3865B8AE3AA1D71,
	BarChart_OnAfterDeserializeEvent_m2B8F8E903EC88922C1663FCFA0E3B2031C976D55,
	BarChart_get_ViewType_m7D2587E3CB9CE5782987C0952A31ECA5C5707D1C,
	BarChart_set_ViewType_m037C90F5FBB00732E708980C3E56F34A47913661,
	BarChart_get_NegativeBars_m077E71C8B9BEAE206C77437E7F89D2906595E024,
	BarChart_set_NegativeBars_m032335B40D045C2EC8EA01D6AB9D79BC1D6EAD43,
	BarChart_get_Stacked_mC462CD191145BE607CA526E9CA6AD341D0491140,
	BarChart_set_Stacked_m2195F482DB877599A93EE2573B0AE71D12CE52C3,
	BarChart_get_ShouldFitCanvas_m60998B83C088913AD58633C2DAB06A4E8AAC2217,
	BarChart_get_DataSource_mE818D5CEE1B81C3346661C3A60F34488763D8FEC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BarChart_get_TransitionTimeBetaFeature_m2CB8DA93CA9B45D9773563ACDC95E157E626753E,
	BarChart_set_TransitionTimeBetaFeature_mAC0FA9B32D4859D9734E2437E199E7F3104E9156,
	BarChart_get_HeightRatio_mD7B169039BD4012852CE55529BE9E64BE80264CB,
	BarChart_set_HeightRatio_m33D77C10C4D9FBE26B40D6648B5FC439AE88C452,
	BarChart_get_TotalDepthLink_mD78B79475A9CF469C5593EA02BD80E97EE900047,
	BarChart_get_TotalHeightLink_m8C9CBE17D577AC9220EAB770F88E96E63D356BE8,
	BarChart_get_TotalWidthLink_m21ACDD8A8B139A42A0DB88FC7F4926DB0BE3EDEC,
	BarChart_Start_mE2CCD4A20FFC0F083CC3C7929732EECF0AB621C0,
	BarChart_ValidateProperties_m0AA020A9567E980600725D8FC5C3E37F0E27A243,
	BarChart_Update_m7D226A7789F53B654AF8E23EB4DD36F50D1BB396,
	BarChart_UpdateAnimations_mAF3868196D432B9CD7C5857F50CC1C4EC47CF8DE,
	BarChart_OnValidate_mEDED956F87D23C20A4946AB129E19BE6D68BE847,
	BarChart_HookEvents_mCCF779B67EDDB40EF7F10826A5EEC01CD94C4E01,
	BarChart_InternalDataSource_ItemsReplaced_m8616CBCB2D04EABB1558B1DD3B711A82C2CA4626,
	BarChart_Data_ProperyUpdated_m580AE28485649AEA286A3997C2C14467EA12A380,
	BarChart_MDataSource_DataValueChanged1_mB08F479A4060C9A4ABFC255BD4A8AE5F94AF3D99,
	BarChart_StackedStage1_mD9822EE654B5DCAAC6CA26EE02F3EFCCABC6C480,
	BarChart_StackedStage2_m5CE7EEDE40F191E0445F76047F0CB80F38DE44D5,
	BarChart_RefreshAllBars_mA3B7A7176074CB61D03D520C8DA9CC2122A1B863,
	BarChart_FixBarLabels_mA71DD1699499493BF49C6E6C7DA8F95B4C47406F,
	BarChart_RefreshBar_m776D172F3409083554D0402779723BCB52D0AA44,
	BarChart_MDataSource_DataStructureChanged_m541AFE1E8E23893D4935C00C90F7CFA065964515,
	BarChart_PointToWorldSpace_mA8E979ECA152E363C175DB4F41373EECA5E2F6AE,
	BarChart_GetBarBottomPosition_m669E02A7AE6DE60929F7A23014CACC7E7DE9245F,
	BarChart_GetBarTrackPosition_mC3064B48EAD1ECCBACFC806B3E730818B6925C8C,
	BarChart_AlignLabel_mD3DDA531B049ACA600FE049540C9A3F26E1A046D,
	BarChart_AlignLabel_m46E8B6C1FE7F8516431E877E27CC275463D5C096,
	BarChart_UpdateTextLables_mC3B3CFC7C2EB7BDBE7698B09D4CB0F66FEEF170E,
	BarChart_CreateBar_m29CB7F4E466DCD46EC87DB2B138A1A625FFD0895,
	BarChart_GetTopPosition_m9763D1F94C82DAFC7F207F500DF6EB496BFA4705,
	BarChart_BarObjToEventArgs_mEDE9EF9F74A962F4C27DCF194D852CA9A2E2C85C,
	BarChart_UserDataToEventArgs_m589E2DB7ED2E8A497AEB6647F44869FEA51660D9,
	BarChart_get_SupportRealtimeGeneration_m077E8988B961920DF1FE5FCB35CA4D99551614E4,
	BarChart_OnItemHoverted_m4E15759CB055C3BDDBF5094CE07E93C63E813426,
	BarChart_OnNonHoverted_m05A0AB73063F68853B97614EEA177F6938C824EE,
	BarChart_OnItemSelected_m83086136E91ECF8F79CCE662FF0FFF671F583AF9,
	BarChart_OnPropertyUpdated_m5976C15596FACB0215ED806F5AE7F5D6ADCB70F0,
	BarChart_SetBarSize_m25D60A5794087E8F46D77FBC1542B7ECAA041AF2,
	BarChart_AddBarToChart_m194E18C4318234C4FE91F850BE881D0B1E1C963F,
	BarChart_HasValues_mD38A53F80284BE06DA833EBB1FEEC081BDB69ED7,
	BarChart_MinValue_m21AB9B71D3F1F19496054A04AFBEFE82E84C6868,
	BarChart_MaxValue_m9F8A2CE3DC95D5C13AA19D92BD06ACBCCE8C828A,
	BarChart_AlignGroupLabel_m05A24C987A4E57E2DD3DED145B534CA21B35631A,
	BarChart_StackedStage3_m22553F4F512D7C2F5159773AA4F31966FB926F23,
	BarChart_AddRowToChartMesh_mDEF7D4EF74727E15A39917033F6FB34D5201711B,
	BarChart_ClearChart_mEC5800F0BE5CBA4DBA9E95DA1522CE4C04C964A9,
	BarChart_OnLabelSettingChanged_m8980A1D7559BEF60E522042F68CE6CFDEC7D8B09,
	BarChart_OnAxisValuesChanged_m00C7112EDA9466C15B7A8694B1BB2D9EC51A997C,
	BarChart_OnLabelSettingsSet_m34CC22285E88025974DAC9C3F162C3449605776D,
	BarChart_MessureWidth_m73AFC9F8AD30F3BA5D7BCF29E1651CF4433D7012,
	BarChart_InternalGenerateChart_mC877303FD59FBA7166CF8793A671C6E9E0AC04A6,
	BarChart_get_LegendInfo_m14FF6C2E093EB8F7536962FFBBE8B7F28B722DED,
	BarChart_get_SupportsCategoryLabels_m5C3DCF476C28897B546746C0DBD9A9EA74BB193A,
	BarChart_get_SupportsGroupLables_mB7628DC5B947A49CB22ED77F04F12A00CFEE409D,
	BarChart_get_SupportsItemLabels_m0C5FA8519A044733D76BA5432E58577B67BB5028,
	BarChart__ctor_mBDBF0E0952D22589FFF07D983F47911150643724,
	BarChart__cctor_m673259ECD04A6F9BFA6C6AF3C7B57FDAD03BBF87,
	BarEventArgs__ctor_m84F48C27C08BE4462D8BFE7C23A1112DED835F1B,
	BarEventArgs_get_TopPosition_mE31D84CAA751B3DF74913ACA6373FF46CEBD4013,
	BarEventArgs_set_TopPosition_m41B22FE4648B00A7E795C1F56F6E1C686BFB43E2,
	BarEventArgs_get_Value_m06CD6D6E61DA6E762AEE2A011CD6B5B104FE331B,
	BarEventArgs_set_Value_mC913E728B00D39DDCDFBD63E75B9BC2932DE9B03,
	BarEventArgs_get_Category_m3E31C561F7C77965215EF838112B706808754EC6,
	BarEventArgs_set_Category_mF8E4603293F4B61290C0AC030DF8C92605749AD1,
	BarEventArgs_get_Group_mE173DA1B1DCD09DF12A094D76BAECE8CE0A1FCFD,
	BarEventArgs_set_Group_mB3EA1BA0BFB6DDC8B50F8CC8A8C5038C2ACFA69A,
	SwitchAnimationEntry__ctor_mB5E36853ADBCC71F59DAC8F6EF5234BFC88AEEE9,
	BarEvent__ctor_m26C4A0E267E38335493679CC4232CE27AB526F2F,
	BarObject__ctor_mBE53D24F981B184F73C0F1551FCDE6727F50A896,
	GroupObject__ctor_mF35FBF00B2102156BFD71595F90A3C8C48F646C0,
	LabelPositionInfo__ctor_m9A08B018C1FB8E065DFA2B1B34E8C6148A8F5168,
	LabelPositionInfo__ctor_mF62A3DF64E35F11CA1218B40AE99723F2636DE26,
	BarData_ChartAndGraph_IInternalBarData_get_InternalDataSource_mB9A94792A3DA6E4C969CB8258D9BBA843B90B511,
	BarData_add_ProperyUpdated_m3BA9B92B6E412D8239F68AA155C0AC8109B79693,
	BarData_remove_ProperyUpdated_mF53A3D39F62A6B39588F48F64E386BA2A7A29107,
	BarData_RaisePropertyUpdated_m531A88B715B3D9B09C48AA1CF93CC5D39B43B113,
	BarData_get_TotalCategories_m8D7A20AF0D0696ABA9B5278F0EC5C5EBE2F2E6AE,
	BarData_get_TotalGroups_m2B4852C3EB01BBEAAF6C64934E14A5DE365481D2,
	BarData_Update_m0798CF1E8580BA04A7DF49A34FEC9F6BFFE506CC,
	BarData_RenameCategory_m56EEE3DB12B836BFDE843FDD02E17EBA96477CC2,
	BarData_RenameGroup_m67141774259FF3651B04253CF068F35BC069C62D,
	BarData_get_AutomaticMaxValue_m583863705121013C75414377A17956908CE2ED3F,
	BarData_set_AutomaticMaxValue_mB91DB91E0C540459CDF85003CA15FDBD21A033C7,
	BarData_get_MaxValue_mD6E8FF0722B6FCF2DBD5769D5A331BF9953BEE66,
	BarData_set_MaxValue_mF179732A9C2CC11EC9067B522F6EF848A424214B,
	BarData_HasGroup_m1E64B99CDBC2E330C0F160F88560EE4C5C1FD4B4,
	BarData_HasCategory_m0D921031E131F02218D24E9F6E2CBBED14FAD40B,
	BarData_get_AutomaticMinValue_m1C951AEB21DC840AB2503894376F3E7BF567F513,
	BarData_set_AutomaticMinValue_mF51752411AB76C72C3F45976C60C8C362C6999E3,
	BarData_get_MinValue_m85AEC54442AB41E522701321963FC52064AEC0B1,
	BarData_set_MinValue_m008A5E0195CE724472CC6A3A79B37189DE7BB47D,
	BarData_ChartAndGraph_IInternalBarData_Update_m81B65DDD107E4F5D6BAFE099836475BCB725F813,
	BarData_StartBatch_m4F385F71BE659C7EE2D13BEAAC6B11DD9541CA07,
	BarData_EndBatch_m907319050E7F64A5A17F0BDD1FA86D241CE0C90F,
	BarData_GetMinValue_m169E40646FCCE40F26311B9578EEED8DA63CB4DB,
	BarData_GetMaxValue_mC33E2474B4A7D2A8685879AE8F903CEBCF293D4E,
	BarData_GetCategoryName_m39F750B40A7173082132152F872DEEA2EAC2D5AF,
	BarData_GetGroupName_mC1541D03DD6F44A2F21D7E9A34AFBC7779CBC610,
	BarData_StoreAllCategoriesinOrder_m1F5AD79FC32E9D51A4B3B92A5F42006FBECA89EC,
	BarData_OnBeforeSerialize_m2623BFDA1F9F352E4721B2720BA4F79540449A06,
	BarData_OnAfterDeserialize_m8EC126212D11D283B03A7BF879D1630C3DF4DD96,
	BarData_AddCategory_m42FF55C7638AD1F15480A7F7B7693175F2B5B24A,
	BarData_ClearGroups_mF23E7AF65B140EF9647F2A8EC8D0BB85411ED6E3,
	BarData_ClearValues_m9D18AA44F8E041ABBC48DE7A29A669F0501142D6,
	BarData_ClearCategories_m6A79A7E9A5C0B04F83A68DA64F66D88C0B5A23D1,
	BarData_AddCategory_m2AE3077B9A8E88D02440C6C240F38D2BB9675C42,
	BarData_MoveCategory_m4C84C891C6796F4155A0EF408C0029D7BB95EE32,
	BarData_SwitchCategoryPositions_mF9BAF22C0A71C0B1425C99BE35F7A046977A1216,
	BarData_AddCategory_m1C0BF07095D6D78D1BC08EB4FCD0EAF63F2FC869,
	BarData_SetCategoryIndex_mFAD87119EBE321205C88A725201201E9C14297D8,
	BarData_SetMaterial_mFA9601D6B951092A0C3F6835E46AD8EC619CC6D9,
	BarData_GetMaterial_m3A24EBB9586D0575B3E074F84906D0E121770CC6,
	BarData_SetMaterial_mAF3A442C4C0A49B6ED6E7172B9C7DD6865F69626,
	BarData_RemoveCategory_mF4898735E1A3B47AD9D43D62BCF3B5E12D80BAE5,
	BarData_RemoveGroup_m6953B6993D0EC1839CA014221D587741D04A4859,
	BarData_AddGroup_m2BDCF1952F58006910566AB29E674D7532E84C35,
	BarData_GetValue_m00A67EEFFEAFDD297C74004A13FFD2898B67B46E,
	BarData_CheckAnimationEnded_mD0BBC1738E0966854A74DBEFC6CD10ACB15A49F9,
	BarData_FixEaseFunction_m1280087C854FAC6F2E48DC2663511083ECDFA772,
	BarData_RestoreCategory_m782C89A3D9E455DD881C27531F573D32A99D9723,
	BarData_SlideValue_mB7667D8B22CAF115435F23619C7DA3377F676BDF,
	BarData_SlideValue_m58CD5E30D4366C9ABDCB38C01C7E7461D1417415,
	BarData_SetValue_m93B3BE94FAE368EE150319187E2FB66A8116204C,
	BarData_SetValueInternal_mC9F39A66F2CFBFE422A25845C51F9F5CF7405099,
	BarData__ctor_mBEB76DA2F3C502E3A15A701CA185F7529255371F,
	CategoryData__ctor_mF268FE6A4CB2996569B0A48B21A3B57FC402A29E,
	DataEntry__ctor_mDB9A0BE840FE814D2EECDA65BF125A3C255E3422,
	U3CU3Ec__cctor_mD9051E904F4D4F371B45A8BB084683970AEAA9D4,
	U3CU3Ec__ctor_mB90F5F7DC5B0071BE8A672161CA40C9059452EB5,
	U3CU3Ec_U3CClearGroupsU3Eb__48_0_m7E7F6680AFCA52FEB76CD72C9A2A73D1AC9E52AB,
	U3CU3Ec_U3CClearValuesU3Eb__49_0_mE9FDADB32DF74D3B705AD381D05F310F9247617F,
	U3CU3Ec_U3CClearValuesU3Eb__49_1_m5BE4F428D726DC443AA57CDC0732ED7F506D919A,
	U3CU3Ec_U3CClearCategoriesU3Eb__50_0_m920FBBF0C83904577FCF36AEAD11B435BB90A2FB,
	NULL,
	NULL,
	BarGenerator__ctor_m4C54ACB7B5F4BFFB7182318224C318D142A4D922,
	BarInfo_get_BarObject_m7E3CC708841B7C5940E943A7111C56B722F90D5D,
	BarInfo_set_BarObject_m55485BFB5ABF5DE9B7ED25DC70E4DC6B63A4852E,
	BarInfo_get_ItemLabel_mA9559BC6E593E85EDAA3A4C499D6B769CD8F1D31,
	BarInfo_get_CategoryLabel_mD11D24E4CFB853331D318C9579FDAE77F946237F,
	BarInfo_get_Value_m0CF1404F57C704D50DAF3E0BC216954878520AD7,
	BarInfo_get_Category_m4294A194D8642AD1A72693F24CDA407E1BF9FB76,
	BarInfo_get_Group_m0A3EF74DCAAB461D7EC0FA80AA8C40D1051F56D7,
	BarInfo__ctor_mF9DA48C6684929471DB7D28AB4505E1EF752B7D9,
	BarMesh_get_StrechMesh2D_mB5B20B9D7D9AC60FDD62FCED6AFFF1E3F90CFE3B,
	BarMesh_Update2DMeshUv_m1BC6E557D6E945866E973467F3F2C6AE21DCF45B,
	BarMesh_Generate2DMesh_mBE5B1C316CD6007DD6B3F15C529A1A0360857DDA,
	BarMesh_WriteRect_m922A4073B73DAF909599421FB448627506390C58,
	BarMesh_WriteTringle_m55D7DED3B849F00665CF4B6CA2C7F63BD1B3F9F4,
	BarMesh_get_StrechMesh3D_m49A164F2C3AFC2A0C40C46055201F2DAA8E928B5,
	BarMesh_Update3DMeshUv_m2AD274FE0F79FB8AF7AB9C8BBF411B07583D71FB,
	BarMesh_Generate3DMesh_m8F8D3DCA18C0959EA6177180E587AA604E43906E,
	BarMesh__ctor_m7CB9ACD811B612A75CFC5E7D1D4EDDE79B89FD51,
	BarMeshGenerator_EnsureMeshFilter_m629BF72229D41AC7648E9ED9D12277E75215D7D0,
	BarMeshGenerator_Generate_mDFB8ED811667D1A94DD6BAB14CAA6B8348E3237D,
	BarMeshGenerator_Clear_mADD7FAC31910BE76957C7463C20E37314479B347,
	BarMeshGenerator_OnDestroy_m4F35D92BC00DAAA5C28AC0C5378E1BC2F5540497,
	BarMeshGenerator__ctor_m5CAC00DC73151B7645CDC8B20D8C6E49419FC524,
	CanvasBarChart_get_FitToContainer_mC473D4570F11316ECD3E358249F7CB74CDB4D666,
	CanvasBarChart_set_FitToContainer_mAD4D6FE5C9151CCDAADC6926333C60C79B3DF15B,
	CanvasBarChart_get_FitMargin_mEFBFCBA33F7F73E0A984CE3CF2854832C108EE0B,
	CanvasBarChart_set_FitMargin_m1E18E11B2AD52A7EB05DBEEEA29E742ED0294A12,
	CanvasBarChart_get_MarginLink_mA6711E253B405A6C71E564D0BC6AFF4D7B159217,
	CanvasBarChart_get_BarPrefab_mDCCD475AE30A3839ED927E6884636EBA1B81AC76,
	CanvasBarChart_set_BarPrefab_m0485790F05BA56B39B55AA084588F620EB581640,
	CanvasBarChart_get_IsCanvas_mC83EC1FCAB5BF8102570B36A74FBCC40A309D965,
	CanvasBarChart_get_TotalDepthLink_m16C4FE0C4564A0E6F0F9C4AAFD01E7378114C323,
	CanvasBarChart_get_AxisSeperation_mE89865474F4D2DBF58C890051E072324FC4F57F9,
	CanvasBarChart_set_AxisSeperation_mF416F38FD96938FEF7989CBAF29A1210E649F00E,
	CanvasBarChart_get_BarSeperation_mDF4B31469CBFA598C62010A387A2B364A1372093,
	CanvasBarChart_set_BarSeperation_m898B3B085E6E6A1E6D4EAA44A14AFEA695A72D71,
	CanvasBarChart_get_GroupSeperation_m6488F1AE8A394C7D62087B03FB7C3DA179E29EA5,
	CanvasBarChart_set_GroupSeperation_mD9D894198EA857D4C787BC72859EFA4918EB6BE8,
	CanvasBarChart_get_SupportRealtimeGeneration_mCF54E2C6CA65CBC4764033CD1DB135D708BD51EB,
	CanvasBarChart_get_BarSize_m3C1DA959EB51037BE8D2CA2EAF4FF01A28807550,
	CanvasBarChart_set_BarSize_m84C620DB58861B71EB3F06E0D4A22DDCBC56040A,
	CanvasBarChart_get_AxisSeperationLink_m42DB617962F047DE2002073B32341F3EEE1F2971,
	CanvasBarChart_get_BarSeperationLink_m72CDB88938ED1C78E059A7E3E5C92F47590778DA,
	CanvasBarChart_get_GroupSeperationLink_mDA7592C5CF2F59A907E597AF4A6228AA92A71ADF,
	CanvasBarChart_get_BarSizeLink_mCC456A5FD458A041AF1F4CB074CD9E4E535BB9A3,
	CanvasBarChart_SetBarSize_m8327A929C1172D3B3F980FC7F54ED481EE790BE3,
	CanvasBarChart_Update_m8A882D2E3B493F4B7D2C958434800AD2F888E129,
	CanvasBarChart_get_BarFitType_mFA58CCDAAB133B31133B78B5851CBB1D0610D563,
	CanvasBarChart_set_BarFitType_mC6ABF38A919CA2B6B438801B261649CCC171F063,
	CanvasBarChart_get_BarFitAlign_m0433A5761C5A64074F916DF04D92235BA2054749,
	CanvasBarChart_set_BarFitAlign_mF0C04A741D47AB24402A716B384991D56AB0A016,
	CanvasBarChart_get_FitAspectCanvas_mD7037D51296FD9FAB0EF95FF4988B6C8187C899E,
	CanvasBarChart_get_FitAlignCanvas_mFD5780B5C6BE7F9489589E02B26D9754BB3A8A80,
	CanvasBarChart_InternalGenerateChart_m48A6475265177A377CD7A1590CE29B2E52E7470E,
	CanvasBarChart_get_BarPrefabLink_mD61833CCFE22D2154BF01BC92C948FE393ABED48,
	CanvasBarChart__ctor_mCDEE64AC43D04AF9AC3D00D333F8CB713996256E,
	NULL,
	NULL,
	WorldSpaceBarChart_get_TextCamera_m5200FC02DC4A0288FEB4B7105B526E3B056D851E,
	WorldSpaceBarChart_set_TextCamera_m6F4E2D4FD6E8A4E4AB679F43036C2339689CF245,
	WorldSpaceBarChart_get_IsCanvas_m6D843ED098999650995345EABF749421072CF3A3,
	WorldSpaceBarChart_get_TextIdleDistance_mE9656FE551464171FC72AC702E64BEA69DB4F2B0,
	WorldSpaceBarChart_set_TextIdleDistance_m6EDAAD2822FC4DE54AF6D081C251B8E585874710,
	WorldSpaceBarChart_get_BarPrefab_mD7264BAE5BF76A6D4D7FC0E56C8611F17C4583F5,
	WorldSpaceBarChart_set_BarPrefab_mB94DF7E4979FB740147FCF7A24A166CDFD63DC8A,
	WorldSpaceBarChart_get_axisSeperation_m9C90A90D80E986C192DC757B2D8F8D9BD56FCD8D,
	WorldSpaceBarChart_set_axisSeperation_m21CB675BC1FB5364481E1C52A5A9A3395B3D369E,
	WorldSpaceBarChart_get_BarSeperation_m11A3B8CED9D4478AE8CF2810979CC6D1B28D3477,
	WorldSpaceBarChart_set_BarSeperation_mB6CC1112E1E9FB7CB9A36B6F9B2EBF43BF86DFBA,
	WorldSpaceBarChart_get_GroupSeperation_m5D2E9D52827299240E6C99CB5FEDA45A6386EB62,
	WorldSpaceBarChart_set_GroupSeperation_mEE558F350DF60770E416ECD6522F9286CD1FB379,
	WorldSpaceBarChart_get_BarSize_m593850078A534DC91727D2D9B5FFE07BA337E94D,
	WorldSpaceBarChart_set_BarSize_mD96AF5A93C6D68B18614D454A6D3CD9D51F5C3AB,
	WorldSpaceBarChart_get_AxisSeperationLink_m8860AA8F523EB6FFF800596AF302CE2A59B3FAA0,
	WorldSpaceBarChart_get_BarSeperationLink_m916DF642C26428A7E05BDA792ED01ACE750DA795,
	WorldSpaceBarChart_get_GroupSeperationLink_mE426E59E772A6513A7380E69899B5DFEF0DEB5E3,
	WorldSpaceBarChart_get_BarSizeLink_mE9E9FF0A87DBED49157A52CF1D3B9CB6E4DCBCD3,
	WorldSpaceBarChart_get_TextCameraLink_mB2F1FD1DD2493D85EEF79DEC9C0A8A603C6A49DD,
	WorldSpaceBarChart_get_TextIdleDistanceLink_m75F46ECBBE10E9D03A0E6109A6A521C76EFC8A96,
	WorldSpaceBarChart_get_BarPrefabLink_m4408CA0CA91A0FE6EF6BF7710F7EBB696422B54E,
	WorldSpaceBarChart_ValidateProperties_mA196DC4D4C01B2045387DFC4F3E0DF137971F85F,
	WorldSpaceBarChart__ctor_m2F3BFB7BE60C06874B611C431447027BE76B6E09,
	CanvasLines_get_Tiling_mE65CEC6E26D8B194309BF763864A0CCE74BA6B17,
	CanvasLines_set_Tiling_m6083B7A5109191E770793EEDE23CD3C5F2144B42,
	CanvasLines_get_ClipRect_m474D9C89D9AE388D759504AAB85FBB2E5F6E253C,
	CanvasLines_set_ClipRect_m8016CC7BCDC55AA769103BAD3800B83F0EBFE926,
	CanvasLines_MakePointRender_mA4ADF69677C8E6E5A71C9F3BB45D0740D5BF4ACA,
	CanvasLines_SetFillZero_m6E9D3754BD74B4379193DA9CE5900D1AB8F8FBF5,
	CanvasLines_MakeFillRender_mD3052B8AC5FD302664C83E2D189F548EC8DB1B66,
	CanvasLines__ctor_m89CCDF01AD700DA84B8168806479C92DB342536F,
	CanvasLines_FindBoundingValues_mCA4E7C9399C94854EB978F2E995E674256872150,
	CanvasLines_ModifyLines_mE282752721041452BE24199EB3A921D53379D212,
	CanvasLines_get_EnableOptimization_m202B20B5E9F00E4A1213D5ECCB945268B1593457,
	CanvasLines_set_EnableOptimization_mCAC2A3AE52E5A5B3BF4C886BD10BF48960BA65FF,
	CanvasLines_SetLines_m5AA7904B90F13C41387A172DE1FEE7E419F2C1EA,
	CanvasLines_UpdateMaterial_m66877253FD2A932E344F35206D7BD0496CE232A9,
	CanvasLines_GetSide_m69A7F84C9228E81FBB63AA7A51F31A85BD3AED69,
	CanvasLines_OnDestroy_m462631C63830C9F45DA9F55EF7AEBA4936881B5C,
	CanvasLines_OnDisable_m0AEABF75331E5CA504364B353973AA5C8958700C,
	CanvasLines_get_material_mC498CFA5EA6C95524B9A93FDAC6190E679FF0567,
	CanvasLines_set_material_m243A75E3A4DDEBADF37A38BA7D042B42E0AED744,
	CanvasLines_get_Min_m96A0B33AE7A51813596DF375DFA91121A5470565,
	CanvasLines_get_Max_mA7A2DD721A200F63E8C9983C93041B6D33CEB893,
	CanvasLines_get_MouseInThreshold_mD4DFF86633EFBCEA488C157198191ECD387D9A69,
	CanvasLines_SetUpHoverObject_mFF285C1EC3299EBD22EA30CF6D26288EF2519848,
	CanvasLines_Pick_m490C1779FDC2E791149FE75D1660B86C75D6A4B2,
	CanvasLines_Update_m7F1E218E058908F594CC79F1EA2F824FB0351CC2,
	CanvasLines_ProcesssPoint_mF6D3700AD0C5E527CF7815E0963167C4E15282BE,
	CanvasLines_getDotVeritces_m57F8AD391BB4EA247CE286F9B61901B445F0B5C5,
	CanvasLines_TransformUv_m544A7D54BDE58042C42E9D7F1F1056228679EE92,
	CanvasLines_getFillVeritces_mEBED888394C7BD35F69B03E2D32028DC50B5D971,
	CanvasLines_getLineVertices_m3EE775FCB0F13DC4E61DE3FDE73ED596A0977B66,
	CanvasLines_getVerices_m34D66F851E2599DBF9EB9E2153C72C6C3F7A1BB5,
	NULL,
	CanvasLines_UpdateGeometry_mDACDCC2CC9DDC6CAE59C250567C83B3760DD8DD0,
	CanvasLines_OnPopulateMesh_m72917C3F9BA452191C4B0E37926403772DDB7D67,
	CanvasLines_PickLine_m78C8710B19E65A2E0E4FF3BEC40FDF2CD37FDB96,
	CanvasLines_PickDot_mC0DD68BC9446F2765E836456D3CD0111ED4818FE,
	CanvasLines_TrimItem_m788921BBDF2A54AE044ED03D445132EAB6BD5B9D,
	CanvasLines_TrimLine_m3EE97578F68CCA48E5C8C33857279D254A7E71C6,
	Line__ctor_m4389E71B32F966E8718C9FAFD8DC7C9F6E2110C7,
	Line_get_Degenerated_mC903C588921C51C16C8A9EDE11AACCF1A4C759B6,
	Line_set_Degenerated_m378A5401A9BAA385E72BC9885DC3B53F1414B451,
	Line_get_P1_mC750288C96B6CE408C61A85DF57B29B463D10C0D,
	Line_set_P1_mC38DFAC465CCB453B3B4AB7E97A447590CC89965,
	Line_get_P2_mB960A6BEF78A9B952DAE1A299D67CFC2012E2A1F,
	Line_set_P2_m160F2F164EF038D1F32214DE28204AE9683D33EA,
	Line_get_P3_m06A9A176422365C1575296A1F25FD5DEF4D99DDD,
	Line_set_P3_mCAF01F4FED931AA69E7246B0FCBB544D303C7B70,
	Line_get_P4_m8283F3E3C3EFF22F76BDAE1934F53F5175BD2616,
	Line_set_P4_mC6DD8CC9AB985D8FB738267F457D4C34250B9E6D,
	Line_get_From_mE45AE1DDC8A9D9EBF61227F17C4266A2F2DE6E9B,
	Line_set_From_m592C4AE6BD4846DFCC12721665EE1B58E1D47F04,
	Line_get_To_mD8FAC6076802B34205395DF694BDCE8314F1F2DA,
	Line_set_To_mB09CD554B116268CF79C72323AE421A1C6C0B1F3,
	Line_get_Dir_mCD8563E867388D16CF2A859711F1868D2A1E77D0,
	Line_set_Dir_m8099FE4EEA13DB6F851843C3258BC6931DE82B85,
	Line_get_Mag_mFBC4B05934E2168B53EA8A64864446F3CE366AF5,
	Line_set_Mag_m8DF404321EC5B783086BAAB927F520F0B4FDE1EB,
	Line_get_Normal_m8F500F8704DEEF72A8F69A20608C2ACEE316146A,
	Line_set_Normal_mD0FB285540CA993D6648EA0A2ECED90968AEF099,
	LineSegement__ctor_mC8F6BA6EEDD0553398CA7F326E32A932B778D0B5,
	LineSegement__ctor_m59BA7FA62AB02CA4B1F910059229A5935542DACC,
	LineSegement_ModifiyLines_m0A23D098CD43B5BD907B7237B82DC11B911375D7,
	LineSegement_get_PointCount_mA0AED9D32FE2F9587749EBB3C9748391F2709BE5,
	LineSegement_get_LineCount_m45BE633409E19733EB4262805AECEB0FF0145434,
	LineSegement_getPoint_mC1566C4EDBA4C7107CDBD82ADA709D19E3AA426B,
	LineSegement_GetLine_m402749446A5EAB355A71D17F76B09D126125E83C,
	LineSegement_GetLine_m1DB5640B2EE99E6C5D40D2E15476285F0BAF18BB,
	LineSegement_GetLineMag_mF41C5C1F747753C72426DA8FEC16959FC0FC7AA1,
	U3CU3Ec__cctor_m88C99D6E184D80254FF1B1EC0FD438D0C1A6D275,
	U3CU3Ec__ctor_m80401BB46A3F46478F980ED78A74C462464E0242,
	U3CU3Ec_U3C_ctorU3Eb__1_0_m5E1E42936DDD29BEE50C30FBF4AFD136C63D3A63,
	U3CgetDotVeritcesU3Ed__54__ctor_m4DB72EA868575928A2B878DB003FC69EC843CB41,
	U3CgetDotVeritcesU3Ed__54_System_IDisposable_Dispose_mD23F1E6AA65C6A057B3CB754453E5EF805A6D1C4,
	U3CgetDotVeritcesU3Ed__54_MoveNext_m21D2E118869BC25ED2257E56C5DD7837FB02EFF2,
	U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mDE5152173FE2DDBBBEFBA74FF5D1003DB57329A5,
	U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_Reset_mB94C31961CCC9ED9E0C43BDA9EDAB2C8AEC8168E,
	U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_get_Current_m5EDAAE023B18488FB09268F34195242C38E00398,
	U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mDD7964939474EC93D2FE78BE7CC49AFBA792B7FE,
	U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerable_GetEnumerator_mDD309DD0E06A30244A07398B3954C6D027892939,
	U3CgetFillVeritcesU3Ed__56__ctor_m84E7F14E607A4946C8501AB381A61E9073030C41,
	U3CgetFillVeritcesU3Ed__56_System_IDisposable_Dispose_m24807AAE5C8A054491097465175CFE35CD1DEF66,
	U3CgetFillVeritcesU3Ed__56_MoveNext_mD455660F194F97BCC07CCE43B42A08F5BA7A28F4,
	U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mD6B1F95D7EB23006D2F8D4AB387FDD8DBB18F306,
	U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_Reset_mB82B5598EA4CE0F46906571B4E8D9DA77BC3B6FF,
	U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_get_Current_m8EA14AB9919BD3DD944A510B246A8C9AD1E044BB,
	U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD742CD8DCFAD0905516F7BD5DC1F0EA2BC14A306,
	U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerable_GetEnumerator_m16CFCBA5318010625AA9C47463B1FDECB73EF4FF,
	U3CgetLineVerticesU3Ed__57__ctor_mEE477D5EAAE9411DFD0492352D6FB42078AC1569,
	U3CgetLineVerticesU3Ed__57_System_IDisposable_Dispose_mC8072E3A4ABA4613032B8D7E71DB91558C416482,
	U3CgetLineVerticesU3Ed__57_MoveNext_m795B9D9B4534F2CBCE238BE166D83C3E8EFBBBFC,
	U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m32AE92CAB16E5053DB5BBCC59C1685CCFF523DE8,
	U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_Reset_mEFF718EC377A4A2E4429F5E1E750125C21D55DB0,
	U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_get_Current_m6323FEDAA1579D420AE2EA6FB069041B77EEE869,
	U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m314F4C290F52F681EB509E6551875D62C947F5F4,
	U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerable_GetEnumerator_m8D920BB8D684EEE193B70580501FDEF370F80984,
	CanvasLinesHover_Init_mB3DC6FE954A3F49525416BFDD22405F8F818BE7A,
	CanvasLinesHover_getVerices_m7234E00BE8E15C9BEEC7F63445BE89EB20DB7229,
	CanvasLinesHover_OnPopulateMesh_mF099508F65972C028394C082CA0A1E9EB6D7D10C,
	CanvasLinesHover__ctor_m1C3FCA3758D72D6CAB15088BE02E48978B8BA9EA,
	U3CgetVericesU3Ed__3__ctor_mFB4CB89B833F447622410D7F7A7CCAD4C98D70BF,
	U3CgetVericesU3Ed__3_System_IDisposable_Dispose_m746EFF4FE9C1FFE4D6A7070FB0CA043639F68F45,
	U3CgetVericesU3Ed__3_MoveNext_m89DA0703213B2918426B9C76BD3EE121A7D2B6DC,
	U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mF499DCABE17E18C34CA602E520367789C8BB8AD9,
	U3CgetVericesU3Ed__3_System_Collections_IEnumerator_Reset_m5F6B721252ABAB165C6649E7E398EE50EDE376B7,
	U3CgetVericesU3Ed__3_System_Collections_IEnumerator_get_Current_m17C9A5DE1F6758BF4F866CC0DD45A53E9E6D33D1,
	U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD5851E834D32366D321671E68BFCB0CA31D1DE41,
	U3CgetVericesU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m76999D741D77F8EB58EFC799D0DBF597C38D6CFA,
	EventHandlingGraphic_get_refrenceIndex_m74CA55213DF53D00162E676956F11D1311D84DB3,
	EventHandlingGraphic_set_refrenceIndex_mA31A5548EE2B4B2D2A76FBB107933ADF89B0EA09,
	EventHandlingGraphic_SetRefrenceIndex_mCB3F59C440167FD68904130C54F91041F5C5E14E,
	EventHandlingGraphic_get_Sensitivity_m6066244F53D9D32CF6EBC441572A9CC5F0300122,
	EventHandlingGraphic_ClearEvents_m88B6238533A9D5E3859B8A66DC83F05CBE02C06C,
	NULL,
	NULL,
	EventHandlingGraphic_add_Hover_m975682AE4DF3A55C09E90918447520BC78FB4C84,
	EventHandlingGraphic_remove_Hover_m83C03EF5E1E66A44170A92CFD02162EDA9F07CE7,
	EventHandlingGraphic_add_Click_m158BAA2833986B66A6D82E45FC0D81BD82F65289,
	EventHandlingGraphic_remove_Click_m46285AF60CA8F9CC618C99DD50E77535486D011B,
	EventHandlingGraphic_add_Leave_m3CEF09D689312D3288058607971942DA3A7E280E,
	EventHandlingGraphic_remove_Leave_mA0C44F388334C72624433637A97EE482B1A7D331,
	EventHandlingGraphic_SetViewRect_mC9080BA124E60101AC5FAB7AAC26BCB6127500E2,
	EventHandlingGraphic_HoverTransform_m223EF0281BF374829863DD221C2625A3A4880AB5,
	EventHandlingGraphic_SetHoverPrefab_m789FB1014508B6CBCA14F7A76800717C237FF7BF,
	EventHandlingGraphic_RefreshInputs_mE124118E0B26059766ACF7EECFB3A41540BC8645,
	EventHandlingGraphic_Update_mA9ED966C414D2C31BF5BD0FFF406E366550C0F0C,
	NULL,
	NULL,
	NULL,
	EventHandlingGraphic_SetUpAllHoverObjects_m85F4FED218C93940F5563751C3AE2B7FE0A61480,
	EventHandlingGraphic_SetupHoverObjectToRect_m131EF02087027565A5C9ACBC3A53894430653A4F,
	EventHandlingGraphic_TriggerOut_m61D01C679776385C10027273CCAB92B61D6252D3,
	EventHandlingGraphic_TriggerIn_mF097DF471EA28EFBE75EC88B724B06A1AF8808E9,
	EventHandlingGraphic_LockHoverObject_m375CE14E249E450F1803AA48E7A228F2374816DC,
	EventHandlingGraphic_Effect_Deactivate_mB36F12B889A87E511A50E0F28F398BE8CC117931,
	EventHandlingGraphic_SetUpHoverObject_mF075E2B2F891460E978C4ADFA4773E96E1203D6C,
	EventHandlingGraphic_DoMouse_m0032911C7667DDD91C8F50DFA9ADFE7CF8D7C232,
	EventHandlingGraphic_HandleMouseMove_m6AB9C576775784D8F64287D5113810858667FB64,
	EventHandlingGraphic_HandleMouseDown_mF4F4AA1559162FBA57E80455A5A46B802BDC3C15,
	EventHandlingGraphic__ctor_mB2BB5F56FAFC4CD2B0E86EB5DD1EA280ED29DA43,
	GraphicEvent__ctor_m0DD5CD0FC7F5085311D1B6E1E9C52CC71D79B0AC,
	GraphicEvent_Invoke_m88B32CF8C4875117F435ADD8A2D2ED06566E3F18,
	GraphicEvent_BeginInvoke_m45AE99ACD97CFD6CAE90D3EA4BB5C24A4ED3E007,
	GraphicEvent_EndInvoke_m2BCCB8F1FC7075EF041FBBA21E9DF022DC1C60FC,
	SensitivityControl__ctor_m4B727A7BE35E69CD262AB685BC203868F6CAC79C,
	ChartAdancedSettings_get_Instance_m93A3FA4A26C18DCF5312E05C6AFA63B13E8BE20D,
	ChartAdancedSettings_InnerFormat_m6A96887E59FC7AFB11B6A2ACA80A1895AA1B2D4B,
	ChartAdancedSettings_getFormat_m8F8F5D21FEDB1FC3E5EFE1E97D0B9CAA1914ED83,
	ChartAdancedSettings_FormatFractionDigits_m154907F18D4D82F326D0406CC94CBD1739BFBA92,
	ChartAdancedSettings__ctor_mC0C2F645CD1F8D6FA6F756BA29F87E01AB8174A6,
	ChartAdancedSettings__cctor_m2D3F8CE33C5D3B6D31D71D5ADC2F49343FA2C3F8,
	ChartCommon__cctor_mAE6B2F155729FFCADA7907B9E157CF073EF66BC2,
	ChartCommon_SmoothLerp_m3DB7B51FBA7A1FCD82AAE0DFC0338D1B579543EA,
	ChartCommon_CreateCanvasChartItem_m6367E70A97FDD247B08C0BBA9B3F85B677C4DB8C,
	ChartCommon_Max_mC3FE068CC7153142E6690CA50A4DE95B023D84A2,
	ChartCommon_Min_mE829F2ED8AA2DF44BAB0B467BA0997B4EAE0586E,
	ChartCommon_Clamp_m7FA20092D4B7BDD2C456996EE4D2C9429EB27041,
	ChartCommon_CreateChartItem_mBBD9BB97FF1448ED4C8F90E325CB1E63D00AFA3D,
	ChartCommon_normalizeInRangeX_m5A9FC8DB93377F29AA36EBF2FB995A0AE8A500BB,
	ChartCommon_normalizeInRangeY_m35A5D1F57761F02B18D4B7FE6C18CC4F3DA3DD60,
	ChartCommon_normalizeInRange_mDA75C1F9FBFE69D76C41CFAD47A4B24A490229B0,
	ChartCommon_interpolateInRectX_m8AFD422B328D5E146F36497E790DE89313328F3C,
	ChartCommon_interpolateInRectY_m31929A422085887AF05040282AD0AC4C9DFF64D4,
	ChartCommon_RectFromCenter_m33AF4D2E3F82303914D512DD7D1B6B07843A8479,
	ChartCommon_interpolateInRect_m88DFBE7CC8DACF7FA54E788BBEBF182D29252364,
	ChartCommon_HideObjectEditor_m5F1CE5AD96DBB15EA841A0E42D09BAE112D34E58,
	ChartCommon_HideObject_m02029FFAFFD8E145E2A4023FD3BD15B06FA3BD92,
	ChartCommon_GetAutoLength_mC1E9A8F65478C1B880C6EED6CFC12EE0FA2B270B,
	ChartCommon_GetAutoDepth_m12DD417D9FD850CFC60154435AB17E7E2CFC2342,
	ChartCommon_GetAutoLength_m244469F67A946AEF4422CC269BE15EEDF49DC29E,
	ChartCommon_Perpendicular_m8984CB53060E190870D26D2FADA4D721C0E5A48A,
	ChartCommon_SegmentIntersection_m15BF96A23B5EFB4D182F2C73E00E84CD1F24A86E,
	ChartCommon_FromPolarRadians_mB05713735FE5CF63F9032FB1E118EF346B70448A,
	ChartCommon_FromPolar_mC7C3BA4D189BB96C571550D023BABB7C26B74648,
	ChartCommon_FixRect_m0CF29336CD969BDAAA29B7C1C8DCCFE16AF5ED9A,
	ChartCommon_get_DefaultMaterial_m1D33DE6E247F3311C5AA70A5A7DC61343A96F809,
	ChartCommon_SafeAssignMaterial_m059805B45166DE800D2A048B890F1079396D193A,
	ChartCommon_CleanMesh_m755E1C0824152425BD9276BB0ABD8109414DC964,
	ChartCommon_get_IsInEditMode_m0B9271E100A57E945F661EEDF0F86B6ACA13B0E4,
	ChartCommon_SafeDestroy_m58D9FA166A831CBEC3AD36A4F52968B940CEA112,
	ChartCommon_CreateVertex_mFC195A32B6F4AD8DB1FF7A15ECD8A854CD8BE556,
	ChartCommon_CreateVertex_m1ADE199AC6F90175A82A6352698F88423360BAB0,
	ChartCommon_GetTiling_m8E6D2702A7438ECB2F4607A904103FFD6723CB14,
	ChartCommon_FixBillboardText_m6D88EEC7627A9D263DFF27A743E18FDD5FD455CE,
	NULL,
	ChartCommon_DotProduct_m5467DC6123D413FE21F1086FC990946230C1B1FC,
	ChartCommon_CrossProduct_mF90F396666234B55E717787B886C7E588C72E750,
	ChartCommon_SegmentPointSqrDistance_m6F01504F9CA014E7BBAE1352CF4A88615EE35C1A,
	ChartCommon_LineCrossing_m1CBED50D13323CF24F4AFD7F6D78FF27EB2D7D43,
	ChartCommon_UpdateBillboardText_m14EF9B304498670F3C541C81DD77CFEDA7FB485B,
	ChartCommon_CreateBillboardText_m6C6C6D21CF8003A39988A9846AD701D5AEC1F6F8,
	ChartCommon_DoTextSignInternal_mDE8C01E9282B2853E664C8F786DCB3AF0FACE4D3,
	ChartCommon_DoTextSign_mD77A567B0F6B022FD728C6CE2B9375C82E7FDB5A,
	ChartCommon_GetTextInternal_mFC69F4FB31FEA49CA1D0C727739CEFD0D7A78608,
	ChartCommon_GetText_m1B7ACA5C982205583B68EE99290163D37994B17B,
	ChartCommon_UpdateTextParamsInternal_m51D34A42A994C7D5AB49C0927CC9DF8BA031FC16,
	ChartCommon_UpdateTextParams_m473E0338A8FAD950003826E2E42AE9BCD5399B0E,
	ChartCommon_MakeMaskable_m35BB02A85D2D3DFD5A50D83D394F9C2C5CE3CDA0,
	ChartCommon_SetTextParamsInternal_m5FCE550700149981A25C9D62F159E3E29B08B4E7,
	ChartCommon_SetTextParams_m6A082A38EE438F6A312445FED24CB245EB252839,
	ChartCommon_get_DefaultIntComparer_mAA3284825DD7E0A551A6AC72786E3AF5704CC917,
	ChartCommon_set_DefaultIntComparer_m6C8736F71A022D13C54B0CAE41C075375F0804AE,
	ChartCommon_get_DefaultDoubleComparer_m207B0BBC5438E7E4DFA79767A7ADE3057B2F4C29,
	ChartCommon_set_DefaultDoubleComparer_m9E82EF5C50F5AEDDAF7CD4D6ABAAE35FDC6E5A7E,
	ChartCommon_get_DefaultDoubleVector3Comparer_m3ACF2569E62983F10A1E0C4D60011A10D9997BBA,
	ChartCommon_set_DefaultDoubleVector3Comparer_m5E898325C6939EF7226EEFD7FAD89B3FDF7BA915,
	ChartCommon__ctor_mC150ED618E2E9DC9F015022ABD0841D19D13A357,
	IntComparer_Equals_mCE5289036CDFEFE50B60C04CE44754C90F07C77E,
	IntComparer_GetHashCode_m0AEE9673DE40DBDB70B54466EA4530321B326CF6,
	IntComparer__ctor_m914121068795F2529C12BF3F6E320E2171EB4781,
	DoubleComparer_Equals_m545DA4616136FD4C4781F743171E270A1A03134A,
	DoubleComparer_GetHashCode_m0E63A72E1EBFCAD7F3712C2CADC9007CB7197F78,
	DoubleComparer__ctor_mB598442B9A76F9E63F32BA24FB79836E6C0313A7,
	DoubleVector3Comparer_Equals_m15019ABE09D8C7858F746371951C182E0B1A64FF,
	DoubleVector3Comparer_GetHashCode_m9C95E96710E934B403932BFC2997F2E199E2E051,
	DoubleVector3Comparer__ctor_m9CEA68B314D59872BA13BAE5ADC106329D068751,
	BaseScrollableCategoryData__ctor_m2FA54FC47B3F343387495C326BC2D9813EA2535C,
	BaseSlider_get_Duration_m5771749A0F1AFACA2997F1053A4502EC124BF094,
	BaseSlider_set_Duration_m7CE1BAD2AADB23CD5710DDAECAFD59B478952C3D,
	BaseSlider_get_StartTime_mABAD31F5ABFF68C526320A9CA287BC29CDC73BAE,
	BaseSlider_set_StartTime_m837A80AE1654C5A03634E81241483A31285604CA,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseSlider_Update_mA24868A419012FA26D3CFAECB75BBFC201E6326D,
	BaseSlider__ctor_mCB4C62D22B35D27501FE4F75B40943425FBA07B3,
	NULL,
	NULL,
	NULL,
	ScrollableChartData_add_DataChanged_m53454856861790BB166459C847049FB880A090C9,
	ScrollableChartData_remove_DataChanged_m7BA12D21F694D295693C39AE35D82029698C03F2,
	ScrollableChartData_add_ViewPortionChanged_mC7F6282B4113B254E79F20BFDEA4E2E735A8AC05,
	ScrollableChartData_remove_ViewPortionChanged_m4F1A28D51F140E9964EAFC4E67761798659D59B3,
	ScrollableChartData_add_RealtimeDataChanged_m0982CDB8E7798B3E606CA10F73634E4868CB64D1,
	ScrollableChartData_remove_RealtimeDataChanged_mDE9BB5466ABAA2D883C6E9726976621D6914CEAB,
	ScrollableChartData_StartBatch_m753BE704EB0CE0B6A0214B75DFDEB46911631D1C,
	ScrollableChartData_EndBatch_m6685BC6D3657A5322B478FD998599A8071112F19,
	ScrollableChartData_get_AutomaticVerticallViewGap_m1DC931258FC2271235E7F0C12E261BF3FC7055B8,
	ScrollableChartData_set_AutomaticVerticallViewGap_mE6A43FC5C89F7ABE30C0481314DCBD746B11A60A,
	ScrollableChartData_get_AutomaticVerticallView_m42D15B487B45AB76AA291341AF18C4717986C014,
	ScrollableChartData_set_AutomaticVerticallView_m815C09C9827B8137FBFC6050ECEAE34C9F7EE8A9,
	ScrollableChartData_get_AutomaticcHorizontaViewGap_mAD22B2701D5F4B7B602BCEE8AC48A06E25605893,
	ScrollableChartData_set_AutomaticcHorizontaViewGap_m35565A02DCBF18AF471E4F3556B431632A73C654,
	ScrollableChartData_get_AutomaticHorizontalView_mF51EC62BBE8006208ABB6DDADD936284F5B1B828,
	ScrollableChartData_set_AutomaticHorizontalView_m203D43700DD4132599139AC7843B64BE3C428889,
	ScrollableChartData_get_HorizontalViewSize_mE3846AD3B190DE8F23B6115C95E7CDF75E568B49,
	ScrollableChartData_set_HorizontalViewSize_m0F4FE5011D658285B43416FDE9C8B82515AA8155,
	ScrollableChartData_get_HorizontalViewOrigin_m79E3B1D9878D0595C1E5B6353EFDCA8BE904273D,
	ScrollableChartData_set_HorizontalViewOrigin_mB5C2AF750BFA50D568B07AB31F5D7CE4CBD4C22F,
	ScrollableChartData_get_VerticalViewSize_m64C2EFE321ACE1AD2EB8DF73542030E81F80134B,
	ScrollableChartData_set_VerticalViewSize_mE720414B1C5FBFB5B903587BA4B58483E773512F,
	ScrollableChartData_Clear_m76409556475B150DCE22E9E17895B681F72F6897,
	ScrollableChartData_get_VerticalViewOrigin_m2AF691D4B0C0BD90BA214821C49912B5EADDCC3E,
	ScrollableChartData_set_VerticalViewOrigin_mBB25EF7C00EDE722B87AA57174B9732FC642BDDC,
	ScrollableChartData_Update_m90A18043BBB0834B143678E5A00C6967F904DEFE,
	ScrollableChartData_HasCategory_m90D552BAF0C3EEE9857FCB3A82EF824F0001E816,
	ScrollableChartData_ModifyMinMax_m44E3BC316E28094BC92A7ACA26125922F9BCE016,
	ScrollableChartData_GetMaxValue_m2DE0E70B4A9F4AA4351F8C2CB1FD461C3213CD63,
	ScrollableChartData_GetMinValue_mF4006D0063CDE7411A14E5FB18276478C0B1CDBB,
	ScrollableChartData_RaiseRealtimeDataChanged_m5A94B2C27F0F9954476D9DA71167CFE570CBF735,
	ScrollableChartData_RaiseViewPortionChanged_m150466DB78E2C9D348746B4E90EC4013D75E89E6,
	ScrollableChartData_RaiseDataChanged_m3065149898B001AF48C25F8EBD215AE6AFA4F81E,
	ScrollableChartData_RestoreDataValues_mC08159937DDC817D985D3BE25440CFD02008D5FF,
	ScrollableChartData_RestoreDataValues_mAE23047F02547B15FE2A7CB12520AD63E33C4B92,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_AddCategory_mBACFB9E256A44B81A7A2CB1BCDC3D26A726CD83C,
	ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_HasCategory_mCB3C025D5035D28D1391E931E880180B6B02699C,
	ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_ClearCategory_m790358592BA70062489A71FF4B8EE87F84212634,
	ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_AppendDatum_m7A49600482E18B2D9B291800CF1CD4A3CCF90031,
	ScrollableChartData_ChartAndGraph_IMixedSeriesProxy_AppendDatum_m2DC78CFC42052D8D010A6AB506948FC00BEACE99,
	ScrollableChartData__ctor_m3898019CFC0087D9FCCF25D9772ECA70A7977B2B,
	ScrollableChartData_U3CUpdateU3Eb__47_0_mE8BFC1F2E09B15FAC7A0D7B71CD96F8B7C2105CE,
	ChartDateUtility_TimeSpanToValue_mD85D68BAA537194C060028AED8770291C9DA471D,
	ChartDateUtility_DateToValue_m547E8C8A33A2AB7168FA07D58170CF34914EDA6C,
	ChartDateUtility_DateToTimeString_m0CD54CFEC440E4B60B3A759B94D5D93387C971F1,
	ChartDateUtility_DateToDateString_mB8AC06BB0C6D4C47805321422F6A4DCECF759A2D,
	ChartDateUtility_DateToDateTimeString_m7A7968FE192BC3552607BA61C43E19FE8101C86B,
	ChartDateUtility_ValueToDate_m03E541A8CAE9CE7C2958BE375484432B3F6A449F,
	ChartDateUtility__ctor_m4297D4721F1BB503159B1E130A94124B797BE24E,
	ChartDateUtility__cctor_m2F4DDB7A3CE5001AA50D12B7E0650FD2BB4DE584,
	ChartDynamicMaterial__ctor_mC3379C8307242316AE5A02C4220C0B5391F0CDDD,
	ChartDynamicMaterial__ctor_mD29B0B9C6654060E87FD8DC034702D28B88BB1B5,
	ChartDynamicMaterial__ctor_m70B79AA9472A38B4263A19EA51F17B5B58400BE9,
	CharItemEffectController_get_WorkOnParent_m903EB04E0D1C9340CF5714BBC942750CF46CAB3F,
	CharItemEffectController_set_WorkOnParent_mB59EB8AF35F52B149CDB79C484084DC7DD101D89,
	CharItemEffectController_get_InitialScale_m407352A2708523959EB60767979C30437DE9B6B7,
	CharItemEffectController_set_InitialScale_m90D615D3B6E717032E683EEA0E1A9D7B27B8CD36,
	CharItemEffectController_get_Parent_m4E2198849F5E8F4461AA7240E2A1C6E2C791604C,
	CharItemEffectController__ctor_mAC6A6CFD7A1AA22A910A6ED0E4E3635750E70E7B,
	CharItemEffectController_Start_m42E8ADC4ECE6095C48403CC3EFD8676B6CF85706,
	CharItemEffectController_OnTransformParentChanged_mA1024CE4DEF92E5E95A0A88205391BB16D793655,
	CharItemEffectController_Update_mD46D24DC52B2E733F69160430C86E04101AD1065,
	CharItemEffectController_Unregister_m57EFA9B3CDC178C118105FE52419B62156A862CE,
	CharItemEffectController_Register_mF0611912B318AE501CE3E0148DB51B4B66BDA98E,
	ChartItemEffect_get_ItemIndex_m4340775567B35B857FCA0AC7F9E145B82BCED8CA,
	ChartItemEffect_set_ItemIndex_m2690B655651FB9CC9E70E2966F476CCE9B072314,
	ChartItemEffect_get_ItemType_m043287B6B47E2BC931B19B9F70D9F40B3EB08A0B,
	ChartItemEffect_set_ItemType_m99ACEB65F5F84DDE284947F6A251D9E3DD8EFD91,
	ChartItemEffect_get_ItemData_m325E440723EC2C55FA58947F2D540423860308BA,
	ChartItemEffect_set_ItemData_mB74438B594B425E6943D0A8E06078CBBC7A24227,
	ChartItemEffect_get_Controller_m454FD7C44623E47515EBA523C68BD5A109D90AE4,
	ChartItemEffect_add_Deactivate_mCED293771E82E147D0842EAF25B06F06810864B7,
	ChartItemEffect_remove_Deactivate_m3A9E4240833B71B8DBB7D2F4F51654FD1DCEFA2B,
	ChartItemEffect_RaiseDeactivated_mFF0196CCB2545FC6F28EF2ADCFE715447ABD25B2,
	ChartItemEffect_Register_mFAA04D751E14B227542F9B9C65E7A8FE6BC1C026,
	ChartItemEffect_Unregister_mEC134C063511E588D322E7144198AE8B7BA0C146,
	ChartItemEffect_OnDisable_m486469A47AFCFB1CE6075F679AF090E00CB99F4C,
	ChartItemEffect_OnEnable_m5B073C831E3ED7B3C28C7BEA3CF4891D2F6E1201,
	ChartItemEffect_Start_m9451458A361B8C4303871AB087F99C63B085C4AD,
	ChartItemEffect_Destroy_mE8F4F293BB6F63D8559879BAD9DAFAAEE0CC146C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChartItemEffect__ctor_m21A2EFA5839620F05EC02DAB1F02FBBE1F2ED56A,
	ChartItemEvents_ChartAndGraph_InternalItemEvents_get_Parent_mECA63081A5FE2EE69594C704534C160E314F5E14,
	ChartItemEvents_ChartAndGraph_InternalItemEvents_set_Parent_mED7B90B0ABF0D0AD54D210B6EF2EF321ED95169C,
	ChartItemEvents_ChartAndGraph_InternalItemEvents_get_UserData_m6EF2B0D7B1D95E7A95C0DF1058DCDACF2CF8CC6E,
	ChartItemEvents_ChartAndGraph_InternalItemEvents_set_UserData_m621FB1CF8E06BB20C649620198743712D90D9F6F,
	ChartItemEvents_Start_mE542B9E82FE8C580A516D3173005D3FF5A917CF1,
	ChartItemEvents_OnMouseEnter_m5A5461805B7058537205A9FDA98E540C15B65ED7,
	ChartItemEvents_OnMouseExit_m585425B7D3171A785BFA89829EE49C1551D1CE62,
	ChartItemEvents_OnMouseDown_mFEE27957BFBAD20A186C9B2B02A6A83B5A382B32,
	ChartItemEvents_OnMouseUp_mFCE6CB3CB398117A24F1708D54050C4BDB058D03,
	ChartItemEvents_OnPointerEnter_m390802A192708FD9B566664006C12DD1D1FF2B03,
	ChartItemEvents_OnPointerExit_m47A5C30AF5E7BBD5C48D6F720DB5202103E8B2F0,
	ChartItemEvents_OnPointerDown_m94532FF8CC379E7E963B7B3F1D5F67BB7F2BE2CC,
	ChartItemEvents_OnPointerUp_mCAECD2912B1B5B6446AFC0C45EC2B487AFFD3B22,
	ChartItemEvents__ctor_m956D57F6AA6A1293701D708FCD067087E7A738A2,
	Event__ctor_m86A64E93F5FBBC5A1D212867A4540C1790CD8A45,
	ChartItemGrowEffect_get_ScaleMultiplier_m75CCFCC77307742DFC9143D61EAB0E056504CC1A,
	ChartItemGrowEffect_get_Rotation_m8E1E848F17AACB81BA4BFD8DFA729EBD4A68F0B2,
	ChartItemGrowEffect_get_Translation_mE5B4C7E8E687CBED8FEDB905D7A3D663839EB3A3,
	ChartItemGrowEffect_GrowAndShrink_m01AAC4E599D8441664FA9586583F19ABA09F7A2D,
	ChartItemGrowEffect_CheckAnimationEnded_m1013DE42CEC05A3EE6FCAC26733A862C4493D3F2,
	ChartItemGrowEffect_FixEaseFunction_m7D49D1ACE24077326B30E4A085B80CF1548BE3CA,
	ChartItemGrowEffect_Update_m7037F22999F52D61304A0060F17334B71B371689,
	ChartItemGrowEffect_TriggerOut_m2785B42BE0C5C58EA0CE38E52B172E4561443527,
	ChartItemGrowEffect_TriggerIn_m6DCEA4FFEE8EA504031DBE1CDA509A0D60D22694,
	ChartItemGrowEffect_Grow_m0D858BB1F561E19FB9B93DCB41AB49FE830324B5,
	ChartItemGrowEffect_Shrink_m136276F173A3A405E50E7D18F61F48DAD4A153D0,
	ChartItemGrowEffect__ctor_m7F877E0F472C81F23960361D3749B836AFFEE793,
	ChartItemLerpEffect_Start_m0DC1B7673756AFD7134570D30A15C9CD004182F2,
	ChartItemLerpEffect_GrowAndShrink_m153D418E580A229DD98DDEF1F98620C0B5BD6B5A,
	ChartItemLerpEffect_CheckAnimationEnded_mE216026304EC9D113E47C196039D3CED5B83A4D6,
	ChartItemLerpEffect_FixEaseFunction_mBAF0478E17EB01F1D1F98F01859E732D1B64FAD9,
	NULL,
	NULL,
	ChartItemLerpEffect_Update_mA7E24BCDD0A68A14FB720830715CE6D2EC3624E2,
	ChartItemLerpEffect_TriggerOut_mB55B91C9D1E281F55BF13351B3274683A06F94E4,
	ChartItemLerpEffect_TriggerIn_m2A0DF1A074E61AF8CF038E1A94E1C3004878A822,
	ChartItemLerpEffect_Grow_m043C7B11D33E747E6ADAC01FDCEE9F003A55C062,
	ChartItemLerpEffect_Shrink_m67ABE2EA648C5C8706D13691F1D6FCACC9A03B09,
	ChartItemLerpEffect__ctor_m433230ECF02C3C0EA75A18E52A5E78B7CDE0442F,
	ChartItemMaterialLerpEffect__ctor_m4DD636FDF3FD3D3FDA12B1D37BD6451CC4E0E17E,
	ChartItemTextBlend_Start_m3214B3E0224A33FF98F796B3B5E027F8194E887F,
	ChartItemTextBlend_get_Rotation_m4076F9EDBB3EF837A354F69D25873415E7A30758,
	ChartItemTextBlend_get_ScaleMultiplier_m136015C8A05CB9AE9547107665D6770AA8E19F1F,
	ChartItemTextBlend_get_Translation_m5059C00742F62D592B1E5372701F0A00FAF73533,
	ChartItemTextBlend_GetStartValue_mA549C99ED0C8F5B4B534C28F82B6AB1B5531C82F,
	ChartItemTextBlend_EnsureRenderer_mBD0250B1BC76A6E7B6561AA0D61D191A3AAED13D,
	ChartItemTextBlend_ApplyLerp_m8212C2E275DF4A4C99E8684C5413CCE9FB8BDA06,
	ChartItemTextBlend__ctor_m88335F931D17990F5161C6FD8E20F867ACDF4A62,
	ChartMagin_get_Left_m8CA3693F74477665EBC79AA26FF6FBFC4A00DF75,
	ChartMagin_get_Right_mD8761085F81F2AF1D421324FD92D98D146C52E2F,
	ChartMagin_get_Top_m98A43BC03361E0A3E97E6DA5E85610206ABD3F10,
	ChartMagin_get_Bottom_m44249037E523659C0F58D7D5135FB1CE247D8B44,
	ChartMagin__ctor_mEA93AF91A96ED580128F4E66524C2DF5DB4FAC5D,
	ChartMaterialController_get_Materials_m7EAB1C6A4854E373BA933345EA69308D019CC858,
	ChartMaterialController_set_Materials_m2B7C3E7A038C61336958F47508AAD1DD364C1051,
	ChartMaterialController_SetMaterial_m65A13F6A406ECC2FA670ADF3CA9FC0A3275427C5,
	ChartMaterialController_WarnNullItem_m69F7A0B523EC21E14DFDE60900A7ECE0E9CF0725,
	ChartMaterialController_Start_m386114D5E91BFCD5FAE539C42981FFB10CF29D28,
	ChartMaterialController_BaseColor_m4B8E58F3ABF69566AEA9924A70CE06E93DD676CD,
	ChartMaterialController_Combine_mE8583C0AEEA0C6D56432180AF81580C084BDAD73,
	ChartMaterialController_TriggerOn_m07B21E0266927C4A1DDEE309A0ADC1B2A6856D2B,
	ChartMaterialController_TriggerOff_m2AE349CF58485061C1DCE395FF13CAC6F1451AC8,
	ChartMaterialController_OnMouseEnter_m4DFC3C2F6D1C343EB84730D59D62B80B461B9DA7,
	ChartMaterialController_OnMouseExit_mAAFA7E23180D9047C4CBAA85CDCB738AF3CF4FC3,
	ChartMaterialController_OnMouseDown_m2A331BC2A53E85FC231D7A874499A56499F6F533,
	ChartMaterialController_OnMouseUp_m0F86B511344328B6E45587F58673F825743A91BE,
	ChartMaterialController_Update_m950BF825350A09416A274A6A8983752829482A54,
	ChartMaterialController_GetColorCombine_mA3E9B5F8647844E6B7A5225CAB1718074508664A,
	ChartMaterialController_SetColorCombine_m980339BCC56F6ACB14773FEB7BB7565BD3C94EEF,
	ChartMaterialController_SetRendererColor_m1A3F4B95F49DDA77039BE038AAACFE09588F1C70,
	ChartMaterialController_SetColor_mABF0F402FA0B9A141F37DDC238FABA28B1DCD25C,
	ChartMaterialController_OnDestroy_m34AAE9210F8F7A6C6F73499C97CFFD3F49823E1F,
	ChartMaterialController_Refresh_m824EF89EC19EC5A5E9BAF1EEAB11FBCE8F060167,
	ChartMaterialController_OnPointerEnter_m9470E5C6A0614561F054C72405D51D2B84A4101E,
	ChartMaterialController_OnPointerExit_m30AE6EE053B099B2572A1F1EA55AA200C020F9B6,
	ChartMaterialController_OnPointerDown_m5A2933134BA392EA267DA288C789A519AC55E56A,
	ChartMaterialController_OnPointerUp_mFC83BD22D26BDDBF8855DF1107B4EB74F0ED1533,
	ChartMaterialController__ctor_m2B0DEC5EE987CF6D0CADF1602FD76C91E688D70E,
	ChartMaterialController__cctor_m8FDAE74C2457745CA5BF9272A6C51C43BC551F11,
	ChartOrientedSize__ctor_m98C035FAED28B3CE37A8F31CA1D2A8A248D418AE,
	ChartOrientedSize__ctor_m56EDC540CC2E2022AC03BC84E8FB1CD8127962CE,
	ChartOrientedSize__ctor_mFD810D916A5E98DFC2B07834697E8FA86BB5E59C,
	ChartOrientedSize_Equals_m086B4EB35A945721AE66C5E1B402126C8D6727BA,
	ChartOrientedSize_GetHashCode_m1F9C3D954472412433C65756C25280F4CF3E01F9,
	ChartItem_get_TagData_mB309D7BE4D449337386A3F11CC5B1E5461622DA9,
	ChartItem_set_TagData_m6EFCAD89C8BFBC7370A001CCDD1446856CEF193F,
	ChartItem__ctor_mA87CC828C72E990292B8C9FFBA71D9C7F68B9880,
	ChartItemNoDelete__ctor_mE4B12DDA0BFA41D20EA4403BE4EA7E118E12CC43,
	DoubleRect__ctor_m1F984B01FA3C146F2E44687D00A5555DE0FB1788,
	DoubleRect_ToString_m919CD47E3D21F81F9FBF3139392A42ADD1C14114,
	DoubleRect_get_min_m2DAC7EA9AF9EDCDC7C8AEE363E235019B55143BE,
	DoubleRect_get_max_m9A803BD921EA1E942317AAA8E0D785412FEE7D1F,
	DoubleVector2__ctor_mB07435411E7635EB6F0184A9CA371FDE8929F41A,
	DoubleVector2_ToVector2_m9F6043976FEBC38B285B63682A15F24AA6DF1393,
	DoubleVector2_ToDoubleVector3_m89ED5CDEBCE31F542002701BEACBAE3002F8D9F7,
	DoubleVector2__ctor_m49A34EB80CA262CEDACCA8A80EEC90973CBFFD0C,
	DoubleVector3_get_Item_mED8C18858451EA7064C2F46FA07E957C2644ED0D,
	DoubleVector3_set_Item_m924522C3E1567A827FCD1CA497289495C98A0AED,
	DoubleVector3_ToVector2_mCA8A08430A8A865321E15246F5843E2049991874,
	DoubleVector3_ToVector3_m536D90101A2050B94772A68DADC79B9EF6A76E95,
	DoubleVector3_ToVector4_m7E82F42A506E67800A6B46BAD15159908CD6F12C,
	DoubleVector3_ToDoubleVector4_m465CDD4A9232979D4AB4F55D349DC46C47A0D70C,
	DoubleVector3_ToDoubleVector2_mDF162BA352854D9C6A3CC52D9C504FEB4CA3F48F,
	DoubleVector3_get_normalized_m90F18F73D68A8B15215A499AF2C5639423485E5D,
	DoubleVector3_get_magnitude_m5506849C53E5CEB754AE27A8BA2D19378010B921,
	DoubleVector3_get_sqrMagnitude_m88FEE9BC6FA36A803CB71EDC6EBA6AA1DBB88EFD,
	DoubleVector3_get_zero_m4BA0DAEF6E6A5E374903A0D3E8938D8C5F1EA8F8,
	DoubleVector3_get_one_m89706A68BF608B9F61D3B481D38C99077F4C2E95,
	DoubleVector3_get_forward_mA5EC8F787B4A77039CC8EF4D7530791FC6A54084,
	DoubleVector3_get_back_m971380F28C21B3BC4CD39EFE1EB61E18E9C65ED5,
	DoubleVector3_get_up_m8E7B5BEB4E1F752F6FC39AC8449032CF0AA3BEAE,
	DoubleVector3_get_down_m62F40AC480B022A58D0F1E4F8E1E8CDE4BD8E4E8,
	DoubleVector3_get_left_mFDF3F2E3E1462E22D86C717A53DD607B09E73AA6,
	DoubleVector3_get_right_m5AABED8AEBDF09F6A89403CE8E0E99F5C8C4A1DF,
	DoubleVector3_get_fwd_m2B40844B46506803F63912F97875F840237E1A1F,
	DoubleVector3__ctor_mFB68746E3A4C58C0EFB00117654573FAA1486902,
	DoubleVector3__ctor_m7E898AB882BD1B76DA5C242BC4C1D752EFC275C9,
	DoubleVector3__ctor_m64CA35E9656B1D9020E4D0006D2F9182C3C8332A,
	DoubleVector3_Lerp_m77D44244DAB2F1BCE647C226FA8F9CD07B992C6E,
	DoubleVector3_LerpUnclamped_mE0B451B62BE86B2011B4A389038798622BEB3910,
	DoubleVector3_MoveTowards_mE5FAA9C8DE0BD2E6F283578CDDC4D6D887F12319,
	DoubleVector3_Set_mDD93E64C2D83554A00D45AEDA8E53BA8EEC9A461,
	DoubleVector3_Scale_mB6007A14B1B464F5303C7E2FC2AB39A838E37D30,
	DoubleVector3_Scale_mDE9D2DCDC9D7E9781D938FC8796B5E924F0EF440,
	DoubleVector3_Cross_m2DCA01528D7E66FE3D4282BD8EB4F4557F0DEAD1,
	DoubleVector3_GetHashCode_m6B0B3D669BFEF329F363FACBABF80EACCC032811,
	DoubleVector3_Equals_m5C3BB2A3751484CE3672E5F49E9C554E9735D32C,
	DoubleVector3_Reflect_mA6272EFD7A04C90F9B19AFDADDDF1CBDD4E3D4DB,
	DoubleVector3_Normalize_mEADC1A8785C72FB3EA37951B851CBFCF6D71EFEF,
	DoubleVector3_Normalize_m6BAE2E860F8AF017BA5FA679637F17A5316186A8,
	DoubleVector3_Dot_m9C5D7B990969624D2575EB1CC92279FC95F52C86,
	DoubleVector3_Distance_m4FDA1C1EFE1C2011F69CC9937B550652B412F222,
	DoubleVector3_ClampMagnitude_mB83103B06A4F891F1FE1D78FD553EF140E279A73,
	DoubleVector3_Magnitude_m2367A75F8FF05DD750C6433929590962F381A29F,
	DoubleVector3_SqrMagnitude_m59ECB08B990EA531C4BF0D8D7415CDDA1384C767,
	DoubleVector3_Min_m8A293E1EC95DDA10E7062878006594705DF96F2F,
	DoubleVector3_Max_m8229C769E26645D5B1AEEA06EB446463FB0EBF85,
	DoubleVector3_op_Addition_m33076677BD53CD9EAFB6593C813E8B9690789FAA,
	DoubleVector3_op_Subtraction_mDD74D64CAFD7EFE78E369115C4A566E01DBFCB13,
	DoubleVector3_op_UnaryNegation_mF11C867369B7D7DC7160AA0519878902DF6761E9,
	DoubleVector3_op_Multiply_m9285A5D4A034C4AF15DF67493B5D84E799366A8E,
	DoubleVector3_op_Multiply_m93BFC6CE511BA9CEE95BB59BAB48BDD7063A71DB,
	DoubleVector3_op_Division_m579F6EEE6580CE5B26C327845F66C12A3F98B279,
	DoubleVector3_op_Equality_m086C68AA3377E5948901E2E2905842C2504C02B1,
	DoubleVector3_op_Inequality_m20100A2A0E8E9D59AE702050EB723F5BCEB6F0A2,
	DoubleVector3_ToString_mA764FD3E053E52D53B2B368E410DE1AE2544211B,
	DoubleVector3_ToString_m4D85420392458F4FC660D1F6EAA01DAB3B0001F9,
	DoubleVector4__ctor_m1A8D3B2BCCF3B9F09BAA42BC481A1ED0B6E1F58D,
	DoubleVector4_ToVector4_m15954E35D363A12FE2A653263AE9AF726E3BC7FE,
	DoubleVector4_ToVector3_m89062AE3A11AF7CE2A02B01A75413B4401AC42C0,
	DoubleVector4_ToDoubleVector3_mE4450D748956E1BF8E4315E2D3658C2B1687C581,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CustomChartPointer_Awake_m052D5994DDD4D359FB7777FAC690F3AF39A13BEF,
	CustomChartPointer_UpdateGeometry_m2DAE70E5D1B8CAEA78122FB0A4330F38F3A73B7E,
	CustomChartPointer_OnPointerClick_m265D470D918F34E39A15BB8ECAB2B9F2D65B778D,
	CustomChartPointer_OnPointerDown_m3D27152BC8B90AC415D82AF4959F9A71893E4291,
	CustomChartPointer_OnPointerEnter_m36598D981FF59CDC8782271D11CBBBCF91E3A545,
	CustomChartPointer_OnPointerExit_m03DE6D6399EE4555B3AE8096D74C46A66A49A7C8,
	CustomChartPointer_OnPointerUp_m9D4D817FF7647F21B8CAC99B69FDA2A226612B8D,
	CustomChartPointer_Raycast_m26C8BF8276F6D16A0A8A07F96F9B4351D16E140F,
	CustomChartPointer_LateUpdate_m06D699311DBFF394FD7B1790385C949B590BDADC,
	CustomChartPointer__ctor_mD5F5B26170AE9D0F6B829DF1250AC7B2351BF490,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChartSparseDataSource__ctor_m5E17A577E752319F0CE300F62A9B376BCC207474,
	ChartSparseDataSource_MColumns_ItemsReplaced_mB60609E59B1DFBF54CE25B73B6BFAD42CAC5231D,
	ChartSparseDataSource_MRows_NameChanged_m8023AE635F2E9F2CFF0F3110E3C7ABBEB4E6DE4C,
	ChartSparseDataSource_MColumns_NameChanged_m404ACDCA6662047A945517DD2B378C31BEB98911,
	ChartSparseDataSource_get_SuspendEvents_mD9C19B139AA3ACACAF5225680218715C53F8824B,
	ChartSparseDataSource_set_SuspendEvents_mA143F18FB951C3BADAC9CECC2714F1AACE315470,
	ChartSparseDataSource_get_Columns_m2CF71F7D09116A06159513F6E25E7F1A9E9F7F94,
	ChartSparseDataSource_get_Rows_m587C7FE2A232468EAAD75680206636B8E4AFB959,
	ChartSparseDataSource_OrderChanged_m71BF74537D1BB2488BE75AF099B0EBFC56275DC8,
	ChartSparseDataSource_FindMinMaxValue_m746CAB84E6F22F442D6EFAF8593CCE93261BEAE2,
	ChartSparseDataSource_Clear_m4C5CD86FEB212BA9DB402B730A84FAD2982CF690,
	ChartSparseDataSource_PrepareRawData_mF1E8B75B50DCAD74CCD2D8E49C00CA83FD852319,
	ChartSparseDataSource_HasZeroItems_m54604D47DE9B10A3D8B64BA9E810EEBA266404BA,
	ChartSparseDataSource_getRawMaxValue_mA44A0F02039FF4515A440D1BDC21033C33F4773D,
	ChartSparseDataSource_getRawMinValue_m0714C0882E54D9054B5587FA241C4562B59C59B5,
	ChartSparseDataSource_ItemRemoved_m0A443FC378C57DB4A0F4A67D1F2FE84AD3DF3709,
	ChartSparseDataSource_Rows_ItemRemoved_mC0E30E1A3D4BFA1B72EF486A1C4278F1D501BE59,
	ChartSparseDataSource_Columns_ItemRemoved_m4A12626B6748F919F0D83557B14AA623D25BCDBB,
	ChartSparseDataSource_EnsureRawData_mBA98C75670E5993115C984D47FAD367D59ADC134,
	ChartSparseDataSource_getRawData_mB2B3AA133EE32E6859090D0577C60A38B25205BA,
	ChartSparseDataSource_VerifyMinMaxValue_mB9A6B84E0177AF7BFC38A55FA5B416C00258128E,
	ChartSparseDataSource_InnerSetValue_m1D9FDE998CCA943E5D9DEEECCA276D04F20C67E5,
	ChartSparseDataSource_InnerGetValue_mB4440C8CD80E98FD7E98AC71D86E858754AC744C,
	ChartSparseDataSource_GetValue_m5A54768FB6583676D241FD7FEFAEE87E0D4B003E,
	ChartSparseDataSource_GetValue_mCFAAAC68FF595BEE04886E97168D223D292F2F50,
	ChartSparseDataSource_GetValue_m3814BE95B7AD48D97B333C0B6C0DD9640D81B1DB,
	ChartSparseDataSource_AddLabel_mE5212C0736692290BAE152CC533AB9A216F02681,
	ChartSparseDataSource_SetValue_m2867624C3DFCE779EA530B27C353A2E9AB440B8C,
	ChartSparseDataSource_SetValue_m59518872614BD12DF9CDEF99AF167A7ED2BC7B7C,
	ChartSparseDataSource_SetValue_mA8FF4E271E839E7C6BF41613754472DE8B74FFE0,
	KeyElement__ctor_mF495D25615988DEAF9D93439848D890FB358E5FA,
	KeyElement_IsIn_mB96974CD61E546BD694B850A130C84598FCAE0CE,
	KeyElement_IsInRow_mDB9A5D4E921914DA9615454A3ADF273EB4B0CB4A,
	KeyElement_IsInColumn_m37197E408CAD2E333AC1979A6FBE45DBCE5988E9,
	KeyElement_op_Equality_mB13C2C5C98BCD00182598A8061509860E4BFE523,
	KeyElement_op_Inequality_m8BE683A6D0114E38378A9E9D3CC3CD149048DA84,
	KeyElement_Equals_mB8633937E6EAD804F05153CFB62D57457E59F27A,
	KeyElement_GetHashCode_m68EFF0FDBC838A8B7E151CDED36F50DF849259A4,
	KeyElement_get_Row_m8E5B2536ABA3F7963BED8C00C37AD9DE246E4182,
	KeyElement_set_Row_mA0B59C94D055895B004E338AD361C4228FC37AD0,
	KeyElement_get_Column_m414AFFA313E39B3AEBAAC5B8DEA3A2E483F69999,
	KeyElement_set_Column_mFD48761D666FCDB501D3C5C79F3FCA47DEDF6D8C,
	GraphChart_OnLineSelected_mA5EAEC051274D304B96E03D7B0C454B1F9B7128F,
	GraphChart_OnLineHovered_m8A7098A585466DFD806D740B095B4B9D78E9F82C,
	GraphChart_get_FitToContainer_m77A4781E39E69CBCFD0FE3FE97B57D7F0F88C53E,
	GraphChart_set_FitToContainer_m4A01B3112B8800ECC6040D7721FCC708F8C803E8,
	GraphChart_get_NegativeFill_m442C189CA82D329BC3B48DFE74CCE06DFFA3B653,
	GraphChart_set_NegativeFill_m74832D2F59983EFE8CDDA53EB8E0A9C6B6083C12,
	GraphChart_get_FitMargin_mC29E8E2E64D13E60CF79B14001A2FFFD856ADCF6,
	GraphChart_set_FitMargin_m6C859E6588F76E141F4DBB786B6CFCC3E5A2D47D,
	GraphChart_get_EnableBetaOptimization_mB9776E81B83F789993D0DD9947767416BCDB62FD,
	GraphChart_set_EnableBetaOptimization_mDBD21CC52BAB9F25579CB6740074BFB8901065D2,
	GraphChart_get_MarginLink_m006682A84831559AD0EBCE004FBF0EC8C3A060D3,
	GraphChart_get_IsCanvas_m13979112AA7DAC2B4F81DA942F2A12DE7C7C6C84,
	GraphChart_get_SupportRealtimeGeneration_m8039BBD28EBC86B41DCB0A7D4C3C460A02E14BA2,
	GraphChart_CenterObject_mC1E8E6AE1A4CD77ED6A6C806734D34F368D5743E,
	GraphChart_CreateDataObject_m4DBD420AEB8476674DFF1025954738D633E71862,
	GraphChart_Update_m6DDF3DDCFEFCBA8562C9A15E8DA71BCE395C6B7E,
	GraphChart_ClearChart_mF2AB0835EDDC8CF4CEB0F039D3F0B8BCED8B95D7,
	GraphChart_ClearCache_m6C0EB9BE07411F6693C04FA5F4076D66FF62FA01,
	GraphChart_GetCategoryDepth_m49347F83748E35EF8BFFC8CDD0294E3E95F5525A,
	GraphChart_AddRadius_mD10AD3E4F8CFCB45FC9C620E8ADA8AE5BA63F951,
	GraphChart_ViewPortionChanged_mA161DCA5BA702C5FB7DDA65A588AB59757D8B3B8,
	GraphChart_GenerateRealtime_m5F944C04426B29D8DF126D4F1D48F85472C14D9C,
	GraphChart_get_ShouldFitCanvas_mAEE877B4DF762551CF746E62669E62ADA5D505FA,
	GraphChart_get_FitAspectCanvas_m5521185E681B053AC21AFF9F520ACE2AF4019BD4,
	GraphChart_InternalGenerateChart_m2FE442227DAE4872202E848D0431E97C3EB75882,
	GraphChart_Dots_Leave_m3BBE3E29DDD1A9A3859037BCAC75F82ED5F15108,
	GraphChart_Lines_Leave_mA053E690FD3A938A438E81484E0E5CBB78B541CD,
	GraphChart_Dots_Click_m24253BD37D184EFE55B2A2DD4C881700518CA62A,
	GraphChart_Lines_Clicked_mA96C530C8ABDF5F0327ADDFF29E84C9DCB051C50,
	GraphChart_Lines_Hover_m74A19A0F7FF1AF78F7A0C931BA8CEF1E0E7A2034,
	GraphChart_Dots_Hover_m369B08B0BDB317C3BD668E3E50B33625E51BA4A5,
	GraphChart_OnItemHoverted_mCDF48D540275EF8F9986F72C83A4D5128FB51EC6,
	GraphChart_OnItemSelected_m32393A801205F307B29910C921B0360C143FBBF2,
	GraphChart_AddOccupiedCategory_mBC04323715A1E22CDDD5BF5D0BB51EC261929401,
	GraphChart_OnItemLeave_m16D729ECD56E1B1CEB9802F7CAAE79E18BB4B4A8,
	GraphChart_SetAsMixedSeries_mE9A526FAD07CCC6D4FBD3C80EC53B98870BC0701,
	GraphChart__ctor_m3D127C0E87610FFC3230A47FC4FDF88F91089F11,
	GraphChart_U3COnItemLeaveU3Eb__58_0_mA557CB16D6A2BA64937454DEDF3ADD5FECDB6FFF,
	CategoryObject__ctor_m4107A0C15093D3A07B5BE2C8F6D7351EA1EBF0B4,
	U3CU3Ec__DisplayClass48_0__ctor_m7D7B5519A450BA1B4BE387755552F027BD238EF9,
	U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__4_m78F30EF8FDBD89708A5D922A5895833CF4CD1ED1,
	U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__5_m6B7E32379767DF99C996EE88971B339F86ACED70,
	U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__6_mA6CB1B1216F1A5A290E591B519121609917AFA01,
	U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__0_mE8CBB50E46CC939E724174C96F8F980A961E49CE,
	U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__1_m14DF60523098941CE43D5387F88AFAA60B89A2B8,
	U3CU3Ec__DisplayClass48_0_U3CInternalGenerateChartU3Eb__2_mDDFB6B19924C3AC9169C0352DEC4E1463C297681,
	U3CU3Ec__cctor_m0C68B0F02946F73B550AD2B9C17E6EF00E2E42DC,
	U3CU3Ec__ctor_m4AD90B5A3A238FDE194C669B988F51F97109C5BA,
	U3CU3Ec_U3CInternalGenerateChartU3Eb__48_3_mEA87D6D36DF69129E35B8148FEE9A84E9FEC8319,
	GraphChartBase_get_HeightRatio_m957D24B07EC9DD15BE62DB27B0C5D09DA69871D7,
	GraphChartBase_set_HeightRatio_m3215A2F82486DD9536A79D7A637E918576B165D6,
	GraphChartBase_get_WidthRatio_mB72FEA76D5FC92F56FDA7E236808DDDF4917F66E,
	GraphChartBase_set_WidthRatio_m1C659060B32F8AA80985121F1836CACB6F33867E,
	GraphChartBase_get_ItemFormat_mE9B7C0621416387221E63DC07378F42AB57E8B07,
	GraphChartBase_set_ItemFormat_mA6181A9070016622898F15F9304B88682035E650,
	GraphChartBase_get_DataSource_m43FA715097E55E3413866C6AECE8D80B3581B80F,
	GraphChartBase_get_LegendInfo_m8DA296AE5601DD8F15298FFDFD32D1C56C4BE6D6,
	GraphChartBase_get_DataLink_m474A46C8258AB257282F1187D516C8F741D6016F,
	NULL,
	GraphChartBase_HookEvents_m6A2262685F100982489EB51E3E9D1E1ACBAC9125,
	GraphChartBase_GraphChartBase_InternalViewPortionChanged_mDD6D74CA30A30A28CD459B65A5B0F250C506E724,
	NULL,
	GraphChartBase_GraphChartBase_InternalRealTimeDataChanged_mE3DC6C52EBD03E1FD6151916755E7D2A07A7C00B,
	GraphChartBase_ClearRealtimeIndexdata_m4D5590BB504B284A40A2AC38C36D8A2797D42F2B,
	GraphChartBase_Invalidate_m74DFAF53F1AB5260A488CF4D6C2A6F6F1EACD6BC,
	GraphChartBase_GraphChart_InternalDataChanged_m28446CC735B69D4E20DFA04E092298A3ACB2CF67,
	GraphChartBase_Start_m2481135A37D1A74D317A4C64280F593B9D8BEF99,
	GraphChartBase_OnValidate_m5498B442A550D684E7EB10F96EDD46DC9416C5C5,
	GraphChartBase_OnLabelSettingChanged_m5D96A0DC501B63DBBEEDB68A3A0B0B38DDAE0E69,
	GraphChartBase_OnAxisValuesChanged_mBDD8DBC4831DC2B27ABCFC9E5872FF85D70FEA64,
	GraphChartBase_OnLabelSettingsSet_mF890CA0E077E38B634E45456F83B7572DF7CBBF2,
	GraphChartBase_TransformPoint_mAB83EEF6C8FDFEEF57359D07D1A0779B02EF70D8,
	GraphChartBase_Update_mF5E975021EE660FD9F57A98546E916CDE2E2F7FD,
	GraphChartBase_UpdateMinMax_m7F1CF952ED57CBF089D75C1B6CE3FFFF2A07991C,
	GraphChartBase_GetScrollingRange_m2724ECDF4A1688763A428DE4A3333C2DFC269216,
	GraphChartBase_CreateUvRect_m0D1370A12B08E7A767475CF6F58FAE80C77A686B,
	GraphChartBase_ClipPoints_m751E589D50E95062CFD1B08AA5B24A46218CD038,
	GraphChartBase_TransformPoints_mDE04BEEB8BA94163362DCE5C220CB6A836D9D286,
	GraphChartBase_TransformPoints_m2420E7CA118A3EB2F3C5DFB645397AADCAF5374A,
	GraphChartBase_ValidateProperties_m71D044C9EBF998D5E0B47D70A3831AF64A424CFB,
	GraphChartBase_get_SupportsCategoryLabels_m05ABE914F22CDEB867B0D605AB4A9AAB5745D7E6,
	GraphChartBase_get_SupportsGroupLables_m256FCF4262E98F92FF9D3EB8DE9436D14A6EF06B,
	GraphChartBase_get_SupportsItemLabels_m3E7AD6DAF4446140E99C81D009FD95136C0E5504,
	GraphChartBase_get_TotalHeightLink_mC54F8C16C54624F33A4647B630320DBA6A410D00,
	GraphChartBase_get_TotalWidthLink_mB27C39FB0E7AE90FFE4256FB8CBDEBBFDA3063BA,
	GraphChartBase_get_TotalDepthLink_mB370917376EA9AE75AB1079F9CC7973E9021248D,
	GraphChartBase_HasValues_m94F574C967CF53BC734765753C053F1BFC5AA740,
	GraphChartBase_MaxValue_m9C8A3CD7F373D6544B7F5E65EB2EB7A401269E44,
	GraphChartBase_MinValue_m5C70463DA41B056DB0B7F39BA1F742D0243808B7,
	GraphChartBase_OnItemHoverted_m49F6C63952C47E98E3D0DFA4227C524C10E41B61,
	GraphChartBase_FormatItem_mF34CC108CE45F5958B19EC2F6392B4999990CD62,
	GraphChartBase_FormatItem_m7B1AE4BB60B92ACAC6410F84CE3449CFC3ED1895,
	GraphChartBase_FormatItem_mB60D6DD8CED22475BB73A99CEBE9F79275B732B9,
	GraphChartBase_OnItemSelected_m2EF711ACB930629E595D45F8F634D33A91D646AB,
	GraphChartBase__ctor_m842DAF976E645C350244C61CB5EBD797262197E3,
	GraphEventArgs__ctor_m3BA1F38D93D6F358D9EFB9A58916CA53BACD8ACD,
	GraphEventArgs_get_Magnitude_m069084EB44CF3477283C3C24DFE227E060139C67,
	GraphEventArgs_set_Magnitude_mE0A9AC6DF6CF9D6532021ED8F40958FE33612C27,
	GraphEventArgs_get_Index_m6E3E1833F3106A2C697659849B4D4DBB3EA3A834,
	GraphEventArgs_set_Index_m779BF98A0027FBF3AE214D4225424BE1465B3330,
	GraphEventArgs_get_XString_mD8983AC95262E528C933388971991F0ED7FBDB04,
	GraphEventArgs_set_XString_mBE4A7E79289C442618EFF15D4863A9A764735C27,
	GraphEventArgs_get_YString_mD55762099B346C4F5F0DB6C742BFE5659DB94074,
	GraphEventArgs_set_YString_m1E0A09FE20ABD9A34C0361EFF8E294D558475401,
	GraphEventArgs_get_Position_m07FE6D33F9802F166E94A69452252688832F68FE,
	GraphEventArgs_set_Position_m1E11A04C49BB3B49F43F5CC58A11A64234E47E73,
	GraphEventArgs_get_Value_mE3ED67FD7BE31E9EEA4FA9F1C24B3AAEDB363055,
	GraphEventArgs_set_Value_m2AEA5B671D29625A88182451F4D8DE567FD9353D,
	GraphEventArgs_get_Category_m132FB8CA4D23BBD5A6597E4C180168227A203D4A,
	GraphEventArgs_set_Category_m7F525C2F77FBD1DF02655BB29922637BC97CC37D,
	GraphEventArgs_get_Group_mEDE877E6A0267C6C014F063601F3A95873209B3E,
	GraphEventArgs_set_Group_mAF49E4F10D93415BBDB3437597F4A6C03D7FD22A,
	GraphEvent__ctor_m9FB528548A4729C3BF13DC90967E1EC87F4A556B,
	GraphData_AddCategory3DGraph_m70581959401067B2717B860FB97DBD2F6166D4F6,
	GraphData_Set3DCategoryPrefabs_mA26193C759E79F326CFC57B81A71C179D9811602,
	GraphData_Set3DCategoryDepth_m1A5139512BB2778D1452A3F8850B0B981337DD23,
	GraphData_AddPointToCategoryWithLabelRealtime_m3B7FA31C9D93ECCD189FB38A2E2CCE239C0AFFAD,
	GraphData_AddPointToCategoryWithLabelRealtime_mB5E1619495590901C1E50A18E9219FDB93DAC2B7,
	GraphData_AddPointToCategoryWithLabelRealtime_m1ED01A75FBB5172795C369F56916F07D7DAA85B9,
	GraphData_AddPointToCategoryWithLabelRealtime_m68DD81F1B6057B6CD5784510070B9F37E92FA7D9,
	GraphData_AddPointToCategoryRealtime_mD79C08438F5B4EC8ACE6010FF73D0F03F6BE6D16,
	GraphData_AddPointToCategoryRealtime_m3EB3B005E65F3C0FC5404081A447750DE6D22773,
	GraphData_AddPointToCategoryRealtime_m3CBB7BD8F8B9AE1A94D5CFAAD8DC3097D9215B4D,
	GraphData_SetCategoryArray_mCCC0A3787E0F0505719B1CF3453EDC7D4DDCF6D8,
	GraphData_UpdateLastPointInCategoryRealtime_m12632BF5D28A545E44D16488960DB933F28F6FD9,
	GraphData_AddPointToCategoryRealtime_m1A37640184DC9DED0B58E355082CDA5B140E96EE,
	GraphData_CheckExtended_m7C0A0A87C42041BF12DBC35521F7C844703CEB8A,
	GraphData_get_IsExtended_mC20FE3C5C023703211AB7B935E96272F06EA0EFB,
	GraphData_ClearAndMakeBezierCurve_m3A32C3FB2279E688A852D9734D2C4C109B3658FD,
	GraphData_ClearAndMakeLinear_m43270963D3A09786A39FA648B11D642BB28E3ED7,
	GraphData_ChartAndGraph_IInternalGraphData_add_InternalRealTimeDataChanged_mD6DBC6B761BC9CDDEAFA2586D623786008C846C9,
	GraphData_ChartAndGraph_IInternalGraphData_remove_InternalRealTimeDataChanged_m5C43AB570712151B5B4C80FD7A0F3EF02E8A47F7,
	GraphData_ChartAndGraph_IInternalGraphData_add_InternalViewPortionChanged_m44CBE4ABA3B7507847D37543C1364A1C1C1E4B45,
	GraphData_ChartAndGraph_IInternalGraphData_remove_InternalViewPortionChanged_mF2781C9217F5BDB8F358F78558042771FBA3050B,
	GraphData_ChartAndGraph_IInternalGraphData_add_InternalDataChanged_mB0BAECFE09A2289538D382AA9B24CEBD99768166,
	GraphData_ChartAndGraph_IInternalGraphData_remove_InternalDataChanged_mD1109D60445B2C1B65330095A039669D9ADEA79D,
	GraphData_RenameCategory_m7DBF7FC7F78B86D4C698BC4B9109096E1C5CBE23,
	GraphData_AddCategory_mD4A0A2E61345771585742A78831B6086017D1C7D,
	GraphData_isCategoryEnabled_m5C0419EBA253AEBEB8C257A75B07594075B15968,
	GraphData_SetCategoryEnabled_m36849B69303A51BF1CD1052D92A855920D067462,
	GraphData_StoreCategory_m876D0AE4C4560B07AABC5430B1D6DA1D9EDE8BC2,
	GraphData_RestoreCategory_m778A8F078A3CF88354A41A276EF20C8B5DDF1C2F,
	GraphData_StoreAllCategoriesinOrder_m22349780531047B1B532BA3C3643D758F4E779E4,
	GraphData_ClearAndSetAllowNonFunctions_mA08A0EB867DA6AF5819A054FECED77B54AB5CEDE,
	GraphData_Set2DCategoryPrefabs_m953A7F757FC0B719C47F1E01BA7D73E9679161E2,
	GraphData_AddInnerCategoryGraph_mA3F29EBD76D93899E44063AFF220CAAF353798DB,
	GraphData_SetInitialData_m37501EA9B2B451E3EB41CF2992454D58427D0536,
	GraphData_SetCategoryLine_mBEC80CBB964D92A28C3A65DB202DE8F09DF92808,
	GraphData_RemoveCategory_mDB5A5A8F928AF771956F8669181255853E5AC813,
	GraphData_SetCategoryPoint_m7457DF540DCA794789A87CDD354ECEB7B6629F9E,
	GraphData_SetCategoryFill_mB8D1DE3BF95449BA4D4ED7251213C833A67B236F,
	GraphData_ClearCategory_m08E3039A3C8D9EEA14899B689875DE88EA4B7A35,
	GraphData_AddPointToCategory_mBA22B51C9B4C172C0669C1DD7B7A723C2F1C470D,
	GraphData_GetLastPoint_m2421808CDE31B5BDD11C110C9F26854F735C1B72,
	GraphData_GetPoint_m72D84A8834B33534679F653EB61A4C85A8A6C70F,
	GraphData_AddPointToCategoryWithLabel_mB882CCF027B42C295ED404BD17632A0F21DB403D,
	GraphData_AddPointToCategoryWithLabel_m3506AFD00A4A3D397CA02EA7400161657EB7DA94,
	GraphData_AddPointToCategoryWithLabel_m88A62F64E8F1FFB1905FD4389E20E65DF40A692D,
	GraphData_AddPointToCategoryWithLabel_m2373B3FF0C1B4409592A20D1DF96E065B4C8DC84,
	GraphData_AddPointToCategory_m121AE6E76D8B015D8B5F0AF4C7DDF8812AA93D7F,
	GraphData_AddPointToCategory_m06BDFA3BC5133DE5FC51DD181BDC461CC449F736,
	GraphData_SetCurveInitialPoint_m132ACAAE5CC51CE8CB0C4BEA13CC92ABDAB2683C,
	GraphData_SetCurveInitialPoint_mD153FF86C0A814E79249B4FCEECBED8B2DB18CB3,
	GraphData_SetCurveInitialPoint_mB93CEC7BA75B4D5B15B6C31318272B9FD8B60838,
	GraphData_SetCategoryViewOrder_mF65EA457658B0665E727D896CA77A75DC567B7AD,
	GraphData_SetCurveInitialPoint_m43E707E1654DC57F626B228049618B2B53E73F93,
	GraphData_min3_mEFABA74DA9EED0B253278640101A93239871DDE9,
	GraphData_max3_mDAEE5DCED8454A6764286ED27C24A962DCC831F1,
	GraphData_max3_m1D1D77940480A82B11AF3920420DBD715D661AFC,
	GraphData_min3_m1238E49AA6581D063E368C91878818DE78AD3D4B,
	GraphData_MakeCurveCategorySmoothCubic_mE38178CF37FF8E5F29FB7DEABCCA092AF1A9D696,
	GraphData_MakeCurveCategorySmooth_m8EA020E1D7048B34F0CE7D1A94658E374C0FF25F,
	GraphData_AddLinearCurveToCategory_mF8DDE07DBEEBA6A80CFFB8ECA374773BB74CD55C,
	GraphData_AddCurveToCategory_m76ACB774CC44D3C8C05CF58FFEA391F125A779F2,
	GraphData_AddPointToCategory_mC08C597F4BD2BA3FB2F464B54A3998D2B843C1F4,
	GraphData_ChartAndGraph_IInternalGraphData_GetMaxValue_m697330585899A3E2813A803D63DF9650BCFA959C,
	GraphData_ChartAndGraph_IInternalGraphData_GetMinValue_m4029C8A74863FCC0CC3066C627006316884FEF1C,
	GraphData_OnAfterDeserialize_m3FC368A4F5DC5C324AEA3FBDE6A0EFF39F8881C4,
	GraphData_OnBeforeSerialize_mCEB9E6863D16C2BF3953B08B2D35F2720175EEBC,
	GraphData_AppendDatum_mD94C70B001ED3505AB66D8692D94C7B3DEFD9866,
	GraphData_InnerClearCategory_m70AE35FCC2B215C219842CAB2349C027C42FF599,
	GraphData_AddCategory_mEE61FFBCFC0E07F391B309470D1187186B218F75,
	GraphData_AppendDatum_m7EC586B99638452FA11A3A00D87EEC709F36C268,
	GraphData_GetDefaultCategory_m636547F9BFB7683C393E6EDE60A96F2B2FF39A7B,
	GraphData_ChartAndGraph_IInternalGraphData_get_TotalCategories_m762C65184FFCB8264C51352937C9B16A9BA0A21E,
	GraphData_ChartAndGraph_IInternalGraphData_get_Categories_m418973E4C21679A279EECC9D0D2A156DBF81917A,
	GraphData__ctor_m0E510F0D5D5B48E1B496B5BB9D614A99BDA1C5A2,
	Slider_Update_mE72393D0DCFA20404B3FA2A32988E92C6C21A521,
	Slider_get_Category_m7F8A2E06D5E487FE1EF19B4062E730DEF9199930,
	Slider__ctor_m037FEDB60F5B69FE19E3762310936C171B2275F7,
	Slider_get_Max_m6D11BA28916F6F1A165200F59CDE86871B218DB6,
	Slider_get_MinIndex_m5C8CB3D796F08273435A87499C56434BFB00A9D4,
	Slider_get_Min_m10830B16365E28ED7646E872D4D086284A8FAED7,
	CategoryData_getPoints_m48DA92AA76B1111D6E69F5ED45F91A3E0148659C,
	CategoryData_AddInnerCurve_m7F8BC0A4329FD036F06FF95D3AD075021C7F92A1,
	CategoryData_Store_m93410CDE47F7E9C484505064AF296B7E41D576B0,
	CategoryData_Restore_m72D79257BA59FAF97B6366F32843B8828B0C5B9E,
	CategoryData__ctor_m8FBFEDB361EF081932EC41FAD2BEC457A4976A17,
	CategoryData__cctor_mBF8497F2ADF47F05877958C3E7FF3C24EB548ACE,
	VectorComparer_Compare_m3A1FFB6B56AF278E7EE2FC25AB1B72088C614A35,
	VectorComparer__ctor_m15516D777433B1DCBFC0DFA5E229720786403068,
	SerializedCategory__ctor_m76C2019F11D2A4E2951A3DE82F5BBDF8C502DC3D,
	U3CU3Ec__cctor_mC4375967E552D708E3D3131C00CA7FC3CB195001,
	U3CU3Ec__ctor_m44855302AB891CBD33AA652D71D0C0CF2CE4B8A1,
	U3CU3Ec_U3CStoreAllCategoriesinOrderU3Eb__40_0_m46B873C4734B5D5A06E4B09DA78C2ADAAF938665,
	U3CU3Ec_U3CStoreAllCategoriesinOrderU3Eb__40_1_m00BFCAF4744FCC44EDF5E0A78940B85AA8B8BA05,
	U3CU3Ec_U3COnBeforeSerializeU3Eb__76_1_m2545A2892DEA0B95A3ECAB275D327EA963B1A442,
	U3CU3Ec_U3COnBeforeSerializeU3Eb__76_0_m3B0A5A481D4A3E7AE7E5F16DE57633C88E4D6A74,
	U3CU3Ec_U3CChartAndGraph_IInternalGraphData_get_CategoriesU3Eb__85_0_mEE6BD649F7A20640674004D15FD6EABF90C90B99,
	U3CU3Ec_U3CChartAndGraph_IInternalGraphData_get_CategoriesU3Eb__85_1_mC0E47F5FAE3FD40955B1ABD008A98589A026AB94,
	U3CU3Ec__DisplayClass46_0__ctor_m967F06DCB10C8F0B5FA0CE5BE1D24A09AE2EAC92,
	U3CU3Ec__DisplayClass46_0_U3CRemoveCategoryU3Eb__0_m55DBEB47F4B2F4DA80A571810BEE60BE53DE9463,
	U3CU3Ec__DisplayClass49_0__ctor_m4411231EF5C353509BB9F0708E03CBFE847946BB,
	U3CU3Ec__DisplayClass49_0_U3CClearCategoryU3Eb__0_mEF47F80A0195AED23C38A0CBF5F46ADBB80A358C,
	WorldSpaceGraphChart_get_TextCamera_m23D7BEDBF926FBFE055DE82D21760DA409B612F1,
	WorldSpaceGraphChart_set_TextCamera_m86CE6644AC1BFF1E8B6BA6582C6C4C2BE9D8940F,
	WorldSpaceGraphChart_get_TextIdleDistance_mCD510C55A0CEF8F1C94C5EB7E784D77DE802FF7C,
	WorldSpaceGraphChart_set_TextIdleDistance_mCD1C03A8872AE1B481B115A886338E596FC68616,
	WorldSpaceGraphChart_get_TextCameraLink_m862D82F641CD5CBDEFC478D9B2C705726765DCB0,
	WorldSpaceGraphChart_get_TextIdleDistanceLink_m24EB84DE48FE8EB97F9730BF7442AC2D3F256950,
	WorldSpaceGraphChart_get_TotalDepthLink_m015EE4161FD9D4BAF2FA716D6BE65CF572CB2CFA,
	WorldSpaceGraphChart_get_IsCanvas_m6B1E720B74BE80B5E50A40107B379A14F2E5F528,
	WorldSpaceGraphChart_get_SupportRealtimeGeneration_m58EC4686F5C3285FDF19A5AFAA3D01A412F6C0B5,
	WorldSpaceGraphChart_OnPropertyUpdated_m3AB2F7795A6EA7412509D9E1880954DB5E4CAF09,
	WorldSpaceGraphChart_CreatePointObject_m144F164F0473A5A297535AF3F39420BB019DB7D1,
	WorldSpaceGraphChart_CreateFillObject_m4474941A1FA34D3BCA97F7EA43A32A35CA1BB801,
	WorldSpaceGraphChart_CreateLineObject_mAEDB4BF8B19712E1C4F736CC71A2FD9E870E830C,
	WorldSpaceGraphChart_OnNonHoverted_mA978B1AFBEED702AA4F938102C9E5B46FC818706,
	WorldSpaceGraphChart_GetCategoryDepth_m45E16E42001132A9F4CCD0FD284510DF862C0886,
	WorldSpaceGraphChart_OnItemHoverted_m926B0DBD6462622ED625D5C82FF9661F8D3F907C,
	WorldSpaceGraphChart_AddBillboardText_mC910E28DCC2FD454E9F8EC358D76FEBFA9731A7B,
	WorldSpaceGraphChart_ClearChart_mBE40D2D4234A6F96F3EE99EAD532319C55C8A4E5,
	WorldSpaceGraphChart_ClearCache_m5A63A3858EE2B14592144BBCB9F2E840E90AFE5B,
	WorldSpaceGraphChart_GenerateRealtime_m38F018228943D6E2086A9B7E7AEE906BBDCCBECC,
	WorldSpaceGraphChart_ViewPortionChanged_m2F42AB998AE880C0B5C0C228E1029CA8164B988B,
	WorldSpaceGraphChart_InternalGenerateChart_mD403B5EEC5B43DAD5F2A7617322A4B58BC7B7F19,
	WorldSpaceGraphChart_SetAsMixedSeries_m33B086E7E24E8751030677CF1CB0B50A0EB7B52F,
	WorldSpaceGraphChart__ctor_m11FEE2638B36BEC38A52B727103083A0C9FA302F,
	U3CU3Ec__DisplayClass29_0__ctor_mF7F5C5109B96351894E4E44DE62842BEEB704A06,
	U3CU3Ec__DisplayClass29_0_U3CGetCategoryDepthU3Eb__0_mB0A9C4AFEB4AD698C845788E90BA30C87F709DAE,
	U3CU3Ec__cctor_mFE42DFB7CBB94DE23643F029119FF14911ECF5C4,
	U3CU3Ec__ctor_m7F323ACB0D421234BB1329DBE77F9ACCB0225F48,
	U3CU3Ec_U3CInternalGenerateChartU3Eb__36_1_mC5FE3C59D5AB982E5F6C65C11025A56BE23578A6,
	U3CU3Ec_U3CInternalGenerateChartU3Eb__36_0_m6F13299ED82EB624BC2E90466101476CE880CFB2,
	GraphFileManager_SaveGraphDataToFile_mED864A8E42C6ED917D9D93894ACD4685B64B0C09,
	GraphFileManager_LoadGraphDataFromFile_mBCB3B2D8328CA25D0E1E28105A173E059059EBAF,
	GraphFileManager__ctor_m3186BD15EDA954B5399E2C85997821842A2BCD82,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LegenedData_AddLegenedItem_m0A4E213A1C7F5D3B95E468F42222875E8A87B96C,
	LegenedData_get_Items_m554485E6B26A46FB5B1A87CBC12DD399F9336DED,
	LegenedData__ctor_mEBAC6D44FDCDC65E9FAE701F5D84C49FCFA0D17A,
	LegenedItem__ctor_m4B5DC2BE1D1F7245DFCF89F1C84103063A8C1F93,
	MaterialTiling__ctor_m0B5FB49A293882E7A35FAEC1EB5DBA660AA07269,
	MaterialTiling_Equals_m466A2E9815E17A86ABED36483492136634F64556,
	MaterialTiling_GetHashCode_m31C922525DC3445EB61DCD90C8066015A8352338,
	CanvasChartMesh__ctor_mC8420991F60F32A4E3D92E3784E9D381995E2747,
	CanvasChartMesh__ctor_m90F9A5D93F0BBAF1CD19420289156CB538915D22,
	CanvasChartMesh__ctor_mF6F9FC3F6A496692D8641E1A07B4882EB8B29CE4,
	CanvasChartMesh_WrapAround_m0B9B647A396473DDAFAEAF7F4A963C07EFE4EDFA,
	CanvasChartMesh_WrapAround_m38C56CD5B079B67EC813B4B1DCAD2518D4CDBB39,
	CanvasChartMesh_AddText_mF74190C7102D9387705DEB7D28067ACDD4E7FD88,
	CanvasChartMesh_FloorVertex_mA7CFA0D10ABC5291C0DBD1278007F72964027A21,
	CanvasChartMesh_AddQuad_m10058BC782160AB63D3E3B539B96C46B3A56B5E2,
	CanvasChartMesh_AddXYRect_mBFF9EDC1493756647C86B38EEADDEE7DE5388B75,
	CanvasChartMesh_AddXZRect_m88E01C307E6910DF3BC01315BBAFDA092292F6D7,
	CanvasChartMesh_AddYZRect_m18A6CB2741340D63F7097F704723DF9EE9FF2BA9,
	ChartMeshBase_get_Orientation_mD1C2C815A175E2BBEA3117ED8EBE1A7932640E87,
	ChartMeshBase_set_Orientation_m87F834D65E5BED545ED69FE1C608E093643A02FE,
	ChartMeshBase_get_Tile_m325A1DA8DF1CE6A5201E16181CA3061C2B4F5079,
	ChartMeshBase_set_Tile_mAED56F5D771AC0946C07F3840834A44252736E44,
	ChartMeshBase_get_Offset_m44B3F4D7B3492223D8C915497E194765BBCA2F02,
	ChartMeshBase_set_Offset_m0A620034DF859A689C768EEC52815E5F1DEB2B00,
	ChartMeshBase_get_Length_mF24BFEEE513BE43BF890161B869DAF8E5CA23FC6,
	ChartMeshBase_set_Length_m05073A1B3BEFECD9F86DF14BFB79695824840418,
	ChartMeshBase_get_RecycleText_mBF81839FE404AF6776B99355A86373744038D025,
	ChartMeshBase_set_RecycleText_mCFE3B45903FC5BABBCF566195FCED69F637A413B,
	ChartMeshBase__ctor_mC07028B8E9EAF4D95B3FC2F395EBB6123FD333AB,
	ChartMeshBase_get_CurrentTextObjects_m84EEEB05795DEC2C619D2CF43EDFB6558061E538,
	ChartMeshBase_get_TextObjects_mD61A9AFBA1144FDA24681A92EE78A5F63ED8CE43,
	ChartMeshBase_GetUvs_m4B5310AE287908344D0F7C85B9AEC4429D3A11F4,
	ChartMeshBase_Clear_m6449E892337D4ADD7A7C1203209BC4B677186157,
	ChartMeshBase_GetUvs_m00C4DC193BB5235A53273303F44F63912A12698A,
	ChartMeshBase_DestoryBillboard_mFDEE9F997AC909905B9C678030E1D8003095E15C,
	ChartMeshBase_DestoryRecycled_mEC94B72D27DC4C54A7B0664229FC207D059B7292,
	ChartMeshBase_AddText_mA665D9EA8727D70252D516570752AD2C99D15541,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WorldSpaceChartMesh__ctor_m2E3C6BC041022321A3D46B36EF5CF565FE2299ED,
	WorldSpaceChartMesh__ctor_mA7D219705F46FCD9AD3D6BD1B19E4C551E352FC2,
	WorldSpaceChartMesh_ValidateMesh_mD63C946E65F9BE74DB95ECE79CF7F9B22DAB669B,
	WorldSpaceChartMesh_AddVertex_m7BC1BD2EEB63D113F3FFFC56C4B1118CF6D63CEA,
	WorldSpaceChartMesh_AddText_mA022A04A643B695991733B1AB32D94F771EFA3B3,
	WorldSpaceChartMesh_AddVertex_mB5DD6E333F49D0D05934B79F0CE9FDCAE9BB04B5,
	WorldSpaceChartMesh_AddTringle_m657A6A980A3FFDFE2DBC4585F445AC77A5EF631B,
	WorldSpaceChartMesh_AddTringle_m1DCD78C509C68A93C50E75756B083565CCB49230,
	WorldSpaceChartMesh_Clear_m28F67D43065681BBEBAFB8F21B01CC12C45E2E50,
	WorldSpaceChartMesh_GetTringlesForGroup_m4844214648411B602839321A9D2A0787B71E2984,
	WorldSpaceChartMesh_AddYZRect_mAD45D502AA70369A86E50A6BAA91FA0EBB721D94,
	WorldSpaceChartMesh_AddXZRect_m36B5C5463288ECD06BFDCE92FFB5BE9683834C5B,
	WorldSpaceChartMesh_AddXYRect_m351BB53A86264D8882FE0E97C46EB5D50BD578C8,
	WorldSpaceChartMesh_AddQuad_mB0D5BAF24B4CB90C056FF13EDD683814F75E920B,
	WorldSpaceChartMesh_GetColors_m5B7016EA5092703679F2D975117322F34FAF00A4,
	WorldSpaceChartMesh_ApplyToMesh_mDF81D371F336EF85E1769F6B9853999D628AF972,
	WorldSpaceChartMesh_Generate_m3EB581349019341221CC0016CFF0A97272D5805B,
	WorldSpaceChartMesh_Generate_m55981EA45B0F0E0CE0EF3C2248D06CBEB52CCA3C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChartSettingItemBase_add_OnDataUpdate_mDD4FC4303A073F4D8704C248857730055FAEF249,
	ChartSettingItemBase_remove_OnDataUpdate_mA808D637C17F5F6E1ABD363FD47CBB39D899D061,
	ChartSettingItemBase_add_OnDataChanged_mEFABE67C67B63454BF30E194741B5BA2B1F5D2AD,
	ChartSettingItemBase_remove_OnDataChanged_m16893529ED8A5D7F8FE580F3107DD651AEEA77DD,
	NULL,
	ChartSettingItemBase_get_SafeChart_m3DEA3107DE28C2A19C172842D614C4261D2B26FC,
	ChartSettingItemBase_AddInnerItem_m184A5C5296559360D74987D8F4B8EB1B3E6D29E1,
	ChartSettingItemBase_Item_InternalOnDataUpdate_m1DC75A6A28470A2B8148FE3D394C91787FACCA6F,
	ChartSettingItemBase_Item_InternalOnDataChanged_m3696A5FAE754C9EF520C0019FD8395146085DB78,
	ChartSettingItemBase_RaiseOnChanged_m8A0FECF1EA9C891D7394298F246ED34DD7DAFCC4,
	ChartSettingItemBase_RaiseOnUpdate_m7A62F9F02DE1353DECEEB73D1374A5BB1702FB79,
	ChartSettingItemBase_SafeAssign_mFB418A18CA26EF34EB9DA391290C0F77973C0FEE,
	ChartSettingItemBase_OnEnable_mCF6FBFBD2FFF1294FB71F15068E0A53D517E3427,
	ChartSettingItemBase_OnDisable_m4A9359AE463435F63ECBB0DB87D0AF40F137339E,
	ChartSettingItemBase_OnDestroy_mD1651F68B1676A67615C60360CA0EFA80602502C,
	ChartSettingItemBase_OnValidate_mAADB5BB1A37F9746EE0B40909B142E992CC6385F,
	ChartSettingItemBase_Start_mDA47E2577247F4F9630FE38EDCA5E7B29EE60687,
	ChartSettingItemBase_ChartAndGraph_IInternalSettings_add_InternalOnDataUpdate_mBA788FFF61D365DCED995DE7749ADE00E6FD9D75,
	ChartSettingItemBase_ChartAndGraph_IInternalSettings_remove_InternalOnDataUpdate_mB599FD2E02CF260112C4D9D8A6D0562EC19FFCB3,
	ChartSettingItemBase_ChartAndGraph_IInternalSettings_add_InternalOnDataChanged_m8E7C41522B67DEF1D422E837208B49F951C236B1,
	ChartSettingItemBase_ChartAndGraph_IInternalSettings_remove_InternalOnDataChanged_mC440E3CB02974E01C9D2620EFCF3637CE7E4BBB7,
	ChartSettingItemBase__ctor_m3BE6C32887690E8537489E85A9FFB88C158AF7BE,
	BoxPathGenerator_WriteBox_m7EE3621B1FE61E0412F23A1848B25E7884037F97,
	BoxPathGenerator_AddTringles_mDB77D5DB9418BB02DD548B375F704D3885E9298A,
	BoxPathGenerator_Generator_m1BB38AAD5CAC72558CD051B7F47FE17F96AD40FA,
	BoxPathGenerator__ctor_mE3876F2A89613E614008E622B6E53EC421E8B4AA,
	CustomPathGenerator_writeItem_m4608FA73D3C6ED5199C79FEC47BD4596B01FB8C0,
	CustomPathGenerator_AddTringles_mE7DDC474A3924357D0966E1B1F38D93A11E81D2B,
	CustomPathGenerator_OfPath_m213D7FD2322D30BE947553826EF499E15812704C,
	CustomPathGenerator_getAngle_m183A500DF1C65FF0805E5A1D18EAFB1831251693,
	CustomPathGenerator_getScale_m6611924BB1F9B0E58371D1B7F51B4CE43909CC9C,
	CustomPathGenerator_Generator_m3720613604D6CDB1BEAB5217C3C7F8D6365D4324,
	CustomPathGenerator_quickBlend_m626A43A59874DCE6ED9A1E7D0110D69ABBA939D7,
	CustomPathGenerator_Generate_mA879B9F2476F7D11F31A5A51547A45699F07A305,
	CustomPathGenerator_ChartAndGraph_IBarGenerator_Generate_m0F260C81AEDD379B854E0B1C4A0D6AA10C56707C,
	CustomPathGenerator_ChartAndGraph_IBarGenerator_Clear_m508E793EA9EB432445DFBA3437FB97AF01DC1098,
	CustomPathGenerator__ctor_m57F797C7AEECDA022B4211138A7DE3CB0E966342,
	CylinderPathGenerator_EnsureCirlce_mD3C5AD52A226005D43535F9160D13E337769B010,
	CylinderPathGenerator_WriteCircle_mB7571B9661A080808FE95776615186EE944451F6,
	CylinderPathGenerator_AddTringles_m018F387507B830E374CF4A6D997113A9A2C40960,
	CylinderPathGenerator_Generator_m1DE0755CAA7C889FD69E4709E8A2FB8D011C7239,
	CylinderPathGenerator__ctor_mF52CF728E55F7345B0C28C611D4E0E905138F6E1,
	FillPathGenerator_get_JointSizeLink_m575B9E75A2C595D25D0D4BBE9122EC3E4035D1CA,
	FillPathGenerator_get_JointSmoothingLink_mCB9270C160E3B49CCC7CAFE5924D15EEB0C9688F,
	FillPathGenerator_SetLineSmoothing_mC3101431F48A93A64BB009F8D51287BA33FDAC34,
	FillPathGenerator_SetStrechFill_mE60AD747B2BA3842C9C02F505762B440281184D4,
	FillPathGenerator_SetGraphBounds_m61780836DDE984366D3EBD53269729E2B1797231,
	FillPathGenerator_WriteVector_mDF2D7A27BC22A3CE5CA646313C8FF75B9A7CD133,
	FillPathGenerator_AddTringles_m0678A0768BB91D3A35D7314FDBC9E1C8352F7626,
	FillPathGenerator_Generator_m219C94C70D29A830B2BBD8CD098B03FCA1846B8A,
	FillPathGenerator__ctor_m28EF7901FC93F3FF47684856905166DF80419AEB,
	LineRendererPathGenerator_Start_m4F6FC06348B0E6944A81F14E2F0116B2F911DB90,
	LineRendererPathGenerator_EnsureRenderer_m1C98460ABC8DA8601F25A7D48BD0D01ED61AD80F,
	LineRendererPathGenerator_Clear_m2E01D8922904F87793D9B13BAB68937628D13C19,
	LineRendererPathGenerator_Generator_m8C16EEF8021D03B3A3E94BA1FE660D4CBDFB86B1,
	LineRendererPathGenerator__ctor_m2389404C35FEFFF926B3A4C08FA5A2D7A9991E88,
	NULL,
	NULL,
	PathGenerator__ctor_m30AB7C7D09A2ED528A50E0CFF5A2107CD62ED719,
	PathMultiplier_LookRotation_m78DF542B79BB3CE2CCE1A2620619B59A556605F5,
	PathMultiplier_AddJointSegments_mC74B9E4D0A5D7908D9F1B0A2D702BB74DC1529AF,
	PathMultiplier_AddCenters_mD0B90CD13135C2DB1884C198DA79C8FD1B8F644A,
	PathMultiplier_ApplyToMesh_mC8205718BA1D62EBE8F54D25B716778068F687FA,
	PathMultiplier_MultiplyPath_mC39857B7CCF198C040706E5D9A26993D4B99BCB0,
	PathMultiplier_ModifyPath_mD09BFF9A47331B9B9673C366036766525EF21AF7,
	PathMultiplier__ctor_m2E832E42E84A57D057ABF236735DC93A7EF346E0,
	SmoothPathGenerator_get_JointSmoothingLink_m9C39609C277255F8748FFF0A27760F9DA7AF91E6,
	SmoothPathGenerator_get_JointSizeLink_m6281A0339CB5D9D11D6794861595FDD85F46B6E4,
	SmoothPathGenerator_Clear_m7E327B19334F2C37D984247CB022FED7B36A6FBF,
	SmoothPathGenerator_OnDestroy_m8B10F4146409C968C1B13B1ADCC9B871BDBC27FD,
	SmoothPathGenerator_EnsureMeshFilter_m579546E8AD41ADD2418B9F790087783F838CF754,
	SmoothPathGenerator_SetMesh_m314D6469FDC81872A12BA0DAA1D68CB67352CBC9,
	SmoothPathGenerator_LookRotation_mBA1E29179CA8A963DF317A5AF23716CD8E4BCB86,
	SmoothPathGenerator_AddJointSegments_m78E8C49A087D0C5BB21CDCA7B9FDE65273FB38A3,
	SmoothPathGenerator_ModifyPath_mFD40AF348E9FB9C9462306D9FA4A1DC8F2FFD0A9,
	SmoothPathGenerator_ModifyPath_m6061F1F1ACCD736355CDA77F734130A73FD0D351,
	SmoothPathGenerator__ctor_m7F0080AA8521913262881666197000FB8AC321FA,
	CanvasPieChart__ctor_m0D0D05DE6A15C40F5E2553CB536FDBE518C78173,
	CanvasPieChart_get_Prefab_mFA7ADB31F8204A50439275DAB13766255D5E6D35,
	CanvasPieChart_set_Prefab_m854B47747293842413CCCA678988EA86952F7646,
	CanvasPieChart_get_IsCanvas_mB8A376FE32E803F0C0CDE2B5DA16EEC047EDF8BA,
	CanvasPieChart_get_InnerDepthLink_mB82D6B0B206D5696BBE436C835F9A364C9007DB3,
	CanvasPieChart_get_OuterDepthLink_m3E4739B6FA30A523604F27A44C491D3AA06BB22D,
	CanvasPieChart_get_LineMaterialLink_m58FE291E0EE8BDD0730B000102E09F1BF531D0F0,
	CanvasPieChart_get_LineThicknessLink_m1B3A721D7D481539304779EA12884985259F28D3,
	CanvasPieChart_get_LineSpacingLink_m0E79B3CCED621279ED935C6F0123B0307282EBA0,
	CanvasPieChart_get_LineSpacing_m7BCE5141269D6A9428661207F7EB31C2E2524E30,
	CanvasPieChart_set_LineSpacing_m746F61A226E58567491CDFFCE2D72BD6C9D88F45,
	CanvasPieChart_ValidateProperties_mE96BB445D5C1FEB8F87A336FF8F6A9996E5338B4,
	CanvasPieChart_get_LineThickness_mBD4AA35A14D6762E6D9C61BD8893E3CD34B0B7BF,
	CanvasPieChart_set_LineThickness_m6D74D0952014C6064561817583814C3010B84B20,
	CanvasPieChart_get_LineMaterial_m1FCED0C273C1843B57574130D7BD75DE57432C25,
	CanvasPieChart_set_LineMaterial_m61036643D6F18BD3C2EEF6BA8F9DA26D2869DCD2,
	CanvasPieChart_InternalGenerateChart_m25D36C1384D34F13A60C3EE477A65114135BB5E1,
	CanvasPieChart_PreparePieObject_m5B1BC1D5508B5A7F0885C7F28ADDADA94472A3FE,
	NULL,
	PieCanvasGenerator_OnFillVBO_m6342BD86F88ECFD2B03B0A3BC1E61FE4DF418DA0,
	PieCanvasGenerator_OnPopulateMesh_m845364C1C0DC72BCB96236C8E0AD9B6BE1F4E7D3,
	PieCanvasGenerator_FillChartMesh_mD2F9873596C7E21F2C66491DF63C8C4A2792D6E6,
	PieCanvasGenerator_Generate_m91D79D36DC6F0CE3D68ED2120AE1B5AFA051104E,
	PieCanvasGenerator_IsRaycastLocationValid_mB69275183754AF7DC0BAAC0063E99A2FD26A1FA0,
	PieCanvasGenerator__ctor_m6E34417FCAB752B174EC3210DB7AD7BFFFEF95C1,
	PieChart_get_ClockWise_mFCB137777262AE9EF5E4083A51733E3FB723D24A,
	PieChart_set_ClockWise_m2A4945A952537B1B60ACD3F0636AC845C34C7A94,
	PieChart_get_DataLink_mB840DF74806D0E6D3193BAE23554B02D4C4007BC,
	PieChart_get_DataSource_m2EA58A6ABAF519FBA7F87552870D7F0F5EE48BD2,
	NULL,
	NULL,
	NULL,
	PieChart_get_TotalDepthLink_m65C1778496C15643E0EBC9CE34D6105AC0F4CF32,
	PieChart_get_TotalHeightLink_m22FAD7AD18038CEE17305BB50890C04774B650C2,
	PieChart_get_TotalWidthLink_m34B6CA8BF1ADC35DA456F660DD25DE23CCEE4AAF,
	PieChart_get_MeshSegements_m4A7289B0EF5936C1EF458F5E7DB153412A2065D1,
	PieChart_set_MeshSegements_mEB1CAB1880E6E11E9C21CC6B12DEA955C1F8A697,
	PieChart_get_AngleSpan_m73FAD22D7B1FAB8D98DABC375BD1EDF5A6D63FDD,
	PieChart_set_AngleSpan_m549F5491905F22C45E8C4BA87CBD8137E3576E0A,
	PieChart_get_SpacingAngle_mA7A171C614EF177CA9A2AB5646B03F394FBBD70F,
	PieChart_set_SpacingAngle_m68F5777240098C29D308DEDFCAFFBE115E589D97,
	PieChart_get_SupportRealtimeGeneration_m0605EC1A3E165A6F120EC0A0D8C94B7C49D23E98,
	PieChart_get_Radius_m9FA236E3D78F208734749FA6F75A15B74EB7F744,
	PieChart_set_Radius_m04EA3281954CE9D9F6A457D77D96BA143ECFC56B,
	PieChart_get_TorusRadius_mA58D6DE6B8F5151780F92F9257E55D442827DCB8,
	PieChart_set_TorusRadius_m4CD86EEE1A8D607B3A05DB6A69723A5CE900DD60,
	PieChart_get_StartAngle_m45404C7A4E159368C869CD6AEE450B7648E9258E,
	PieChart_set_StartAngle_m7CF0D4D475311FA754E80292BEB060095FB15BB0,
	PieChart_get_Extrusion_m71CDD0B95441F39C90E9AD46F6032E3725C860A0,
	PieChart_set_Extrusion_mC02B66637137AB3DF9DCD6906E80C496164DF74B,
	PieChart_OnDidApplyAnimationProperties_mAB75039DB9834C72B16695D3E1D3F08CA942A501,
	NULL,
	NULL,
	PieChart_get_LegendInfo_m478B6EF82793D6F0EEB2E4712B7020A3B4DA97ED,
	PieChart__ctor_mE67D6F3E162C075707ECBCE49982923CEF1A544D,
	PieChart_HookEvents_m61E0478E234C901F3886381FB6BB451BD2E2ACB8,
	PieChart_Data_ProperyUpdated_mF1472C937B837E54F3734BC761C9DFF57622E8AF,
	PieChart_QuickInvalidate_mC15DE676DD48660F6806F7BF9367C1A66249A363,
	PieChart_Invalidate_m7AC1DAF0081DAB36348F86C5AE97F696850DC851,
	PieChart_MDataSource_DataValueChanged_m7A8F9B03EC93F3A6681A59AECEFE43AF53A7C91A,
	PieChart_MDataSource_DataStructureChanged_m1FDDC6A28D391C8915CB6CBAC72068C7C47D2560,
	PieChart_Start_m9698F1A2547D85AE487C8A0970AD1DBCCA72243B,
	PieChart_OnValidate_m542DB0EFE4AB6AF93EABE99CDCCAE5392363B04E,
	PieChart_ClearChart_mE55068A075E7939BECCDFEF63152D2536E1A869A,
	PieChart_AlignTextPosition_m5E2D84F9BBF0795184D343C6E69C56EBD44F5D85,
	PieChart_AddLineRenderer_mCB039E1BF628DA170FBE81B167560EF95661D5E5,
	PieChart_GeneratePie_m9DB5F22BB52F987E2F6CEC2C92DE8D2492B1466B,
	NULL,
	PieChart_OnLabelSettingChanged_m8FC252D9ACE47F56BC94FCEC2844FEE9FA864B2D,
	PieChart_OnLabelSettingsSet_mC51CB0677BFCEDB73DEAD92C3CF242C0B765CA74,
	PieChart_InternalGenerateChart_mE04E609DBD0647FAED3E39703B242A8D673261CF,
	PieChart_HasValues_m332A8670E759D3CA705FF52DFB8A1278A0B77434,
	PieChart_MaxValue_m2A4AB3A22C810118C1C3F8AE5393CC240DA8C76B,
	PieChart_MinValue_m71D76DF3FDA61B120AE739DEEDF360152C31A8B6,
	PieChart_OnPropertyChanged_m126952AEE52DDF87B3947EC1811845B8D8F9ED70,
	PieChart_OnPropertyUpdated_m0F2A8917F537889299030D430F7ED4E12B543120,
	PieChart_ValidateProperties_m7BB704663520A38C1773776DAD522025A9D0128B,
	PieChart_userDataToEventArgs_m34092B0D6D9296DD4FC369A944321104C9AD3E1E,
	PieChart_OnNonHoverted_m9EC54D8DEBD959EE6CC78CC0FC60F203E05B5E6C,
	PieChart_OnItemHoverted_m5B85BE5F9A16330E42807B6119C3A425B78D77FA,
	PieChart_OnItemSelected_mB52D340EB373FD245973AD2BD743A0FEBFB3E347,
	PieChart_Update_m1611C9840407E094EAB2A50DDFA29117AD70AFA7,
	PieChart_get_SupportsCategoryLabels_m184227D8A7086D3D520F08F0C90CFB176882BDCD,
	PieChart_get_SupportsGroupLables_m9D4DBADBB5376CBB4E9B56F23009DF29D609E495,
	PieChart_get_SupportsItemLabels_mBBF43FED4601026C9702E7E5EB18D86F39F6493C,
	PieEventArgs__ctor_m086F6E1E42D63DDE73276C08979BEF70A3120E70,
	PieEventArgs_get_Value_m34E34DB40147B026905D90FF0579DF887371A9FF,
	PieEventArgs_set_Value_m57932DB099DD42DDEE99D1F87CE10DB0B6C128C3,
	PieEventArgs_get_Category_m4104152AEE2A00AF85E1E806EDE4936580E24920,
	PieEventArgs_set_Category_m1D0F3E34466A4EA1B70186116AF07992FC8D6206,
	PieEvent__ctor_mE1F60C9925E2E45CD52A8449667CA6F1C69D1509,
	PieObject__ctor_mD178AD6BBBF5509CD0AEFE04441C7B3EFB063522,
	PieData__ctor_mFB43B6D6EDA8849D492EF0E10C748F189E7ACF17,
	PieData_ChartAndGraph_IInternalPieData_get_InternalDataSource_mEF2702E9CAD9B8342106C4828DBCF0EBA58AFEA6,
	PieData_get_TotalCategories_m03E19463EF6CF4ACA3E0B5B4F94C19E18B268233,
	PieData_Update_m6AC15684466C0CFA5C809A942DF0E0AF4D850FF7,
	PieData_GetCategoryName_mE5C421B17E2F9CFDE8EB3EA479F983C3BB7319CE,
	PieData_OnBeforeSerialize_mDCC2160261E78966599E79BDE008521E1F90A389,
	PieData_add_ProperyUpdated_m4960F7F4225FF6396F630FDDF953FC61F1E7C15B,
	PieData_remove_ProperyUpdated_m81A424A95A8D71D68AD29F6C4B7EE4103DFD65ED,
	PieData_RaisePropertyUpdated_m4593E7A6101DC1B1DA569479A7131D1E0D074C18,
	PieData_HasCategory_m683486CE43763D5DB2AAA8E037FF39F125ED7D28,
	PieData_RenameCategory_m28E3CFDFA1F2EE3F5611609599CC985E6CD73C4F,
	PieData_StoreCategory_m74BF28D255E9473FC7B3A8C98175227F0D0CBA34,
	PieData_RestoreCategory_m4A0D607C1E8C5B04260C4DFAFF18661F01F6C5DB,
	PieData_SetCateogryParams_m6D0D8ED467C333B64572DEC2E3135274C70355DE,
	PieData_StartBatch_m4E94F84E7F7F198FA65718237A566E461C1F6194,
	PieData_EndBatch_mC9FC2C884D9D1C74ADCD63552347E436FC410566,
	PieData_OnAfterDeserialize_m90AF8B77FBB1E4ED71271C9DEE2E6C671B90E1B0,
	PieData_AddGroup_mAA390186A7FA0F200934B71D3CE3812EF543DD52,
	PieData_AddCategory_mDDC8D602D08A6B3CE6D9CDC8D02A4E02E4AF1417,
	PieData_Clear_m8B295EAAC0D69A9F1C3AC8BD7790BA9E8B7D2A08,
	PieData_AddCategory_m1624DCC5FBF814970CBB52BFD9E5AFD1CB22B714,
	PieData_SetMaterial_mB52EA8F4A2FBAE39E7B08125C5A19896249D223B,
	PieData_GetMaterial_mF1A6EDCB2734C520DDAAFA8C46CEC9D5DB1820A2,
	PieData_SetMaterial_mEC84FADB276984D3D041630C64ECC3A12A3D9C4B,
	PieData_RemoveCategory_m94E6DD67A6C2548DCD6AA37FA102343833B4E0E2,
	PieData_GetValue_mB5FEB9E58585E0A291EA6F871512826733A4C61B,
	PieData_CheckAnimationEnded_m3F88C840661BB80E5482476FBF46E90EB67E7986,
	PieData_StoreAllCategoriesinOrder_mBDECEEF75E9D7C65749BA9F3397F5FDB3F266749,
	PieData_FixEaseFunction_m50ECBDF8EC936BFFE013E122CFDCE99ECDE9B049,
	PieData_SlideValue_mD005484D571F83F853101E10170E0BE7A434FC6B,
	PieData_SlideValue_m8ADE6BDAE78DFE75AB32DCA0C15437A235B72693,
	PieData_SetValue_m3C5DE50D73B97FBCE3DD0A2658A8B57B5295AB7A,
	PieData_SetValueInternal_mA2A483296791D4C8D3AA657E6005D7103CBC2524,
	CategoryData__ctor_m9C63DA3E719D44CD498C63AB2946AFBDAB5A4569,
	DataEntry__ctor_m322EBF74DDEAF4EB0AAEBC15ED17AA2879B1C375,
	U3CU3Ec__cctor_mB3FE18FEE4CB7AECFD410090D4C840CFCB9BA1E9,
	U3CU3Ec__ctor_mC9319E3F06175EB32FEDBA4B0BB1898654973860,
	U3CU3Ec_U3CClearU3Eb__28_0_m44DCC4BBEBFAE4B5F1C57F8C5B386CAC57687191,
	PieInfo_get_pieObject_mDBF95EBF9EF48A131C00E13C4BBC824636127571,
	PieInfo_set_pieObject_m94FFB57B6A64D14E3C5031D72DA5791AC030CC3F,
	PieInfo_get_Category_m67E84E4AFB994A57C04B7BD7423C004E8D5F5130,
	PieInfo__ctor_mA6C1E8F3EBA53EEB2A155EF9E080C7A2F6EBCC86,
	PieMesh_Generate2dPath_m92E63EFC254B384A5D16839A7628E6EF35A2C28B,
	PieMesh_Generate2dMesh_m2FFE004BFECE53EF9AA67292DA07F122783EA83F,
	PieMesh_Generate3dMesh_m1A6485F1D48C1A27A2290170C4BA84D86D4CECBE,
	PieMesh__ctor_m4FD1F62B7FD11F6193AE2D0BC1972ADBDFE5EF1A,
	WorldSpacePieChart_get_TextCamera_mFDAE1CA423D5B2C52C03C83283C2506207B3393D,
	WorldSpacePieChart_set_TextCamera_m9C0EDE1FDCA1217F9400F1850A3A74FB6424B822,
	WorldSpacePieChart_get_TextIdleDistance_mB1955EDBF33A6ED8635006278C07ADCF143C8712,
	WorldSpacePieChart_set_TextIdleDistance_mFD554F60F0C5B1FCC607F4508E46DB2AFB7B045D,
	WorldSpacePieChart_get_TextCameraLink_m65704EAF7E010E7A36D5DBCBBF4423D706E8F8A7,
	WorldSpacePieChart_get_IsCanvas_m6F6BE23848DFC45433FBB12515097D481CA6E25D,
	WorldSpacePieChart_get_TextIdleDistanceLink_m78335F11624737FBF314B83B125C6414C17C3FDD,
	WorldSpacePieChart__ctor_m0E4D31440D9A8EB198D4C52B0DB862E9403566D7,
	WorldSpacePieChart_get_Prefab_m238DD7272D86807CAFC38F47162AFA625237546C,
	WorldSpacePieChart_set_Prefab_mBBD1FB33A5DF4D6F19C80814415577895F8C578F,
	WorldSpacePieChart_get_OuterDepth_mF1FF8B99C566AF38E191AD2E88BDA6405EBA1A52,
	WorldSpacePieChart_set_OuterDepth_mD111B6F36D13674799E14A36424BF7BD8564318F,
	WorldSpacePieChart_get_InnerDepth_mDEDCA1D569687C91AAB8A31DF332E2760A5C6FCA,
	WorldSpacePieChart_set_InnerDepth_m81C1E7C36EEF2D5197882A6C001C05364EDF057E,
	WorldSpacePieChart_get_InnerDepthLink_m17B23B505994EDAC74AAF1C50E156F093327E316,
	WorldSpacePieChart_get_OuterDepthLink_mF5BD4C4CA5C5F2B70655E881CBB6157CB981CA96,
	WorldSpacePieChart_PreparePieObject_m2E769E29F3485BF001517AC5DF424A5BC2505574,
	WorldSpacePieChart_ValidateProperties_m32EB68138AC218546B9358B366F7B0A1A97CA783,
	WorldSpacePieChart_get_LineMaterialLink_mFCDCC6AF751C5DCCB8557318DF5D1EE709EDFAFD,
	WorldSpacePieChart_get_LineThicknessLink_m16A2FA5F79F15E401E2BCD76A58AD01CB4619996,
	WorldSpacePieChart_get_LineSpacingLink_mA029269FFF79DA98C773A3BFA395AB4ED0D2288A,
	WorldSpacePieGenerator_Start_mD9FBF594E0BC4F5C91602276D4F6A6E30D044B5F,
	WorldSpacePieGenerator_Generate_mEC59AD4B1E9D550CB386E56A77BE23469D13607A,
	WorldSpacePieGenerator_OnDestroy_m194936688A6F10E673A93489D95D89C7F45DF215,
	WorldSpacePieGenerator__ctor_mACE9588E5BC015DD69DE0C6B454CA617E3F64DA4,
	CanvasRadarChart_CreateFillObject_mE84CCCD1A5203D55EF4F01042C98A4303C591ACB,
	CanvasRadarChart_CreateLinesObject_mC20E3CE7F2DB901967F6BCCE9C27372A5C5D9DF5,
	CanvasRadarChart_CreateAxisObject_mE22109B3D0A6957881ECE8F8AAC8A72A5DA4449B,
	CanvasRadarChart_InternalGenerateChart_mEAA64B0BE56B0328AE6AE59E35C40B5596CF04EB,
	CanvasRadarChart_CreateCategoryObject_m960496039E2D5A02EAAE42577CC4E89D594CCD17,
	CanvasRadarChart_OnEnable_m376B677255FC4D46DE92606D0F11ECA452AD9EAE,
	CanvasRadarChart_OnItemHoverted_mE93590D70158AFD9C25330537610705FC503732E,
	CanvasRadarChart_OnItemLeave_m55DE371A6EF157669C3D8C57BB74C9F6B99F319C,
	CanvasRadarChart_get_IsCanvas_mB56D16A567E4DFE9727CCC63C1A99582665BC5B9,
	CanvasRadarChart_OnItemSelected_m26E1B7280EE52944927DFCE3CF34E735BE753D20,
	CanvasRadarChart_Points_Click_m01D15DD53BAAAB26740536B371BD1323AAD97984,
	CanvasRadarChart_Points_Leave_m22182EF45BAE4418422697B3A2881A5DA0122E2E,
	CanvasRadarChart_Points_Hover_mB5F74DD1D87EFCB75EBFB025D6FE18CB858CF186,
	CanvasRadarChart__ctor_m34838197BC7B999B2BBC8E95520B2E601C400AC3,
	U3CU3Ec__DisplayClass4_0__ctor_mB156B14C76DE2DB071DF58E1B5D031517B8333B1,
	U3CU3Ec__DisplayClass4_0_U3CCreateCategoryObjectU3Eb__0_mEDA2B44034101F032233300832FA34E59114FAA3,
	U3CU3Ec__DisplayClass4_0_U3CCreateCategoryObjectU3Eb__1_mB1DA6A1138507F1F5C71530888BEA147C441DEA8,
	U3CU3Ec__DisplayClass4_0_U3CCreateCategoryObjectU3Eb__2_mEE3056D008A6D479D2A7B1D9F43EA6A877CBC7A6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	RadarChart_get_Radius_m8318721E7CE43B8B5BEC7DE06452938A10283365,
	RadarChart_set_Radius_m527724F79BA03C6FEEDAD13E69A5B6074ED9CB0B,
	RadarChart_get_Angle_m817D37185EA5005394BDDA0D026EAE5179B605BA,
	RadarChart_set_Angle_m64C56B7B8B36623FC97702C740C44AA2EA599676,
	RadarChart_AddBillboardText_m645DE699A39B06C86EDA30624FDF3BC7250B438B,
	RadarChart_get_AxisPointMaterial_mA7927B95DC8B39B5A9671304B3C850AD1A389DCB,
	RadarChart_set_AxisPointMaterial_mEA25067B67FAEBF70CC7F8D36916F52B32E9238A,
	RadarChart_get_AxisLineMaterial_mF09A5C8950CF29FA4600B6395D86E38ED8507767,
	RadarChart_set_AxisLineMaterial_mE886F785E4B7C1E44369D46E3B624B6772F16289,
	RadarChart_Start_m25ABEB8F0A1C9F4934CFBB8C9026BA7EDF7B0A99,
	RadarChart_get_AxisThickness_m01E8C15C5708A2B60DF0573DB5B1791DDBF33D1E,
	RadarChart_set_AxisThickness_mC4E4ED2C091B6A657AF6AD75D0E1EC8B6C97606B,
	RadarChart_get_AxisPointSize_m9C002B1C338AD0DD46D9A1231DFABF42BCCC73A0,
	RadarChart_set_AxisPointSize_mA084688B46DC33DD3CB045301B32D45422DE3C9B,
	RadarChart_get_AxisAdd_m42BF8E79E424CDC9EF4483FB9E4625CAE96BD6CA,
	RadarChart_set_AxisAdd_m0E7F0A977DE3207E9627B8DA49151CC75B122C23,
	RadarChart_get_TotalAxisDevisions_mDF0636265F972B9ED397CC241D57213E751ECE02,
	RadarChart_set_TotalAxisDevisions_m888E549AB0A70C1F80BA778ABDCBBCA7871DA40F,
	RadarChart_get_DataSource_mDDF96F479AA2B9C7FAF63914CF7653066688BA01,
	RadarChart_HookEvents_m306842507EF6A74AE348C9C9755A4CEA04F27BF6,
	RadarChart_ValueChanged_m89F84280DFAC3F7D94F55D902A339E4C1156534E,
	RadarChart_StructureUpdated_mA96622AF76BE9F0B8AF6C10CA6D8D77679E86DDA,
	RadarChart_DataUpdated_m659B39DC0C75A7A0C4B3646A0DA18AF349899B89,
	RadarChart_get_SupportRealtimeGeneration_m0E533B667BD9ED2470E91D151706C0A3583E5BEF,
	RadarChart_get_LegendInfo_m0BDC4969FD87EE43962C6DFC9C8274DAB4072F24,
	RadarChart_get_DataLink_m065E1235AE5AC6F65EFE09B09C15D1C9984B6A03,
	RadarChart_get_SupportsCategoryLabels_m086B8AF3B826DF7E9DDB8AC7F995CE9712FC34F6,
	RadarChart_get_SupportsGroupLables_m7FB6D8ED10855F9134802486F889A93703D7A74D,
	RadarChart_get_SupportsItemLabels_mD5385572E838D89C73AE38FCDAF53A6CB8AA65A6,
	RadarChart_get_TotalDepthLink_m2F96E3D16046F2228D732F482568501FF273C712,
	RadarChart_get_TotalHeightLink_m87AD917707F607F410FE471CAC8AFF663F9A937B,
	RadarChart_get_TotalWidthLink_mACE5094CEA324AA4306194915DFFBCAF616F2AAC,
	RadarChart_Update_mF6BD08A85BC940337A7A905BDBBDCF61F8164F5F,
	RadarChart_OnValidate_mA76670232387C5623ED6A06A650CC2511011C426,
	RadarChart_ClearChart_mA148229BCABCEA41F146D9AD97EE79A587C0BEC7,
	RadarChart_LateUpdate_m13B67BD9AB084BC67DB62F7D1CD2785DC26992C9,
	RadarChart_ItemToWorldPosition_mCCEE8AD9270F3E41E22020E83E3C62747CBB3B5E,
	RadarChart_SnapWorldPointToPosition_m69FC14A4012B47192ABBF3BA0996B398A1806ED4,
	RadarChart_InternalGenerateChart_m549DB27FABC19B671A2F36F479F9C1EBA62708E3,
	RadarChart_OnItemSelected_mD2570E6F1DF882ED6EF4020C6BD3EB39D99AF959,
	RadarChart_OnItemLeave_mCCCE4A876F76F3CAE2241CD8F97985423A56B42F,
	RadarChart_OnItemHoverted_m983DE106593376BC2BB03D472E7E76A73E5AC162,
	NULL,
	NULL,
	RadarChart_HasValues_m0715B686F30DC04D091FE66B8F57FF9E87C4AB8F,
	RadarChart_MaxValue_m9312A80108F81298488BCA80D37D1035D7E3E6FC,
	RadarChart_MinValue_mB31C6FDCE073B880377AB330355C2C63401387C9,
	RadarChart__ctor_mAAE823A6B63E1D29BDA758759B969D8ED80817B6,
	RadarChart_U3COnItemLeaveU3Eb__77_0_mCD5E6245CDA4737B40897F15288E0A869E9F23B7,
	RadarEventArgs__ctor_mA1E00EF3B49C1667A98A9CBF751D99A2C196A34B,
	RadarEventArgs_get_Index_m62007451B3AA6C66F095A4C35B8B7B8EC5E484A9,
	RadarEventArgs_set_Index_mA4261CF977A216CAF9751F99FC765CDAA82F8A59,
	RadarEventArgs_get_Category_m9311857293FCD862890CAEC7153DFFF54AC0A15A,
	RadarEventArgs_set_Category_m9A98ADC9A460C6B28C355DFF54D344DEB0BCFBAA,
	RadarEventArgs_get_Group_m0006840DB7F009CC5BE3E17EEEBD03B79AB7526D,
	RadarEventArgs_set_Group_mE1D6C61197C2FB44AC097B9F052595AA93D9D60E,
	RadarEventArgs_get_Value_mB6E37324C906F817C5B2E4755A1CA24293C5DB4D,
	RadarEventArgs_set_Value_mA19E1B1C45A5B260E50606F70636DB311AF02501,
	RadarEventArgs_get_Position_mB4AC59E9FCD59B2EE4E4F20F81778D7DFA6DD9BD,
	RadarEventArgs_set_Position_mD64D46B5CEB08F082E4BC6D2B53A714228152CB9,
	RadarEvent__ctor_m9B10C5DDCECCD2F11C2978755F155C3F827A75BC,
	RadarChartData_Add3DCategory_m130E06E50720F05B371D7751CBF383D40FCF4190,
	RadarChartData_Set3DCategoryOrientation_m3F7BCBD6AC90C3F56C3D687029889EC061880205,
	RadarChartData_Set3DCategoryFill_mADF46298559B857D171236FD1791951AD57B2B06,
	RadarChartData_Set3DCategoryLine_m336DEA8A3FF5440B17D58808C7030254D7FF37F3,
	RadarChartData_Set3DCategoryPoint_m876E582161714135D3D6D96A9B35C86CBF301AE3,
	RadarChartData_ChartAndGraph_IInternalRadarData_get_InternalDataSource_m6AC4C3114C247644A7BCF9875F27A89F84960265,
	RadarChartData_add_DataChanged_m99D8EA4C37777EEE904B2472F067B27701E3DE25,
	RadarChartData_remove_DataChanged_m1E3D1F74E6E7B12E51D57747E9E72B85111CD90B,
	RadarChartData_add_ProperyUpdated_m94ED02CCC412FD825345AE1C6D9D000D3EC14005,
	RadarChartData_remove_ProperyUpdated_mCFABC49D427FE3F42604FE13456B70DFC4B01304,
	RadarChartData_RaisePropertyUpdated_m85CBDCA8FA513E2E522A244EF08E8B57BEA94231,
	RadarChartData_RaiseDataChanged_m913925A2ADF57160C1A7B601AD1BE8DAD8125CD3,
	RadarChartData_get_TotalCategories_m64459272A6219D14AF321A0E3CAE11909A42263A,
	RadarChartData_get_TotalGroups_mE126A19EB3F10824DCC0FF3BD3638073A4F28038,
	RadarChartData_RenameCategory_m040D719A27246E2F7337C4B378903A237BC398D2,
	RadarChartData_RenameGroup_m55173E2CF110A173732B96F14C0B2900ABB77AE9,
	RadarChartData_get_AutomaticMaxValue_m348712965E81ACEF31462FE970A71560FAE19924,
	RadarChartData_set_AutomaticMaxValue_m9EE5266D3C4C2FE011C4F6B44D2F3A7EB6ABDC49,
	RadarChartData_get_MaxValue_m790303C375045BDF3033F35EF21BB79E5EFBF6B2,
	RadarChartData_set_MaxValue_mC7F8DAA096D703AC7E4B6FD1A7DCCBC3E61E4188,
	RadarChartData_get_AutomaticMinValue_m49F9D64491C3605E417247C8FCF53D134B350453,
	RadarChartData_set_AutomaticMinValue_mCDDD54C26724124F243DE346E5F33A514FE8CE76,
	RadarChartData_get_MinValue_m921EAB67CF981900348A48CE09CFAE0A559AA0FC,
	RadarChartData_set_MinValue_m632CD37D5224E6E613AA9C34A86C9CD2806AF4F8,
	RadarChartData_Update_mBFC46C0D68C76A5AD53AAA706995E1BC84CD42EB,
	RadarChartData_StartBatch_mC70F5847873D3B2C1904FAF7F4BC575B95535F94,
	RadarChartData_EndBatch_m9F7E8593991858075D5E242E68CA0C5C2532B8B5,
	RadarChartData_GetMinValue_m9C9F4851112F97B1A518AD6E29D5FA50F27678F3,
	RadarChartData_HasGroup_m8695CE055ACD348AD36159DBA1F4898F15459FEA,
	RadarChartData_GetMaxValue_mEB8DB85D9F5B1BCB66EE509917E69B85DDF6A384,
	RadarChartData_HasCategory_m01D7C8E1AB982ABA20AD06AC3E081C9D5BE2A7DF,
	RadarChartData_GetCategoryName_m4A3B0335843AE3C2CBF123078375765BA2408567,
	RadarChartData_GetCategoryMaxValue_m008D3C169C2FF87BC13725FB83FE1639C09CEBEB,
	RadarChartData_SetCategoryMaxValue_m5CDFEFB1322EF5843222773D10DCE73946296950,
	RadarChartData_GetGroupIndex_m2D5768659D2B959A5328DD2C4DE981FFD46DB28B,
	RadarChartData_GetGroupName_mDD7F9D5330A0A19AECEB621E2F6303FB9F4786BD,
	RadarChartData_StoreAllCategoriesinOrder_mD5A61B66E34BD21FF0FF5498283E1162590D5322,
	RadarChartData_OnBeforeSerialize_mEED2F3E6184CEA410AD18832AD3EC5C23E804077,
	RadarChartData_OnAfterDeserialize_m0C45500DB9E5900352C9DAC5CFE75F1C4967E0EB,
	RadarChartData_AddCategory_mE5CDBFD3F5AC1BE0A1D68EC8274091A8BAEAA0AA,
	RadarChartData_ClearGroups_mFA3ECC13EA8F0E9E6D2D429505214DD865CEBC0F,
	RadarChartData_ClearCategories_mB51296D801643E4D1655A36D181185A7B412F8DB,
	RadarChartData_AddCategory_mE8074171A469325092AC49FC59D1BF2C00B76ECA,
	RadarChartData_AddInnerCategory_mE94BC7FC9215A338F7E1D7D6610F95FD2E461B26,
	RadarChartData_SetCategoryHover_m7EA49FCD93E367C16662ACD8CD32A2E5ACE1EBCE,
	RadarChartData_SetInnerCategoryFill_mC9601A1BFDA8448DDF46765B0966FF1F082AA95E,
	RadarChartData_SetInnerCategoryLine_m9FDC2510CDFCA4449BCB19E7528D094EA3A7C811,
	RadarChartData_SetInnerCategoryPoint_m81E11EDE8B768EE225813DCCF3743151C3D97BE4,
	RadarChartData_SetCategoryFill_m48C4D1A12E900C13414192E4F4531F0195C02C1D,
	RadarChartData_SetCategoryLine_m3FF12F349A37BC994A5B07DBC020A162A9794200,
	RadarChartData_SetCategoryPoint_m626597C9774A604D5EF11923520D927F440F44AF,
	RadarChartData_SetMaterial_mB358824E191056E603B0FD16713D7951DD2D650C,
	RadarChartData_GetMaterial_mEE8559CBC26738273C3AC5B9D8E8F852E4419828,
	RadarChartData_SetMaterial_mA4B2759040229EBBA347ED2736F39187AE48350A,
	RadarChartData_RemoveCategory_m9D0A3334267E96F3E4919F469261A39EDB1C1327,
	RadarChartData_RestoreCategory_mAC5189FF3298E0B9A9D69FAE5A222C6C99E63539,
	RadarChartData_RemoveGroup_m924DC8293C1D58D47E016D0AB40D1C6F9C8B79F9,
	RadarChartData_AddGroup_m1F74100E9748C2D4003A034BFA6F6292AB880904,
	RadarChartData_GetValue_m002560F54544DF76554D66C4D38FE6B9E30BA69E,
	RadarChartData_CheckAnimationEnded_mAD0C5D51EB0F3904B6512C28EEF207AC7F6E1A94,
	RadarChartData_FixEaseFunction_m6D500A3AB6A269DC71B5822077C634AA14CDDD43,
	RadarChartData_SlideValue_mD20632B9B3E6ACCACD827E9DAA5300E1333A8F68,
	RadarChartData_SlideValue_mF88966F4D0C9CCC81E6C3FEFC880C1ED01355089,
	RadarChartData_SetValue_m3B44AF655063A77B53C29A048352EE3B085AB470,
	RadarChartData_SetValueInternal_m9DB16C8DF97FB4D4CC5EB5F879F5BEDD239B8584,
	RadarChartData_ChartAndGraph_IInternalRadarData_getCategoryData_mCC4D8AE683FED0FDCDF1404B74FA1429D920E998,
	RadarChartData_ChartAndGraph_IInternalRadarData_add_InternalDataChanged_m741C827E0D695F9C3CC703B400352E69AF3FA5BB,
	RadarChartData_ChartAndGraph_IInternalRadarData_remove_InternalDataChanged_mE513DEBBBF0B81FE4172E3532AE52D65E9B3FAD2,
	RadarChartData__ctor_m32273BD441DD9474CB345D14AEAD3900201EF0F9,
	CategoryData__ctor_m9832E3233589FF77B473687D603FB448CCE6B536,
	DataEntry__ctor_mB775C8234DB7D698783D05015656305277BF74BE,
	U3CU3Ec__cctor_m2AD9301513291F7B2B6528A5F2CA72BC540816A3,
	U3CU3Ec__ctor_m3DEFE7B4A55719CD1DAE2C06DD154E76E2BC9CD4,
	U3CU3Ec_U3CClearGroupsU3Eb__59_0_m54484CBB1E8A76B71B9D97DA61F6BEAA6818D0CF,
	U3CU3Ec_U3CClearCategoriesU3Eb__60_0_m0A8F4F4E4299B0CAFAAFADA47E48F49FD6593ED7,
	RadarFill__ctor_m0313F6B94057C40CBFB2FC6C109451B9DA25C4EC,
	RadarFill_SetPath_m233FEF3BF93EF2D5D833174646306BCE352B03D8,
	RadarFill_UpdateMaterial_mC9982E2583E6F5E1A0937155D68E649E4C182746,
	RadarFill_OnDisable_mF0C072E47D293E8E2B1F5EEA6A61C42640B5444E,
	RadarFill_get_material_mE06D8FE51309D3A6AA0C35C4A07B86232657271A,
	RadarFill_set_material_m3314AD9E5E8660D7B7D268B4B7A26D64CB32EC7D,
	RadarFill_InterpolateInViewRect_m7D35B6BFE5E16239827A14160871D278B7FB9E7F,
	RadarFill_getVerices_m93B92F5B9E70EC783CF194DC2AAAD21F7DC12707,
	RadarFill_OnPopulateMesh_m723D5D656F46320E0B6218121035CC29F593714F,
	U3CgetVericesU3Ed__11__ctor_m95EAA0E125642D785F6170A26A63A985795114EC,
	U3CgetVericesU3Ed__11_System_IDisposable_Dispose_mA707AEC25B6F42499A916D5625F27CC57C387E70,
	U3CgetVericesU3Ed__11_MoveNext_mC280799E2CD850DF504CF0A846FBACC3AD8D96EF,
	U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m0ABBFDCEE2D7145EBE071FCFE6684F6BAFEC3AF9,
	U3CgetVericesU3Ed__11_System_Collections_IEnumerator_Reset_mE28A7F9BD47AA43584DAFA502888ACF0235FB4F3,
	U3CgetVericesU3Ed__11_System_Collections_IEnumerator_get_Current_m53326F4FE9B852D2151D3849221DE6279144AAFF,
	U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mFE0741653FF30F25813799307B8423F0B24C7C53,
	U3CgetVericesU3Ed__11_System_Collections_IEnumerable_GetEnumerator_mC4C94D6FA75874BC56F1392AE45985B0A0F0F00E,
	RadarFillGenerator_EnsureMeshFilter_m708F04B71F0292D732B6467115059A2D251FC95A,
	RadarFillGenerator_InterpolateInViewRect_m219D9F3BF7B6C39BC7DEE426D764A02D23E87F0B,
	RadarFillGenerator_curve_m9CE5144398806A9D062F105693339CE7475F293C,
	RadarFillGenerator_getVerices_mF7C8A6EBFC00C77EDB871799777143548EC570EF,
	RadarFillGenerator_Generate_mD633997BF5ADC452ECB5E159DFEC57BB6F2DA55B,
	RadarFillGenerator_Clear_mB624624C76C84837D8FB75FBE6CDCF259EB40213,
	RadarFillGenerator_OnDestroy_m7DFC9D02618E463B3FFA68C2312938FC77D72134,
	RadarFillGenerator__ctor_m2AA33BDD3FD11DAB3D9AEA0AA87CB1A1E44CBE7A,
	U3CgetVericesU3Ed__9__ctor_mAC242DA1B337EB73A1ACF54B5008C8982895E7BE,
	U3CgetVericesU3Ed__9_System_IDisposable_Dispose_mA5B4335205957942C2C19D8CF215EE867ADE91DA,
	U3CgetVericesU3Ed__9_MoveNext_mA19A6718F5D410AA11B9E4A2742E0CA0FCE2ADC1,
	U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m26748669262E86E2C5B4B00464074D5101846F44,
	U3CgetVericesU3Ed__9_System_Collections_IEnumerator_Reset_m2806ECA59982D7E816ABAEE95594A91198113387,
	U3CgetVericesU3Ed__9_System_Collections_IEnumerator_get_Current_mEC742CBC4BFF915F4437AFE99135CE5D95A38D2C,
	U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m18B10F0B05365FD3906E1B6F7909DDAB07663928,
	U3CgetVericesU3Ed__9_System_Collections_IEnumerable_GetEnumerator_mE68D27E149F9F1E558455CA8B7C987FB5485CD25,
	WorldSpaceRadarChart_get_AxisPrefab_m1B090BDD3151E89FDB6E10141B1BFD962F9E7A47,
	WorldSpaceRadarChart_set_AxisPrefab_m6858D3C6CD8F6D7C3BECD45CE4BDE0C44CBC651C,
	WorldSpaceRadarChart_get_AxisPointPrefab_mF1BF25C84798B3AB9E7E8519F27E3BDA4C6190FC,
	WorldSpaceRadarChart_set_AxisPointPrefab_m3784BCAC3AAC49DFA8F0E52B4F26603C12A5DB31,
	WorldSpaceRadarChart_CreateAxisObject_m364FE323E71618652FBFA481DB43CD32F70E5782,
	WorldSpaceRadarChart_CreatePrefab_m15318BAA6D75AB9D4261B44DECA9327AE3E9B144,
	WorldSpaceRadarChart_CreateFillObject_mC638A0CCDC17755DD65A6B5835BDE6CEDF984A46,
	WorldSpaceRadarChart_get_IsCanvas_m56F7FE3420454551D10E54FA7FFE4CD64736309C,
	WorldSpaceRadarChart_CreateCategoryObject_m67D55F775C49BF3014B09775CAA1202CC50A1AF4,
	WorldSpaceRadarChart__ctor_mA274F974380017450FFD8B7D1092F18A8472D93E,
	ScrollableAxisChart_get_ScrollableData_mF6B505446F40A825D01C5D05B9C0B276181BBBBD,
	ScrollableAxisChart_get_AutoScrollHorizontally_m0119650CF15A68143E03351A5CDB8EC6BE591744,
	ScrollableAxisChart_set_AutoScrollHorizontally_m5ED34138D96AF142165479908982263DBA0D7469,
	ScrollableAxisChart_Update_mB88F71FF8803E8007544F908D203FA0A6E79CC93,
	ScrollableAxisChart_GenerateRealtime_m6CD2F771D39503CD10FAA0B2672CFE408CD6D066,
	ScrollableAxisChart_GetScrollOffset_m6AA9D13986C5D2E3C46EDAF92AAF042CA819E7AD,
	ScrollableAxisChart_AddBillboardText_m26DA501F3E62CCF8AB9A1C86845444E567E4A462,
	ScrollableAxisChart_ClearBillboard_m5BC2AE0997B7E6590F993BCA2A0062212794662D,
	ScrollableAxisChart_ClearBillboardCategories_m2AD60E7521A0CCFD9B44DA4A9BBACA734B42012B,
	ScrollableAxisChart_get_Scrollable_m20D6DDF3AC3C161A0D93610B3244C1A7ABAE7257,
	ScrollableAxisChart_set_Scrollable_m9A8BDC045D11031CA97020A4D65682F676C72C2C,
	ScrollableAxisChart_get_HorizontalScrolling_m89B95AEC4C79BEF0F7D03B12E72EA090097ADEB9,
	ScrollableAxisChart_set_HorizontalScrolling_mCDA5CCBF57D9502DCA231D3B40FDB1704A29877B,
	ScrollableAxisChart_get_AutoScrollVertically_mF27BB4C25493A8D4B866E53E93563721D980C005,
	ScrollableAxisChart_set_AutoScrollVertically_m6FB6FB491B56531D6461AA9ECC6B278BF3E8E947,
	ScrollableAxisChart_get_VerticalScrolling_m79372E57FD6B44B33DBB5744CF5C555B476E906B,
	ScrollableAxisChart_set_VerticalScrolling_m6271CE083E9237183DE9B58EF8C3CB2AE49F39A3,
	ScrollableAxisChart_get_RaycastTarget_mEED1B5750596D66777103EFA63B16209B744E9C9,
	ScrollableAxisChart_set_RaycastTarget_mCCC2BA8FCCF02988A16205E5B23E43C6522DFD34,
	ScrollableAxisChart_PointToClient_m92EAB332DDDA386A003208EBF3E7BE906FF3B9FB,
	ScrollableAxisChart_PointToClient_m3777F4C5CD77CDA447AF211F98198CFFA307D7C5,
	ScrollableAxisChart_PointToClient_m723CA78010FD72D2CA6FFEDB5865F60D06BC2454,
	ScrollableAxisChart_PointToWorldSpace_m3914970E17E7F0527513EFFB60A30C352088BCC5,
	ScrollableAxisChart_PointToWorldSpace_mDC31114BA9343ED423C8B60D327F25724A5619E3,
	ScrollableAxisChart_PointToWorldSpace_m3D8ACB12C8D0AB38D9B2CA4CAC17C6A33BD5C697,
	NULL,
	NULL,
	ScrollableAxisChart_PointToNormalized_mA13C59E2931C28D63CF24669AAFEE1A51E69DF8E,
	ScrollableAxisChart_NormalizedToPoint_m3214950D806AF4830E2597271BFC39C381FCC736,
	ScrollableAxisChart_get_PointShift_mAD73FA814A124CFD102CC0E0CE0D093CF3841386,
	ScrollableAxisChart_get_CanvasFitOffset_m10E03FAD4C331CC26EB009F8F37097CDCBEC6FB9,
	ScrollableAxisChart_PointToWorldSpace_m7EEDDAFC797F298F8586FAA7DE548336B0807D11,
	ScrollableAxisChart_MouseToClient_mA8EE4656273AD78AF80ACA4380E4C42AED8020D0,
	ScrollableAxisChart_PointToClient_m45A693188B6BBB8DCBD9FD1B442F877ADC93B33E,
	ScrollableAxisChart_TrimRect_mB40DAD04B18B238E3343D647A858FD7A4624CC78,
	ScrollableAxisChart_IsRectVisible_m9D14A660695D71226B59964D49DD774ED185D744,
	ScrollableAxisChart_RectToCanvas_m68D2BFA24F1847EE1B56CD609A6552EC54999AB2,
	NULL,
	ScrollableAxisChart_get_HorizontalPanning_m4D2E6510CC1061CE22D3EF0F6F36F88B5756D7F1,
	ScrollableAxisChart_set_HorizontalPanning_m11D6AD0F91299ADDCB2DE627430C13093D3B4E71,
	ScrollableAxisChart_get_VerticalPanning_m4D040CD390A540F7F11D3402267319887DEA756C,
	ScrollableAxisChart_set_VerticalPanning_mB37E5FBCDE3448752DF6DF516433F01B9DDD15CD,
	ScrollableAxisChart_CreateRectMask_m337026D0A97ADE0A0162371636D7B2A05297B58D,
	ScrollableAxisChart_ClearChart_m11BB4672C0CD5DF042764411C7675B4FAA86E768,
	ScrollableAxisChart_StringFromAxisFormat_mD391FEFB4D49311A8A505EE7B940481B09F81C3A,
	ScrollableAxisChart_StringFromAxisFormat_m459D838D430A4E89026F3E64B51FB4908821892F,
	ScrollableAxisChart_GetScrollParams_mC50B26C21345FBFB4AA4E16AF0E277A468CC25F1,
	ScrollableAxisChart_MouseDraged_mA0C6908F207EC8FBF64855B4B1D82746B085280D,
	ScrollableAxisChart_HandleMouseDrag_m73A25D0D90F58AD361FC68DC62C9B809EA014E2D,
	ScrollableAxisChart_TriggerActiveTextsOut_m81E5F13BCC7C7FCFF62E97018B9D19858D012B14,
	ScrollableAxisChart_AddActiveText_mAEBB053AEE6A03A4A79F8789B418A9FF09D1870F,
	ScrollableAxisChart_SelectActiveText_m174B8A7A926499B5374DA1C9E546D80BDBB45B77,
	ScrollableAxisChart__ctor_m267104CD7AF077C36E087B856ABD8125157C45F7,
	AlignedItemLabels_get_Alignment_mECB6C3B817CB67F8F417EB5A99BA99B0AF5BD451,
	AlignedItemLabels_set_Alignment_m831FC445E8263B1C4F7C327F834634A853D50E7D,
	AlignedItemLabels__ctor_mFA74C6A2F31A81643C7A400FA5685A3279997809,
	ItemLabels_get_FractionDigits_m82BF3FF9409EF8305A14022754B7857ADF6DB566,
	ItemLabels_set_FractionDigits_m7B02640144572DA0EC5C7C927E8FADAE316B59A6,
	ItemLabels_get_Assign_m088722D6792ED742B2537A592938FE18ACA27283,
	ItemLabels__ctor_m7F365D10C7DC8760ACAE0AB08ABEA2B50098F698,
	ItemLabels_U3Cget_AssignU3Eb__5_0_m961E93586578FB887F59CD9628AEC9CE40E8C929,
	TextDirection_Start_m52920200EF741582D800E1FBE7637BC988D3B040,
	TextDirection_SetTextController_mA891F1CAD539228F24B23B187786B0B5AFBD3EA4,
	TextDirection_SetRelativeTo_mA1BF2964DA4802337856175D7E014381B1C6A5E3,
	TextDirection_LateUpdate_m681E8C5668E9C3A25F1C42F708E76A5C34900CE1,
	TextDirection_SetDirection_m9202893CE4705CA57CA37251F35ED252E3AC120A,
	TextDirection_SetDirection_mA61EAE1203BB917D55BA197CC1A877358EEC8916,
	TextDirection__ctor_m25491B9E3CCC345F953E9DBC6E477DF736747987,
	TextFormatting_get_Suffix_mD60BF477085B8EF4BA141764BA3BB306814E98A8,
	TextFormatting_set_Suffix_mA100B03ABD880619CDAB53DF6ABE006B71F3E0E1,
	TextFormatting_get_Prefix_m3D4865298EF50EE7DD09C32F5251126951C25AAE,
	TextFormatting_set_Prefix_m8F85B66321B6FB246514F0FA9D5C2D60CBA684E2,
	TextFormatting_get_CustomFormat_mD981569C543D767A4E3D64EC98B1CC5C29E03CE8,
	TextFormatting_set_CustomFormat_m9666A3CD1B30F98D3828D40681C43D09DE5B73F2,
	TextFormatting_add_OnDataUpdate_m2F65466175314C9D5ED7FE225FE1BD80ADBEDBBB,
	TextFormatting_remove_OnDataUpdate_mA5F909354E837998B946D9345C788F83D4CDF9B9,
	TextFormatting_add_OnDataChanged_m8C1F23C5AD0504F224A32812FA950812A484F1AA,
	TextFormatting_remove_OnDataChanged_m1FCA55929D4EFD9908E2D572D8240F8069F0497F,
	TextFormatting_RaiseOnChanged_m869D7B0A2965421425FD54F74D68F175DD3D4422,
	TextFormatting_RaiseOnUpdate_m0632B9BD9178D8DBAB5254C9E84ECFE9D0E46136,
	TextFormatting_ChartAndGraph_IInternalSettings_add_InternalOnDataUpdate_mE1B274D666F185B5481859D4A53C65A21CEC5549,
	TextFormatting_ChartAndGraph_IInternalSettings_remove_InternalOnDataUpdate_m063CF339F9C62982533F5369DF08872AF9708A97,
	TextFormatting_ChartAndGraph_IInternalSettings_add_InternalOnDataChanged_m45291A45F01E4DD7688D1E6546506ABF12D1E007,
	TextFormatting_ChartAndGraph_IInternalSettings_remove_InternalOnDataChanged_m6A4E4E2D0A8593DD2B2111D927D80A92E343F0DB,
	TextFormatting_FormatKeywords_m6DE37BFDFC22EEB565DBD764002D5352FF1C0237,
	TextFormatting_FormatKeywords_mC8218DC10A38C97C40A0DB73D503EEE5E5C9A652,
	TextFormatting_ValidString_m1A0C04C7C125E882D77A7AA1408E96BCBB6AF836,
	TextFormatting_Format_m84D20D445C001711330E560E1109B6B1F8E89AC1,
	TextFormatting_Format_mC7F6EF9E7F6F13F2FB2024C76244ED0679F21EC6,
	TextFormatting__ctor_m25E5FC8D31710F0A9BC49FDD34BC8BB86B38AF0E,
	PieAnimation_Start_m8B31A7265C85482C8A431B865A81316540955B35,
	PieAnimation_OnEnable_m9D4B16130434DF8A08DB82FBB4BF0DB8EB66A185,
	PieAnimation_Animate_mDD9ABEA9092357464BD61530DA1E471FC8E44A20,
	PieAnimation_Update_m26C407791F9536275651E92FDDE20DE333DD8E9B,
	PieAnimation__ctor_m05BA7F8B9535B821DA5C8AB1332F13A22D9C6168,
	SelectScene_Start_m28D362FCD02A06755ABC5219946A66055A2EAA38,
	SelectScene_ChangeCanvas_m0EABB3FF2CD8616219670C6A03E48BC415A52F51,
	SelectScene_Select_mBDB0B21D990C0124D7C8165E4A852BD415EF162E,
	SelectScene_SelectMain_m7B23C7E5C250046326542829CDA7ECE32BA37749,
	SelectScene__ctor_mAD684180B1324A8ACC6C896AB8357975FBCC9053,
	U3CU3Ec__DisplayClass6_0__ctor_mD7F0B3A3E430A93B3DE0DD0272749EFB146F2F8C,
	U3CU3Ec__DisplayClass6_0_U3CStartU3Eb__0_m6094D067135F047261703D726A6AAC79533E633A,
	BubbleGraphFeed_Start_m47505E34E07E5518CAFEB7A3371313536AD25382,
	BubbleGraphFeed__ctor_m117A1C9DF31730D9FBD3DD33B15A7E48E8EADAC6,
	CanvasLegend_CreateimageDictionary_m43DFF4711EFF207CE0F0897C8A33A376D6AA0157,
	CanvasLegend_get_FontSize_m11FC61224312844E27302F5372FA7A107BB01751,
	CanvasLegend_set_FontSize_m5BA01C235A686523E10B3846C7C7F30E28CBBA74,
	CanvasLegend_get_LegenedItemPrefab_m22A1217E0A1B0B27AEC45CDF5751DDCF296AAA2A,
	CanvasLegend_set_LegenedItemPrefab_mFFA536B2CECD0FE6A658B84A1580EC0EAABFE970,
	CanvasLegend_get_Chart_mFDF2B73F82DD7EFC3719D707CB43E82DD1697E27,
	CanvasLegend_set_Chart_m2F990939D41CE727B4B0806CFC49E6CCC3FCFE07,
	CanvasLegend_Start_mAE84920B6784218D10B29F572B484AE7A890255A,
	CanvasLegend_OnEnable_m87D5E1E5D20CD36798C6DC797F0802B3AE173350,
	CanvasLegend_OnDisable_mAC95861AAD6C5D4F124773BEFF074C214D4B7C87,
	CanvasLegend_OnDestory_m8669EF0C7388E33C5A09221FEDF3F30605E78AF8,
	CanvasLegend_CanvasLegend_Generated_mD793D8281E062F41E21E007E112C59CDFAD61958,
	CanvasLegend_OnValidate_m9AAFDDB05D385011F0288325CE2F498112B3A01F,
	CanvasLegend_PropertyChanged_mA71794893BF1F896DA64A4071E90068C37E702B4,
	CanvasLegend_Clear_m607799FC14B73F07176AF7DE2323B9273341E734,
	CanvasLegend_isGradientShader_m84DDD48676C5B89E2FE672F6EF04AF6D3F78A431,
	CanvasLegend_CreateSpriteFromTexture_mE32C880FB78FC7DA4A39F3BBE0E7DEE46D086A9B,
	CanvasLegend_CreateCanvasGradient_m4DDBBF0B228DD6CBBADE960FB9AFF8C50932ABAA,
	CanvasLegend_Generate_m5F8E99B064158CFF9AF591C65FFB9E6F66D03F64,
	CanvasLegend_Update_m5411E860A625AE7AEBD2E8AEFF4D7CE62B8455BA,
	CanvasLegend_InnerGenerate_mE0FCEB937FDE2871D578EE38AE76CFCF63DA0C0C,
	CanvasLegend__ctor_m0A3BD0E8C09083359C9F47CE082649421F24EDEA,
	ImageOverride__ctor_mCAC69304E42166E716B9C50BC7562040B20D1CE8,
	CanvasLegendItem__ctor_m58E47F56D9CAB3B1F84094D65CF9F7A66F1CC107,
	ChartDuplicateItemException__ctor_m472095D9E1987B99C83EC00395BD02E289A63985,
	ChartException__ctor_m10F2DA5E37A9D03341BD0FF2DA2F8439C1FBD429,
	ChartItemNotExistException__ctor_m0CD84B036E6213139DFEEF95DF4E859BDF8C97ED,
	ChartColumnCollection_get_ItemTypeName_mCEACE72AD02A54F3E2EA6C7DDB6EE50E70B4D165,
	ChartColumnCollection__ctor_m10B7EB73DC78BDF6223E8DDBCEA4BFAE73C1EB18,
	ChartDataColumn__ctor_m9CAE894BFD62A6C4BF4BEF87E6A2DD084EAAF935,
	ChartDataItemBase_get_Name_m1A4BDABA4661B4EDF30DB1E4FCC970E161467C1D,
	ChartDataItemBase_set_Name_mAD91AA6A39FFC10A2A708166E3EF6C70E6203926,
	ChartDataItemBase_get_UserData_m7366DD757A7150DDD8A9BED5BD84833CF439A79E,
	ChartDataItemBase_set_UserData_m68BB91284256A958A1965AD38A7F65970958CFCA,
	ChartDataItemBase_get_Material_m884D84994D5DFB145F7F80D646ED047675946B82,
	ChartDataItemBase_set_Material_m556B6A8463CE116F5C41CB9A7C82CD866F333A43,
	ChartDataItemBase__ctor_mC1F6B7DCAEB6418FF77AAA8B260FD35C6B583979,
	ChartDataItemBase_add_NameChanged_mAB59E2935A5636D41ACCABCA450D3AC205B6BCDE,
	ChartDataItemBase_remove_NameChanged_m16AFE4B4FF6A7A5DB09EE7E5FAAF7AE835A3E030,
	ChartDataItemBase_CancelNameChange_mDC7C7FA5119AA0B4B50D01E084A8948AD1645F6C,
	ChartDataRow__ctor_m170E0D453D698E5B865DB80B05E95815509719A6,
	ChartDataSourceBase_add_DataStructureChanged_m3DFE5025CFA79F2777ACF301467AEE3227CFCD43,
	ChartDataSourceBase_remove_DataStructureChanged_m8AAA481D8DF9741946B75D75912507CD4D14179D,
	ChartDataSourceBase_add_ItemsReplaced_mFA7A223CBFD7512E5E929AE0E0A717DE475AFFBA,
	ChartDataSourceBase_remove_ItemsReplaced_m8102FBC9B601DCBA582A2DD9B1B9F41D9F21B9FD,
	ChartDataSourceBase_add_DataValueChanged_m46AFF31FDDA9040F4A0E8FEDBB8AA5F618F72F6B,
	ChartDataSourceBase_remove_DataValueChanged_m2B90A93D01B42AB10A91FA075051E2B995B982DC,
	ChartDataSourceBase_OnDataStructureChanged_m0860BC4844E190490E025D7C856611D81C0DD1F3,
	ChartDataSourceBase_OnItemsReplaced_mDDA31C181C7CE7CBE46DAEC17B32869CBBA27AD7,
	ChartDataSourceBase_OnDataValueChanged_mAFC19B6C9432BB5F12BFE77BBA965AFA597DB4D2,
	NULL,
	NULL,
	NULL,
	ChartDataSourceBase__ctor_mFE3E99C34B9C46A1ECEE154058F410D176CA3386,
	DataValueChangedEventArgs_get_ItemIndex_m567F892ACB8F7484334FE772FAD6772D47BA3168,
	DataValueChangedEventArgs_set_ItemIndex_m3F4E84261B9A7E33423D30D574AE5CA803D1C070,
	DataValueChangedEventArgs_get_OldValue_m0982C5004334F1A50D175D9FE9164DA2A80F9C45,
	DataValueChangedEventArgs_set_OldValue_m3017E9DAA2BC8F3144A12C5623B7EEABC4F0C32B,
	DataValueChangedEventArgs_get_NewValue_m58B94C77C099C4EE56C72968A6FAD8A45F9A9F5A,
	DataValueChangedEventArgs_set_NewValue_m9560AAB634ECBA050B2023D8CF4F96D05E99ED1B,
	DataValueChangedEventArgs_get_MinMaxChanged_m52927FBE8B2DCCE7C6C5F22AA9559C6EDFE56FDC,
	DataValueChangedEventArgs_set_MinMaxChanged_mE75097F31ECA36DF1DED4BD67CF3BEF9CA8E1541,
	DataValueChangedEventArgs__ctor_mE9A533BF6AFACA01151D246BDB5D9B1CACDA5D64,
	ChartRowCollection_get_ItemTypeName_m9AAD9DC6F3644EC040171ADBCD178401CEA766CB,
	ChartRowCollection__ctor_m5B54347CF12CD85575586913DB873AA56006ABBB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ChartItemIndex__ctor_m4941151A043FAF2B807F6DA558F40770D70AA68C,
	ChartItemIndex_Equals_m3ED9D2A4A198B737C7A5D0AF25A359CF6582C6A9,
	ChartItemIndex_GetHashCode_m34B5BD4640C9BFF87E9B100043FA0936960CE5F4,
	ChartItemIndex_get_Group_mBBD268E7B2942B2B9F44E76CF11463EB2247B92B,
	ChartItemIndex_set_Group_m1FD2ECC0C5E3DE8D62E22CF59AB13BB2C63E940B,
	ChartItemIndex_get_Category_m65B6C653CB412F5D8B8A8E8E34759FE50B81C3A5,
	ChartItemIndex_set_Category_mF159D3DEB39D5C73D7ECE7448170A56B9488F5E4,
	AxisGenerator_InnerFixLabels_m04B10787F75A30F78CDAFD2BB2D484C646E70CE8,
	AxisGenerator_InnerSetAxis_m965951C8051950098B7B51A49ED807D62CC2E236,
	AxisGenerator_FixLabels_m6F2D034508501859E8B5CD23D92AB20EB62F7EA3,
	AxisGenerator_SetAxis_mF18009916DBA9522B0AF968E05EF11FBA6EB4F51,
	AxisGenerator_This_m542C8ED27846766C425D159D20BD38E739CBD239,
	AxisGenerator_GetGameObject_m36B5AC2E1F5EE76A4AC78D731E0363D3E1FF48F0,
	AxisGenerator_Start_m41BC4CEEF0F59B141AC681D2A997A8194B8C8EC2,
	AxisGenerator_OnDestroy_mA052767C7A8ADDA4310774E19CA9BFD3962A8E03,
	AxisGenerator_GetTiling_m5FB69574A741BB7DBBF1A4CA08FDE78A4176F772,
	AxisGenerator_Update_m087962844BAA3E28F061DD5375C9FF0EE21C1CC8,
	AxisGenerator__ctor_mA7EA9076FE53A1D5BC76AC31183173988AE25065,
	CanvasAxisGenerator_OnPopulateMesh_m492525484E4BE7E2D2D2F7D386E0E81B879FEDAA,
	CanvasAxisGenerator_AddToCanvasChartMesh_m42E2453727FAA2EAEFE79DD2C0D38E4BEA96936C,
	CanvasAxisGenerator_OnPopulateMesh_mF3EC87C795A2C4BED0312F0647BBE17A058EDA1C,
	CanvasAxisGenerator_FixLabels_mECCA8953C761E8844939FF15A74924FE672CF872,
	CanvasAxisGenerator_UpdateMaterial_m11D57D722B4534D79425E68062D346B37D95C789,
	CanvasAxisGenerator_OnDestroy_m20642C4884C7DC636B0B84C82C174D6E37D54A2D,
	CanvasAxisGenerator_GetTiling_m74F9BA48027EEA8A3C947D901B6202255445384D,
	CanvasAxisGenerator_SetAxis_m2782DC9E983468DD535AF3F39A640D90E33854B6,
	CanvasAxisGenerator_Update_mD8A6742AB2128ED6469AE00A2FE99D607ACFE785,
	CanvasAxisGenerator_GetGameObject_mD0DF412ED4B864F1102293CFFC94EEB6D739C5AA,
	CanvasAxisGenerator_This_mF13963B6497D6E628B9BB7CA2B5423324096DBEA,
	CanvasAxisGenerator__ctor_mE31297F392611291BCC38579703DF7FAE7813372,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void ExtensionFilter__ctor_m005AD482A024C782159F183FFD5579DBF4CD7887_AdjustorThunk (void);
extern void Enumerator_get_IsValid_m0DF87B70D0CB8C46C3B337005248A54B7C86979F_AdjustorThunk (void);
extern void Enumerator__ctor_mF85148FE3692D3904C073278CFE8716EB9B4B0E2_AdjustorThunk (void);
extern void Enumerator__ctor_mA42ACFD81450787E450330C66FE5CBB9FD0644EC_AdjustorThunk (void);
extern void Enumerator_get_Current_m80F00207F02904E3A63CA549AD1E34DB016C4A47_AdjustorThunk (void);
extern void Enumerator_MoveNext_m9947F7EE949531A65662E0BA157E3534993C36BF_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mA049E3670B0C9E04EFFA9CD8702AA9A74DB9C718_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m65F47E21C6E8DBB2A363B8FCF67709863FF36C35_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mC832E5CC32931EE67681635AA27CF2ED262630ED_AdjustorThunk (void);
extern void ValueEnumerator_get_Current_m285C54105C3F05F978CD31F37025375C39C83542_AdjustorThunk (void);
extern void ValueEnumerator_MoveNext_mEA96FAD65F2306BBC4B323984BD480FE6D2DB5AF_AdjustorThunk (void);
extern void ValueEnumerator_GetEnumerator_m44A09F9E247CBB2F7542237EF47BD0A985FC3663_AdjustorThunk (void);
extern void KeyEnumerator__ctor_mD632C7FC751F302A79EC4073C7DFF80C496266F6_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m7D1CC885A67D308EED89D61D67DBD02D4D17DE79_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m7F24F46E4952C1FC737D3F638603E33208952562_AdjustorThunk (void);
extern void KeyEnumerator_get_Current_mF0ABCA5D5FEBDF2C7FC8BFD989D44026434E463B_AdjustorThunk (void);
extern void KeyEnumerator_MoveNext_m063B0761C6BC5BB84B8A9ADEBE965D51E8958D2A_AdjustorThunk (void);
extern void KeyEnumerator_GetEnumerator_mA4473824DD3BB4D3421080111235E5A58757FF5D_AdjustorThunk (void);
extern void CandleValue__ctor_m3DF3507ACFD389F979CEA1D114D7455CE7B62BB5_AdjustorThunk (void);
extern void CandleValue__ctor_m113D5BA7F228439CB190BA3CE617FB07BDDAFBBF_AdjustorThunk (void);
extern void CandleValue_get_isUp_m327DC66B9BE3EFF66FF5299A41E200883479E80F_AdjustorThunk (void);
extern void CandleValue_get_End_m2431E9875D452C1194BBC5FC3FB8668A4F9DAE52_AdjustorThunk (void);
extern void CandleValue_get_MidPoint_m1C6D8595E8D75F3D474E520A562D457CCBD8232A_AdjustorThunk (void);
extern void CandleValue_get_Max_m35263E3FD9F3780330E691CC51D241D442D8A228_AdjustorThunk (void);
extern void CandleValue_get_LowBound_m916E8CBB1CCE4211DAFE86888125AB79A57F329B_AdjustorThunk (void);
extern void CandleValue_get_HighBound_m954D4BEC75899641B1CCB5E0792E9460F80BDCF0_AdjustorThunk (void);
extern void CandleValue_get_Min_m93794657F2D8ED00CB5B56EB649E5278FECEB04E_AdjustorThunk (void);
extern void AutoFloat__ctor_mFA106E4A358AB6F23F2C124C4C9BD3811B422C1A_AdjustorThunk (void);
extern void AutoFloat_Equals_m33B82696A2015E983C87F70E380C9942593D0BFC_AdjustorThunk (void);
extern void AutoFloat_GetHashCode_mBA93BDD7EA6E5F3DAF7F2B880B8DD0CA23539989_AdjustorThunk (void);
extern void Line__ctor_m4389E71B32F966E8718C9FAFD8DC7C9F6E2110C7_AdjustorThunk (void);
extern void Line_get_Degenerated_mC903C588921C51C16C8A9EDE11AACCF1A4C759B6_AdjustorThunk (void);
extern void Line_set_Degenerated_m378A5401A9BAA385E72BC9885DC3B53F1414B451_AdjustorThunk (void);
extern void Line_get_P1_mC750288C96B6CE408C61A85DF57B29B463D10C0D_AdjustorThunk (void);
extern void Line_set_P1_mC38DFAC465CCB453B3B4AB7E97A447590CC89965_AdjustorThunk (void);
extern void Line_get_P2_mB960A6BEF78A9B952DAE1A299D67CFC2012E2A1F_AdjustorThunk (void);
extern void Line_set_P2_m160F2F164EF038D1F32214DE28204AE9683D33EA_AdjustorThunk (void);
extern void Line_get_P3_m06A9A176422365C1575296A1F25FD5DEF4D99DDD_AdjustorThunk (void);
extern void Line_set_P3_mCAF01F4FED931AA69E7246B0FCBB544D303C7B70_AdjustorThunk (void);
extern void Line_get_P4_m8283F3E3C3EFF22F76BDAE1934F53F5175BD2616_AdjustorThunk (void);
extern void Line_set_P4_mC6DD8CC9AB985D8FB738267F457D4C34250B9E6D_AdjustorThunk (void);
extern void Line_get_From_mE45AE1DDC8A9D9EBF61227F17C4266A2F2DE6E9B_AdjustorThunk (void);
extern void Line_set_From_m592C4AE6BD4846DFCC12721665EE1B58E1D47F04_AdjustorThunk (void);
extern void Line_get_To_mD8FAC6076802B34205395DF694BDCE8314F1F2DA_AdjustorThunk (void);
extern void Line_set_To_mB09CD554B116268CF79C72323AE421A1C6C0B1F3_AdjustorThunk (void);
extern void Line_get_Dir_mCD8563E867388D16CF2A859711F1868D2A1E77D0_AdjustorThunk (void);
extern void Line_set_Dir_m8099FE4EEA13DB6F851843C3258BC6931DE82B85_AdjustorThunk (void);
extern void Line_get_Mag_mFBC4B05934E2168B53EA8A64864446F3CE366AF5_AdjustorThunk (void);
extern void Line_set_Mag_m8DF404321EC5B783086BAAB927F520F0B4FDE1EB_AdjustorThunk (void);
extern void Line_get_Normal_m8F500F8704DEEF72A8F69A20608C2ACEE316146A_AdjustorThunk (void);
extern void Line_set_Normal_mD0FB285540CA993D6648EA0A2ECED90968AEF099_AdjustorThunk (void);
extern void ChartMagin_get_Left_m8CA3693F74477665EBC79AA26FF6FBFC4A00DF75_AdjustorThunk (void);
extern void ChartMagin_get_Right_mD8761085F81F2AF1D421324FD92D98D146C52E2F_AdjustorThunk (void);
extern void ChartMagin_get_Top_m98A43BC03361E0A3E97E6DA5E85610206ABD3F10_AdjustorThunk (void);
extern void ChartMagin_get_Bottom_m44249037E523659C0F58D7D5135FB1CE247D8B44_AdjustorThunk (void);
extern void ChartMagin__ctor_mEA93AF91A96ED580128F4E66524C2DF5DB4FAC5D_AdjustorThunk (void);
extern void DoubleRect__ctor_m1F984B01FA3C146F2E44687D00A5555DE0FB1788_AdjustorThunk (void);
extern void DoubleRect_ToString_m919CD47E3D21F81F9FBF3139392A42ADD1C14114_AdjustorThunk (void);
extern void DoubleRect_get_min_m2DAC7EA9AF9EDCDC7C8AEE363E235019B55143BE_AdjustorThunk (void);
extern void DoubleRect_get_max_m9A803BD921EA1E942317AAA8E0D785412FEE7D1F_AdjustorThunk (void);
extern void DoubleVector2__ctor_mB07435411E7635EB6F0184A9CA371FDE8929F41A_AdjustorThunk (void);
extern void DoubleVector2_ToVector2_m9F6043976FEBC38B285B63682A15F24AA6DF1393_AdjustorThunk (void);
extern void DoubleVector2_ToDoubleVector3_m89ED5CDEBCE31F542002701BEACBAE3002F8D9F7_AdjustorThunk (void);
extern void DoubleVector2__ctor_m49A34EB80CA262CEDACCA8A80EEC90973CBFFD0C_AdjustorThunk (void);
extern void DoubleVector3_get_Item_mED8C18858451EA7064C2F46FA07E957C2644ED0D_AdjustorThunk (void);
extern void DoubleVector3_set_Item_m924522C3E1567A827FCD1CA497289495C98A0AED_AdjustorThunk (void);
extern void DoubleVector3_ToVector2_mCA8A08430A8A865321E15246F5843E2049991874_AdjustorThunk (void);
extern void DoubleVector3_ToVector3_m536D90101A2050B94772A68DADC79B9EF6A76E95_AdjustorThunk (void);
extern void DoubleVector3_ToVector4_m7E82F42A506E67800A6B46BAD15159908CD6F12C_AdjustorThunk (void);
extern void DoubleVector3_ToDoubleVector4_m465CDD4A9232979D4AB4F55D349DC46C47A0D70C_AdjustorThunk (void);
extern void DoubleVector3_ToDoubleVector2_mDF162BA352854D9C6A3CC52D9C504FEB4CA3F48F_AdjustorThunk (void);
extern void DoubleVector3_get_normalized_m90F18F73D68A8B15215A499AF2C5639423485E5D_AdjustorThunk (void);
extern void DoubleVector3_get_magnitude_m5506849C53E5CEB754AE27A8BA2D19378010B921_AdjustorThunk (void);
extern void DoubleVector3_get_sqrMagnitude_m88FEE9BC6FA36A803CB71EDC6EBA6AA1DBB88EFD_AdjustorThunk (void);
extern void DoubleVector3__ctor_mFB68746E3A4C58C0EFB00117654573FAA1486902_AdjustorThunk (void);
extern void DoubleVector3__ctor_m7E898AB882BD1B76DA5C242BC4C1D752EFC275C9_AdjustorThunk (void);
extern void DoubleVector3__ctor_m64CA35E9656B1D9020E4D0006D2F9182C3C8332A_AdjustorThunk (void);
extern void DoubleVector3_Set_mDD93E64C2D83554A00D45AEDA8E53BA8EEC9A461_AdjustorThunk (void);
extern void DoubleVector3_Scale_mDE9D2DCDC9D7E9781D938FC8796B5E924F0EF440_AdjustorThunk (void);
extern void DoubleVector3_GetHashCode_m6B0B3D669BFEF329F363FACBABF80EACCC032811_AdjustorThunk (void);
extern void DoubleVector3_Equals_m5C3BB2A3751484CE3672E5F49E9C554E9735D32C_AdjustorThunk (void);
extern void DoubleVector3_Normalize_m6BAE2E860F8AF017BA5FA679637F17A5316186A8_AdjustorThunk (void);
extern void DoubleVector3_ToString_mA764FD3E053E52D53B2B368E410DE1AE2544211B_AdjustorThunk (void);
extern void DoubleVector3_ToString_m4D85420392458F4FC660D1F6EAA01DAB3B0001F9_AdjustorThunk (void);
extern void KeyElement__ctor_mF495D25615988DEAF9D93439848D890FB358E5FA_AdjustorThunk (void);
extern void KeyElement_IsIn_mB96974CD61E546BD694B850A130C84598FCAE0CE_AdjustorThunk (void);
extern void KeyElement_IsInRow_mDB9A5D4E921914DA9615454A3ADF273EB4B0CB4A_AdjustorThunk (void);
extern void KeyElement_IsInColumn_m37197E408CAD2E333AC1979A6FBE45DBCE5988E9_AdjustorThunk (void);
extern void KeyElement_Equals_mB8633937E6EAD804F05153CFB62D57457E59F27A_AdjustorThunk (void);
extern void KeyElement_GetHashCode_m68EFF0FDBC838A8B7E151CDED36F50DF849259A4_AdjustorThunk (void);
extern void KeyElement_get_Row_m8E5B2536ABA3F7963BED8C00C37AD9DE246E4182_AdjustorThunk (void);
extern void KeyElement_set_Row_mA0B59C94D055895B004E338AD361C4228FC37AD0_AdjustorThunk (void);
extern void KeyElement_get_Column_m414AFFA313E39B3AEBAAC5B8DEA3A2E483F69999_AdjustorThunk (void);
extern void KeyElement_set_Column_mFD48761D666FCDB501D3C5C79F3FCA47DEDF6D8C_AdjustorThunk (void);
extern void MaterialTiling__ctor_m0B5FB49A293882E7A35FAEC1EB5DBA660AA07269_AdjustorThunk (void);
extern void MaterialTiling_Equals_m466A2E9815E17A86ABED36483492136634F64556_AdjustorThunk (void);
extern void MaterialTiling_GetHashCode_m31C922525DC3445EB61DCD90C8066015A8352338_AdjustorThunk (void);
extern void ChartItemIndex__ctor_m4941151A043FAF2B807F6DA558F40770D70AA68C_AdjustorThunk (void);
extern void ChartItemIndex_Equals_m3ED9D2A4A198B737C7A5D0AF25A359CF6582C6A9_AdjustorThunk (void);
extern void ChartItemIndex_GetHashCode_m34B5BD4640C9BFF87E9B100043FA0936960CE5F4_AdjustorThunk (void);
extern void ChartItemIndex_get_Group_mBBD268E7B2942B2B9F44E76CF11463EB2247B92B_AdjustorThunk (void);
extern void ChartItemIndex_set_Group_m1FD2ECC0C5E3DE8D62E22CF59AB13BB2C63E940B_AdjustorThunk (void);
extern void ChartItemIndex_get_Category_m65B6C653CB412F5D8B8A8E8E34759FE50B81C3A5_AdjustorThunk (void);
extern void ChartItemIndex_set_Category_mF159D3DEB39D5C73D7ECE7448170A56B9488F5E4_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[104] = 
{
	{ 0x0600019C, ExtensionFilter__ctor_m005AD482A024C782159F183FFD5579DBF4CD7887_AdjustorThunk },
	{ 0x0600021B, Enumerator_get_IsValid_m0DF87B70D0CB8C46C3B337005248A54B7C86979F_AdjustorThunk },
	{ 0x0600021C, Enumerator__ctor_mF85148FE3692D3904C073278CFE8716EB9B4B0E2_AdjustorThunk },
	{ 0x0600021D, Enumerator__ctor_mA42ACFD81450787E450330C66FE5CBB9FD0644EC_AdjustorThunk },
	{ 0x0600021E, Enumerator_get_Current_m80F00207F02904E3A63CA549AD1E34DB016C4A47_AdjustorThunk },
	{ 0x0600021F, Enumerator_MoveNext_m9947F7EE949531A65662E0BA157E3534993C36BF_AdjustorThunk },
	{ 0x06000220, ValueEnumerator__ctor_mA049E3670B0C9E04EFFA9CD8702AA9A74DB9C718_AdjustorThunk },
	{ 0x06000221, ValueEnumerator__ctor_m65F47E21C6E8DBB2A363B8FCF67709863FF36C35_AdjustorThunk },
	{ 0x06000222, ValueEnumerator__ctor_mC832E5CC32931EE67681635AA27CF2ED262630ED_AdjustorThunk },
	{ 0x06000223, ValueEnumerator_get_Current_m285C54105C3F05F978CD31F37025375C39C83542_AdjustorThunk },
	{ 0x06000224, ValueEnumerator_MoveNext_mEA96FAD65F2306BBC4B323984BD480FE6D2DB5AF_AdjustorThunk },
	{ 0x06000225, ValueEnumerator_GetEnumerator_m44A09F9E247CBB2F7542237EF47BD0A985FC3663_AdjustorThunk },
	{ 0x06000226, KeyEnumerator__ctor_mD632C7FC751F302A79EC4073C7DFF80C496266F6_AdjustorThunk },
	{ 0x06000227, KeyEnumerator__ctor_m7D1CC885A67D308EED89D61D67DBD02D4D17DE79_AdjustorThunk },
	{ 0x06000228, KeyEnumerator__ctor_m7F24F46E4952C1FC737D3F638603E33208952562_AdjustorThunk },
	{ 0x06000229, KeyEnumerator_get_Current_mF0ABCA5D5FEBDF2C7FC8BFD989D44026434E463B_AdjustorThunk },
	{ 0x0600022A, KeyEnumerator_MoveNext_m063B0761C6BC5BB84B8A9ADEBE965D51E8958D2A_AdjustorThunk },
	{ 0x0600022B, KeyEnumerator_GetEnumerator_mA4473824DD3BB4D3421080111235E5A58757FF5D_AdjustorThunk },
	{ 0x060003E3, CandleValue__ctor_m3DF3507ACFD389F979CEA1D114D7455CE7B62BB5_AdjustorThunk },
	{ 0x060003E4, CandleValue__ctor_m113D5BA7F228439CB190BA3CE617FB07BDDAFBBF_AdjustorThunk },
	{ 0x060003E5, CandleValue_get_isUp_m327DC66B9BE3EFF66FF5299A41E200883479E80F_AdjustorThunk },
	{ 0x060003E6, CandleValue_get_End_m2431E9875D452C1194BBC5FC3FB8668A4F9DAE52_AdjustorThunk },
	{ 0x060003E7, CandleValue_get_MidPoint_m1C6D8595E8D75F3D474E520A562D457CCBD8232A_AdjustorThunk },
	{ 0x060003E8, CandleValue_get_Max_m35263E3FD9F3780330E691CC51D241D442D8A228_AdjustorThunk },
	{ 0x060003E9, CandleValue_get_LowBound_m916E8CBB1CCE4211DAFE86888125AB79A57F329B_AdjustorThunk },
	{ 0x060003EA, CandleValue_get_HighBound_m954D4BEC75899641B1CCB5E0792E9460F80BDCF0_AdjustorThunk },
	{ 0x060003EB, CandleValue_get_Min_m93794657F2D8ED00CB5B56EB649E5278FECEB04E_AdjustorThunk },
	{ 0x06000511, AutoFloat__ctor_mFA106E4A358AB6F23F2C124C4C9BD3811B422C1A_AdjustorThunk },
	{ 0x06000512, AutoFloat_Equals_m33B82696A2015E983C87F70E380C9942593D0BFC_AdjustorThunk },
	{ 0x06000513, AutoFloat_GetHashCode_mBA93BDD7EA6E5F3DAF7F2B880B8DD0CA23539989_AdjustorThunk },
	{ 0x06000676, Line__ctor_m4389E71B32F966E8718C9FAFD8DC7C9F6E2110C7_AdjustorThunk },
	{ 0x06000677, Line_get_Degenerated_mC903C588921C51C16C8A9EDE11AACCF1A4C759B6_AdjustorThunk },
	{ 0x06000678, Line_set_Degenerated_m378A5401A9BAA385E72BC9885DC3B53F1414B451_AdjustorThunk },
	{ 0x06000679, Line_get_P1_mC750288C96B6CE408C61A85DF57B29B463D10C0D_AdjustorThunk },
	{ 0x0600067A, Line_set_P1_mC38DFAC465CCB453B3B4AB7E97A447590CC89965_AdjustorThunk },
	{ 0x0600067B, Line_get_P2_mB960A6BEF78A9B952DAE1A299D67CFC2012E2A1F_AdjustorThunk },
	{ 0x0600067C, Line_set_P2_m160F2F164EF038D1F32214DE28204AE9683D33EA_AdjustorThunk },
	{ 0x0600067D, Line_get_P3_m06A9A176422365C1575296A1F25FD5DEF4D99DDD_AdjustorThunk },
	{ 0x0600067E, Line_set_P3_mCAF01F4FED931AA69E7246B0FCBB544D303C7B70_AdjustorThunk },
	{ 0x0600067F, Line_get_P4_m8283F3E3C3EFF22F76BDAE1934F53F5175BD2616_AdjustorThunk },
	{ 0x06000680, Line_set_P4_mC6DD8CC9AB985D8FB738267F457D4C34250B9E6D_AdjustorThunk },
	{ 0x06000681, Line_get_From_mE45AE1DDC8A9D9EBF61227F17C4266A2F2DE6E9B_AdjustorThunk },
	{ 0x06000682, Line_set_From_m592C4AE6BD4846DFCC12721665EE1B58E1D47F04_AdjustorThunk },
	{ 0x06000683, Line_get_To_mD8FAC6076802B34205395DF694BDCE8314F1F2DA_AdjustorThunk },
	{ 0x06000684, Line_set_To_mB09CD554B116268CF79C72323AE421A1C6C0B1F3_AdjustorThunk },
	{ 0x06000685, Line_get_Dir_mCD8563E867388D16CF2A859711F1868D2A1E77D0_AdjustorThunk },
	{ 0x06000686, Line_set_Dir_m8099FE4EEA13DB6F851843C3258BC6931DE82B85_AdjustorThunk },
	{ 0x06000687, Line_get_Mag_mFBC4B05934E2168B53EA8A64864446F3CE366AF5_AdjustorThunk },
	{ 0x06000688, Line_set_Mag_m8DF404321EC5B783086BAAB927F520F0B4FDE1EB_AdjustorThunk },
	{ 0x06000689, Line_get_Normal_m8F500F8704DEEF72A8F69A20608C2ACEE316146A_AdjustorThunk },
	{ 0x0600068A, Line_set_Normal_mD0FB285540CA993D6648EA0A2ECED90968AEF099_AdjustorThunk },
	{ 0x060007C2, ChartMagin_get_Left_m8CA3693F74477665EBC79AA26FF6FBFC4A00DF75_AdjustorThunk },
	{ 0x060007C3, ChartMagin_get_Right_mD8761085F81F2AF1D421324FD92D98D146C52E2F_AdjustorThunk },
	{ 0x060007C4, ChartMagin_get_Top_m98A43BC03361E0A3E97E6DA5E85610206ABD3F10_AdjustorThunk },
	{ 0x060007C5, ChartMagin_get_Bottom_m44249037E523659C0F58D7D5135FB1CE247D8B44_AdjustorThunk },
	{ 0x060007C6, ChartMagin__ctor_mEA93AF91A96ED580128F4E66524C2DF5DB4FAC5D_AdjustorThunk },
	{ 0x060007EA, DoubleRect__ctor_m1F984B01FA3C146F2E44687D00A5555DE0FB1788_AdjustorThunk },
	{ 0x060007EB, DoubleRect_ToString_m919CD47E3D21F81F9FBF3139392A42ADD1C14114_AdjustorThunk },
	{ 0x060007EC, DoubleRect_get_min_m2DAC7EA9AF9EDCDC7C8AEE363E235019B55143BE_AdjustorThunk },
	{ 0x060007ED, DoubleRect_get_max_m9A803BD921EA1E942317AAA8E0D785412FEE7D1F_AdjustorThunk },
	{ 0x060007EE, DoubleVector2__ctor_mB07435411E7635EB6F0184A9CA371FDE8929F41A_AdjustorThunk },
	{ 0x060007EF, DoubleVector2_ToVector2_m9F6043976FEBC38B285B63682A15F24AA6DF1393_AdjustorThunk },
	{ 0x060007F0, DoubleVector2_ToDoubleVector3_m89ED5CDEBCE31F542002701BEACBAE3002F8D9F7_AdjustorThunk },
	{ 0x060007F1, DoubleVector2__ctor_m49A34EB80CA262CEDACCA8A80EEC90973CBFFD0C_AdjustorThunk },
	{ 0x060007F2, DoubleVector3_get_Item_mED8C18858451EA7064C2F46FA07E957C2644ED0D_AdjustorThunk },
	{ 0x060007F3, DoubleVector3_set_Item_m924522C3E1567A827FCD1CA497289495C98A0AED_AdjustorThunk },
	{ 0x060007F4, DoubleVector3_ToVector2_mCA8A08430A8A865321E15246F5843E2049991874_AdjustorThunk },
	{ 0x060007F5, DoubleVector3_ToVector3_m536D90101A2050B94772A68DADC79B9EF6A76E95_AdjustorThunk },
	{ 0x060007F6, DoubleVector3_ToVector4_m7E82F42A506E67800A6B46BAD15159908CD6F12C_AdjustorThunk },
	{ 0x060007F7, DoubleVector3_ToDoubleVector4_m465CDD4A9232979D4AB4F55D349DC46C47A0D70C_AdjustorThunk },
	{ 0x060007F8, DoubleVector3_ToDoubleVector2_mDF162BA352854D9C6A3CC52D9C504FEB4CA3F48F_AdjustorThunk },
	{ 0x060007F9, DoubleVector3_get_normalized_m90F18F73D68A8B15215A499AF2C5639423485E5D_AdjustorThunk },
	{ 0x060007FA, DoubleVector3_get_magnitude_m5506849C53E5CEB754AE27A8BA2D19378010B921_AdjustorThunk },
	{ 0x060007FB, DoubleVector3_get_sqrMagnitude_m88FEE9BC6FA36A803CB71EDC6EBA6AA1DBB88EFD_AdjustorThunk },
	{ 0x06000805, DoubleVector3__ctor_mFB68746E3A4C58C0EFB00117654573FAA1486902_AdjustorThunk },
	{ 0x06000806, DoubleVector3__ctor_m7E898AB882BD1B76DA5C242BC4C1D752EFC275C9_AdjustorThunk },
	{ 0x06000807, DoubleVector3__ctor_m64CA35E9656B1D9020E4D0006D2F9182C3C8332A_AdjustorThunk },
	{ 0x0600080B, DoubleVector3_Set_mDD93E64C2D83554A00D45AEDA8E53BA8EEC9A461_AdjustorThunk },
	{ 0x0600080D, DoubleVector3_Scale_mDE9D2DCDC9D7E9781D938FC8796B5E924F0EF440_AdjustorThunk },
	{ 0x0600080F, DoubleVector3_GetHashCode_m6B0B3D669BFEF329F363FACBABF80EACCC032811_AdjustorThunk },
	{ 0x06000810, DoubleVector3_Equals_m5C3BB2A3751484CE3672E5F49E9C554E9735D32C_AdjustorThunk },
	{ 0x06000813, DoubleVector3_Normalize_m6BAE2E860F8AF017BA5FA679637F17A5316186A8_AdjustorThunk },
	{ 0x06000823, DoubleVector3_ToString_mA764FD3E053E52D53B2B368E410DE1AE2544211B_AdjustorThunk },
	{ 0x06000824, DoubleVector3_ToString_m4D85420392458F4FC660D1F6EAA01DAB3B0001F9_AdjustorThunk },
	{ 0x06000871, KeyElement__ctor_mF495D25615988DEAF9D93439848D890FB358E5FA_AdjustorThunk },
	{ 0x06000872, KeyElement_IsIn_mB96974CD61E546BD694B850A130C84598FCAE0CE_AdjustorThunk },
	{ 0x06000873, KeyElement_IsInRow_mDB9A5D4E921914DA9615454A3ADF273EB4B0CB4A_AdjustorThunk },
	{ 0x06000874, KeyElement_IsInColumn_m37197E408CAD2E333AC1979A6FBE45DBCE5988E9_AdjustorThunk },
	{ 0x06000877, KeyElement_Equals_mB8633937E6EAD804F05153CFB62D57457E59F27A_AdjustorThunk },
	{ 0x06000878, KeyElement_GetHashCode_m68EFF0FDBC838A8B7E151CDED36F50DF849259A4_AdjustorThunk },
	{ 0x06000879, KeyElement_get_Row_m8E5B2536ABA3F7963BED8C00C37AD9DE246E4182_AdjustorThunk },
	{ 0x0600087A, KeyElement_set_Row_mA0B59C94D055895B004E338AD361C4228FC37AD0_AdjustorThunk },
	{ 0x0600087B, KeyElement_get_Column_m414AFFA313E39B3AEBAAC5B8DEA3A2E483F69999_AdjustorThunk },
	{ 0x0600087C, KeyElement_set_Column_mFD48761D666FCDB501D3C5C79F3FCA47DEDF6D8C_AdjustorThunk },
	{ 0x060009B2, MaterialTiling__ctor_m0B5FB49A293882E7A35FAEC1EB5DBA660AA07269_AdjustorThunk },
	{ 0x060009B3, MaterialTiling_Equals_m466A2E9815E17A86ABED36483492136634F64556_AdjustorThunk },
	{ 0x060009B4, MaterialTiling_GetHashCode_m31C922525DC3445EB61DCD90C8066015A8352338_AdjustorThunk },
	{ 0x06000C68, ChartItemIndex__ctor_m4941151A043FAF2B807F6DA558F40770D70AA68C_AdjustorThunk },
	{ 0x06000C69, ChartItemIndex_Equals_m3ED9D2A4A198B737C7A5D0AF25A359CF6582C6A9_AdjustorThunk },
	{ 0x06000C6A, ChartItemIndex_GetHashCode_m34B5BD4640C9BFF87E9B100043FA0936960CE5F4_AdjustorThunk },
	{ 0x06000C6B, ChartItemIndex_get_Group_mBBD268E7B2942B2B9F44E76CF11463EB2247B92B_AdjustorThunk },
	{ 0x06000C6C, ChartItemIndex_set_Group_m1FD2ECC0C5E3DE8D62E22CF59AB13BB2C63E940B_AdjustorThunk },
	{ 0x06000C6D, ChartItemIndex_get_Category_m65B6C653CB412F5D8B8A8E8E34759FE50B81C3A5_AdjustorThunk },
	{ 0x06000C6E, ChartItemIndex_set_Category_mF159D3DEB39D5C73D7ECE7448170A56B9488F5E4_AdjustorThunk },
};
static const int32_t s_InvokerIndices[3209] = 
{
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	1847,
	1132,
	3786,
	3119,
	3119,
	1847,
	1847,
	1847,
	3786,
	5339,
	3786,
	1215,
	3119,
	3119,
	3786,
	3786,
	3119,
	3119,
	3119,
	2484,
	2484,
	3786,
	3786,
	3786,
	1845,
	3119,
	915,
	3119,
	3105,
	3786,
	3747,
	3786,
	3723,
	3786,
	3723,
	5339,
	3786,
	5339,
	1215,
	3119,
	3119,
	3119,
	3786,
	3786,
	3119,
	3119,
	3119,
	2484,
	2484,
	3786,
	3786,
	700,
	3786,
	1845,
	3119,
	915,
	3119,
	3105,
	3786,
	3747,
	3786,
	3723,
	3786,
	3723,
	5339,
	1215,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	3119,
	2484,
	2484,
	3786,
	3786,
	3105,
	3786,
	3747,
	3786,
	3723,
	3786,
	3723,
	5339,
	3786,
	1215,
	3119,
	3119,
	3786,
	3786,
	3119,
	3119,
	3119,
	2484,
	2484,
	3786,
	3786,
	3786,
	1845,
	3119,
	915,
	3119,
	3105,
	3786,
	3747,
	3786,
	3723,
	3786,
	3723,
	3723,
	3119,
	3723,
	3119,
	3142,
	3723,
	3786,
	3708,
	3105,
	3723,
	3786,
	1850,
	3786,
	3786,
	3786,
	3708,
	3105,
	3723,
	3786,
	1850,
	3723,
	3119,
	3786,
	3786,
	3723,
	3119,
	3708,
	3105,
	3754,
	3148,
	3754,
	3148,
	3786,
	3723,
	3119,
	3786,
	3786,
	3723,
	3148,
	3723,
	3119,
	3786,
	3786,
	3786,
	3786,
	3723,
	3786,
	3786,
	3119,
	3723,
	2484,
	3786,
	1046,
	744,
	3786,
	3786,
	3786,
	3786,
	2742,
	3786,
	3786,
	3786,
	3786,
	3786,
	2708,
	1133,
	3786,
	3786,
	3119,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	2484,
	3119,
	1150,
	3119,
	3119,
	3786,
	5339,
	3786,
	2742,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	1837,
	5339,
	3786,
	1275,
	3786,
	3786,
	3786,
	3786,
	3119,
	3786,
	3786,
	3786,
	3723,
	3786,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	1063,
	3786,
	1063,
	3786,
	3685,
	3786,
	3119,
	3119,
	2742,
	3786,
	3119,
	2320,
	3684,
	1062,
	3786,
	1725,
	1725,
	3079,
	1253,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	1389,
	3786,
	3119,
	1150,
	1843,
	3786,
	1847,
	5339,
	3786,
	2742,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3723,
	3786,
	3786,
	3786,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3747,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	1512,
	3786,
	3786,
	3786,
	3119,
	3119,
	3786,
	3119,
	3119,
	3119,
	4418,
	3119,
	3119,
	2484,
	3786,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	4418,
	3119,
	3119,
	2484,
	3786,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	4418,
	3119,
	3119,
	2484,
	3786,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	3786,
	4161,
	3119,
	3786,
	3786,
	4161,
	3119,
	3786,
	3786,
	586,
	918,
	585,
	341,
	763,
	340,
	1847,
	5339,
	4305,
	4305,
	4164,
	4164,
	4536,
	4419,
	4304,
	4304,
	4162,
	4162,
	3786,
	3708,
	2480,
	1737,
	2484,
	1847,
	3723,
	3119,
	3708,
	3747,
	3747,
	3747,
	3747,
	3747,
	3747,
	3747,
	3142,
	1847,
	3119,
	2484,
	2480,
	2484,
	3723,
	3723,
	3723,
	2480,
	732,
	3812,
	3723,
	3813,
	3814,
	3684,
	3079,
	3708,
	3105,
	3754,
	3148,
	3747,
	3142,
	3709,
	3106,
	3723,
	3723,
	5172,
	5172,
	5164,
	5088,
	5182,
	5225,
	5169,
	5113,
	5170,
	5123,
	5180,
	5208,
	5136,
	4886,
	4886,
	2742,
	3708,
	5316,
	5172,
	4806,
	5172,
	3119,
	3119,
	3119,
	3119,
	3723,
	3119,
	3723,
	5172,
	5172,
	5172,
	5172,
	5172,
	5172,
	5172,
	5169,
	5185,
	5186,
	5187,
	5174,
	5176,
	5172,
	5242,
	5247,
	5252,
	5193,
	5198,
	5172,
	2909,
	1547,
	3779,
	953,
	2916,
	1043,
	3781,
	628,
	2924,
	3783,
	2498,
	2514,
	3732,
	2485,
	2525,
	3737,
	2486,
	2484,
	3723,
	2484,
	3719,
	2483,
	3786,
	5339,
	3747,
	2970,
	2971,
	3580,
	3747,
	2970,
	2971,
	3198,
	3723,
	3747,
	3814,
	2970,
	2971,
	3198,
	3723,
	3747,
	3813,
	3119,
	3580,
	3723,
	3747,
	3786,
	3723,
	3786,
	3723,
	3105,
	3786,
	3747,
	3723,
	3786,
	3723,
	3723,
	3723,
	3105,
	3786,
	3747,
	3786,
	3786,
	3723,
	3786,
	3723,
	3723,
	3723,
	3747,
	3142,
	3708,
	3747,
	3812,
	2480,
	1737,
	2484,
	1847,
	3708,
	1847,
	2480,
	2484,
	3723,
	732,
	3119,
	3786,
	3105,
	3786,
	3747,
	3786,
	3723,
	3786,
	3723,
	3723,
	3723,
	3747,
	3142,
	3708,
	3747,
	3812,
	2484,
	1847,
	2480,
	1737,
	3708,
	1847,
	2484,
	2480,
	2484,
	3723,
	732,
	3119,
	3786,
	3786,
	2606,
	3105,
	3786,
	3747,
	3786,
	3723,
	3786,
	3723,
	3723,
	3723,
	3708,
	3747,
	3812,
	3723,
	3119,
	3119,
	732,
	2742,
	3708,
	3119,
	3708,
	3747,
	3812,
	3723,
	3119,
	3684,
	3079,
	3709,
	3106,
	3079,
	3119,
	732,
	5208,
	2742,
	3708,
	3119,
	3708,
	3747,
	3812,
	3723,
	3119,
	3747,
	3142,
	3142,
	3119,
	732,
	2742,
	3708,
	3119,
	5316,
	3786,
	3708,
	3747,
	3812,
	3723,
	3119,
	3747,
	3142,
	2742,
	3708,
	732,
	3119,
	5339,
	3708,
	3812,
	3119,
	1847,
	-1,
	2480,
	1737,
	2484,
	1847,
	3119,
	1847,
	4886,
	4886,
	2742,
	3708,
	3708,
	3105,
	3754,
	3148,
	3684,
	3079,
	3709,
	3106,
	3747,
	3142,
	3723,
	3723,
	732,
	3119,
	5172,
	2742,
	2484,
	1389,
	2484,
	1389,
	1387,
	1387,
	2348,
	2484,
	3786,
	3119,
	1389,
	2348,
	1389,
	2742,
	2484,
	1387,
	1387,
	2484,
	1389,
	2484,
	3105,
	3786,
	3747,
	3581,
	3786,
	3723,
	3723,
	3723,
	3119,
	2348,
	1389,
	1387,
	1389,
	2484,
	2742,
	1389,
	1387,
	2484,
	2484,
	3105,
	3786,
	3747,
	3786,
	3581,
	3786,
	3723,
	3723,
	3723,
	3723,
	3723,
	46,
	1551,
	1556,
	3786,
	759,
	3148,
	3786,
	3723,
	3148,
	3723,
	759,
	3786,
	1551,
	1556,
	46,
	3119,
	3119,
	3119,
	3786,
	3723,
	3119,
	3754,
	3148,
	3708,
	3105,
	3708,
	3105,
	3723,
	3119,
	3723,
	3723,
	3747,
	3723,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	1847,
	1847,
	3786,
	3786,
	3786,
	674,
	2467,
	3142,
	3786,
	3786,
	3786,
	2742,
	2161,
	2161,
	3786,
	3786,
	3786,
	2484,
	3786,
	3119,
	3119,
	3786,
	3747,
	3747,
	3747,
	3747,
	3754,
	3754,
	3754,
	3747,
	1131,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3786,
	3786,
	3786,
	3723,
	3708,
	3786,
	2480,
	3786,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	2742,
	1847,
	2484,
	1847,
	3786,
	3786,
	3786,
	3119,
	340,
	6,
	757,
	1851,
	1851,
	775,
	1146,
	1851,
	3786,
	1847,
	2484,
	1847,
	3119,
	2161,
	1495,
	3723,
	3119,
	1128,
	3779,
	3786,
	3786,
	5339,
	3786,
	2484,
	3853,
	3786,
	3754,
	3754,
	226,
	810,
	85,
	3708,
	3105,
	3723,
	3754,
	3148,
	2855,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3786,
	3786,
	3786,
	1847,
	1737,
	1847,
	3786,
	3786,
	3786,
	3723,
	3747,
	3747,
	3747,
	2742,
	2161,
	2161,
	1049,
	1185,
	1906,
	93,
	155,
	155,
	155,
	155,
	121,
	73,
	343,
	1275,
	3786,
	3119,
	3119,
	3786,
	3786,
	185,
	3747,
	3747,
	3747,
	3737,
	3136,
	3781,
	3172,
	3708,
	3105,
	3795,
	3184,
	3723,
	3119,
	3119,
	3119,
	3119,
	3119,
	1847,
	755,
	755,
	2742,
	1847,
	1847,
	1837,
	3119,
	2348,
	1905,
	2348,
	189,
	1843,
	316,
	1152,
	1213,
	1213,
	3786,
	3786,
	3723,
	3119,
	1846,
	1847,
	1472,
	3708,
	3119,
	3119,
	3723,
	3786,
	174,
	175,
	3747,
	3684,
	3685,
	3684,
	3684,
	3684,
	3684,
	3786,
	3786,
	1326,
	3786,
	3786,
	3119,
	3685,
	3685,
	3723,
	3708,
	3747,
	5339,
	3786,
	1989,
	2484,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3723,
	3119,
	3786,
	3105,
	767,
	3119,
	321,
	321,
	3786,
	3786,
	707,
	707,
	3786,
	3747,
	3142,
	3671,
	3062,
	3754,
	3747,
	3747,
	3747,
	3708,
	1389,
	2161,
	3786,
	3786,
	223,
	2809,
	2809,
	1865,
	1268,
	3786,
	321,
	321,
	3119,
	3119,
	3119,
	1847,
	3786,
	3786,
	2742,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3786,
	1850,
	321,
	321,
	3119,
	3786,
	3786,
	707,
	707,
	3786,
	707,
	707,
	3786,
	3779,
	3779,
	972,
	733,
	806,
	806,
	806,
	3754,
	3786,
	1080,
	3723,
	3723,
	3723,
	3723,
	3119,
	3786,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	767,
	1213,
	1213,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3708,
	3723,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	1847,
	2742,
	3786,
	1128,
	3786,
	2742,
	3786,
	3786,
	2742,
	3786,
	2742,
	3786,
	2742,
	3723,
	3119,
	3723,
	3119,
	3723,
	3723,
	3723,
	3723,
	3754,
	3747,
	3142,
	3747,
	3142,
	3747,
	3142,
	3754,
	3148,
	3747,
	3142,
	3747,
	3142,
	3747,
	3142,
	1847,
	3786,
	1847,
	1847,
	3786,
	3754,
	3754,
	3754,
	3723,
	3747,
	3754,
	3754,
	3754,
	2159,
	3747,
	3786,
	3786,
	3786,
	3786,
	3142,
	3786,
	3786,
	3747,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3723,
	3119,
	3723,
	2742,
	2161,
	2161,
	3786,
	3786,
	3747,
	3786,
	3786,
	3786,
	3786,
	3786,
	3723,
	3671,
	3781,
	3747,
	3708,
	3708,
	3786,
	3786,
	1847,
	3119,
	3119,
	140,
	3786,
	3142,
	3119,
	3119,
	3786,
	3119,
	3119,
	3119,
	3119,
	3119,
	3786,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3723,
	3723,
	3754,
	2742,
	2161,
	2161,
	3786,
	3786,
	3786,
	3786,
	3754,
	3754,
	3754,
	3747,
	3747,
	3747,
	3747,
	3747,
	3747,
	3786,
	5339,
	3786,
	3105,
	3105,
	3105,
	3786,
	3786,
	1868,
	2742,
	3708,
	3708,
	3105,
	3664,
	3054,
	3723,
	3723,
	3786,
	3786,
	3786,
	3786,
	324,
	1146,
	21,
	69,
	291,
	1187,
	291,
	3786,
	3786,
	3786,
	3786,
	2158,
	3786,
	2859,
	3708,
	3105,
	3723,
	3119,
	3718,
	3115,
	3664,
	3054,
	3664,
	3054,
	3664,
	3054,
	3754,
	3148,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3708,
	3105,
	3708,
	3105,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3708,
	3105,
	3119,
	3119,
	3119,
	3119,
	3786,
	3119,
	3119,
	3119,
	3119,
	3786,
	3708,
	3105,
	3754,
	3148,
	3786,
	2859,
	3786,
	3723,
	3786,
	1850,
	3723,
	3786,
	1850,
	3786,
	1131,
	1847,
	1847,
	3723,
	3786,
	3786,
	3708,
	3105,
	3747,
	3142,
	3747,
	3142,
	3747,
	3723,
	3723,
	3723,
	3723,
	3723,
	3723,
	3754,
	3148,
	3754,
	3148,
	3754,
	3754,
	3754,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	736,
	3786,
	1847,
	1557,
	1557,
	3786,
	3119,
	3119,
	1847,
	280,
	1016,
	1016,
	1044,
	1044,
	3786,
	30,
	2914,
	2484,
	2484,
	3747,
	3119,
	3786,
	3119,
	3786,
	1151,
	20,
	2742,
	2161,
	2161,
	2914,
	681,
	729,
	3786,
	3786,
	3786,
	3786,
	3754,
	3786,
	3723,
	3747,
	3747,
	3747,
	3786,
	5339,
	807,
	3781,
	3172,
	3684,
	3079,
	3723,
	3119,
	3723,
	3119,
	3786,
	3786,
	3786,
	3786,
	1847,
	1847,
	3723,
	3119,
	3119,
	3786,
	3708,
	3708,
	3786,
	1847,
	1847,
	3747,
	3142,
	3684,
	3079,
	2742,
	2742,
	3747,
	3142,
	3684,
	3079,
	3786,
	3786,
	3786,
	3684,
	3684,
	2480,
	2480,
	3723,
	3786,
	3786,
	1847,
	3786,
	3079,
	3786,
	1130,
	1843,
	1847,
	1847,
	1843,
	1847,
	2484,
	1847,
	3119,
	3119,
	3119,
	1215,
	1495,
	3119,
	1847,
	329,
	748,
	1128,
	1128,
	3786,
	3786,
	3786,
	5339,
	3786,
	2484,
	2484,
	2484,
	2484,
	1879,
	3786,
	3786,
	3723,
	3119,
	3723,
	3723,
	3684,
	3723,
	3723,
	3786,
	5316,
	4986,
	5182,
	3918,
	4150,
	5316,
	4986,
	5182,
	3786,
	3747,
	1879,
	3786,
	3786,
	3786,
	3747,
	3142,
	3671,
	3062,
	3671,
	3723,
	3119,
	3747,
	3754,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3747,
	3754,
	3148,
	3723,
	3723,
	3723,
	3723,
	1151,
	3786,
	3708,
	3105,
	3708,
	3105,
	3708,
	3708,
	3786,
	3723,
	3786,
	1879,
	3786,
	3723,
	3119,
	3747,
	3754,
	3148,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3723,
	3723,
	3723,
	3723,
	3754,
	3723,
	3786,
	3786,
	3754,
	3148,
	3595,
	3015,
	3148,
	3148,
	783,
	3786,
	3786,
	1737,
	3747,
	3142,
	3119,
	3786,
	84,
	3786,
	3786,
	3723,
	3119,
	3779,
	3779,
	3754,
	733,
	806,
	3786,
	1556,
	3723,
	2909,
	3723,
	3723,
	3723,
	-1,
	3786,
	3119,
	806,
	806,
	81,
	1156,
	362,
	3747,
	3142,
	3781,
	3172,
	3781,
	3172,
	3781,
	3172,
	3781,
	3172,
	3781,
	3172,
	3781,
	3172,
	3781,
	3172,
	3754,
	3148,
	3781,
	3172,
	3119,
	3119,
	3119,
	3708,
	3708,
	2920,
	1067,
	811,
	2159,
	5339,
	3786,
	2923,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3148,
	3723,
	3119,
	3786,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3708,
	3105,
	3105,
	3754,
	3786,
	3779,
	3779,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	1861,
	3119,
	3119,
	3786,
	3786,
	733,
	3754,
	806,
	3786,
	734,
	3119,
	3119,
	896,
	3119,
	3119,
	1180,
	2765,
	3786,
	3786,
	1845,
	707,
	142,
	3119,
	3786,
	5316,
	1386,
	2480,
	892,
	3786,
	5339,
	5339,
	4612,
	5316,
	4719,
	4719,
	5083,
	5316,
	4448,
	4448,
	4447,
	4726,
	4726,
	4330,
	4813,
	4984,
	4984,
	4915,
	4609,
	4609,
	5243,
	4131,
	4942,
	4942,
	5199,
	5316,
	4592,
	4972,
	5327,
	5263,
	4938,
	4615,
	5224,
	4981,
	-1,
	4613,
	4613,
	4613,
	4620,
	3850,
	3844,
	4975,
	4975,
	4972,
	5172,
	4981,
	4981,
	4984,
	4159,
	4362,
	5316,
	5263,
	5316,
	5263,
	5316,
	5263,
	3786,
	1459,
	2337,
	3786,
	1439,
	2320,
	3786,
	1443,
	2322,
	3786,
	3786,
	3684,
	3079,
	3684,
	3079,
	3685,
	3685,
	3723,
	3708,
	3747,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	3684,
	3079,
	3747,
	3142,
	3684,
	3079,
	3747,
	3142,
	3684,
	3079,
	3684,
	3079,
	3684,
	3079,
	3786,
	3684,
	3079,
	3786,
	2742,
	1838,
	1213,
	1213,
	1737,
	3786,
	3786,
	3786,
	3105,
	3119,
	1472,
	1846,
	1847,
	3723,
	3786,
	3786,
	1472,
	2742,
	3119,
	1846,
	1847,
	3786,
	2742,
	5091,
	5081,
	5161,
	5161,
	4781,
	5065,
	3786,
	5339,
	3786,
	3119,
	1100,
	3747,
	3142,
	3747,
	3142,
	3723,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	3708,
	3105,
	3708,
	3105,
	3723,
	3119,
	3723,
	3119,
	3119,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3142,
	3142,
	3781,
	3732,
	3781,
	3786,
	3723,
	3119,
	3723,
	3119,
	3786,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	3781,
	3732,
	3781,
	3786,
	1495,
	3119,
	3786,
	3142,
	3142,
	3786,
	3786,
	3786,
	3786,
	3786,
	1495,
	3119,
	3148,
	3754,
	3786,
	3142,
	3142,
	3786,
	3786,
	3786,
	3786,
	3786,
	3732,
	3781,
	3781,
	3754,
	3723,
	3148,
	3786,
	3754,
	3754,
	3754,
	3754,
	804,
	3723,
	3119,
	1847,
	3786,
	3786,
	3708,
	3708,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	2128,
	1833,
	3063,
	3063,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3786,
	5339,
	3786,
	3148,
	1879,
	2742,
	3708,
	3723,
	3119,
	3786,
	3786,
	692,
	3723,
	3686,
	3686,
	3170,
	3779,
	3686,
	1579,
	2159,
	1708,
	3779,
	3781,
	3781,
	3723,
	3685,
	3686,
	3684,
	3684,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	5304,
	1063,
	1579,
	3172,
	4452,
	4452,
	4452,
	1063,
	4730,
	3081,
	4730,
	3708,
	2742,
	4730,
	5092,
	3786,
	4723,
	4723,
	4729,
	5084,
	5084,
	4730,
	4730,
	4730,
	4730,
	5092,
	4729,
	4728,
	4729,
	4866,
	4866,
	3723,
	2484,
	692,
	3783,
	3781,
	3686,
	-1,
	-1,
	-1,
	-1,
	-1,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3119,
	1509,
	3786,
	3786,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3786,
	736,
	1847,
	1847,
	3747,
	3142,
	3723,
	3723,
	1847,
	3786,
	3786,
	3786,
	3747,
	3592,
	3592,
	3119,
	3119,
	3119,
	3786,
	3723,
	1523,
	1128,
	1215,
	1215,
	1214,
	1212,
	1117,
	1128,
	1112,
	1072,
	1847,
	2742,
	2742,
	2742,
	4906,
	4906,
	2742,
	3708,
	3723,
	3119,
	3723,
	3119,
	3119,
	3119,
	3747,
	3142,
	3747,
	3142,
	3671,
	3062,
	3747,
	3142,
	3671,
	3747,
	3747,
	1847,
	918,
	3786,
	3786,
	3786,
	2161,
	368,
	3786,
	3786,
	3747,
	3708,
	3786,
	3119,
	3119,
	1120,
	1120,
	1120,
	1120,
	3119,
	3119,
	1847,
	1847,
	3786,
	3786,
	2742,
	3786,
	3786,
	707,
	707,
	3786,
	707,
	707,
	3786,
	5339,
	3786,
	2919,
	3754,
	3148,
	3754,
	3148,
	3723,
	3119,
	3723,
	3723,
	3723,
	3786,
	3786,
	1847,
	3786,
	1737,
	3786,
	3786,
	1847,
	3786,
	3786,
	3786,
	3786,
	3786,
	607,
	3786,
	293,
	2855,
	1410,
	848,
	766,
	284,
	3786,
	3747,
	3747,
	3747,
	3754,
	3754,
	3754,
	2742,
	2161,
	2161,
	3119,
	1366,
	1389,
	1131,
	3119,
	3786,
	103,
	3754,
	3148,
	3708,
	3105,
	3723,
	3119,
	3723,
	3119,
	3781,
	3172,
	3685,
	3080,
	3723,
	3119,
	3723,
	3119,
	3786,
	4,
	757,
	1837,
	3885,
	3886,
	3884,
	3887,
	313,
	314,
	315,
	750,
	316,
	316,
	3051,
	3747,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	1847,
	37,
	2742,
	1850,
	2484,
	1847,
	3723,
	1850,
	1131,
	3,
	1132,
	747,
	2742,
	1128,
	1132,
	3119,
	723,
	1468,
	1217,
	3922,
	3923,
	3921,
	3924,
	724,
	725,
	724,
	723,
	725,
	1843,
	726,
	821,
	821,
	822,
	822,
	3119,
	1851,
	1106,
	318,
	726,
	1213,
	1213,
	3786,
	3786,
	1846,
	3119,
	1472,
	1847,
	3723,
	3708,
	3723,
	3786,
	3747,
	3723,
	3119,
	3685,
	3708,
	3685,
	3723,
	693,
	3723,
	3119,
	3786,
	5339,
	1255,
	3786,
	3786,
	5339,
	3786,
	2742,
	2348,
	1990,
	2348,
	2484,
	2348,
	3786,
	2742,
	3786,
	2742,
	3723,
	3119,
	3754,
	3148,
	3723,
	3754,
	3754,
	3747,
	3747,
	3786,
	2484,
	2484,
	2484,
	3786,
	2161,
	3119,
	1847,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	2742,
	5339,
	3786,
	2919,
	2917,
	1847,
	1847,
	3786,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3754,
	3723,
	3723,
	2742,
	2161,
	2161,
	3119,
	3119,
	3119,
	3786,
	3754,
	3754,
	3754,
	3747,
	3747,
	3747,
	3119,
	3119,
	3747,
	3723,
	3786,
	3684,
	3684,
	3786,
	3786,
	1213,
	1213,
	3786,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3708,
	3723,
	3723,
	3119,
	3119,
	3119,
	3119,
	3723,
	3119,
	3723,
	3119,
	3119,
	3723,
	3786,
	3786,
	1868,
	2742,
	3708,
	3142,
	3119,
	3119,
	3119,
	3119,
	11,
	2897,
	805,
	1158,
	1158,
	1158,
	3708,
	3105,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3747,
	3142,
	3786,
	3723,
	3723,
	2486,
	3786,
	1395,
	3119,
	3786,
	11,
	805,
	1158,
	1158,
	1158,
	3723,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	11,
	1158,
	1158,
	1158,
	805,
	3142,
	3105,
	3786,
	2384,
	11,
	1313,
	1074,
	732,
	3786,
	2480,
	1158,
	1158,
	1158,
	805,
	3723,
	3119,
	2484,
	3723,
	1389,
	3119,
	3119,
	3119,
	3119,
	1472,
	2742,
	3119,
	1846,
	1847,
	3119,
	3119,
	3119,
	3119,
	3723,
	3723,
	3119,
	1847,
	1847,
	3786,
	3786,
	3142,
	3786,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3786,
	519,
	1115,
	1145,
	3786,
	516,
	1115,
	3119,
	1406,
	2855,
	1145,
	2859,
	131,
	1879,
	3786,
	3786,
	3786,
	519,
	1115,
	1145,
	3786,
	3754,
	3708,
	1165,
	3142,
	1879,
	855,
	1115,
	1145,
	3786,
	3786,
	3786,
	3786,
	1145,
	3786,
	1145,
	3786,
	3786,
	2515,
	1182,
	1894,
	3119,
	3119,
	1850,
	3786,
	3708,
	3754,
	3786,
	3786,
	3747,
	3119,
	2515,
	1182,
	1141,
	1850,
	3786,
	3786,
	3723,
	3119,
	3747,
	3754,
	3754,
	3723,
	3754,
	3754,
	3754,
	3148,
	3786,
	3754,
	3148,
	3723,
	3119,
	3786,
	2467,
	131,
	3119,
	3119,
	3119,
	131,
	1509,
	3786,
	3747,
	3142,
	3723,
	3723,
	3754,
	3754,
	3723,
	3754,
	3754,
	3754,
	3708,
	3105,
	3754,
	3148,
	3754,
	3148,
	3747,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3786,
	3754,
	3754,
	3723,
	3786,
	3786,
	3786,
	3786,
	3786,
	1847,
	1847,
	3786,
	3786,
	3786,
	673,
	1389,
	3142,
	2467,
	3786,
	3786,
	3786,
	2742,
	2161,
	2161,
	3786,
	3786,
	3786,
	2484,
	3786,
	3119,
	3119,
	3786,
	3747,
	3747,
	3747,
	1837,
	3684,
	3079,
	3723,
	3119,
	3786,
	3786,
	3786,
	3723,
	3708,
	3786,
	2480,
	3786,
	3119,
	3119,
	3786,
	2742,
	1847,
	2484,
	1847,
	775,
	3786,
	3786,
	3786,
	3119,
	1847,
	3786,
	346,
	1847,
	2484,
	1847,
	3119,
	2161,
	1495,
	3723,
	3119,
	727,
	1105,
	1837,
	1128,
	3786,
	3786,
	5339,
	3786,
	2484,
	3723,
	3119,
	3723,
	3786,
	3928,
	3996,
	3889,
	3786,
	3723,
	3119,
	3754,
	3148,
	3723,
	3747,
	3754,
	3786,
	3723,
	3119,
	3754,
	3148,
	3754,
	3148,
	3754,
	3754,
	2467,
	3786,
	3723,
	3754,
	3754,
	3786,
	131,
	3786,
	3786,
	2484,
	2484,
	1399,
	3786,
	1387,
	3786,
	3119,
	1847,
	3747,
	3119,
	1120,
	3119,
	1120,
	3786,
	3786,
	707,
	3786,
	707,
	3723,
	3684,
	3684,
	3119,
	3119,
	2480,
	3754,
	3148,
	3754,
	3148,
	1847,
	3723,
	3119,
	3723,
	3119,
	3786,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3708,
	3105,
	3723,
	3786,
	1847,
	1847,
	3786,
	3747,
	3723,
	3723,
	3747,
	3747,
	3747,
	3754,
	3754,
	3754,
	3786,
	3786,
	3786,
	3786,
	1011,
	1033,
	3786,
	3119,
	1847,
	3119,
	1387,
	1399,
	2742,
	2161,
	2161,
	3786,
	2742,
	330,
	3708,
	3105,
	3723,
	3119,
	3723,
	3119,
	3684,
	3079,
	3781,
	3172,
	3786,
	16,
	1146,
	1130,
	759,
	759,
	3723,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	3708,
	3708,
	1847,
	1847,
	3747,
	3142,
	3684,
	3079,
	3747,
	3142,
	3684,
	3079,
	3786,
	3786,
	3786,
	3684,
	2742,
	3684,
	2742,
	2480,
	2159,
	1837,
	2348,
	2480,
	3723,
	3786,
	3786,
	1847,
	3786,
	3786,
	74,
	16,
	1131,
	1130,
	759,
	759,
	1847,
	1133,
	1133,
	1847,
	2484,
	1847,
	3119,
	1847,
	3119,
	3119,
	1215,
	1495,
	3119,
	329,
	748,
	1128,
	1128,
	2480,
	3119,
	3119,
	3786,
	3786,
	3786,
	5339,
	3786,
	2484,
	2484,
	3786,
	1851,
	3786,
	3786,
	3723,
	3119,
	2910,
	3723,
	3119,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3747,
	2910,
	1045,
	3723,
	1146,
	3786,
	3786,
	3786,
	3105,
	3786,
	3747,
	3775,
	3786,
	3723,
	3723,
	3723,
	3723,
	3119,
	3723,
	3119,
	1399,
	1389,
	2484,
	3747,
	1387,
	3786,
	3723,
	3747,
	3142,
	3786,
	3786,
	2159,
	1117,
	3786,
	3786,
	3747,
	3142,
	3684,
	3079,
	3747,
	3142,
	3684,
	3079,
	3747,
	3142,
	1033,
	1033,
	1033,
	650,
	651,
	649,
	2161,
	3786,
	1216,
	1216,
	3781,
	3781,
	652,
	1423,
	1033,
	1440,
	2709,
	1012,
	2855,
	3747,
	3142,
	3747,
	3142,
	2486,
	3786,
	886,
	548,
	34,
	3170,
	3786,
	3786,
	3119,
	3119,
	3786,
	3708,
	3105,
	3786,
	3708,
	3105,
	3723,
	3786,
	1850,
	3786,
	3119,
	1847,
	3786,
	3148,
	3172,
	3786,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	915,
	1131,
	2484,
	757,
	915,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3119,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3723,
	3708,
	3105,
	3723,
	3119,
	3723,
	3119,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	2742,
	2484,
	2484,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3119,
	3119,
	3119,
	3723,
	3786,
	3119,
	3723,
	3119,
	3723,
	3119,
	3723,
	3119,
	3119,
	3119,
	3119,
	3786,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3119,
	3786,
	736,
	3119,
	3723,
	3723,
	3723,
	3786,
	3670,
	3061,
	3684,
	3079,
	3684,
	3079,
	3747,
	3142,
	297,
	3723,
	3786,
	3723,
	3119,
	3786,
	3119,
	3119,
	1725,
	2742,
	3708,
	3708,
	3105,
	3708,
	3105,
	3119,
	290,
	3119,
	290,
	3723,
	3723,
	3786,
	3786,
	1038,
	3786,
	3786,
	3119,
	3119,
	3119,
	3119,
	3786,
	3786,
	2856,
	290,
	3786,
	3723,
	3723,
	3786,
	3723,
	3723,
	3119,
	290,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x020000F5, { 7, 8 } },
	{ 0x020000F8, { 15, 31 } },
	{ 0x060002B6, { 0, 1 } },
	{ 0x0600066F, { 1, 3 } },
	{ 0x06000707, { 4, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[46] = 
{
	{ (Il2CppRGCTXDataType)2, 388 },
	{ (Il2CppRGCTXDataType)3, 23409 },
	{ (Il2CppRGCTXDataType)3, 23408 },
	{ (Il2CppRGCTXDataType)3, 23410 },
	{ (Il2CppRGCTXDataType)3, 44758 },
	{ (Il2CppRGCTXDataType)2, 94 },
	{ (Il2CppRGCTXDataType)3, 44710 },
	{ (Il2CppRGCTXDataType)3, 23488 },
	{ (Il2CppRGCTXDataType)3, 23491 },
	{ (Il2CppRGCTXDataType)3, 23492 },
	{ (Il2CppRGCTXDataType)3, 23490 },
	{ (Il2CppRGCTXDataType)2, 1530 },
	{ (Il2CppRGCTXDataType)3, 23489 },
	{ (Il2CppRGCTXDataType)2, 13581 },
	{ (Il2CppRGCTXDataType)3, 23487 },
	{ (Il2CppRGCTXDataType)3, 23444 },
	{ (Il2CppRGCTXDataType)3, 7972 },
	{ (Il2CppRGCTXDataType)3, 7975 },
	{ (Il2CppRGCTXDataType)3, 7977 },
	{ (Il2CppRGCTXDataType)3, 7974 },
	{ (Il2CppRGCTXDataType)3, 23445 },
	{ (Il2CppRGCTXDataType)3, 23446 },
	{ (Il2CppRGCTXDataType)2, 1351 },
	{ (Il2CppRGCTXDataType)3, 7976 },
	{ (Il2CppRGCTXDataType)3, 11696 },
	{ (Il2CppRGCTXDataType)3, 23439 },
	{ (Il2CppRGCTXDataType)3, 23442 },
	{ (Il2CppRGCTXDataType)3, 7973 },
	{ (Il2CppRGCTXDataType)3, 11694 },
	{ (Il2CppRGCTXDataType)2, 6257 },
	{ (Il2CppRGCTXDataType)3, 45126 },
	{ (Il2CppRGCTXDataType)3, 11698 },
	{ (Il2CppRGCTXDataType)3, 11697 },
	{ (Il2CppRGCTXDataType)3, 23441 },
	{ (Il2CppRGCTXDataType)3, 14628 },
	{ (Il2CppRGCTXDataType)3, 3294 },
	{ (Il2CppRGCTXDataType)3, 14627 },
	{ (Il2CppRGCTXDataType)2, 8046 },
	{ (Il2CppRGCTXDataType)3, 23438 },
	{ (Il2CppRGCTXDataType)3, 11695 },
	{ (Il2CppRGCTXDataType)3, 23440 },
	{ (Il2CppRGCTXDataType)3, 23443 },
	{ (Il2CppRGCTXDataType)2, 7715 },
	{ (Il2CppRGCTXDataType)3, 11693 },
	{ (Il2CppRGCTXDataType)2, 13573 },
	{ (Il2CppRGCTXDataType)3, 23437 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpU2Dfirstpass_CodeGenModule = 
{
	"Assembly-CSharp-firstpass.dll",
	3209,
	s_methodPointers,
	104,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	46,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
