﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// ChartAndGraph.CanvasAttribute
struct CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069;
// ChartAndGraph.ChartFillerEditorAttribute
struct ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// ChartAndGraph.NonCanvasAttribute
struct NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// ChartAndGraph.SimpleAttribute
struct SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667;
// System.String
struct String_t;
// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* CustomChartPointer_t4EC5C8C22F34C8DA585F64F6D8DA28C2E25ACFC5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// ChartAndGraph.CanvasAttribute
struct CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// ChartAndGraph.NonCanvasAttribute
struct NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// ChartAndGraph.SimpleAttribute
struct SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.AttributeTargets
struct AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923 
{
public:
	// System.Int32 System.AttributeTargets::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeTargets_t5F71273DFE1D0CA9B8109F02A023A7DBA9BFC923, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// BarDataFiller/DataType
struct DataType_t8129745CCDE6EA5A4E6A0DFAFBBA2D59FDB29922 
{
public:
	// System.Int32 BarDataFiller/DataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_t8129745CCDE6EA5A4E6A0DFAFBBA2D59FDB29922, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// GraphDataFiller/DataType
struct DataType_tFF138E5B8042D53F5CDD6CB5D052D39F0E4D30BB 
{
public:
	// System.Int32 GraphDataFiller/DataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_tFF138E5B8042D53F5CDD6CB5D052D39F0E4D30BB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RadarDataFiller/DataType
struct DataType_t2DDCBA9710E812B2862E32D7C0D03132D0CBE9D9 
{
public:
	// System.Int32 RadarDataFiller/DataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataType_t2DDCBA9710E812B2862E32D7C0D03132D0CBE9D9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.AttributeUsageAttribute
struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.AttributeTargets System.AttributeUsageAttribute::m_attributeTarget
	int32_t ___m_attributeTarget_0;
	// System.Boolean System.AttributeUsageAttribute::m_allowMultiple
	bool ___m_allowMultiple_1;
	// System.Boolean System.AttributeUsageAttribute::m_inherited
	bool ___m_inherited_2;

public:
	inline static int32_t get_offset_of_m_attributeTarget_0() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_attributeTarget_0)); }
	inline int32_t get_m_attributeTarget_0() const { return ___m_attributeTarget_0; }
	inline int32_t* get_address_of_m_attributeTarget_0() { return &___m_attributeTarget_0; }
	inline void set_m_attributeTarget_0(int32_t value)
	{
		___m_attributeTarget_0 = value;
	}

	inline static int32_t get_offset_of_m_allowMultiple_1() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_allowMultiple_1)); }
	inline bool get_m_allowMultiple_1() const { return ___m_allowMultiple_1; }
	inline bool* get_address_of_m_allowMultiple_1() { return &___m_allowMultiple_1; }
	inline void set_m_allowMultiple_1(bool value)
	{
		___m_allowMultiple_1 = value;
	}

	inline static int32_t get_offset_of_m_inherited_2() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C, ___m_inherited_2)); }
	inline bool get_m_inherited_2() const { return ___m_inherited_2; }
	inline bool* get_address_of_m_inherited_2() { return &___m_inherited_2; }
	inline void set_m_inherited_2(bool value)
	{
		___m_inherited_2 = value;
	}
};

struct AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields
{
public:
	// System.AttributeUsageAttribute System.AttributeUsageAttribute::Default
	AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * ___Default_3;

public:
	inline static int32_t get_offset_of_Default_3() { return static_cast<int32_t>(offsetof(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C_StaticFields, ___Default_3)); }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * get_Default_3() const { return ___Default_3; }
	inline AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C ** get_address_of_Default_3() { return &___Default_3; }
	inline void set_Default_3(AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * value)
	{
		___Default_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Default_3), (void*)value);
	}
};


// ChartAndGraph.ChartFillerEditorAttribute
struct ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// GraphDataFiller/DataType ChartAndGraph.ChartFillerEditorAttribute::ShowForType
	int32_t ___ShowForType_0;
	// BarDataFiller/DataType ChartAndGraph.ChartFillerEditorAttribute::ShowForBarType
	int32_t ___ShowForBarType_1;
	// RadarDataFiller/DataType ChartAndGraph.ChartFillerEditorAttribute::ShowForRadarType
	int32_t ___ShowForRadarType_2;

public:
	inline static int32_t get_offset_of_ShowForType_0() { return static_cast<int32_t>(offsetof(ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9, ___ShowForType_0)); }
	inline int32_t get_ShowForType_0() const { return ___ShowForType_0; }
	inline int32_t* get_address_of_ShowForType_0() { return &___ShowForType_0; }
	inline void set_ShowForType_0(int32_t value)
	{
		___ShowForType_0 = value;
	}

	inline static int32_t get_offset_of_ShowForBarType_1() { return static_cast<int32_t>(offsetof(ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9, ___ShowForBarType_1)); }
	inline int32_t get_ShowForBarType_1() const { return ___ShowForBarType_1; }
	inline int32_t* get_address_of_ShowForBarType_1() { return &___ShowForBarType_1; }
	inline void set_ShowForBarType_1(int32_t value)
	{
		___ShowForBarType_1 = value;
	}

	inline static int32_t get_offset_of_ShowForRadarType_2() { return static_cast<int32_t>(offsetof(ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9, ___ShowForRadarType_2)); }
	inline int32_t get_ShowForRadarType_2() const { return ___ShowForRadarType_2; }
	inline int32_t* get_address_of_ShowForRadarType_2() { return &___ShowForRadarType_2; }
	inline void set_ShowForRadarType_2(int32_t value)
	{
		___ShowForRadarType_2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void ChartAndGraph.ChartFillerEditorAttribute::.ctor(BarDataFiller/DataType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChartFillerEditorAttribute__ctor_mF182ABD485E7833635757C57D47433F2841475AA (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * __this, int32_t ___type0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void ChartAndGraph.ChartFillerEditorAttribute::.ctor(GraphDataFiller/DataType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * __this, int32_t ___type0, const RuntimeMethod* method);
// System.Void ChartAndGraph.ChartFillerEditorAttribute::.ctor(RadarDataFiller/DataType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChartFillerEditorAttribute__ctor_mF6FDCA0F315DB5CB80A0D6C4B49E795AEE6A4927 (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * __this, int32_t ___type0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.ThreadStaticAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::.ctor(System.AttributeTargets)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, int32_t ___validOn0, const RuntimeMethod* method);
// System.Void System.AttributeUsageAttribute::set_AllowMultiple(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void ChartAndGraph.NonCanvasAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6 (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * __this, const RuntimeMethod* method);
// System.Void ChartAndGraph.CanvasAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299 (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * __this, const RuntimeMethod* method);
// System.Void ChartAndGraph.SimpleAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * __this, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868 (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, const RuntimeMethod* method);
static void AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[2];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void BarContentFitter_t2CB20F0FDAC91A858D0AC80D75F3D4918CA10D8B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_0_0_0_var), NULL);
	}
}
static void BarDataFiller_t3BCD05D26ACB05BC8E69A76364821707E72F0CC6_CustomAttributesCacheGenerator_BarDataFiller_GetData_mD41A3305539B936D9A51B5EF5F6EFF55107730C2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_0_0_0_var), NULL);
	}
}
static void CategoryData_tFDD3D32896EEB6A2DBB307635F35800942855C31_CustomAttributesCacheGenerator_Name(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_mF182ABD485E7833635757C57D47433F2841475AA(tmp, 1LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_mF182ABD485E7833635757C57D47433F2841475AA(tmp, 0LL, NULL);
	}
}
static void CategoryData_tFDD3D32896EEB6A2DBB307635F35800942855C31_CustomAttributesCacheGenerator_DataObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_mF182ABD485E7833635757C57D47433F2841475AA(tmp, 0LL, NULL);
	}
}
static void U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24__ctor_m23346B90BF5520515FB01571AAD820E9056BC2AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_IDisposable_Dispose_mEC6A57765D641AA00A34DCFA6092965012FE2958(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D8AD14E0C19EE312760EFFF80A2F14CBE757EFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_mB0F4ABD0744B078491FA16002ED4D4749F55AD9E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_mCD2136265947A93966E384448D5C93D5B0D037D5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GraphDataFiller_t61E9DCD0514869E9116CF79779DA2B0250E1D5B5_CustomAttributesCacheGenerator_GraphDataFiller_GetData_mDB9DBCC704135811FA58F24E972BEAF1918525C8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_0_0_0_var), NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_Name(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[2];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 0LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_DataFormat(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 0LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_Skip(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 0LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_ParentObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_DataObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 0LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_XDataObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_YDataObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_SizeDataObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_XDateFormat(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
}
static void CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_YDateFormat(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 2LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_m80C3DDD10F5D62357297352F43DCD0C1AE7C4DCD(tmp, 1LL, NULL);
	}
}
static void U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29__ctor_m3C343F3EC10F96FC8639D32AA99C25FA12075BD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_IDisposable_Dispose_mE4AA5BFA3FC25D675CA57B4C8075D20B84A1830B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F32C1C524518E6F04CCCCAD3771DAAE1665EC45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_Collections_IEnumerator_Reset_m7DCBB1D02F477820DCE81C95EB63B1D2747AC323(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_Collections_IEnumerator_get_Current_m628029FF6FC5324234A6D305A09351A288BF00D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PieDataFiller_tE8F7318A12D99CCBB9818DEC6756D9A1E6A53EDE_CustomAttributesCacheGenerator_PieDataFiller_GetData_m7AD75ED5136A7540B377D95CDC73B5156AE07BE0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_0_0_0_var), NULL);
	}
}
static void U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21__ctor_m5FDFF7FBE070B5D39B9735F93E54F91C4650592E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_IDisposable_Dispose_m3690B5FEA5C363A68E5CF5491D67E4DAF05E0C08(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m162CF48EF0D446615C4EF6D006C692CDD0154710(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_Collections_IEnumerator_Reset_m37DE13669B84A238C4A939A80966461EADE38B04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_Collections_IEnumerator_get_Current_m36AEC0B435EC47BE9E67D3BF1F81D07AE22C4C1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RadarDataFiller_tE61CC6A21189511702B918A36F3A0F5078AF39D8_CustomAttributesCacheGenerator_RadarDataFiller_GetData_mB7FDF2D44705CFB8C333109D80F1D6581A254D91(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_0_0_0_var), NULL);
	}
}
static void CategoryData_tDABB1C8838A4A1D1E04FA8476579E25A3B413532_CustomAttributesCacheGenerator_Name(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_mF6FDCA0F315DB5CB80A0D6C4B49E795AEE6A4927(tmp, 1LL, NULL);
	}
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[1];
		ChartFillerEditorAttribute__ctor_mF6FDCA0F315DB5CB80A0D6C4B49E795AEE6A4927(tmp, 0LL, NULL);
	}
}
static void CategoryData_tDABB1C8838A4A1D1E04FA8476579E25A3B413532_CustomAttributesCacheGenerator_DataObjectName(CustomAttributesCache* cache)
{
	{
		ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 * tmp = (ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9 *)cache->attributes[0];
		ChartFillerEditorAttribute__ctor_mF6FDCA0F315DB5CB80A0D6C4B49E795AEE6A4927(tmp, 0LL, NULL);
	}
}
static void U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24__ctor_mE5AC81C7F88B9C90F79E72BE064CC18DD6783980(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_IDisposable_Dispose_m1682B988B3E5EC516BD0A4DFE5AA57A883EE6F51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70B61A70670719AED94EC9A2B8C80E97A217024D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_m5F195A69B5832A7F5B608683EC834806914785E3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_m2101121EA708E368E26EAF98BE1B497878498183(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_U3CUITextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_U3CUserDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_get_UIText_mAB7F8962EC45CF04171D3F0D4C2CF3BBE84DB0B0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_set_UIText_m06B97F41D530290E48CC6D721F830DAA5C0380B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_get_UserData_mE921AB0E2FE52066A0EA5DA30DFA7D3F80157BE8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_set_UserData_m0E39DB9C7C06ADB39CF5E98F7B89C46D9B6D4571(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryLabels_t7F560C7AC111B78F13D44B39C8C137F64D74517E_CustomAttributesCacheGenerator_visibleLabels(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x74\x65\x72\x6D\x69\x6E\x65\x73\x20\x77\x68\x69\x63\x68\x20\x6C\x61\x62\x65\x6C\x73\x20\x61\x72\x65\x20\x76\x69\x73\x69\x62\x6C\x65"), NULL);
	}
}
static void CategoryLabels_t7F560C7AC111B78F13D44B39C8C137F64D74517E_CustomAttributesCacheGenerator_CategoryLabels_U3Cget_AssignU3Eb__6_0_m1E37F7A84B474AC9DE3E6366332A2BF2A757026C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupLabels_t1103562719B3D26A50E5BD3D08EACAA7DF809C31_CustomAttributesCacheGenerator_alignment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x74\x65\x6D\x69\x6E\x65\x73\x20\x74\x68\x65\x20\x61\x6C\x69\x67\x6E\x6D\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x72\x6F\x75\x70\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
}
static void GroupLabels_t1103562719B3D26A50E5BD3D08EACAA7DF809C31_CustomAttributesCacheGenerator_GroupLabels_U3Cget_AssignU3Eb__5_0_m8BB30C6A5D704B6E4463E5D6C87799990E5BAB9E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_textPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x70\x72\x65\x66\x61\x62\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x72\x65\x61\x74\x65\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x61\x73\x73\x6F\x63\x69\x61\x74\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x2E\x20\x49\x66\x20\x74\x68\x65\x20\x70\x72\x65\x66\x61\x62\x20\x69\x73\x20\x6E\x75\x6C\x6C\x20\x6E\x6F\x20\x6C\x61\x62\x65\x6C\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x68\x6F\x77\x6E"), NULL);
	}
}
static void ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_textFormat(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x20\x74\x68\x65\x20\x66\x6F\x72\x6D\x61\x74\x74\x69\x6E\x67\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x61\x62\x65\x6C\x20\x64\x61\x74\x61\x2E\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x73\x20\x61\x72\x65\x20\x61\x76\x61\x69\x6C\x61\x62\x6C\x65\x20\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x75\x73\x65\x20\x74\x68\x65\x20\x70\x72\x65\x64\x65\x66\x69\x6E\x65\x64\x20\x6D\x61\x63\x72\x6F\x73\x20\x3A\x20\x27\x5C\x6E\x27\x20\x66\x6F\x72\x20\x6E\x65\x77\x6C\x69\x6E\x65\x20\x2C\x20\x27\x3C\x3F\x63\x61\x74\x65\x67\x6F\x72\x79\x3E\x27\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x63\x61\x74\x65\x67\x6F\x72\x79\x20\x61\x6E\x64\x20\x27\x3C\x3F\x67\x72\x6F\x75\x70\x3E\x27\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x67\x72\x6F\x75\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_fontSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x66\x6F\x6E\x74\x20\x73\x69\x7A\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_fontSharpness(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x61\x64\x6A\x75\x73\x74\x73\x20\x74\x68\x65\x20\x73\x68\x61\x72\x70\x6E\x65\x73\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x6F\x6E\x74"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 3.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_seperation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x6C\x61\x62\x65\x6C\x20\x66\x72\x6F\x6D\x20\x69\x74\x27\x73\x20\x6F\x72\x69\x67\x69\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_location(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x61\x62\x65\x6C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x74\x65\x6D"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TextController_t2B01B36AAF87E0AB554C457EE44056351C9C79E9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_tA3CCC09942E2F3195532FC009C891A4B9A1DA601_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HoverText_t851B98B482893DB3A6168ED3D76FA1FCA3776188_CustomAttributesCacheGenerator_HoverText_SelectText_mC859EF99FE3BA0697E61E7784454E77239FECEA9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_tC0C7D4916053334B4AEAA3A02788811EF98B4869_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11__ctor_m97FF7574BECDA2356CE91AB79D964D437607610B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m0137CCE653C298077E28C9B537639ABF411CF179(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD62DC4EDF7D813AD2A227E8A3997EC293D5F4D42(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m4A0993E9C1AE47CF9B6BCABFB295969705F88EB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m1D812B2A83110FF66329D8CF9E21A57F6D078C09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t4123600E95DCFA38B3656F4920820A7285BB739C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphChartFeed_t778BCE9C466C9547B6B59D00FF1B55C596AC1319_CustomAttributesCacheGenerator_GraphChartFeed_ClearAll_mFDDC6A3A71B5FBA7B10276AA0261B00A77514396(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_0_0_0_var), NULL);
	}
}
static void U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1__ctor_mC3D3B41DC95CA047E8CBB6D4EE7EE0A3C9B93887(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_IDisposable_Dispose_m9C5BBF85B5384B6AB898F8AE85C44DCC0AA54B93(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BB8B95E41786ADB3903FAAE2B823A72A4E4C0CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_Collections_IEnumerator_Reset_mFCAE984F92148CEAD61AFDC447B2B13371123DD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_Collections_IEnumerator_get_Current_mC9FDAC481477167131505D1A917E36F942CA3CD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PointHighlight_tB4103DCE16A58CFBD7D300E42F30CD6E24BFB974_CustomAttributesCacheGenerator_PointHighlight_SelectText_m8A72511CDE4D7594F99C839614E4CD5C12AF43DE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t1511C530E9C57D2EAA6D009320443957E751F8A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11__ctor_m3A5A14A90EDDBB09F4306CC9935BAB476EA0D3D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m5D7B4F7984536AFA0FC52BED1C0B323B93EBB09A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF876D96FB9874E3A5498EBF30EDA4899359AB74C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m9E9192BE9A6918BBBAE4E1CE01CE78D9250D66A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m4FD7A47CBC202B781432238F82B2B019FDDC5878(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MixedCharts_t47E9B56D0A4A93EBC3EF42C248DDDF093F5CCC0D_CustomAttributesCacheGenerator_MixedCharts_FillGraphWait_m4D334BD00C325E608B9863ACF308B1CFB44CDBF4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_0_0_0_var), NULL);
	}
}
static void U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3__ctor_m605B8A9DF0DEF3C1D6455E9106738FEF84BB1D27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_IDisposable_Dispose_m8252579F9A52AC98B6829F106FB8413592DB3A2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7564710146FBEBC473D30BAD1A81F231303B6F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_Reset_m5A91A4B97550476A7720F47010FEE9AB4FC11D8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_get_Current_m349F3E4E8B051771D60B027E82B421A6949E0546(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GraphRectZoom_tAD8A839F845F838855A9B6958FB463E4DE777CE0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CustomChartPointer_t4EC5C8C22F34C8DA585F64F6D8DA28C2E25ACFC5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CustomChartPointer_t4EC5C8C22F34C8DA585F64F6D8DA28C2E25ACFC5_0_0_0_var), NULL);
	}
}
static void GraphZoom_t942615FE8FBC67CB3B822C9F2700F217F3A6B829_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_0_0_0_var), NULL);
	}
}
static void BasicSample_tE5EFC159CADB6A8C8E71AA56BAD9A061A209C1C4_CustomAttributesCacheGenerator_BasicSample_U3COnGUIU3Eb__1_0_m1A5B0705F6B217AA15C3D60BA3B8F957AE9A97DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BasicSample_tE5EFC159CADB6A8C8E71AA56BAD9A061A209C1C4_CustomAttributesCacheGenerator_BasicSample_U3COnGUIU3Eb__1_1_m854014CDE7FA4D215818F9CCE18CC1C06D8666BE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BasicSample_tE5EFC159CADB6A8C8E71AA56BAD9A061A209C1C4_CustomAttributesCacheGenerator_BasicSample_U3COnGUIU3Eb__1_2_m640031C29BC7B5CA6966D6C5A57C5FA0F42654C8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasSampleOpenFileImage_t16B5870D57BD12D55FA713BA93628EEF53FFEE37_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void CanvasSampleOpenFileImage_t16B5870D57BD12D55FA713BA93628EEF53FFEE37_CustomAttributesCacheGenerator_CanvasSampleOpenFileImage_OutputRoutine_mC6AC6C7020F774BBBB5C1EC7D70113DAC9CE2AE1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_0_0_0_var), NULL);
	}
}
static void U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4__ctor_m98B984849785F14A4C8AB9C792DD30C7E2C217DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m93AEB066101CFA725B073186A05AA61331643C68(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED88C4EFD5B495F4D2972BFFEDEDA8A4FA23A8BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m5F856A6AF129DDDCD28467ABFEA0B6EC6E826F73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m8F721991272364EAF9BEDDBBB389F230FF990E2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CanvasSampleOpenFileText_tBD2B2C8E2DAA1044307221A4BC638C0F6250A0AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void CanvasSampleOpenFileText_tBD2B2C8E2DAA1044307221A4BC638C0F6250A0AA_CustomAttributesCacheGenerator_CanvasSampleOpenFileText_OutputRoutine_m6EC022ECFF5A958B50C9FD430032EE0A18972DD5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_0_0_0_var), NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4__ctor_m6052F5D62A93EBD9C0E11383EB07CBD7660382CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m185B54E87BBFA49A5D93D56378C5D5B699C6D0E3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD425B64024BB6BC1769FAD619E67F9F309A486D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_mF82EE533AC3EFB9F1A356CA8603DE322C066D634(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m95A29BAFA9A8371AEF30517414CE3A5CD502F8B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CanvasSampleOpenFileTextMultiple_t6A74601385701DA6AE2BD30AEEA0217D8987F387_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void CanvasSampleOpenFileTextMultiple_t6A74601385701DA6AE2BD30AEEA0217D8987F387_CustomAttributesCacheGenerator_CanvasSampleOpenFileTextMultiple_OutputRoutine_mBC9DD3FC851968220AFE1A5B97FF7F0A81A830C2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_0_0_0_var), NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4__ctor_mBDFF06C3C8D544BE9FBC7EA29B4790B7F3F288ED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_mEB828D2EA10D8A142380291F8820E27C7F743386(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57BAA75BD2D41E4F0A357D1679D79CF925EF1FD5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m15778AB91FC3ABCC7AA558BFE3224F64EA05925E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_mC1183837FBAE1B02788D12B358A1F890CE7D069F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CanvasSampleSaveFileImage_t99EC606B45EAD318F89C4C5FD3949DE66A196755_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void CanvasSampleSaveFileText_tD184142E7CE1AE4783312141F0AD832603D4F388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D_0_0_0_var), NULL);
	}
}
static void ExtensionFilter_t0526FEA4184C326B0C4F04155B5EA8114F2E83EF_CustomAttributesCacheGenerator_ExtensionFilter__ctor_m005AD482A024C782159F183FFD5579DBF4CD7887____filterExtensions1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator_m_EscapeBuilder(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * tmp = (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A(tmp, NULL);
	}
}
static void JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator_JSONNode_get_Children_m9A419003FE3E6C582C581168325DFF2F916EA4E6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_0_0_0_var), NULL);
	}
}
static void JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator_JSONNode_get_DeepChildren_mA4B063A992BD18F9254C2E4798DA93637ED05EEE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_0_0_0_var), NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40__ctor_mD2664979A1B8AE875DDB2840B6156772FB3ECFDD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m944D00543660D19F0E42EC6B6B139CB5D7EDFECC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m4FC76B45630A04E0A0046A1409F00FB5FABE46B9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_m97CD00E06349E3DE3CAD68B058AE2EA97797AEDC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_m8F61A110C435979412391A15BC54115CA7E0DEE5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_mD91303D1AB77DD3B2593235773ED3B39DF1C2895(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m8E4D404140934BAD769BAA96C71954DF437DBEB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42__ctor_m1AD4A760497B8C4CAB4E547E035B8ED4EBDDBFAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_m75E2DD336D0A67E1EA6196D218C1F2D9E26D2242(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_mF52E7C66AC2CFE035C5422378EBA10E005BD16FD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_m093AE80D972ACB3ED4CF25C53041441B789576A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_mDC85D32E36F7E3EEA9C509E44985F3540F4659A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m3E11223B21486BF7B377ED4B07EA9557C32ED634(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mB972D9FF70DEDEBBC7507696C8353787B73D448A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONArray_t04D56DA332EFB150BB42B2CC5764F7C4A41D92A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONArray_t04D56DA332EFB150BB42B2CC5764F7C4A41D92A5_CustomAttributesCacheGenerator_JSONArray_get_Children_m7459647CAFE8FB57E0C4D2F757AA72BCA7749CEC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_0_0_0_var), NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22__ctor_m09E100DFDE814208000E288F38AACAA139288972(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mFBB72D4FC09BA522703066660A1B6C84A2B2662E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m0997099AAF452C670AC89A3DC8EB6F3F42EF8A2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mA44FDA37FE9633368C64D524CC7FFBD664058879(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mDB53E925A7A6DC776A4F6BBB37B8E7AE9A7B4ACC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m85636C898F51CA71FB68F27A5F0E3AE34CE40854(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m2724CBA0FE19F63D89407542FD0085D90ACBEC0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONObject_tAC0F80D5CCC63632CF36C37AE4112FCBE6AE196E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONObject_tAC0F80D5CCC63632CF36C37AE4112FCBE6AE196E_CustomAttributesCacheGenerator_JSONObject_get_Children_mBA3F815A9E4AA185FB9D3B41346F80D3D89FCEEB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t97AC8537B1356ECB017CDF0A3BD89ECF81CB65DE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23__ctor_m27FE9E533731FF76A4DA5A1495777DE4976EB2B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m14D80EBCF6DEF404F017771F3E97B465C4E7B66A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m406355CB8A2EB21F8264E1A23C80A1921AB1C795(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mC76C5EF6AE752D074A3A428BEBAA2A1CCAB85232(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m99340EAA456C373D91FF62F016A592BBCFDAEC01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m1ADC7153B59830633F604BBAB11DC65F176AC157(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m78AC7A7CE03612379624BE539ABBE50619F0149C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONLazyCreator_t5F13AFBDD4F4BD950659E92C30BCE03BA8C552AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JsonParser_tB519CED5504523188C3E6488A912391A11661B8A_CustomAttributesCacheGenerator_JsonParser_GetAllChildObjects_m64B3F53686284293C50CD18063C787EF61B153C2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_0_0_0_var), NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12__ctor_m47139479FE975B344E47217D280FFC312E318714(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_m8BBB18472020BE742C8E39B81239FDACD74663BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m24515EF29C62E5B28F804336D64C95621B543A8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_mD2374F91492CA89E5E1A6224468E705D910E38CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m3715E650CF1A0C57AAA0F132B149816F130815C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_mEC7AFF580DB38394CB779DFBD45E50A202643EF1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE9A7F7C95471C247081CF66366D9A4DA207933EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void XMLParser_tAEAA67638C8565FDD734BFAFCF7450135AA8755A_CustomAttributesCacheGenerator_XMLParser_GetAllChildObjects_mE2F140C257C62461EF9667E77BF1BD013EE8459B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_0_0_0_var), NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12__ctor_m5531B147F08A6B3597DD2C73FF2550DD018683FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_mD6F2D843626DF724B8E34D5DF9B05FDD7D826D98(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m5D93105895C6A0F0C06B8B11727EA7C8B91C8E8F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_m5FB1E2B73B3C5F5EA1DA48A97191F445FCB1CE0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m24F99202D95EB103F25D1F71E0FDC04BED1A8CCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_m8F633C74F5985C71F8C1B945DF1A566EEF7DDDFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m36887960DD50A7F7F13F9B101A78F4D3FCCA2741(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_backMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x61\x63\x6B\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x79\x72\x61\x6D\x69\x64"), NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_inset(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6E\x73\x65\x74\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x70\x79\x72\x61\x6D\x69\x64\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_justification(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x65\x78\x74\x20\x6A\x75\x73\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x79\x72\x61\x6D\x69\x64\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_slope(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x6C\x6F\x70\x65\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x79\x72\x61\x6D\x69\x64"), NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_prefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x79\x72\x61\x6D\x69\x64\x20\x69\x74\x65\x6D\x2E\x20\x6D\x75\x73\x74\x20\x63\x6F\x6E\x74\x61\x69\x6E\x20\x61\x20\x50\x79\x72\x61\x6D\x69\x64\x43\x61\x6E\x76\x61\x73\x47\x65\x6E\x65\x72\x61\x74\x6F\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_U3CTitleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_get_Category_m775875544DFBCAB3C0166BD745E20B325A86949D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_set_Category_mDE1EDDF8E56E2BE4BFA785EC1BE243D6BFE91E47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_get_Title_m9DFB5ADA82BE5AB49408925E61963B2A0FA8A5CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_set_Title_m6A6B22563451D13C04CA6969727FD453C5523235(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_get_Text_mE039C1B01C569C62FF5A03AAB2FB2E60C9697B13(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_set_Text_mA128C4B843BCB63A903A1C4EF0CD6CF9168BCFBE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_mCategories(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_mGroups(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_mData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_ProperyUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_RealtimeProperyUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_add_ProperyUpdated_m8DFF08F96D64766B67602D3E02BCD47F9E38C7C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_remove_ProperyUpdated_m6AD009FE63BD91431ECBEA9447D177E4A0A902AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_add_RealtimeProperyUpdated_m11C0E533367D009433E0BEAA414BF8FE6E37E88D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_remove_RealtimeProperyUpdated_mE3E870B58AC7B15C2FAF95507D56A16D56181C02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t987457C1F23A8F6B930C9FD8728008D8876B1729_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_heightRatio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x68\x65\x69\x67\x68\x74\x20\x72\x61\x74\x69\x6F\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_widthRatio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x77\x69\x64\x74\x68\x20\x72\x61\x74\x69\x6F\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_thicknessMode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x63\x6B\x6E\x65\x73\x73\x20\x6D\x6F\x64\x65\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x61\x6E\x64\x6C\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_thicknessConstant(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x63\x6B\x6E\x65\x73\x73\x20\x63\x6F\x6E\x74\x61\x6E\x74\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x61\x6E\x64\x6C\x65\x20\x63\x68\x61\x72\x74\x20\x2C\x20\x69\x74\x27\x73\x20\x6D\x65\x61\x6E\x69\x6E\x67\x20\x64\x65\x70\x65\x6E\x64\x73\x20\x6F\x6E\x20\x74\x68\x65\x20\x74\x68\x69\x63\x6B\x6E\x65\x73\x73\x20\x6D\x6F\x64\x65"), NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_itemFormat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x66\x6F\x72\x6D\x61\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x6C\x61\x62\x65\x6C\x73\x3A\x20\x3C\x3F\x73\x74\x61\x72\x74\x3E\x20\x2C\x20\x3C\x3F\x64\x75\x72\x61\x74\x69\x6F\x6E\x3E\x2C\x3C\x3F\x6F\x70\x65\x6E\x3E\x2C\x3C\x3F\x68\x69\x67\x68\x3E\x2C\x3C\x3F\x6C\x6F\x77\x3E\x2C\x3C\x3F\x63\x6C\x6F\x73\x65\x3E"), NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_bodyFormat(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x75\x73\x69\x6E\x67\x20\x68\x6F\x76\x65\x72\x49\x74\x65\x6D\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6D\x6F\x75\x73\x65\x20\x69\x73\x20\x68\x6F\x76\x65\x72\x69\x6E\x67\x20\x6F\x76\x65\x72\x20\x74\x68\x65\x20\x62\x6F\x64\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x6E\x64\x6C\x65\x2C\x66\x6F\x72\x6D\x61\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x6C\x61\x62\x65\x6C\x73\x3A\x20\x3C\x3F\x73\x74\x61\x72\x74\x3E\x20\x2C\x20\x3C\x3F\x64\x75\x72\x61\x74\x69\x6F\x6E\x3E\x2C\x3C\x3F\x6F\x70\x65\x6E\x3E\x2C\x3C\x3F\x68\x69\x67\x68\x3E\x2C\x3C\x3F\x6C\x6F\x77\x3E\x2C\x3C\x3F\x63\x6C\x6F\x73\x65\x3E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_highFormat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x75\x73\x69\x6E\x67\x20\x68\x6F\x76\x65\x72\x49\x74\x65\x6D\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6D\x6F\x75\x73\x65\x20\x69\x73\x20\x68\x6F\x76\x65\x72\x69\x6E\x67\x20\x6F\x76\x65\x72\x20\x74\x68\x65\x20\x68\x69\x67\x68\x20\x6C\x69\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x6E\x64\x6C\x65\x2C\x66\x6F\x72\x6D\x61\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x6C\x61\x62\x65\x6C\x73\x3A\x20\x3C\x3F\x73\x74\x61\x72\x74\x3E\x20\x2C\x20\x3C\x3F\x64\x75\x72\x61\x74\x69\x6F\x6E\x3E\x2C\x3C\x3F\x6F\x70\x65\x6E\x3E\x2C\x3C\x3F\x68\x69\x67\x68\x3E\x2C\x3C\x3F\x6C\x6F\x77\x3E\x2C\x3C\x3F\x63\x6C\x6F\x73\x65\x3E"), NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_lowFormat(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x75\x73\x69\x6E\x67\x20\x68\x6F\x76\x65\x72\x49\x74\x65\x6D\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x2C\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x6D\x6F\x75\x73\x65\x20\x69\x73\x20\x68\x6F\x76\x65\x72\x69\x6E\x67\x20\x6F\x76\x65\x72\x20\x74\x68\x65\x20\x6C\x6F\x77\x20\x6C\x69\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x61\x6E\x64\x6C\x65\x2C\x66\x6F\x72\x6D\x61\x74\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x66\x6F\x6C\x6C\x6F\x77\x69\x6E\x67\x20\x6C\x61\x62\x65\x6C\x73\x3A\x20\x3C\x3F\x73\x74\x61\x72\x74\x3E\x20\x2C\x20\x3C\x3F\x64\x75\x72\x61\x74\x69\x6F\x6E\x3E\x2C\x3C\x3F\x6F\x70\x65\x6E\x3E\x2C\x3C\x3F\x68\x69\x67\x68\x3E\x2C\x3C\x3F\x6C\x6F\x77\x3E\x2C\x3C\x3F\x63\x6C\x6F\x73\x65\x3E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CSelectionRectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CCandleValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_SelectionRect_m1BA91E7AC81BEF22E923E40B1B6F3D1A980EC63D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_SelectionRect_m335496950C896DF96106DA2CC0C7A5E2D9DFF041(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_Position_m4E369BBC39D884BFC6196F170705CE2F787B99A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_Position_m2F6E62EE3976061B158387AA04B8DBA38BB6F5F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_Index_m1124A9CE28D536D86334A6988DEDEA4CB97D7FC2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_Index_mA835DEEF93ABB5FC3E5D025662A85A18AD07570E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_CandleValue_m8D6CA1E8F4F22BC926F537B47ED160F769992D43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_CandleValue_mD145E35AA00E49F70E63237F7F72853F916879D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_Category_m9ED5F70A96903ED3BAACD6717412E2D09596F354(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_Category_m3D313A3E5EE3DA1262B5DEF2C06AD4711E3FDD8A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CandleChartData_tA8E56FFE52FDC511919850D56E37A0C2BFD75BB2_CustomAttributesCacheGenerator_mSerializedData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MaxX(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MaxY(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MinX(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MinY(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_t17FF798A47EB32301A9CAF01F84D5C8D3307B57A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_Hover(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_Click(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_Leave(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_add_Hover_m7A03F0024F862BDB44D4B95310A38BA14E6EC5FB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_remove_Hover_m08E6A223A93FA472CB1946DC0298731133BF76EE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_add_Click_m2955A032522633F04F7896963B5DE7E69FA672AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_remove_Click_mB33B571C99D746F92B3E0A20ACE09828A18DC08E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_add_Leave_m473992F13C76B971C580B3B73D112C80F7F5136E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_remove_Leave_mC622586CBA098291105A33A16BC79ABAAF382AB8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_tD66B40B5A861605561DE2139F8C5365DC3D1F506_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandleChart_tEA6058533D5E8ED7ECD1DCD04F3AF9B34CED0AD9_CustomAttributesCacheGenerator_fitToContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasCandleChart_tEA6058533D5E8ED7ECD1DCD04F3AF9B34CED0AD9_CustomAttributesCacheGenerator_fitMargin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasCandleChart_tEA6058533D5E8ED7ECD1DCD04F3AF9B34CED0AD9_CustomAttributesCacheGenerator_CanvasCandleChart_U3COnItemLeaveU3Eb__45_0_m811F170BE8838C17E66ADF78BEC2F32838A8F701(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_Hover(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_Click(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_Leave(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_add_Hover_m8BF66FBE73A4BB5FBD226C41DBB6F894B3BBF93E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_remove_Hover_m162A8BCC790D2A9BEB17971D675E30F13ACBA443(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_add_Click_m7131C9252F108EACEB73E742A113B1745186FC6A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_remove_Click_m704370212C1B5977C2B01D7EE3E5F7E3AA699F83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_add_Leave_mCDC72A0E33C950DC0EF06F5175A3728E3FE0782F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_remove_Leave_mD290676B1FF98DCE20CB2D9ADAF06D8C6FF87D98(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_tDEF17818921A9F591C8F434FC8E023A4A56CD1DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass39_0_t6CFCEA55CF41A45D364631A3104F2A76AA6EC751_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasCandleGraphic_t365BA4E9C82105329E6F8A213CA4315700C93D23_CustomAttributesCacheGenerator_CanvasCandleGraphic_getOutline_m798E8CB9D2C34E3562F6F0AC84703B1D56FA42C5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_0_0_0_var), NULL);
	}
}
static void CanvasCandleGraphic_t365BA4E9C82105329E6F8A213CA4315700C93D23_CustomAttributesCacheGenerator_CanvasCandleGraphic_getCandle_m3B6B00CD1C59B6EDD3CE21871A5804774A79B9CC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_0_0_0_var), NULL);
	}
}
static void CanvasCandleGraphic_t365BA4E9C82105329E6F8A213CA4315700C93D23_CustomAttributesCacheGenerator_CanvasCandleGraphic_getLine_mA140EF9183453BEC0C4FC21070BD37783E57AA89(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_0_0_0_var), NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19__ctor_mEFDBA2A898B7FBF0830DD5FB9F9B83F4BC10E633(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_IDisposable_Dispose_m42DB601169ECF3C4049FF5FA8C7C40A82768B6DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m13CD6F38EC403E61CC2DFA08B5F37CF5E297D386(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_Reset_m8F76A0AA1EFF342B4F641587492819DF94693951(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_get_Current_m8F0F3DDF253B4632CAAD0D24DFF88B1BA6D1E9BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m6D8E95C447218F47700C4C2495D423ACE17CB9AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_IEnumerable_GetEnumerator_mE3E50EE5A6C6DB0C705F3B7DDDD78C530B4DA2C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20__ctor_mC82F7A257D54893E320CAA74A4A9F8E3C46F7A6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_IDisposable_Dispose_m81A77A4E49DF47FE1ABDD3EB07FF558721202A35(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m4BD2F0D780FDE9F01E9498D7976DA81876228CEE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_IEnumerator_Reset_m6F0292D8486F07BDD361E3A7872B63C1961C1EC0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_IEnumerator_get_Current_m9111EB1D61ACFBDCC6E9593D576936C79A24EF70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mB9B8A2839CD947E1084EEE1508B62501C9C9A98B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_IEnumerable_GetEnumerator_m9645F135E86563D363D9F1A8D234A86E607AC9F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21__ctor_m0326018FB282A0BA3FEC754F4D9D47FA72F54172(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_IDisposable_Dispose_mA0D475FB96EB7B1DB8A497F5E7A45D331428E58D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m5603D072D3ECD9E42D2013A333694F7E1C09C69A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_IEnumerator_Reset_m01DC42B477E9802F36C385E8C991F6492566F885(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_IEnumerator_get_Current_m8FE1DCA1D916AC6C8846759C6A879874BDDAD7FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m20622E0E1FDDEE6266F33E1492744F6A8F0045D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_IEnumerable_GetEnumerator_m2FDF32F5C4E66B3920C4557CBFF30D99422DDC2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_add_InternalDataChanged_mAFC2FB0E987F32503DE4675E3B4528E27FD106FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_remove_InternalDataChanged_m35FC5D9C3662DEF17C6A42C74DD2EDDD5298C5E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_add_InternalViewPortionChanged_m4D9F7CFD4AF0FBE12271B85E5B74AB500AA2F32A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_remove_InternalViewPortionChanged_mBEEA538E6EAFA3F041A9B9FCBC7D71D9D8C76770(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_add_InternalRealTimeDataChanged_mDC032C202968EA790C442E614C1F99598C6105B5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_remove_InternalRealTimeDataChanged_mC1AB19A666CDBE6CFBF410696E3BB17BDFD62DB6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t4545532939CE37F9D443BDE5480ACD48837D113F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_t96F1A89A3FF6327A51CB422D228BC0AB7831F4C6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t67BA2B4C4BC9A31BF0B49945FFC3E8DDB8D3E9ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_mPreviewObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_keepOrthoSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_paperEffectText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_vRSpaceText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_vRSpaceScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_maintainLabelSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_U3CIsUnderCanvasU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_U3CCanvasChangedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_ChartGenerated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_get_IsUnderCanvas_m195603F81F69BB55C721E932AE8A27457CE3C89C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_set_IsUnderCanvas_mC1095C52AD717D93A0FF99BB0E5734571ED51BBF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_get_CanvasChanged_mC57ECBC1E94FE78A2BCB30A80D0A44302F8DBC6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_set_CanvasChanged_m12590C6FF3B6795685A5E876D7E9F4CB76D2A834(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_add_ChartGenerated_mA7DB9A7B4BB163D50DECF59625C459C4C2095DA6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_remove_ChartGenerated_m523F495E2E3E114810C67FBD82A5BC5909D7B5A1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * tmp = (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C *)cache->attributes[0];
		AttributeUsageAttribute__ctor_m5114E18826A49A025D48DC71904C430BD590656D(tmp, 256LL, NULL);
		AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline(tmp, true, NULL);
	}
}
static void AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_SimpleView(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_format(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x66\x6F\x72\x6D\x61\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x78\x69\x73\x20\x6C\x61\x62\x65\x6C\x73\x2E\x20\x54\x68\x69\x73\x20\x63\x61\x6E\x20\x62\x65\x20\x65\x69\x74\x68\x65\x72\x20\x61\x20\x6E\x75\x6D\x62\x65\x72\x2C\x20\x74\x69\x6D\x65\x20\x6F\x72\x20\x64\x61\x74\x65\x20\x74\x69\x6D\x65\x2E\x20\x49\x66\x20\x74\x68\x65\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x65\x69\x74\x68\x65\x72\x20\x44\x61\x74\x65\x54\x69\x6D\x65\x20\x6F\x72\x20\x54\x69\x6D\x65\x20\x2C\x20\x75\x73\x65\x72\x20\x43\x68\x61\x72\x74\x44\x61\x74\x65\x55\x74\x69\x6C\x6C\x69\x74\x79\x20\x74\x6F\x20\x63\x6F\x6E\x76\x65\x72\x74\x20\x64\x61\x74\x65\x73\x20\x74\x6F\x20\x64\x6F\x75\x62\x6C\x65\x20\x76\x61\x6C\x75\x65\x73\x20\x74\x68\x61\x74\x20\x63\x61\x6E\x20\x62\x65\x20\x73\x65\x74\x20\x74\x6F\x20\x74\x68\x65\x20\x67\x72\x61\x70\x68"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_depth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x64\x65\x70\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x61\x78\x69\x73\x20\x72\x65\x6C\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E"), NULL);
	}
}
static void AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_mainDivisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x69\x6E\x44\x69\x76\x69\x73\x69\x6F\x6E\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6D\x61\x69\x6E\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x61\x78\x69\x73"), NULL);
	}
}
static void AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_subDivisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x75\x62\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x73\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x6D\x61\x69\x6E\x20\x64\x69\x76\x69\x73\x69\x6F\x6E"), NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x75\x62\x44\x69\x76\x69\x73\x69\x6F\x6E\x73"), NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tD32243AE5CF4061EE858ABD86589073746407D29_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_messure(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x65\x73\x73\x75\x72\x65\x20\x75\x73\x65\x64\x20\x74\x6F\x20\x63\x72\x65\x61\x74\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x73"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_unitsPerDivision(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[2];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x64\x61\x74\x61\x20\x75\x6E\x69\x74\x73\x20\x70\x65\x72\x20\x64\x69\x76\x69\x73\x69\x6F\x6E"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_total(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[2];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x6F\x74\x61\x6C\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x69\x6E\x65\x73"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_material(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x6C\x69\x6E\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[2];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_materialTiling(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[1];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x74\x69\x6C\x69\x6E\x67\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x69\x6E\x65\x73\x2E\x20\x55\x73\x65\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x73\x74\x72\x65\x63\x68\x20\x6F\x72\x20\x74\x69\x6C\x65\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x6C\x69\x6E\x65"), NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[3];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markBackLength(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x65\x6E\x67\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x66\x61\x72\x20\x73\x69\x64\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x69\x6E\x65\x73\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x75\x73\x65\x64\x20\x6F\x6E\x6C\x79\x20\x62\x79\x20\x33\x64\x20\x63\x68\x61\x72\x74\x20\x77\x68\x65\x6E\x20\x4D\x61\x72\x6B\x44\x65\x70\x74\x68\x20\x3E\x30"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[2];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markLength(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[1];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[2];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[4];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x65\x6E\x67\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x69\x6E\x65\x73\x2E"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markDepth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[1];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[2];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markThickness(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[1];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[2];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x68\x69\x63\x6B\x6E\x65\x73\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x69\x6E\x65\x73"), NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[4];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textPrefab(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[2];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[4];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x20\x70\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textPrefix(CustomAttributesCache* cache)
{
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[0];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[2];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[3];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x69\x78\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x78\x69\x73\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[4];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textSuffix(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[1];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[2];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[4];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x73\x75\x66\x66\x69\x78\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x61\x78\x69\x73\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_fractionDigits(CustomAttributesCache* cache)
{
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[0];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 7.0f, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[2];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[4];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x66\x72\x61\x63\x74\x69\x6F\x6E\x20\x64\x69\x67\x69\x74\x73\x20\x69\x6E\x20\x74\x65\x78\x74\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[5];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_fontSize(CustomAttributesCache* cache)
{
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[0];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[2];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[4];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x61\x62\x65\x6C\x20\x66\x6F\x6E\x74\x20\x73\x69\x7A\x65"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_fontSharpness(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x6D\x61\x6B\x65\x73\x20\x74\x68\x65\x20\x6C\x61\x62\x65\x6C\x73\x20\x73\x68\x61\x72\x70\x65\x72\x20\x69\x66\x20\x74\x68\x65\x79\x20\x61\x72\x65\x20\x62\x6C\x75\x72\x72\x79"), NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[2];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[3];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[4];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 3.0f, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[5];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textDepth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[1];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x64\x65\x70\x74\x68\x20\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x61\x62\x6C\x65\x73"), NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textSeperation(CustomAttributesCache* cache)
{
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[0];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[1];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[3];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[4];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x62\x72\x65\x61\x64\x74\x68\x20\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x61\x62\x65\x6C\x73"), NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_alignment(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6C\x69\x67\x6E\x6D\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x64\x69\x76\x69\x73\x69\x6F\x6E\x20\x6C\x61\x62\x6C\x65\x73"), NULL);
	}
	{
		SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 * tmp = (SimpleAttribute_t00CA176DFBDE6CDF8CA98293A3DD8C304A328667 *)cache->attributes[3];
		SimpleAttribute__ctor_mA86B587C764AD7467905991C033795A1932F732D(tmp, NULL);
	}
	{
		CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 * tmp = (CanvasAttribute_t9346E61E9BD0DA1A248B3E6154D55C0BFBF1D069 *)cache->attributes[4];
		CanvasAttribute__ctor_m242F55CCFAF2AE075072E5A163E4B957BF3F0299(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_OnDataUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_OnDataChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_add_OnDataUpdate_mAC71E3C98BDF170D02D0AD5B2696655EC74FFD36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_remove_OnDataUpdate_mB943D54C9978DFF4D3502C4E7977E206BCCE64BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_add_OnDataChanged_m3D8BD87F819D255B2B010F2123A4EEC7F76A1A5B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_remove_OnDataChanged_m2287FF3D6D991ACCB9A6D84E73C352A9ADFD193D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HorizontalAxis_tD419D17477F803DF3DFA77A5A64DF7A265195C8C_CustomAttributesCacheGenerator_HorizontalAxis_U3Cget_AssignU3Eb__1_0_m6189D24F8E7B7616E51A82A16920AA7CAE45E1DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VerticalAxis_t53DBFF8EAF1812932F1C27298A47D58E34559D95_CustomAttributesCacheGenerator_VerticalAxis_U3Cget_AssignU3Eb__1_0_m89177C3B9F17E4E1551B566302417DC94F34F3A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_viewType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_negativeBars(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_stacked(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_transitionTimeBetaFeature(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E\x20\x74\x69\x6D\x65\x20\x66\x6F\x72\x20\x73\x77\x69\x74\x63\x68\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_heightRatio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x74\x6F\x74\x61\x6C\x48\x65\x69\x67\x68\x74"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x68\x65\x69\x67\x68\x74\x20\x72\x61\x74\x69\x6F\x2E\x20\x74\x68\x65\x20\x77\x69\x64\x74\x68\x20\x69\x73\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x64\x20\x62\x79\x20\x74\x68\x65\x20\x70\x72\x6F\x70\x65\x72\x74\x69\x65\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x62\x61\x72\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_totalWidth(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_totalDepth(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CTopPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_TopPosition_mE31D84CAA751B3DF74913ACA6373FF46CEBD4013(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_TopPosition_m41B22FE4648B00A7E795C1F56F6E1C686BFB43E2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_Value_m06CD6D6E61DA6E762AEE2A011CD6B5B104FE331B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_Value_mC913E728B00D39DDCDFBD63E75B9BC2932DE9B03(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_Category_m3E31C561F7C77965215EF838112B706808754EC6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_Category_mF8E4603293F4B61290C0AC030DF8C92605749AD1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_Group_mE173DA1B1DCD09DF12A094D76BAECE8CE0A1FCFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_Group_mB3EA1BA0BFB6DDC8B50F8CC8A8C5038C2ACFA69A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_mCategories(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_mGroups(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_mData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_ProperyUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_automaticMaxValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_maxValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_automaticMinValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_minValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_BarData_add_ProperyUpdated_m3BA9B92B6E412D8239F68AA155C0AC8109B79693(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_BarData_remove_ProperyUpdated_mF53A3D39F62A6B39588F48F64E386BA2A7A29107(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t35C6B43A9008274F11692B7091A19C7768E4CE27_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarInfo_tDDFB4A1E88A5178668512462BDA2920DCBBBBF89_CustomAttributesCacheGenerator_U3CBarObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarInfo_tDDFB4A1E88A5178668512462BDA2920DCBBBBF89_CustomAttributesCacheGenerator_BarInfo_get_BarObject_m7E3CC708841B7C5940E943A7111C56B722F90D5D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarInfo_tDDFB4A1E88A5178668512462BDA2920DCBBBBF89_CustomAttributesCacheGenerator_BarInfo_set_BarObject_m55485BFB5ABF5DE9B7ED25DC70E4DC6B63A4852E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BarMeshGenerator_t4F14218CC54151FCAC8CBB46035CD135D4345C82_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_fitToContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_fitMargin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barPrefab(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x62\x61\x72\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x2E\x20\x6D\x75\x73\x74\x20\x62\x65\x20\x74\x68\x65\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x6F\x6E\x65\x20\x75\x6E\x69\x74\x20\x77\x69\x74\x68\x20\x61\x20\x70\x69\x76\x6F\x74\x20\x61\x74\x20\x74\x68\x65\x20\x6D\x69\x64\x64\x6C\x65\x20\x62\x6F\x74\x74\x6F\x6D"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_axisSeperation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x68\x65\x20\x61\x78\x69\x73\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x62\x61\x72\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barSeperation(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x62\x61\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x67\x72\x6F\x75\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_groupSeperation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x62\x61\x72\x20\x67\x72\x6F\x75\x70\x73"), NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x77\x69\x64\x74\x68\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x62\x61\x72\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barFitType(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barFitAlign(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper(""), NULL);
	}
}
static void CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_CanvasBarChart_InternalGenerateChart_m48A6475265177A377CD7A1590CE29B2E52E7470E(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x72\x65\x73\x68\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_textCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x73\x65\x74\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x73\x70\x65\x63\x69\x66\x69\x63\x20\x63\x61\x6D\x65\x72\x61\x2E\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x74\x65\x78\x74\x20\x69\x73\x20\x72\x65\x6E\x64\x65\x72\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x6D\x61\x69\x6E\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_textIdleDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x69\x73\x20\x61\x74\x20\x69\x74\x27\x73\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_barPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x62\x61\x72\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x2E\x20\x6D\x75\x73\x74\x20\x62\x65\x20\x6F\x6E\x65\x20\x75\x6E\x69\x74\x20\x73\x69\x7A\x65\x20\x61\x6E\x64\x20\x77\x69\x74\x68\x20\x61\x20\x62\x6F\x74\x74\x6F\x6D\x20\x6D\x69\x64\x64\x6C\x65\x20\x70\x69\x76\x6F\x74"), NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_AxisSeperation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x68\x65\x20\x61\x78\x69\x73\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x62\x61\x72\x73"), NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_barSeperation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x62\x61\x72\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x61\x6D\x65\x20\x67\x72\x6F\x75\x70"), NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_groupSeperation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x73\x65\x70\x65\x72\x61\x74\x69\x6F\x6E\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x62\x61\x72\x20\x67\x72\x6F\x75\x70\x73"), NULL);
	}
}
static void WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_barSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x62\x61\x72\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_U3CClipRectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_U3CEnableOptimizationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_get_ClipRect_m474D9C89D9AE388D759504AAB85FBB2E5F6E253C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_set_ClipRect_m8016CC7BCDC55AA769103BAD3800B83F0EBFE926(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_get_EnableOptimization_m202B20B5E9F00E4A1213D5ECCB945268B1593457(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_set_EnableOptimization_mCAC2A3AE52E5A5B3BF4C886BD10BF48960BA65FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_getDotVeritces_m57F8AD391BB4EA247CE286F9B61901B445F0B5C5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_0_0_0_var), NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_getFillVeritces_mEBED888394C7BD35F69B03E2D32028DC50B5D971(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_0_0_0_var), NULL);
	}
}
static void CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_getLineVertices_m3EE775FCB0F13DC4E61DE3FDE73ED596A0977B66(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_0_0_0_var), NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CDegeneratedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP1U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP2U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP3U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP4U3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CFromU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CToU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CDirU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CMagU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CNormalU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Degenerated_mC903C588921C51C16C8A9EDE11AACCF1A4C759B6(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Degenerated_m378A5401A9BAA385E72BC9885DC3B53F1414B451(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P1_mC750288C96B6CE408C61A85DF57B29B463D10C0D(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P1_mC38DFAC465CCB453B3B4AB7E97A447590CC89965(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P2_mB960A6BEF78A9B952DAE1A299D67CFC2012E2A1F(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P2_m160F2F164EF038D1F32214DE28204AE9683D33EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P3_m06A9A176422365C1575296A1F25FD5DEF4D99DDD(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P3_mCAF01F4FED931AA69E7246B0FCBB544D303C7B70(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P4_m8283F3E3C3EFF22F76BDAE1934F53F5175BD2616(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P4_mC6DD8CC9AB985D8FB738267F457D4C34250B9E6D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_From_mE45AE1DDC8A9D9EBF61227F17C4266A2F2DE6E9B(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_From_m592C4AE6BD4846DFCC12721665EE1B58E1D47F04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_To_mD8FAC6076802B34205395DF694BDCE8314F1F2DA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_To_mB09CD554B116268CF79C72323AE421A1C6C0B1F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Dir_mCD8563E867388D16CF2A859711F1868D2A1E77D0(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Dir_m8099FE4EEA13DB6F851843C3258BC6931DE82B85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Mag_mFBC4B05934E2168B53EA8A64864446F3CE366AF5(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Mag_m8DF404321EC5B783086BAAB927F520F0B4FDE1EB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Normal_m8F500F8704DEEF72A8F69A20608C2ACEE316146A(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Normal_mD0FB285540CA993D6648EA0A2ECED90968AEF099(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0FC208679FC9CA006469A3768BA5B48CAE792C4A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54__ctor_m4DB72EA868575928A2B878DB003FC69EC843CB41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_IDisposable_Dispose_mD23F1E6AA65C6A057B3CB754453E5EF805A6D1C4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mDE5152173FE2DDBBBEFBA74FF5D1003DB57329A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_Reset_mB94C31961CCC9ED9E0C43BDA9EDAB2C8AEC8168E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_get_Current_m5EDAAE023B18488FB09268F34195242C38E00398(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mDD7964939474EC93D2FE78BE7CC49AFBA792B7FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerable_GetEnumerator_mDD309DD0E06A30244A07398B3954C6D027892939(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56__ctor_m84E7F14E607A4946C8501AB381A61E9073030C41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_IDisposable_Dispose_m24807AAE5C8A054491097465175CFE35CD1DEF66(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mD6B1F95D7EB23006D2F8D4AB387FDD8DBB18F306(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_Reset_mB82B5598EA4CE0F46906571B4E8D9DA77BC3B6FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_get_Current_m8EA14AB9919BD3DD944A510B246A8C9AD1E044BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD742CD8DCFAD0905516F7BD5DC1F0EA2BC14A306(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerable_GetEnumerator_m16CFCBA5318010625AA9C47463B1FDECB73EF4FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57__ctor_mEE477D5EAAE9411DFD0492352D6FB42078AC1569(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_IDisposable_Dispose_mC8072E3A4ABA4613032B8D7E71DB91558C416482(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m32AE92CAB16E5053DB5BBCC59C1685CCFF523DE8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_Reset_mEFF718EC377A4A2E4429F5E1E750125C21D55DB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_get_Current_m6323FEDAA1579D420AE2EA6FB069041B77EEE869(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m314F4C290F52F681EB509E6551875D62C947F5F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerable_GetEnumerator_m8D920BB8D684EEE193B70580501FDEF370F80984(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CanvasLinesHover_t1537D8594C6ECF102D6A5EC34FFD2EE7BE096863_CustomAttributesCacheGenerator_CanvasLinesHover_getVerices_m7234E00BE8E15C9BEEC7F63445BE89EB20DB7229(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_0_0_0_var), NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3__ctor_mFB4CB89B833F447622410D7F7A7CCAD4C98D70BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_IDisposable_Dispose_m746EFF4FE9C1FFE4D6A7070FB0CA043639F68F45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mF499DCABE17E18C34CA602E520367789C8BB8AD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_IEnumerator_Reset_m5F6B721252ABAB165C6649E7E398EE50EDE376B7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_IEnumerator_get_Current_m17C9A5DE1F6758BF4F866CC0DD45A53E9E6D33D1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD5851E834D32366D321671E68BFCB0CA31D1DE41(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m76999D741D77F8EB58EFC799D0DBF597C38D6CFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_U3CrefrenceIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_Hover(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_Click(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_Leave(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_get_refrenceIndex_m74CA55213DF53D00162E676956F11D1311D84DB3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_set_refrenceIndex_mA31A5548EE2B4B2D2A76FBB107933ADF89B0EA09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_add_Hover_m975682AE4DF3A55C09E90918447520BC78FB4C84(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_remove_Hover_m83C03EF5E1E66A44170A92CFD02162EDA9F07CE7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_add_Click_m158BAA2833986B66A6D82E45FC0D81BD82F65289(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_remove_Click_m46285AF60CA8F9CC618C99DD50E77535486D011B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_add_Leave_m3CEF09D689312D3288058607971942DA3A7E280E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_remove_Leave_mA0C44F388334C72624433637A97EE482B1A7D331(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartAdancedSettings_tF4F7D43A199A95A32F786E7DF921165A929DD663_CustomAttributesCacheGenerator_ValueFractionDigits(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 7.0f, NULL);
	}
}
static void ChartAdancedSettings_tF4F7D43A199A95A32F786E7DF921165A929DD663_CustomAttributesCacheGenerator_AxisFractionDigits(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 7.0f, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_U3CDefaultIntComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_U3CDefaultDoubleComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_U3CDefaultDoubleVector3ComparerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_get_DefaultIntComparer_mAA3284825DD7E0A551A6AC72786E3AF5704CC917(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_set_DefaultIntComparer_m6C8736F71A022D13C54B0CAE41C075375F0804AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_get_DefaultDoubleComparer_m207B0BBC5438E7E4DFA79767A7ADE3057B2F4C29(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_set_DefaultDoubleComparer_m9E82EF5C50F5AEDDAF7CD4D6ABAAE35FDC6E5A7E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_get_DefaultDoubleVector3Comparer_m3ACF2569E62983F10A1E0C4D60011A10D9997BBA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_set_DefaultDoubleVector3Comparer_m5E898325C6939EF7226EEFD7FAD89B3FDF7BA915(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseScrollableCategoryData_t2A653271124CC36DB3EEEE83466D72BCE54A167C_CustomAttributesCacheGenerator_ViewOrder(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_U3CDurationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_U3CStartTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_get_Duration_m5771749A0F1AFACA2997F1053A4502EC124BF094(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_set_Duration_m7CE1BAD2AADB23CD5710DDAECAFD59B478952C3D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_get_StartTime_mABAD31F5ABFF68C526320A9CA287BC29CDC73BAE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_set_StartTime_m837A80AE1654C5A03634E81241483A31285604CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_DataChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ViewPortionChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_RealtimeDataChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticVerticalViewGap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticVerticallView(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticcHorizontaViewGap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticHorizontalView(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_horizontalViewSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_horizontalViewOrigin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_verticalViewSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_verticalViewOrigin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_add_DataChanged_m53454856861790BB166459C847049FB880A090C9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_remove_DataChanged_m7BA12D21F694D295693C39AE35D82029698C03F2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_add_ViewPortionChanged_mC7F6282B4113B254E79F20BFDEA4E2E735A8AC05(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_remove_ViewPortionChanged_m4F1A28D51F140E9964EAFC4E67761798659D59B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_add_RealtimeDataChanged_m0982CDB8E7798B3E606CA10F73634E4868CB64D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_remove_RealtimeDataChanged_mDE9BB5466ABAA2D883C6E9726976621D6914CEAB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_U3CUpdateU3Eb__47_0_mE8BFC1F2E09B15FAC7A0D7B71CD96F8B7C2105CE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_U3CWorkOnParentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_U3CInitialScaleU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_get_WorkOnParent_m903EB04E0D1C9340CF5714BBC942750CF46CAB3F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_set_WorkOnParent_mB59EB8AF35F52B149CDB79C484084DC7DD101D89(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_get_InitialScale_m407352A2708523959EB60767979C30437DE9B6B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_set_InitialScale_m90D615D3B6E717032E683EEA0E1A9D7B27B8CD36(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_U3CItemIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_U3CItemTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_U3CItemDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_Deactivate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_get_ItemIndex_m4340775567B35B857FCA0AC7F9E145B82BCED8CA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_set_ItemIndex_m2690B655651FB9CC9E70E2966F476CCE9B072314(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_get_ItemType_m043287B6B47E2BC931B19B9F70D9F40B3EB08A0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_set_ItemType_m99ACEB65F5F84DDE284947F6A251D9E3DD8EFD91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_get_ItemData_m325E440723EC2C55FA58947F2D540423860308BA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_set_ItemData_mB74438B594B425E6943D0A8E06078CBBC7A24227(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_add_Deactivate_mCED293771E82E147D0842EAF25B06F06810864B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_remove_Deactivate_m3A9E4240833B71B8DBB7D2F4F51654FD1DCEFA2B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemEvents_tA5C1E2A4E427C036700A599F45FB70160E07D70B_CustomAttributesCacheGenerator_OnMouseHover(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x63\x63\x75\x72\x65\x73\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x6D\x6F\x75\x73\x65\x20\x69\x73\x20\x6F\x76\x65\x72\x20\x74\x68\x65\x20\x69\x74\x65\x6D"), NULL);
	}
}
static void ChartItemEvents_tA5C1E2A4E427C036700A599F45FB70160E07D70B_CustomAttributesCacheGenerator_OnMouseLeave(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x63\x63\x75\x72\x73\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x6D\x6F\x75\x73\x65\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x6F\x76\x65\x72\x20\x74\x68\x65\x20\x69\x74\x65\x6D"), NULL);
	}
}
static void ChartItemEvents_tA5C1E2A4E427C036700A599F45FB70160E07D70B_CustomAttributesCacheGenerator_OnSelected(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x63\x63\x75\x72\x73\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x63\x6C\x69\x63\x6B\x73\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x69\x74\x65\x6D"), NULL);
	}
}
static void ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_left(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_top(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_right(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_bottom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartMaterialController_t46529A1D1C468A9BF1C2EAA4A6A1BB6869DB87F8_CustomAttributesCacheGenerator_materials(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_CustomAttributesCacheGenerator_U3CTagDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_CustomAttributesCacheGenerator_ChartItem_get_TagData_mB309D7BE4D449337386A3F11CC5B1E5461622DA9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_CustomAttributesCacheGenerator_ChartItem_set_TagData_m6EFCAD89C8BFBC7370A001CCDD1446856CEF193F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DoubleVector3_t067DE045154E14BD0F85E0054C51AFC9000A776C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void DoubleVector3_t067DE045154E14BD0F85E0054C51AFC9000A776C_CustomAttributesCacheGenerator_DoubleVector3_t067DE045154E14BD0F85E0054C51AFC9000A776C____fwd_PropertyInfo(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_mAC32A5CCD287DA84CDA9F08282C1C8B0DB7B9868(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x44\x6F\x75\x62\x6C\x65\x56\x65\x63\x74\x6F\x72\x33\x2E\x66\x6F\x72\x77\x61\x72\x64\x20\x69\x6E\x73\x74\x65\x61\x64\x2E"), NULL);
	}
}
static void CustomChartPointer_t4EC5C8C22F34C8DA585F64F6D8DA28C2E25ACFC5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var), NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_NameChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_OrderChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ItemRemoved(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ItemsReplaced(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_NameChanged_mB4F42D35E494EA59BB90B4BF2EF48BC82ED63245(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_NameChanged_m842004605E5DF0DC56F89AB01FF165E7BE11DE2A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_OrderChanged_m308ED8FBFCF9A66C69C96B7DFABC23139D46CB2F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_OrderChanged_mD9B0980B75467DACF6345834DDE9FCC09ADD429D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_ItemRemoved_m6F8743B115A784B99758A6DE9BA8A442E31FD17A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_ItemRemoved_mAD48997C50B4A3C066869F3DC559BC9D6AA1C581(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_ItemsReplaced_mF87097C01142F2BFEA12285AC97855EBEFC6423D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_ItemsReplaced_m6AEB3E00A48EE2B0FBDBC2C598782CAF888303EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_U3CRowU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_U3CColumnU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_get_Row_m8E5B2536ABA3F7963BED8C00C37AD9DE246E4182(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_set_Row_mA0B59C94D055895B004E338AD361C4228FC37AD0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_get_Column_m414AFFA313E39B3AEBAAC5B8DEA3A2E483F69999(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_set_Column_mFD48761D666FCDB501D3C5C79F3FCA47DEDF6D8C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_fitToContainer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_negativeFill(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_fitMargin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_GraphChart_U3COnItemLeaveU3Eb__58_0_mA557CB16D6A2BA64937454DEDF3ADD5FECDB6FFF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass48_0_t6495EB11621B27869AA20C874F3155A901B050E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tE0014C34268BBB80C3B444BA615F0A558812081F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_heightRatio(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x68\x65\x69\x67\x68\x74\x20\x72\x61\x74\x69\x6F\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_widthRatio(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x77\x69\x64\x74\x68\x20\x72\x61\x74\x69\x6F\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_itemFormat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA____HeightRatio_PropertyInfo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CMagnitudeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CXStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CYStringU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Magnitude_m069084EB44CF3477283C3C24DFE227E060139C67(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Magnitude_mE0A9AC6DF6CF9D6532021ED8F40958FE33612C27(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Index_m6E3E1833F3106A2C697659849B4D4DBB3EA3A834(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Index_m779BF98A0027FBF3AE214D4225424BE1465B3330(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_XString_mD8983AC95262E528C933388971991F0ED7FBDB04(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_XString_mBE4A7E79289C442618EFF15D4863A9A764735C27(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_YString_mD55762099B346C4F5F0DB6C742BFE5659DB94074(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_YString_m1E0A09FE20ABD9A34C0361EFF8E294D558475401(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Position_m07FE6D33F9802F166E94A69452252688832F68FE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Position_m1E11A04C49BB3B49F43F5CC58A11A64234E47E73(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Value_mE3ED67FD7BE31E9EEA4FA9F1C24B3AAEDB363055(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Value_m2AEA5B671D29625A88182451F4D8DE567FD9353D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Category_m132FB8CA4D23BBD5A6597E4C180168227A203D4A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Category_m7F525C2F77FBD1DF02655BB29922637BC97CC37D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Group_mEDE877E6A0267C6C014F063601F3A95873209B3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Group_mAF49E4F10D93415BBDB3437597F4A6C03D7FD22A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GraphData_t376FC9F33A5BD4455A70B789EDF9018C2E669D9E_CustomAttributesCacheGenerator_mSerializedData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_Depth(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_data(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MaxX(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MaxY(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MinX(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MinY(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MaxRadius(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_LinePrefab(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_FillPrefab(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_DotPrefab(CustomAttributesCache* cache)
{
	{
		NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F * tmp = (NonCanvasAttribute_tDA4990D5ED27F68AF0E35D0DDE57143E3070461F *)cache->attributes[0];
		NonCanvasAttribute__ctor_m920E0468FFD44321C45ACC594E58D1C0AC7102A6(tmp, NULL);
	}
}
static void SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_AllowNonFunctionsBeta(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_t03FE4757760FC33E9FB20BE671C6D6E8AD9C281E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass46_0_t7A3D2754706A033FA9D18D92BB265665601844BE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass49_0_tAACC950DBCD06E86BA2BC4C265FDCAA8E3630C20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldSpaceGraphChart_t689AAB22CFEB7473627C3822123DA6CCF837080A_CustomAttributesCacheGenerator_textCamera(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x73\x65\x74\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x73\x70\x65\x63\x69\x66\x69\x63\x20\x63\x61\x6D\x65\x72\x61\x2E\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x74\x65\x78\x74\x20\x69\x73\x20\x72\x65\x6E\x64\x65\x72\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x6D\x61\x69\x6E\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldSpaceGraphChart_t689AAB22CFEB7473627C3822123DA6CCF837080A_CustomAttributesCacheGenerator_textIdleDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x69\x73\x20\x61\x74\x20\x69\x74\x27\x73\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass29_0_t58C140E2EF87917B83720F17F1557268E454B092_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t2186A3E8A6CDAB964B8652EBEE5E117410C93556_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalUse_t29E20E2E4270444070C32C401C3EA43A86F6B265_CustomAttributesCacheGenerator_IInternalUse_add_Generated_mE5D0A14C93F88D9A3FAFD05854322EA5A8A0CCFF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalUse_t29E20E2E4270444070C32C401C3EA43A86F6B265_CustomAttributesCacheGenerator_IInternalUse_remove_Generated_m90F87610019DD8F9FFFA7956FFCB046A4B38A58E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_add_InternalDataChanged_m1E053110E2F3B76E0A0A0AEBA060FCDA8E7CFF06(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_remove_InternalDataChanged_mECE52BC3CCC5486916167DDA3780BFA627EFA181(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_add_InternalViewPortionChanged_mA2DE5EE83DA759D5C4194DC326562ABCAF234E0C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_remove_InternalViewPortionChanged_m57887986084687152FA36944627A68011D328BEF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_add_InternalRealTimeDataChanged_m3527EBCFA81EFB7A767DEC20EBE3A61BB8952339(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_remove_InternalRealTimeDataChanged_m8A079A6E42ED8503D7685C641100DA7881B88776(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_add_InternalOnDataUpdate_m996E2E19D178559A5856C7496EFFEE5D138C1398(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_remove_InternalOnDataUpdate_m55F4229E72756C6B95BE545FCAE67A90BDED728A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_add_InternalOnDataChanged_mD75E3ADF66DEC948897A2D8A46E9B69C06F0A977(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_remove_InternalOnDataChanged_m698D03A6235E342E4BC2A4E1955AA98B470639C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3COrientationU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3COffsetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3CRecycleTextU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_Orientation_mD1C2C815A175E2BBEA3117ED8EBE1A7932640E87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_Orientation_m87F834D65E5BED545ED69FE1C608E093643A02FE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_Offset_m44B3F4D7B3492223D8C915497E194765BBCA2F02(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_Offset_m0A620034DF859A689C768EEC52815E5F1DEB2B00(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_Length_mF24BFEEE513BE43BF890161B869DAF8E5CA23FC6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_Length_m05073A1B3BEFECD9F86DF14BFB79695824840418(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_RecycleText_mBF81839FE404AF6776B99355A86373744038D025(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_RecycleText_mCFE3B45903FC5BABBCF566195FCED69F637A413B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_0_0_0_var), NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_OnDataUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_OnDataChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_add_OnDataUpdate_mDD4FC4303A073F4D8704C248857730055FAEF249(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_remove_OnDataUpdate_mA808D637C17F5F6E1ABD363FD47CBB39D899D061(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_add_OnDataChanged_mEFABE67C67B63454BF30E194741B5BA2B1F5D2AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_remove_OnDataChanged_m16893529ED8A5D7F8FE580F3107DD651AEEA77DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoxPathGenerator_t89EA4BE0C76203D58A311CFF61B24836F4E86104_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
}
static void BoxPathGenerator_t89EA4BE0C76203D58A311CFF61B24836F4E86104_CustomAttributesCacheGenerator_HeightRatio(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void CustomPathGenerator_t5E45C54F930DF70A03D0AEE692C2ABBB42DF3097_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
}
static void CylinderPathGenerator_t3D207A37699975810819C6484F1164998AB60BA2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
}
static void CylinderPathGenerator_t3D207A37699975810819C6484F1164998AB60BA2_CustomAttributesCacheGenerator_HeightRatio(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.00999999978f, 10.0f, NULL);
	}
}
static void LineRendererPathGenerator_t6189884C6B597C902595D210EEEAF36FF881AB42_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(LineRenderer_t237E878F3E77C127A540DE7AC4681B3706727967_0_0_0_var), NULL);
	}
}
static void CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072_0_0_0_var), NULL);
	}
}
static void CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_prefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x70\x72\x65\x66\x61\x62\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x70\x69\x65\x20\x69\x74\x65\x6D\x2E\x20\x6D\x75\x73\x74\x20\x63\x6F\x6E\x74\x61\x69\x6E\x20\x61\x20\x50\x69\x65\x43\x61\x6E\x76\x61\x73\x47\x65\x6E\x65\x72\x61\x74\x6F\x72\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74"), NULL);
	}
}
static void CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_lineThickness(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x74\x68\x65\x20\x74\x68\x69\x63\x6B\x6E\x65\x73\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x67\x75\x69\x64\x65\x6C\x69\x6E\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x65\x61\x63\x68\x20\x73\x6C\x69\x63\x65\x20\x61\x6E\x64\x20\x69\x74\x27\x73\x20\x6C\x61\x62\x65\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_lineSpacing(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x69\x6E\x65\x20\x73\x70\x61\x63\x69\x6E\x67\x20\x66\x6F\x72\x20\x65\x61\x63\x68\x20\x63\x61\x74\x65\x67\x6F\x72\x79\x20\x6C\x61\x62\x65\x6C\x20\x6C\x69\x6E\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_lineMaterial(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x69\x6E\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x20\x66\x6F\x72\x20\x65\x61\x63\x68\x20\x63\x61\x74\x65\x67\x6F\x72\x79\x20\x6C\x61\x62\x65\x6C\x20\x6C\x69\x6E\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_meshSegements(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x6D\x65\x73\x68\x20\x73\x65\x67\x65\x6D\x65\x6E\x74\x73\x20\x69\x6E\x20\x65\x61\x63\x68\x20\x70\x69\x65\x20\x73\x6C\x69\x63\x65"), NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_startAngle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x74\x61\x72\x74\x20\x61\x6E\x67\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_angleSpan(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6E\x67\x6C\x65\x20\x73\x70\x61\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 360.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_spacingAngle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 360.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x70\x61\x63\x69\x6E\x67\x20\x61\x6E\x67\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6F\x75\x74\x65\x72\x20\x72\x61\x64\x69\x75\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_torusRadius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x69\x6E\x6E\x65\x72\x20\x72\x61\x64\x69\x75\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x63\x68\x61\x72\x74"), NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_extrusion(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x65\x78\x74\x72\x75\x73\x69\x6F\x6E\x20\x6F\x66\x20\x65\x61\x63\x68\x20\x70\x69\x65\x20\x73\x6C\x69\x63\x65"), NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_clockWise(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x64\x72\x61\x77\x20\x74\x68\x65\x20\x70\x69\x65\x20\x69\x6E\x20\x61\x20\x63\x6C\x6F\x63\x6B\x77\x69\x73\x65\x20\x6F\x72\x64\x65\x72\x20"), NULL);
	}
}
static void PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_get_Value_m34E34DB40147B026905D90FF0579DF887371A9FF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_set_Value_m57932DB099DD42DDEE99D1F87CE10DB0B6C128C3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_get_Category_m4104152AEE2A00AF85E1E806EDE4936580E24920(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_set_Category_m1D0F3E34466A4EA1B70186116AF07992FC8D6206(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_mCategories(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_mGroups(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_mData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_ProperyUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_PieData_add_ProperyUpdated_m4960F7F4225FF6396F630FDDF953FC61F1E7C15B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_PieData_remove_ProperyUpdated_m81A424A95A8D71D68AD29F6C4B7EE4103DFD65ED(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryData_t4945CE6065C155C06D9AD09BDBD73DB7F47A53D1_CustomAttributesCacheGenerator_RadiusScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CategoryData_t4945CE6065C155C06D9AD09BDBD73DB7F47A53D1_CustomAttributesCacheGenerator_DepthScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void CategoryData_t4945CE6065C155C06D9AD09BDBD73DB7F47A53D1_CustomAttributesCacheGenerator_DepthOffset(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void U3CU3Ec_t8B2732CBF7743AAA1586354DB2C59E86032D5D19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieInfo_tDC5F07B5F293ECBB720A8460A8992534FF89EB6B_CustomAttributesCacheGenerator_U3CpieObjectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieInfo_tDC5F07B5F293ECBB720A8460A8992534FF89EB6B_CustomAttributesCacheGenerator_PieInfo_get_pieObject_mDBF95EBF9EF48A131C00E13C4BBC824636127571(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieInfo_tDC5F07B5F293ECBB720A8460A8992534FF89EB6B_CustomAttributesCacheGenerator_PieInfo_set_pieObject_m94FFB57B6A64D14E3C5031D72DA5791AC030CC3F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_textCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x20\x69\x73\x20\x73\x65\x74\x20\x61\x6C\x6C\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x68\x61\x72\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x64\x20\x74\x6F\x20\x74\x68\x69\x73\x20\x73\x70\x65\x63\x69\x66\x69\x63\x20\x63\x61\x6D\x65\x72\x61\x2E\x20\x6F\x74\x68\x65\x72\x77\x69\x73\x65\x20\x74\x65\x78\x74\x20\x69\x73\x20\x72\x65\x6E\x64\x65\x72\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x6D\x61\x69\x6E\x20\x63\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_textIdleDistance(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x63\x61\x6D\x65\x72\x61\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x74\x65\x78\x74\x20\x69\x73\x20\x61\x74\x20\x69\x74\x27\x73\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_innerDepth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x69\x6E\x6E\x65\x72\x20\x64\x65\x70\x74\x68\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x73\x6C\x69\x63\x65\x73"), NULL);
	}
}
static void WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_outerDepth(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x6F\x75\x74\x65\x72\x20\x64\x65\x70\x74\x68\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x73\x6C\x69\x63\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x64\x65\x70\x74\x68"), NULL);
	}
}
static void WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_prefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x70\x69\x65\x20\x70\x72\x65\x66\x61\x62\x20\x6F\x66\x20\x74\x68\x65\x20\x70\x69\x65\x20\x73\x6C\x69\x63\x65\x73"), NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t0CF425C31C7E5B9B92AFD96A8259C3F26E9BF319_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalRadarData_t279BEC1E138A7A0F7B6C2CC18EFB770AF0D2D397_CustomAttributesCacheGenerator_IInternalRadarData_add_InternalDataChanged_mD1A3FC03541A392F90454DDB6D3C504179530175(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IInternalRadarData_t279BEC1E138A7A0F7B6C2CC18EFB770AF0D2D397_CustomAttributesCacheGenerator_IInternalRadarData_remove_InternalDataChanged_m027EF2D7022C372AC441CE50C785A093419D0921(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_radius(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_angle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisPointMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisLineMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisThickness(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisPointSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisAdd(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_totalAxisDevisions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_Data(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_RadarChart_U3COnItemLeaveU3Eb__77_0_mCD5E6245CDA4737B40897F15288E0A869E9F23B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Index_m62007451B3AA6C66F095A4C35B8B7B8EC5E484A9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Index_mA4261CF977A216CAF9751F99FC765CDAA82F8A59(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Category_m9311857293FCD862890CAEC7153DFFF54AC0A15A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Category_m9A98ADC9A460C6B28C355DFF54D344DEB0BCFBAA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Group_m0006840DB7F009CC5BE3E17EEEBD03B79AB7526D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Group_mE1D6C61197C2FB44AC097B9F052595AA93D9D60E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Value_mB6E37324C906F817C5B2E4755A1CA24293C5DB4D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Value_mA19E1B1C45A5B260E50606F70636DB311AF02501(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Position_mB4AC59E9FCD59B2EE4E4F20F81778D7DFA6DD9BD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Position_mD64D46B5CEB08F082E4BC6D2B53A714228152CB9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_mCategories(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_mGroups(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_mData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_DataChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_ProperyUpdated(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_automaticMaxValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_maxValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_automaticMinValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_minValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_add_DataChanged_m99D8EA4C37777EEE904B2472F067B27701E3DE25(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_remove_DataChanged_m1E3D1F74E6E7B12E51D57747E9E72B85111CD90B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_add_ProperyUpdated_m94ED02CCC412FD825345AE1C6D9D000D3EC14005(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_remove_ProperyUpdated_mCFABC49D427FE3F42604FE13456B70DFC4B01304(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CategoryData_t03BB589B3636224DA50177D9BEA0C882C1632F92_CustomAttributesCacheGenerator_MaxValue(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_tC6775FBF74F4CAFBF224B2EFFDCB89F15F1357CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RadarFill_tE5CCC550AF849BD6352A5C1CF1959A7FCEDBAD7C_CustomAttributesCacheGenerator_RadarFill_getVerices_m93B92F5B9E70EC783CF194DC2AAAD21F7DC12707(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_0_0_0_var), NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11__ctor_m95EAA0E125642D785F6170A26A63A985795114EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_IDisposable_Dispose_mA707AEC25B6F42499A916D5625F27CC57C387E70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m0ABBFDCEE2D7145EBE071FCFE6684F6BAFEC3AF9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_IEnumerator_Reset_mE28A7F9BD47AA43584DAFA502888ACF0235FB4F3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_IEnumerator_get_Current_m53326F4FE9B852D2151D3849221DE6279144AAFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mFE0741653FF30F25813799307B8423F0B24C7C53(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_IEnumerable_GetEnumerator_mC4C94D6FA75874BC56F1392AE45985B0A0F0F00E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RadarFillGenerator_t9B039EF2C832742FAEA0550A0C4BB5F9E3994CAB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
}
static void RadarFillGenerator_t9B039EF2C832742FAEA0550A0C4BB5F9E3994CAB_CustomAttributesCacheGenerator_RadarFillGenerator_getVerices_mF7C8A6EBFC00C77EDB871799777143548EC570EF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_0_0_0_var), NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9__ctor_mAC242DA1B337EB73A1ACF54B5008C8982895E7BE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_IDisposable_Dispose_mA5B4335205957942C2C19D8CF215EE867ADE91DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m26748669262E86E2C5B4B00464074D5101846F44(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_IEnumerator_Reset_m2806ECA59982D7E816ABAEE95594A91198113387(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_IEnumerator_get_Current_mEC742CBC4BFF915F4437AFE99135CE5D95A38D2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m18B10F0B05365FD3906E1B6F7909DDAB07663928(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_IEnumerable_GetEnumerator_mE68D27E149F9F1E558455CA8B7C987FB5485CD25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WorldSpaceRadarChart_t3FEE5BCF4312293C38569712DF5D7F6C78AD3E18_CustomAttributesCacheGenerator_axisPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldSpaceRadarChart_t3FEE5BCF4312293C38569712DF5D7F6C78AD3E18_CustomAttributesCacheGenerator_axisPointPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_autoScrollHorizontally(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_scrollable(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_horizontalScrolling(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_autoScrollVertically(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_verticalScrolling(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_raycastTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_horizontalPanning(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_verticalPanning(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AlignedItemLabels_t916ED3D73B7285478225379BAD5447D11A7E3A65_CustomAttributesCacheGenerator_alignment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x20\x74\x68\x65\x20\x61\x6C\x69\x67\x6E\x6D\x65\x6E\x74\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x61\x62\x65\x6C\x20\x72\x65\x6C\x61\x74\x69\x76\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x69\x74\x65\x6D"), NULL);
	}
}
static void ItemLabels_tBA5545E0FFC4D9B10BF17C46057E810D4DD406D9_CustomAttributesCacheGenerator_fractionDigits(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 7.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemLabels_tBA5545E0FFC4D9B10BF17C46057E810D4DD406D9_CustomAttributesCacheGenerator_ItemLabels_U3Cget_AssignU3Eb__5_0_m961E93586578FB887F59CD9628AEC9CE40E8C929(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextDirection_tEC3547541EBAD4AF021131F7F034225013DBCACA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_prefix(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_suffix(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_customFormat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_OnDataUpdate(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_OnDataChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_add_OnDataUpdate_m2F65466175314C9D5ED7FE225FE1BD80ADBEDBBB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_remove_OnDataUpdate_mA5F909354E837998B946D9345C788F83D4CDF9B9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_add_OnDataChanged_m8C1F23C5AD0504F224A32812FA950812A484F1AA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_remove_OnDataChanged_m1FCA55929D4EFD9908E2D572D8240F8069F0497F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PieAnimation_tC5CB4F1D58BA7F7C0FF7517744BFD4B52022FEEC_CustomAttributesCacheGenerator_AnimateSpan(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 360.0f, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t62E4DF5127AC3B2B3C0A055B7F85C3C16E82700A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_fontSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_CategoryImages(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_legendItemPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_chart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_U3CUserDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_U3CMaterialU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_NameChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_get_UserData_m7366DD757A7150DDD8A9BED5BD84833CF439A79E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_set_UserData_m68BB91284256A958A1965AD38A7F65970958CFCA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_get_Material_m884D84994D5DFB145F7F80D646ED047675946B82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_set_Material_m556B6A8463CE116F5C41CB9A7C82CD866F333A43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_add_NameChanged_mAB59E2935A5636D41ACCABCA450D3AC205B6BCDE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_remove_NameChanged_m16AFE4B4FF6A7A5DB09EE7E5FAAF7AE835A3E030(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_DataStructureChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ItemsReplaced(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_DataValueChanged(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_add_DataStructureChanged_m3DFE5025CFA79F2777ACF301467AEE3227CFCD43(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_remove_DataStructureChanged_m8AAA481D8DF9741946B75D75912507CD4D14179D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_add_ItemsReplaced_mFA7A223CBFD7512E5E929AE0E0A717DE475AFFBA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_remove_ItemsReplaced_m8102FBC9B601DCBA582A2DD9B1B9F41D9F21B9FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_add_DataValueChanged_m46AFF31FDDA9040F4A0E8FEDBB8AA5F618F72F6B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_remove_DataValueChanged_m2B90A93D01B42AB10A91FA075051E2B995B982DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3CItemIndexU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3COldValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3CNewValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3CMinMaxChangedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_ItemIndex_m567F892ACB8F7484334FE772FAD6772D47BA3168(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_ItemIndex_m3F4E84261B9A7E33423D30D574AE5CA803D1C070(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_OldValue_m0982C5004334F1A50D175D9FE9164DA2A80F9C45(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_OldValue_m3017E9DAA2BC8F3144A12C5623B7EEABC4F0C32B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_NewValue_m58B94C77C099C4EE56C72968A6FAD8A45F9A9F5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_NewValue_m9560AAB634ECBA050B2023D8CF4F96D05E99ED1B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_MinMaxChanged_m52927FBE8B2DCCE7C6C5F22AA9559C6EDFE56FDC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_MinMaxChanged_mE75097F31ECA36DF1DED4BD67CF3BEF9CA8E1541(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IDataItem_t69912D58F7030CB9482018BD1CEC874F026CCD63_CustomAttributesCacheGenerator_IDataItem_add_NameChanged_mD20156002607BC7C7BA54091EB74139BE756EB6C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void IDataItem_t69912D58F7030CB9482018BD1CEC874F026CCD63_CustomAttributesCacheGenerator_IDataItem_remove_NameChanged_m4D661067FCC994B776B8EDDB0A029C63A06B235B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_get_Group_mBBD268E7B2942B2B9F44E76CF11463EB2247B92B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_set_Group_m1FD2ECC0C5E3DE8D62E22CF59AB13BB2C63E940B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_get_Category_m65B6C653CB412F5D8B8A8E8E34759FE50B81C3A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_set_Category_mF159D3DEB39D5C73D7ECE7448170A56B9488F5E4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AxisGenerator_t50787EE278F4D2CD096BEB2AF8DC38760DD71A46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
}
static void CanvasAxisGenerator_t40D28746D976A0B2D0BF7384C6367BD902CFDBF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E_0_0_0_var), NULL);
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[1];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharpU2Dfirstpass_AttributeGenerators[840] = 
{
	BarContentFitter_t2CB20F0FDAC91A858D0AC80D75F3D4918CA10D8B_CustomAttributesCacheGenerator,
	U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator,
	U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator,
	U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator,
	U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator,
	TextController_t2B01B36AAF87E0AB554C457EE44056351C9C79E9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_tA3CCC09942E2F3195532FC009C891A4B9A1DA601_CustomAttributesCacheGenerator,
	U3CU3Ec_tC0C7D4916053334B4AEAA3A02788811EF98B4869_CustomAttributesCacheGenerator,
	U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator,
	U3CU3Ec_t4123600E95DCFA38B3656F4920820A7285BB739C_CustomAttributesCacheGenerator,
	U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator,
	U3CU3Ec_t1511C530E9C57D2EAA6D009320443957E751F8A6_CustomAttributesCacheGenerator,
	U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator,
	U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator,
	GraphRectZoom_tAD8A839F845F838855A9B6958FB463E4DE777CE0_CustomAttributesCacheGenerator,
	GraphZoom_t942615FE8FBC67CB3B822C9F2700F217F3A6B829_CustomAttributesCacheGenerator,
	CanvasSampleOpenFileImage_t16B5870D57BD12D55FA713BA93628EEF53FFEE37_CustomAttributesCacheGenerator,
	U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator,
	CanvasSampleOpenFileText_tBD2B2C8E2DAA1044307221A4BC638C0F6250A0AA_CustomAttributesCacheGenerator,
	U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator,
	CanvasSampleOpenFileTextMultiple_t6A74601385701DA6AE2BD30AEEA0217D8987F387_CustomAttributesCacheGenerator,
	U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator,
	CanvasSampleSaveFileImage_t99EC606B45EAD318F89C4C5FD3949DE66A196755_CustomAttributesCacheGenerator,
	CanvasSampleSaveFileText_tD184142E7CE1AE4783312141F0AD832603D4F388_CustomAttributesCacheGenerator,
	JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator,
	JSONArray_t04D56DA332EFB150BB42B2CC5764F7C4A41D92A5_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator,
	JSONObject_tAC0F80D5CCC63632CF36C37AE4112FCBE6AE196E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t97AC8537B1356ECB017CDF0A3BD89ECF81CB65DE_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator,
	JSONLazyCreator_t5F13AFBDD4F4BD950659E92C30BCE03BA8C552AA_CustomAttributesCacheGenerator,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator,
	U3CU3Ec_t987457C1F23A8F6B930C9FD8728008D8876B1729_CustomAttributesCacheGenerator,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator,
	U3CU3Ec_t17FF798A47EB32301A9CAF01F84D5C8D3307B57A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_tD66B40B5A861605561DE2139F8C5365DC3D1F506_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_tDEF17818921A9F591C8F434FC8E023A4A56CD1DA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass39_0_t6CFCEA55CF41A45D364631A3104F2A76AA6EC751_CustomAttributesCacheGenerator,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t4545532939CE37F9D443BDE5480ACD48837D113F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_t96F1A89A3FF6327A51CB422D228BC0AB7831F4C6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t67BA2B4C4BC9A31BF0B49945FFC3E8DDB8D3E9ED_CustomAttributesCacheGenerator,
	ChartFillerEditorAttribute_tA88DFD839463247F91240077BC621BD5116017A9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tD32243AE5CF4061EE858ABD86589073746407D29_CustomAttributesCacheGenerator,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator,
	U3CU3Ec_t35C6B43A9008274F11692B7091A19C7768E4CE27_CustomAttributesCacheGenerator,
	BarMeshGenerator_t4F14218CC54151FCAC8CBB46035CD135D4345C82_CustomAttributesCacheGenerator,
	U3CU3Ec_t0FC208679FC9CA006469A3768BA5B48CAE792C4A_CustomAttributesCacheGenerator,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator,
	DoubleVector3_t067DE045154E14BD0F85E0054C51AFC9000A776C_CustomAttributesCacheGenerator,
	CustomChartPointer_t4EC5C8C22F34C8DA585F64F6D8DA28C2E25ACFC5_CustomAttributesCacheGenerator,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass48_0_t6495EB11621B27869AA20C874F3155A901B050E7_CustomAttributesCacheGenerator,
	U3CU3Ec_tE0014C34268BBB80C3B444BA615F0A558812081F_CustomAttributesCacheGenerator,
	GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator,
	U3CU3Ec_t03FE4757760FC33E9FB20BE671C6D6E8AD9C281E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass46_0_t7A3D2754706A033FA9D18D92BB265665601844BE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass49_0_tAACC950DBCD06E86BA2BC4C265FDCAA8E3630C20_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass29_0_t58C140E2EF87917B83720F17F1557268E454B092_CustomAttributesCacheGenerator,
	U3CU3Ec_t2186A3E8A6CDAB964B8652EBEE5E117410C93556_CustomAttributesCacheGenerator,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator,
	BoxPathGenerator_t89EA4BE0C76203D58A311CFF61B24836F4E86104_CustomAttributesCacheGenerator,
	CustomPathGenerator_t5E45C54F930DF70A03D0AEE692C2ABBB42DF3097_CustomAttributesCacheGenerator,
	CylinderPathGenerator_t3D207A37699975810819C6484F1164998AB60BA2_CustomAttributesCacheGenerator,
	LineRendererPathGenerator_t6189884C6B597C902595D210EEEAF36FF881AB42_CustomAttributesCacheGenerator,
	CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator,
	U3CU3Ec_t8B2732CBF7743AAA1586354DB2C59E86032D5D19_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t0CF425C31C7E5B9B92AFD96A8259C3F26E9BF319_CustomAttributesCacheGenerator,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator,
	U3CU3Ec_tC6775FBF74F4CAFBF224B2EFFDCB89F15F1357CB_CustomAttributesCacheGenerator,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator,
	RadarFillGenerator_t9B039EF2C832742FAEA0550A0C4BB5F9E3994CAB_CustomAttributesCacheGenerator,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator,
	TextDirection_tEC3547541EBAD4AF021131F7F034225013DBCACA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t62E4DF5127AC3B2B3C0A055B7F85C3C16E82700A_CustomAttributesCacheGenerator,
	CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator,
	AxisGenerator_t50787EE278F4D2CD096BEB2AF8DC38760DD71A46_CustomAttributesCacheGenerator,
	CanvasAxisGenerator_t40D28746D976A0B2D0BF7384C6367BD902CFDBF9_CustomAttributesCacheGenerator,
	CategoryData_tFDD3D32896EEB6A2DBB307635F35800942855C31_CustomAttributesCacheGenerator_Name,
	CategoryData_tFDD3D32896EEB6A2DBB307635F35800942855C31_CustomAttributesCacheGenerator_DataObjectName,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_Name,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_DataFormat,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_Skip,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_ParentObjectName,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_DataObjectName,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_XDataObjectName,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_YDataObjectName,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_SizeDataObjectName,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_XDateFormat,
	CategoryData_t5C7FFD46A30B9C966AE1517E87C8A4F5A8A81019_CustomAttributesCacheGenerator_YDateFormat,
	CategoryData_tDABB1C8838A4A1D1E04FA8476579E25A3B413532_CustomAttributesCacheGenerator_Name,
	CategoryData_tDABB1C8838A4A1D1E04FA8476579E25A3B413532_CustomAttributesCacheGenerator_DataObjectName,
	BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_U3CUITextU3Ek__BackingField,
	BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_U3CUserDataU3Ek__BackingField,
	CategoryLabels_t7F560C7AC111B78F13D44B39C8C137F64D74517E_CustomAttributesCacheGenerator_visibleLabels,
	GroupLabels_t1103562719B3D26A50E5BD3D08EACAA7DF809C31_CustomAttributesCacheGenerator_alignment,
	ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_textPrefab,
	ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_textFormat,
	ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_fontSize,
	ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_fontSharpness,
	ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_seperation,
	ItemLabelsBase_t75DB14DF187AB4C178EE87029DAAD71EF653586D_CustomAttributesCacheGenerator_location,
	JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator_m_EscapeBuilder,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_backMaterial,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_inset,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_justification,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_slope,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_prefab,
	PyramidChart_tA0675F7FFA5E8D5AB58C8A13B90FE0201977B55B_CustomAttributesCacheGenerator_Data,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_U3CTitleU3Ek__BackingField,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_U3CTextU3Ek__BackingField,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_mCategories,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_mGroups,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_mData,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_ProperyUpdated,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_RealtimeProperyUpdated,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_heightRatio,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_widthRatio,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_thicknessMode,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_thicknessConstant,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_itemFormat,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_bodyFormat,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_highFormat,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_lowFormat,
	CandleChart_t6DE8B82BE10F8BEFDF311BDF358B56FBFCEFB8D8_CustomAttributesCacheGenerator_Data,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CSelectionRectU3Ek__BackingField,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CCandleValueU3Ek__BackingField,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	CandleChartData_tA8E56FFE52FDC511919850D56E37A0C2BFD75BB2_CustomAttributesCacheGenerator_mSerializedData,
	SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_Data,
	SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MaxX,
	SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MaxY,
	SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MinX,
	SerializedCategory_t9A3FBFFD5A78F50ED8CE690356A2A58996F17C7F_CustomAttributesCacheGenerator_MinY,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_Hover,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_Click,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_Leave,
	CanvasCandleChart_tEA6058533D5E8ED7ECD1DCD04F3AF9B34CED0AD9_CustomAttributesCacheGenerator_fitToContainer,
	CanvasCandleChart_tEA6058533D5E8ED7ECD1DCD04F3AF9B34CED0AD9_CustomAttributesCacheGenerator_fitMargin,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_Hover,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_Click,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_Leave,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_mPreviewObject,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_keepOrthoSize,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_paperEffectText,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_vRSpaceText,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_vRSpaceScale,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_maintainLabelSize,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_U3CIsUnderCanvasU3Ek__BackingField,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_U3CCanvasChangedU3Ek__BackingField,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_ChartGenerated,
	AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_SimpleView,
	AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_format,
	AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_depth,
	AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_mainDivisions,
	AxisBase_tEC2E3697B70FA24AFA3C79390F4904EEB1B51376_CustomAttributesCacheGenerator_subDivisions,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_messure,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_unitsPerDivision,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_total,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_material,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_materialTiling,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markBackLength,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markLength,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markDepth,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_markThickness,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textPrefab,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textPrefix,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textSuffix,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_fractionDigits,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_fontSize,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_fontSharpness,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textDepth,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_textSeperation,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_alignment,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_OnDataUpdate,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_OnDataChanged,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_viewType,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_negativeBars,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_stacked,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_Data,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_transitionTimeBetaFeature,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_heightRatio,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_totalWidth,
	BarChart_t1F8FF7F6F753C896472044E249D0FDFD4B5B9120_CustomAttributesCacheGenerator_totalDepth,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CTopPositionU3Ek__BackingField,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_mCategories,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_mGroups,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_mData,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_ProperyUpdated,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_automaticMaxValue,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_maxValue,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_automaticMinValue,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_minValue,
	BarInfo_tDDFB4A1E88A5178668512462BDA2920DCBBBBF89_CustomAttributesCacheGenerator_U3CBarObjectU3Ek__BackingField,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_fitToContainer,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_fitMargin,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barPrefab,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_axisSeperation,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barSeperation,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_groupSeperation,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barSize,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barFitType,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_barFitAlign,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_textCamera,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_textIdleDistance,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_barPrefab,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_AxisSeperation,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_barSeperation,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_groupSeperation,
	WorldSpaceBarChart_t5942FB809A77CE31FF642299A029A7F163BFF56E_CustomAttributesCacheGenerator_barSize,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_U3CClipRectU3Ek__BackingField,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_U3CEnableOptimizationU3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CDegeneratedU3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP1U3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP2U3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP3U3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CP4U3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CFromU3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CToU3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CDirU3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CMagU3Ek__BackingField,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_U3CNormalU3Ek__BackingField,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_U3CrefrenceIndexU3Ek__BackingField,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_Hover,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_Click,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_Leave,
	ChartAdancedSettings_tF4F7D43A199A95A32F786E7DF921165A929DD663_CustomAttributesCacheGenerator_ValueFractionDigits,
	ChartAdancedSettings_tF4F7D43A199A95A32F786E7DF921165A929DD663_CustomAttributesCacheGenerator_AxisFractionDigits,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_U3CDefaultIntComparerU3Ek__BackingField,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_U3CDefaultDoubleComparerU3Ek__BackingField,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_U3CDefaultDoubleVector3ComparerU3Ek__BackingField,
	BaseScrollableCategoryData_t2A653271124CC36DB3EEEE83466D72BCE54A167C_CustomAttributesCacheGenerator_ViewOrder,
	BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_U3CDurationU3Ek__BackingField,
	BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_U3CStartTimeU3Ek__BackingField,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_DataChanged,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ViewPortionChanged,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_RealtimeDataChanged,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticVerticalViewGap,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticVerticallView,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticcHorizontaViewGap,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_automaticHorizontalView,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_horizontalViewSize,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_horizontalViewOrigin,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_verticalViewSize,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_verticalViewOrigin,
	CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_U3CWorkOnParentU3Ek__BackingField,
	CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_U3CInitialScaleU3Ek__BackingField,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_U3CItemIndexU3Ek__BackingField,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_U3CItemTypeU3Ek__BackingField,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_U3CItemDataU3Ek__BackingField,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_Deactivate,
	ChartItemEvents_tA5C1E2A4E427C036700A599F45FB70160E07D70B_CustomAttributesCacheGenerator_OnMouseHover,
	ChartItemEvents_tA5C1E2A4E427C036700A599F45FB70160E07D70B_CustomAttributesCacheGenerator_OnMouseLeave,
	ChartItemEvents_tA5C1E2A4E427C036700A599F45FB70160E07D70B_CustomAttributesCacheGenerator_OnSelected,
	ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_left,
	ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_top,
	ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_right,
	ChartMagin_t48775F7CC3EEFA26E6B38937C38B661A68D6E013_CustomAttributesCacheGenerator_bottom,
	ChartMaterialController_t46529A1D1C468A9BF1C2EAA4A6A1BB6869DB87F8_CustomAttributesCacheGenerator_materials,
	ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_CustomAttributesCacheGenerator_U3CTagDataU3Ek__BackingField,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_NameChanged,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_OrderChanged,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ItemRemoved,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ItemsReplaced,
	KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_U3CRowU3Ek__BackingField,
	KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_U3CColumnU3Ek__BackingField,
	GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_fitToContainer,
	GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_negativeFill,
	GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_fitMargin,
	GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_heightRatio,
	GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_widthRatio,
	GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_itemFormat,
	GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_Data,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CMagnitudeU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CXStringU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CYStringU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField,
	GraphData_t376FC9F33A5BD4455A70B789EDF9018C2E669D9E_CustomAttributesCacheGenerator_mSerializedData,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_Depth,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_data,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MaxX,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MaxY,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MinX,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MinY,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_MaxRadius,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_LinePrefab,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_FillPrefab,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_DotPrefab,
	SerializedCategory_tAE5D22492836A4FC0D1C819DD95931FE11E97BD5_CustomAttributesCacheGenerator_AllowNonFunctionsBeta,
	WorldSpaceGraphChart_t689AAB22CFEB7473627C3822123DA6CCF837080A_CustomAttributesCacheGenerator_textCamera,
	WorldSpaceGraphChart_t689AAB22CFEB7473627C3822123DA6CCF837080A_CustomAttributesCacheGenerator_textIdleDistance,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3COrientationU3Ek__BackingField,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3COffsetU3Ek__BackingField,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3CLengthU3Ek__BackingField,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_U3CRecycleTextU3Ek__BackingField,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_OnDataUpdate,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_OnDataChanged,
	BoxPathGenerator_t89EA4BE0C76203D58A311CFF61B24836F4E86104_CustomAttributesCacheGenerator_HeightRatio,
	CylinderPathGenerator_t3D207A37699975810819C6484F1164998AB60BA2_CustomAttributesCacheGenerator_HeightRatio,
	CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_prefab,
	CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_lineThickness,
	CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_lineSpacing,
	CanvasPieChart_t2993A63AC243B53E9EB384BF39E64D189C5CC563_CustomAttributesCacheGenerator_lineMaterial,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_meshSegements,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_startAngle,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_angleSpan,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_spacingAngle,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_radius,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_torusRadius,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_extrusion,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_Data,
	PieChart_t99AED86FA9EE478428FD94B20B73B6DCDD83715F_CustomAttributesCacheGenerator_clockWise,
	PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_mCategories,
	PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_mGroups,
	PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_mData,
	PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_ProperyUpdated,
	CategoryData_t4945CE6065C155C06D9AD09BDBD73DB7F47A53D1_CustomAttributesCacheGenerator_RadiusScale,
	CategoryData_t4945CE6065C155C06D9AD09BDBD73DB7F47A53D1_CustomAttributesCacheGenerator_DepthScale,
	CategoryData_t4945CE6065C155C06D9AD09BDBD73DB7F47A53D1_CustomAttributesCacheGenerator_DepthOffset,
	PieInfo_tDC5F07B5F293ECBB720A8460A8992534FF89EB6B_CustomAttributesCacheGenerator_U3CpieObjectU3Ek__BackingField,
	WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_textCamera,
	WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_textIdleDistance,
	WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_innerDepth,
	WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_outerDepth,
	WorldSpacePieChart_t27F3BAE7D9A5420CD2BA8FB08970456DC901D517_CustomAttributesCacheGenerator_prefab,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_radius,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_angle,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisPointMaterial,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisLineMaterial,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisThickness,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisPointSize,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_axisAdd,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_totalAxisDevisions,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_Data,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CIndexU3Ek__BackingField,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_U3CPositionU3Ek__BackingField,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_mCategories,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_mGroups,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_mData,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_DataChanged,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_ProperyUpdated,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_automaticMaxValue,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_maxValue,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_automaticMinValue,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_minValue,
	CategoryData_t03BB589B3636224DA50177D9BEA0C882C1632F92_CustomAttributesCacheGenerator_MaxValue,
	WorldSpaceRadarChart_t3FEE5BCF4312293C38569712DF5D7F6C78AD3E18_CustomAttributesCacheGenerator_axisPrefab,
	WorldSpaceRadarChart_t3FEE5BCF4312293C38569712DF5D7F6C78AD3E18_CustomAttributesCacheGenerator_axisPointPrefab,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_autoScrollHorizontally,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_scrollable,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_horizontalScrolling,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_autoScrollVertically,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_verticalScrolling,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_raycastTarget,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_horizontalPanning,
	ScrollableAxisChart_tB563C96965FA7209E0333ABADE6949DF3DED238C_CustomAttributesCacheGenerator_verticalPanning,
	AlignedItemLabels_t916ED3D73B7285478225379BAD5447D11A7E3A65_CustomAttributesCacheGenerator_alignment,
	ItemLabels_tBA5545E0FFC4D9B10BF17C46057E810D4DD406D9_CustomAttributesCacheGenerator_fractionDigits,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_prefix,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_suffix,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_customFormat,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_OnDataUpdate,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_OnDataChanged,
	PieAnimation_tC5CB4F1D58BA7F7C0FF7517744BFD4B52022FEEC_CustomAttributesCacheGenerator_AnimateSpan,
	CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_fontSize,
	CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_CategoryImages,
	CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_legendItemPrefab,
	CanvasLegend_t71981DAE68A8158128741AD484C086DFE224E363_CustomAttributesCacheGenerator_chart,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_U3CUserDataU3Ek__BackingField,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_U3CMaterialU3Ek__BackingField,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_NameChanged,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_DataStructureChanged,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ItemsReplaced,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_DataValueChanged,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3CItemIndexU3Ek__BackingField,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3COldValueU3Ek__BackingField,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3CNewValueU3Ek__BackingField,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_U3CMinMaxChangedU3Ek__BackingField,
	ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField,
	ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_U3CCategoryU3Ek__BackingField,
	BarDataFiller_t3BCD05D26ACB05BC8E69A76364821707E72F0CC6_CustomAttributesCacheGenerator_BarDataFiller_GetData_mD41A3305539B936D9A51B5EF5F6EFF55107730C2,
	U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24__ctor_m23346B90BF5520515FB01571AAD820E9056BC2AF,
	U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_IDisposable_Dispose_mEC6A57765D641AA00A34DCFA6092965012FE2958,
	U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D8AD14E0C19EE312760EFFF80A2F14CBE757EFB,
	U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_mB0F4ABD0744B078491FA16002ED4D4749F55AD9E,
	U3CGetDataU3Ed__24_tE0074C6DF1FA9BE6C311055FE3C8F45386C24F9D_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_mCD2136265947A93966E384448D5C93D5B0D037D5,
	GraphDataFiller_t61E9DCD0514869E9116CF79779DA2B0250E1D5B5_CustomAttributesCacheGenerator_GraphDataFiller_GetData_mDB9DBCC704135811FA58F24E972BEAF1918525C8,
	U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29__ctor_m3C343F3EC10F96FC8639D32AA99C25FA12075BD5,
	U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_IDisposable_Dispose_mE4AA5BFA3FC25D675CA57B4C8075D20B84A1830B,
	U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0F32C1C524518E6F04CCCCAD3771DAAE1665EC45,
	U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_Collections_IEnumerator_Reset_m7DCBB1D02F477820DCE81C95EB63B1D2747AC323,
	U3CGetDataU3Ed__29_t77FAD087931849AC1E4AFA71F0066709EF7F8104_CustomAttributesCacheGenerator_U3CGetDataU3Ed__29_System_Collections_IEnumerator_get_Current_m628029FF6FC5324234A6D305A09351A288BF00D2,
	PieDataFiller_tE8F7318A12D99CCBB9818DEC6756D9A1E6A53EDE_CustomAttributesCacheGenerator_PieDataFiller_GetData_m7AD75ED5136A7540B377D95CDC73B5156AE07BE0,
	U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21__ctor_m5FDFF7FBE070B5D39B9735F93E54F91C4650592E,
	U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_IDisposable_Dispose_m3690B5FEA5C363A68E5CF5491D67E4DAF05E0C08,
	U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m162CF48EF0D446615C4EF6D006C692CDD0154710,
	U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_Collections_IEnumerator_Reset_m37DE13669B84A238C4A939A80966461EADE38B04,
	U3CGetDataU3Ed__21_t24A91561847688D5C1489EA8727BA412EB58E05A_CustomAttributesCacheGenerator_U3CGetDataU3Ed__21_System_Collections_IEnumerator_get_Current_m36AEC0B435EC47BE9E67D3BF1F81D07AE22C4C1E,
	RadarDataFiller_tE61CC6A21189511702B918A36F3A0F5078AF39D8_CustomAttributesCacheGenerator_RadarDataFiller_GetData_mB7FDF2D44705CFB8C333109D80F1D6581A254D91,
	U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24__ctor_mE5AC81C7F88B9C90F79E72BE064CC18DD6783980,
	U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_IDisposable_Dispose_m1682B988B3E5EC516BD0A4DFE5AA57A883EE6F51,
	U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70B61A70670719AED94EC9A2B8C80E97A217024D,
	U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_Reset_m5F195A69B5832A7F5B608683EC834806914785E3,
	U3CGetDataU3Ed__24_t41077E2AB0EA1D498A7CEA90629D8444FE3BD94F_CustomAttributesCacheGenerator_U3CGetDataU3Ed__24_System_Collections_IEnumerator_get_Current_m2101121EA708E368E26EAF98BE1B497878498183,
	BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_get_UIText_mAB7F8962EC45CF04171D3F0D4C2CF3BBE84DB0B0,
	BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_set_UIText_m06B97F41D530290E48CC6D721F830DAA5C0380B7,
	BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_get_UserData_mE921AB0E2FE52066A0EA5DA30DFA7D3F80157BE8,
	BillboardText_t0737A0AC8DF8F0D8F2FFB9BC066A09FECE7D6866_CustomAttributesCacheGenerator_BillboardText_set_UserData_m0E39DB9C7C06ADB39CF5E98F7B89C46D9B6D4571,
	CategoryLabels_t7F560C7AC111B78F13D44B39C8C137F64D74517E_CustomAttributesCacheGenerator_CategoryLabels_U3Cget_AssignU3Eb__6_0_m1E37F7A84B474AC9DE3E6366332A2BF2A757026C,
	GroupLabels_t1103562719B3D26A50E5BD3D08EACAA7DF809C31_CustomAttributesCacheGenerator_GroupLabels_U3Cget_AssignU3Eb__5_0_m8BB30C6A5D704B6E4463E5D6C87799990E5BAB9E,
	HoverText_t851B98B482893DB3A6168ED3D76FA1FCA3776188_CustomAttributesCacheGenerator_HoverText_SelectText_mC859EF99FE3BA0697E61E7784454E77239FECEA9,
	U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11__ctor_m97FF7574BECDA2356CE91AB79D964D437607610B,
	U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m0137CCE653C298077E28C9B537639ABF411CF179,
	U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD62DC4EDF7D813AD2A227E8A3997EC293D5F4D42,
	U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m4A0993E9C1AE47CF9B6BCABFB295969705F88EB8,
	U3CSelectTextU3Ed__11_t3A0EBF8D346C3628FD97969FBA354F713DB296F1_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m1D812B2A83110FF66329D8CF9E21A57F6D078C09,
	GraphChartFeed_t778BCE9C466C9547B6B59D00FF1B55C596AC1319_CustomAttributesCacheGenerator_GraphChartFeed_ClearAll_mFDDC6A3A71B5FBA7B10276AA0261B00A77514396,
	U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1__ctor_mC3D3B41DC95CA047E8CBB6D4EE7EE0A3C9B93887,
	U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_IDisposable_Dispose_m9C5BBF85B5384B6AB898F8AE85C44DCC0AA54B93,
	U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BB8B95E41786ADB3903FAAE2B823A72A4E4C0CE,
	U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_Collections_IEnumerator_Reset_mFCAE984F92148CEAD61AFDC447B2B13371123DD3,
	U3CClearAllU3Ed__1_tAB2ECE93CA93A79F38E7F910DB723577B25721D8_CustomAttributesCacheGenerator_U3CClearAllU3Ed__1_System_Collections_IEnumerator_get_Current_mC9FDAC481477167131505D1A917E36F942CA3CD9,
	PointHighlight_tB4103DCE16A58CFBD7D300E42F30CD6E24BFB974_CustomAttributesCacheGenerator_PointHighlight_SelectText_m8A72511CDE4D7594F99C839614E4CD5C12AF43DE,
	U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11__ctor_m3A5A14A90EDDBB09F4306CC9935BAB476EA0D3D0,
	U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_IDisposable_Dispose_m5D7B4F7984536AFA0FC52BED1C0B323B93EBB09A,
	U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF876D96FB9874E3A5498EBF30EDA4899359AB74C,
	U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_Reset_m9E9192BE9A6918BBBAE4E1CE01CE78D9250D66A1,
	U3CSelectTextU3Ed__11_tBC911EA99D67FEE380DCF7F58889A19B97A98D7F_CustomAttributesCacheGenerator_U3CSelectTextU3Ed__11_System_Collections_IEnumerator_get_Current_m4FD7A47CBC202B781432238F82B2B019FDDC5878,
	MixedCharts_t47E9B56D0A4A93EBC3EF42C248DDDF093F5CCC0D_CustomAttributesCacheGenerator_MixedCharts_FillGraphWait_m4D334BD00C325E608B9863ACF308B1CFB44CDBF4,
	U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3__ctor_m605B8A9DF0DEF3C1D6455E9106738FEF84BB1D27,
	U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_IDisposable_Dispose_m8252579F9A52AC98B6829F106FB8413592DB3A2A,
	U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC7564710146FBEBC473D30BAD1A81F231303B6F7,
	U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_Reset_m5A91A4B97550476A7720F47010FEE9AB4FC11D8A,
	U3CFillGraphWaitU3Ed__3_tA04C86DB265CE5AECD1C2AC1A28800E08BA13111_CustomAttributesCacheGenerator_U3CFillGraphWaitU3Ed__3_System_Collections_IEnumerator_get_Current_m349F3E4E8B051771D60B027E82B421A6949E0546,
	BasicSample_tE5EFC159CADB6A8C8E71AA56BAD9A061A209C1C4_CustomAttributesCacheGenerator_BasicSample_U3COnGUIU3Eb__1_0_m1A5B0705F6B217AA15C3D60BA3B8F957AE9A97DA,
	BasicSample_tE5EFC159CADB6A8C8E71AA56BAD9A061A209C1C4_CustomAttributesCacheGenerator_BasicSample_U3COnGUIU3Eb__1_1_m854014CDE7FA4D215818F9CCE18CC1C06D8666BE,
	BasicSample_tE5EFC159CADB6A8C8E71AA56BAD9A061A209C1C4_CustomAttributesCacheGenerator_BasicSample_U3COnGUIU3Eb__1_2_m640031C29BC7B5CA6966D6C5A57C5FA0F42654C8,
	CanvasSampleOpenFileImage_t16B5870D57BD12D55FA713BA93628EEF53FFEE37_CustomAttributesCacheGenerator_CanvasSampleOpenFileImage_OutputRoutine_mC6AC6C7020F774BBBB5C1EC7D70113DAC9CE2AE1,
	U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4__ctor_m98B984849785F14A4C8AB9C792DD30C7E2C217DA,
	U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m93AEB066101CFA725B073186A05AA61331643C68,
	U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mED88C4EFD5B495F4D2972BFFEDEDA8A4FA23A8BE,
	U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m5F856A6AF129DDDCD28467ABFEA0B6EC6E826F73,
	U3COutputRoutineU3Ed__4_tD477FF8DC1CD84EDFD107E1C8351E1CD14444925_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m8F721991272364EAF9BEDDBBB389F230FF990E2A,
	CanvasSampleOpenFileText_tBD2B2C8E2DAA1044307221A4BC638C0F6250A0AA_CustomAttributesCacheGenerator_CanvasSampleOpenFileText_OutputRoutine_m6EC022ECFF5A958B50C9FD430032EE0A18972DD5,
	U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4__ctor_m6052F5D62A93EBD9C0E11383EB07CBD7660382CD,
	U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_m185B54E87BBFA49A5D93D56378C5D5B699C6D0E3,
	U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD425B64024BB6BC1769FAD619E67F9F309A486D8,
	U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_mF82EE533AC3EFB9F1A356CA8603DE322C066D634,
	U3COutputRoutineU3Ed__4_t159C9E6E36F094E4A45CCAD95C7880DC3DF79AFF_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_m95A29BAFA9A8371AEF30517414CE3A5CD502F8B9,
	CanvasSampleOpenFileTextMultiple_t6A74601385701DA6AE2BD30AEEA0217D8987F387_CustomAttributesCacheGenerator_CanvasSampleOpenFileTextMultiple_OutputRoutine_mBC9DD3FC851968220AFE1A5B97FF7F0A81A830C2,
	U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4__ctor_mBDFF06C3C8D544BE9FBC7EA29B4790B7F3F288ED,
	U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_IDisposable_Dispose_mEB828D2EA10D8A142380291F8820E27C7F743386,
	U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m57BAA75BD2D41E4F0A357D1679D79CF925EF1FD5,
	U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_Reset_m15778AB91FC3ABCC7AA558BFE3224F64EA05925E,
	U3COutputRoutineU3Ed__4_t7D0BD90299E1AF1F3CFDCD576C19F61D3EFB1103_CustomAttributesCacheGenerator_U3COutputRoutineU3Ed__4_System_Collections_IEnumerator_get_Current_mC1183837FBAE1B02788D12B358A1F890CE7D069F,
	JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator_JSONNode_get_Children_m9A419003FE3E6C582C581168325DFF2F916EA4E6,
	JSONNode_t29D42096352C7177A8E65064D3AE25BC60837A4C_CustomAttributesCacheGenerator_JSONNode_get_DeepChildren_mA4B063A992BD18F9254C2E4798DA93637ED05EEE,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40__ctor_mD2664979A1B8AE875DDB2840B6156772FB3ECFDD,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_IDisposable_Dispose_m944D00543660D19F0E42EC6B6B139CB5D7EDFECC,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m4FC76B45630A04E0A0046A1409F00FB5FABE46B9,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_Reset_m97CD00E06349E3DE3CAD68B058AE2EA97797AEDC,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerator_get_Current_m8F61A110C435979412391A15BC54115CA7E0DEE5,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_mD91303D1AB77DD3B2593235773ED3B39DF1C2895,
	U3Cget_ChildrenU3Ed__40_tB848419922D1AE3324C87D48415130984E115253_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m8E4D404140934BAD769BAA96C71954DF437DBEB4,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42__ctor_m1AD4A760497B8C4CAB4E547E035B8ED4EBDDBFAB,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_IDisposable_Dispose_m75E2DD336D0A67E1EA6196D218C1F2D9E26D2242,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_mF52E7C66AC2CFE035C5422378EBA10E005BD16FD,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_Reset_m093AE80D972ACB3ED4CF25C53041441B789576A5,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerator_get_Current_mDC85D32E36F7E3EEA9C509E44985F3540F4659A4,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m3E11223B21486BF7B377ED4B07EA9557C32ED634,
	U3Cget_DeepChildrenU3Ed__42_t703BF79D437975B62C6A1A47A642015055F7D91E_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__42_System_Collections_IEnumerable_GetEnumerator_mB972D9FF70DEDEBBC7507696C8353787B73D448A,
	JSONArray_t04D56DA332EFB150BB42B2CC5764F7C4A41D92A5_CustomAttributesCacheGenerator_JSONArray_get_Children_m7459647CAFE8FB57E0C4D2F757AA72BCA7749CEC,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22__ctor_m09E100DFDE814208000E288F38AACAA139288972,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mFBB72D4FC09BA522703066660A1B6C84A2B2662E,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m0997099AAF452C670AC89A3DC8EB6F3F42EF8A2E,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_mA44FDA37FE9633368C64D524CC7FFBD664058879,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_mDB53E925A7A6DC776A4F6BBB37B8E7AE9A7B4ACC,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m85636C898F51CA71FB68F27A5F0E3AE34CE40854,
	U3Cget_ChildrenU3Ed__22_tA3BBC63B4916C5B56869703F00BD2D01EE47D3E1_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_m2724CBA0FE19F63D89407542FD0085D90ACBEC0A,
	JSONObject_tAC0F80D5CCC63632CF36C37AE4112FCBE6AE196E_CustomAttributesCacheGenerator_JSONObject_get_Children_mBA3F815A9E4AA185FB9D3B41346F80D3D89FCEEB,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23__ctor_m27FE9E533731FF76A4DA5A1495777DE4976EB2B5,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m14D80EBCF6DEF404F017771F3E97B465C4E7B66A,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CGraphAndChartSimpleJSON_JSONNodeU3E_get_Current_m406355CB8A2EB21F8264E1A23C80A1921AB1C795,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_mC76C5EF6AE752D074A3A428BEBAA2A1CCAB85232,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m99340EAA456C373D91FF62F016A592BBCFDAEC01,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CGraphAndChartSimpleJSON_JSONNodeU3E_GetEnumerator_m1ADC7153B59830633F604BBAB11DC65F176AC157,
	U3Cget_ChildrenU3Ed__23_tF8BA0D42C363B66A409A6336A131D6F635B97514_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m78AC7A7CE03612379624BE539ABBE50619F0149C,
	JsonParser_tB519CED5504523188C3E6488A912391A11661B8A_CustomAttributesCacheGenerator_JsonParser_GetAllChildObjects_m64B3F53686284293C50CD18063C787EF61B153C2,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12__ctor_m47139479FE975B344E47217D280FFC312E318714,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_m8BBB18472020BE742C8E39B81239FDACD74663BB,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m24515EF29C62E5B28F804336D64C95621B543A8A,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_mD2374F91492CA89E5E1A6224468E705D910E38CC,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m3715E650CF1A0C57AAA0F132B149816F130815C2,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_mEC7AFF580DB38394CB779DFBD45E50A202643EF1,
	U3CGetAllChildObjectsU3Ed__12_tB8ED420AFBEBA433D0D53CCB2786970DA240F3B0_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_mE9A7F7C95471C247081CF66366D9A4DA207933EF,
	XMLParser_tAEAA67638C8565FDD734BFAFCF7450135AA8755A_CustomAttributesCacheGenerator_XMLParser_GetAllChildObjects_mE2F140C257C62461EF9667E77BF1BD013EE8459B,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12__ctor_m5531B147F08A6B3597DD2C73FF2550DD018683FA,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_IDisposable_Dispose_mD6F2D843626DF724B8E34D5DF9B05FDD7D826D98,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_get_Current_m5D93105895C6A0F0C06B8B11727EA7C8B91C8E8F,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_Reset_m5FB1E2B73B3C5F5EA1DA48A97191F445FCB1CE0C,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerator_get_Current_m24F99202D95EB103F25D1F71E0FDC04BED1A8CCE,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CSystem_StringU2CSystem_ObjectU3EU3E_GetEnumerator_m8F633C74F5985C71F8C1B945DF1A566EEF7DDDFE,
	U3CGetAllChildObjectsU3Ed__12_t7EA4AA169DE33A7CA21BEA21FFCE70DA65A62F6E_CustomAttributesCacheGenerator_U3CGetAllChildObjectsU3Ed__12_System_Collections_IEnumerable_GetEnumerator_m36887960DD50A7F7F13F9B101A78F4D3FCCA2741,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_get_Category_m775875544DFBCAB3C0166BD745E20B325A86949D,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_set_Category_mDE1EDDF8E56E2BE4BFA785EC1BE243D6BFE91E47,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_get_Title_m9DFB5ADA82BE5AB49408925E61963B2A0FA8A5CF,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_set_Title_m6A6B22563451D13C04CA6969727FD453C5523235,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_get_Text_mE039C1B01C569C62FF5A03AAB2FB2E60C9697B13,
	PyramidEventArgs_t841A1C00AED1B72C776E3A4D3F4AD33DAD426368_CustomAttributesCacheGenerator_PyramidEventArgs_set_Text_mA128C4B843BCB63A903A1C4EF0CD6CF9168BCFBE,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_add_ProperyUpdated_m8DFF08F96D64766B67602D3E02BCD47F9E38C7C4,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_remove_ProperyUpdated_m6AD009FE63BD91431ECBEA9447D177E4A0A902AC,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_add_RealtimeProperyUpdated_m11C0E533367D009433E0BEAA414BF8FE6E37E88D,
	PyramidData_t6609B80D2C90B738AB74FEF046BF1FC4F1F9F02B_CustomAttributesCacheGenerator_PyramidData_remove_RealtimeProperyUpdated_mE3E870B58AC7B15C2FAF95507D56A16D56181C02,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_SelectionRect_m1BA91E7AC81BEF22E923E40B1B6F3D1A980EC63D,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_SelectionRect_m335496950C896DF96106DA2CC0C7A5E2D9DFF041,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_Position_m4E369BBC39D884BFC6196F170705CE2F787B99A9,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_Position_m2F6E62EE3976061B158387AA04B8DBA38BB6F5F9,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_Index_m1124A9CE28D536D86334A6988DEDEA4CB97D7FC2,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_Index_mA835DEEF93ABB5FC3E5D025662A85A18AD07570E,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_CandleValue_m8D6CA1E8F4F22BC926F537B47ED160F769992D43,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_CandleValue_mD145E35AA00E49F70E63237F7F72853F916879D8,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_get_Category_m9ED5F70A96903ED3BAACD6717412E2D09596F354,
	CandleEventArgs_t3D82B490D00E506D6B357390F1F18A83908D5894_CustomAttributesCacheGenerator_CandleEventArgs_set_Category_m3D313A3E5EE3DA1262B5DEF2C06AD4711E3FDD8A,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_add_Hover_m7A03F0024F862BDB44D4B95310A38BA14E6EC5FB,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_remove_Hover_m08E6A223A93FA472CB1946DC0298731133BF76EE,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_add_Click_m2955A032522633F04F7896963B5DE7E69FA672AA,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_remove_Click_mB33B571C99D746F92B3E0A20ACE09828A18DC08E,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_add_Leave_m473992F13C76B971C580B3B73D112C80F7F5136E,
	CanvasCandle_t6157AE31240717E1E82AE4BC611E7747A643066A_CustomAttributesCacheGenerator_CanvasCandle_remove_Leave_mC622586CBA098291105A33A16BC79ABAAF382AB8,
	CanvasCandleChart_tEA6058533D5E8ED7ECD1DCD04F3AF9B34CED0AD9_CustomAttributesCacheGenerator_CanvasCandleChart_U3COnItemLeaveU3Eb__45_0_m811F170BE8838C17E66ADF78BEC2F32838A8F701,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_add_Hover_m8BF66FBE73A4BB5FBD226C41DBB6F894B3BBF93E,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_remove_Hover_m162A8BCC790D2A9BEB17971D675E30F13ACBA443,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_add_Click_m7131C9252F108EACEB73E742A113B1745186FC6A,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_remove_Click_m704370212C1B5977C2B01D7EE3E5F7E3AA699F83,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_add_Leave_mCDC72A0E33C950DC0EF06F5175A3728E3FE0782F,
	CategoryObject_tCAFAFB397BC20F9E161BFE86B6D2DC4181F8ED1F_CustomAttributesCacheGenerator_CategoryObject_remove_Leave_mD290676B1FF98DCE20CB2D9ADAF06D8C6FF87D98,
	CanvasCandleGraphic_t365BA4E9C82105329E6F8A213CA4315700C93D23_CustomAttributesCacheGenerator_CanvasCandleGraphic_getOutline_m798E8CB9D2C34E3562F6F0AC84703B1D56FA42C5,
	CanvasCandleGraphic_t365BA4E9C82105329E6F8A213CA4315700C93D23_CustomAttributesCacheGenerator_CanvasCandleGraphic_getCandle_m3B6B00CD1C59B6EDD3CE21871A5804774A79B9CC,
	CanvasCandleGraphic_t365BA4E9C82105329E6F8A213CA4315700C93D23_CustomAttributesCacheGenerator_CanvasCandleGraphic_getLine_mA140EF9183453BEC0C4FC21070BD37783E57AA89,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19__ctor_mEFDBA2A898B7FBF0830DD5FB9F9B83F4BC10E633,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_IDisposable_Dispose_m42DB601169ECF3C4049FF5FA8C7C40A82768B6DA,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m13CD6F38EC403E61CC2DFA08B5F37CF5E297D386,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_Reset_m8F76A0AA1EFF342B4F641587492819DF94693951,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_IEnumerator_get_Current_m8F0F3DDF253B4632CAAD0D24DFF88B1BA6D1E9BF,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m6D8E95C447218F47700C4C2495D423ACE17CB9AA,
	U3CgetOutlineU3Ed__19_t9F56D4C7CF2C843A9DD58F1659AB85F8D2591CD8_CustomAttributesCacheGenerator_U3CgetOutlineU3Ed__19_System_Collections_IEnumerable_GetEnumerator_mE3E50EE5A6C6DB0C705F3B7DDDD78C530B4DA2C8,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20__ctor_mC82F7A257D54893E320CAA74A4A9F8E3C46F7A6D,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_IDisposable_Dispose_m81A77A4E49DF47FE1ABDD3EB07FF558721202A35,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m4BD2F0D780FDE9F01E9498D7976DA81876228CEE,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_IEnumerator_Reset_m6F0292D8486F07BDD361E3A7872B63C1961C1EC0,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_IEnumerator_get_Current_m9111EB1D61ACFBDCC6E9593D576936C79A24EF70,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mB9B8A2839CD947E1084EEE1508B62501C9C9A98B,
	U3CgetCandleU3Ed__20_tFD2844DE3C2EA6FCF3905290D609BF23A0C5C45A_CustomAttributesCacheGenerator_U3CgetCandleU3Ed__20_System_Collections_IEnumerable_GetEnumerator_m9645F135E86563D363D9F1A8D234A86E607AC9F6,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21__ctor_m0326018FB282A0BA3FEC754F4D9D47FA72F54172,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_IDisposable_Dispose_mA0D475FB96EB7B1DB8A497F5E7A45D331428E58D,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m5603D072D3ECD9E42D2013A333694F7E1C09C69A,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_IEnumerator_Reset_m01DC42B477E9802F36C385E8C991F6492566F885,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_IEnumerator_get_Current_m8FE1DCA1D916AC6C8846759C6A879874BDDAD7FF,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m20622E0E1FDDEE6266F33E1492744F6A8F0045D6,
	U3CgetLineU3Ed__21_tE7DEAC1FE71F40CB149C1ACE90978A41FC7B8D77_CustomAttributesCacheGenerator_U3CgetLineU3Ed__21_System_Collections_IEnumerable_GetEnumerator_m2FDF32F5C4E66B3920C4557CBFF30D99422DDC2E,
	IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_add_InternalDataChanged_mAFC2FB0E987F32503DE4675E3B4528E27FD106FC,
	IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_remove_InternalDataChanged_m35FC5D9C3662DEF17C6A42C74DD2EDDD5298C5E0,
	IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_add_InternalViewPortionChanged_m4D9F7CFD4AF0FBE12271B85E5B74AB500AA2F32A,
	IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_remove_InternalViewPortionChanged_mBEEA538E6EAFA3F041A9B9FCBC7D71D9D8C76770,
	IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_add_InternalRealTimeDataChanged_mDC032C202968EA790C442E614C1F99598C6105B5,
	IInternalCandleData_t001282BD800F57454EFE9CC97D105EECA72C8C58_CustomAttributesCacheGenerator_IInternalCandleData_remove_InternalRealTimeDataChanged_mC1AB19A666CDBE6CFBF410696E3BB17BDFD62DB6,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_get_IsUnderCanvas_m195603F81F69BB55C721E932AE8A27457CE3C89C,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_set_IsUnderCanvas_mC1095C52AD717D93A0FF99BB0E5734571ED51BBF,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_get_CanvasChanged_mC57ECBC1E94FE78A2BCB30A80D0A44302F8DBC6F,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_set_CanvasChanged_m12590C6FF3B6795685A5E876D7E9F4CB76D2A834,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_add_ChartGenerated_mA7DB9A7B4BB163D50DECF59625C459C4C2095DA6,
	AnyChart_tE2E5687EE3F16215D61BCF628924A2FCB040A7A7_CustomAttributesCacheGenerator_AnyChart_remove_ChartGenerated_m523F495E2E3E114810C67FBD82A5BC5909D7B5A1,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_add_OnDataUpdate_mAC71E3C98BDF170D02D0AD5B2696655EC74FFD36,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_remove_OnDataUpdate_mB943D54C9978DFF4D3502C4E7977E206BCCE64BA,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_add_OnDataChanged_m3D8BD87F819D255B2B010F2123A4EEC7F76A1A5B,
	ChartDivisionInfo_t6FC8790222217A5D846EDBBE993850113D633166_CustomAttributesCacheGenerator_ChartDivisionInfo_remove_OnDataChanged_m2287FF3D6D991ACCB9A6D84E73C352A9ADFD193D,
	HorizontalAxis_tD419D17477F803DF3DFA77A5A64DF7A265195C8C_CustomAttributesCacheGenerator_HorizontalAxis_U3Cget_AssignU3Eb__1_0_m6189D24F8E7B7616E51A82A16920AA7CAE45E1DA,
	VerticalAxis_t53DBFF8EAF1812932F1C27298A47D58E34559D95_CustomAttributesCacheGenerator_VerticalAxis_U3Cget_AssignU3Eb__1_0_m89177C3B9F17E4E1551B566302417DC94F34F3A4,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_TopPosition_mE31D84CAA751B3DF74913ACA6373FF46CEBD4013,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_TopPosition_m41B22FE4648B00A7E795C1F56F6E1C686BFB43E2,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_Value_m06CD6D6E61DA6E762AEE2A011CD6B5B104FE331B,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_Value_mC913E728B00D39DDCDFBD63E75B9BC2932DE9B03,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_Category_m3E31C561F7C77965215EF838112B706808754EC6,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_Category_mF8E4603293F4B61290C0AC030DF8C92605749AD1,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_get_Group_mE173DA1B1DCD09DF12A094D76BAECE8CE0A1FCFD,
	BarEventArgs_t9702E01810250ACCF7914084EC3B032C833B0FEF_CustomAttributesCacheGenerator_BarEventArgs_set_Group_mB3EA1BA0BFB6DDC8B50F8CC8A8C5038C2ACFA69A,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_BarData_add_ProperyUpdated_m3BA9B92B6E412D8239F68AA155C0AC8109B79693,
	BarData_t036DE51C879FA4D3FD55B5F51D1DBB2A9DBDCEE9_CustomAttributesCacheGenerator_BarData_remove_ProperyUpdated_mF53A3D39F62A6B39588F48F64E386BA2A7A29107,
	BarInfo_tDDFB4A1E88A5178668512462BDA2920DCBBBBF89_CustomAttributesCacheGenerator_BarInfo_get_BarObject_m7E3CC708841B7C5940E943A7111C56B722F90D5D,
	BarInfo_tDDFB4A1E88A5178668512462BDA2920DCBBBBF89_CustomAttributesCacheGenerator_BarInfo_set_BarObject_m55485BFB5ABF5DE9B7ED25DC70E4DC6B63A4852E,
	CanvasBarChart_tEF87E66609104726D579AC30D3D1717EF9534FE8_CustomAttributesCacheGenerator_CanvasBarChart_InternalGenerateChart_m48A6475265177A377CD7A1590CE29B2E52E7470E,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_get_ClipRect_m474D9C89D9AE388D759504AAB85FBB2E5F6E253C,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_set_ClipRect_m8016CC7BCDC55AA769103BAD3800B83F0EBFE926,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_get_EnableOptimization_m202B20B5E9F00E4A1213D5ECCB945268B1593457,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_set_EnableOptimization_mCAC2A3AE52E5A5B3BF4C886BD10BF48960BA65FF,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_getDotVeritces_m57F8AD391BB4EA247CE286F9B61901B445F0B5C5,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_getFillVeritces_mEBED888394C7BD35F69B03E2D32028DC50B5D971,
	CanvasLines_tCCA1CFD3EE81D2C784FFAD2C8A6E9D71B6EFBABA_CustomAttributesCacheGenerator_CanvasLines_getLineVertices_m3EE775FCB0F13DC4E61DE3FDE73ED596A0977B66,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Degenerated_mC903C588921C51C16C8A9EDE11AACCF1A4C759B6,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Degenerated_m378A5401A9BAA385E72BC9885DC3B53F1414B451,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P1_mC750288C96B6CE408C61A85DF57B29B463D10C0D,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P1_mC38DFAC465CCB453B3B4AB7E97A447590CC89965,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P2_mB960A6BEF78A9B952DAE1A299D67CFC2012E2A1F,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P2_m160F2F164EF038D1F32214DE28204AE9683D33EA,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P3_m06A9A176422365C1575296A1F25FD5DEF4D99DDD,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P3_mCAF01F4FED931AA69E7246B0FCBB544D303C7B70,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_P4_m8283F3E3C3EFF22F76BDAE1934F53F5175BD2616,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_P4_mC6DD8CC9AB985D8FB738267F457D4C34250B9E6D,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_From_mE45AE1DDC8A9D9EBF61227F17C4266A2F2DE6E9B,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_From_m592C4AE6BD4846DFCC12721665EE1B58E1D47F04,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_To_mD8FAC6076802B34205395DF694BDCE8314F1F2DA,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_To_mB09CD554B116268CF79C72323AE421A1C6C0B1F3,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Dir_mCD8563E867388D16CF2A859711F1868D2A1E77D0,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Dir_m8099FE4EEA13DB6F851843C3258BC6931DE82B85,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Mag_mFBC4B05934E2168B53EA8A64864446F3CE366AF5,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Mag_m8DF404321EC5B783086BAAB927F520F0B4FDE1EB,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_get_Normal_m8F500F8704DEEF72A8F69A20608C2ACEE316146A,
	Line_tC735BBD98ED3AA38D14C142B0DF9CC8C1CF5E5FB_CustomAttributesCacheGenerator_Line_set_Normal_mD0FB285540CA993D6648EA0A2ECED90968AEF099,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54__ctor_m4DB72EA868575928A2B878DB003FC69EC843CB41,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_IDisposable_Dispose_mD23F1E6AA65C6A057B3CB754453E5EF805A6D1C4,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mDE5152173FE2DDBBBEFBA74FF5D1003DB57329A5,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_Reset_mB94C31961CCC9ED9E0C43BDA9EDAB2C8AEC8168E,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerator_get_Current_m5EDAAE023B18488FB09268F34195242C38E00398,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mDD7964939474EC93D2FE78BE7CC49AFBA792B7FE,
	U3CgetDotVeritcesU3Ed__54_t4E5F0C636F3746F117AFD91555649A05FBEDA99F_CustomAttributesCacheGenerator_U3CgetDotVeritcesU3Ed__54_System_Collections_IEnumerable_GetEnumerator_mDD309DD0E06A30244A07398B3954C6D027892939,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56__ctor_m84E7F14E607A4946C8501AB381A61E9073030C41,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_IDisposable_Dispose_m24807AAE5C8A054491097465175CFE35CD1DEF66,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mD6B1F95D7EB23006D2F8D4AB387FDD8DBB18F306,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_Reset_mB82B5598EA4CE0F46906571B4E8D9DA77BC3B6FF,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerator_get_Current_m8EA14AB9919BD3DD944A510B246A8C9AD1E044BB,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD742CD8DCFAD0905516F7BD5DC1F0EA2BC14A306,
	U3CgetFillVeritcesU3Ed__56_t3E9C83FC5529998D21371DBD9C48E3C2DA2489C0_CustomAttributesCacheGenerator_U3CgetFillVeritcesU3Ed__56_System_Collections_IEnumerable_GetEnumerator_m16CFCBA5318010625AA9C47463B1FDECB73EF4FF,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57__ctor_mEE477D5EAAE9411DFD0492352D6FB42078AC1569,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_IDisposable_Dispose_mC8072E3A4ABA4613032B8D7E71DB91558C416482,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m32AE92CAB16E5053DB5BBCC59C1685CCFF523DE8,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_Reset_mEFF718EC377A4A2E4429F5E1E750125C21D55DB0,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerator_get_Current_m6323FEDAA1579D420AE2EA6FB069041B77EEE869,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m314F4C290F52F681EB509E6551875D62C947F5F4,
	U3CgetLineVerticesU3Ed__57_tD02AC7F7B55E892C3F3708C079E757F284CD10F6_CustomAttributesCacheGenerator_U3CgetLineVerticesU3Ed__57_System_Collections_IEnumerable_GetEnumerator_m8D920BB8D684EEE193B70580501FDEF370F80984,
	CanvasLinesHover_t1537D8594C6ECF102D6A5EC34FFD2EE7BE096863_CustomAttributesCacheGenerator_CanvasLinesHover_getVerices_m7234E00BE8E15C9BEEC7F63445BE89EB20DB7229,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3__ctor_mFB4CB89B833F447622410D7F7A7CCAD4C98D70BF,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_IDisposable_Dispose_m746EFF4FE9C1FFE4D6A7070FB0CA043639F68F45,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_mF499DCABE17E18C34CA602E520367789C8BB8AD9,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_IEnumerator_Reset_m5F6B721252ABAB165C6649E7E398EE50EDE376B7,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_IEnumerator_get_Current_m17C9A5DE1F6758BF4F866CC0DD45A53E9E6D33D1,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mD5851E834D32366D321671E68BFCB0CA31D1DE41,
	U3CgetVericesU3Ed__3_tF0AC4D2B9E1D9B9B81FD43ACAFDA135F24C18EC0_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__3_System_Collections_IEnumerable_GetEnumerator_m76999D741D77F8EB58EFC799D0DBF597C38D6CFA,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_get_refrenceIndex_m74CA55213DF53D00162E676956F11D1311D84DB3,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_set_refrenceIndex_mA31A5548EE2B4B2D2A76FBB107933ADF89B0EA09,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_add_Hover_m975682AE4DF3A55C09E90918447520BC78FB4C84,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_remove_Hover_m83C03EF5E1E66A44170A92CFD02162EDA9F07CE7,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_add_Click_m158BAA2833986B66A6D82E45FC0D81BD82F65289,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_remove_Click_m46285AF60CA8F9CC618C99DD50E77535486D011B,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_add_Leave_m3CEF09D689312D3288058607971942DA3A7E280E,
	EventHandlingGraphic_t65FCA67BDC8A221E2909B20196F2929A33D6B567_CustomAttributesCacheGenerator_EventHandlingGraphic_remove_Leave_mA0C44F388334C72624433637A97EE482B1A7D331,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_get_DefaultIntComparer_mAA3284825DD7E0A551A6AC72786E3AF5704CC917,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_set_DefaultIntComparer_m6C8736F71A022D13C54B0CAE41C075375F0804AE,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_get_DefaultDoubleComparer_m207B0BBC5438E7E4DFA79767A7ADE3057B2F4C29,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_set_DefaultDoubleComparer_m9E82EF5C50F5AEDDAF7CD4D6ABAAE35FDC6E5A7E,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_get_DefaultDoubleVector3Comparer_m3ACF2569E62983F10A1E0C4D60011A10D9997BBA,
	ChartCommon_t32F04C8AEFA2313A6B4FB948A5CC40FF14C8C7D8_CustomAttributesCacheGenerator_ChartCommon_set_DefaultDoubleVector3Comparer_m5E898325C6939EF7226EEFD7FAD89B3FDF7BA915,
	BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_get_Duration_m5771749A0F1AFACA2997F1053A4502EC124BF094,
	BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_set_Duration_m7CE1BAD2AADB23CD5710DDAECAFD59B478952C3D,
	BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_get_StartTime_mABAD31F5ABFF68C526320A9CA287BC29CDC73BAE,
	BaseSlider_t4C797A4CD18F2C3B03407E135403783B86707AC0_CustomAttributesCacheGenerator_BaseSlider_set_StartTime_m837A80AE1654C5A03634E81241483A31285604CA,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_add_DataChanged_m53454856861790BB166459C847049FB880A090C9,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_remove_DataChanged_m7BA12D21F694D295693C39AE35D82029698C03F2,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_add_ViewPortionChanged_mC7F6282B4113B254E79F20BFDEA4E2E735A8AC05,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_remove_ViewPortionChanged_m4F1A28D51F140E9964EAFC4E67761798659D59B3,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_add_RealtimeDataChanged_m0982CDB8E7798B3E606CA10F73634E4868CB64D1,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_remove_RealtimeDataChanged_mDE9BB5466ABAA2D883C6E9726976621D6914CEAB,
	ScrollableChartData_t18226CDE0ACBB0DD80695406CCAD53F4CE604405_CustomAttributesCacheGenerator_ScrollableChartData_U3CUpdateU3Eb__47_0_mE8BFC1F2E09B15FAC7A0D7B71CD96F8B7C2105CE,
	CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_get_WorkOnParent_m903EB04E0D1C9340CF5714BBC942750CF46CAB3F,
	CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_set_WorkOnParent_mB59EB8AF35F52B149CDB79C484084DC7DD101D89,
	CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_get_InitialScale_m407352A2708523959EB60767979C30437DE9B6B7,
	CharItemEffectController_t553B2AB23F0E246E78BBAF3ED376C4D8709EAA5A_CustomAttributesCacheGenerator_CharItemEffectController_set_InitialScale_m90D615D3B6E717032E683EEA0E1A9D7B27B8CD36,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_get_ItemIndex_m4340775567B35B857FCA0AC7F9E145B82BCED8CA,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_set_ItemIndex_m2690B655651FB9CC9E70E2966F476CCE9B072314,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_get_ItemType_m043287B6B47E2BC931B19B9F70D9F40B3EB08A0B,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_set_ItemType_m99ACEB65F5F84DDE284947F6A251D9E3DD8EFD91,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_get_ItemData_m325E440723EC2C55FA58947F2D540423860308BA,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_set_ItemData_mB74438B594B425E6943D0A8E06078CBBC7A24227,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_add_Deactivate_mCED293771E82E147D0842EAF25B06F06810864B7,
	ChartItemEffect_tB9522D9ACE98ACC35EA630A77392A607198E9002_CustomAttributesCacheGenerator_ChartItemEffect_remove_Deactivate_m3A9E4240833B71B8DBB7D2F4F51654FD1DCEFA2B,
	ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_CustomAttributesCacheGenerator_ChartItem_get_TagData_mB309D7BE4D449337386A3F11CC5B1E5461622DA9,
	ChartItem_t4C7F6804CBEB83D0701045F97183A12312296CDD_CustomAttributesCacheGenerator_ChartItem_set_TagData_m6EFCAD89C8BFBC7370A001CCDD1446856CEF193F,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_NameChanged_mB4F42D35E494EA59BB90B4BF2EF48BC82ED63245,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_NameChanged_m842004605E5DF0DC56F89AB01FF165E7BE11DE2A,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_OrderChanged_m308ED8FBFCF9A66C69C96B7DFABC23139D46CB2F,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_OrderChanged_mD9B0980B75467DACF6345834DDE9FCC09ADD429D,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_ItemRemoved_m6F8743B115A784B99758A6DE9BA8A442E31FD17A,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_ItemRemoved_mAD48997C50B4A3C066869F3DC559BC9D6AA1C581,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_add_ItemsReplaced_mF87097C01142F2BFEA12285AC97855EBEFC6423D,
	ChartDataSourceBaseCollection_1_tD33B183CA1993457A4EC35A7E38E477350B1150E_CustomAttributesCacheGenerator_ChartDataSourceBaseCollection_1_remove_ItemsReplaced_m6AEB3E00A48EE2B0FBDBC2C598782CAF888303EF,
	KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_get_Row_m8E5B2536ABA3F7963BED8C00C37AD9DE246E4182,
	KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_set_Row_mA0B59C94D055895B004E338AD361C4228FC37AD0,
	KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_get_Column_m414AFFA313E39B3AEBAAC5B8DEA3A2E483F69999,
	KeyElement_t3F4F761C75BE574FE2C18BAF7BD559B5F69EC6DB_CustomAttributesCacheGenerator_KeyElement_set_Column_mFD48761D666FCDB501D3C5C79F3FCA47DEDF6D8C,
	GraphChart_t1752FD2EB3EACF5A04CAAA7282DC8B1F180B2B7F_CustomAttributesCacheGenerator_GraphChart_U3COnItemLeaveU3Eb__58_0_mA557CB16D6A2BA64937454DEDF3ADD5FECDB6FFF,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Magnitude_m069084EB44CF3477283C3C24DFE227E060139C67,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Magnitude_mE0A9AC6DF6CF9D6532021ED8F40958FE33612C27,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Index_m6E3E1833F3106A2C697659849B4D4DBB3EA3A834,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Index_m779BF98A0027FBF3AE214D4225424BE1465B3330,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_XString_mD8983AC95262E528C933388971991F0ED7FBDB04,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_XString_mBE4A7E79289C442618EFF15D4863A9A764735C27,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_YString_mD55762099B346C4F5F0DB6C742BFE5659DB94074,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_YString_m1E0A09FE20ABD9A34C0361EFF8E294D558475401,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Position_m07FE6D33F9802F166E94A69452252688832F68FE,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Position_m1E11A04C49BB3B49F43F5CC58A11A64234E47E73,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Value_mE3ED67FD7BE31E9EEA4FA9F1C24B3AAEDB363055,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Value_m2AEA5B671D29625A88182451F4D8DE567FD9353D,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Category_m132FB8CA4D23BBD5A6597E4C180168227A203D4A,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Category_m7F525C2F77FBD1DF02655BB29922637BC97CC37D,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_get_Group_mEDE877E6A0267C6C014F063601F3A95873209B3E,
	GraphEventArgs_tD11C07566E3F36C65D20812D77A95941BAF009C5_CustomAttributesCacheGenerator_GraphEventArgs_set_Group_mAF49E4F10D93415BBDB3437597F4A6C03D7FD22A,
	IInternalUse_t29E20E2E4270444070C32C401C3EA43A86F6B265_CustomAttributesCacheGenerator_IInternalUse_add_Generated_mE5D0A14C93F88D9A3FAFD05854322EA5A8A0CCFF,
	IInternalUse_t29E20E2E4270444070C32C401C3EA43A86F6B265_CustomAttributesCacheGenerator_IInternalUse_remove_Generated_m90F87610019DD8F9FFFA7956FFCB046A4B38A58E,
	IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_add_InternalDataChanged_m1E053110E2F3B76E0A0A0AEBA060FCDA8E7CFF06,
	IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_remove_InternalDataChanged_mECE52BC3CCC5486916167DDA3780BFA627EFA181,
	IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_add_InternalViewPortionChanged_mA2DE5EE83DA759D5C4194DC326562ABCAF234E0C,
	IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_remove_InternalViewPortionChanged_m57887986084687152FA36944627A68011D328BEF,
	IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_add_InternalRealTimeDataChanged_m3527EBCFA81EFB7A767DEC20EBE3A61BB8952339,
	IInternalGraphData_tDDE537FBD6E644DCD61E3FE06268A0103EAB0B64_CustomAttributesCacheGenerator_IInternalGraphData_remove_InternalRealTimeDataChanged_m8A079A6E42ED8503D7685C641100DA7881B88776,
	IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_add_InternalOnDataUpdate_m996E2E19D178559A5856C7496EFFEE5D138C1398,
	IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_remove_InternalOnDataUpdate_m55F4229E72756C6B95BE545FCAE67A90BDED728A,
	IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_add_InternalOnDataChanged_mD75E3ADF66DEC948897A2D8A46E9B69C06F0A977,
	IInternalSettings_t95D9EFE91AC309F9C684D402386A4010449E4160_CustomAttributesCacheGenerator_IInternalSettings_remove_InternalOnDataChanged_m698D03A6235E342E4BC2A4E1955AA98B470639C2,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_Orientation_mD1C2C815A175E2BBEA3117ED8EBE1A7932640E87,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_Orientation_m87F834D65E5BED545ED69FE1C608E093643A02FE,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_Offset_m44B3F4D7B3492223D8C915497E194765BBCA2F02,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_Offset_m0A620034DF859A689C768EEC52815E5F1DEB2B00,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_Length_mF24BFEEE513BE43BF890161B869DAF8E5CA23FC6,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_Length_m05073A1B3BEFECD9F86DF14BFB79695824840418,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_get_RecycleText_mBF81839FE404AF6776B99355A86373744038D025,
	ChartMeshBase_t72510CCDDA1F5E74521B33E959E10252BD98BF5C_CustomAttributesCacheGenerator_ChartMeshBase_set_RecycleText_mCFE3B45903FC5BABBCF566195FCED69F637A413B,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_add_OnDataUpdate_mDD4FC4303A073F4D8704C248857730055FAEF249,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_remove_OnDataUpdate_mA808D637C17F5F6E1ABD363FD47CBB39D899D061,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_add_OnDataChanged_mEFABE67C67B63454BF30E194741B5BA2B1F5D2AD,
	ChartSettingItemBase_t56E885A2A5A097545AF27DA2626CF9F2ACAC698C_CustomAttributesCacheGenerator_ChartSettingItemBase_remove_OnDataChanged_m16893529ED8A5D7F8FE580F3107DD651AEEA77DD,
	PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_get_Value_m34E34DB40147B026905D90FF0579DF887371A9FF,
	PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_set_Value_m57932DB099DD42DDEE99D1F87CE10DB0B6C128C3,
	PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_get_Category_m4104152AEE2A00AF85E1E806EDE4936580E24920,
	PieEventArgs_t3AC9DECDF48D6919EE574AB69487434217B6D406_CustomAttributesCacheGenerator_PieEventArgs_set_Category_m1D0F3E34466A4EA1B70186116AF07992FC8D6206,
	PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_PieData_add_ProperyUpdated_m4960F7F4225FF6396F630FDDF953FC61F1E7C15B,
	PieData_t1D56C7E55404D2DE805CA737B802FA633BB8ACA1_CustomAttributesCacheGenerator_PieData_remove_ProperyUpdated_m81A424A95A8D71D68AD29F6C4B7EE4103DFD65ED,
	PieInfo_tDC5F07B5F293ECBB720A8460A8992534FF89EB6B_CustomAttributesCacheGenerator_PieInfo_get_pieObject_mDBF95EBF9EF48A131C00E13C4BBC824636127571,
	PieInfo_tDC5F07B5F293ECBB720A8460A8992534FF89EB6B_CustomAttributesCacheGenerator_PieInfo_set_pieObject_m94FFB57B6A64D14E3C5031D72DA5791AC030CC3F,
	IInternalRadarData_t279BEC1E138A7A0F7B6C2CC18EFB770AF0D2D397_CustomAttributesCacheGenerator_IInternalRadarData_add_InternalDataChanged_mD1A3FC03541A392F90454DDB6D3C504179530175,
	IInternalRadarData_t279BEC1E138A7A0F7B6C2CC18EFB770AF0D2D397_CustomAttributesCacheGenerator_IInternalRadarData_remove_InternalDataChanged_m027EF2D7022C372AC441CE50C785A093419D0921,
	RadarChart_t188A2204B921A66B066EB3BE0EECE2879CCDF8E2_CustomAttributesCacheGenerator_RadarChart_U3COnItemLeaveU3Eb__77_0_mCD5E6245CDA4737B40897F15288E0A869E9F23B7,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Index_m62007451B3AA6C66F095A4C35B8B7B8EC5E484A9,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Index_mA4261CF977A216CAF9751F99FC765CDAA82F8A59,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Category_m9311857293FCD862890CAEC7153DFFF54AC0A15A,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Category_m9A98ADC9A460C6B28C355DFF54D344DEB0BCFBAA,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Group_m0006840DB7F009CC5BE3E17EEEBD03B79AB7526D,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Group_mE1D6C61197C2FB44AC097B9F052595AA93D9D60E,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Value_mB6E37324C906F817C5B2E4755A1CA24293C5DB4D,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Value_mA19E1B1C45A5B260E50606F70636DB311AF02501,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_get_Position_mB4AC59E9FCD59B2EE4E4F20F81778D7DFA6DD9BD,
	RadarEventArgs_tF0B4148D8E46343430D775E389A54DC88A82A623_CustomAttributesCacheGenerator_RadarEventArgs_set_Position_mD64D46B5CEB08F082E4BC6D2B53A714228152CB9,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_add_DataChanged_m99D8EA4C37777EEE904B2472F067B27701E3DE25,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_remove_DataChanged_m1E3D1F74E6E7B12E51D57747E9E72B85111CD90B,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_add_ProperyUpdated_m94ED02CCC412FD825345AE1C6D9D000D3EC14005,
	RadarChartData_tC95304CEEBAD7AE6A5A359059DECA3E8B5005BD1_CustomAttributesCacheGenerator_RadarChartData_remove_ProperyUpdated_mCFABC49D427FE3F42604FE13456B70DFC4B01304,
	RadarFill_tE5CCC550AF849BD6352A5C1CF1959A7FCEDBAD7C_CustomAttributesCacheGenerator_RadarFill_getVerices_m93B92F5B9E70EC783CF194DC2AAAD21F7DC12707,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11__ctor_m95EAA0E125642D785F6170A26A63A985795114EC,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_IDisposable_Dispose_mA707AEC25B6F42499A916D5625F27CC57C387E70,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m0ABBFDCEE2D7145EBE071FCFE6684F6BAFEC3AF9,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_IEnumerator_Reset_mE28A7F9BD47AA43584DAFA502888ACF0235FB4F3,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_IEnumerator_get_Current_m53326F4FE9B852D2151D3849221DE6279144AAFF,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_mFE0741653FF30F25813799307B8423F0B24C7C53,
	U3CgetVericesU3Ed__11_tD766F4A5E93753AC98A18AC484068AFDAFEC2625_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__11_System_Collections_IEnumerable_GetEnumerator_mC4C94D6FA75874BC56F1392AE45985B0A0F0F00E,
	RadarFillGenerator_t9B039EF2C832742FAEA0550A0C4BB5F9E3994CAB_CustomAttributesCacheGenerator_RadarFillGenerator_getVerices_mF7C8A6EBFC00C77EDB871799777143548EC570EF,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9__ctor_mAC242DA1B337EB73A1ACF54B5008C8982895E7BE,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_IDisposable_Dispose_mA5B4335205957942C2C19D8CF215EE867ADE91DA,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumeratorU3CUnityEngine_UIVertexU3E_get_Current_m26748669262E86E2C5B4B00464074D5101846F44,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_IEnumerator_Reset_m2806ECA59982D7E816ABAEE95594A91198113387,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_IEnumerator_get_Current_mEC742CBC4BFF915F4437AFE99135CE5D95A38D2C,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_Generic_IEnumerableU3CUnityEngine_UIVertexU3E_GetEnumerator_m18B10F0B05365FD3906E1B6F7909DDAB07663928,
	U3CgetVericesU3Ed__9_t1CAF56DD99018AEBBBAC1A9B734063F0AA0C12D9_CustomAttributesCacheGenerator_U3CgetVericesU3Ed__9_System_Collections_IEnumerable_GetEnumerator_mE68D27E149F9F1E558455CA8B7C987FB5485CD25,
	ItemLabels_tBA5545E0FFC4D9B10BF17C46057E810D4DD406D9_CustomAttributesCacheGenerator_ItemLabels_U3Cget_AssignU3Eb__5_0_m961E93586578FB887F59CD9628AEC9CE40E8C929,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_add_OnDataUpdate_m2F65466175314C9D5ED7FE225FE1BD80ADBEDBBB,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_remove_OnDataUpdate_mA5F909354E837998B946D9345C788F83D4CDF9B9,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_add_OnDataChanged_m8C1F23C5AD0504F224A32812FA950812A484F1AA,
	TextFormatting_t2F1BD07DD707A3DE7737B1F1C3256D2DB6F3D2E2_CustomAttributesCacheGenerator_TextFormatting_remove_OnDataChanged_m1FCA55929D4EFD9908E2D572D8240F8069F0497F,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_get_UserData_m7366DD757A7150DDD8A9BED5BD84833CF439A79E,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_set_UserData_m68BB91284256A958A1965AD38A7F65970958CFCA,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_get_Material_m884D84994D5DFB145F7F80D646ED047675946B82,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_set_Material_m556B6A8463CE116F5C41CB9A7C82CD866F333A43,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_add_NameChanged_mAB59E2935A5636D41ACCABCA450D3AC205B6BCDE,
	ChartDataItemBase_t2CAEE71CB12044292B7398E90C0E8C6DF46CC580_CustomAttributesCacheGenerator_ChartDataItemBase_remove_NameChanged_m16AFE4B4FF6A7A5DB09EE7E5FAAF7AE835A3E030,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_add_DataStructureChanged_m3DFE5025CFA79F2777ACF301467AEE3227CFCD43,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_remove_DataStructureChanged_m8AAA481D8DF9741946B75D75912507CD4D14179D,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_add_ItemsReplaced_mFA7A223CBFD7512E5E929AE0E0A717DE475AFFBA,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_remove_ItemsReplaced_m8102FBC9B601DCBA582A2DD9B1B9F41D9F21B9FD,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_add_DataValueChanged_m46AFF31FDDA9040F4A0E8FEDBB8AA5F618F72F6B,
	ChartDataSourceBase_t7BE377C55914271BB5CD744ED37C6FDE788199D6_CustomAttributesCacheGenerator_ChartDataSourceBase_remove_DataValueChanged_m2B90A93D01B42AB10A91FA075051E2B995B982DC,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_ItemIndex_m567F892ACB8F7484334FE772FAD6772D47BA3168,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_ItemIndex_m3F4E84261B9A7E33423D30D574AE5CA803D1C070,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_OldValue_m0982C5004334F1A50D175D9FE9164DA2A80F9C45,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_OldValue_m3017E9DAA2BC8F3144A12C5623B7EEABC4F0C32B,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_NewValue_m58B94C77C099C4EE56C72968A6FAD8A45F9A9F5A,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_NewValue_m9560AAB634ECBA050B2023D8CF4F96D05E99ED1B,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_get_MinMaxChanged_m52927FBE8B2DCCE7C6C5F22AA9559C6EDFE56FDC,
	DataValueChangedEventArgs_tC045B5B5FE9A306B02EE40E1D68AF61F9B8811D3_CustomAttributesCacheGenerator_DataValueChangedEventArgs_set_MinMaxChanged_mE75097F31ECA36DF1DED4BD67CF3BEF9CA8E1541,
	IDataItem_t69912D58F7030CB9482018BD1CEC874F026CCD63_CustomAttributesCacheGenerator_IDataItem_add_NameChanged_mD20156002607BC7C7BA54091EB74139BE756EB6C,
	IDataItem_t69912D58F7030CB9482018BD1CEC874F026CCD63_CustomAttributesCacheGenerator_IDataItem_remove_NameChanged_m4D661067FCC994B776B8EDDB0A029C63A06B235B,
	ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_get_Group_mBBD268E7B2942B2B9F44E76CF11463EB2247B92B,
	ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_set_Group_m1FD2ECC0C5E3DE8D62E22CF59AB13BB2C63E940B,
	ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_get_Category_m65B6C653CB412F5D8B8A8E8E34759FE50B81C3A5,
	ChartItemIndex_t8B0F38CB3332AFE28A67C5F920C2727152B29B62_CustomAttributesCacheGenerator_ChartItemIndex_set_Category_mF159D3DEB39D5C73D7ECE7448170A56B9488F5E4,
	ExtensionFilter_t0526FEA4184C326B0C4F04155B5EA8114F2E83EF_CustomAttributesCacheGenerator_ExtensionFilter__ctor_m005AD482A024C782159F183FFD5579DBF4CD7887____filterExtensions1,
	DoubleVector3_t067DE045154E14BD0F85E0054C51AFC9000A776C_CustomAttributesCacheGenerator_DoubleVector3_t067DE045154E14BD0F85E0054C51AFC9000A776C____fwd_PropertyInfo,
	GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA_CustomAttributesCacheGenerator_GraphChartBase_tC38A93E14B9DC032366011D267F7B98DC8616EDA____HeightRatio_PropertyInfo,
	AssemblyU2DCSharpU2Dfirstpass_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void AttributeUsageAttribute_set_AllowMultiple_mF412CDAFFE16D056721EF81A1EC04ACE63612055_inline (AttributeUsageAttribute_tBB0BAAA82036E6FCDD80A688BBD039F6FFD8EA1C * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_allowMultiple_1(L_0);
		return;
	}
}
