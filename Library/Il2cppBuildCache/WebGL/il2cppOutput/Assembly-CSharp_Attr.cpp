﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC;
// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// Zenject.InjectAttribute
struct InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD;
// Zenject.InjectAttributeBase
struct InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7;
// Zenject.InjectOptionalAttribute
struct InjectOptionalAttribute_tAE1940FDC35B6C11EA38885101EF8582FE530BAA;
// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// System.Runtime.CompilerServices.TupleElementNamesAttribute
struct TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// XNode.Node/InputAttribute
struct InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD;
// XNode.Node/OutputAttribute
struct OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585;

IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeType* Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_0_0_0_var;

struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.ExtensionAttribute
struct ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.FlagsAttribute
struct FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Runtime.CompilerServices.IsReadOnlyAttribute
struct IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.ParamArrayAttribute
struct ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// Zenject.Internal.PreserveAttribute
struct PreserveAttribute_t917FF99C161EA919E410C46999AFD6D1A7BFEA27  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SelectionBaseAttribute
struct SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.TupleElementNamesAttribute
struct TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String[] System.Runtime.CompilerServices.TupleElementNamesAttribute::_transformNames
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ____transformNames_0;

public:
	inline static int32_t get_offset_of__transformNames_0() { return static_cast<int32_t>(offsetof(TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD, ____transformNames_0)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get__transformNames_0() const { return ____transformNames_0; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of__transformNames_0() { return &____transformNames_0; }
	inline void set__transformNames_0(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		____transformNames_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____transformNames_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Zenject.InjectSources
struct InjectSources_t1889CC2C5CE4BBB2548A208748288C27F423499C 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t1889CC2C5CE4BBB2548A208748288C27F423499C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// XNode.Node/ConnectionType
struct ConnectionType_t358038D75191873A2DBDBD5C7C4BC6D3D2FC9B41 
{
public:
	// System.Int32 XNode.Node/ConnectionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionType_t358038D75191873A2DBDBD5C7C4BC6D3D2FC9B41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// XNode.Node/ShowBackingValue
struct ShowBackingValue_t39EB6A46698E21FA7FEAC466A96A0C633CEDF296 
{
public:
	// System.Int32 XNode.Node/ShowBackingValue::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShowBackingValue_t39EB6A46698E21FA7FEAC466A96A0C633CEDF296, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// XNode.Node/TypeConstraint
struct TypeConstraint_t131F0C4500E3FBE3F6BDC2191BD581C471782ACF 
{
public:
	// System.Int32 XNode.Node/TypeConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeConstraint_t131F0C4500E3FBE3F6BDC2191BD581C471782ACF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// Zenject.InjectAttributeBase
struct InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7  : public PreserveAttribute_t917FF99C161EA919E410C46999AFD6D1A7BFEA27
{
public:
	// System.Boolean Zenject.InjectAttributeBase::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_0;
	// System.Object Zenject.InjectAttributeBase::<Id>k__BackingField
	RuntimeObject * ___U3CIdU3Ek__BackingField_1;
	// Zenject.InjectSources Zenject.InjectAttributeBase::<Source>k__BackingField
	int32_t ___U3CSourceU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7, ___U3COptionalU3Ek__BackingField_0)); }
	inline bool get_U3COptionalU3Ek__BackingField_0() const { return ___U3COptionalU3Ek__BackingField_0; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_0() { return &___U3COptionalU3Ek__BackingField_0; }
	inline void set_U3COptionalU3Ek__BackingField_0(bool value)
	{
		___U3COptionalU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7, ___U3CIdU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CIdU3Ek__BackingField_1() const { return ___U3CIdU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CIdU3Ek__BackingField_1() { return &___U3CIdU3Ek__BackingField_1; }
	inline void set_U3CIdU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CIdU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7, ___U3CSourceU3Ek__BackingField_2)); }
	inline int32_t get_U3CSourceU3Ek__BackingField_2() const { return ___U3CSourceU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CSourceU3Ek__BackingField_2() { return &___U3CSourceU3Ek__BackingField_2; }
	inline void set_U3CSourceU3Ek__BackingField_2(int32_t value)
	{
		___U3CSourceU3Ek__BackingField_2 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// XNode.Node/InputAttribute
struct InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// XNode.Node/ShowBackingValue XNode.Node/InputAttribute::backingValue
	int32_t ___backingValue_0;
	// XNode.Node/ConnectionType XNode.Node/InputAttribute::connectionType
	int32_t ___connectionType_1;
	// System.Boolean XNode.Node/InputAttribute::dynamicPortList
	bool ___dynamicPortList_2;
	// XNode.Node/TypeConstraint XNode.Node/InputAttribute::typeConstraint
	int32_t ___typeConstraint_3;

public:
	inline static int32_t get_offset_of_backingValue_0() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___backingValue_0)); }
	inline int32_t get_backingValue_0() const { return ___backingValue_0; }
	inline int32_t* get_address_of_backingValue_0() { return &___backingValue_0; }
	inline void set_backingValue_0(int32_t value)
	{
		___backingValue_0 = value;
	}

	inline static int32_t get_offset_of_connectionType_1() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___connectionType_1)); }
	inline int32_t get_connectionType_1() const { return ___connectionType_1; }
	inline int32_t* get_address_of_connectionType_1() { return &___connectionType_1; }
	inline void set_connectionType_1(int32_t value)
	{
		___connectionType_1 = value;
	}

	inline static int32_t get_offset_of_dynamicPortList_2() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___dynamicPortList_2)); }
	inline bool get_dynamicPortList_2() const { return ___dynamicPortList_2; }
	inline bool* get_address_of_dynamicPortList_2() { return &___dynamicPortList_2; }
	inline void set_dynamicPortList_2(bool value)
	{
		___dynamicPortList_2 = value;
	}

	inline static int32_t get_offset_of_typeConstraint_3() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___typeConstraint_3)); }
	inline int32_t get_typeConstraint_3() const { return ___typeConstraint_3; }
	inline int32_t* get_address_of_typeConstraint_3() { return &___typeConstraint_3; }
	inline void set_typeConstraint_3(int32_t value)
	{
		___typeConstraint_3 = value;
	}
};


// XNode.Node/OutputAttribute
struct OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// XNode.Node/ShowBackingValue XNode.Node/OutputAttribute::backingValue
	int32_t ___backingValue_0;
	// XNode.Node/ConnectionType XNode.Node/OutputAttribute::connectionType
	int32_t ___connectionType_1;
	// System.Boolean XNode.Node/OutputAttribute::dynamicPortList
	bool ___dynamicPortList_2;
	// XNode.Node/TypeConstraint XNode.Node/OutputAttribute::typeConstraint
	int32_t ___typeConstraint_3;

public:
	inline static int32_t get_offset_of_backingValue_0() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___backingValue_0)); }
	inline int32_t get_backingValue_0() const { return ___backingValue_0; }
	inline int32_t* get_address_of_backingValue_0() { return &___backingValue_0; }
	inline void set_backingValue_0(int32_t value)
	{
		___backingValue_0 = value;
	}

	inline static int32_t get_offset_of_connectionType_1() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___connectionType_1)); }
	inline int32_t get_connectionType_1() const { return ___connectionType_1; }
	inline int32_t* get_address_of_connectionType_1() { return &___connectionType_1; }
	inline void set_connectionType_1(int32_t value)
	{
		___connectionType_1 = value;
	}

	inline static int32_t get_offset_of_dynamicPortList_2() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___dynamicPortList_2)); }
	inline bool get_dynamicPortList_2() const { return ___dynamicPortList_2; }
	inline bool* get_address_of_dynamicPortList_2() { return &___dynamicPortList_2; }
	inline void set_dynamicPortList_2(bool value)
	{
		___dynamicPortList_2 = value;
	}

	inline static int32_t get_offset_of_typeConstraint_3() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___typeConstraint_3)); }
	inline int32_t get_typeConstraint_3() const { return ___typeConstraint_3; }
	inline int32_t* get_address_of_typeConstraint_3() { return &___typeConstraint_3; }
	inline void set_typeConstraint_3(int32_t value)
	{
		___typeConstraint_3 = value;
	}
};


// Zenject.InjectAttribute
struct InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD  : public InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7
{
public:

public:
};


// Zenject.InjectOptionalAttribute
struct InjectOptionalAttribute_tAE1940FDC35B6C11EA38885101EF8582FE530BAA  : public InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};



// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.ExtensionAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void XNode.Node/OutputAttribute::.ctor(XNode.Node/ShowBackingValue,XNode.Node/ConnectionType,XNode.Node/TypeConstraint,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70 (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * __this, int32_t ___backingValue0, int32_t ___connectionType1, int32_t ___typeConstraint2, bool ___dynamicPortList3, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void XNode.Node/InputAttribute::.ctor(XNode.Node/ShowBackingValue,XNode.Node/ConnectionType,XNode.Node/TypeConstraint,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15 (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * __this, int32_t ___backingValue0, int32_t ___connectionType1, int32_t ___typeConstraint2, bool ___dynamicPortList3, const RuntimeMethod* method);
// System.Void System.FlagsAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229 (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method);
// System.Void Zenject.InjectOptionalAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectOptionalAttribute__ctor_mE1632B355CB0EB10882096FFDE8292A496A09489 (InjectOptionalAttribute_tAE1940FDC35B6C11EA38885101EF8582FE530BAA * __this, const RuntimeMethod* method);
// System.Void Zenject.InjectAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0 (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.ParamArrayAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719 (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void Zenject.InjectAttributeBase::set_Id(System.Object)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline (InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7 * __this, RuntimeObject * ___value0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, int32_t ___order1, const RuntimeMethod* method);
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * __this, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SelectionBaseAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SelectionBaseAttribute__ctor_mDCDA943585A570BA4243FEFB022DABA360910E11 (SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.TupleElementNamesAttribute::.ctor(System.String[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * __this, StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___transformNames0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[0];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[1];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[2];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
}
static void CameraAnchor_t25E643DAE0EB984E3D6CBC24ADF72FBE6D5B5022_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void CameraAnchor_t25E643DAE0EB984E3D6CBC24ADF72FBE6D5B5022_CustomAttributesCacheGenerator_CameraAnchor_UpdateAnchorAsync_m9996C1A34F5DBBCC095E14C8E28FCC087499E4E6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_0_0_0_var), NULL);
	}
}
static void U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5__ctor_mBE52E2E7D91E961713D5BDFA45E653D344A0ABAD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_IDisposable_Dispose_m0BC1DCA1783EB09C527A2F5A8FEB01EC75CB5E78(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BA42456D21130C39DD01A0DBF53EE7C603C2652(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_Collections_IEnumerator_Reset_m0C7A7CE466E3169E5764F963664EF3DB6E5AA4DB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_Collections_IEnumerator_get_Current_mD745BFAE404A38DAAAC6F15F2EA1BCED908C02C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ViewportHandler_tCE17BD0CF0A82CB45D31EC6BE2026445FB737BE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_0_0_0_var), NULL);
	}
}
static void ComplexSymmetricNode_t74D1E6231C91C684AF1C4BE1D4AF7556640BBB07_CustomAttributesCacheGenerator_andConnections(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void ComplexSymmetricNode_t74D1E6231C91C684AF1C4BE1D4AF7556640BBB07_CustomAttributesCacheGenerator_symmetricFragment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t3EB52E86FB2025362BE3829D8B3E885828C7F0BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_t19E47D36ED5E8A9FFA3AE14329E61C89D4666D4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t15451E0426330982363A8EF0840B118034CEC2C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t4AC76BFAC8B8DB351209611254D9F99585403D4D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnumerationNode_tADEDABB643C51B14A75F57771C1A8F6FF75825F9_CustomAttributesCacheGenerator__fragments(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tE6D280ADC4BE70235312ECDCA65E7F361375631D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_enter(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_exit(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_U3CIsEnteredU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_SQLQueryNode_get_IsEntered_m13F26B8AED387BD1388DDA9B44D9CD1FD03CB714(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_SQLQueryNode_set_IsEntered_mECA5F6A406982FDD902B88D730E59F8E70A8A0F4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SimpleNode_tE7C486CEE3E526D36E2597ADA51FC5372A75FCFE_CustomAttributesCacheGenerator_fragment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t022CD8CE5876D03E04D1120CDCA715DB69440BCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tF759D930C1038A45A0EC57EC954E1EFEC5EBE9BC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SymmetricNode_t01252F0107E0B47BBBBC1053BCBD0B9DF7E238C0_CustomAttributesCacheGenerator_firstFragment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SymmetricNode_t01252F0107E0B47BBBBC1053BCBD0B9DF7E238C0_CustomAttributesCacheGenerator_secondFragment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SymmetricNode_t01252F0107E0B47BBBBC1053BCBD0B9DF7E238C0_CustomAttributesCacheGenerator_middleFragment(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Status_t8BB9BF0B27F7DA0CB38BE377390FBB9AE3762854_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 * tmp = (FlagsAttribute_t511C558FACEF1CC64702A8FAB67CAF3CBA65DF36 *)cache->attributes[0];
		FlagsAttribute__ctor_mE8DCBA1BE0E6B0424FEF5E5F249733CF6A0E1229(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tB8038464B6BF24E4D14FDD19976AE14765294B20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x51\x4C\x20\x51\x75\x65\x72\x79\x20\x47\x72\x61\x70\x68"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x2F\x47\x72\x61\x70\x68\x65\x73\x2F\x53\x51\x4C\x20\x51\x75\x65\x72\x79\x20\x47\x72\x61\x70\x68"), NULL);
	}
}
static void SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator__startNode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator__endNode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator__currentNode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator_debugField(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 15LL, 50LL, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERModelNodeGraph_t8B7F06E925A5B043788ADF19524CE738B081F49E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x52\x20\x4D\x6F\x64\x65\x6C\x20\x4E\x6F\x64\x65\x20\x47\x72\x61\x70\x68"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x2F\x47\x72\x61\x70\x68\x65\x73\x2F\x45\x52\x20\x4D\x6F\x64\x65\x6C\x20\x4E\x6F\x64\x65\x20\x47\x72\x61\x70\x68"), NULL);
	}
}
static void ERModelNodeGraph_t8B7F06E925A5B043788ADF19524CE738B081F49E_CustomAttributesCacheGenerator__entryNode(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator__entryPort(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[1];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
		tmp->set_connectionType_1(1LL);
	}
}
static void EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator__enter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[1];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator__exit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[1];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator_context(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EntryNode_tB4E87C7B753BB7B317809DEACCE7246A5178FDF6_CustomAttributesCacheGenerator__entityPort(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[1];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_tE6DAA23C8B41705D399D9E25F0709F79DF2176D2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator__enter(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
		tmp->set_connectionType_1(1LL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator__exit(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
		tmp->set_connectionType_1(1LL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator_outputRatio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator_inputRatio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MainSceneInstaller_t80ECFDA23D95028A0A6A8A14126AAA851FA9392F_CustomAttributesCacheGenerator__provider(CustomAttributesCache* cache)
{
	{
		InjectOptionalAttribute_tAE1940FDC35B6C11EA38885101EF8582FE530BAA * tmp = (InjectOptionalAttribute_tAE1940FDC35B6C11EA38885101EF8582FE530BAA *)cache->attributes[0];
		InjectOptionalAttribute__ctor_mE1632B355CB0EB10882096FFDE8292A496A09489(tmp, NULL);
	}
}
static void MainSceneInstaller_t80ECFDA23D95028A0A6A8A14126AAA851FA9392F_CustomAttributesCacheGenerator__containerOptions(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void MainSceneInstaller_t80ECFDA23D95028A0A6A8A14126AAA851FA9392F_CustomAttributesCacheGenerator__assets(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void ScriptableObjectsProjectContextInstaller_tFD46AA6891F407D8417C6D4AE8F429AF86A38282_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x73\x50\x72\x6F\x6A\x65\x63\x74\x43\x6F\x6E\x74\x65\x78\x74\x49\x6E\x73\x74\x61\x6C\x6C\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x73\x74\x61\x6C\x6C\x65\x72\x73\x2F\x53\x63\x72\x69\x70\x74\x61\x62\x6C\x65\x4F\x62\x6A\x65\x63\x74\x73\x50\x72\x6F\x6A\x65\x63\x74\x43\x6F\x6E\x74\x65\x78\x74\x49\x6E\x73\x74\x61\x6C\x6C\x65\x72"), NULL);
	}
}
static void ScriptableObjectsProjectContextInstaller_tFD46AA6891F407D8417C6D4AE8F429AF86A38282_CustomAttributesCacheGenerator__assets(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScriptableObjectsProjectContextInstaller_tFD46AA6891F407D8417C6D4AE8F429AF86A38282_CustomAttributesCacheGenerator__containerOptions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_frameRange(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_U3CAverageFPSU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_U3CHighestFPSU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_U3CLowestFPSU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_get_AverageFPS_mEEA62F4A79543C4FE1475AFC36854F2171C58C1F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_set_AverageFPS_m30C6572E854B2FC04B7FC86E7A47C6085982179F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_get_HighestFPS_mF9706AD4FA4FA6C0B807BA74A9CF4671E78E97D1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_set_HighestFPS_mD2B5447252682EE2776399CA7D4248CF8D0B7B6E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_get_LowestFPS_m09E86470F62541368824DC67877BB59BD62D053F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_set_LowestFPS_m7CB4C3FF06A5E0F895E334961825214FF17FFC91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_U3C_ctorU3Eb__15_0_m0BEE9FA9172C5DF92C766EE1011733FD34CF92D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FPSDisplay_t53F5BA4921803909BE8686CFD37DC0D3A93CAFF9_CustomAttributesCacheGenerator_isShow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ComponentUtility_t342E2DC5ED7FFF72D9DE83F854929378A603286A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ComponentUtility_t342E2DC5ED7FFF72D9DE83F854929378A603286A_CustomAttributesCacheGenerator_ComponentUtility_GetComponentNullException_mA7D3C2A60ECEB30F9A03918A60A074A58ABDE85E(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ComponentUtility_t342E2DC5ED7FFF72D9DE83F854929378A603286A_CustomAttributesCacheGenerator_ComponentUtility_GetComponentInChildrenNullException_mE9267BDAAD7B3DC7385F9CA9491F1FE84BFBA2DD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t7EBE2FE8040582909A81D38BCA3FD0C249A90BCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t7EBE2FE8040582909A81D38BCA3FD0C249A90BCE_CustomAttributesCacheGenerator_Extensions_SetActive_m504313DF682759CF548ADD0BC7F50DDF62B85723(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void Extensions_t7EBE2FE8040582909A81D38BCA3FD0C249A90BCE_CustomAttributesCacheGenerator_Extensions_SetRaycastTarget_m3B8D2EA28AD8E7BAFE69BDDC76497FFEDBEE9A13(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator_CameraExtensions_GetMouseWorldPosition_m2B117497423533E11575488FDE12BE8850BB70D4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator_CameraExtensions_ConvertWorldToCanvasPosition_m93B3C353C415D959F119E336574AB0DEAF892613(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator_CameraExtensions_ConvertCanvasToWorldPosition_m9000219ECAC96C10E5E2880479046DE20F0F3FC1(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_GetSize_mEAAD4D2793DBD62F56A8BF154DB66FF1CCB2A753(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_GetWorldRect_m1C9CD5FF9F753CDEC448C60141B8BC06B69E0288(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_GetWorlRectRelativeCameraSize_mCD81C276C743D4E0CAF3D115A13DD15C2DEF93A7(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpSizeDelta_m2A9E5ACEB91867C94DF44C2D22102339BE24E1C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_0_0_0_var), NULL);
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[1];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpRectSizeDelta_m39C65E725A62C41731F1B4E4450438B2675D9D02(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_0_0_0_var), NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpAnchoredPosition_m0C926F16516AA68BD53AFA2992AF4DD742802C4B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_0_0_0_var), NULL);
	}
}
static void RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpRectAnchoredPosition_mAAB08EFDE563F14BEFCDEA573393155FCC5C6188(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass3_0_tA34FF1DD369994CB019C61B1920FDDFA275DDF3B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_CustomAttributesCacheGenerator_U3CLerpSizeDeltaU3Ed__3_SetStateMachine_m7444DFAFDD7E0F1C8509B9FC48973843EB6A72D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4__ctor_mF1888E43C25E604566E283BB989E80D70D50D9C6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_IDisposable_Dispose_mB00AD5311FE89E7940B8B6719928483B21609CC9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C83DCB8260D0D446E68BE1427BA4720BEECEA7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_Collections_IEnumerator_Reset_m8427AD15687BE5354CFB80803CE38823B9F35D09(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_Collections_IEnumerator_get_Current_m363CB8532ADCFA86E6CA5973CE4821654F9ADEEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t954CDC21B4CC0E870BBA3A9A6C3600FF6AE786A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_CustomAttributesCacheGenerator_U3CLerpAnchoredPositionU3Ed__5_SetStateMachine_m61FC70772C264B875E62E4D1F4DE711A0DA3AEFD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6__ctor_mFE9C09660DED78EA23598CABC1E5D57F4D567616(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_IDisposable_Dispose_mE05334D602CD581DA1652952E4D42E1E326E1C9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27E78D509F15357448603159979E21A28734F5AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_Collections_IEnumerator_Reset_mBD5D41D6A1E243541E88C3FDD8F26BF72841FD21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_Collections_IEnumerator_get_Current_mE7AFA3560DBDD05C198156E4D09663D5D398D919(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator_ImageExtensions_LerpColor_m62FC6D0894E971E19AEAC1E0BCC0A2FBB4E610E7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_0_0_0_var), NULL);
	}
}
static void ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator_ImageExtensions_LerpImageColor_m80BB6A014D89313752CC70AEBFC28AEF363A0E8F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_0_0_0_var), NULL);
	}
}
static void ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator_ImageExtensions_LerpAlpha_m150FD3150DCA35A8116C44E6370A4615F67C006F(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t372DD3C76BDBA25C7FAE65ECB4D4564403D759B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_CustomAttributesCacheGenerator_U3CLerpColorU3Ed__0_SetStateMachine_m3E4DCC6CD3A4BACA7C419035B957E3EDDA17D9B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1__ctor_m7082FB6805788880EB1F5690764D35CC4D23A69B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_IDisposable_Dispose_m65E403F86AA083D75BACC561BAB5B4A0A45FD730(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63391AF6FC954A45D7D0CF2FFD7118D021AD3E40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_Collections_IEnumerator_Reset_mA68E70FBE44DD75DBD49A373BDF2645191EC7761(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_Collections_IEnumerator_get_Current_m7486836D1F223C28318C45F1C22DE8BF31A8349D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VectorExtensions_tB2198A25EAA37F28D7E233AB2445C545B21DA352_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_tB2198A25EAA37F28D7E233AB2445C545B21DA352_CustomAttributesCacheGenerator_VectorExtensions_Approximately_m6D21476F6B52E413A78E8B75B4C096841DBFA911(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void VectorExtensions_tB2198A25EAA37F28D7E233AB2445C545B21DA352_CustomAttributesCacheGenerator_VectorExtensions_Approximately_m6019DED3F59E93DB41296D5D9188C6588D00FB79(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator_StringExtensions_WithoutWhiteSpace_mF2F95631E719204E888DB0F3F4525995E10085F4(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator_StringExtensions_WithoutWhiteSpaceAndNewLine_mE29AB05C3967FE4B3C55802CFA21EF2E10060889(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator_StringExtensions_WithoutNewLine_mE635EB68415D012EC33402A2EE715C9C80E77DFD(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TaskExtensions_t78223F1FBEE2223BA86E2DD4A4CE46FF0D83170C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void TaskExtensions_t78223F1FBEE2223BA86E2DD4A4CE46FF0D83170C_CustomAttributesCacheGenerator_TaskExtensions_CancelUnity_mCF92A94C84EC3B7E3716799993F973C9201377F4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_0_0_0_var), NULL);
	}
}
static void U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_CustomAttributesCacheGenerator_U3CCancelUnityU3Ed__0_SetStateMachine_m00470DD2D9A34297A4ED534E774C4D6F21FA6AFC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Has_mDB6F620256C9D647F43F8717D77ED79F9C42A271(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Is_m4768AA265AC5CC4522C16B6303031BFB6E4ADD07(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Add_mC40753F35FF13B2E9174E2D60080375C965E682A(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Remove_m878211BAD2875CE1FD7978B4A3E56202AC50A162(CustomAttributesCache* cache)
{
	{
		ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC * tmp = (ExtensionAttribute_t917F3F92E717DC8B2D7BC03967A9790B1B8EF7CC *)cache->attributes[0];
		ExtensionAttribute__ctor_mB331519C39C4210259A248A4C629DF934937C1FA(tmp, NULL);
	}
}
static void ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_mFDEB62E2BFF17A7732FDB0A5AE90650F9AE77380(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_0_0_0_var), NULL);
	}
}
static void ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_mFDEB62E2BFF17A7732FDB0A5AE90650F9AE77380____buttons0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_m937A9E1E10427CADEF8494BF60E49F18719A9AE9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_0_0_0_var), NULL);
	}
}
static void ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_m937A9E1E10427CADEF8494BF60E49F18719A9AE9____buttons1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_U3CButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_U3CIsClickedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_get_Button_mFDFA434D21266FF80FE663BF106FCA3E8F1141B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_set_Button_m75A7B009F280F723F77FB7B7FDEF6117D9F472CB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_get_IsClicked_mC2D2B28D84F05A1DCCA8D77D89F12B88A2D8A6F9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_set_IsClicked_m60934C9B8C324AED25EEB76FB1065D12F9CAB7CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_CustomAttributesCacheGenerator_U3CTrackU3Ed__0_SetStateMachine_mD61A88711D2C54D458397DA51936A6DD2E176276(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_CustomAttributesCacheGenerator_U3CTrackU3Ed__1_SetStateMachine_m4FC89EAD355BD026515EB17BF3645D9024978CF5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_TaskContext_1_get_Data_m53ECF9A23083055A7A43604293BF73821B4C216F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_TaskContext_1_set_Data_m53B0B9C0E9B727650702F77FF938B90CB49CF44B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_TaskContext_1_CancelAwaitFrame_mB102339A88EF6823BA6BB38A5C55F6D9AB3F24FE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_0_0_0_var), NULL);
	}
}
static void U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_CustomAttributesCacheGenerator_U3CCancelAwaitFrameU3Ed__10_SetStateMachine_mD064F16CC9808BE617D27F23F709A1CE6BEA4713(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UnitySerializedDictionary_2_tA69318E009DEDC312E6E1116C3A40A337CC00471_CustomAttributesCacheGenerator_keyData(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitySerializedDictionary_2_tA69318E009DEDC312E6E1116C3A40A337CC00471_CustomAttributesCacheGenerator_valueData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void InputFieldERFragment_t8F5BDE9AF7D6C3CE1F763CF9F6FDC372496BEE4E_CustomAttributesCacheGenerator_InputFieldERFragment_Construct_mA7D0821C515E974CDB0760C21D27DD6AA344692D(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void InputFieldLowerSpace_t8592D6DA91D1D375DB3CDE1D18E822C3502956A5_CustomAttributesCacheGenerator_InputFieldLowerSpace_Construct_m8D3D8125CFFB48AEDDA207706EA39BE53180BB0B(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void GroupSwitchButtons_tCAFE9670185628FC84348AD9CC140E163230D100_CustomAttributesCacheGenerator_GroupSwitchButtons_Construct_mA2909FDCA45B1D458FA72E148465EB86CEFCAEAC(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t30452C0E2D9E7F5663CEB1EC263E83624C30853F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SwitchButton_t9363131C8BBB13C3288D35C9AC856A38C42A5946_CustomAttributesCacheGenerator_SwitchButton_Construct_m0046AE5A2D987808A31F5C026F0C0B26F3A200D7(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TextButton_tAF2485C054CEC742789066DA22F2CC7E493D9ADD_CustomAttributesCacheGenerator_TextButton_Construct_mEE94EB47F2E69F18711B3FBBEF95281CD29F6F81(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator__loweredRectTransform(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator__openImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_timeAnimationMove(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_timeAnimationDissolve(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Construct_mB3B5EF6C4A161814828A05C9FB66009F1E4F7929(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Construct_mB3B5EF6C4A161814828A05C9FB66009F1E4F7929____scaler0(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x76\x61\x73"), NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Open_m9A335DD4CEEA162F8548E2A0B63901CF50703921(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_0_0_0_var), NULL);
	}
}
static void Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Close_mFCEC7E8A2C203CAFD008B7D7FEFA6C8E81769C33(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_0_0_0_var), NULL);
	}
}
static void U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_CustomAttributesCacheGenerator_U3COpenU3Ed__7_SetStateMachine_m5C63B446232F882B11C4BD6D90BA347AC24C5C79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_CustomAttributesCacheGenerator_U3CCloseU3Ed__8_SetStateMachine_mA9BB57103AD1F16470A85A04BA91178BF187CB19(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InteractiveZone_tF2FA3D885AEFBA87CA8361A6F800CB98E8A7D712_CustomAttributesCacheGenerator_InteractiveZone_Construct_mDC9566F1634F8A1F834302F1690F6010563B52DF(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void InteractiveZone_tF2FA3D885AEFBA87CA8361A6F800CB98E8A7D712_CustomAttributesCacheGenerator_InteractiveZone_Construct_mDC9566F1634F8A1F834302F1690F6010563B52DF____rectTransformCanvas0(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x76\x61\x73"), NULL);
	}
}
static void Chart_t8304A8D9D3A855E57406A433FF9DDC1184C8F899_CustomAttributesCacheGenerator_Chart_Construct_m8A26C885C19C1B691896392227A97B0977197425(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__creditBook(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__result(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__ratingAndOption(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__time(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PreviewWindow_tEA593855B275850C60DB7D9F47A0C6B4DF450829_CustomAttributesCacheGenerator__board(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void PreviewWindow_tEA593855B275850C60DB7D9F47A0C6B4DF450829_CustomAttributesCacheGenerator__chart(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__firstTaskPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__secondTaskPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__thirdTaskPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__fourthTaskPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator_ReportWindow_Construct_m5C033787D9434858CA84FE600FE67E7271010F9A(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator_ReportWindow_Show_m3457717E1D7CE56DD8D6E4DF56CBF422DBFB8DA1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_0_0_0_var), NULL);
	}
}
static void U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_CustomAttributesCacheGenerator_U3CShowU3Ed__8_SetStateMachine_m8ACF4C0E04A9AF97A4215D3A63E87613915330BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TaskPanel_t88933EAD57CAC4DE2885BF873901D7EB421723D4_CustomAttributesCacheGenerator_Description(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TaskPanel_t88933EAD57CAC4DE2885BF873901D7EB421723D4_CustomAttributesCacheGenerator_Score(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TaskPanel_t88933EAD57CAC4DE2885BF873901D7EB421723D4_CustomAttributesCacheGenerator_MaxScore(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MessageClickedMenuButton_1_t0796B4502DB586CDA1EFDD47BBC2BA5778F7E36B_CustomAttributesCacheGenerator_U3CButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageClickedMenuButton_1_t0796B4502DB586CDA1EFDD47BBC2BA5778F7E36B_CustomAttributesCacheGenerator_MessageClickedMenuButton_1_get_Button_m0E21AD273F23C0C814AAB4AB09E957D13CF96C8F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageClickedMenuButton_1_t0796B4502DB586CDA1EFDD47BBC2BA5778F7E36B_CustomAttributesCacheGenerator_MessageClickedMenuButton_1_set_Button_m804451BBE46BC193E4B24AC2A5B0C434D41FED92(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500____FIOInputField0(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x46\x49\x4F\x20\x49\x6E\x70\x75\x74\x20\x46\x69\x65\x6C\x64"), NULL);
	}
}
static void AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500____groupInputField1(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x6F\x75\x70\x20\x49\x6E\x70\x75\x74\x20\x46\x69\x65\x6C\x64"), NULL);
	}
}
static void AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500____creditBookInputField2(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x65\x64\x69\x74\x42\x6F\x6F\x6B\x20\x49\x6E\x70\x75\x74\x20\x46\x69\x65\x6C\x64"), NULL);
	}
}
static void AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Show_m206BDF648DE2A716E59E9643A62A2288A0F46F72(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_0_0_0_var), NULL);
	}
}
static void AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_U3CShowU3Eb__7_0_mFA836FDA1FA98E6D0C628A754D8F9351B016EF9B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CFIOU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CCreditBookU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CIsCancelU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_FIO_m2504011F37B75ECDEF1E68E77F36A09AA63B6C40(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_FIO_mC61D6CC8F6CA0C4A4DAB4B4DFA856CFE5B577CDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_Group_m90C61F525E0B414BF47C1D77794E7C1C44D7E92D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_Group_m357A8DD7354CF384A11CEEB9084376492762A320(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_CreditBook_mD58C2FD9DBEC03B46B77CF0BA57AC1DC13F83AE1(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_CreditBook_mE713E8686EE294BA011CDAB946A7C1B137673667(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_IsCancel_m408FCFE481EEA26FD37106B23D248EC80511D1AE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_IsCancel_m3334A239B6D3443C01AD70A5D8D0F9101DEA9C37(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_CustomAttributesCacheGenerator_U3CShowU3Ed__7_SetStateMachine_mDDF440CB75DE305ABD1D96544612D40911DB90D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator__warningLabel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_type(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_U3CIsCompleteU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_Init_mCC93046BBA6FF74C8680AF0AF35FD16617198A6C(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_get_IsComplete_m76961F1349F916503E8758B6E093DB53DF580938(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_set_IsComplete_m91AC9403F0A2E9B026CA4B7ECA9E6F24283FEF01(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_Construct_mA7108BD5FCA7AC70D3AB38D6EFE5F32978971C2F(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_U3CConstructU3Eb__12_0_mFCE7E43DEADE54248DBD659C8461447D60D45F09(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Form_t3416818ABF401D25D26A122A78364F8E828A12E1_CustomAttributesCacheGenerator_Form_Construct_m37907F759BED7FE7A8CF69837AA9DDC2B2180969(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void OptionNumberForm_tE69ECEA57544513BB96B87F956974B92406B99B2_CustomAttributesCacheGenerator__number(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OptionNumberForm_tE69ECEA57544513BB96B87F956974B92406B99B2_CustomAttributesCacheGenerator_OptionNumberForm_Init_m231E5A8EFA0F8FD0D7F2217B34B330091377ECBD(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void OptionNumberForm_tE69ECEA57544513BB96B87F956974B92406B99B2_CustomAttributesCacheGenerator_OptionNumberForm_Show_m4ADEA2D60FA145203E7F96974656FC0A1CA60DF6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_0_0_0_var), NULL);
	}
}
static void U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_CustomAttributesCacheGenerator_U3CShowU3Ed__3_SetStateMachine_mE708EDB48840703C57AE986D9F4F6CCB8D8071D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SessionRecoveryForm_t7815D4D8A3C226A7F5BBD773DAFB5C9109CD7AB9_CustomAttributesCacheGenerator__userData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SessionRecoveryForm_t7815D4D8A3C226A7F5BBD773DAFB5C9109CD7AB9_CustomAttributesCacheGenerator_SessionRecoveryForm_Construct_m155487A59D4FA76B48F14867CDB7649B10CF1679(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void SessionRecoveryForm_t7815D4D8A3C226A7F5BBD773DAFB5C9109CD7AB9_CustomAttributesCacheGenerator_SessionRecoveryForm_Show_m136AF7B73B125894E9AE44C15960E3B77BEBBFEB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_0_0_0_var), NULL);
	}
}
static void U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_CustomAttributesCacheGenerator_U3CShowU3Ed__4_SetStateMachine_mD2E737A532624B5AFED689C61278B3E1611FED75(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TaskForm_t59E39BE010706B19371DE5D694949B7721B0A3A3_CustomAttributesCacheGenerator_TaskForm_Init_mAA34F4AA02C274D4BCDBA30091FCC17DA5A774AD(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TaskForm_t59E39BE010706B19371DE5D694949B7721B0A3A3_CustomAttributesCacheGenerator_TaskForm_Show_m2849FDB66EFAD54C0C2CF0CCF0F47A870B38C31C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_0_0_0_var), NULL);
	}
}
static void U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_CustomAttributesCacheGenerator_U3CShowU3Ed__2_SetStateMachine_mF26F66655CF628E79C98B605F26A0A60DEF70B59(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_U3CStartTestButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_U3CInfotTestButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_U3CExitTestButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_get_StartTestButton_m428E4A5CD47E1265E76D58BC0A846030D0C8C2B2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_set_StartTestButton_m512BFD084DFC17A37C76996CE81730D9F66711E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_get_InfotTestButton_mA8D53D5C773625286BFCDC5FEFE7CFB735ED599F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_set_InfotTestButton_m9BBBEF2C4EC85B213455E993A1C0E17EB1B7E0C1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_get_ExitTestButton_m87500BCEE5BD25F5E8C3304BF137917102C9AE82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_set_ExitTestButton_m9401EC3D50032A4C399D9AFD5F31D830E51C5A07(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PreviewWindow_t7F38D8081E43EB5B14CE1ECC57E5000AFF2990E2_CustomAttributesCacheGenerator_PreviewWindow_Construct_m2C7A66FF642F0DA084B63E9945E3B6A9A9C7B64D(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void ConfirmationWindow_tE139526D6BB92EEA8C0A6D0D943783FD293F62F1_CustomAttributesCacheGenerator_ConfirmationWindow_Construct_m35EAC04470E9CD87BE46B9AD1FDE72E72C6FF2B2(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void ConfirmationWindow_tE139526D6BB92EEA8C0A6D0D943783FD293F62F1_CustomAttributesCacheGenerator_ConfirmationWindow_Show_m827376CAFE9B30A0A53817B2138B6FFC1773111E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_0_0_0_var), NULL);
	}
}
static void U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_CustomAttributesCacheGenerator_U3CShowU3Ed__5_SetStateMachine_m0829C12D4EDFACC0D4D17500984389EC178D4137(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ExitCompletionButton_t713DCD480F78111AF1A77FA9A59B2F14ADF42655_CustomAttributesCacheGenerator_ExitCompletionButton_Construct_mA7BEBAE0F537415AD07F0BD5D1F9F194E04CA879(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void ExitCompletionButton_t713DCD480F78111AF1A77FA9A59B2F14ADF42655_CustomAttributesCacheGenerator_ExitCompletionButton_Construct_mA7BEBAE0F537415AD07F0BD5D1F9F194E04CA879____window0(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x69\x74\x20\x54\x65\x73\x74"), NULL);
	}
}
static void ExitCompletionButton_t713DCD480F78111AF1A77FA9A59B2F14ADF42655_CustomAttributesCacheGenerator_ExitCompletionButton_HandleClick_m4DA3F62DBE28FD0A66501F7E77C2E91B27D7C7DC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_0_0_0_var), NULL);
	}
}
static void U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_CustomAttributesCacheGenerator_U3CHandleClickU3Ed__2_SetStateMachine_mDD5A481BB6353371B51AC1833025146A27919BE3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator__outInputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator__inInputField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator_timeAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator_FormCreatingRelationship_Construct_m0328465B4F2344B40F774FFAB35B5BEBCDB5D2F4(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator_FormCreatingRelationship_Open_m9E7312830DF74F522DBCFC6DF6770ADB18AD60A1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_0_0_0_var), NULL);
	}
}
static void U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_CustomAttributesCacheGenerator_U3COpenU3Ed__13_SetStateMachine_mA2077041EF90D3B2442A55A743FF1BA10DC124CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Factory_t91CDCF6D0AF81908EBF3642A02FB83327A4BE974_CustomAttributesCacheGenerator_Factory__ctor_mF2F6BB5C1AA028892CB1A25DEBDB0E31CE89FC13____lockRectZone2(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x6F\x63\x6B\x20\x52\x65\x63\x74\x20\x5A\x6F\x6E\x65"), NULL);
	}
}
static void Factory_t91CDCF6D0AF81908EBF3642A02FB83327A4BE974_CustomAttributesCacheGenerator_Factory_Create_m88729AD160719FE67807D222D50FFAA5F4698E53____childrens0(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void Factory_t91CDCF6D0AF81908EBF3642A02FB83327A4BE974_CustomAttributesCacheGenerator_Factory_U3CCreateU3Eb__7_0_m41D6B95E1B7208C71E9844AD7417B3C50E1A5406(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskCompletionButton_t2EA5D9AD9670001FA421E9248D9DE7A1FE9FDBC5_CustomAttributesCacheGenerator_TaskCompletionButton_Construct_mF7CB4D0316029FB9ACC887FAB7A926D9E324AA39(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TaskCompletionButton_t2EA5D9AD9670001FA421E9248D9DE7A1FE9FDBC5_CustomAttributesCacheGenerator_TaskCompletionButton_Construct_mF7CB4D0316029FB9ACC887FAB7A926D9E324AA39____window0(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6D\x70\x6C\x65\x74\x69\x6F\x6E\x20\x54\x61\x73\x6B"), NULL);
	}
}
static void TaskCompletionButton_t2EA5D9AD9670001FA421E9248D9DE7A1FE9FDBC5_CustomAttributesCacheGenerator_TaskCompletionButton_HandleClick_m04A5805ABE604C6D992BE9C8DDA82DD8CC9F65B1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_0_0_0_var), NULL);
	}
}
static void U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_CustomAttributesCacheGenerator_U3CHandleClickU3Ed__3_SetStateMachine_m952ADFA6A041BD5FFEBAC8D153F3B211623DEA6C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WarningWindow_t2136D2CED21EF93ECC920B35811B8EF4B2F2E507_CustomAttributesCacheGenerator__description(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WarningWindow_t2136D2CED21EF93ECC920B35811B8EF4B2F2E507_CustomAttributesCacheGenerator_WarningWindow_Construct_mEE65B5C6D253FDAB06BAFD2A220A1C04663B55C3(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void WarningWindow_t2136D2CED21EF93ECC920B35811B8EF4B2F2E507_CustomAttributesCacheGenerator_WarningWindow_Show_mFA5E54FB0D6771AEA6394044E43F70310CDF6A33(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_0_0_0_var), NULL);
	}
}
static void U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_CustomAttributesCacheGenerator_U3CShowU3Ed__8_SetStateMachine_m2A972ED1FBE5932E44D69AF5B571E79B1EC7CEBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WindowSQLCode_tF4BD109D056CA977F157568FC1E4715E6BECC84D_CustomAttributesCacheGenerator__tittle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WindowSQLCode_tF4BD109D056CA977F157568FC1E4715E6BECC84D_CustomAttributesCacheGenerator__input(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WindowSQLCode_tF4BD109D056CA977F157568FC1E4715E6BECC84D_CustomAttributesCacheGenerator_WindowSQLCode_U3CInitializeU3Eb__3_0_m532C633C60357CB6C723E5C47D00F1BE6E4D1A53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EntityFormationLine_t51AB013F5E0C823A0CB07B0D15EEDA9A3A2A7779_CustomAttributesCacheGenerator__nameEntity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass6_0_tC4E15A154CB155879C3E9FC29701D51510F9A96C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EntityFormationList_t59AFE1AEC07057CDB545F0C2A09F7C270186F1CB_CustomAttributesCacheGenerator__factory(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void EntityFormationList_t59AFE1AEC07057CDB545F0C2A09F7C270186F1CB_CustomAttributesCacheGenerator_EntityFormationList_ValidatInputFieldEdit_m736CA5B2874160C5807F225AC734B430144DEF50(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_0_0_0_var), NULL);
	}
}
static void U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_CustomAttributesCacheGenerator_U3CValidatInputFieldEditU3Ed__5_SetStateMachine_mA0818812A8D5E76815B6A78F3F4D446F4E85DE33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__nameRelationships(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__parentEntity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__connectingEntity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__typeConnection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RelationshipsFormationList_t04B7F444E66B53E451A6E51931B64367B8339825_CustomAttributesCacheGenerator__factory(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator__key(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator__image(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator_SelectableLine_get_Value_m367D2123D20977BEADDEA073392328932E2852B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator_SelectableLine_set_Value_m518FC2A427835FC571FF5ACAC41B9EC82D45E07B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator__tittle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_timeAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_Construct_m455B01837B83327EB01804EFB234265AF9C9D7A3(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_Open_mC9DBC445090E0D12678D893FCE36D9CFF7150594(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_0_0_0_var), NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_Open_m7F01FB408F7E5991E20DD6D245912046BEF915A8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_0_0_0_var), NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_ShowForm_mB7F16E3F66B41E2E0527EA416E09F1D31C38C179(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_0_0_0_var), NULL);
	}
}
static void SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_UnshowForm_mF2EDD534498E1AB1C81A377B85565470E545B364(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_0_0_0_var), NULL);
	}
}
static void U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_CustomAttributesCacheGenerator_U3COpenU3Ed__15_SetStateMachine_m25A241FB41C376614FCD476190A1B6CB2EBE79D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_CustomAttributesCacheGenerator_U3COpenU3Ed__16_SetStateMachine_m59AA724DFA457754DB1F833125844D69F97D9294(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_CustomAttributesCacheGenerator_U3CShowFormU3Ed__20_SetStateMachine_m87A5D22CC65A9AE807EEE2AC4F7F1404F88FF7A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_CustomAttributesCacheGenerator_U3CUnshowFormU3Ed__21_SetStateMachine_mFE9ECC8AF67BA48403D6E5792E960519C1F06994(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator__factory(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator__tableEditor(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator__taskForm(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_Open_mB0E053E52F57D51D6345A19CE4DCAF3EF08D49C9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_0_0_0_var), NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_ShowForm_m6AF23B2B689AF89F4563EFE15943B0CDCFDDC1DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_0_0_0_var), NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_UnshowForm_m8A0443BDF4E1BC2531B304E7C795EF77260E2B5A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_0_0_0_var), NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_ValidateInputFieldEdit_mC1BACEE6ACF7579E7B10AD08A28EE350A0C2D8DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_0_0_0_var), NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_EditTable_m5008FC449A2DBFFFFFA1F3D1CACB1190403F98E0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_0_0_0_var), NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_U3CU3En__0_m1527926A736012D2A7E1AD50F8606423D82E109A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_U3CU3En__1_m345FE8EB766F048293479D3EB813E9BCDD981D18(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[1];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_CustomAttributesCacheGenerator_U3COpenU3Ed__7_SetStateMachine_m2D9A7B58AFE3934EC736E2B95A5ABDDECC6B0AB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_CustomAttributesCacheGenerator_U3CShowFormU3Ed__8_SetStateMachine_m1010B25F136ED30D96229B56183D6BA3352143FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_CustomAttributesCacheGenerator_U3CUnshowFormU3Ed__9_SetStateMachine_mECB0EC6F69A5076F394E9A32B1796262EB54D3E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_CustomAttributesCacheGenerator_U3CValidateInputFieldEditU3Ed__15_SetStateMachine_mED2BD6C534A2DA7DE851AD529F651CD8012FF482(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t16B7F9B42E6B4EB0C3F5E22EF3FE6DB3114E743A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_CustomAttributesCacheGenerator_U3CEditTableU3Ed__16_SetStateMachine_m51CF6C38FF19AE177D344CED7CBA5C84ED9A1F89(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TableListLine_tBA4908DCFCD9802C0246A4AA20D8B7E6A4E60231_CustomAttributesCacheGenerator__editButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableListLine_tBA4908DCFCD9802C0246A4AA20D8B7E6A4E60231_CustomAttributesCacheGenerator__nameTable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass8_0_t072DB55594034415BC7E5EF8C93FF1E27A39E47A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_U3CFieldsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_get_ID_m84339BB129C41BF33D1AE4F32B4DA701784C4CF1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_get_Name_m86FC6ACF48682C6B8B58B77B00B08012F349AACD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_set_Name_mD0275CE51CEB81D57EFB131A4BD7CCEACF7B49DF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_get_Fields_m2BD42217ECD4307793F3C1261724F0E9A5889994(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_set_Fields_m0AAAA9660E23BE990EC4C6A077002FA27224B450(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator__factory(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_Construct_m37651B457C7851C779A880EE800B6D6D36A593C7(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_Open_mD5CAA7687CDD73BC4DC75B5313FFBBA1A64A67FC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_0_0_0_var), NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_EditTypeField_m4E94185E2C6FF1170393A8F8CCC7D7750827C79E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_0_0_0_var), NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_ValidateInputFieldEdit_m2AE7C83ABB4CBF14F102892918CEFD2BBBB7012E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_0_0_0_var), NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_SelectLinkingTable_m9B1473FF979AC3DA3B31D887D92E4D045999A9F0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_0_0_0_var), NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_SelectLinkingField_m8695B74B1D59E6CA3ACA23028E4501E9E016453D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_0_0_0_var), NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_0_m8FCDE10EE5069B91D2DB7A530C5ACD3F5BA16ECA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_1_m355F5E3B921F08C270F38CA88988740F3D8A90D5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_2_mBE1A3EBECB1509D97B2C91E4503157EE28018CE1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_3_mCA863663C36DF56A51BC3665640671F938235DEB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_4_m0138D64D4304D189F7BADE2703FB6F057E4FF95C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_5_m50BD05C35ADE68A86B7A7BF08AAE15B3415A8CA0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_6_m718ED9CAC1F03139395D85C3497C409F30D2C46D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_CustomAttributesCacheGenerator_U3COpenU3Ed__13_SetStateMachine_m7AA4935C448CFE3C3A2E27F62ACCDC58CF52E1AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_CustomAttributesCacheGenerator_U3CEditTypeFieldU3Ed__19_SetStateMachine_m1F82D70EDC986EC5565E9C7DBD4D39DD04F3D036(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_CustomAttributesCacheGenerator_U3CValidateInputFieldEditU3Ed__20_SetStateMachine_m1ED2683C1C1A15446C21CF04098529DD7777C618(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_CustomAttributesCacheGenerator_U3CSelectLinkingTableU3Ed__21_SetStateMachine_m1E662E4F606310BD5B785782F4A409BDF488A5DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_tC7E5C6AFB35CDD2355B3AD82C2831A3D1205AE48_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_CustomAttributesCacheGenerator_U3CSelectLinkingFieldU3Ed__22_SetStateMachine_m33413D8E19FD6392429F2566C7C9EAC7A8F4E79A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__nameField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__typeField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__primaryKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__secondaryKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__linkedTable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__linkedField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass27_0_t462AF001F282A2694D8ACB0E7BD04604F124A348_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CIsPrimaryKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CIsSecondaryKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CLinkingTableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CLinkingFieldU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_ID_m6B489923A70B2506C41D85C1A3311186763FA210(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_Name_mFE5D46C6C8413B6D477E238EA696707B979B069C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_Name_m9A85EB4A5AA5AAD79F030F7293A5DCDED0A0EB97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_Type_mCB18134F60D382F6AEC72173F6BA97055F1A493C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_Type_m29EFE5A4BAD118BC97D0EA5DBE2590685D5DA809(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_IsPrimaryKey_mD0C966804822EC294109A9F9705912829FCE17E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_IsPrimaryKey_mF42468E54065ED9FEAF7163DC940AA8B00DB7E23(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_IsSecondaryKey_m40701D30049DEBC67B0A18E4D8B93E6F25B1E877(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_IsSecondaryKey_m0B73B5DD85E2BC1F3E3F87C0A4F8A38E3894D30F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_LinkingTable_mBDA3F6647F37703726061453893DAAD2FD8B9B5A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_LinkingTable_m27C3614D3CF8234D6155F84ADE91AFD2617110DE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_LinkingField_m042A69A76301BFF079F3108681C6309591C11FDD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_LinkingField_m80D3A9AC11F10A33E4A9088B6F1D2C5425F9F159(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UILine_1_tB207B12975E12A9D8FCD95A727FAC8FDD4225360_CustomAttributesCacheGenerator__number(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UILine_1_tB207B12975E12A9D8FCD95A727FAC8FDD4225360_CustomAttributesCacheGenerator__removeButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UILine_1_tB207B12975E12A9D8FCD95A727FAC8FDD4225360_CustomAttributesCacheGenerator_UILine_1_U3CInitU3Eb__13_0_mA87881ADED7ABB18BF6D1291A1653520D92BFB7F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator__addButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_timeAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Construct_mB3AA87ADC7F476047383EF551EC599B4EAE3B225(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Open_m99E3F13499142CC72A5003A6ED00F7429E78EA6C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_0_0_0_var), NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Open_mB7C4270E070F1A8A3CD55553582A7A0DC9874104(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_0_0_0_var), NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Open_mB7C4270E070F1A8A3CD55553582A7A0DC9874104____additionalUIElements1(CustomAttributesCache* cache)
{
	{
		ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F * tmp = (ParamArrayAttribute_t9DCEB4CDDB8EDDB1124171D4C51FA6069EEA5C5F *)cache->attributes[0];
		ParamArrayAttribute__ctor_mCC72AFF718185BA7B87FD8D9471F1274400C5719(tmp, NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_ShowForm_m01B205097E38695C9217478F274BB6ECEAD065DE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_0_0_0_var), NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_UnshowForm_m36C070914FE40DBF2697FEFDA89CDA24BF9C96B7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_0_0_0_var), NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_U3COpenU3Eb__17_0_m3974A1B0FCC88D408532ADD1AB4DF9C24B040FF5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_U3COpenU3Eb__18_0_m7D397DC660D578C5C30E54E226AABDA7DB5FF288(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_CustomAttributesCacheGenerator_U3COpenU3Ed__17_SetStateMachine_mDD912E2482C42889E6C4F99F5EEB6FBBCEB78BA1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_CustomAttributesCacheGenerator_U3COpenU3Ed__18_SetStateMachine_m568C6E912253EDA7583F5772822C51C03A77EB2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_CustomAttributesCacheGenerator_U3CShowFormU3Ed__28_SetStateMachine_m0FA23A91FA0E623050BF9C9350D927E3133DE41C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_CustomAttributesCacheGenerator_U3CUnshowFormU3Ed__29_SetStateMachine_m0EA2077B412C2DB9E12C985684E9FF6FA4C71937(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GroupToolbarButtons_t655F33EFB05BECDBEDB2BA73FD6C4BF5C8938289_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x54\x6F\x6F\x6C\x62\x61\x72\x42\x75\x74\x74\x6F\x6E\x73"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void Toolbar_t726FFA0787ED4F3EABAE3F369C31446515E2BCC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x54\x6F\x6F\x6C\x62\x61\x72"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void Toolbar_t726FFA0787ED4F3EABAE3F369C31446515E2BCC0_CustomAttributesCacheGenerator__groups(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Toolbar_t726FFA0787ED4F3EABAE3F369C31446515E2BCC0_CustomAttributesCacheGenerator_Toolbar_Construct_mF4C5F6EA0A73156BDBCA4D14808BD8DB49DBC837(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void CreatingEntityShapeButton_tE5676158C3C09EAFBAA1035B62EC9AED1BB459BA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x46\x69\x72\x73\x74\x20\x54\x61\x73\x6B\x2F\x43\x72\x65\x61\x74\x69\x6E\x67\x45\x6E\x74\x69\x74\x79\x53\x68\x61\x70\x65\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void CreatingRelationshipShapeButton_t7C6A7206A061063064968D2C4174E928CD207526_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x46\x69\x72\x73\x74\x20\x54\x61\x73\x6B\x2F\x43\x72\x65\x61\x74\x69\x6E\x67\x52\x65\x6C\x61\x74\x69\x6F\x6E\x73\x68\x69\x70\x53\x68\x61\x70\x65\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void EntityFormationButton_t4FF9052C5F5654F12A8A87C26356D679FC74B999_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x46\x69\x72\x73\x74\x20\x54\x61\x73\x6B\x2F\x45\x6E\x74\x69\x74\x79\x46\x6F\x72\x6D\x61\x74\x69\x6F\x6E\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void FormingRelationshipsButton_t50F627C2A13BEC794F45470120FBC93423D7B837_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x46\x69\x72\x73\x74\x20\x54\x61\x73\x6B\x2F\x46\x6F\x72\x6D\x69\x6E\x67\x52\x65\x6C\x61\x74\x69\x6F\x6E\x73\x68\x69\x70\x73\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void ChartDisplayButton_tE6CC8E60C4F7809E64194DB29F78F49FE7841A1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x46\x6F\x75\x72\x74\x68\x20\x54\x61\x73\x6B\x2F\x43\x68\x61\x72\x74\x44\x69\x73\x70\x6C\x61\x79\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void TableDisplayButton_tAEC69EC229B07B2A59794A9EA844EF1F298C3C03_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x53\x65\x63\x6F\x6E\x64\x20\x54\x61\x73\x6B\x2F\x54\x61\x62\x6C\x65\x44\x69\x73\x70\x6C\x61\x79\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void TableManagementButton_tD6B0C5E15B6D4E09A1A8E2FCD2D557637878C1AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x53\x65\x63\x6F\x6E\x64\x20\x54\x61\x73\x6B\x2F\x54\x61\x62\x6C\x65\x4D\x61\x6E\x61\x67\x65\x6D\x65\x6E\x74\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void CodeResetButton_tCD7A51CBD963C5017A0169A3AE7F04CCBB139A09_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x47\x72\x6F\x75\x70\x20\x54\x68\x69\x72\x64\x20\x54\x61\x73\x6B\x2F\x43\x6F\x64\x65\x52\x65\x73\x65\x74\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void HelpButton_tB992ED5639C666B19F42ACBF5B15CBEA4BA96212_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x48\x65\x6C\x70\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void TaskButton_tCCE941BCF58129CE5C291BE4BE1985D2119E05F5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x6F\x6F\x6C\x62\x61\x72\x2F\x54\x61\x73\x6B\x42\x75\x74\x74\x6F\x6E"), 0LL, NULL);
	}
}
static void SwitchableToolbarButton_1_t69E0B51732DD040B2A65E9AE579C5BD49896945D_CustomAttributesCacheGenerator_SwitchableToolbarButton_1_Construct_m9AD58EFE04505DABE978154EAA6562458789F08F(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void MessageClickedToolbarButton_1_t76A8B45531D30A3F952606209C0E0FDC7BFFB184_CustomAttributesCacheGenerator_U3CButtonU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageClickedToolbarButton_1_t76A8B45531D30A3F952606209C0E0FDC7BFFB184_CustomAttributesCacheGenerator_MessageClickedToolbarButton_1_get_Button_mE788B5DB8F524FF172A88E657D7D2034606C9DDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MessageClickedToolbarButton_1_t76A8B45531D30A3F952606209C0E0FDC7BFFB184_CustomAttributesCacheGenerator_MessageClickedToolbarButton_1_set_Button_m93A2166AAFF859FCC41A0B80C268248DEA09F8B3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Tab_tB0931206C740359C158CEA6CB0A0AA8183933A8C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x61\x73\x6B\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E\x54\x61\x62\x62\x61\x72\x2F\x54\x61\x62"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void Tab_tB0931206C740359C158CEA6CB0A0AA8183933A8C_CustomAttributesCacheGenerator_Tab_Construct_mECEDE8A800B396EA73336E474FDE656AC7881B81(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TabAngle_t3E766F09877E8B321471F1AFECEBE5CD6E7A7D43_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x61\x73\x6B\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E\x54\x61\x62\x62\x61\x72\x2F\x54\x61\x62\x41\x6E\x67\x6C\x65"), 0LL, NULL);
	}
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[1];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
}
static void TaskSelectionTabbar_t621920382F0975D019FEDDDD6DE034EA98D441F8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x61\x73\x6B\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E\x54\x61\x62\x62\x61\x72\x2F\x54\x61\x73\x6B\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E\x54\x61\x62\x62\x61\x72"), 0LL, NULL);
	}
}
static void TaskSelectionTabbar_t621920382F0975D019FEDDDD6DE034EA98D441F8_CustomAttributesCacheGenerator__dictionary(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x61\x73\x6B\x48\x65\x6C\x70\x46\x6F\x72\x6D\x2F\x46\x6F\x72\x6D"), 0LL, NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator__description(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_timeAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_Construct_m9B880246098E6A6A5BED80DA09F83C558E3BF3BD(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_ShowWithFocus_m0BA8D93C5093E28B200BA2BEED522D8CBAD69590(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_0_0_0_var), NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_Show_mA1589E371CB45CB67C3864E16639D76DE1E4B518(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_0_0_0_var), NULL);
	}
}
static void Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_Unshow_mEB967B933E57BD9BB5FAAB901FF3C81762F83319(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_0_0_0_var), NULL);
	}
}
static void U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_CustomAttributesCacheGenerator_U3CShowWithFocusU3Ed__12_SetStateMachine_m1DEDC57D06D255EEE40EB8297644CAB3BE1B52F4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_CustomAttributesCacheGenerator_U3CShowU3Ed__13_SetStateMachine_m70BC076CB022F813ECEEBE83981309221B9437E7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_CustomAttributesCacheGenerator_U3CUnshowU3Ed__14_SetStateMachine_m1A37AF8BC0657BA2FC806793BB50579BAED8FAA3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E * tmp = (DisallowMultipleComponent_tDB3D3DBC9AC523A0BD11DA0B7D88F960FDB89E3E *)cache->attributes[0];
		DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D(tmp, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56(tmp, il2cpp_codegen_string_new_wrapper("\x43\x49\x54\x47\x2F\x55\x49\x2F\x4D\x61\x69\x6E\x20\x53\x63\x65\x6E\x65\x2F\x54\x61\x73\x6B\x48\x65\x6C\x70\x46\x6F\x72\x6D\x2F\x46\x6F\x72\x6D\x50\x6C\x61\x63\x65\x68\x6F\x6C\x64\x65\x72"), 0LL, NULL);
	}
}
static void FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator_FormPlaceholder_Initialize_m813F3879487C69CFF7FFCB85E5E91C92CFFFB5A1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_0_0_0_var), NULL);
	}
}
static void FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator_FormPlaceholder_U3CSubscribeToMessagesU3Eb__9_0_m1B25DFF772C47D048C776CD56BA67B51DCE06839(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_0_0_0_var), NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator_FormPlaceholder_U3CSubscribeToMessagesU3Eb__9_1_m7D3EED2C9734E1A25DE69B8B3988518BC9C3025E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_0_0_0_var), NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_CustomAttributesCacheGenerator_U3CInitializeU3Ed__6_SetStateMachine_mE1AECB03CDDE715730DF7773D95C396CC483C7AE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_CustomAttributesCacheGenerator_U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_SetStateMachine_mB2FE8828F3A7B82985DF6D9853845C9312B24CB9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_CustomAttributesCacheGenerator_U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_SetStateMachine_m224FE3F394078E076C249ACEFA6525FA595E1696(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void HelpForm_t277349E855034C5307CD7BCCB68F658922DC7BD6_CustomAttributesCacheGenerator_HelpForm_Construct_m2CABFE1CC344B7BC24BFEBF754E6B2145689BB7F(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void HintProvider_t95194E80FF0E5443BE06F4DE67514D1BD0B7099C_CustomAttributesCacheGenerator_HintProvider_DisplayHint_mFA6B792222965194337FDCF17DEF722399725B7C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_0_0_0_var), NULL);
	}
}
static void HintProvider_t95194E80FF0E5443BE06F4DE67514D1BD0B7099C_CustomAttributesCacheGenerator_HintProvider_StartAnimation_m8D88CB848914E12ABF8986A6E64066678559A543(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_0_0_0_var), NULL);
	}
}
static void HintProvider_t95194E80FF0E5443BE06F4DE67514D1BD0B7099C_CustomAttributesCacheGenerator_HintProvider_ChangeScale_m2841AEAC53F9044178FB679703A92E68FB30095D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_0_0_0_var), NULL);
	}
}
static void U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_CustomAttributesCacheGenerator_U3CDisplayHintU3Ed__3_SetStateMachine_m0DC25574E8EDD8FBF3697B909D08843ED803399D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass4_0_t3A95ECB6781BAFF18AA1F24E4495F4633D10CE4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_CustomAttributesCacheGenerator_U3CStartAnimationU3Ed__4_SetStateMachine_m46397A5DFC151EF869F043A6125E79B116C40E31(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5__ctor_m0715FD638864704901AAF0EEB5AE631DC5994402(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_IDisposable_Dispose_mB6FF7DA598646E9BC99CBCC3F32EC42714DEC7F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF19E5CF7AC91B863A4E129FD6E966FF4A2EACC99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_Collections_IEnumerator_Reset_m89D39062590CA66F649E5646E9656736BDBBDFE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_Collections_IEnumerator_get_Current_m66A5E06443CC82EF0A58A9FCED6927CCD395ECF3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageFirstTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageSecondTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageThirdTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageFourTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TaskForm_t31DA1AE62CDD2C7ACA54FDAE4BD4F6C6E27E8844_CustomAttributesCacheGenerator_TaskForm_Construct_m3BCBD75652453F89D36599055BC0E333EDFBC113(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void TaskManager_tC3B8E946B702E831442C905DA0D11259E18DA5FA_CustomAttributesCacheGenerator_TaskManager_U3CSubscribeToMessageU3Eb__11_0_m1CDD6BC1385CD44BC9465610B820D7B3BE4629E0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskManager_tC3B8E946B702E831442C905DA0D11259E18DA5FA_CustomAttributesCacheGenerator_TaskManager_U3CSubscribeToMessageU3Eb__11_1_mACFB2BE36A61F75DFF84F31106C0AE4209D5F545(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskManager_tC3B8E946B702E831442C905DA0D11259E18DA5FA_CustomAttributesCacheGenerator_TaskManager_U3CCompleteTestingU3Eb__15_0_mCF0632A138D4BD6331BFFCA4CB7B110064AE2817(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ThirdTask_t0AEB00122F9344731C7DB0DAB6BD69B911C03FEB_CustomAttributesCacheGenerator_ThirdTask_U3CInitializeU3Eb__6_0_mE86743699D637A90F4B73B21D330894DF7F06549(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tDB681ED161303E52235E7CD560F6701C814AB095_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t0D4B52F942C27EDBE4171670E1595A963CF19FD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t85B7E2EEB988EE5B94C1B27D6968B47DBC4A7BC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tDA6D3D7033896068AA90D65806DEF5922E5F49DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskDebugStateMachine_t1F83FE3E14F5D72A120EA7DE00FBE4E36CBA0B32_CustomAttributesCacheGenerator_U3CCurrentStateU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskDebugStateMachine_t1F83FE3E14F5D72A120EA7DE00FBE4E36CBA0B32_CustomAttributesCacheGenerator_TaskDebugStateMachine_get_CurrentState_mF58CB79F74A7DE6ECBBBA752B3E4815E96D8E7AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TaskDebugStateMachine_t1F83FE3E14F5D72A120EA7DE00FBE4E36CBA0B32_CustomAttributesCacheGenerator_TaskDebugStateMachine_set_CurrentState_mD5E8470ECFF91596FC3C63099A211C28F117661B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Startup_tD9CDCC99E10BA274A878E7BAA33661601A581368_CustomAttributesCacheGenerator_Startup_Initialize_m79F0FEF23261E4A2B406F1DCFA9ABDE03486B921(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_0_0_0_var), NULL);
	}
}
static void Startup_tD9CDCC99E10BA274A878E7BAA33661601A581368_CustomAttributesCacheGenerator_Startup_StartTest_m66AF9579D7F9ECEF9519CB3C35064DC107278C56(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass5_0_t3EA6BEBF4DBC59490A3AAFC311BBDA48DCA0A3D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_CustomAttributesCacheGenerator_U3CInitializeU3Ed__5_SetStateMachine_m8481F4F01CD81C43007A6B8E673752B69346129F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_CustomAttributesCacheGenerator_U3CStartTestU3Ed__7_SetStateMachine_m48DB0560207121B43A8E9AA5717836AC5E02E4A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_tF3D7EF628FB05EA86200C2006D504731BB5B86ED_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_U3CReportU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_U3CDateOpenU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_U3CTypeLastCompletedTaskU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_get_Report_mB6457B7C7508A226095757EE37A6E2D4171F0C97(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_set_Report_m048AB0F59A9C190ECBFB40EDF82AEED661284AF0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_get_DateOpen_mC3DFA41FEC23AE827EA4ACB643E2C215ACB1E090(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_set_DateOpen_mB75DA6BDB1CA777820D617C8FD4E9104DEE6209F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_get_TypeLastCompletedTask_m8F51FD58B16B47CCD62450ADD58B4B57D2C51F44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_set_TypeLastCompletedTask_m80CC35975CD36197EF33C0FA1C2693CC0F21BB11(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t44D97896938755F82E023D4937115F1F1889694A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator__reportButton(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x70\x6F\x72\x74\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator__saveButton(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x76\x65\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator__exitButton(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x69\x74\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager__ctor_m58A00C4FAB87908EE2422847DBF62E760F2B0B41____reportButton0(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x70\x6F\x72\x74\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager__ctor_m58A00C4FAB87908EE2422847DBF62E760F2B0B41____saveButton1(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x76\x65\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager__ctor_m58A00C4FAB87908EE2422847DBF62E760F2B0B41____exitButton2(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x69\x74\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager_ShowReport_m192E756997C5F86B8F6A43DD5C22FC524E6BA9F8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_0_0_0_var), NULL);
	}
}
static void U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_CustomAttributesCacheGenerator_U3CShowReportU3Ed__8_SetStateMachine_mF89C01371E9C74732EE224A7AD721E42B5D4811B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager__ctor_mB70C09BE42C5142DDB029A516D3CAE1E91481F97____reportButton3(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x70\x6F\x72\x74\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager__ctor_mB70C09BE42C5142DDB029A516D3CAE1E91481F97____saveButton4(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x76\x65\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager__ctor_mB70C09BE42C5142DDB029A516D3CAE1E91481F97____exitButton5(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
		InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline(tmp, il2cpp_codegen_string_new_wrapper("\x45\x78\x69\x74\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager_ShowReport_m82AAF8B941D8209BC4A6E66F60D47C6DA05BF1C4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_0_0_0_var), NULL);
	}
}
static void U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_CustomAttributesCacheGenerator_U3CShowReportU3Ed__11_SetStateMachine_m64B60D2F27D412000EBC02E03E17CE618D21FA63(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_DisplaySessionRecovery_mFB88B1A4662408536B1D883573A7E007A2350C65(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_0_0_0_var), NULL);
	}
}
static void Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_OpenRegistration_mA671753110D6512E161BC4339654EF1BC7244C73(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_0_0_0_var), NULL);
	}
}
static void Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_ShowTask_mAD563FEEEBCFEC90F43969AAB700D51B5282F5C2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_0_0_0_var), NULL);
	}
}
static void Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_OpenGlass_mA1C1C072E1E900D54FA97E127965EB347413442E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_0_0_0_var), NULL);
	}
}
static void Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_CloseGlass_m15F3B84D4228AA4648D5E908DC37A5DCB39F9AA5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_0_0_0_var), NULL);
	}
}
static void Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_U3CDisplayU3Eb__10_0_m4CE1F6B68AF2761A163CD5DFF34B47AB8F58E93B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_CustomAttributesCacheGenerator_U3CDisplaySessionRecoveryU3Ed__9_SetStateMachine_mA4D56E1345425B5A401D117D3C056C8DB277A05B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_CustomAttributesCacheGenerator_U3COpenRegistrationU3Ed__11_SetStateMachine_mA3A20FD1D38023E0F64A5CD1C56656865B33DAAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_CustomAttributesCacheGenerator_U3CShowTaskU3Ed__12_SetStateMachine_m89686BDB9A31B966F1335A339B8117FCBAE57268(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_CustomAttributesCacheGenerator_U3COpenGlassU3Ed__13_SetStateMachine_m9ED18D3957D0525610AD0F55FF69A72436882CC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_CustomAttributesCacheGenerator_U3CCloseGlassU3Ed__14_SetStateMachine_m1ED0482EDB3E3EE584B7B9DC38A4486384C22C21(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_U3CTotalScoreU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_U3CTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_get_TotalScore_m89ACA46685656CBEF81165B0BBA00130FCB0A802(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_set_TotalScore_m06C1E9C4F531C3479253B4B3F661B9DC16F30081(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_get_Time_m0E3527840B8F3F18F69254A941F0672F30D25910(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_set_Time_mD4FBD3C9749731A772AD9FAE6314E0707716F347(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CFIOU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CCreditBookU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CNumberOptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_FIO_m98367C2563560CFDB69AADBCCC12E4E97F48D113(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_FIO_m36A8729A541D23D9D4E37AC62097746C8A27F95D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_Group_m82C88E637186D07E6BD0E1EE2D39CF9F2273E1EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_Group_mC5CC388E0BB1C8A1E48806B60CD673A3625E5564(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_CreditBook_m73746C8880091B9485391EB38CAE6A9C9C5C46AC(CustomAttributesCache* cache)
{
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[0];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_CreditBook_m9952D73F94A2E7D2DEC708A5537DB1D3FB4A1896(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_NumberOption_mDE5A790E11E24ECE872AC436E4239B7EE058BC73(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 * tmp = (IsReadOnlyAttribute_tB6E31A0106212818B0AB6DC627AA320291BD7566 *)cache->attributes[1];
		IsReadOnlyAttribute__ctor_m02F9F7CD56DE227F7ABDD1E2593D82E6FFA57E9A(tmp, NULL);
	}
}
static void UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_NumberOption_m61D573E3713F5A9FBDE16C3FBF4EFA9354F7D461(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ContainerOptions_t599769CAEFEEADA3DC532845DED463607D4EDD85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x4F\x70\x74\x69\x6F\x6E\x73"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x20\x4F\x70\x74\x69\x6F\x6E\x73\x2F\x43\x6F\x6E\x74\x61\x69\x6E\x65\x72\x20\x4F\x70\x74\x69\x6F\x6E\x73"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 51LL, NULL);
	}
}
static void ContainerOptions_t599769CAEFEEADA3DC532845DED463607D4EDD85_CustomAttributesCacheGenerator__options(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_t63094766CF22F62AC9E77058CDD8CED4DED9BFCE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x20\x4F\x70\x74\x69\x6F\x6E\x73\x2F\x4F\x70\x74\x69\x6F\x6E"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 51LL, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_number(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator__taskDescriptions(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_erModelVerificationData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_tableVerifications(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_visualSQLQueryDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator__SQLCodeDatas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec_t595904110144D09022625ED4D6FC690579397A5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisualSQLQueryData_tE1E50FCA239D62ECBD9FF1FA2D4001201AC6DFD3_CustomAttributesCacheGenerator_Description(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 5LL, 10LL, NULL);
	}
}
static void SQLCodeData_t3DF6C789953B5DABF481C6DCC29E425B7F65E068_CustomAttributesCacheGenerator_FinalCode(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 10LL, 20LL, NULL);
	}
}
static void SQLCodeData_t3DF6C789953B5DABF481C6DCC29E425B7F65E068_CustomAttributesCacheGenerator_StartCode(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 10LL, 20LL, NULL);
	}
}
static void TaskDescription_tA37CC2402EF4FBD6604F07CAA4D4A84A6747E38C_CustomAttributesCacheGenerator_TaskText(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 10LL, 20LL, NULL);
	}
}
static void TaskDescription_tA37CC2402EF4FBD6604F07CAA4D4A84A6747E38C_CustomAttributesCacheGenerator_HelpText(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 10LL, 20LL, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x65\x74\x73"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x79\x73\x74\x65\x6D\x2F\x41\x73\x73\x65\x74\x73"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 51LL, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_inputFieldAuthorizationFormSprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__settingsMainScene(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__helpDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_spriteTableSwitchableToolbarButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_taskSelectionTabSprites(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_spritesSwitchButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__switchButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__lockRect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableERFragmentPrefabs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableERTransitionPrefabs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__fragmentTableDiagram(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__lineFragmentTableDiagram(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__connectionTableFragmentDiagram(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__chainTableFragmentDiagram(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__entityFormationLinePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__relationshipsFormationLinePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableEditorListLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableEditorLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__selectableLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SettingsMainScene_t8D74C65C3190D9C49689AB148BA8F1BC81E9FE04_CustomAttributesCacheGenerator_firstActiveTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SettingsMainScene_t8D74C65C3190D9C49689AB148BA8F1BC81E9FE04_CustomAttributesCacheGenerator_onDebug(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SettingsMainScene_t8D74C65C3190D9C49689AB148BA8F1BC81E9FE04_CustomAttributesCacheGenerator_defaultNumberOption(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpriteTableSwitchableToolbarButton_tBB0DDB77CCE13C46BB1F56EBD2AC0877B1D95ABE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void SpriteTableSwitchableToolbarButton_tBB0DDB77CCE13C46BB1F56EBD2AC0877B1D95ABE_CustomAttributesCacheGenerator__list(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t5DD87F75BAE74A65AFB8C8618EBCE1203F703B4E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableERFragmentPrefabs_t7B58AA9E6F1979E8D4DB82ED941D9F3129E041D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void TableERFragmentPrefabs_t7B58AA9E6F1979E8D4DB82ED941D9F3129E041D1_CustomAttributesCacheGenerator__list(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t7AF7586DFACE13E902E808E09FE4156DBB16677F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableERTransitionPrefabs_t8FB58C470A273C18931A20567DF56800265069B8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void TableERTransitionPrefabs_t8FB58C470A273C18931A20567DF56800265069B8_CustomAttributesCacheGenerator__list(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t9F7D7FAF49CF7B48B79DCB4899D367F551911CD0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionFirstTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 20LL, 20LL, NULL);
	}
}
static void DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionSecondTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 20LL, 20LL, NULL);
	}
}
static void DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionThirdTask(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 20LL, 20LL, NULL);
	}
}
static void DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionFourTask(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 20LL, 20LL, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_U3CCurrentTimeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_get_CurrentTime_mDFF26B5F38FF289BBD69FEF831F783687DFE5C85(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_set_CurrentTime_m3983821472F7F97EA35982BCBE57E8347443EDDD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_Update_m95BC1D186A1D6B31F7888D9C5F507F7B94F0CA01(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_0_0_0_var), NULL);
	}
}
static void StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_U3CStartU3Eb__8_0_mB498A8A1E00B0ADD4DD86D30230E36D73D08AEEE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t77796F10BC5E754166BEA7BD420425C694443D7C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11__ctor_m3D432530ED9CC09263EF3CD72CE25CFA22A8635D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_IDisposable_Dispose_m0706329A81C59168D6C15399603C4537C9D6FCCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0512EB602BE3C8C1AEB0BBF2BF1CF6FBA06EFCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_Collections_IEnumerator_Reset_m8D09BEB60F2456035598BB01BDBE4361154577B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_Collections_IEnumerator_get_Current_mF333E020EBE2B9003161602F064B93D1A8589CF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DragAndDropBehaviour_t5DBCAA66242378E7DA320AA1BA07C9F95C93D2BD_CustomAttributesCacheGenerator_DragAndDropBehaviour_Construct_m19507CF26310A7316AE0E59FD160C1600302B214(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void DragAndDropController_tDD314C6D9F4F7C98A8CE7652E6CC2FDE98835784_CustomAttributesCacheGenerator_DragAndDropController_U3CSetMoverU3Eb__13_0_m0064C3D8B1F5410909E0A52ADA8D4C7FB55FB592(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DragAndDropController_tDD314C6D9F4F7C98A8CE7652E6CC2FDE98835784_CustomAttributesCacheGenerator_DragAndDropController_U3CHandleMouseButtonDownU3Eb__16_0_m3B95A053B0762C12CFFF81C08A5744CCAAFC151E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB * tmp = (SelectionBaseAttribute_tDF4887CDD948FC2AB6384128E30778DF6BE8BAAB *)cache->attributes[0];
		SelectionBaseAttribute__ctor_mDCDA943585A570BA4243FEFB022DABA360910E11(tmp, NULL);
	}
}
static void SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_scaleModifySelecting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_scaleModifyReviews(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_overlapBoxSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_SQLQueryFragment_Construct_m47D32346D5B88A992B0DF1BC5150069E0BB9DE35(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_U3CCurrentFragmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_SQLQueryFragmentPlace_get_CurrentFragment_mA8FD811A108ED4EF2999840A6A62504D594E5F3E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_SQLQueryFragmentPlace_set_CurrentFragment_m68F24AEE02B1778012074471D788DD05CF873255(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_SQLQueryFragmentPlace_Construct_mF3A3FFAF62F3F9B3BE7E7D97F59A4C13313C9A5A(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void VisualSQLQuery_t88AA278C6F062184A4FFAD1EC91F25824261DA32_CustomAttributesCacheGenerator_U3CDescriptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisualSQLQuery_t88AA278C6F062184A4FFAD1EC91F25824261DA32_CustomAttributesCacheGenerator_VisualSQLQuery_get_Description_mECCD38AE3DABB7C7B276D17B8C8C28E090664287(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisualSQLQuery_t88AA278C6F062184A4FFAD1EC91F25824261DA32_CustomAttributesCacheGenerator_VisualSQLQuery_set_Description_m73667C4D4FE12C5C0FBC6AC042B573095B320646(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t830EC8B38E9D4E68D1A557F0789DE06631DC9DA3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator__description(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator_VisualSQLQueryGroup_Construct_m2CD326CAE657C888795DD9B3D031CA719C3C7448(CustomAttributesCache* cache)
{
	{
		InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD * tmp = (InjectAttribute_tACFFF08424F2C68DF426C0B4CA3CE270C3A0E5CD *)cache->attributes[0];
		InjectAttribute__ctor_m3E63055F81749686F5DCA99CD12175AF74E08CC0(tmp, NULL);
	}
}
static void VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator_VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D____Current_PropertyInfo(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* _tmp_0 = (StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A*)SZArrayNew(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A_il2cpp_TypeInfo_var, 2);
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(0), il2cpp_codegen_string_new_wrapper("\x71\x75\x65\x72\x79"));
		(_tmp_0)->SetAtUnchecked(static_cast<il2cpp_array_size_t>(1), il2cpp_codegen_string_new_wrapper("\x69\x6E\x64\x65\x78"));
		TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD * tmp = (TupleElementNamesAttribute_tA4BB7E54E3D9A06A7EA4334EC48A0BFC809F65FD *)cache->attributes[0];
		TupleElementNamesAttribute__ctor_mAA971B5CB6FAE0690C06558CA92983ABC486068E(tmp, _tmp_0, NULL);
	}
}
static void ConnectionMap_t79190FE19909D64A6724949C68BFAFAE073CAAC9_CustomAttributesCacheGenerator_ConnectionMap_U3CStartTrackU3Eb__5_0_m9088F01D6DE96BC5152B83593D9F6A38FCE5BF89(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_0_t96107D88CD4974E12554183AC0890B4E0621158D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_1_t3B060763C276AFA2C00A66F0049C8810431E76C7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass9_2_t8F37A859CEC9C564A82421B6CEF9DB76D10A609F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_tCEDFC79EE145791EB8EBCFD8CE92EC1E0FF4F50A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass10_1_tAEB5A5501DE6D32AAD66E35343E285F8303C52CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CFirstFragmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CFirstLineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CSecondFragmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CSecondLineU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_FirstFragment_mD36C6BFF3C2F9B7754132D38E86AA4C88BAA7C53(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_FirstFragment_m80101B65E3442B7447457B753E262C2EE2EAC79C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_FirstLine_m2F444BB0816B0BFAB9850453725439D4D0A0BC91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_FirstLine_m55867E351D4B3569DC01C140FE0E0B5C42BC45FD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_SecondFragment_m64C76564838E4F75A0EC29A278C1CF34DF5E716A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_SecondFragment_m3222DF22EE5626287156ECFAD7F9E3996FBD2131(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_SecondLine_mB741610EBC5109C8DD1E31ED784B9E3FCD72CA87(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_SecondLine_m100B1B8D536D258CFF8AB6DB6E641FC215AFF9B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator__tableName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator__background(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator__content(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_U3CColliderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_U3CLinesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_U3CLinesChainsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_get_Collider_mA3C6D1B276CC7A344151E25BEDEABCB5A2D313CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_set_Collider_m7456A3ABA5B2FE79F148827641DCD76A296DDFAD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_get_Lines_mD4073C1AE5C75265FF768AE0F6998AE8C4E27823(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_set_Lines_mD587BD466F0FBE402EC2723E82A1333F7F155D4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_get_LinesChains_m5A9183D316014832FEE2B1A31DF8DD1ED3236980(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_set_LinesChains_m37311C9146D49C4DB484450E8EEB6735AFA912A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__imagePrimaryKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__imageSecondaryKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__value(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__chain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator_U3CNumberU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator_LineFragment_get_Number_m84D91CC9F491BF7A8DE2C03A0471742F768EE979(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator_LineFragment_set_Number_m3540B3A497CE0514DEA8C46592A1DCD3AEA572A4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void LineLocationsChain_t76C84DD8C5A26AD5EFA6A591093D9687D50F04CE_CustomAttributesCacheGenerator__leftChain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LineLocationsChain_t76C84DD8C5A26AD5EFA6A591093D9687D50F04CE_CustomAttributesCacheGenerator__rightChain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Model_t6B00FC8716EF4E2960A0980F3C76275AF4E403B0_CustomAttributesCacheGenerator_Model_CreateFragment_m87E4A19A9A5C9A40539888A7D83069EE079B553E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_0_0_0_var), NULL);
	}
}
static void Model_t6B00FC8716EF4E2960A0980F3C76275AF4E403B0_CustomAttributesCacheGenerator_Model_U3CInitializeU3Eb__18_0_m0D98F9175A1EC4F8AA904A18256EF3FF37A3E16A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_0_0_0_var), NULL);
	}
}
static void Model_t6B00FC8716EF4E2960A0980F3C76275AF4E403B0_CustomAttributesCacheGenerator_Model_U3CStartUpdateU3Eb__25_0_mA7963A825EAA0F91E224D17D50FD3013C2B4B19F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_tC89300084C4E1A831A54222C4364F1A5E689D0D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass28_0_tE124DECDB211EFF043DFE9216C119B940B70115A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_CustomAttributesCacheGenerator_U3CCreateFragmentU3Ed__28_SetStateMachine_m61F0AC8AE2106A52C3540BE2DDB85CA13A605FD0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__18_0U3Ed_SetStateMachine_m5596F358F217B2EAC9567D85E028E2E6F01130F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_U3CFieldsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_get_ID_mE380E920FEBCD95F65DB8B799774AC73B591B574(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_get_Name_m8E702E0073024E1BD48075155ED50C2B6AE0217C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_set_Name_mA0BAA4AB07C95AAED682BB25A22B614512BDBD65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_get_Fields_m66B7C8A1ABABFC4B0C56B207A9595FA9EE6A7F6C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_set_Fields_m3144580CA90297DFA2F73085BD495DBB724ED821(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CKeyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CLinkingTableU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CLinkingFieldU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_ID_m81B0DD81E9C3994EA202102719CC9CC81E5C8339(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_Name_m14BF7B65C2562DCCB8DBD7BE43186F702D1771CC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_Name_m99A4DFF739E956E1807943DF86C798C928CF75C7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_Type_m145EA533DF19C3FAB2314A742F5251DAC22151E1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_Type_mF11BD257B86CBF7720E2744BDCF86B374F7B9C7A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_Key_m344443E3BF1F4A107D842E23D1A63EB3314D0802(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_Key_m04D61761E1D2852E9FF98AA33793F45C02A62D4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_LinkingTable_m81B07C6840986C82521D6D185A96CADB69509974(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_LinkingTable_m1591FF28A5B7F00F8DB52B39E51BD856382AEDD5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_LinkingField_m2E9C8D3EBE625E3C7C2EF1284B3F46297A0196DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_LinkingField_m62A67C4103E3014DC7860DABB240F04468F9D243(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_U3CTablesU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_onUpdatedTables(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_get_Tables_m09493D27D57C1261B0C46A41CDD2D7554741E2CF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_set_Tables_mDF8846DE74846488B3874325028FCCDCD6DF5CCE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_add_onUpdatedTables_m71A4C5F597DF7D5A59627FABE5DF20AF44DEA307(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_remove_onUpdatedTables_mE53F8DCDBBFEE03A8B0E75D15B3E268DFD37776A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_EditTables_mF5117207F05584415E8709C8EA0FD2FD06FDD0D9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_0_0_0_var), NULL);
	}
}
static void TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_U3CInitializeU3Eb__14_0_m3DD4AC55D5E34D3DC7BB8C1F0FC9F03397177552(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_0_0_0_var), NULL);
	}
}
static void U3CU3Ec_t26FC7EDA0598D3EC8809A92FBED49204C6D94636_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_CustomAttributesCacheGenerator_U3CEditTablesU3Ed__16_SetStateMachine_mBB6231AC2DD6A0E60AD9829E1B95C139BF4A3B1F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__14_0U3Ed_SetStateMachine_m04324A6598D6DF1FC66790DBE18C3B04F25677DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_tDA3DA7E92432C91DCDA3786939C83C655454BF3C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_1_t9C15D3AAE2B7F7C342AC9F237B8B325F70772F62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLCodeManager_t5DC704E43C51077069D9F27DD852618E0F97A62F_CustomAttributesCacheGenerator_SQLCodeManager_U3CInitializeU3Eb__6_0_m8F76CA6BC23EF9D1BA44A68C845EDDCEE660ADB0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SQLCodeProvider_t49DE0C3E173F52E4ABD4BAC5AA6CD027580F693B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator__groupButtons(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator_timeAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator_EREntity_ShowLinkDisplayButtons_m7FD36F8EBF3132C7194E9A92ECCB985CD5F2E181(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_0_0_0_var), NULL);
	}
}
static void EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator_EREntity_UnshowLinkDisplayButtons_mE6D9DC889CBAA961970A340849E5C50B50E6697E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_0_0_0_var), NULL);
	}
}
static void U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_CustomAttributesCacheGenerator_U3CShowLinkDisplayButtonsU3Ed__4_SetStateMachine_mE47BF74EBDF45EA132A80E00E679D49DB64E99BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_CustomAttributesCacheGenerator_U3CUnshowLinkDisplayButtonsU3Ed__5_SetStateMachine_mAFD31CFB8945EE2311B318558901134A18F48BC1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_U3CColliderU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator__background(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator__arrowLinkDisplayButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator__lineLinkDisplayButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_get_Collider_m0792E6DD000B038C2F10E1E65409D3245E4235B7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_set_Collider_m68A82980890D17507A3A2BC0645652E1B0B17B65(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_ShowLinkDisplayButtons_m3F4AAC4B6696881CAA9B6AC9B328F0575F86B1FF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_0_0_0_var), NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_UnshowLinkDisplayButtons_mC051BC5AAF2DB71958512653734A540119667EF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_0_0_0_var), NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_ActivateLinkDisplayButtons_m1F090D10129FE23DD2925542C60AAA7A050067F2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_0_0_0_var), NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_DeactivateLinkDisplayButtons_mD5F67F937566CA834F44673DD9260337D1DEBF07(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_0_0_0_var), NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_U3CInitU3Eb__25_0_m0922C3EFD8C403C2AB72C902CF90C2B5900F210E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_U3CInitU3Eb__25_1_m2388AE5175E427CE6784BCA9350EBEFCF6E6312F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_U3CInitU3Eb__25_2_m49D386CFAAB51441D5CACC2146CBF492E9E7C757(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_U3CFragmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_U3CTypeMessageU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_get_Fragment_m7AF05ECD15D2EB0C6349E935F8EBF753E7D4BF69(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_set_Fragment_m50AD2FC5EE2E9881E468AB70C3C3FD1BEDEF8BF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_get_TypeMessage_mB91B10ECD28D1363833F33639F2DA1536D464F17(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_set_TypeMessage_m6282482770D3976CB8E83BEC7B01148E9EFF160C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass35_0_tF43F27B4043E1DC7BD46C1BDC6CAD6CC3003797A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass36_0_t31CA5E73BEBA2C31B7112C2247BF454A555248F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_CustomAttributesCacheGenerator_U3CShowLinkDisplayButtonsU3Ed__37_SetStateMachine_m5FAFD23E9524B9A069D3E6E73828B0C2FFC93085(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_CustomAttributesCacheGenerator_U3CUnshowLinkDisplayButtonsU3Ed__38_SetStateMachine_m1CB68E36EF5209ABAC644A21D01B8D7D29E967C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_CustomAttributesCacheGenerator_U3CActivateLinkDisplayButtonsU3Ed__39_SetStateMachine_m8ADA06384F1B79934412A6C47CB56C7CB1E84C45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_CustomAttributesCacheGenerator_U3CDeactivateLinkDisplayButtonsU3Ed__40_SetStateMachine_m881582DD8156252C91E72C9C5FA62A8E47D9F801(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator__canvasLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator__canvasArrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_positionsCanvasLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_positionsCanvasArrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_timeAnimation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_ERRelationship_ShowLinkDisplayButtons_mA24CF8467A321A0C6672A3051AAD60CE7AFEDA15(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_0_0_0_var), NULL);
	}
}
static void ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_ERRelationship_UnshowLinkDisplayButtons_m92789CF90DE876B9CCAD22CF51FE3E19E754D400(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_0_0_0_var), NULL);
	}
}
static void U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_CustomAttributesCacheGenerator_U3CShowLinkDisplayButtonsU3Ed__5_SetStateMachine_mB071BFC4AC2977AFE704F64B07AA05AA3086AF95(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_CustomAttributesCacheGenerator_U3CUnshowLinkDisplayButtonsU3Ed__6_SetStateMachine_m65BB9EC4E5B8DC540051A76087750CED6C950AE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_CreateTransition_mF5711BAD95F1E2E8132FDFD20B34F6D83E71724E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_0_0_0_var), NULL);
	}
}
static void ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_0_m7CBFD25F79C090A9FC4FD8E781DAF1911DB632DE(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_1_mC5D4747DC10AAA288B3D49AA2C0769E16CD8F635(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_3_mC85670B8972D1822EE2C583061E3AAC5F0DA91D0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_5_m4032B450CD99368CF84653482ED4757AF4D8068D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CStartUpdateU3Eb__22_0_mDAA5A6AB52C5E68B2EC196D4B666F664A98E8EE4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t6E951D2B61D3CA724391AA899487D3FFC4DDC11F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_CustomAttributesCacheGenerator_U3CCreateTransitionU3Ed__26_1_SetStateMachine_m9B1672DDF3CF2B82088D451BC5354BBE0EF2D2CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERTransitionCreator_t2292862F8C6AD57EBC71AE50E4B24210D346F15E_CustomAttributesCacheGenerator_ERTransitionCreator_Create_m6A5E73828881EBEC4D727D1E587BC26F2FA8E6FA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_0_0_0_var), NULL);
	}
}
static void ERTransitionCreator_t2292862F8C6AD57EBC71AE50E4B24210D346F15E_CustomAttributesCacheGenerator_ERTransitionCreator_UpdateTrackingTransitionCreation_m65A48164D9421715212BBB1429EA4EFD461243DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_0_0_0_var), NULL);
	}
}
static void U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_CustomAttributesCacheGenerator_U3CCreateU3Ed__4_1_SetStateMachine_mB7EBB404F9979424FF0F93831DCC62A706ABE939(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_CustomAttributesCacheGenerator_U3CUpdateTrackingTransitionCreationU3Ed__5_SetStateMachine_m38F51C2CF08090B045A97BC8389F1FF160CBA2BF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ERArrow_t12563D19D39B8ACB137F060082A60AE3B4FFDAAF_CustomAttributesCacheGenerator_firstArrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERArrow_t12563D19D39B8ACB137F060082A60AE3B4FFDAAF_CustomAttributesCacheGenerator_secondArrow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Arrow_t2ABF955437584721E6BF066661FBF689A70BEBF5_CustomAttributesCacheGenerator_gameObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERLine_tD9B51933F3789E5E04A333FB4F0308B03997FA30_CustomAttributesCacheGenerator_point(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Point_tA6B87B7C999AF294E7C966D9B247133EB459DFE6_CustomAttributesCacheGenerator_gameObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_U3CFirstFragmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_U3CSecondFragmentU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator__firstText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator__secondText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_delta(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_scalerCollider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_get_FirstFragment_m29BA75C690B51CCF551F400E29D1D765E64C0D44(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_set_FirstFragment_m3C8BAD6E886AB3F0A8E9D938770CF33AC19C6AB5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_get_SecondFragment_m9E82F524AEBA845FC6F2A9C85B9E1FB413D04104(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_set_SecondFragment_mE69669FEBE198E283777BD002DA4E542C9B78A80(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_U3CEstablishConnectionU3Eb__29_0_mF5B9579857714D4DCF3723AD9FB86C12E591ECA6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_U3CUpdateConnectionU3Eb__31_0_m9BFE0A8ABC510C65DD26C83B510403D41E401D83(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_U3CUpdateConnectionU3Eb__31_1_m4843ACC3903B4088A46DA2E519C640224963CF6D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass2_0_t3F2764ECC5CBC25DC6D9767670F717B6C707336A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t11693AF3EDFF2ECE6B4518A82453B40B5ADF992C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ContainerEntitiesAndRelationships_tA36DCFFB740E70FF1997793A0825BFA0692A5EEB_CustomAttributesCacheGenerator_ContainerEntitiesAndRelationships_U3CInitializeU3Eb__6_0_mD5992A4FE80FF08FBF0D6745C92113A7EFB29B80(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_0_0_0_var), NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ContainerEntitiesAndRelationships_tA36DCFFB740E70FF1997793A0825BFA0692A5EEB_CustomAttributesCacheGenerator_ContainerEntitiesAndRelationships_U3CInitializeU3Eb__6_1_mF8A5776CACCA67D576666075CA0F80F16712C6CB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_0_0_0_var), NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t0B2EE12BF5B33C07FB7AD40B59C634FF41EAFEC2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__6_0U3Ed_SetStateMachine_mB4439752281856794143C585BA5017FF9892CDC8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__6_1U3Ed_SetStateMachine_m875AF4B221DC01ABCF9DFE08161AD59EDEB3B66B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[827] = 
{
	CameraAnchor_t25E643DAE0EB984E3D6CBC24ADF72FBE6D5B5022_CustomAttributesCacheGenerator,
	U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator,
	ViewportHandler_tCE17BD0CF0A82CB45D31EC6BE2026445FB737BE9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t3EB52E86FB2025362BE3829D8B3E885828C7F0BB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_t19E47D36ED5E8A9FFA3AE14329E61C89D4666D4E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t15451E0426330982363A8EF0840B118034CEC2C7_CustomAttributesCacheGenerator,
	U3CU3Ec_t4AC76BFAC8B8DB351209611254D9F99585403D4D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tE6D280ADC4BE70235312ECDCA65E7F361375631D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t022CD8CE5876D03E04D1120CDCA715DB69440BCE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tF759D930C1038A45A0EC57EC954E1EFEC5EBE9BC_CustomAttributesCacheGenerator,
	Status_t8BB9BF0B27F7DA0CB38BE377390FBB9AE3762854_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tB8038464B6BF24E4D14FDD19976AE14765294B20_CustomAttributesCacheGenerator,
	SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator,
	ERModelNodeGraph_t8B7F06E925A5B043788ADF19524CE738B081F49E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_tE6DAA23C8B41705D399D9E25F0709F79DF2176D2_CustomAttributesCacheGenerator,
	ScriptableObjectsProjectContextInstaller_tFD46AA6891F407D8417C6D4AE8F429AF86A38282_CustomAttributesCacheGenerator,
	ComponentUtility_t342E2DC5ED7FFF72D9DE83F854929378A603286A_CustomAttributesCacheGenerator,
	Extensions_t7EBE2FE8040582909A81D38BCA3FD0C249A90BCE_CustomAttributesCacheGenerator,
	CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass3_0_tA34FF1DD369994CB019C61B1920FDDFA275DDF3B_CustomAttributesCacheGenerator,
	U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_CustomAttributesCacheGenerator,
	U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t954CDC21B4CC0E870BBA3A9A6C3600FF6AE786A4_CustomAttributesCacheGenerator,
	U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_CustomAttributesCacheGenerator,
	U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator,
	ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t372DD3C76BDBA25C7FAE65ECB4D4564403D759B9_CustomAttributesCacheGenerator,
	U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_CustomAttributesCacheGenerator,
	U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator,
	VectorExtensions_tB2198A25EAA37F28D7E233AB2445C545B21DA352_CustomAttributesCacheGenerator,
	StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator,
	TaskExtensions_t78223F1FBEE2223BA86E2DD4A4CE46FF0D83170C_CustomAttributesCacheGenerator,
	U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_CustomAttributesCacheGenerator,
	EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator,
	U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_CustomAttributesCacheGenerator,
	U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_CustomAttributesCacheGenerator,
	U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t30452C0E2D9E7F5663CEB1EC263E83624C30853F_CustomAttributesCacheGenerator,
	U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_CustomAttributesCacheGenerator,
	U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_CustomAttributesCacheGenerator,
	U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_CustomAttributesCacheGenerator,
	U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_CustomAttributesCacheGenerator,
	U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_CustomAttributesCacheGenerator,
	U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_CustomAttributesCacheGenerator,
	U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_CustomAttributesCacheGenerator,
	U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_CustomAttributesCacheGenerator,
	U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_CustomAttributesCacheGenerator,
	U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_CustomAttributesCacheGenerator,
	U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_CustomAttributesCacheGenerator,
	U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass6_0_tC4E15A154CB155879C3E9FC29701D51510F9A96C_CustomAttributesCacheGenerator,
	U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_CustomAttributesCacheGenerator,
	U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_CustomAttributesCacheGenerator,
	U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_CustomAttributesCacheGenerator,
	U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_CustomAttributesCacheGenerator,
	U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_CustomAttributesCacheGenerator,
	U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_CustomAttributesCacheGenerator,
	U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_CustomAttributesCacheGenerator,
	U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_CustomAttributesCacheGenerator,
	U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t16B7F9B42E6B4EB0C3F5E22EF3FE6DB3114E743A_CustomAttributesCacheGenerator,
	U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass8_0_t072DB55594034415BC7E5EF8C93FF1E27A39E47A_CustomAttributesCacheGenerator,
	U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_CustomAttributesCacheGenerator,
	U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_CustomAttributesCacheGenerator,
	U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_CustomAttributesCacheGenerator,
	U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_tC7E5C6AFB35CDD2355B3AD82C2831A3D1205AE48_CustomAttributesCacheGenerator,
	U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass27_0_t462AF001F282A2694D8ACB0E7BD04604F124A348_CustomAttributesCacheGenerator,
	U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_CustomAttributesCacheGenerator,
	U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_CustomAttributesCacheGenerator,
	U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_CustomAttributesCacheGenerator,
	U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_CustomAttributesCacheGenerator,
	GroupToolbarButtons_t655F33EFB05BECDBEDB2BA73FD6C4BF5C8938289_CustomAttributesCacheGenerator,
	Toolbar_t726FFA0787ED4F3EABAE3F369C31446515E2BCC0_CustomAttributesCacheGenerator,
	CreatingEntityShapeButton_tE5676158C3C09EAFBAA1035B62EC9AED1BB459BA_CustomAttributesCacheGenerator,
	CreatingRelationshipShapeButton_t7C6A7206A061063064968D2C4174E928CD207526_CustomAttributesCacheGenerator,
	EntityFormationButton_t4FF9052C5F5654F12A8A87C26356D679FC74B999_CustomAttributesCacheGenerator,
	FormingRelationshipsButton_t50F627C2A13BEC794F45470120FBC93423D7B837_CustomAttributesCacheGenerator,
	ChartDisplayButton_tE6CC8E60C4F7809E64194DB29F78F49FE7841A1F_CustomAttributesCacheGenerator,
	TableDisplayButton_tAEC69EC229B07B2A59794A9EA844EF1F298C3C03_CustomAttributesCacheGenerator,
	TableManagementButton_tD6B0C5E15B6D4E09A1A8E2FCD2D557637878C1AA_CustomAttributesCacheGenerator,
	CodeResetButton_tCD7A51CBD963C5017A0169A3AE7F04CCBB139A09_CustomAttributesCacheGenerator,
	HelpButton_tB992ED5639C666B19F42ACBF5B15CBEA4BA96212_CustomAttributesCacheGenerator,
	TaskButton_tCCE941BCF58129CE5C291BE4BE1985D2119E05F5_CustomAttributesCacheGenerator,
	Tab_tB0931206C740359C158CEA6CB0A0AA8183933A8C_CustomAttributesCacheGenerator,
	TabAngle_t3E766F09877E8B321471F1AFECEBE5CD6E7A7D43_CustomAttributesCacheGenerator,
	TaskSelectionTabbar_t621920382F0975D019FEDDDD6DE034EA98D441F8_CustomAttributesCacheGenerator,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator,
	U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_CustomAttributesCacheGenerator,
	U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_CustomAttributesCacheGenerator,
	U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_CustomAttributesCacheGenerator,
	FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_CustomAttributesCacheGenerator,
	U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_CustomAttributesCacheGenerator,
	U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_CustomAttributesCacheGenerator,
	U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass4_0_t3A95ECB6781BAFF18AA1F24E4495F4633D10CE4E_CustomAttributesCacheGenerator,
	U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_CustomAttributesCacheGenerator,
	U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator,
	ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tDB681ED161303E52235E7CD560F6701C814AB095_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t0D4B52F942C27EDBE4171670E1595A963CF19FD7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t85B7E2EEB988EE5B94C1B27D6968B47DBC4A7BC8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tDA6D3D7033896068AA90D65806DEF5922E5F49DB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass5_0_t3EA6BEBF4DBC59490A3AAFC311BBDA48DCA0A3D8_CustomAttributesCacheGenerator,
	U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_CustomAttributesCacheGenerator,
	U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_tF3D7EF628FB05EA86200C2006D504731BB5B86ED_CustomAttributesCacheGenerator,
	U3CU3Ec_t44D97896938755F82E023D4937115F1F1889694A_CustomAttributesCacheGenerator,
	U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_CustomAttributesCacheGenerator,
	U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_CustomAttributesCacheGenerator,
	U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_CustomAttributesCacheGenerator,
	U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_CustomAttributesCacheGenerator,
	U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_CustomAttributesCacheGenerator,
	U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_CustomAttributesCacheGenerator,
	U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_CustomAttributesCacheGenerator,
	ContainerOptions_t599769CAEFEEADA3DC532845DED463607D4EDD85_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_t63094766CF22F62AC9E77058CDD8CED4DED9BFCE_CustomAttributesCacheGenerator,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator,
	U3CU3Ec_t595904110144D09022625ED4D6FC690579397A5D_CustomAttributesCacheGenerator,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator,
	SpriteTableSwitchableToolbarButton_tBB0DDB77CCE13C46BB1F56EBD2AC0877B1D95ABE_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t5DD87F75BAE74A65AFB8C8618EBCE1203F703B4E_CustomAttributesCacheGenerator,
	TableERFragmentPrefabs_t7B58AA9E6F1979E8D4DB82ED941D9F3129E041D1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t7AF7586DFACE13E902E808E09FE4156DBB16677F_CustomAttributesCacheGenerator,
	TableERTransitionPrefabs_t8FB58C470A273C18931A20567DF56800265069B8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t9F7D7FAF49CF7B48B79DCB4899D367F551911CD0_CustomAttributesCacheGenerator,
	DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t77796F10BC5E754166BEA7BD420425C694443D7C_CustomAttributesCacheGenerator,
	U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator,
	SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t830EC8B38E9D4E68D1A557F0789DE06631DC9DA3_CustomAttributesCacheGenerator,
	VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_0_t96107D88CD4974E12554183AC0890B4E0621158D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_1_t3B060763C276AFA2C00A66F0049C8810431E76C7_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass9_2_t8F37A859CEC9C564A82421B6CEF9DB76D10A609F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_tCEDFC79EE145791EB8EBCFD8CE92EC1E0FF4F50A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_1_tAEB5A5501DE6D32AAD66E35343E285F8303C52CB_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_tC89300084C4E1A831A54222C4364F1A5E689D0D3_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass28_0_tE124DECDB211EFF043DFE9216C119B940B70115A_CustomAttributesCacheGenerator,
	U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_CustomAttributesCacheGenerator,
	U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_CustomAttributesCacheGenerator,
	U3CU3Ec_t26FC7EDA0598D3EC8809A92FBED49204C6D94636_CustomAttributesCacheGenerator,
	U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_CustomAttributesCacheGenerator,
	U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_tDA3DA7E92432C91DCDA3786939C83C655454BF3C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_1_t9C15D3AAE2B7F7C342AC9F237B8B325F70772F62_CustomAttributesCacheGenerator,
	SQLCodeProvider_t49DE0C3E173F52E4ABD4BAC5AA6CD027580F693B_CustomAttributesCacheGenerator,
	U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_CustomAttributesCacheGenerator,
	U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass35_0_tF43F27B4043E1DC7BD46C1BDC6CAD6CC3003797A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass36_0_t31CA5E73BEBA2C31B7112C2247BF454A555248F6_CustomAttributesCacheGenerator,
	U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_CustomAttributesCacheGenerator,
	U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_CustomAttributesCacheGenerator,
	U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_CustomAttributesCacheGenerator,
	U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_CustomAttributesCacheGenerator,
	U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_CustomAttributesCacheGenerator,
	U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_CustomAttributesCacheGenerator,
	U3CU3Ec_t6E951D2B61D3CA724391AA899487D3FFC4DDC11F_CustomAttributesCacheGenerator,
	U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_CustomAttributesCacheGenerator,
	U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_CustomAttributesCacheGenerator,
	U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass2_0_t3F2764ECC5CBC25DC6D9767670F717B6C707336A_CustomAttributesCacheGenerator,
	U3CU3Ec_t11693AF3EDFF2ECE6B4518A82453B40B5ADF992C_CustomAttributesCacheGenerator,
	U3CU3Ec_t0B2EE12BF5B33C07FB7AD40B59C634FF41EAFEC2_CustomAttributesCacheGenerator,
	U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_CustomAttributesCacheGenerator,
	U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	ComplexSymmetricNode_t74D1E6231C91C684AF1C4BE1D4AF7556640BBB07_CustomAttributesCacheGenerator_andConnections,
	ComplexSymmetricNode_t74D1E6231C91C684AF1C4BE1D4AF7556640BBB07_CustomAttributesCacheGenerator_symmetricFragment,
	EnumerationNode_tADEDABB643C51B14A75F57771C1A8F6FF75825F9_CustomAttributesCacheGenerator__fragments,
	SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_enter,
	SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_exit,
	SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_U3CIsEnteredU3Ek__BackingField,
	SimpleNode_tE7C486CEE3E526D36E2597ADA51FC5372A75FCFE_CustomAttributesCacheGenerator_fragment,
	SymmetricNode_t01252F0107E0B47BBBBC1053BCBD0B9DF7E238C0_CustomAttributesCacheGenerator_firstFragment,
	SymmetricNode_t01252F0107E0B47BBBBC1053BCBD0B9DF7E238C0_CustomAttributesCacheGenerator_secondFragment,
	SymmetricNode_t01252F0107E0B47BBBBC1053BCBD0B9DF7E238C0_CustomAttributesCacheGenerator_middleFragment,
	SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator__startNode,
	SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator__endNode,
	SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator__currentNode,
	SQLQueryGraph_t8AA41892B0830EA78F90E6D47CA0405411BA8584_CustomAttributesCacheGenerator_debugField,
	ERModelNodeGraph_t8B7F06E925A5B043788ADF19524CE738B081F49E_CustomAttributesCacheGenerator__entryNode,
	EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator__entryPort,
	EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator__enter,
	EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator__exit,
	EntityNode_tE0F34CB4E0E276BC9F466261672FDE06C858E42A_CustomAttributesCacheGenerator_context,
	EntryNode_tB4E87C7B753BB7B317809DEACCE7246A5178FDF6_CustomAttributesCacheGenerator__entityPort,
	RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator__enter,
	RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator__exit,
	RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator_outputRatio,
	RelationNode_t384AF71C19E6CDCB069FC4506C58ECDB211978B9_CustomAttributesCacheGenerator_inputRatio,
	MainSceneInstaller_t80ECFDA23D95028A0A6A8A14126AAA851FA9392F_CustomAttributesCacheGenerator__provider,
	MainSceneInstaller_t80ECFDA23D95028A0A6A8A14126AAA851FA9392F_CustomAttributesCacheGenerator__containerOptions,
	MainSceneInstaller_t80ECFDA23D95028A0A6A8A14126AAA851FA9392F_CustomAttributesCacheGenerator__assets,
	ScriptableObjectsProjectContextInstaller_tFD46AA6891F407D8417C6D4AE8F429AF86A38282_CustomAttributesCacheGenerator__assets,
	ScriptableObjectsProjectContextInstaller_tFD46AA6891F407D8417C6D4AE8F429AF86A38282_CustomAttributesCacheGenerator__containerOptions,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_frameRange,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_U3CAverageFPSU3Ek__BackingField,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_U3CHighestFPSU3Ek__BackingField,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_U3CLowestFPSU3Ek__BackingField,
	FPSDisplay_t53F5BA4921803909BE8686CFD37DC0D3A93CAFF9_CustomAttributesCacheGenerator_isShow,
	ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_U3CButtonU3Ek__BackingField,
	ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_U3CIsClickedU3Ek__BackingField,
	TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_U3CDataU3Ek__BackingField,
	UnitySerializedDictionary_2_tA69318E009DEDC312E6E1116C3A40A337CC00471_CustomAttributesCacheGenerator_keyData,
	UnitySerializedDictionary_2_tA69318E009DEDC312E6E1116C3A40A337CC00471_CustomAttributesCacheGenerator_valueData,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator__loweredRectTransform,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator__openImage,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_timeAnimationMove,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_timeAnimationDissolve,
	InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__creditBook,
	InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__result,
	InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__ratingAndOption,
	InformationBoard_t70D3FF065DAC189E9FAB657945C9DD00E0571B22_CustomAttributesCacheGenerator__time,
	PreviewWindow_tEA593855B275850C60DB7D9F47A0C6B4DF450829_CustomAttributesCacheGenerator__board,
	PreviewWindow_tEA593855B275850C60DB7D9F47A0C6B4DF450829_CustomAttributesCacheGenerator__chart,
	ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__firstTaskPanel,
	ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__secondTaskPanel,
	ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__thirdTaskPanel,
	ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator__fourthTaskPanel,
	TaskPanel_t88933EAD57CAC4DE2885BF873901D7EB421723D4_CustomAttributesCacheGenerator_Description,
	TaskPanel_t88933EAD57CAC4DE2885BF873901D7EB421723D4_CustomAttributesCacheGenerator_Score,
	TaskPanel_t88933EAD57CAC4DE2885BF873901D7EB421723D4_CustomAttributesCacheGenerator_MaxScore,
	MessageClickedMenuButton_1_t0796B4502DB586CDA1EFDD47BBC2BA5778F7E36B_CustomAttributesCacheGenerator_U3CButtonU3Ek__BackingField,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CFIOU3Ek__BackingField,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CCreditBookU3Ek__BackingField,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_U3CIsCancelU3Ek__BackingField,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator__warningLabel,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_type,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_U3CIsCompleteU3Ek__BackingField,
	OptionNumberForm_tE69ECEA57544513BB96B87F956974B92406B99B2_CustomAttributesCacheGenerator__number,
	SessionRecoveryForm_t7815D4D8A3C226A7F5BBD773DAFB5C9109CD7AB9_CustomAttributesCacheGenerator__userData,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_U3CStartTestButtonU3Ek__BackingField,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_U3CInfotTestButtonU3Ek__BackingField,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_U3CExitTestButtonU3Ek__BackingField,
	FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator__outInputField,
	FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator__inInputField,
	FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator_timeAnimation,
	WarningWindow_t2136D2CED21EF93ECC920B35811B8EF4B2F2E507_CustomAttributesCacheGenerator__description,
	WindowSQLCode_tF4BD109D056CA977F157568FC1E4715E6BECC84D_CustomAttributesCacheGenerator__tittle,
	WindowSQLCode_tF4BD109D056CA977F157568FC1E4715E6BECC84D_CustomAttributesCacheGenerator__input,
	EntityFormationLine_t51AB013F5E0C823A0CB07B0D15EEDA9A3A2A7779_CustomAttributesCacheGenerator__nameEntity,
	EntityFormationList_t59AFE1AEC07057CDB545F0C2A09F7C270186F1CB_CustomAttributesCacheGenerator__factory,
	RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__nameRelationships,
	RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__parentEntity,
	RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__connectingEntity,
	RelationshipsFormationLine_tBB0804ECC8AF76230144D52F7E2EE152B9542DA7_CustomAttributesCacheGenerator__typeConnection,
	RelationshipsFormationList_t04B7F444E66B53E451A6E51931B64367B8339825_CustomAttributesCacheGenerator__factory,
	SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator__key,
	SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator__image,
	SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator_U3CValueU3Ek__BackingField,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator__tittle,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_timeAnimation,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator__factory,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator__tableEditor,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator__taskForm,
	TableListLine_tBA4908DCFCD9802C0246A4AA20D8B7E6A4E60231_CustomAttributesCacheGenerator__editButton,
	TableListLine_tBA4908DCFCD9802C0246A4AA20D8B7E6A4E60231_CustomAttributesCacheGenerator__nameTable,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_U3CFieldsU3Ek__BackingField,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator__factory,
	TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__nameField,
	TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__typeField,
	TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__primaryKey,
	TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__secondaryKey,
	TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__linkedTable,
	TableEditorLine_t088EA2BE4C3DFF4269DB36068CD1C3BB6D35E96A_CustomAttributesCacheGenerator__linkedField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CIsPrimaryKeyU3Ek__BackingField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CIsSecondaryKeyU3Ek__BackingField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CLinkingTableU3Ek__BackingField,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_U3CLinkingFieldU3Ek__BackingField,
	UILine_1_tB207B12975E12A9D8FCD95A727FAC8FDD4225360_CustomAttributesCacheGenerator__number,
	UILine_1_tB207B12975E12A9D8FCD95A727FAC8FDD4225360_CustomAttributesCacheGenerator__removeButton,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator__addButton,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_timeAnimation,
	Toolbar_t726FFA0787ED4F3EABAE3F369C31446515E2BCC0_CustomAttributesCacheGenerator__groups,
	MessageClickedToolbarButton_1_t76A8B45531D30A3F952606209C0E0FDC7BFFB184_CustomAttributesCacheGenerator_U3CButtonU3Ek__BackingField,
	TaskSelectionTabbar_t621920382F0975D019FEDDDD6DE034EA98D441F8_CustomAttributesCacheGenerator__dictionary,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator__description,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_timeAnimation,
	ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageFirstTask,
	ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageSecondTask,
	ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageThirdTask,
	ImageProvider_tB8778214C4AFC421A7A09EC16C60E1366D3CB82D_CustomAttributesCacheGenerator__imageFourTask,
	TaskDebugStateMachine_t1F83FE3E14F5D72A120EA7DE00FBE4E36CBA0B32_CustomAttributesCacheGenerator_U3CCurrentStateU3Ek__BackingField,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_U3CReportU3Ek__BackingField,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_U3CDateOpenU3Ek__BackingField,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_U3CTypeLastCompletedTaskU3Ek__BackingField,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator__reportButton,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator__saveButton,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator__exitButton,
	Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_U3CTotalScoreU3Ek__BackingField,
	Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_U3CTimeU3Ek__BackingField,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CFIOU3Ek__BackingField,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CGroupU3Ek__BackingField,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CCreditBookU3Ek__BackingField,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_U3CNumberOptionU3Ek__BackingField,
	ContainerOptions_t599769CAEFEEADA3DC532845DED463607D4EDD85_CustomAttributesCacheGenerator__options,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_number,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator__taskDescriptions,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_erModelVerificationData,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_tableVerifications,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator_visualSQLQueryDatas,
	Option_t1F535C86C00AB1D1B0E96012D4F75199FDC45284_CustomAttributesCacheGenerator__SQLCodeDatas,
	VisualSQLQueryData_tE1E50FCA239D62ECBD9FF1FA2D4001201AC6DFD3_CustomAttributesCacheGenerator_Description,
	SQLCodeData_t3DF6C789953B5DABF481C6DCC29E425B7F65E068_CustomAttributesCacheGenerator_FinalCode,
	SQLCodeData_t3DF6C789953B5DABF481C6DCC29E425B7F65E068_CustomAttributesCacheGenerator_StartCode,
	TaskDescription_tA37CC2402EF4FBD6604F07CAA4D4A84A6747E38C_CustomAttributesCacheGenerator_TaskText,
	TaskDescription_tA37CC2402EF4FBD6604F07CAA4D4A84A6747E38C_CustomAttributesCacheGenerator_HelpText,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_inputFieldAuthorizationFormSprites,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__settingsMainScene,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__helpDescription,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_spriteTableSwitchableToolbarButton,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_taskSelectionTabSprites,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator_spritesSwitchButton,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__switchButton,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__lockRect,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableERFragmentPrefabs,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableERTransitionPrefabs,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__fragmentTableDiagram,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__lineFragmentTableDiagram,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__connectionTableFragmentDiagram,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__chainTableFragmentDiagram,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__entityFormationLinePrefab,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__relationshipsFormationLinePrefab,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableEditorListLine,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__tableEditorLine,
	Assets_t5831685464F427CED965994A86100E552D5EA6A5_CustomAttributesCacheGenerator__selectableLine,
	SettingsMainScene_t8D74C65C3190D9C49689AB148BA8F1BC81E9FE04_CustomAttributesCacheGenerator_firstActiveTask,
	SettingsMainScene_t8D74C65C3190D9C49689AB148BA8F1BC81E9FE04_CustomAttributesCacheGenerator_onDebug,
	SettingsMainScene_t8D74C65C3190D9C49689AB148BA8F1BC81E9FE04_CustomAttributesCacheGenerator_defaultNumberOption,
	SpriteTableSwitchableToolbarButton_tBB0DDB77CCE13C46BB1F56EBD2AC0877B1D95ABE_CustomAttributesCacheGenerator__list,
	TableERFragmentPrefabs_t7B58AA9E6F1979E8D4DB82ED941D9F3129E041D1_CustomAttributesCacheGenerator__list,
	TableERTransitionPrefabs_t8FB58C470A273C18931A20567DF56800265069B8_CustomAttributesCacheGenerator__list,
	DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionFirstTask,
	DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionSecondTask,
	DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionThirdTask,
	DictionaryHelpDescription_tFCFF3D224A317DD01020E9A4E956ECFC57233FBA_CustomAttributesCacheGenerator__descriptionFourTask,
	StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_U3CCurrentTimeU3Ek__BackingField,
	SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_value,
	SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_scaleModifySelecting,
	SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_scaleModifyReviews,
	SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_overlapBoxSize,
	SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_U3CCurrentFragmentU3Ek__BackingField,
	VisualSQLQuery_t88AA278C6F062184A4FFAD1EC91F25824261DA32_CustomAttributesCacheGenerator_U3CDescriptionU3Ek__BackingField,
	VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator__description,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CFirstFragmentU3Ek__BackingField,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CFirstLineU3Ek__BackingField,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CSecondFragmentU3Ek__BackingField,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_U3CSecondLineU3Ek__BackingField,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator__tableName,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator__background,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator__content,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_U3CColliderU3Ek__BackingField,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_U3CLinesU3Ek__BackingField,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_U3CLinesChainsU3Ek__BackingField,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__imagePrimaryKey,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__imageSecondaryKey,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__value,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator__chain,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator_U3CNumberU3Ek__BackingField,
	LineLocationsChain_t76C84DD8C5A26AD5EFA6A591093D9687D50F04CE_CustomAttributesCacheGenerator__leftChain,
	LineLocationsChain_t76C84DD8C5A26AD5EFA6A591093D9687D50F04CE_CustomAttributesCacheGenerator__rightChain,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_U3CFieldsU3Ek__BackingField,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CIDU3Ek__BackingField,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CNameU3Ek__BackingField,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CTypeU3Ek__BackingField,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CKeyU3Ek__BackingField,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CLinkingTableU3Ek__BackingField,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_U3CLinkingFieldU3Ek__BackingField,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_U3CTablesU3Ek__BackingField,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_onUpdatedTables,
	EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator__groupButtons,
	EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator_timeAnimation,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_U3CColliderU3Ek__BackingField,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator__background,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator__arrowLinkDisplayButton,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator__lineLinkDisplayButton,
	Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_U3CFragmentU3Ek__BackingField,
	Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_U3CTypeMessageU3Ek__BackingField,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator__canvasLine,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator__canvasArrow,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_positionsCanvasLine,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_positionsCanvasArrow,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_timeAnimation,
	ERArrow_t12563D19D39B8ACB137F060082A60AE3B4FFDAAF_CustomAttributesCacheGenerator_firstArrow,
	ERArrow_t12563D19D39B8ACB137F060082A60AE3B4FFDAAF_CustomAttributesCacheGenerator_secondArrow,
	Arrow_t2ABF955437584721E6BF066661FBF689A70BEBF5_CustomAttributesCacheGenerator_gameObject,
	ERLine_tD9B51933F3789E5E04A333FB4F0308B03997FA30_CustomAttributesCacheGenerator_point,
	Point_tA6B87B7C999AF294E7C966D9B247133EB459DFE6_CustomAttributesCacheGenerator_gameObject,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_U3CFirstFragmentU3Ek__BackingField,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_U3CSecondFragmentU3Ek__BackingField,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator__firstText,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator__secondText,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_delta,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_scalerCollider,
	CameraAnchor_t25E643DAE0EB984E3D6CBC24ADF72FBE6D5B5022_CustomAttributesCacheGenerator_CameraAnchor_UpdateAnchorAsync_m9996C1A34F5DBBCC095E14C8E28FCC087499E4E6,
	U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5__ctor_mBE52E2E7D91E961713D5BDFA45E653D344A0ABAD,
	U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_IDisposable_Dispose_m0BC1DCA1783EB09C527A2F5A8FEB01EC75CB5E78,
	U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2BA42456D21130C39DD01A0DBF53EE7C603C2652,
	U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_Collections_IEnumerator_Reset_m0C7A7CE466E3169E5764F963664EF3DB6E5AA4DB,
	U3CUpdateAnchorAsyncU3Ed__5_t217DB759A858529EE86F04E9E4B8583D6DA781C4_CustomAttributesCacheGenerator_U3CUpdateAnchorAsyncU3Ed__5_System_Collections_IEnumerator_get_Current_mD745BFAE404A38DAAAC6F15F2EA1BCED908C02C7,
	SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_SQLQueryNode_get_IsEntered_m13F26B8AED387BD1388DDA9B44D9CD1FD03CB714,
	SQLQueryNode_t537CBEFCAEBA0D073B9AE2526F2EDE6BA379FCD5_CustomAttributesCacheGenerator_SQLQueryNode_set_IsEntered_mECA5F6A406982FDD902B88D730E59F8E70A8A0F4,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_get_AverageFPS_mEEA62F4A79543C4FE1475AFC36854F2171C58C1F,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_set_AverageFPS_m30C6572E854B2FC04B7FC86E7A47C6085982179F,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_get_HighestFPS_mF9706AD4FA4FA6C0B807BA74A9CF4671E78E97D1,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_set_HighestFPS_mD2B5447252682EE2776399CA7D4248CF8D0B7B6E,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_get_LowestFPS_m09E86470F62541368824DC67877BB59BD62D053F,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_set_LowestFPS_m7CB4C3FF06A5E0F895E334961825214FF17FFC91,
	FPSCounter_t952A663F8DDFF54DE4A01F1C46C0EFDEE119E707_CustomAttributesCacheGenerator_FPSCounter_U3C_ctorU3Eb__15_0_m0BEE9FA9172C5DF92C766EE1011733FD34CF92D5,
	ComponentUtility_t342E2DC5ED7FFF72D9DE83F854929378A603286A_CustomAttributesCacheGenerator_ComponentUtility_GetComponentNullException_mA7D3C2A60ECEB30F9A03918A60A074A58ABDE85E,
	ComponentUtility_t342E2DC5ED7FFF72D9DE83F854929378A603286A_CustomAttributesCacheGenerator_ComponentUtility_GetComponentInChildrenNullException_mE9267BDAAD7B3DC7385F9CA9491F1FE84BFBA2DD,
	Extensions_t7EBE2FE8040582909A81D38BCA3FD0C249A90BCE_CustomAttributesCacheGenerator_Extensions_SetActive_m504313DF682759CF548ADD0BC7F50DDF62B85723,
	Extensions_t7EBE2FE8040582909A81D38BCA3FD0C249A90BCE_CustomAttributesCacheGenerator_Extensions_SetRaycastTarget_m3B8D2EA28AD8E7BAFE69BDDC76497FFEDBEE9A13,
	CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator_CameraExtensions_GetMouseWorldPosition_m2B117497423533E11575488FDE12BE8850BB70D4,
	CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator_CameraExtensions_ConvertWorldToCanvasPosition_m93B3C353C415D959F119E336574AB0DEAF892613,
	CameraExtensions_tED99EAC1B38C2B95A95DEC7DEF9E9F532F4ADF74_CustomAttributesCacheGenerator_CameraExtensions_ConvertCanvasToWorldPosition_m9000219ECAC96C10E5E2880479046DE20F0F3FC1,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_GetSize_mEAAD4D2793DBD62F56A8BF154DB66FF1CCB2A753,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_GetWorldRect_m1C9CD5FF9F753CDEC448C60141B8BC06B69E0288,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_GetWorlRectRelativeCameraSize_mCD81C276C743D4E0CAF3D115A13DD15C2DEF93A7,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpSizeDelta_m2A9E5ACEB91867C94DF44C2D22102339BE24E1C9,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpRectSizeDelta_m39C65E725A62C41731F1B4E4450438B2675D9D02,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpAnchoredPosition_m0C926F16516AA68BD53AFA2992AF4DD742802C4B,
	RectTransformExtensions_t442DD4654ACCE0AF89203FF9947B7E318189E71D_CustomAttributesCacheGenerator_RectTransformExtensions_LerpRectAnchoredPosition_mAAB08EFDE563F14BEFCDEA573393155FCC5C6188,
	U3CLerpSizeDeltaU3Ed__3_t0FD0ACE468873367C029C84AB158038F00B8785C_CustomAttributesCacheGenerator_U3CLerpSizeDeltaU3Ed__3_SetStateMachine_m7444DFAFDD7E0F1C8509B9FC48973843EB6A72D9,
	U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4__ctor_mF1888E43C25E604566E283BB989E80D70D50D9C6,
	U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_IDisposable_Dispose_mB00AD5311FE89E7940B8B6719928483B21609CC9,
	U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C83DCB8260D0D446E68BE1427BA4720BEECEA7F,
	U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_Collections_IEnumerator_Reset_m8427AD15687BE5354CFB80803CE38823B9F35D09,
	U3CLerpRectSizeDeltaU3Ed__4_t0AF7DA3D68E8A984A0406FD71D8F292A7E352A65_CustomAttributesCacheGenerator_U3CLerpRectSizeDeltaU3Ed__4_System_Collections_IEnumerator_get_Current_m363CB8532ADCFA86E6CA5973CE4821654F9ADEEC,
	U3CLerpAnchoredPositionU3Ed__5_t9135D732043E3329B393F0585EC47AEA3AEBA4BB_CustomAttributesCacheGenerator_U3CLerpAnchoredPositionU3Ed__5_SetStateMachine_m61FC70772C264B875E62E4D1F4DE711A0DA3AEFD,
	U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6__ctor_mFE9C09660DED78EA23598CABC1E5D57F4D567616,
	U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_IDisposable_Dispose_mE05334D602CD581DA1652952E4D42E1E326E1C9C,
	U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27E78D509F15357448603159979E21A28734F5AF,
	U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_Collections_IEnumerator_Reset_mBD5D41D6A1E243541E88C3FDD8F26BF72841FD21,
	U3CLerpRectAnchoredPositionU3Ed__6_t47916EF5344C939DE7F1C3AB591408290E5E093D_CustomAttributesCacheGenerator_U3CLerpRectAnchoredPositionU3Ed__6_System_Collections_IEnumerator_get_Current_mE7AFA3560DBDD05C198156E4D09663D5D398D919,
	ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator_ImageExtensions_LerpColor_m62FC6D0894E971E19AEAC1E0BCC0A2FBB4E610E7,
	ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator_ImageExtensions_LerpImageColor_m80BB6A014D89313752CC70AEBFC28AEF363A0E8F,
	ImageExtensions_t323824DBAC08777319EE410F084C1D4B7DC7214E_CustomAttributesCacheGenerator_ImageExtensions_LerpAlpha_m150FD3150DCA35A8116C44E6370A4615F67C006F,
	U3CLerpColorU3Ed__0_t91B0B64A80AA77F07882BDF90763DF4DFA56E330_CustomAttributesCacheGenerator_U3CLerpColorU3Ed__0_SetStateMachine_m3E4DCC6CD3A4BACA7C419035B957E3EDDA17D9B3,
	U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1__ctor_m7082FB6805788880EB1F5690764D35CC4D23A69B,
	U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_IDisposable_Dispose_m65E403F86AA083D75BACC561BAB5B4A0A45FD730,
	U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m63391AF6FC954A45D7D0CF2FFD7118D021AD3E40,
	U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_Collections_IEnumerator_Reset_mA68E70FBE44DD75DBD49A373BDF2645191EC7761,
	U3CLerpImageColorU3Ed__1_t482A38387FA4749D7F41E4D055C43BD0DAE2097B_CustomAttributesCacheGenerator_U3CLerpImageColorU3Ed__1_System_Collections_IEnumerator_get_Current_m7486836D1F223C28318C45F1C22DE8BF31A8349D,
	VectorExtensions_tB2198A25EAA37F28D7E233AB2445C545B21DA352_CustomAttributesCacheGenerator_VectorExtensions_Approximately_m6D21476F6B52E413A78E8B75B4C096841DBFA911,
	VectorExtensions_tB2198A25EAA37F28D7E233AB2445C545B21DA352_CustomAttributesCacheGenerator_VectorExtensions_Approximately_m6019DED3F59E93DB41296D5D9188C6588D00FB79,
	StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator_StringExtensions_WithoutWhiteSpace_mF2F95631E719204E888DB0F3F4525995E10085F4,
	StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator_StringExtensions_WithoutWhiteSpaceAndNewLine_mE29AB05C3967FE4B3C55802CFA21EF2E10060889,
	StringExtensions_t5EEA291ADE7B3F1C4714A7675770ABBB2908BD53_CustomAttributesCacheGenerator_StringExtensions_WithoutNewLine_mE635EB68415D012EC33402A2EE715C9C80E77DFD,
	TaskExtensions_t78223F1FBEE2223BA86E2DD4A4CE46FF0D83170C_CustomAttributesCacheGenerator_TaskExtensions_CancelUnity_mCF92A94C84EC3B7E3716799993F973C9201377F4,
	U3CCancelUnityU3Ed__0_t94F683629FB4444A71785DE5EF1D793DDD2C6747_CustomAttributesCacheGenerator_U3CCancelUnityU3Ed__0_SetStateMachine_m00470DD2D9A34297A4ED534E774C4D6F21FA6AFC,
	EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Has_mDB6F620256C9D647F43F8717D77ED79F9C42A271,
	EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Is_m4768AA265AC5CC4522C16B6303031BFB6E4ADD07,
	EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Add_mC40753F35FF13B2E9174E2D60080375C965E682A,
	EnumExtensions_t75D8FC10235F832D97011B4AA688E3B720A20EE7_CustomAttributesCacheGenerator_EnumExtensions_Remove_m878211BAD2875CE1FD7978B4A3E56202AC50A162,
	ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_mFDEB62E2BFF17A7732FDB0A5AE90650F9AE77380,
	ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_m937A9E1E10427CADEF8494BF60E49F18719A9AE9,
	ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_get_Button_mFDFA434D21266FF80FE663BF106FCA3E8F1141B3,
	ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_set_Button_m75A7B009F280F723F77FB7B7FDEF6117D9F472CB,
	ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_get_IsClicked_mC2D2B28D84F05A1DCCA8D77D89F12B88A2D8A6F9,
	ObservableButton_t0F24D5B0311A2819F2E9454C2CB6A39816379A92_CustomAttributesCacheGenerator_ObservableButton_set_IsClicked_m60934C9B8C324AED25EEB76FB1065D12F9CAB7CF,
	U3CTrackU3Ed__0_t2372893FF24D6DB55B466B8059B70FEBB750A066_CustomAttributesCacheGenerator_U3CTrackU3Ed__0_SetStateMachine_mD61A88711D2C54D458397DA51936A6DD2E176276,
	U3CTrackU3Ed__1_t9D3A296800E5BE5EC3A81E688A1C4E9E806DACC2_CustomAttributesCacheGenerator_U3CTrackU3Ed__1_SetStateMachine_m4FC89EAD355BD026515EB17BF3645D9024978CF5,
	TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_TaskContext_1_get_Data_m53ECF9A23083055A7A43604293BF73821B4C216F,
	TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_TaskContext_1_set_Data_m53B0B9C0E9B727650702F77FF938B90CB49CF44B,
	TaskContext_1_t126A905AA164FF501848323BE747A2E28DF02CCD_CustomAttributesCacheGenerator_TaskContext_1_CancelAwaitFrame_mB102339A88EF6823BA6BB38A5C55F6D9AB3F24FE,
	U3CCancelAwaitFrameU3Ed__10_t53B5B5696B51BE371DC2E88CCCE939D4D78ABDE9_CustomAttributesCacheGenerator_U3CCancelAwaitFrameU3Ed__10_SetStateMachine_mD064F16CC9808BE617D27F23F709A1CE6BEA4713,
	InputFieldERFragment_t8F5BDE9AF7D6C3CE1F763CF9F6FDC372496BEE4E_CustomAttributesCacheGenerator_InputFieldERFragment_Construct_mA7D0821C515E974CDB0760C21D27DD6AA344692D,
	InputFieldLowerSpace_t8592D6DA91D1D375DB3CDE1D18E822C3502956A5_CustomAttributesCacheGenerator_InputFieldLowerSpace_Construct_m8D3D8125CFFB48AEDDA207706EA39BE53180BB0B,
	GroupSwitchButtons_tCAFE9670185628FC84348AD9CC140E163230D100_CustomAttributesCacheGenerator_GroupSwitchButtons_Construct_mA2909FDCA45B1D458FA72E148465EB86CEFCAEAC,
	SwitchButton_t9363131C8BBB13C3288D35C9AC856A38C42A5946_CustomAttributesCacheGenerator_SwitchButton_Construct_m0046AE5A2D987808A31F5C026F0C0B26F3A200D7,
	TextButton_tAF2485C054CEC742789066DA22F2CC7E493D9ADD_CustomAttributesCacheGenerator_TextButton_Construct_mEE94EB47F2E69F18711B3FBBEF95281CD29F6F81,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Construct_mB3B5EF6C4A161814828A05C9FB66009F1E4F7929,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Open_m9A335DD4CEEA162F8548E2A0B63901CF50703921,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Close_mFCEC7E8A2C203CAFD008B7D7FEFA6C8E81769C33,
	U3COpenU3Ed__7_t5F8CF36262378830E2A6AC164ECB2B674EB98074_CustomAttributesCacheGenerator_U3COpenU3Ed__7_SetStateMachine_m5C63B446232F882B11C4BD6D90BA347AC24C5C79,
	U3CCloseU3Ed__8_t5FC9DD00824C75BA7C6B1F5087147739601C00C5_CustomAttributesCacheGenerator_U3CCloseU3Ed__8_SetStateMachine_mA9BB57103AD1F16470A85A04BA91178BF187CB19,
	InteractiveZone_tF2FA3D885AEFBA87CA8361A6F800CB98E8A7D712_CustomAttributesCacheGenerator_InteractiveZone_Construct_mDC9566F1634F8A1F834302F1690F6010563B52DF,
	Chart_t8304A8D9D3A855E57406A433FF9DDC1184C8F899_CustomAttributesCacheGenerator_Chart_Construct_m8A26C885C19C1B691896392227A97B0977197425,
	ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator_ReportWindow_Construct_m5C033787D9434858CA84FE600FE67E7271010F9A,
	ReportWindow_t02D5767C350E8C63F0415672ABF4985B2D33F6C7_CustomAttributesCacheGenerator_ReportWindow_Show_m3457717E1D7CE56DD8D6E4DF56CBF422DBFB8DA1,
	U3CShowU3Ed__8_t620DA0112888F376B2923C3D97B5B9350EEE9B4C_CustomAttributesCacheGenerator_U3CShowU3Ed__8_SetStateMachine_m8ACF4C0E04A9AF97A4215D3A63E87613915330BB,
	MessageClickedMenuButton_1_t0796B4502DB586CDA1EFDD47BBC2BA5778F7E36B_CustomAttributesCacheGenerator_MessageClickedMenuButton_1_get_Button_m0E21AD273F23C0C814AAB4AB09E957D13CF96C8F,
	MessageClickedMenuButton_1_t0796B4502DB586CDA1EFDD47BBC2BA5778F7E36B_CustomAttributesCacheGenerator_MessageClickedMenuButton_1_set_Button_m804451BBE46BC193E4B24AC2A5B0C434D41FED92,
	AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500,
	AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Show_m206BDF648DE2A716E59E9643A62A2288A0F46F72,
	AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_U3CShowU3Eb__7_0_mFA836FDA1FA98E6D0C628A754D8F9351B016EF9B,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_FIO_m2504011F37B75ECDEF1E68E77F36A09AA63B6C40,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_FIO_mC61D6CC8F6CA0C4A4DAB4B4DFA856CFE5B577CDB,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_Group_m90C61F525E0B414BF47C1D77794E7C1C44D7E92D,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_Group_m357A8DD7354CF384A11CEEB9084376492762A320,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_CreditBook_mD58C2FD9DBEC03B46B77CF0BA57AC1DC13F83AE1,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_CreditBook_mE713E8686EE294BA011CDAB946A7C1B137673667,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_get_IsCancel_m408FCFE481EEA26FD37106B23D248EC80511D1AE,
	Result_t0B8317D394DF4AF674050A39E4761E673D9E8B4A_CustomAttributesCacheGenerator_Result_set_IsCancel_m3334A239B6D3443C01AD70A5D8D0F9101DEA9C37,
	U3CShowU3Ed__7_tA327AC25C37850B961418FCD05629BAC54636B32_CustomAttributesCacheGenerator_U3CShowU3Ed__7_SetStateMachine_mDDF440CB75DE305ABD1D96544612D40911DB90D3,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_Init_mCC93046BBA6FF74C8680AF0AF35FD16617198A6C,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_get_IsComplete_m76961F1349F916503E8758B6E093DB53DF580938,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_set_IsComplete_m91AC9403F0A2E9B026CA4B7ECA9E6F24283FEF01,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_Construct_mA7108BD5FCA7AC70D3AB38D6EFE5F32978971C2F,
	InputFieldAuthorizationForm_t84870321ED3400A13366F58773EB5E853D48D65E_CustomAttributesCacheGenerator_InputFieldAuthorizationForm_U3CConstructU3Eb__12_0_mFCE7E43DEADE54248DBD659C8461447D60D45F09,
	Form_t3416818ABF401D25D26A122A78364F8E828A12E1_CustomAttributesCacheGenerator_Form_Construct_m37907F759BED7FE7A8CF69837AA9DDC2B2180969,
	OptionNumberForm_tE69ECEA57544513BB96B87F956974B92406B99B2_CustomAttributesCacheGenerator_OptionNumberForm_Init_m231E5A8EFA0F8FD0D7F2217B34B330091377ECBD,
	OptionNumberForm_tE69ECEA57544513BB96B87F956974B92406B99B2_CustomAttributesCacheGenerator_OptionNumberForm_Show_m4ADEA2D60FA145203E7F96974656FC0A1CA60DF6,
	U3CShowU3Ed__3_tFB8E768E04A073B597D3B00924B80335A9F9AA08_CustomAttributesCacheGenerator_U3CShowU3Ed__3_SetStateMachine_mE708EDB48840703C57AE986D9F4F6CCB8D8071D2,
	SessionRecoveryForm_t7815D4D8A3C226A7F5BBD773DAFB5C9109CD7AB9_CustomAttributesCacheGenerator_SessionRecoveryForm_Construct_m155487A59D4FA76B48F14867CDB7649B10CF1679,
	SessionRecoveryForm_t7815D4D8A3C226A7F5BBD773DAFB5C9109CD7AB9_CustomAttributesCacheGenerator_SessionRecoveryForm_Show_m136AF7B73B125894E9AE44C15960E3B77BEBBFEB,
	U3CShowU3Ed__4_t184468A73EF682D6AB64B22F954DC0D93DDA2D2F_CustomAttributesCacheGenerator_U3CShowU3Ed__4_SetStateMachine_mD2E737A532624B5AFED689C61278B3E1611FED75,
	TaskForm_t59E39BE010706B19371DE5D694949B7721B0A3A3_CustomAttributesCacheGenerator_TaskForm_Init_mAA34F4AA02C274D4BCDBA30091FCC17DA5A774AD,
	TaskForm_t59E39BE010706B19371DE5D694949B7721B0A3A3_CustomAttributesCacheGenerator_TaskForm_Show_m2849FDB66EFAD54C0C2CF0CCF0F47A870B38C31C,
	U3CShowU3Ed__2_t058236D4568EF0D4C76A1BE24C1D0A23D08E6C03_CustomAttributesCacheGenerator_U3CShowU3Ed__2_SetStateMachine_mF26F66655CF628E79C98B605F26A0A60DEF70B59,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_get_StartTestButton_m428E4A5CD47E1265E76D58BC0A846030D0C8C2B2,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_set_StartTestButton_m512BFD084DFC17A37C76996CE81730D9F66711E1,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_get_InfotTestButton_mA8D53D5C773625286BFCDC5FEFE7CFB735ED599F,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_set_InfotTestButton_m9BBBEF2C4EC85B213455E993A1C0E17EB1B7E0C1,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_get_ExitTestButton_m87500BCEE5BD25F5E8C3304BF137917102C9AE82,
	GroupButtons_t81AFD4F68B04E397494103CA617297A9C84CB1F1_CustomAttributesCacheGenerator_GroupButtons_set_ExitTestButton_m9401EC3D50032A4C399D9AFD5F31D830E51C5A07,
	PreviewWindow_t7F38D8081E43EB5B14CE1ECC57E5000AFF2990E2_CustomAttributesCacheGenerator_PreviewWindow_Construct_m2C7A66FF642F0DA084B63E9945E3B6A9A9C7B64D,
	ConfirmationWindow_tE139526D6BB92EEA8C0A6D0D943783FD293F62F1_CustomAttributesCacheGenerator_ConfirmationWindow_Construct_m35EAC04470E9CD87BE46B9AD1FDE72E72C6FF2B2,
	ConfirmationWindow_tE139526D6BB92EEA8C0A6D0D943783FD293F62F1_CustomAttributesCacheGenerator_ConfirmationWindow_Show_m827376CAFE9B30A0A53817B2138B6FFC1773111E,
	U3CShowU3Ed__5_t69FD70CF2F9F10BA93752047CB70D5BB9E5F7FBD_CustomAttributesCacheGenerator_U3CShowU3Ed__5_SetStateMachine_m0829C12D4EDFACC0D4D17500984389EC178D4137,
	ExitCompletionButton_t713DCD480F78111AF1A77FA9A59B2F14ADF42655_CustomAttributesCacheGenerator_ExitCompletionButton_Construct_mA7BEBAE0F537415AD07F0BD5D1F9F194E04CA879,
	ExitCompletionButton_t713DCD480F78111AF1A77FA9A59B2F14ADF42655_CustomAttributesCacheGenerator_ExitCompletionButton_HandleClick_m4DA3F62DBE28FD0A66501F7E77C2E91B27D7C7DC,
	U3CHandleClickU3Ed__2_tD48E01F4C5639C384762FC55FD9CF9DE9B7CF385_CustomAttributesCacheGenerator_U3CHandleClickU3Ed__2_SetStateMachine_mDD5A481BB6353371B51AC1833025146A27919BE3,
	FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator_FormCreatingRelationship_Construct_m0328465B4F2344B40F774FFAB35B5BEBCDB5D2F4,
	FormCreatingRelationship_t98B98B974135F20F93B1A06B94E921CBF4B4F188_CustomAttributesCacheGenerator_FormCreatingRelationship_Open_m9E7312830DF74F522DBCFC6DF6770ADB18AD60A1,
	U3COpenU3Ed__13_t2CC7F2504CA2058D082F52C5EB4582E2D192839E_CustomAttributesCacheGenerator_U3COpenU3Ed__13_SetStateMachine_mA2077041EF90D3B2442A55A743FF1BA10DC124CB,
	Factory_t91CDCF6D0AF81908EBF3642A02FB83327A4BE974_CustomAttributesCacheGenerator_Factory_U3CCreateU3Eb__7_0_m41D6B95E1B7208C71E9844AD7417B3C50E1A5406,
	TaskCompletionButton_t2EA5D9AD9670001FA421E9248D9DE7A1FE9FDBC5_CustomAttributesCacheGenerator_TaskCompletionButton_Construct_mF7CB4D0316029FB9ACC887FAB7A926D9E324AA39,
	TaskCompletionButton_t2EA5D9AD9670001FA421E9248D9DE7A1FE9FDBC5_CustomAttributesCacheGenerator_TaskCompletionButton_HandleClick_m04A5805ABE604C6D992BE9C8DDA82DD8CC9F65B1,
	U3CHandleClickU3Ed__3_t96B299444BD312FF20EF0545A8F1486C03449342_CustomAttributesCacheGenerator_U3CHandleClickU3Ed__3_SetStateMachine_m952ADFA6A041BD5FFEBAC8D153F3B211623DEA6C,
	WarningWindow_t2136D2CED21EF93ECC920B35811B8EF4B2F2E507_CustomAttributesCacheGenerator_WarningWindow_Construct_mEE65B5C6D253FDAB06BAFD2A220A1C04663B55C3,
	WarningWindow_t2136D2CED21EF93ECC920B35811B8EF4B2F2E507_CustomAttributesCacheGenerator_WarningWindow_Show_mFA5E54FB0D6771AEA6394044E43F70310CDF6A33,
	U3CShowU3Ed__8_t801DB094B3758F465F031A6069C4A5CE10A00870_CustomAttributesCacheGenerator_U3CShowU3Ed__8_SetStateMachine_m2A972ED1FBE5932E44D69AF5B571E79B1EC7CEBF,
	WindowSQLCode_tF4BD109D056CA977F157568FC1E4715E6BECC84D_CustomAttributesCacheGenerator_WindowSQLCode_U3CInitializeU3Eb__3_0_m532C633C60357CB6C723E5C47D00F1BE6E4D1A53,
	EntityFormationList_t59AFE1AEC07057CDB545F0C2A09F7C270186F1CB_CustomAttributesCacheGenerator_EntityFormationList_ValidatInputFieldEdit_m736CA5B2874160C5807F225AC734B430144DEF50,
	U3CValidatInputFieldEditU3Ed__5_tD74C5DE2D6F736BEBD2C503B30A10FE430005CC0_CustomAttributesCacheGenerator_U3CValidatInputFieldEditU3Ed__5_SetStateMachine_mA0818812A8D5E76815B6A78F3F4D446F4E85DE33,
	SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator_SelectableLine_get_Value_m367D2123D20977BEADDEA073392328932E2852B6,
	SelectableLine_tD1049F147C73F7FD477F430905D76D22E04452A8_CustomAttributesCacheGenerator_SelectableLine_set_Value_m518FC2A427835FC571FF5ACAC41B9EC82D45E07B,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_Construct_m455B01837B83327EB01804EFB234265AF9C9D7A3,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_Open_mC9DBC445090E0D12678D893FCE36D9CFF7150594,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_Open_m7F01FB408F7E5991E20DD6D245912046BEF915A8,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_ShowForm_mB7F16E3F66B41E2E0527EA416E09F1D31C38C179,
	SelectableList_t18CFB28D7C0B5857BF89D62EF8668AECD7273D77_CustomAttributesCacheGenerator_SelectableList_UnshowForm_mF2EDD534498E1AB1C81A377B85565470E545B364,
	U3COpenU3Ed__15_t6F9441A1E825F09664D6794A130467E391372005_CustomAttributesCacheGenerator_U3COpenU3Ed__15_SetStateMachine_m25A241FB41C376614FCD476190A1B6CB2EBE79D0,
	U3COpenU3Ed__16_t498120B98546EBA9098D626169C10D0A21D52DCF_CustomAttributesCacheGenerator_U3COpenU3Ed__16_SetStateMachine_m59AA724DFA457754DB1F833125844D69F97D9294,
	U3CShowFormU3Ed__20_t4E377A4BB4A972419AC123601A7A8E6FACFB390B_CustomAttributesCacheGenerator_U3CShowFormU3Ed__20_SetStateMachine_m87A5D22CC65A9AE807EEE2AC4F7F1404F88FF7A6,
	U3CUnshowFormU3Ed__21_t5B9CC22535F603C48B8FBD32D384287F07BBBA44_CustomAttributesCacheGenerator_U3CUnshowFormU3Ed__21_SetStateMachine_mFE9ECC8AF67BA48403D6E5792E960519C1F06994,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_Open_mB0E053E52F57D51D6345A19CE4DCAF3EF08D49C9,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_ShowForm_m6AF23B2B689AF89F4563EFE15943B0CDCFDDC1DD,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_UnshowForm_m8A0443BDF4E1BC2531B304E7C795EF77260E2B5A,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_ValidateInputFieldEdit_mC1BACEE6ACF7579E7B10AD08A28EE350A0C2D8DF,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_EditTable_m5008FC449A2DBFFFFFA1F3D1CACB1190403F98E0,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_U3CU3En__0_m1527926A736012D2A7E1AD50F8606423D82E109A,
	TableList_t91109DA1E5162B3C2469B2582E09D817BBF2D90E_CustomAttributesCacheGenerator_TableList_U3CU3En__1_m345FE8EB766F048293479D3EB813E9BCDD981D18,
	U3COpenU3Ed__7_t5C1E07F3F27BA622F396AF482D56E194767313AC_CustomAttributesCacheGenerator_U3COpenU3Ed__7_SetStateMachine_m2D9A7B58AFE3934EC736E2B95A5ABDDECC6B0AB0,
	U3CShowFormU3Ed__8_tC2B52A893B838283C37AF255CCA3693A0B208EBE_CustomAttributesCacheGenerator_U3CShowFormU3Ed__8_SetStateMachine_m1010B25F136ED30D96229B56183D6BA3352143FE,
	U3CUnshowFormU3Ed__9_t45AD8D25CFF14539C5A20AA48CB76C6EC7D9B386_CustomAttributesCacheGenerator_U3CUnshowFormU3Ed__9_SetStateMachine_mECB0EC6F69A5076F394E9A32B1796262EB54D3E0,
	U3CValidateInputFieldEditU3Ed__15_tBD9E4530887E2D1F2079532A68C5F6E73E054091_CustomAttributesCacheGenerator_U3CValidateInputFieldEditU3Ed__15_SetStateMachine_mED2BD6C534A2DA7DE851AD529F651CD8012FF482,
	U3CEditTableU3Ed__16_tFBC40162197E4EFF60AADE41A8E3FF34C5D10C98_CustomAttributesCacheGenerator_U3CEditTableU3Ed__16_SetStateMachine_m51CF6C38FF19AE177D344CED7CBA5C84ED9A1F89,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_get_ID_m84339BB129C41BF33D1AE4F32B4DA701784C4CF1,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_get_Name_m86FC6ACF48682C6B8B58B77B00B08012F349AACD,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_set_Name_mD0275CE51CEB81D57EFB131A4BD7CCEACF7B49DF,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_get_Fields_m2BD42217ECD4307793F3C1261724F0E9A5889994,
	TableData_t6FD23E6C53D69A4884D4A943F88F7F5FCF1243BB_CustomAttributesCacheGenerator_TableData_set_Fields_m0AAAA9660E23BE990EC4C6A077002FA27224B450,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_Construct_m37651B457C7851C779A880EE800B6D6D36A593C7,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_Open_mD5CAA7687CDD73BC4DC75B5313FFBBA1A64A67FC,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_EditTypeField_m4E94185E2C6FF1170393A8F8CCC7D7750827C79E,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_ValidateInputFieldEdit_m2AE7C83ABB4CBF14F102892918CEFD2BBBB7012E,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_SelectLinkingTable_m9B1473FF979AC3DA3B31D887D92E4D045999A9F0,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_SelectLinkingField_m8695B74B1D59E6CA3ACA23028E4501E9E016453D,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_0_m8FCDE10EE5069B91D2DB7A530C5ACD3F5BA16ECA,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_1_m355F5E3B921F08C270F38CA88988740F3D8A90D5,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_2_mBE1A3EBECB1509D97B2C91E4503157EE28018CE1,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_3_mCA863663C36DF56A51BC3665640671F938235DEB,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_4_m0138D64D4304D189F7BADE2703FB6F057E4FF95C,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_5_m50BD05C35ADE68A86B7A7BF08AAE15B3415A8CA0,
	TableEditor_t969A0CBB55589BC17CCF07D6758B80317607512A_CustomAttributesCacheGenerator_TableEditor_U3CConstructU3Eb__12_6_m718ED9CAC1F03139395D85C3497C409F30D2C46D,
	U3COpenU3Ed__13_tAD05455305F17F95687A8A614CF417CDE61A5CD9_CustomAttributesCacheGenerator_U3COpenU3Ed__13_SetStateMachine_m7AA4935C448CFE3C3A2E27F62ACCDC58CF52E1AB,
	U3CEditTypeFieldU3Ed__19_t64A7E0D164B5F7EA3B3D71510B0101D366B095AF_CustomAttributesCacheGenerator_U3CEditTypeFieldU3Ed__19_SetStateMachine_m1F82D70EDC986EC5565E9C7DBD4D39DD04F3D036,
	U3CValidateInputFieldEditU3Ed__20_t731812031E6259468EB2FC01669DA03F77897A91_CustomAttributesCacheGenerator_U3CValidateInputFieldEditU3Ed__20_SetStateMachine_m1ED2683C1C1A15446C21CF04098529DD7777C618,
	U3CSelectLinkingTableU3Ed__21_tBAA369860F47AF5E8B606E2656A6DD77A0AFAC8D_CustomAttributesCacheGenerator_U3CSelectLinkingTableU3Ed__21_SetStateMachine_m1E662E4F606310BD5B785782F4A409BDF488A5DC,
	U3CSelectLinkingFieldU3Ed__22_t06B579DF38127CB300EB7F1C34AFF86B3646593A_CustomAttributesCacheGenerator_U3CSelectLinkingFieldU3Ed__22_SetStateMachine_m33413D8E19FD6392429F2566C7C9EAC7A8F4E79A,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_ID_m6B489923A70B2506C41D85C1A3311186763FA210,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_Name_mFE5D46C6C8413B6D477E238EA696707B979B069C,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_Name_m9A85EB4A5AA5AAD79F030F7293A5DCDED0A0EB97,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_Type_mCB18134F60D382F6AEC72173F6BA97055F1A493C,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_Type_m29EFE5A4BAD118BC97D0EA5DBE2590685D5DA809,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_IsPrimaryKey_mD0C966804822EC294109A9F9705912829FCE17E6,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_IsPrimaryKey_mF42468E54065ED9FEAF7163DC940AA8B00DB7E23,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_IsSecondaryKey_m40701D30049DEBC67B0A18E4D8B93E6F25B1E877,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_IsSecondaryKey_m0B73B5DD85E2BC1F3E3F87C0A4F8A38E3894D30F,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_LinkingTable_mBDA3F6647F37703726061453893DAAD2FD8B9B5A,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_LinkingTable_m27C3614D3CF8234D6155F84ADE91AFD2617110DE,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_get_LinkingField_m042A69A76301BFF079F3108681C6309591C11FDD,
	FieldData_t4548FC48054BE8C3014EA9C8437486D5BACFD003_CustomAttributesCacheGenerator_FieldData_set_LinkingField_m80D3A9AC11F10A33E4A9088B6F1D2C5425F9F159,
	UILine_1_tB207B12975E12A9D8FCD95A727FAC8FDD4225360_CustomAttributesCacheGenerator_UILine_1_U3CInitU3Eb__13_0_mA87881ADED7ABB18BF6D1291A1653520D92BFB7F,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Construct_mB3AA87ADC7F476047383EF551EC599B4EAE3B225,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Open_m99E3F13499142CC72A5003A6ED00F7429E78EA6C,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Open_mB7C4270E070F1A8A3CD55553582A7A0DC9874104,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_ShowForm_m01B205097E38695C9217478F274BB6ECEAD065DE,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_UnshowForm_m36C070914FE40DBF2697FEFDA89CDA24BF9C96B7,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_U3COpenU3Eb__17_0_m3974A1B0FCC88D408532ADD1AB4DF9C24B040FF5,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_U3COpenU3Eb__18_0_m7D397DC660D578C5C30E54E226AABDA7DB5FF288,
	U3COpenU3Ed__17_t3CAEA546F9BD3C2231057D796173B15D99D2715E_CustomAttributesCacheGenerator_U3COpenU3Ed__17_SetStateMachine_mDD912E2482C42889E6C4F99F5EEB6FBBCEB78BA1,
	U3COpenU3Ed__18_tB7158E96ACA1FDDCAC39D20FBF9E0AF45066C19C_CustomAttributesCacheGenerator_U3COpenU3Ed__18_SetStateMachine_m568C6E912253EDA7583F5772822C51C03A77EB2F,
	U3CShowFormU3Ed__28_tFD28C48BB652C068C294D7BD7859800E9A56CC24_CustomAttributesCacheGenerator_U3CShowFormU3Ed__28_SetStateMachine_m0FA23A91FA0E623050BF9C9350D927E3133DE41C,
	U3CUnshowFormU3Ed__29_t673D069BD98B8D9EF8D3B5A61359B2E41C0F38CF_CustomAttributesCacheGenerator_U3CUnshowFormU3Ed__29_SetStateMachine_m0EA2077B412C2DB9E12C985684E9FF6FA4C71937,
	Toolbar_t726FFA0787ED4F3EABAE3F369C31446515E2BCC0_CustomAttributesCacheGenerator_Toolbar_Construct_mF4C5F6EA0A73156BDBCA4D14808BD8DB49DBC837,
	SwitchableToolbarButton_1_t69E0B51732DD040B2A65E9AE579C5BD49896945D_CustomAttributesCacheGenerator_SwitchableToolbarButton_1_Construct_m9AD58EFE04505DABE978154EAA6562458789F08F,
	MessageClickedToolbarButton_1_t76A8B45531D30A3F952606209C0E0FDC7BFFB184_CustomAttributesCacheGenerator_MessageClickedToolbarButton_1_get_Button_mE788B5DB8F524FF172A88E657D7D2034606C9DDB,
	MessageClickedToolbarButton_1_t76A8B45531D30A3F952606209C0E0FDC7BFFB184_CustomAttributesCacheGenerator_MessageClickedToolbarButton_1_set_Button_m93A2166AAFF859FCC41A0B80C268248DEA09F8B3,
	Tab_tB0931206C740359C158CEA6CB0A0AA8183933A8C_CustomAttributesCacheGenerator_Tab_Construct_mECEDE8A800B396EA73336E474FDE656AC7881B81,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_Construct_m9B880246098E6A6A5BED80DA09F83C558E3BF3BD,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_ShowWithFocus_m0BA8D93C5093E28B200BA2BEED522D8CBAD69590,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_Show_mA1589E371CB45CB67C3864E16639D76DE1E4B518,
	Form_t8AD5869DE8B33D881B0840309ADB0B2A6BFB6D85_CustomAttributesCacheGenerator_Form_Unshow_mEB967B933E57BD9BB5FAAB901FF3C81762F83319,
	U3CShowWithFocusU3Ed__12_t5765994E13707BAACD4DCFCE8A55AA08FEC14B0F_CustomAttributesCacheGenerator_U3CShowWithFocusU3Ed__12_SetStateMachine_m1DEDC57D06D255EEE40EB8297644CAB3BE1B52F4,
	U3CShowU3Ed__13_t6EE323B02F2573BC6E6BDFCA3385DDA5DC10B689_CustomAttributesCacheGenerator_U3CShowU3Ed__13_SetStateMachine_m70BC076CB022F813ECEEBE83981309221B9437E7,
	U3CUnshowU3Ed__14_t0459768399A242393C7432FD6E4BBB510A5DA939_CustomAttributesCacheGenerator_U3CUnshowU3Ed__14_SetStateMachine_m1A37AF8BC0657BA2FC806793BB50579BAED8FAA3,
	FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator_FormPlaceholder_Initialize_m813F3879487C69CFF7FFCB85E5E91C92CFFFB5A1,
	FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator_FormPlaceholder_U3CSubscribeToMessagesU3Eb__9_0_m1B25DFF772C47D048C776CD56BA67B51DCE06839,
	FormPlaceholder_t284BD1F9ADBA7DC9C5F6D3E583FCC36227288EA9_CustomAttributesCacheGenerator_FormPlaceholder_U3CSubscribeToMessagesU3Eb__9_1_m7D3EED2C9734E1A25DE69B8B3988518BC9C3025E,
	U3CInitializeU3Ed__6_t999731248D4A7BD0B1905D18A12A2EF7B0E84DA4_CustomAttributesCacheGenerator_U3CInitializeU3Ed__6_SetStateMachine_mE1AECB03CDDE715730DF7773D95C396CC483C7AE,
	U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_t50B87ED9D4DD2A356D13B52C315722678B37AEC0_CustomAttributesCacheGenerator_U3CU3CSubscribeToMessagesU3Eb__9_0U3Ed_SetStateMachine_mB2FE8828F3A7B82985DF6D9853845C9312B24CB9,
	U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_t793A8801DEFB5520BB080F5A6F8EF70C2C9935A0_CustomAttributesCacheGenerator_U3CU3CSubscribeToMessagesU3Eb__9_1U3Ed_SetStateMachine_m224FE3F394078E076C249ACEFA6525FA595E1696,
	HelpForm_t277349E855034C5307CD7BCCB68F658922DC7BD6_CustomAttributesCacheGenerator_HelpForm_Construct_m2CABFE1CC344B7BC24BFEBF754E6B2145689BB7F,
	HintProvider_t95194E80FF0E5443BE06F4DE67514D1BD0B7099C_CustomAttributesCacheGenerator_HintProvider_DisplayHint_mFA6B792222965194337FDCF17DEF722399725B7C,
	HintProvider_t95194E80FF0E5443BE06F4DE67514D1BD0B7099C_CustomAttributesCacheGenerator_HintProvider_StartAnimation_m8D88CB848914E12ABF8986A6E64066678559A543,
	HintProvider_t95194E80FF0E5443BE06F4DE67514D1BD0B7099C_CustomAttributesCacheGenerator_HintProvider_ChangeScale_m2841AEAC53F9044178FB679703A92E68FB30095D,
	U3CDisplayHintU3Ed__3_t8FA0F749F0960A1F57CB8F95E1E438A4E45B11B1_CustomAttributesCacheGenerator_U3CDisplayHintU3Ed__3_SetStateMachine_m0DC25574E8EDD8FBF3697B909D08843ED803399D,
	U3CStartAnimationU3Ed__4_tCE0DE9D144085098CC813DBA484DC168CEB173D7_CustomAttributesCacheGenerator_U3CStartAnimationU3Ed__4_SetStateMachine_m46397A5DFC151EF869F043A6125E79B116C40E31,
	U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5__ctor_m0715FD638864704901AAF0EEB5AE631DC5994402,
	U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_IDisposable_Dispose_mB6FF7DA598646E9BC99CBCC3F32EC42714DEC7F2,
	U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF19E5CF7AC91B863A4E129FD6E966FF4A2EACC99,
	U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_Collections_IEnumerator_Reset_m89D39062590CA66F649E5646E9656736BDBBDFE6,
	U3CChangeScaleU3Ed__5_tF22D953589215481BDF252180CB143DF03C81D0E_CustomAttributesCacheGenerator_U3CChangeScaleU3Ed__5_System_Collections_IEnumerator_get_Current_m66A5E06443CC82EF0A58A9FCED6927CCD395ECF3,
	TaskForm_t31DA1AE62CDD2C7ACA54FDAE4BD4F6C6E27E8844_CustomAttributesCacheGenerator_TaskForm_Construct_m3BCBD75652453F89D36599055BC0E333EDFBC113,
	TaskManager_tC3B8E946B702E831442C905DA0D11259E18DA5FA_CustomAttributesCacheGenerator_TaskManager_U3CSubscribeToMessageU3Eb__11_0_m1CDD6BC1385CD44BC9465610B820D7B3BE4629E0,
	TaskManager_tC3B8E946B702E831442C905DA0D11259E18DA5FA_CustomAttributesCacheGenerator_TaskManager_U3CSubscribeToMessageU3Eb__11_1_mACFB2BE36A61F75DFF84F31106C0AE4209D5F545,
	TaskManager_tC3B8E946B702E831442C905DA0D11259E18DA5FA_CustomAttributesCacheGenerator_TaskManager_U3CCompleteTestingU3Eb__15_0_mCF0632A138D4BD6331BFFCA4CB7B110064AE2817,
	ThirdTask_t0AEB00122F9344731C7DB0DAB6BD69B911C03FEB_CustomAttributesCacheGenerator_ThirdTask_U3CInitializeU3Eb__6_0_mE86743699D637A90F4B73B21D330894DF7F06549,
	TaskDebugStateMachine_t1F83FE3E14F5D72A120EA7DE00FBE4E36CBA0B32_CustomAttributesCacheGenerator_TaskDebugStateMachine_get_CurrentState_mF58CB79F74A7DE6ECBBBA752B3E4815E96D8E7AD,
	TaskDebugStateMachine_t1F83FE3E14F5D72A120EA7DE00FBE4E36CBA0B32_CustomAttributesCacheGenerator_TaskDebugStateMachine_set_CurrentState_mD5E8470ECFF91596FC3C63099A211C28F117661B,
	Startup_tD9CDCC99E10BA274A878E7BAA33661601A581368_CustomAttributesCacheGenerator_Startup_Initialize_m79F0FEF23261E4A2B406F1DCFA9ABDE03486B921,
	Startup_tD9CDCC99E10BA274A878E7BAA33661601A581368_CustomAttributesCacheGenerator_Startup_StartTest_m66AF9579D7F9ECEF9519CB3C35064DC107278C56,
	U3CInitializeU3Ed__5_t65542F49D15AB4488A0FCA4E74DD6F5070156E5B_CustomAttributesCacheGenerator_U3CInitializeU3Ed__5_SetStateMachine_m8481F4F01CD81C43007A6B8E673752B69346129F,
	U3CStartTestU3Ed__7_tB72985884AB3714B4CDECCBE6D5D61E0D155FA2F_CustomAttributesCacheGenerator_U3CStartTestU3Ed__7_SetStateMachine_m48DB0560207121B43A8E9AA5717836AC5E02E4A1,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_get_Report_mB6457B7C7508A226095757EE37A6E2D4171F0C97,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_set_Report_m048AB0F59A9C190ECBFB40EDF82AEED661284AF0,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_get_DateOpen_mC3DFA41FEC23AE827EA4ACB643E2C215ACB1E090,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_set_DateOpen_mB75DA6BDB1CA777820D617C8FD4E9104DEE6209F,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_get_TypeLastCompletedTask_m8F51FD58B16B47CCD62450ADD58B4B57D2C51F44,
	Session_t034C97FA3572AAA23CF606E6DBE0534B245A473E_CustomAttributesCacheGenerator_Session_set_TypeLastCompletedTask_m80CC35975CD36197EF33C0FA1C2693CC0F21BB11,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager_ShowReport_m192E756997C5F86B8F6A43DD5C22FC524E6BA9F8,
	U3CShowReportU3Ed__8_t0FC7BED16CC507978DC41243F381B6413697C651_CustomAttributesCacheGenerator_U3CShowReportU3Ed__8_SetStateMachine_mF89C01371E9C74732EE224A7AD721E42B5D4811B,
	ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager_ShowReport_m82AAF8B941D8209BC4A6E66F60D47C6DA05BF1C4,
	U3CShowReportU3Ed__11_tF052F8F8C61D5A9B0264F88C827391DE74BC3754_CustomAttributesCacheGenerator_U3CShowReportU3Ed__11_SetStateMachine_m64B60D2F27D412000EBC02E03E17CE618D21FA63,
	Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_DisplaySessionRecovery_mFB88B1A4662408536B1D883573A7E007A2350C65,
	Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_OpenRegistration_mA671753110D6512E161BC4339654EF1BC7244C73,
	Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_ShowTask_mAD563FEEEBCFEC90F43969AAB700D51B5282F5C2,
	Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_OpenGlass_mA1C1C072E1E900D54FA97E127965EB347413442E,
	Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_CloseGlass_m15F3B84D4228AA4648D5E908DC37A5DCB39F9AA5,
	Menu_tCC33C701EBE5969F4AF9487A7BC8D78A6363999D_CustomAttributesCacheGenerator_Menu_U3CDisplayU3Eb__10_0_m4CE1F6B68AF2761A163CD5DFF34B47AB8F58E93B,
	U3CDisplaySessionRecoveryU3Ed__9_tAAD8CE782E02557A9AC682F69B533AA7B89A6F19_CustomAttributesCacheGenerator_U3CDisplaySessionRecoveryU3Ed__9_SetStateMachine_mA4D56E1345425B5A401D117D3C056C8DB277A05B,
	U3COpenRegistrationU3Ed__11_tACB7D1D08861E6DEC65C8BD2E66CA817AAECF9E8_CustomAttributesCacheGenerator_U3COpenRegistrationU3Ed__11_SetStateMachine_mA3A20FD1D38023E0F64A5CD1C56656865B33DAAA,
	U3CShowTaskU3Ed__12_t6D82BE13F42B47A8B978A58BB988CD750094F818_CustomAttributesCacheGenerator_U3CShowTaskU3Ed__12_SetStateMachine_m89686BDB9A31B966F1335A339B8117FCBAE57268,
	U3COpenGlassU3Ed__13_t2475D01663179FCE96B1BFD9DD4C600163844002_CustomAttributesCacheGenerator_U3COpenGlassU3Ed__13_SetStateMachine_m9ED18D3957D0525610AD0F55FF69A72436882CC8,
	U3CCloseGlassU3Ed__14_t1240E7C7F56E64C4AFBF9C9011199BE291B0A371_CustomAttributesCacheGenerator_U3CCloseGlassU3Ed__14_SetStateMachine_m1ED0482EDB3E3EE584B7B9DC38A4486384C22C21,
	Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_get_TotalScore_m89ACA46685656CBEF81165B0BBA00130FCB0A802,
	Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_set_TotalScore_m06C1E9C4F531C3479253B4B3F661B9DC16F30081,
	Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_get_Time_m0E3527840B8F3F18F69254A941F0672F30D25910,
	Report_t614566BCB26778B0453B85A8B6FB9893459EC90C_CustomAttributesCacheGenerator_Report_set_Time_mD4FBD3C9749731A772AD9FAE6314E0707716F347,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_FIO_m98367C2563560CFDB69AADBCCC12E4E97F48D113,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_FIO_m36A8729A541D23D9D4E37AC62097746C8A27F95D,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_Group_m82C88E637186D07E6BD0E1EE2D39CF9F2273E1EA,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_Group_mC5CC388E0BB1C8A1E48806B60CD673A3625E5564,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_CreditBook_m73746C8880091B9485391EB38CAE6A9C9C5C46AC,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_CreditBook_m9952D73F94A2E7D2DEC708A5537DB1D3FB4A1896,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_get_NumberOption_mDE5A790E11E24ECE872AC436E4239B7EE058BC73,
	UserData_tD2343AB830CECD0D109DF577460CB83581F8D387_CustomAttributesCacheGenerator_UserData_set_NumberOption_m61D573E3713F5A9FBDE16C3FBF4EFA9354F7D461,
	StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_get_CurrentTime_mDFF26B5F38FF289BBD69FEF831F783687DFE5C85,
	StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_set_CurrentTime_m3983821472F7F97EA35982BCBE57E8347443EDDD,
	StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_Update_m95BC1D186A1D6B31F7888D9C5F507F7B94F0CA01,
	StopWatch_tA00364878CB56A6AE6BA403E6113F87F2161C69B_CustomAttributesCacheGenerator_StopWatch_U3CStartU3Eb__8_0_mB498A8A1E00B0ADD4DD86D30230E36D73D08AEEE,
	U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11__ctor_m3D432530ED9CC09263EF3CD72CE25CFA22A8635D,
	U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_IDisposable_Dispose_m0706329A81C59168D6C15399603C4537C9D6FCCB,
	U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF0512EB602BE3C8C1AEB0BBF2BF1CF6FBA06EFCE,
	U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_Collections_IEnumerator_Reset_m8D09BEB60F2456035598BB01BDBE4361154577B5,
	U3CUpdateU3Ed__11_t856517FFD3ED1DB13B38C29E81041A453A90A276_CustomAttributesCacheGenerator_U3CUpdateU3Ed__11_System_Collections_IEnumerator_get_Current_mF333E020EBE2B9003161602F064B93D1A8589CF8,
	DragAndDropBehaviour_t5DBCAA66242378E7DA320AA1BA07C9F95C93D2BD_CustomAttributesCacheGenerator_DragAndDropBehaviour_Construct_m19507CF26310A7316AE0E59FD160C1600302B214,
	DragAndDropController_tDD314C6D9F4F7C98A8CE7652E6CC2FDE98835784_CustomAttributesCacheGenerator_DragAndDropController_U3CSetMoverU3Eb__13_0_m0064C3D8B1F5410909E0A52ADA8D4C7FB55FB592,
	DragAndDropController_tDD314C6D9F4F7C98A8CE7652E6CC2FDE98835784_CustomAttributesCacheGenerator_DragAndDropController_U3CHandleMouseButtonDownU3Eb__16_0_m3B95A053B0762C12CFFF81C08A5744CCAAFC151E,
	SQLQueryFragment_t7ED0216BCEFAE4DE3CE0BFD105AD6CB83C79B0A2_CustomAttributesCacheGenerator_SQLQueryFragment_Construct_m47D32346D5B88A992B0DF1BC5150069E0BB9DE35,
	SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_SQLQueryFragmentPlace_get_CurrentFragment_mA8FD811A108ED4EF2999840A6A62504D594E5F3E,
	SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_SQLQueryFragmentPlace_set_CurrentFragment_m68F24AEE02B1778012074471D788DD05CF873255,
	SQLQueryFragmentPlace_tB02EC1BE85429E4B37B7F9A66E84B3209B44855A_CustomAttributesCacheGenerator_SQLQueryFragmentPlace_Construct_mF3A3FFAF62F3F9B3BE7E7D97F59A4C13313C9A5A,
	VisualSQLQuery_t88AA278C6F062184A4FFAD1EC91F25824261DA32_CustomAttributesCacheGenerator_VisualSQLQuery_get_Description_mECCD38AE3DABB7C7B276D17B8C8C28E090664287,
	VisualSQLQuery_t88AA278C6F062184A4FFAD1EC91F25824261DA32_CustomAttributesCacheGenerator_VisualSQLQuery_set_Description_m73667C4D4FE12C5C0FBC6AC042B573095B320646,
	VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator_VisualSQLQueryGroup_Construct_m2CD326CAE657C888795DD9B3D031CA719C3C7448,
	ConnectionMap_t79190FE19909D64A6724949C68BFAFAE073CAAC9_CustomAttributesCacheGenerator_ConnectionMap_U3CStartTrackU3Eb__5_0_m9088F01D6DE96BC5152B83593D9F6A38FCE5BF89,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_FirstFragment_mD36C6BFF3C2F9B7754132D38E86AA4C88BAA7C53,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_FirstFragment_m80101B65E3442B7447457B753E262C2EE2EAC79C,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_FirstLine_m2F444BB0816B0BFAB9850453725439D4D0A0BC91,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_FirstLine_m55867E351D4B3569DC01C140FE0E0B5C42BC45FD,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_SecondFragment_m64C76564838E4F75A0EC29A278C1CF34DF5E716A,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_SecondFragment_m3222DF22EE5626287156ECFAD7F9E3996FBD2131,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_get_SecondLine_mB741610EBC5109C8DD1E31ED784B9E3FCD72CA87,
	Connection_tA49FBCE333F70B1ABB8D5E0BF25265F403C2CB7B_CustomAttributesCacheGenerator_Connection_set_SecondLine_m100B1B8D536D258CFF8AB6DB6E641FC215AFF9B7,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_get_Collider_mA3C6D1B276CC7A344151E25BEDEABCB5A2D313CC,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_set_Collider_m7456A3ABA5B2FE79F148827641DCD76A296DDFAD,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_get_Lines_mD4073C1AE5C75265FF768AE0F6998AE8C4E27823,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_set_Lines_mD587BD466F0FBE402EC2723E82A1333F7F155D4F,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_get_LinesChains_m5A9183D316014832FEE2B1A31DF8DD1ED3236980,
	Fragment_t03E69D42D8B4069981358F8DF815BFC28DDFF804_CustomAttributesCacheGenerator_Fragment_set_LinesChains_m37311C9146D49C4DB484450E8EEB6735AFA912A4,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator_LineFragment_get_Number_m84D91CC9F491BF7A8DE2C03A0471742F768EE979,
	LineFragment_tE3AB0A3CDA5EB3316F88428FD918BF1FE014630A_CustomAttributesCacheGenerator_LineFragment_set_Number_m3540B3A497CE0514DEA8C46592A1DCD3AEA572A4,
	Model_t6B00FC8716EF4E2960A0980F3C76275AF4E403B0_CustomAttributesCacheGenerator_Model_CreateFragment_m87E4A19A9A5C9A40539888A7D83069EE079B553E,
	Model_t6B00FC8716EF4E2960A0980F3C76275AF4E403B0_CustomAttributesCacheGenerator_Model_U3CInitializeU3Eb__18_0_m0D98F9175A1EC4F8AA904A18256EF3FF37A3E16A,
	Model_t6B00FC8716EF4E2960A0980F3C76275AF4E403B0_CustomAttributesCacheGenerator_Model_U3CStartUpdateU3Eb__25_0_mA7963A825EAA0F91E224D17D50FD3013C2B4B19F,
	U3CCreateFragmentU3Ed__28_tDBCC1DF85E9D3801E6E8124ACF8FD62B594008FB_CustomAttributesCacheGenerator_U3CCreateFragmentU3Ed__28_SetStateMachine_m61F0AC8AE2106A52C3540BE2DDB85CA13A605FD0,
	U3CU3CInitializeU3Eb__18_0U3Ed_t0A759397F7005992AF5BBEADDD61FB6D99A01AE2_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__18_0U3Ed_SetStateMachine_m5596F358F217B2EAC9567D85E028E2E6F01130F6,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_get_ID_mE380E920FEBCD95F65DB8B799774AC73B591B574,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_get_Name_m8E702E0073024E1BD48075155ED50C2B6AE0217C,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_set_Name_mA0BAA4AB07C95AAED682BB25A22B614512BDBD65,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_get_Fields_m66B7C8A1ABABFC4B0C56B207A9595FA9EE6A7F6C,
	Table_t9125934DF6C76FF649016A96830A9780DE318F56_CustomAttributesCacheGenerator_Table_set_Fields_m3144580CA90297DFA2F73085BD495DBB724ED821,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_ID_m81B0DD81E9C3994EA202102719CC9CC81E5C8339,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_Name_m14BF7B65C2562DCCB8DBD7BE43186F702D1771CC,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_Name_m99A4DFF739E956E1807943DF86C798C928CF75C7,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_Type_m145EA533DF19C3FAB2314A742F5251DAC22151E1,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_Type_mF11BD257B86CBF7720E2744BDCF86B374F7B9C7A,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_Key_m344443E3BF1F4A107D842E23D1A63EB3314D0802,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_Key_m04D61761E1D2852E9FF98AA33793F45C02A62D4F,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_LinkingTable_m81B07C6840986C82521D6D185A96CADB69509974,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_LinkingTable_m1591FF28A5B7F00F8DB52B39E51BD856382AEDD5,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_get_LinkingField_m2E9C8D3EBE625E3C7C2EF1284B3F46297A0196DB,
	Field_t2B94A7E0D3020A891654F3B26AE38DD053946F21_CustomAttributesCacheGenerator_Field_set_LinkingField_m62A67C4103E3014DC7860DABB240F04468F9D243,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_get_Tables_m09493D27D57C1261B0C46A41CDD2D7554741E2CF,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_set_Tables_mDF8846DE74846488B3874325028FCCDCD6DF5CCE,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_add_onUpdatedTables_m71A4C5F597DF7D5A59627FABE5DF20AF44DEA307,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_remove_onUpdatedTables_mE53F8DCDBBFEE03A8B0E75D15B3E268DFD37776A,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_EditTables_mF5117207F05584415E8709C8EA0FD2FD06FDD0D9,
	TableManager_tA0691D2D3478166A5B1A01D5EE8DD884031AE9FE_CustomAttributesCacheGenerator_TableManager_U3CInitializeU3Eb__14_0_m3DD4AC55D5E34D3DC7BB8C1F0FC9F03397177552,
	U3CEditTablesU3Ed__16_t127B7B62A82B256A94497E86001DF832AF91E8A4_CustomAttributesCacheGenerator_U3CEditTablesU3Ed__16_SetStateMachine_mBB6231AC2DD6A0E60AD9829E1B95C139BF4A3B1F,
	U3CU3CInitializeU3Eb__14_0U3Ed_t75030BBEB9C7A5D6B8FF66C3867037A79C8F8926_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__14_0U3Ed_SetStateMachine_m04324A6598D6DF1FC66790DBE18C3B04F25677DF,
	SQLCodeManager_t5DC704E43C51077069D9F27DD852618E0F97A62F_CustomAttributesCacheGenerator_SQLCodeManager_U3CInitializeU3Eb__6_0_m8F76CA6BC23EF9D1BA44A68C845EDDCEE660ADB0,
	EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator_EREntity_ShowLinkDisplayButtons_m7FD36F8EBF3132C7194E9A92ECCB985CD5F2E181,
	EREntity_t9540F67AE49581D36B4A6B038E53F6CDD4FA6CAE_CustomAttributesCacheGenerator_EREntity_UnshowLinkDisplayButtons_mE6D9DC889CBAA961970A340849E5C50B50E6697E,
	U3CShowLinkDisplayButtonsU3Ed__4_t4C2E2600FA89A9011FC9EE8F91B445A66E0599C1_CustomAttributesCacheGenerator_U3CShowLinkDisplayButtonsU3Ed__4_SetStateMachine_mE47BF74EBDF45EA132A80E00E679D49DB64E99BB,
	U3CUnshowLinkDisplayButtonsU3Ed__5_tC45F29F15812835AD4EFEC612F777FDF930C57B2_CustomAttributesCacheGenerator_U3CUnshowLinkDisplayButtonsU3Ed__5_SetStateMachine_mAFD31CFB8945EE2311B318558901134A18F48BC1,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_get_Collider_m0792E6DD000B038C2F10E1E65409D3245E4235B7,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_set_Collider_m68A82980890D17507A3A2BC0645652E1B0B17B65,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_ShowLinkDisplayButtons_m3F4AAC4B6696881CAA9B6AC9B328F0575F86B1FF,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_UnshowLinkDisplayButtons_mC051BC5AAF2DB71958512653734A540119667EF2,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_ActivateLinkDisplayButtons_m1F090D10129FE23DD2925542C60AAA7A050067F2,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_DeactivateLinkDisplayButtons_mD5F67F937566CA834F44673DD9260337D1DEBF07,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_U3CInitU3Eb__25_0_m0922C3EFD8C403C2AB72C902CF90C2B5900F210E,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_U3CInitU3Eb__25_1_m2388AE5175E427CE6784BCA9350EBEFCF6E6312F,
	ERFragment_tAC5DF66BBEDC4639EC3FBFD1E4D4E1141E349DB5_CustomAttributesCacheGenerator_ERFragment_U3CInitU3Eb__25_2_m49D386CFAAB51441D5CACC2146CBF492E9E7C757,
	Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_get_Fragment_m7AF05ECD15D2EB0C6349E935F8EBF753E7D4BF69,
	Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_set_Fragment_m50AD2FC5EE2E9881E468AB70C3C3FD1BEDEF8BF3,
	Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_get_TypeMessage_mB91B10ECD28D1363833F33639F2DA1536D464F17,
	Message_tBC0D16DB6BA6B0A108AC1122815AD97A2A3430BC_CustomAttributesCacheGenerator_Message_set_TypeMessage_m6282482770D3976CB8E83BEC7B01148E9EFF160C,
	U3CShowLinkDisplayButtonsU3Ed__37_t7B2A1D634AA96C6AC293CBBC4529A84A4EEAE829_CustomAttributesCacheGenerator_U3CShowLinkDisplayButtonsU3Ed__37_SetStateMachine_m5FAFD23E9524B9A069D3E6E73828B0C2FFC93085,
	U3CUnshowLinkDisplayButtonsU3Ed__38_tF5C07FE65B249D2F8A254A384EC9ED4885640A15_CustomAttributesCacheGenerator_U3CUnshowLinkDisplayButtonsU3Ed__38_SetStateMachine_m1CB68E36EF5209ABAC644A21D01B8D7D29E967C3,
	U3CActivateLinkDisplayButtonsU3Ed__39_t5211A2E9CF9A881E33BCA03C33A37DB2222A6EDE_CustomAttributesCacheGenerator_U3CActivateLinkDisplayButtonsU3Ed__39_SetStateMachine_m8ADA06384F1B79934412A6C47CB56C7CB1E84C45,
	U3CDeactivateLinkDisplayButtonsU3Ed__40_t385603BD53B747FC3E95C0177802DA0629EE729C_CustomAttributesCacheGenerator_U3CDeactivateLinkDisplayButtonsU3Ed__40_SetStateMachine_m881582DD8156252C91E72C9C5FA62A8E47D9F801,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_ERRelationship_ShowLinkDisplayButtons_mA24CF8467A321A0C6672A3051AAD60CE7AFEDA15,
	ERRelationship_t0A2ACC1E0106782D31992A90C5441E124169CBC1_CustomAttributesCacheGenerator_ERRelationship_UnshowLinkDisplayButtons_m92789CF90DE876B9CCAD22CF51FE3E19E754D400,
	U3CShowLinkDisplayButtonsU3Ed__5_tD23DC9E402559593D0F58F932DF9FABFF9C412EF_CustomAttributesCacheGenerator_U3CShowLinkDisplayButtonsU3Ed__5_SetStateMachine_mB071BFC4AC2977AFE704F64B07AA05AA3086AF95,
	U3CUnshowLinkDisplayButtonsU3Ed__6_tDDB48DE44440BFDD3991FC5960F255A0D81C1B55_CustomAttributesCacheGenerator_U3CUnshowLinkDisplayButtonsU3Ed__6_SetStateMachine_m65BB9EC4E5B8DC540051A76087750CED6C950AE0,
	ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_CreateTransition_mF5711BAD95F1E2E8132FDFD20B34F6D83E71724E,
	ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_0_m7CBFD25F79C090A9FC4FD8E781DAF1911DB632DE,
	ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_1_mC5D4747DC10AAA288B3D49AA2C0769E16CD8F635,
	ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_3_mC85670B8972D1822EE2C583061E3AAC5F0DA91D0,
	ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CSubscribeToMessagesU3Eb__21_5_m4032B450CD99368CF84653482ED4757AF4D8068D,
	ERModel_t5F84D8E5A1AD3FAF1DF7503CBB144C0C33A9A205_CustomAttributesCacheGenerator_ERModel_U3CStartUpdateU3Eb__22_0_mDAA5A6AB52C5E68B2EC196D4B666F664A98E8EE4,
	U3CCreateTransitionU3Ed__26_1_tAA359239D43FAB0BEB185059BED32CF5D8B489F4_CustomAttributesCacheGenerator_U3CCreateTransitionU3Ed__26_1_SetStateMachine_m9B1672DDF3CF2B82088D451BC5354BBE0EF2D2CA,
	ERTransitionCreator_t2292862F8C6AD57EBC71AE50E4B24210D346F15E_CustomAttributesCacheGenerator_ERTransitionCreator_Create_m6A5E73828881EBEC4D727D1E587BC26F2FA8E6FA,
	ERTransitionCreator_t2292862F8C6AD57EBC71AE50E4B24210D346F15E_CustomAttributesCacheGenerator_ERTransitionCreator_UpdateTrackingTransitionCreation_m65A48164D9421715212BBB1429EA4EFD461243DD,
	U3CCreateU3Ed__4_1_t12F6B18A6C0E56F4D1AD2A7E4B336D11F2AB7F9F_CustomAttributesCacheGenerator_U3CCreateU3Ed__4_1_SetStateMachine_mB7EBB404F9979424FF0F93831DCC62A706ABE939,
	U3CUpdateTrackingTransitionCreationU3Ed__5_tBD9EE7811D72011A0853E8756DCA7F17C27313A5_CustomAttributesCacheGenerator_U3CUpdateTrackingTransitionCreationU3Ed__5_SetStateMachine_m38F51C2CF08090B045A97BC8389F1FF160CBA2BF,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_get_FirstFragment_m29BA75C690B51CCF551F400E29D1D765E64C0D44,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_set_FirstFragment_m3C8BAD6E886AB3F0A8E9D938770CF33AC19C6AB5,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_get_SecondFragment_m9E82F524AEBA845FC6F2A9C85B9E1FB413D04104,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_set_SecondFragment_mE69669FEBE198E283777BD002DA4E542C9B78A80,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_U3CEstablishConnectionU3Eb__29_0_mF5B9579857714D4DCF3723AD9FB86C12E591ECA6,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_U3CUpdateConnectionU3Eb__31_0_m9BFE0A8ABC510C65DD26C83B510403D41E401D83,
	ERTransition_t2260260319D53B257BE361FF2C335703AB1A7E66_CustomAttributesCacheGenerator_ERTransition_U3CUpdateConnectionU3Eb__31_1_m4843ACC3903B4088A46DA2E519C640224963CF6D,
	ContainerEntitiesAndRelationships_tA36DCFFB740E70FF1997793A0825BFA0692A5EEB_CustomAttributesCacheGenerator_ContainerEntitiesAndRelationships_U3CInitializeU3Eb__6_0_mD5992A4FE80FF08FBF0D6745C92113A7EFB29B80,
	ContainerEntitiesAndRelationships_tA36DCFFB740E70FF1997793A0825BFA0692A5EEB_CustomAttributesCacheGenerator_ContainerEntitiesAndRelationships_U3CInitializeU3Eb__6_1_mF8A5776CACCA67D576666075CA0F80F16712C6CB,
	U3CU3CInitializeU3Eb__6_0U3Ed_tB6E29CD70B3D5265DD8E8F5DD0823E83D0175C25_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__6_0U3Ed_SetStateMachine_mB4439752281856794143C585BA5017FF9892CDC8,
	U3CU3CInitializeU3Eb__6_1U3Ed_t697B94A3EA9B1A6626AFA616C30B7F01F75308F2_CustomAttributesCacheGenerator_U3CU3CInitializeU3Eb__6_1U3Ed_SetStateMachine_m875AF4B221DC01ABCF9DFE08161AD59EDEB3B66B,
	ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_mFDEB62E2BFF17A7732FDB0A5AE90650F9AE77380____buttons0,
	ButtonObserver_t0B99606EB8FD5EF808CEE722DDB4B05625B33660_CustomAttributesCacheGenerator_ButtonObserver_Track_m937A9E1E10427CADEF8494BF60E49F18719A9AE9____buttons1,
	Glass_t0682D15FDD33F1314844C64DE078C214A3ADB123_CustomAttributesCacheGenerator_Glass_Construct_mB3B5EF6C4A161814828A05C9FB66009F1E4F7929____scaler0,
	InteractiveZone_tF2FA3D885AEFBA87CA8361A6F800CB98E8A7D712_CustomAttributesCacheGenerator_InteractiveZone_Construct_mDC9566F1634F8A1F834302F1690F6010563B52DF____rectTransformCanvas0,
	AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500____FIOInputField0,
	AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500____groupInputField1,
	AuthorizationForm_tB799270662C0ADBDC505416A2E06877CDB4877AB_CustomAttributesCacheGenerator_AuthorizationForm_Init_m9528EC3A960272BFB15844A549DFB3AE474C3500____creditBookInputField2,
	ExitCompletionButton_t713DCD480F78111AF1A77FA9A59B2F14ADF42655_CustomAttributesCacheGenerator_ExitCompletionButton_Construct_mA7BEBAE0F537415AD07F0BD5D1F9F194E04CA879____window0,
	Factory_t91CDCF6D0AF81908EBF3642A02FB83327A4BE974_CustomAttributesCacheGenerator_Factory__ctor_mF2F6BB5C1AA028892CB1A25DEBDB0E31CE89FC13____lockRectZone2,
	Factory_t91CDCF6D0AF81908EBF3642A02FB83327A4BE974_CustomAttributesCacheGenerator_Factory_Create_m88729AD160719FE67807D222D50FFAA5F4698E53____childrens0,
	TaskCompletionButton_t2EA5D9AD9670001FA421E9248D9DE7A1FE9FDBC5_CustomAttributesCacheGenerator_TaskCompletionButton_Construct_mF7CB4D0316029FB9ACC887FAB7A926D9E324AA39____window0,
	UIList_1_t12E3619AB42A062DB5A582B33AE0F5F753E44695_CustomAttributesCacheGenerator_UIList_1_Open_mB7C4270E070F1A8A3CD55553582A7A0DC9874104____additionalUIElements1,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager__ctor_m58A00C4FAB87908EE2422847DBF62E760F2B0B41____reportButton0,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager__ctor_m58A00C4FAB87908EE2422847DBF62E760F2B0B41____saveButton1,
	ButtonManager_tD6F023755A95E10C4534D527FAB413E819F86BFC_CustomAttributesCacheGenerator_ButtonManager__ctor_m58A00C4FAB87908EE2422847DBF62E760F2B0B41____exitButton2,
	ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager__ctor_mB70C09BE42C5142DDB029A516D3CAE1E91481F97____reportButton3,
	ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager__ctor_mB70C09BE42C5142DDB029A516D3CAE1E91481F97____saveButton4,
	ResultManager_tC7700258D9E832DE2F887D742C60A258317F27D8_CustomAttributesCacheGenerator_ResultManager__ctor_mB70C09BE42C5142DDB029A516D3CAE1E91481F97____exitButton5,
	VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D_CustomAttributesCacheGenerator_VisualSQLQueryGroup_tE1674EB668B80EDC55A57064E57C30060B74670D____Current_PropertyInfo,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void InjectAttributeBase_set_Id_m92E78C4EDD8EA83AA980F824A68D26918CAA5134_inline (InjectAttributeBase_tBEA6A6FCBD9B487AAF830C7FD5F089DE74551DC7 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___value0;
		__this->set_U3CIdU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
