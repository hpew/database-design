﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single Packer::ToFloat(System.Single,System.Single,System.Single,System.Single)
extern void Packer_ToFloat_mA7D23F51E9C8A6E62848712A27E2309BC31F94A7 (void);
// 0x00000002 System.Single Packer::ToFloat(UnityEngine.Vector4)
extern void Packer_ToFloat_mC5388DBB78FE07E8AB135B8759A20EAA43AF320E (void);
// 0x00000003 System.Single Packer::ToFloat(System.Single,System.Single,System.Single)
extern void Packer_ToFloat_m7691266B0941E760FB34C506126EEAF8A8FBE1D7 (void);
// 0x00000004 System.Single Packer::ToFloat(System.Single,System.Single)
extern void Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662 (void);
// 0x00000005 System.Int32 Coffee.UIEffects.BaseMaterialEffect::get_parameterIndex()
extern void BaseMaterialEffect_get_parameterIndex_mCEAD6C474EC79C1D3F2E3B840905E10254EFE61F (void);
// 0x00000006 System.Void Coffee.UIEffects.BaseMaterialEffect::set_parameterIndex(System.Int32)
extern void BaseMaterialEffect_set_parameterIndex_m087EF0A73CE6D3519D94F9AD309EB3414A227524 (void);
// 0x00000007 Coffee.UIEffects.ParameterTexture Coffee.UIEffects.BaseMaterialEffect::get_paramTex()
extern void BaseMaterialEffect_get_paramTex_m102366D70593F2CD331F01163A461AD61BE46051 (void);
// 0x00000008 System.Void Coffee.UIEffects.BaseMaterialEffect::SetMaterialDirty()
extern void BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0 (void);
// 0x00000009 UnityEngine.Hash128 Coffee.UIEffects.BaseMaterialEffect::GetMaterialHash(UnityEngine.Material)
extern void BaseMaterialEffect_GetMaterialHash_m56C3761346414A62F442ECB7A389A4F8844744D1 (void);
// 0x0000000A UnityEngine.Material Coffee.UIEffects.BaseMaterialEffect::GetModifiedMaterial(UnityEngine.Material)
extern void BaseMaterialEffect_GetModifiedMaterial_mABA10CC6BFFDFD3A82EEFF3D9156AFD08D071B1A (void);
// 0x0000000B UnityEngine.Material Coffee.UIEffects.BaseMaterialEffect::GetModifiedMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void BaseMaterialEffect_GetModifiedMaterial_mB36BD87123ED2DC4AF906BE069DCAD95AC1A54ED (void);
// 0x0000000C System.Void Coffee.UIEffects.BaseMaterialEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void BaseMaterialEffect_ModifyMaterial_m5B22D41500FF33965FDC9E4843527EC5C535BCD6 (void);
// 0x0000000D System.Void Coffee.UIEffects.BaseMaterialEffect::SetShaderVariants(UnityEngine.Material,System.Object[])
extern void BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1 (void);
// 0x0000000E System.Void Coffee.UIEffects.BaseMaterialEffect::OnEnable()
extern void BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE (void);
// 0x0000000F System.Void Coffee.UIEffects.BaseMaterialEffect::OnDisable()
extern void BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580 (void);
// 0x00000010 System.Void Coffee.UIEffects.BaseMaterialEffect::.ctor()
extern void BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497 (void);
// 0x00000011 System.Void Coffee.UIEffects.BaseMaterialEffect::.cctor()
extern void BaseMaterialEffect__cctor_mE50CD52D2F92D00BB3AD6A076F045C83F8878203 (void);
// 0x00000012 System.Void Coffee.UIEffects.BaseMaterialEffect/<>c::.cctor()
extern void U3CU3Ec__cctor_m0B882E2EB74347504170994F853131BB520A2017 (void);
// 0x00000013 System.Void Coffee.UIEffects.BaseMaterialEffect/<>c::.ctor()
extern void U3CU3Ec__ctor_mFFE62F74902632613ECF890912C74E4243194389 (void);
// 0x00000014 System.Boolean Coffee.UIEffects.BaseMaterialEffect/<>c::<SetShaderVariants>b__15_0(System.Object)
extern void U3CU3Ec_U3CSetShaderVariantsU3Eb__15_0_m86A0BD07D9513900697A9A21089709A5AE45B974 (void);
// 0x00000015 System.String Coffee.UIEffects.BaseMaterialEffect/<>c::<SetShaderVariants>b__15_1(System.Object)
extern void U3CU3Ec_U3CSetShaderVariantsU3Eb__15_1_mB4C34D6B741E7313073BF9AA37C633F2A40B3DB3 (void);
// 0x00000016 Coffee.UIEffects.GraphicConnector Coffee.UIEffects.BaseMeshEffect::get_connector()
extern void BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D (void);
// 0x00000017 UnityEngine.UI.Graphic Coffee.UIEffects.BaseMeshEffect::get_graphic()
extern void BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB (void);
// 0x00000018 UnityEngine.RectTransform Coffee.UIEffects.BaseMeshEffect::get_rectTransform()
extern void BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6 (void);
// 0x00000019 System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.Mesh)
extern void BaseMeshEffect_ModifyMesh_mD039BBBF805029CDB8980BC571AF1C8EEA5DE77B (void);
// 0x0000001A System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper)
extern void BaseMeshEffect_ModifyMesh_m5ED9F4CB50A4155F08EBF97C991240485AD2E794 (void);
// 0x0000001B System.Void Coffee.UIEffects.BaseMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void BaseMeshEffect_ModifyMesh_m6EE8AFAC5195AFA3B2B0073E95ACB45FF3838912 (void);
// 0x0000001C System.Void Coffee.UIEffects.BaseMeshEffect::SetVerticesDirty()
extern void BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9 (void);
// 0x0000001D System.Void Coffee.UIEffects.BaseMeshEffect::OnEnable()
extern void BaseMeshEffect_OnEnable_m900129F36764E412C7FCE57A7C00DB091F48F4C6 (void);
// 0x0000001E System.Void Coffee.UIEffects.BaseMeshEffect::OnDisable()
extern void BaseMeshEffect_OnDisable_m714D32A1FD3155B874FD836C76954208747FED85 (void);
// 0x0000001F System.Void Coffee.UIEffects.BaseMeshEffect::SetEffectParamsDirty()
extern void BaseMeshEffect_SetEffectParamsDirty_m73280E2344924B24DC679377A461ED67FAFCEA19 (void);
// 0x00000020 System.Void Coffee.UIEffects.BaseMeshEffect::OnDidApplyAnimationProperties()
extern void BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E (void);
// 0x00000021 System.Void Coffee.UIEffects.BaseMeshEffect::.ctor()
extern void BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D (void);
// 0x00000022 System.Void Coffee.UIEffects.EffectPlayer::OnEnable(System.Action`1<System.Single>)
extern void EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502 (void);
// 0x00000023 System.Void Coffee.UIEffects.EffectPlayer::OnDisable()
extern void EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2 (void);
// 0x00000024 System.Void Coffee.UIEffects.EffectPlayer::Play(System.Boolean,System.Action`1<System.Single>)
extern void EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802 (void);
// 0x00000025 System.Void Coffee.UIEffects.EffectPlayer::Stop(System.Boolean)
extern void EffectPlayer_Stop_mCDDB096B24E282D85C931BC545CE0C03FE4AF357 (void);
// 0x00000026 System.Void Coffee.UIEffects.EffectPlayer::OnWillRenderCanvases()
extern void EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD (void);
// 0x00000027 System.Void Coffee.UIEffects.EffectPlayer::.ctor()
extern void EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE (void);
// 0x00000028 System.Void Coffee.UIEffects.EffectPlayer/<>c::.cctor()
extern void U3CU3Ec__cctor_m95D50A34703A638FA30A294E31DA8E9AB89FD3D5 (void);
// 0x00000029 System.Void Coffee.UIEffects.EffectPlayer/<>c::.ctor()
extern void U3CU3Ec__ctor_m6D644EF6639B2C8942C4F547E344568976E93B5C (void);
// 0x0000002A System.Void Coffee.UIEffects.EffectPlayer/<>c::<OnEnable>b__7_0()
extern void U3CU3Ec_U3COnEnableU3Eb__7_0_m03524910EABBB0BB877A6F4343AADDF0B2C8B8D2 (void);
// 0x0000002B System.Void Coffee.UIEffects.GraphicConnector::Init()
extern void GraphicConnector_Init_m26072284172DE1108E2D55D752D2DA4AF20A552A (void);
// 0x0000002C System.Void Coffee.UIEffects.GraphicConnector::AddConnector(Coffee.UIEffects.GraphicConnector)
extern void GraphicConnector_AddConnector_m1D64A1D518EE65C19E8A0D2EE1D3B4584F7C3CFE (void);
// 0x0000002D Coffee.UIEffects.GraphicConnector Coffee.UIEffects.GraphicConnector::FindConnector(UnityEngine.UI.Graphic)
extern void GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0 (void);
// 0x0000002E System.Int32 Coffee.UIEffects.GraphicConnector::get_priority()
extern void GraphicConnector_get_priority_m90E013783C0364AC6BBB00E5ED2D6F16CEB1E056 (void);
// 0x0000002F UnityEngine.AdditionalCanvasShaderChannels Coffee.UIEffects.GraphicConnector::get_extraChannel()
extern void GraphicConnector_get_extraChannel_mBACD9FB6B2EFD87E46936C068B0928C983B66493 (void);
// 0x00000030 System.Boolean Coffee.UIEffects.GraphicConnector::IsValid(UnityEngine.UI.Graphic)
extern void GraphicConnector_IsValid_m1C83898406961516C0B3B3EBF11F070397224BB3 (void);
// 0x00000031 UnityEngine.Shader Coffee.UIEffects.GraphicConnector::FindShader(System.String)
extern void GraphicConnector_FindShader_m79BE9DE2146376902CBE2647B0CF83A8409A67D3 (void);
// 0x00000032 System.Void Coffee.UIEffects.GraphicConnector::OnEnable(UnityEngine.UI.Graphic)
extern void GraphicConnector_OnEnable_m0B0E987BB2634B8784713B5B40D091E5CD339638 (void);
// 0x00000033 System.Void Coffee.UIEffects.GraphicConnector::OnDisable(UnityEngine.UI.Graphic)
extern void GraphicConnector_OnDisable_mE8D7E9477569A5C57F19D7003B5E919760130739 (void);
// 0x00000034 System.Void Coffee.UIEffects.GraphicConnector::SetVerticesDirty(UnityEngine.UI.Graphic)
extern void GraphicConnector_SetVerticesDirty_m929125B81A45CC98E05D7A42417D62E9BB99CB20 (void);
// 0x00000035 System.Void Coffee.UIEffects.GraphicConnector::SetMaterialDirty(UnityEngine.UI.Graphic)
extern void GraphicConnector_SetMaterialDirty_m168E165024112455DDCD7AAE3B65926C9890D990 (void);
// 0x00000036 System.Void Coffee.UIEffects.GraphicConnector::GetPositionFactor(Coffee.UIEffects.EffectArea,System.Int32,UnityEngine.Rect,UnityEngine.Vector2,System.Single&,System.Single&)
extern void GraphicConnector_GetPositionFactor_m78368E99AED4E4C093AFE2D0E4608519C14F2B58 (void);
// 0x00000037 System.Boolean Coffee.UIEffects.GraphicConnector::IsText(UnityEngine.UI.Graphic)
extern void GraphicConnector_IsText_mEB728B485D4E21FA91A2CF75597708D345B2DE82 (void);
// 0x00000038 System.Void Coffee.UIEffects.GraphicConnector::SetExtraChannel(UnityEngine.UIVertex&,UnityEngine.Vector2)
extern void GraphicConnector_SetExtraChannel_m494279F3F5E68D70DE0F43EF25B939358E965603 (void);
// 0x00000039 System.Void Coffee.UIEffects.GraphicConnector::GetNormalizedFactor(Coffee.UIEffects.EffectArea,System.Int32,Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2,UnityEngine.Vector2&)
extern void GraphicConnector_GetNormalizedFactor_m6555A1423252B5DDD7A71FA1C5400AE805A50DC3 (void);
// 0x0000003A System.Void Coffee.UIEffects.GraphicConnector::.ctor()
extern void GraphicConnector__ctor_mF95FB5F86269F4423CC5F9DFA370EA4849FF6DAD (void);
// 0x0000003B System.Void Coffee.UIEffects.GraphicConnector::.cctor()
extern void GraphicConnector__cctor_mC73D5A65390F0C216962225BEB934BCBEF9EE522 (void);
// 0x0000003C System.Void Coffee.UIEffects.GraphicConnector/<>c::.cctor()
extern void U3CU3Ec__cctor_m296B372A3F55B194FA47324D2FD76C6B9F46F5C8 (void);
// 0x0000003D System.Void Coffee.UIEffects.GraphicConnector/<>c::.ctor()
extern void U3CU3Ec__ctor_m827FF5CD8B256D37FC61A732312435C6F12041CE (void);
// 0x0000003E System.Int32 Coffee.UIEffects.GraphicConnector/<>c::<AddConnector>b__4_0(Coffee.UIEffects.GraphicConnector,Coffee.UIEffects.GraphicConnector)
extern void U3CU3Ec_U3CAddConnectorU3Eb__4_0_mC99D9E9096B78F00A3509748193374ACB23CBB91 (void);
// 0x0000003F UnityEngine.Material Coffee.UIEffects.MaterialCache::Register(UnityEngine.Material,UnityEngine.Hash128,System.Action`2<UnityEngine.Material,UnityEngine.UI.Graphic>,UnityEngine.UI.Graphic)
extern void MaterialCache_Register_m41B9F13914C2BBF0F30CC45DBF8E8713E15079F8 (void);
// 0x00000040 System.Void Coffee.UIEffects.MaterialCache::Unregister(UnityEngine.Hash128)
extern void MaterialCache_Unregister_m5F0CFE2E6BFD7A6D3E0862D6034B1CB4F3A5B394 (void);
// 0x00000041 System.Void Coffee.UIEffects.MaterialCache::.ctor()
extern void MaterialCache__ctor_mA988DB48B7DC46EC3020C7DEC2DCBBD9AD785C8A (void);
// 0x00000042 System.Void Coffee.UIEffects.MaterialCache::.cctor()
extern void MaterialCache__cctor_m46A748483AC54F7C2C85EF3ED75A0247BA85E583 (void);
// 0x00000043 System.Void Coffee.UIEffects.MaterialCache/MaterialEntry::Release()
extern void MaterialEntry_Release_mB302360107E7F32A760184279DA7E3D24149498E (void);
// 0x00000044 System.Void Coffee.UIEffects.MaterialCache/MaterialEntry::.ctor()
extern void MaterialEntry__ctor_m409D7E4C408065F797EFAC708ADD6836DDAD7FC8 (void);
// 0x00000045 System.Void Coffee.UIEffects.Matrix2x3::.ctor(UnityEngine.Rect,System.Single,System.Single)
extern void Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE (void);
// 0x00000046 UnityEngine.Vector2 Coffee.UIEffects.Matrix2x3::op_Multiply(Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2)
extern void Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0 (void);
// 0x00000047 System.Int32 Coffee.UIEffects.IParameterTexture::get_parameterIndex()
// 0x00000048 System.Void Coffee.UIEffects.IParameterTexture::set_parameterIndex(System.Int32)
// 0x00000049 Coffee.UIEffects.ParameterTexture Coffee.UIEffects.IParameterTexture::get_paramTex()
// 0x0000004A System.Void Coffee.UIEffects.ParameterTexture::.ctor(System.Int32,System.Int32,System.String)
extern void ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876 (void);
// 0x0000004B System.Void Coffee.UIEffects.ParameterTexture::Register(Coffee.UIEffects.IParameterTexture)
extern void ParameterTexture_Register_m418624C23515BB36BFE2E30FDC66AB2DD855D62A (void);
// 0x0000004C System.Void Coffee.UIEffects.ParameterTexture::Unregister(Coffee.UIEffects.IParameterTexture)
extern void ParameterTexture_Unregister_mC8283ADEEC7D0B15321727EC445C0909671918BC (void);
// 0x0000004D System.Void Coffee.UIEffects.ParameterTexture::SetData(Coffee.UIEffects.IParameterTexture,System.Int32,System.Byte)
extern void ParameterTexture_SetData_m3870BC4215A7F85A68A38D2E6B50DB27D65284BB (void);
// 0x0000004E System.Void Coffee.UIEffects.ParameterTexture::SetData(Coffee.UIEffects.IParameterTexture,System.Int32,System.Single)
extern void ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F (void);
// 0x0000004F System.Void Coffee.UIEffects.ParameterTexture::RegisterMaterial(UnityEngine.Material)
extern void ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9 (void);
// 0x00000050 System.Single Coffee.UIEffects.ParameterTexture::GetNormalizedIndex(Coffee.UIEffects.IParameterTexture)
extern void ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5 (void);
// 0x00000051 System.Void Coffee.UIEffects.ParameterTexture::Initialize()
extern void ParameterTexture_Initialize_m947A572C82BF5BB6BC7A84E95F3498401183F29A (void);
// 0x00000052 System.Void Coffee.UIEffects.ParameterTexture::UpdateParameterTexture()
extern void ParameterTexture_UpdateParameterTexture_mF9DFFADF593F9954190F1161E8C2E117077B2B63 (void);
// 0x00000053 System.Void Coffee.UIEffects.ParameterTexture/<>c::.cctor()
extern void U3CU3Ec__cctor_mABB703F0252422D599BC010492D66F0782FFB526 (void);
// 0x00000054 System.Void Coffee.UIEffects.ParameterTexture/<>c::.ctor()
extern void U3CU3Ec__ctor_m685DAAD421CE9C891A571D72713AD4D024D5B079 (void);
// 0x00000055 System.Void Coffee.UIEffects.ParameterTexture/<>c::<Initialize>b__16_0()
extern void U3CU3Ec_U3CInitializeU3Eb__16_0_mD35535E6182BFEEE7882780DE18A230F2611C9E9 (void);
// 0x00000056 UnityEngine.Rect Coffee.UIEffects.EffectAreaExtensions::GetEffectArea(Coffee.UIEffects.EffectArea,UnityEngine.UI.VertexHelper,UnityEngine.Rect,System.Single)
extern void EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7 (void);
// 0x00000057 System.Void Coffee.UIEffects.EffectAreaExtensions::GetPositionFactor(Coffee.UIEffects.EffectArea,System.Int32,UnityEngine.Rect,UnityEngine.Vector2,System.Boolean,System.Boolean,System.Single&,System.Single&)
extern void EffectAreaExtensions_GetPositionFactor_m133BD727A155DAC41FE3FD89F266BA037EEA56AF (void);
// 0x00000058 System.Void Coffee.UIEffects.EffectAreaExtensions::GetNormalizedFactor(Coffee.UIEffects.EffectArea,System.Int32,Coffee.UIEffects.Matrix2x3,UnityEngine.Vector2,System.Boolean,UnityEngine.Vector2&)
extern void EffectAreaExtensions_GetNormalizedFactor_mE54719FA9D4F34F2EEAF3418B47B37E3F0DA67FF (void);
// 0x00000059 System.Void Coffee.UIEffects.EffectAreaExtensions::.cctor()
extern void EffectAreaExtensions__cctor_m62D31F3EED0577276B9296CD675BEE743245ABE5 (void);
// 0x0000005A System.Single Coffee.UIEffects.UIDissolve::get_effectFactor()
extern void UIDissolve_get_effectFactor_mB944EE30EB78C6EF26D9BBFD3E0E3F4949831D22 (void);
// 0x0000005B System.Void Coffee.UIEffects.UIDissolve::set_effectFactor(System.Single)
extern void UIDissolve_set_effectFactor_mDAD33D00CAF5DB3E62CFDFE65263D7A6E59C0FC2 (void);
// 0x0000005C System.Single Coffee.UIEffects.UIDissolve::get_width()
extern void UIDissolve_get_width_m05E9EDE3012FAF89A60F49C567612C3459F70DD0 (void);
// 0x0000005D System.Void Coffee.UIEffects.UIDissolve::set_width(System.Single)
extern void UIDissolve_set_width_m4260A791CD3364EA0823369787298EB1F7C95EBE (void);
// 0x0000005E System.Single Coffee.UIEffects.UIDissolve::get_softness()
extern void UIDissolve_get_softness_m1057EF2111B6739297278CBFA3395B682721C7E9 (void);
// 0x0000005F System.Void Coffee.UIEffects.UIDissolve::set_softness(System.Single)
extern void UIDissolve_set_softness_m937B3F02AF8B03419481A65756AE2581E24B1E47 (void);
// 0x00000060 UnityEngine.Color Coffee.UIEffects.UIDissolve::get_color()
extern void UIDissolve_get_color_mEB32F760AD1BD1125D983FC1B313022EC1248359 (void);
// 0x00000061 System.Void Coffee.UIEffects.UIDissolve::set_color(UnityEngine.Color)
extern void UIDissolve_set_color_mCCDFC676B00E5090890B47C5EF1FAB654AF10BA9 (void);
// 0x00000062 UnityEngine.Texture Coffee.UIEffects.UIDissolve::get_transitionTexture()
extern void UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792 (void);
// 0x00000063 System.Void Coffee.UIEffects.UIDissolve::set_transitionTexture(UnityEngine.Texture)
extern void UIDissolve_set_transitionTexture_m033F2F344098C2C1879D4D73B03EF787B8380238 (void);
// 0x00000064 UnityEngine.Texture Coffee.UIEffects.UIDissolve::get_defaultTransitionTexture()
extern void UIDissolve_get_defaultTransitionTexture_m16638E0BF5F9B8E7E089B2BAF0D5B5325CDBB0D4 (void);
// 0x00000065 Coffee.UIEffects.EffectArea Coffee.UIEffects.UIDissolve::get_effectArea()
extern void UIDissolve_get_effectArea_m12B5A1099A85CEA396F92CCF83EC37E43C65DC82 (void);
// 0x00000066 System.Void Coffee.UIEffects.UIDissolve::set_effectArea(Coffee.UIEffects.EffectArea)
extern void UIDissolve_set_effectArea_mC95BE4CAD13FFD675BF38A4A6EDA16896D0B3C05 (void);
// 0x00000067 System.Boolean Coffee.UIEffects.UIDissolve::get_keepAspectRatio()
extern void UIDissolve_get_keepAspectRatio_mF2B7DBCF08E8EA6D7494006ED4F377C660ED5874 (void);
// 0x00000068 System.Void Coffee.UIEffects.UIDissolve::set_keepAspectRatio(System.Boolean)
extern void UIDissolve_set_keepAspectRatio_m9BA74A15B0B3E6AFEFCD30F9A4502F2EFF850B92 (void);
// 0x00000069 Coffee.UIEffects.ColorMode Coffee.UIEffects.UIDissolve::get_colorMode()
extern void UIDissolve_get_colorMode_m500352BC8F5DD2FE5EF279D4D94F2CEFD572C2D8 (void);
// 0x0000006A System.Void Coffee.UIEffects.UIDissolve::set_colorMode(Coffee.UIEffects.ColorMode)
extern void UIDissolve_set_colorMode_mFE133DE54ACEADE97857F52F22473D7714B362E5 (void);
// 0x0000006B Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIDissolve::get_paramTex()
extern void UIDissolve_get_paramTex_mC7CD59F8E1E87B0FC575638D6D6182C7CF21B55C (void);
// 0x0000006C Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIDissolve::get_effectPlayer()
extern void UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A (void);
// 0x0000006D UnityEngine.Hash128 Coffee.UIEffects.UIDissolve::GetMaterialHash(UnityEngine.Material)
extern void UIDissolve_GetMaterialHash_m92BC68A7FE25F1F1130B46A13B67C9D49BAFB392 (void);
// 0x0000006E System.Void Coffee.UIEffects.UIDissolve::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void UIDissolve_ModifyMaterial_m864A13BF4E46CED12F1C1C03C70C3AD540E0D46D (void);
// 0x0000006F System.Void Coffee.UIEffects.UIDissolve::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIDissolve_ModifyMesh_m7BA4DB2233B3E73643612C8E08256D896A8B77FB (void);
// 0x00000070 System.Void Coffee.UIEffects.UIDissolve::SetEffectParamsDirty()
extern void UIDissolve_SetEffectParamsDirty_m654479EEBFC40E3EC6A07438BC27CF1BC4A33D83 (void);
// 0x00000071 System.Void Coffee.UIEffects.UIDissolve::SetVerticesDirty()
extern void UIDissolve_SetVerticesDirty_mDFDAC5475D74749DE0261E86EAC59E16A03F396E (void);
// 0x00000072 System.Void Coffee.UIEffects.UIDissolve::OnDidApplyAnimationProperties()
extern void UIDissolve_OnDidApplyAnimationProperties_m74FCBDF6CEC9CB6D293687D6651CC4F36A5E5E3E (void);
// 0x00000073 System.Void Coffee.UIEffects.UIDissolve::Play(System.Boolean)
extern void UIDissolve_Play_m11CEE7F8593F191A9B3A6E7908318DB68F4D0822 (void);
// 0x00000074 System.Void Coffee.UIEffects.UIDissolve::Stop(System.Boolean)
extern void UIDissolve_Stop_mB2C1169F0638C0692559A4366BFA2C3D7BADB676 (void);
// 0x00000075 System.Void Coffee.UIEffects.UIDissolve::OnEnable()
extern void UIDissolve_OnEnable_m185245A83DA598463AED735F1E901B86AB945B7B (void);
// 0x00000076 System.Void Coffee.UIEffects.UIDissolve::OnDisable()
extern void UIDissolve_OnDisable_m1381CC4F4C83657F28506892C9B8C12BC966B774 (void);
// 0x00000077 System.Void Coffee.UIEffects.UIDissolve::.ctor()
extern void UIDissolve__ctor_m99E95E10A3C975048CAE46A47A0784384791777D (void);
// 0x00000078 System.Void Coffee.UIEffects.UIDissolve::.cctor()
extern void UIDissolve__cctor_mBF082F322F7071A5B1CCFBF4987AF53ABB84765F (void);
// 0x00000079 System.Void Coffee.UIEffects.UIDissolve::<OnEnable>b__54_0(System.Single)
extern void UIDissolve_U3COnEnableU3Eb__54_0_m6FEE944A61838694BDB117ADC92E7733C84C07AF (void);
// 0x0000007A UnityEngine.AdditionalCanvasShaderChannels Coffee.UIEffects.UIEffect::get_uvMaskChannel()
extern void UIEffect_get_uvMaskChannel_m321DED93A70D6F9C73D1709E4DC7112E977B9404 (void);
// 0x0000007B System.Single Coffee.UIEffects.UIEffect::get_effectFactor()
extern void UIEffect_get_effectFactor_m00273C0BB3B80F99858764F8D80FD8D1BD7CB92E (void);
// 0x0000007C System.Void Coffee.UIEffects.UIEffect::set_effectFactor(System.Single)
extern void UIEffect_set_effectFactor_m4CE7979B846A9CC8968C5C50A4333743F9925674 (void);
// 0x0000007D System.Single Coffee.UIEffects.UIEffect::get_colorFactor()
extern void UIEffect_get_colorFactor_m75D2A042A8576CEAC03C4B798E4F7DB235AC6ABD (void);
// 0x0000007E System.Void Coffee.UIEffects.UIEffect::set_colorFactor(System.Single)
extern void UIEffect_set_colorFactor_m8AB6B802918563B908F4E37AD6BE06592B626864 (void);
// 0x0000007F System.Single Coffee.UIEffects.UIEffect::get_blurFactor()
extern void UIEffect_get_blurFactor_mB06AFF2FC0044E3ECBD81DBF2C9303FB908E81A4 (void);
// 0x00000080 System.Void Coffee.UIEffects.UIEffect::set_blurFactor(System.Single)
extern void UIEffect_set_blurFactor_m3C443FD10A75407F541E6EB620B9B55EF85F90F3 (void);
// 0x00000081 Coffee.UIEffects.EffectMode Coffee.UIEffects.UIEffect::get_effectMode()
extern void UIEffect_get_effectMode_m4D2487E963178FE58E1AA2FE976227C0B4645C1F (void);
// 0x00000082 System.Void Coffee.UIEffects.UIEffect::set_effectMode(Coffee.UIEffects.EffectMode)
extern void UIEffect_set_effectMode_mCFE02D088374C730DD8E35992E50FD1E1F574991 (void);
// 0x00000083 Coffee.UIEffects.ColorMode Coffee.UIEffects.UIEffect::get_colorMode()
extern void UIEffect_get_colorMode_m7A813F78AF2153B5C7A381D7F29A1D9C4511E664 (void);
// 0x00000084 System.Void Coffee.UIEffects.UIEffect::set_colorMode(Coffee.UIEffects.ColorMode)
extern void UIEffect_set_colorMode_m606B079D79B95DA229EF15B571723CEDE7110309 (void);
// 0x00000085 Coffee.UIEffects.BlurMode Coffee.UIEffects.UIEffect::get_blurMode()
extern void UIEffect_get_blurMode_m97D9F46E04E8B291EEB1E7ADCAE27842981FA6DD (void);
// 0x00000086 System.Void Coffee.UIEffects.UIEffect::set_blurMode(Coffee.UIEffects.BlurMode)
extern void UIEffect_set_blurMode_m92E5F74746B132960206951A9BCD75B16F1C8ADA (void);
// 0x00000087 Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIEffect::get_paramTex()
extern void UIEffect_get_paramTex_m9108661AB3CFCE2A89F16E49DEAD847166E30EDF (void);
// 0x00000088 System.Boolean Coffee.UIEffects.UIEffect::get_advancedBlur()
extern void UIEffect_get_advancedBlur_m37BF17E16B82618676E23CF6338093ECA5AEB4AD (void);
// 0x00000089 System.Void Coffee.UIEffects.UIEffect::set_advancedBlur(System.Boolean)
extern void UIEffect_set_advancedBlur_m9CAFAD3649A2C0F169B9F8B421AD0E94EDACC140 (void);
// 0x0000008A UnityEngine.Hash128 Coffee.UIEffects.UIEffect::GetMaterialHash(UnityEngine.Material)
extern void UIEffect_GetMaterialHash_mB81CBA44D34978619BA96B13CFFD3491CE6700F0 (void);
// 0x0000008B System.Void Coffee.UIEffects.UIEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void UIEffect_ModifyMaterial_mBB59C215B8E1F198565EDEB57537247DD4D48DAA (void);
// 0x0000008C System.Void Coffee.UIEffects.UIEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIEffect_ModifyMesh_m4633FC2713ED5D9CE152E9FFC484F89F9554B543 (void);
// 0x0000008D System.Void Coffee.UIEffects.UIEffect::SetEffectParamsDirty()
extern void UIEffect_SetEffectParamsDirty_mFAEDBE0F8F4804226CB2B62195BEBFEF996B2476 (void);
// 0x0000008E System.Void Coffee.UIEffects.UIEffect::GetBounds(System.Collections.Generic.List`1<UnityEngine.UIVertex>,System.Int32,System.Int32,UnityEngine.Rect&,UnityEngine.Rect&,System.Boolean)
extern void UIEffect_GetBounds_m35577243A9C58E39854A2538290A13D64C205BAD (void);
// 0x0000008F System.Void Coffee.UIEffects.UIEffect::.ctor()
extern void UIEffect__ctor_mEC430F1FC686DCDF70EE103A523A09F8E19DA37F (void);
// 0x00000090 System.Void Coffee.UIEffects.UIEffect::.cctor()
extern void UIEffect__cctor_m0BEF19CA78CE673FB7B29883F6705793CEB2DB33 (void);
// 0x00000091 System.Boolean Coffee.UIEffects.UIFlip::get_horizontal()
extern void UIFlip_get_horizontal_mF54C27E018057B8F9A36E7CA2E6FADA1D7743114 (void);
// 0x00000092 System.Void Coffee.UIEffects.UIFlip::set_horizontal(System.Boolean)
extern void UIFlip_set_horizontal_m918D709FB4B6888F5E24F3712CEC77AA68DB5E37 (void);
// 0x00000093 System.Boolean Coffee.UIEffects.UIFlip::get_vertical()
extern void UIFlip_get_vertical_m0250BFEECB51A1633A09D2CF5C705D1587C3D06A (void);
// 0x00000094 System.Void Coffee.UIEffects.UIFlip::set_vertical(System.Boolean)
extern void UIFlip_set_vertical_m71A359FCC21F9D5DE5C9007DBC4A520FC4CA5F62 (void);
// 0x00000095 System.Void Coffee.UIEffects.UIFlip::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIFlip_ModifyMesh_m79A6FC275515C150F5FCABE88B97ED4BD21A1645 (void);
// 0x00000096 System.Void Coffee.UIEffects.UIFlip::.ctor()
extern void UIFlip__ctor_mD4CE3113E5EA38CB61767AD622C84063715A94E3 (void);
// 0x00000097 Coffee.UIEffects.UIGradient/Direction Coffee.UIEffects.UIGradient::get_direction()
extern void UIGradient_get_direction_m094F3EB30335D4C941FFE1C9CD92A1EA46FFD209 (void);
// 0x00000098 System.Void Coffee.UIEffects.UIGradient::set_direction(Coffee.UIEffects.UIGradient/Direction)
extern void UIGradient_set_direction_m978ABB95A3EE5868DC04D333CCA44D48CD79CA54 (void);
// 0x00000099 UnityEngine.Color Coffee.UIEffects.UIGradient::get_color1()
extern void UIGradient_get_color1_m423D956F1D7112DA243036673C5132976AAAAC38 (void);
// 0x0000009A System.Void Coffee.UIEffects.UIGradient::set_color1(UnityEngine.Color)
extern void UIGradient_set_color1_mE085DBC6446EFEF5659869CAB0A24388A22BC0F4 (void);
// 0x0000009B UnityEngine.Color Coffee.UIEffects.UIGradient::get_color2()
extern void UIGradient_get_color2_mF8239D06170F4FB39247E60942112B738997056E (void);
// 0x0000009C System.Void Coffee.UIEffects.UIGradient::set_color2(UnityEngine.Color)
extern void UIGradient_set_color2_mFCC6472DFDAF34B009A11A581EE40A2FDF698C24 (void);
// 0x0000009D UnityEngine.Color Coffee.UIEffects.UIGradient::get_color3()
extern void UIGradient_get_color3_m6A6ADF7604891CF1BE6BAFBAA652F1CA2E97C7FC (void);
// 0x0000009E System.Void Coffee.UIEffects.UIGradient::set_color3(UnityEngine.Color)
extern void UIGradient_set_color3_m5866EAB3CF8D10559C80944A1CD53125CF994551 (void);
// 0x0000009F UnityEngine.Color Coffee.UIEffects.UIGradient::get_color4()
extern void UIGradient_get_color4_mA95E03D16F86A39D3003BC0390629E40E3859298 (void);
// 0x000000A0 System.Void Coffee.UIEffects.UIGradient::set_color4(UnityEngine.Color)
extern void UIGradient_set_color4_m0A4217B2A704F895A15AD5745970225D7047C837 (void);
// 0x000000A1 System.Single Coffee.UIEffects.UIGradient::get_rotation()
extern void UIGradient_get_rotation_m77FADBF0C5FC07638849FBB32F7DC3739876108E (void);
// 0x000000A2 System.Void Coffee.UIEffects.UIGradient::set_rotation(System.Single)
extern void UIGradient_set_rotation_m4581F1FA269346868CC5FFC6A716ACDAF59E45A7 (void);
// 0x000000A3 System.Single Coffee.UIEffects.UIGradient::get_offset()
extern void UIGradient_get_offset_mF798A5666929324378FD513ED51C988EE5091887 (void);
// 0x000000A4 System.Void Coffee.UIEffects.UIGradient::set_offset(System.Single)
extern void UIGradient_set_offset_m3F3A747D71F33B04448E557E6183FDD2E4849EEA (void);
// 0x000000A5 UnityEngine.Vector2 Coffee.UIEffects.UIGradient::get_offset2()
extern void UIGradient_get_offset2_m5B5F98FAADFFC9ACD34BC208000DCC0D0318507C (void);
// 0x000000A6 System.Void Coffee.UIEffects.UIGradient::set_offset2(UnityEngine.Vector2)
extern void UIGradient_set_offset2_m6941C3CAB75BFAD13296ED00F6E3A83865C830B2 (void);
// 0x000000A7 Coffee.UIEffects.UIGradient/GradientStyle Coffee.UIEffects.UIGradient::get_gradientStyle()
extern void UIGradient_get_gradientStyle_mE294AE9A685ED8906035884704E0C7158764840D (void);
// 0x000000A8 System.Void Coffee.UIEffects.UIGradient::set_gradientStyle(Coffee.UIEffects.UIGradient/GradientStyle)
extern void UIGradient_set_gradientStyle_m44B6AB97EA9E06EF31A34906E03AA0A41EB2D30B (void);
// 0x000000A9 UnityEngine.ColorSpace Coffee.UIEffects.UIGradient::get_colorSpace()
extern void UIGradient_get_colorSpace_m7BC8625644E03D801B620B418B219676DB742020 (void);
// 0x000000AA System.Void Coffee.UIEffects.UIGradient::set_colorSpace(UnityEngine.ColorSpace)
extern void UIGradient_set_colorSpace_m2336F2412186992DBFFE0072A402C840E06FF989 (void);
// 0x000000AB System.Boolean Coffee.UIEffects.UIGradient::get_ignoreAspectRatio()
extern void UIGradient_get_ignoreAspectRatio_mD64D86E1D8FFD9BEC4606ED2C11257E9D9053AA2 (void);
// 0x000000AC System.Void Coffee.UIEffects.UIGradient::set_ignoreAspectRatio(System.Boolean)
extern void UIGradient_set_ignoreAspectRatio_m1F52E4804269268E898C04B8ED1C618E313316C2 (void);
// 0x000000AD System.Void Coffee.UIEffects.UIGradient::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIGradient_ModifyMesh_m23B0DAD902A385843C277C66D01B5359B84B40C6 (void);
// 0x000000AE System.Void Coffee.UIEffects.UIGradient::.ctor()
extern void UIGradient__ctor_m9400C38CE25C5FA85BE64AA54384FC7F16F8FE6F (void);
// 0x000000AF System.Void Coffee.UIEffects.UIGradient::.cctor()
extern void UIGradient__cctor_mEAC8743086EA5920BBF96651F4F48D4BA01FB8B2 (void);
// 0x000000B0 UnityEngine.Color Coffee.UIEffects.UIHsvModifier::get_targetColor()
extern void UIHsvModifier_get_targetColor_mB8E7080EDD10DA0D149EF2017910EC4F13119D2E (void);
// 0x000000B1 System.Void Coffee.UIEffects.UIHsvModifier::set_targetColor(UnityEngine.Color)
extern void UIHsvModifier_set_targetColor_m57B784581FD98B38E5EA7CD8CA7232B58D2D6CB0 (void);
// 0x000000B2 System.Single Coffee.UIEffects.UIHsvModifier::get_range()
extern void UIHsvModifier_get_range_m7C1D0EEBFBD69B2B32DD4C12943B56C36CD3DC5C (void);
// 0x000000B3 System.Void Coffee.UIEffects.UIHsvModifier::set_range(System.Single)
extern void UIHsvModifier_set_range_m622FDB03F2E37F5D3832E501B66D9F4959CBF8E6 (void);
// 0x000000B4 System.Single Coffee.UIEffects.UIHsvModifier::get_saturation()
extern void UIHsvModifier_get_saturation_mE5AD1F97E367095DBD1EA452DD24E46C2F037E17 (void);
// 0x000000B5 System.Void Coffee.UIEffects.UIHsvModifier::set_saturation(System.Single)
extern void UIHsvModifier_set_saturation_m01D7FA3519D65959C9D084A452395571EAB02077 (void);
// 0x000000B6 System.Single Coffee.UIEffects.UIHsvModifier::get_value()
extern void UIHsvModifier_get_value_mE2043521CB9AA0AA3A812D9468DECDB25D2E801F (void);
// 0x000000B7 System.Void Coffee.UIEffects.UIHsvModifier::set_value(System.Single)
extern void UIHsvModifier_set_value_m789131F4CD1C18A59D44F32AE3E29CC5BA34D732 (void);
// 0x000000B8 System.Single Coffee.UIEffects.UIHsvModifier::get_hue()
extern void UIHsvModifier_get_hue_m17FACAC80A9C5EF5AF4271A9274366B37DDA8174 (void);
// 0x000000B9 System.Void Coffee.UIEffects.UIHsvModifier::set_hue(System.Single)
extern void UIHsvModifier_set_hue_m7404ED8A0A744D41C1DC729258A4BF3D8E9D9D92 (void);
// 0x000000BA Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIHsvModifier::get_paramTex()
extern void UIHsvModifier_get_paramTex_m6B4568923620E4AE1805B1C133E1332D38D33B85 (void);
// 0x000000BB UnityEngine.Hash128 Coffee.UIEffects.UIHsvModifier::GetMaterialHash(UnityEngine.Material)
extern void UIHsvModifier_GetMaterialHash_m77225F452DCD129FA334029C9DC064B6935F4715 (void);
// 0x000000BC System.Void Coffee.UIEffects.UIHsvModifier::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void UIHsvModifier_ModifyMaterial_m1944A64071017AB765A700C75EC6787B4CA43887 (void);
// 0x000000BD System.Void Coffee.UIEffects.UIHsvModifier::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIHsvModifier_ModifyMesh_m224465843842D791A0E3A965E20C3F630DBE7B94 (void);
// 0x000000BE System.Void Coffee.UIEffects.UIHsvModifier::SetEffectParamsDirty()
extern void UIHsvModifier_SetEffectParamsDirty_m786AD408DE613467C527EAD7AA3DB1F3868B0409 (void);
// 0x000000BF System.Void Coffee.UIEffects.UIHsvModifier::.ctor()
extern void UIHsvModifier__ctor_m96CF14648BB39439DEA95EC2DFEEE7946B9A822A (void);
// 0x000000C0 System.Void Coffee.UIEffects.UIHsvModifier::.cctor()
extern void UIHsvModifier__cctor_mBFF73E20AFB72787C7E5240BAEEAF7B0366CE491 (void);
// 0x000000C1 UnityEngine.Color Coffee.UIEffects.UIShadow::get_effectColor()
extern void UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38 (void);
// 0x000000C2 System.Void Coffee.UIEffects.UIShadow::set_effectColor(UnityEngine.Color)
extern void UIShadow_set_effectColor_m6D7B016793A2C176AA66F257BD3C53BFD71297EC (void);
// 0x000000C3 UnityEngine.Vector2 Coffee.UIEffects.UIShadow::get_effectDistance()
extern void UIShadow_get_effectDistance_m2AB1626DC40D34C0D91FF336342F938195B201EE (void);
// 0x000000C4 System.Void Coffee.UIEffects.UIShadow::set_effectDistance(UnityEngine.Vector2)
extern void UIShadow_set_effectDistance_m267278942B1CE1058C014208F2D79948626A99BC (void);
// 0x000000C5 System.Boolean Coffee.UIEffects.UIShadow::get_useGraphicAlpha()
extern void UIShadow_get_useGraphicAlpha_mEC09F1BCF8D368129B771A94C129F048C7C0CE1E (void);
// 0x000000C6 System.Void Coffee.UIEffects.UIShadow::set_useGraphicAlpha(System.Boolean)
extern void UIShadow_set_useGraphicAlpha_m2A048380CA0E931D640C1B26A68B7C8E57798F7D (void);
// 0x000000C7 System.Single Coffee.UIEffects.UIShadow::get_blurFactor()
extern void UIShadow_get_blurFactor_m5A7E95CE6D4AC039A6C84A95C9293FA23B7002E8 (void);
// 0x000000C8 System.Void Coffee.UIEffects.UIShadow::set_blurFactor(System.Single)
extern void UIShadow_set_blurFactor_mDCFC5282751C9A98A4D2163964AB5A688BE1D079 (void);
// 0x000000C9 Coffee.UIEffects.ShadowStyle Coffee.UIEffects.UIShadow::get_style()
extern void UIShadow_get_style_mCAA0D48FB84802EBB12777BD82587CF6B1A20379 (void);
// 0x000000CA System.Void Coffee.UIEffects.UIShadow::set_style(Coffee.UIEffects.ShadowStyle)
extern void UIShadow_set_style_m57011D0B6E5E8F3E16B1E6B24272D2348068C555 (void);
// 0x000000CB System.Int32 Coffee.UIEffects.UIShadow::get_parameterIndex()
extern void UIShadow_get_parameterIndex_m421844BF83F2BD63CB36E171DCA3A4173B1E0D94 (void);
// 0x000000CC System.Void Coffee.UIEffects.UIShadow::set_parameterIndex(System.Int32)
extern void UIShadow_set_parameterIndex_mC140571AED150F3BB3FFCF5029F8D9878E54EE4A (void);
// 0x000000CD Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShadow::get_paramTex()
extern void UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2 (void);
// 0x000000CE System.Void Coffee.UIEffects.UIShadow::set_paramTex(Coffee.UIEffects.ParameterTexture)
extern void UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010 (void);
// 0x000000CF System.Void Coffee.UIEffects.UIShadow::OnEnable()
extern void UIShadow_OnEnable_m60DEFD2F94DC112AE059AF3E9D47FB3A65D3125C (void);
// 0x000000D0 System.Void Coffee.UIEffects.UIShadow::OnDisable()
extern void UIShadow_OnDisable_m86DD53B994C6902D25002910BFA026E3057E53BB (void);
// 0x000000D1 System.Void Coffee.UIEffects.UIShadow::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIShadow_ModifyMesh_m99583022DC19BEAAD324A9F7E22FF313129EB38A (void);
// 0x000000D2 System.Void Coffee.UIEffects.UIShadow::ApplyShadow(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color,System.Int32&,System.Int32&,UnityEngine.Vector2,Coffee.UIEffects.ShadowStyle,System.Boolean)
extern void UIShadow_ApplyShadow_m5422EC048717AA600AD8A06FCD534E94350034E1 (void);
// 0x000000D3 System.Void Coffee.UIEffects.UIShadow::ApplyShadowZeroAlloc(System.Collections.Generic.List`1<UnityEngine.UIVertex>,UnityEngine.Color,System.Int32&,System.Int32&,System.Single,System.Single,System.Boolean)
extern void UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D (void);
// 0x000000D4 System.Void Coffee.UIEffects.UIShadow::.ctor()
extern void UIShadow__ctor_m0A21DE63CDED6A313DCAA7FCE15EA6F5D8EEBF48 (void);
// 0x000000D5 System.Void Coffee.UIEffects.UIShadow::.cctor()
extern void UIShadow__cctor_m987303AB23CE53BB5BEF7B30A4A47D34B0E28CCD (void);
// 0x000000D6 System.Single Coffee.UIEffects.UIShiny::get_effectFactor()
extern void UIShiny_get_effectFactor_m7CC24197EA6EC3D9E3F2FDF760FE7491D33D9DAE (void);
// 0x000000D7 System.Void Coffee.UIEffects.UIShiny::set_effectFactor(System.Single)
extern void UIShiny_set_effectFactor_mDC9BF9072772D010621E6ECEEFA08070094F9C37 (void);
// 0x000000D8 System.Single Coffee.UIEffects.UIShiny::get_width()
extern void UIShiny_get_width_mB3097400337647C1B974AC977F1EA8EF885FFA41 (void);
// 0x000000D9 System.Void Coffee.UIEffects.UIShiny::set_width(System.Single)
extern void UIShiny_set_width_mB05860895310CE75AB56D3A4C85C52705D4DEA01 (void);
// 0x000000DA System.Single Coffee.UIEffects.UIShiny::get_softness()
extern void UIShiny_get_softness_mEA7CD28F61BAA8EDBE937D029CD6CDDFD8864F33 (void);
// 0x000000DB System.Void Coffee.UIEffects.UIShiny::set_softness(System.Single)
extern void UIShiny_set_softness_m918558EC9B443129ABA1765B7A6E443784FCB5E5 (void);
// 0x000000DC System.Single Coffee.UIEffects.UIShiny::get_brightness()
extern void UIShiny_get_brightness_mFD1E509264A2332E79F52059EAFD7D0E98AC30B1 (void);
// 0x000000DD System.Void Coffee.UIEffects.UIShiny::set_brightness(System.Single)
extern void UIShiny_set_brightness_mBC5ED959615ABF2B95150577AF926CE66327F9BF (void);
// 0x000000DE System.Single Coffee.UIEffects.UIShiny::get_gloss()
extern void UIShiny_get_gloss_mE1CC5FFDAB91B9EF04F663B0B903E5C2CDB835A0 (void);
// 0x000000DF System.Void Coffee.UIEffects.UIShiny::set_gloss(System.Single)
extern void UIShiny_set_gloss_m17BA5F225B65CE3B9F678B8DB000D2EEB4B52F55 (void);
// 0x000000E0 System.Single Coffee.UIEffects.UIShiny::get_rotation()
extern void UIShiny_get_rotation_m987EBABE0F01A057E4B8252178B311B2AAF0ECA0 (void);
// 0x000000E1 System.Void Coffee.UIEffects.UIShiny::set_rotation(System.Single)
extern void UIShiny_set_rotation_m9F4B304CA1A09EED8163B83C662D685BC44F9C7C (void);
// 0x000000E2 Coffee.UIEffects.EffectArea Coffee.UIEffects.UIShiny::get_effectArea()
extern void UIShiny_get_effectArea_mC4190ECC3E1AA96AE907892422ED41DDA89DA98F (void);
// 0x000000E3 System.Void Coffee.UIEffects.UIShiny::set_effectArea(Coffee.UIEffects.EffectArea)
extern void UIShiny_set_effectArea_mD926F674DDFBD4147DA077CD829794E19D294D71 (void);
// 0x000000E4 Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UIShiny::get_paramTex()
extern void UIShiny_get_paramTex_m35E6A178B7C5D9112925E72BDFAA5E23AD8A35E2 (void);
// 0x000000E5 Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UIShiny::get_effectPlayer()
extern void UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A (void);
// 0x000000E6 System.Void Coffee.UIEffects.UIShiny::OnEnable()
extern void UIShiny_OnEnable_m9B87F17E22CFEB9A7660BA408933050482C806C6 (void);
// 0x000000E7 System.Void Coffee.UIEffects.UIShiny::OnDisable()
extern void UIShiny_OnDisable_m2B6E97484F72EF1EB8CCF0A58804F8469ABDB982 (void);
// 0x000000E8 UnityEngine.Hash128 Coffee.UIEffects.UIShiny::GetMaterialHash(UnityEngine.Material)
extern void UIShiny_GetMaterialHash_m028145B948E2A4159F7C5071B0CA6816C893C865 (void);
// 0x000000E9 System.Void Coffee.UIEffects.UIShiny::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void UIShiny_ModifyMaterial_m39A0C2725A242EBF52121665B15DD99F2B946B41 (void);
// 0x000000EA System.Void Coffee.UIEffects.UIShiny::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UIShiny_ModifyMesh_m4AAC9C9885AC7A23843755180BA9D3EDBC53D74F (void);
// 0x000000EB System.Void Coffee.UIEffects.UIShiny::Play(System.Boolean)
extern void UIShiny_Play_mD7A99CB880214409A701FC18D30E079BF900F9E0 (void);
// 0x000000EC System.Void Coffee.UIEffects.UIShiny::Stop(System.Boolean)
extern void UIShiny_Stop_mA1E3C7A37FDFD05D8ABD5D6BA62E448141FE2806 (void);
// 0x000000ED System.Void Coffee.UIEffects.UIShiny::SetEffectParamsDirty()
extern void UIShiny_SetEffectParamsDirty_m263709BB893DDC8C9A4ADFBE9410B0EB4E518C8F (void);
// 0x000000EE System.Void Coffee.UIEffects.UIShiny::SetVerticesDirty()
extern void UIShiny_SetVerticesDirty_mBCDE768BFC79060F457CDA382D12A9792D7BE56C (void);
// 0x000000EF System.Void Coffee.UIEffects.UIShiny::OnDidApplyAnimationProperties()
extern void UIShiny_OnDidApplyAnimationProperties_m9CD7B1B017035892BD2D3134FAC9B9BD48FBD1DC (void);
// 0x000000F0 System.Void Coffee.UIEffects.UIShiny::.ctor()
extern void UIShiny__ctor_m213C91AA2E259A559D0D2E0CDF8A5E953A4CE02E (void);
// 0x000000F1 System.Void Coffee.UIEffects.UIShiny::.cctor()
extern void UIShiny__cctor_m236A3CDD15FBE0448E4E6F64467737B74CC7F227 (void);
// 0x000000F2 System.Void Coffee.UIEffects.UIShiny::<OnEnable>b__37_0(System.Single)
extern void UIShiny_U3COnEnableU3Eb__37_0_mD8C5F5D76DC15E78E11D122BB45ABD3C6A94E89E (void);
// 0x000000F3 Coffee.UIEffects.BaseMeshEffect Coffee.UIEffects.UISyncEffect::get_targetEffect()
extern void UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C (void);
// 0x000000F4 System.Void Coffee.UIEffects.UISyncEffect::set_targetEffect(Coffee.UIEffects.BaseMeshEffect)
extern void UISyncEffect_set_targetEffect_mCCFC727AC6494DA2150CCBA6F1B418AC20E7BA16 (void);
// 0x000000F5 System.Void Coffee.UIEffects.UISyncEffect::OnEnable()
extern void UISyncEffect_OnEnable_m6543A8A00A4E24DDA8709078CDD42B608BF9FFA6 (void);
// 0x000000F6 System.Void Coffee.UIEffects.UISyncEffect::OnDisable()
extern void UISyncEffect_OnDisable_m6B06EF4FE6D838E0F6A3AD34495AA050E116038B (void);
// 0x000000F7 UnityEngine.Hash128 Coffee.UIEffects.UISyncEffect::GetMaterialHash(UnityEngine.Material)
extern void UISyncEffect_GetMaterialHash_m606DFC3572F7EB6F5DFCA33847F2E316BA67CDC9 (void);
// 0x000000F8 System.Void Coffee.UIEffects.UISyncEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void UISyncEffect_ModifyMaterial_mDDD99F8EA072BF04EEB90855BAF954870BFFC251 (void);
// 0x000000F9 System.Void Coffee.UIEffects.UISyncEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UISyncEffect_ModifyMesh_m42AC1484094636F2EA42979A8F3F59822C22DC70 (void);
// 0x000000FA System.Void Coffee.UIEffects.UISyncEffect::.ctor()
extern void UISyncEffect__ctor_mBC4DE35774A9EB941735869ED682B8FD3BEB2F98 (void);
// 0x000000FB System.Single Coffee.UIEffects.UITransitionEffect::get_effectFactor()
extern void UITransitionEffect_get_effectFactor_m632CBDEC96902006CF2EFB9D1E94F2A7CF65C1E6 (void);
// 0x000000FC System.Void Coffee.UIEffects.UITransitionEffect::set_effectFactor(System.Single)
extern void UITransitionEffect_set_effectFactor_m6316BCC7814AE16693E998498FE3F199DEFE6BC7 (void);
// 0x000000FD UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::get_transitionTexture()
extern void UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C (void);
// 0x000000FE System.Void Coffee.UIEffects.UITransitionEffect::set_transitionTexture(UnityEngine.Texture)
extern void UITransitionEffect_set_transitionTexture_m8354B8733DD664EA8D8E5EC7DD60D39F9667DD31 (void);
// 0x000000FF UnityEngine.Texture Coffee.UIEffects.UITransitionEffect::get_defaultTransitionTexture()
extern void UITransitionEffect_get_defaultTransitionTexture_mB6C7677AF8930EA977477611EC747B8E44B9FE80 (void);
// 0x00000100 Coffee.UIEffects.UITransitionEffect/EffectMode Coffee.UIEffects.UITransitionEffect::get_effectMode()
extern void UITransitionEffect_get_effectMode_m791A11B214ACF5EC355A16F2E0C86E20116377F2 (void);
// 0x00000101 System.Void Coffee.UIEffects.UITransitionEffect::set_effectMode(Coffee.UIEffects.UITransitionEffect/EffectMode)
extern void UITransitionEffect_set_effectMode_m03EA15F5C7F92D8A8A2EDB35E93A324E4B3E362A (void);
// 0x00000102 System.Boolean Coffee.UIEffects.UITransitionEffect::get_keepAspectRatio()
extern void UITransitionEffect_get_keepAspectRatio_m6484BC9EBFF7A32D329BCD6950D42D4B4218637A (void);
// 0x00000103 System.Void Coffee.UIEffects.UITransitionEffect::set_keepAspectRatio(System.Boolean)
extern void UITransitionEffect_set_keepAspectRatio_m715D03A69C94555B3FD9AC4BF0E3E092CFCA6C6C (void);
// 0x00000104 Coffee.UIEffects.ParameterTexture Coffee.UIEffects.UITransitionEffect::get_paramTex()
extern void UITransitionEffect_get_paramTex_m8CCFCA1D815FC09F713F2F7853DCF75CC6C7EA35 (void);
// 0x00000105 System.Single Coffee.UIEffects.UITransitionEffect::get_dissolveWidth()
extern void UITransitionEffect_get_dissolveWidth_m82A0BF713399790EF9AC1023E814B48E4BA962E4 (void);
// 0x00000106 System.Void Coffee.UIEffects.UITransitionEffect::set_dissolveWidth(System.Single)
extern void UITransitionEffect_set_dissolveWidth_m90D68E355B7F41A0A46C11DF2879403AEC673F77 (void);
// 0x00000107 System.Single Coffee.UIEffects.UITransitionEffect::get_dissolveSoftness()
extern void UITransitionEffect_get_dissolveSoftness_mFC4C5C7A6A76E00FC256964D4BFE617D3876CCC4 (void);
// 0x00000108 System.Void Coffee.UIEffects.UITransitionEffect::set_dissolveSoftness(System.Single)
extern void UITransitionEffect_set_dissolveSoftness_m7359FBFDF96454D92B6FDCF6F1035C9269935685 (void);
// 0x00000109 UnityEngine.Color Coffee.UIEffects.UITransitionEffect::get_dissolveColor()
extern void UITransitionEffect_get_dissolveColor_m087996550F653B3DD57D5B7C4ED28A6700720583 (void);
// 0x0000010A System.Void Coffee.UIEffects.UITransitionEffect::set_dissolveColor(UnityEngine.Color)
extern void UITransitionEffect_set_dissolveColor_mA08833DCB9133AA6C022C4FF48EB674BE5BB7863 (void);
// 0x0000010B System.Boolean Coffee.UIEffects.UITransitionEffect::get_passRayOnHidden()
extern void UITransitionEffect_get_passRayOnHidden_m7F6FCC01F882D6DDD86E86FC477C2A14336A0272 (void);
// 0x0000010C System.Void Coffee.UIEffects.UITransitionEffect::set_passRayOnHidden(System.Boolean)
extern void UITransitionEffect_set_passRayOnHidden_m485D4113137975F3E7894291A68CC8DDD4CAC31E (void);
// 0x0000010D Coffee.UIEffects.EffectPlayer Coffee.UIEffects.UITransitionEffect::get_effectPlayer()
extern void UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6 (void);
// 0x0000010E System.Void Coffee.UIEffects.UITransitionEffect::Show(System.Boolean)
extern void UITransitionEffect_Show_m94DE5DA72ABAC6F4B4BD08238BDE9BA371C852D6 (void);
// 0x0000010F System.Void Coffee.UIEffects.UITransitionEffect::Hide(System.Boolean)
extern void UITransitionEffect_Hide_m1C9F1CB283F5DF13BAC04BC4FF43316133919DDB (void);
// 0x00000110 UnityEngine.Hash128 Coffee.UIEffects.UITransitionEffect::GetMaterialHash(UnityEngine.Material)
extern void UITransitionEffect_GetMaterialHash_mA610989C67E64BF3A05096838EC7F381A777ED17 (void);
// 0x00000111 System.Void Coffee.UIEffects.UITransitionEffect::ModifyMaterial(UnityEngine.Material,UnityEngine.UI.Graphic)
extern void UITransitionEffect_ModifyMaterial_mF7DFA18AA710C04E11443F2B6DF29CDA25C3435F (void);
// 0x00000112 System.Void Coffee.UIEffects.UITransitionEffect::ModifyMesh(UnityEngine.UI.VertexHelper,UnityEngine.UI.Graphic)
extern void UITransitionEffect_ModifyMesh_mE2925A2E4B63AB8BFB78E632089B124F84DE142C (void);
// 0x00000113 System.Void Coffee.UIEffects.UITransitionEffect::OnEnable()
extern void UITransitionEffect_OnEnable_mA79771EC4DE69ED8844126AB7CF69C10CBD43CEF (void);
// 0x00000114 System.Void Coffee.UIEffects.UITransitionEffect::OnDisable()
extern void UITransitionEffect_OnDisable_m6BDD9389E4B91B0032D70E1016FD507B6C7A074F (void);
// 0x00000115 System.Void Coffee.UIEffects.UITransitionEffect::SetEffectParamsDirty()
extern void UITransitionEffect_SetEffectParamsDirty_m4895AE69C72B203EFA443AC7EB3B87BF575908EE (void);
// 0x00000116 System.Void Coffee.UIEffects.UITransitionEffect::SetVerticesDirty()
extern void UITransitionEffect_SetVerticesDirty_mCA6B03E3DF89E3B60C50556BA3FFE4403B4733A7 (void);
// 0x00000117 System.Void Coffee.UIEffects.UITransitionEffect::OnDidApplyAnimationProperties()
extern void UITransitionEffect_OnDidApplyAnimationProperties_mD7E02BB3378AAB63D843174CBD5D01D10DDD0423 (void);
// 0x00000118 System.Void Coffee.UIEffects.UITransitionEffect::.ctor()
extern void UITransitionEffect__ctor_mF067FB43A27FD374FCE4DE22891068F0419EC966 (void);
// 0x00000119 System.Void Coffee.UIEffects.UITransitionEffect::.cctor()
extern void UITransitionEffect__cctor_m29CED01502E9237C081448992B84557A9E6EEA58 (void);
// 0x0000011A System.Void Coffee.UIEffects.UITransitionEffect::<Show>b__46_0(System.Single)
extern void UITransitionEffect_U3CShowU3Eb__46_0_m57FEB42ADD3994430AF16F786501D9B18B5ED71A (void);
// 0x0000011B System.Void Coffee.UIEffects.UITransitionEffect::<Hide>b__47_0(System.Single)
extern void UITransitionEffect_U3CHideU3Eb__47_0_mF3CCECEBADA02DE02348B5455B049D700C33B4B9 (void);
static Il2CppMethodPointer s_methodPointers[283] = 
{
	Packer_ToFloat_mA7D23F51E9C8A6E62848712A27E2309BC31F94A7,
	Packer_ToFloat_mC5388DBB78FE07E8AB135B8759A20EAA43AF320E,
	Packer_ToFloat_m7691266B0941E760FB34C506126EEAF8A8FBE1D7,
	Packer_ToFloat_m014942D1B313CCD78B363010DFBC76E8ACCE3662,
	BaseMaterialEffect_get_parameterIndex_mCEAD6C474EC79C1D3F2E3B840905E10254EFE61F,
	BaseMaterialEffect_set_parameterIndex_m087EF0A73CE6D3519D94F9AD309EB3414A227524,
	BaseMaterialEffect_get_paramTex_m102366D70593F2CD331F01163A461AD61BE46051,
	BaseMaterialEffect_SetMaterialDirty_mA1351D41AD8E7E74CB7398C9C8847BF4770BE8A0,
	BaseMaterialEffect_GetMaterialHash_m56C3761346414A62F442ECB7A389A4F8844744D1,
	BaseMaterialEffect_GetModifiedMaterial_mABA10CC6BFFDFD3A82EEFF3D9156AFD08D071B1A,
	BaseMaterialEffect_GetModifiedMaterial_mB36BD87123ED2DC4AF906BE069DCAD95AC1A54ED,
	BaseMaterialEffect_ModifyMaterial_m5B22D41500FF33965FDC9E4843527EC5C535BCD6,
	BaseMaterialEffect_SetShaderVariants_m5C6DF7C79D401050E1D1991D1B4C082883E386B1,
	BaseMaterialEffect_OnEnable_m564FDB9C572C8E8C43300191BC8797FC1C1DEBCE,
	BaseMaterialEffect_OnDisable_m0F9F397AAED4A83F24EBBFDC850279942728D580,
	BaseMaterialEffect__ctor_mEF7E6D707F403310D3EEBD2CB02134DBDCA61497,
	BaseMaterialEffect__cctor_mE50CD52D2F92D00BB3AD6A076F045C83F8878203,
	U3CU3Ec__cctor_m0B882E2EB74347504170994F853131BB520A2017,
	U3CU3Ec__ctor_mFFE62F74902632613ECF890912C74E4243194389,
	U3CU3Ec_U3CSetShaderVariantsU3Eb__15_0_m86A0BD07D9513900697A9A21089709A5AE45B974,
	U3CU3Ec_U3CSetShaderVariantsU3Eb__15_1_mB4C34D6B741E7313073BF9AA37C633F2A40B3DB3,
	BaseMeshEffect_get_connector_m7EB352C1618B8421CCA94A0DBCCCCE48771E444D,
	BaseMeshEffect_get_graphic_m7E5AE1F7F1B0F8C64FB482A808F04156F9D91EAB,
	BaseMeshEffect_get_rectTransform_mBBBE8B8D28B96A213BFFE30A6F02B0D8A61029D6,
	BaseMeshEffect_ModifyMesh_mD039BBBF805029CDB8980BC571AF1C8EEA5DE77B,
	BaseMeshEffect_ModifyMesh_m5ED9F4CB50A4155F08EBF97C991240485AD2E794,
	BaseMeshEffect_ModifyMesh_m6EE8AFAC5195AFA3B2B0073E95ACB45FF3838912,
	BaseMeshEffect_SetVerticesDirty_m6BF2A600AD94E4048E3186ACDFBBEB73C6099CA9,
	BaseMeshEffect_OnEnable_m900129F36764E412C7FCE57A7C00DB091F48F4C6,
	BaseMeshEffect_OnDisable_m714D32A1FD3155B874FD836C76954208747FED85,
	BaseMeshEffect_SetEffectParamsDirty_m73280E2344924B24DC679377A461ED67FAFCEA19,
	BaseMeshEffect_OnDidApplyAnimationProperties_m319B39D68B09BA86C8CB8A8ED37B321C39E08F4E,
	BaseMeshEffect__ctor_mE1DAEB91FED6ADABADD9030BB0DD1EFB156ADB0D,
	EffectPlayer_OnEnable_m0EF48656581BC825628C7D9ACBACAFB2A8D5C502,
	EffectPlayer_OnDisable_m563A64C22235C0B05AE41B4F69D42188851F0DD2,
	EffectPlayer_Play_mCE6EFCF5B8BF63E1A3A6ED57F789BBE1BEFDA802,
	EffectPlayer_Stop_mCDDB096B24E282D85C931BC545CE0C03FE4AF357,
	EffectPlayer_OnWillRenderCanvases_m314B00D1BE5B4E3E06CC14D6F33508A4B32A33FD,
	EffectPlayer__ctor_m8D5C77D0816E0E9CD890FD5B02869AE4CDCD70EE,
	U3CU3Ec__cctor_m95D50A34703A638FA30A294E31DA8E9AB89FD3D5,
	U3CU3Ec__ctor_m6D644EF6639B2C8942C4F547E344568976E93B5C,
	U3CU3Ec_U3COnEnableU3Eb__7_0_m03524910EABBB0BB877A6F4343AADDF0B2C8B8D2,
	GraphicConnector_Init_m26072284172DE1108E2D55D752D2DA4AF20A552A,
	GraphicConnector_AddConnector_m1D64A1D518EE65C19E8A0D2EE1D3B4584F7C3CFE,
	GraphicConnector_FindConnector_m5D4C15F7E0A8DF53940DB34AEBFB3BED637DE3E0,
	GraphicConnector_get_priority_m90E013783C0364AC6BBB00E5ED2D6F16CEB1E056,
	GraphicConnector_get_extraChannel_mBACD9FB6B2EFD87E46936C068B0928C983B66493,
	GraphicConnector_IsValid_m1C83898406961516C0B3B3EBF11F070397224BB3,
	GraphicConnector_FindShader_m79BE9DE2146376902CBE2647B0CF83A8409A67D3,
	GraphicConnector_OnEnable_m0B0E987BB2634B8784713B5B40D091E5CD339638,
	GraphicConnector_OnDisable_mE8D7E9477569A5C57F19D7003B5E919760130739,
	GraphicConnector_SetVerticesDirty_m929125B81A45CC98E05D7A42417D62E9BB99CB20,
	GraphicConnector_SetMaterialDirty_m168E165024112455DDCD7AAE3B65926C9890D990,
	GraphicConnector_GetPositionFactor_m78368E99AED4E4C093AFE2D0E4608519C14F2B58,
	GraphicConnector_IsText_mEB728B485D4E21FA91A2CF75597708D345B2DE82,
	GraphicConnector_SetExtraChannel_m494279F3F5E68D70DE0F43EF25B939358E965603,
	GraphicConnector_GetNormalizedFactor_m6555A1423252B5DDD7A71FA1C5400AE805A50DC3,
	GraphicConnector__ctor_mF95FB5F86269F4423CC5F9DFA370EA4849FF6DAD,
	GraphicConnector__cctor_mC73D5A65390F0C216962225BEB934BCBEF9EE522,
	U3CU3Ec__cctor_m296B372A3F55B194FA47324D2FD76C6B9F46F5C8,
	U3CU3Ec__ctor_m827FF5CD8B256D37FC61A732312435C6F12041CE,
	U3CU3Ec_U3CAddConnectorU3Eb__4_0_mC99D9E9096B78F00A3509748193374ACB23CBB91,
	MaterialCache_Register_m41B9F13914C2BBF0F30CC45DBF8E8713E15079F8,
	MaterialCache_Unregister_m5F0CFE2E6BFD7A6D3E0862D6034B1CB4F3A5B394,
	MaterialCache__ctor_mA988DB48B7DC46EC3020C7DEC2DCBBD9AD785C8A,
	MaterialCache__cctor_m46A748483AC54F7C2C85EF3ED75A0247BA85E583,
	MaterialEntry_Release_mB302360107E7F32A760184279DA7E3D24149498E,
	MaterialEntry__ctor_m409D7E4C408065F797EFAC708ADD6836DDAD7FC8,
	Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE,
	Matrix2x3_op_Multiply_mF25AF35C2E592A527797F2145518D4BFB0101ED0,
	NULL,
	NULL,
	NULL,
	ParameterTexture__ctor_m9797AC41DB5FCAD61F7176C1C9AE2713BC4B8876,
	ParameterTexture_Register_m418624C23515BB36BFE2E30FDC66AB2DD855D62A,
	ParameterTexture_Unregister_mC8283ADEEC7D0B15321727EC445C0909671918BC,
	ParameterTexture_SetData_m3870BC4215A7F85A68A38D2E6B50DB27D65284BB,
	ParameterTexture_SetData_m702D3FE27452A0D9327DDF4900EA86F3D09A990F,
	ParameterTexture_RegisterMaterial_mBB086C3F4B2A2ACAC4FBB90B46BB84297CDD62B9,
	ParameterTexture_GetNormalizedIndex_mFDD1138CB5F0B3A128BADB669E2D6877DCD3F0D5,
	ParameterTexture_Initialize_m947A572C82BF5BB6BC7A84E95F3498401183F29A,
	ParameterTexture_UpdateParameterTexture_mF9DFFADF593F9954190F1161E8C2E117077B2B63,
	U3CU3Ec__cctor_mABB703F0252422D599BC010492D66F0782FFB526,
	U3CU3Ec__ctor_m685DAAD421CE9C891A571D72713AD4D024D5B079,
	U3CU3Ec_U3CInitializeU3Eb__16_0_mD35535E6182BFEEE7882780DE18A230F2611C9E9,
	EffectAreaExtensions_GetEffectArea_m67DA76B72E33D9F96D1BBE04F5B9BFCE9A256BD7,
	EffectAreaExtensions_GetPositionFactor_m133BD727A155DAC41FE3FD89F266BA037EEA56AF,
	EffectAreaExtensions_GetNormalizedFactor_mE54719FA9D4F34F2EEAF3418B47B37E3F0DA67FF,
	EffectAreaExtensions__cctor_m62D31F3EED0577276B9296CD675BEE743245ABE5,
	UIDissolve_get_effectFactor_mB944EE30EB78C6EF26D9BBFD3E0E3F4949831D22,
	UIDissolve_set_effectFactor_mDAD33D00CAF5DB3E62CFDFE65263D7A6E59C0FC2,
	UIDissolve_get_width_m05E9EDE3012FAF89A60F49C567612C3459F70DD0,
	UIDissolve_set_width_m4260A791CD3364EA0823369787298EB1F7C95EBE,
	UIDissolve_get_softness_m1057EF2111B6739297278CBFA3395B682721C7E9,
	UIDissolve_set_softness_m937B3F02AF8B03419481A65756AE2581E24B1E47,
	UIDissolve_get_color_mEB32F760AD1BD1125D983FC1B313022EC1248359,
	UIDissolve_set_color_mCCDFC676B00E5090890B47C5EF1FAB654AF10BA9,
	UIDissolve_get_transitionTexture_m26D7ED51DE3163DE505EFD1624532938E8873792,
	UIDissolve_set_transitionTexture_m033F2F344098C2C1879D4D73B03EF787B8380238,
	UIDissolve_get_defaultTransitionTexture_m16638E0BF5F9B8E7E089B2BAF0D5B5325CDBB0D4,
	UIDissolve_get_effectArea_m12B5A1099A85CEA396F92CCF83EC37E43C65DC82,
	UIDissolve_set_effectArea_mC95BE4CAD13FFD675BF38A4A6EDA16896D0B3C05,
	UIDissolve_get_keepAspectRatio_mF2B7DBCF08E8EA6D7494006ED4F377C660ED5874,
	UIDissolve_set_keepAspectRatio_m9BA74A15B0B3E6AFEFCD30F9A4502F2EFF850B92,
	UIDissolve_get_colorMode_m500352BC8F5DD2FE5EF279D4D94F2CEFD572C2D8,
	UIDissolve_set_colorMode_mFE133DE54ACEADE97857F52F22473D7714B362E5,
	UIDissolve_get_paramTex_mC7CD59F8E1E87B0FC575638D6D6182C7CF21B55C,
	UIDissolve_get_effectPlayer_m14F60D76D34994F8E3508FD3F5B29B703B34147A,
	UIDissolve_GetMaterialHash_m92BC68A7FE25F1F1130B46A13B67C9D49BAFB392,
	UIDissolve_ModifyMaterial_m864A13BF4E46CED12F1C1C03C70C3AD540E0D46D,
	UIDissolve_ModifyMesh_m7BA4DB2233B3E73643612C8E08256D896A8B77FB,
	UIDissolve_SetEffectParamsDirty_m654479EEBFC40E3EC6A07438BC27CF1BC4A33D83,
	UIDissolve_SetVerticesDirty_mDFDAC5475D74749DE0261E86EAC59E16A03F396E,
	UIDissolve_OnDidApplyAnimationProperties_m74FCBDF6CEC9CB6D293687D6651CC4F36A5E5E3E,
	UIDissolve_Play_m11CEE7F8593F191A9B3A6E7908318DB68F4D0822,
	UIDissolve_Stop_mB2C1169F0638C0692559A4366BFA2C3D7BADB676,
	UIDissolve_OnEnable_m185245A83DA598463AED735F1E901B86AB945B7B,
	UIDissolve_OnDisable_m1381CC4F4C83657F28506892C9B8C12BC966B774,
	UIDissolve__ctor_m99E95E10A3C975048CAE46A47A0784384791777D,
	UIDissolve__cctor_mBF082F322F7071A5B1CCFBF4987AF53ABB84765F,
	UIDissolve_U3COnEnableU3Eb__54_0_m6FEE944A61838694BDB117ADC92E7733C84C07AF,
	UIEffect_get_uvMaskChannel_m321DED93A70D6F9C73D1709E4DC7112E977B9404,
	UIEffect_get_effectFactor_m00273C0BB3B80F99858764F8D80FD8D1BD7CB92E,
	UIEffect_set_effectFactor_m4CE7979B846A9CC8968C5C50A4333743F9925674,
	UIEffect_get_colorFactor_m75D2A042A8576CEAC03C4B798E4F7DB235AC6ABD,
	UIEffect_set_colorFactor_m8AB6B802918563B908F4E37AD6BE06592B626864,
	UIEffect_get_blurFactor_mB06AFF2FC0044E3ECBD81DBF2C9303FB908E81A4,
	UIEffect_set_blurFactor_m3C443FD10A75407F541E6EB620B9B55EF85F90F3,
	UIEffect_get_effectMode_m4D2487E963178FE58E1AA2FE976227C0B4645C1F,
	UIEffect_set_effectMode_mCFE02D088374C730DD8E35992E50FD1E1F574991,
	UIEffect_get_colorMode_m7A813F78AF2153B5C7A381D7F29A1D9C4511E664,
	UIEffect_set_colorMode_m606B079D79B95DA229EF15B571723CEDE7110309,
	UIEffect_get_blurMode_m97D9F46E04E8B291EEB1E7ADCAE27842981FA6DD,
	UIEffect_set_blurMode_m92E5F74746B132960206951A9BCD75B16F1C8ADA,
	UIEffect_get_paramTex_m9108661AB3CFCE2A89F16E49DEAD847166E30EDF,
	UIEffect_get_advancedBlur_m37BF17E16B82618676E23CF6338093ECA5AEB4AD,
	UIEffect_set_advancedBlur_m9CAFAD3649A2C0F169B9F8B421AD0E94EDACC140,
	UIEffect_GetMaterialHash_mB81CBA44D34978619BA96B13CFFD3491CE6700F0,
	UIEffect_ModifyMaterial_mBB59C215B8E1F198565EDEB57537247DD4D48DAA,
	UIEffect_ModifyMesh_m4633FC2713ED5D9CE152E9FFC484F89F9554B543,
	UIEffect_SetEffectParamsDirty_mFAEDBE0F8F4804226CB2B62195BEBFEF996B2476,
	UIEffect_GetBounds_m35577243A9C58E39854A2538290A13D64C205BAD,
	UIEffect__ctor_mEC430F1FC686DCDF70EE103A523A09F8E19DA37F,
	UIEffect__cctor_m0BEF19CA78CE673FB7B29883F6705793CEB2DB33,
	UIFlip_get_horizontal_mF54C27E018057B8F9A36E7CA2E6FADA1D7743114,
	UIFlip_set_horizontal_m918D709FB4B6888F5E24F3712CEC77AA68DB5E37,
	UIFlip_get_vertical_m0250BFEECB51A1633A09D2CF5C705D1587C3D06A,
	UIFlip_set_vertical_m71A359FCC21F9D5DE5C9007DBC4A520FC4CA5F62,
	UIFlip_ModifyMesh_m79A6FC275515C150F5FCABE88B97ED4BD21A1645,
	UIFlip__ctor_mD4CE3113E5EA38CB61767AD622C84063715A94E3,
	UIGradient_get_direction_m094F3EB30335D4C941FFE1C9CD92A1EA46FFD209,
	UIGradient_set_direction_m978ABB95A3EE5868DC04D333CCA44D48CD79CA54,
	UIGradient_get_color1_m423D956F1D7112DA243036673C5132976AAAAC38,
	UIGradient_set_color1_mE085DBC6446EFEF5659869CAB0A24388A22BC0F4,
	UIGradient_get_color2_mF8239D06170F4FB39247E60942112B738997056E,
	UIGradient_set_color2_mFCC6472DFDAF34B009A11A581EE40A2FDF698C24,
	UIGradient_get_color3_m6A6ADF7604891CF1BE6BAFBAA652F1CA2E97C7FC,
	UIGradient_set_color3_m5866EAB3CF8D10559C80944A1CD53125CF994551,
	UIGradient_get_color4_mA95E03D16F86A39D3003BC0390629E40E3859298,
	UIGradient_set_color4_m0A4217B2A704F895A15AD5745970225D7047C837,
	UIGradient_get_rotation_m77FADBF0C5FC07638849FBB32F7DC3739876108E,
	UIGradient_set_rotation_m4581F1FA269346868CC5FFC6A716ACDAF59E45A7,
	UIGradient_get_offset_mF798A5666929324378FD513ED51C988EE5091887,
	UIGradient_set_offset_m3F3A747D71F33B04448E557E6183FDD2E4849EEA,
	UIGradient_get_offset2_m5B5F98FAADFFC9ACD34BC208000DCC0D0318507C,
	UIGradient_set_offset2_m6941C3CAB75BFAD13296ED00F6E3A83865C830B2,
	UIGradient_get_gradientStyle_mE294AE9A685ED8906035884704E0C7158764840D,
	UIGradient_set_gradientStyle_m44B6AB97EA9E06EF31A34906E03AA0A41EB2D30B,
	UIGradient_get_colorSpace_m7BC8625644E03D801B620B418B219676DB742020,
	UIGradient_set_colorSpace_m2336F2412186992DBFFE0072A402C840E06FF989,
	UIGradient_get_ignoreAspectRatio_mD64D86E1D8FFD9BEC4606ED2C11257E9D9053AA2,
	UIGradient_set_ignoreAspectRatio_m1F52E4804269268E898C04B8ED1C618E313316C2,
	UIGradient_ModifyMesh_m23B0DAD902A385843C277C66D01B5359B84B40C6,
	UIGradient__ctor_m9400C38CE25C5FA85BE64AA54384FC7F16F8FE6F,
	UIGradient__cctor_mEAC8743086EA5920BBF96651F4F48D4BA01FB8B2,
	UIHsvModifier_get_targetColor_mB8E7080EDD10DA0D149EF2017910EC4F13119D2E,
	UIHsvModifier_set_targetColor_m57B784581FD98B38E5EA7CD8CA7232B58D2D6CB0,
	UIHsvModifier_get_range_m7C1D0EEBFBD69B2B32DD4C12943B56C36CD3DC5C,
	UIHsvModifier_set_range_m622FDB03F2E37F5D3832E501B66D9F4959CBF8E6,
	UIHsvModifier_get_saturation_mE5AD1F97E367095DBD1EA452DD24E46C2F037E17,
	UIHsvModifier_set_saturation_m01D7FA3519D65959C9D084A452395571EAB02077,
	UIHsvModifier_get_value_mE2043521CB9AA0AA3A812D9468DECDB25D2E801F,
	UIHsvModifier_set_value_m789131F4CD1C18A59D44F32AE3E29CC5BA34D732,
	UIHsvModifier_get_hue_m17FACAC80A9C5EF5AF4271A9274366B37DDA8174,
	UIHsvModifier_set_hue_m7404ED8A0A744D41C1DC729258A4BF3D8E9D9D92,
	UIHsvModifier_get_paramTex_m6B4568923620E4AE1805B1C133E1332D38D33B85,
	UIHsvModifier_GetMaterialHash_m77225F452DCD129FA334029C9DC064B6935F4715,
	UIHsvModifier_ModifyMaterial_m1944A64071017AB765A700C75EC6787B4CA43887,
	UIHsvModifier_ModifyMesh_m224465843842D791A0E3A965E20C3F630DBE7B94,
	UIHsvModifier_SetEffectParamsDirty_m786AD408DE613467C527EAD7AA3DB1F3868B0409,
	UIHsvModifier__ctor_m96CF14648BB39439DEA95EC2DFEEE7946B9A822A,
	UIHsvModifier__cctor_mBFF73E20AFB72787C7E5240BAEEAF7B0366CE491,
	UIShadow_get_effectColor_m426279C2B45EE8EE02122ED0F0B991E1ABA39C38,
	UIShadow_set_effectColor_m6D7B016793A2C176AA66F257BD3C53BFD71297EC,
	UIShadow_get_effectDistance_m2AB1626DC40D34C0D91FF336342F938195B201EE,
	UIShadow_set_effectDistance_m267278942B1CE1058C014208F2D79948626A99BC,
	UIShadow_get_useGraphicAlpha_mEC09F1BCF8D368129B771A94C129F048C7C0CE1E,
	UIShadow_set_useGraphicAlpha_m2A048380CA0E931D640C1B26A68B7C8E57798F7D,
	UIShadow_get_blurFactor_m5A7E95CE6D4AC039A6C84A95C9293FA23B7002E8,
	UIShadow_set_blurFactor_mDCFC5282751C9A98A4D2163964AB5A688BE1D079,
	UIShadow_get_style_mCAA0D48FB84802EBB12777BD82587CF6B1A20379,
	UIShadow_set_style_m57011D0B6E5E8F3E16B1E6B24272D2348068C555,
	UIShadow_get_parameterIndex_m421844BF83F2BD63CB36E171DCA3A4173B1E0D94,
	UIShadow_set_parameterIndex_mC140571AED150F3BB3FFCF5029F8D9878E54EE4A,
	UIShadow_get_paramTex_mB05161803BA7480AD83B5A91B699FB989AD2BFC2,
	UIShadow_set_paramTex_m4F6B0DA78E0B8870D6560C8F514756AB1DDF1010,
	UIShadow_OnEnable_m60DEFD2F94DC112AE059AF3E9D47FB3A65D3125C,
	UIShadow_OnDisable_m86DD53B994C6902D25002910BFA026E3057E53BB,
	UIShadow_ModifyMesh_m99583022DC19BEAAD324A9F7E22FF313129EB38A,
	UIShadow_ApplyShadow_m5422EC048717AA600AD8A06FCD534E94350034E1,
	UIShadow_ApplyShadowZeroAlloc_m02D1825D39BCA7BCF58AB96BF745EBFCC34EA25D,
	UIShadow__ctor_m0A21DE63CDED6A313DCAA7FCE15EA6F5D8EEBF48,
	UIShadow__cctor_m987303AB23CE53BB5BEF7B30A4A47D34B0E28CCD,
	UIShiny_get_effectFactor_m7CC24197EA6EC3D9E3F2FDF760FE7491D33D9DAE,
	UIShiny_set_effectFactor_mDC9BF9072772D010621E6ECEEFA08070094F9C37,
	UIShiny_get_width_mB3097400337647C1B974AC977F1EA8EF885FFA41,
	UIShiny_set_width_mB05860895310CE75AB56D3A4C85C52705D4DEA01,
	UIShiny_get_softness_mEA7CD28F61BAA8EDBE937D029CD6CDDFD8864F33,
	UIShiny_set_softness_m918558EC9B443129ABA1765B7A6E443784FCB5E5,
	UIShiny_get_brightness_mFD1E509264A2332E79F52059EAFD7D0E98AC30B1,
	UIShiny_set_brightness_mBC5ED959615ABF2B95150577AF926CE66327F9BF,
	UIShiny_get_gloss_mE1CC5FFDAB91B9EF04F663B0B903E5C2CDB835A0,
	UIShiny_set_gloss_m17BA5F225B65CE3B9F678B8DB000D2EEB4B52F55,
	UIShiny_get_rotation_m987EBABE0F01A057E4B8252178B311B2AAF0ECA0,
	UIShiny_set_rotation_m9F4B304CA1A09EED8163B83C662D685BC44F9C7C,
	UIShiny_get_effectArea_mC4190ECC3E1AA96AE907892422ED41DDA89DA98F,
	UIShiny_set_effectArea_mD926F674DDFBD4147DA077CD829794E19D294D71,
	UIShiny_get_paramTex_m35E6A178B7C5D9112925E72BDFAA5E23AD8A35E2,
	UIShiny_get_effectPlayer_m3B594DB184034489BE79868B428022DE2D64244A,
	UIShiny_OnEnable_m9B87F17E22CFEB9A7660BA408933050482C806C6,
	UIShiny_OnDisable_m2B6E97484F72EF1EB8CCF0A58804F8469ABDB982,
	UIShiny_GetMaterialHash_m028145B948E2A4159F7C5071B0CA6816C893C865,
	UIShiny_ModifyMaterial_m39A0C2725A242EBF52121665B15DD99F2B946B41,
	UIShiny_ModifyMesh_m4AAC9C9885AC7A23843755180BA9D3EDBC53D74F,
	UIShiny_Play_mD7A99CB880214409A701FC18D30E079BF900F9E0,
	UIShiny_Stop_mA1E3C7A37FDFD05D8ABD5D6BA62E448141FE2806,
	UIShiny_SetEffectParamsDirty_m263709BB893DDC8C9A4ADFBE9410B0EB4E518C8F,
	UIShiny_SetVerticesDirty_mBCDE768BFC79060F457CDA382D12A9792D7BE56C,
	UIShiny_OnDidApplyAnimationProperties_m9CD7B1B017035892BD2D3134FAC9B9BD48FBD1DC,
	UIShiny__ctor_m213C91AA2E259A559D0D2E0CDF8A5E953A4CE02E,
	UIShiny__cctor_m236A3CDD15FBE0448E4E6F64467737B74CC7F227,
	UIShiny_U3COnEnableU3Eb__37_0_mD8C5F5D76DC15E78E11D122BB45ABD3C6A94E89E,
	UISyncEffect_get_targetEffect_mE94A4CA10EC4F9E4149201608041648FA79FF12C,
	UISyncEffect_set_targetEffect_mCCFC727AC6494DA2150CCBA6F1B418AC20E7BA16,
	UISyncEffect_OnEnable_m6543A8A00A4E24DDA8709078CDD42B608BF9FFA6,
	UISyncEffect_OnDisable_m6B06EF4FE6D838E0F6A3AD34495AA050E116038B,
	UISyncEffect_GetMaterialHash_m606DFC3572F7EB6F5DFCA33847F2E316BA67CDC9,
	UISyncEffect_ModifyMaterial_mDDD99F8EA072BF04EEB90855BAF954870BFFC251,
	UISyncEffect_ModifyMesh_m42AC1484094636F2EA42979A8F3F59822C22DC70,
	UISyncEffect__ctor_mBC4DE35774A9EB941735869ED682B8FD3BEB2F98,
	UITransitionEffect_get_effectFactor_m632CBDEC96902006CF2EFB9D1E94F2A7CF65C1E6,
	UITransitionEffect_set_effectFactor_m6316BCC7814AE16693E998498FE3F199DEFE6BC7,
	UITransitionEffect_get_transitionTexture_mB4E99990AAF3BC7A81CAFA2AA6DF4AC0B67F391C,
	UITransitionEffect_set_transitionTexture_m8354B8733DD664EA8D8E5EC7DD60D39F9667DD31,
	UITransitionEffect_get_defaultTransitionTexture_mB6C7677AF8930EA977477611EC747B8E44B9FE80,
	UITransitionEffect_get_effectMode_m791A11B214ACF5EC355A16F2E0C86E20116377F2,
	UITransitionEffect_set_effectMode_m03EA15F5C7F92D8A8A2EDB35E93A324E4B3E362A,
	UITransitionEffect_get_keepAspectRatio_m6484BC9EBFF7A32D329BCD6950D42D4B4218637A,
	UITransitionEffect_set_keepAspectRatio_m715D03A69C94555B3FD9AC4BF0E3E092CFCA6C6C,
	UITransitionEffect_get_paramTex_m8CCFCA1D815FC09F713F2F7853DCF75CC6C7EA35,
	UITransitionEffect_get_dissolveWidth_m82A0BF713399790EF9AC1023E814B48E4BA962E4,
	UITransitionEffect_set_dissolveWidth_m90D68E355B7F41A0A46C11DF2879403AEC673F77,
	UITransitionEffect_get_dissolveSoftness_mFC4C5C7A6A76E00FC256964D4BFE617D3876CCC4,
	UITransitionEffect_set_dissolveSoftness_m7359FBFDF96454D92B6FDCF6F1035C9269935685,
	UITransitionEffect_get_dissolveColor_m087996550F653B3DD57D5B7C4ED28A6700720583,
	UITransitionEffect_set_dissolveColor_mA08833DCB9133AA6C022C4FF48EB674BE5BB7863,
	UITransitionEffect_get_passRayOnHidden_m7F6FCC01F882D6DDD86E86FC477C2A14336A0272,
	UITransitionEffect_set_passRayOnHidden_m485D4113137975F3E7894291A68CC8DDD4CAC31E,
	UITransitionEffect_get_effectPlayer_mBAFB7A4E5FAD8E2DA28EBF7E9A4CE808C6164DA6,
	UITransitionEffect_Show_m94DE5DA72ABAC6F4B4BD08238BDE9BA371C852D6,
	UITransitionEffect_Hide_m1C9F1CB283F5DF13BAC04BC4FF43316133919DDB,
	UITransitionEffect_GetMaterialHash_mA610989C67E64BF3A05096838EC7F381A777ED17,
	UITransitionEffect_ModifyMaterial_mF7DFA18AA710C04E11443F2B6DF29CDA25C3435F,
	UITransitionEffect_ModifyMesh_mE2925A2E4B63AB8BFB78E632089B124F84DE142C,
	UITransitionEffect_OnEnable_mA79771EC4DE69ED8844126AB7CF69C10CBD43CEF,
	UITransitionEffect_OnDisable_m6BDD9389E4B91B0032D70E1016FD507B6C7A074F,
	UITransitionEffect_SetEffectParamsDirty_m4895AE69C72B203EFA443AC7EB3B87BF575908EE,
	UITransitionEffect_SetVerticesDirty_mCA6B03E3DF89E3B60C50556BA3FFE4403B4733A7,
	UITransitionEffect_OnDidApplyAnimationProperties_mD7E02BB3378AAB63D843174CBD5D01D10DDD0423,
	UITransitionEffect__ctor_mF067FB43A27FD374FCE4DE22891068F0419EC966,
	UITransitionEffect__cctor_m29CED01502E9237C081448992B84557A9E6EEA58,
	UITransitionEffect_U3CShowU3Eb__46_0_m57FEB42ADD3994430AF16F786501D9B18B5ED71A,
	UITransitionEffect_U3CHideU3Eb__47_0_mF3CCECEBADA02DE02348B5455B049D700C33B4B9,
};
extern void Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[1] = 
{
	{ 0x06000045, Matrix2x3__ctor_mD60495A35CA6C7D74492003BA5ABEE322D3582DE_AdjustorThunk },
};
static const int32_t s_InvokerIndices[283] = 
{
	4379,
	5229,
	4612,
	4920,
	3708,
	3105,
	3723,
	3786,
	2184,
	2484,
	1389,
	1847,
	1847,
	3786,
	3786,
	3786,
	5339,
	5339,
	3786,
	2742,
	2484,
	3723,
	3723,
	3723,
	3119,
	3119,
	1847,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	3119,
	3786,
	1866,
	3142,
	3786,
	3786,
	5339,
	3786,
	3786,
	5339,
	5263,
	5172,
	3708,
	3708,
	2742,
	2484,
	3119,
	3119,
	3119,
	3119,
	184,
	2742,
	1563,
	302,
	3786,
	5339,
	5339,
	3786,
	1275,
	4282,
	5258,
	3786,
	5339,
	3786,
	3786,
	1160,
	4939,
	3708,
	3105,
	3723,
	1075,
	3119,
	3119,
	1118,
	1119,
	3119,
	2857,
	3786,
	3786,
	5339,
	3786,
	3786,
	4326,
	3879,
	3986,
	5339,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3672,
	3063,
	3723,
	3119,
	5316,
	3708,
	3105,
	3747,
	3142,
	3708,
	3105,
	3723,
	3723,
	2184,
	1847,
	1847,
	3786,
	3786,
	3786,
	3142,
	3142,
	3786,
	3786,
	3786,
	5339,
	3148,
	3708,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3708,
	3105,
	3708,
	3105,
	3708,
	3105,
	3723,
	3747,
	3142,
	2184,
	1847,
	1847,
	3786,
	3989,
	3786,
	5339,
	3747,
	3142,
	3747,
	3142,
	1847,
	3786,
	3708,
	3105,
	3672,
	3063,
	3672,
	3063,
	3672,
	3063,
	3672,
	3063,
	3754,
	3148,
	3754,
	3148,
	3779,
	3170,
	3708,
	3105,
	3708,
	3105,
	3747,
	3142,
	1847,
	3786,
	5339,
	3672,
	3063,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3723,
	2184,
	1847,
	1847,
	3786,
	3786,
	5339,
	3672,
	3063,
	3779,
	3170,
	3747,
	3142,
	3754,
	3148,
	3708,
	3105,
	3708,
	3105,
	3723,
	3119,
	3786,
	3786,
	1847,
	105,
	104,
	3786,
	5339,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3754,
	3148,
	3708,
	3105,
	3723,
	3723,
	3786,
	3786,
	2184,
	1847,
	1847,
	3142,
	3142,
	3786,
	3786,
	3786,
	3786,
	5339,
	3148,
	3723,
	3119,
	3786,
	3786,
	2184,
	1847,
	1847,
	3786,
	3754,
	3148,
	3723,
	3119,
	5316,
	3708,
	3105,
	3747,
	3142,
	3723,
	3754,
	3148,
	3754,
	3148,
	3672,
	3063,
	3747,
	3142,
	3723,
	3142,
	3142,
	2184,
	1847,
	1847,
	3786,
	3786,
	3786,
	3786,
	3786,
	3786,
	5339,
	3148,
	3148,
};
extern const CustomAttributesCacheGenerator g_UIEffect_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UIEffect_CodeGenModule;
const Il2CppCodeGenModule g_UIEffect_CodeGenModule = 
{
	"UIEffect.dll",
	283,
	s_methodPointers,
	1,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_UIEffect_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
