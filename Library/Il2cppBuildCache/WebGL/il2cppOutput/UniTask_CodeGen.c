﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void System.Runtime.CompilerServices.AsyncMethodBuilderAttribute::.ctor(System.Type)
extern void AsyncMethodBuilderAttribute__ctor_mE063A71B5304DD0DD014F095271112AC8CBB9CDB (void);
// 0x00000002 System.Int32 Cysharp.Threading.Tasks.AsyncUnit::GetHashCode()
extern void AsyncUnit_GetHashCode_m313965D898D515FE5DF5464ED8C0C9EF64B9F0C6 (void);
// 0x00000003 System.Boolean Cysharp.Threading.Tasks.AsyncUnit::Equals(Cysharp.Threading.Tasks.AsyncUnit)
extern void AsyncUnit_Equals_m7F9CC10F7DDF7211EAD2D0A56B413C82156C2E07 (void);
// 0x00000004 System.String Cysharp.Threading.Tasks.AsyncUnit::ToString()
extern void AsyncUnit_ToString_m3BB12D2C87E38E4D5B1D1A6F3BD3EEC6F2A6488F (void);
// 0x00000005 System.Void Cysharp.Threading.Tasks.CancellationTokenExtensions::Callback(System.Object)
extern void CancellationTokenExtensions_Callback_mAAAA5DC087F3C99992626783AC1D13515FE0A51C (void);
// 0x00000006 System.Threading.CancellationTokenRegistration Cysharp.Threading.Tasks.CancellationTokenExtensions::RegisterWithoutCaptureExecutionContext(System.Threading.CancellationToken,System.Action`1<System.Object>,System.Object)
extern void CancellationTokenExtensions_RegisterWithoutCaptureExecutionContext_mFA67F8204DAD04F3F4E4CC416B5E8055A343A08F (void);
// 0x00000007 System.Void Cysharp.Threading.Tasks.CancellationTokenExtensions::DisposeCallback(System.Object)
extern void CancellationTokenExtensions_DisposeCallback_mA0FA6FDDC70BC2CDBA14B6B20D75750C1A1BB97A (void);
// 0x00000008 System.Void Cysharp.Threading.Tasks.CancellationTokenExtensions::.cctor()
extern void CancellationTokenExtensions__cctor_m9D16193F0DF4E5D40308F660E4185900D6C8FB02 (void);
// 0x00000009 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.IUniTaskSource::GetStatus(System.Int16)
// 0x0000000A System.Void Cysharp.Threading.Tasks.IUniTaskSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x0000000B System.Void Cysharp.Threading.Tasks.IUniTaskSource::GetResult(System.Int16)
// 0x0000000C Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.IUniTaskSource::UnsafeGetStatus()
// 0x0000000D T Cysharp.Threading.Tasks.IUniTaskSource`1::GetResult(System.Int16)
// 0x0000000E System.Boolean Cysharp.Threading.Tasks.UniTaskStatusExtensions::IsCompleted(Cysharp.Threading.Tasks.UniTaskStatus)
extern void UniTaskStatusExtensions_IsCompleted_m7A9E3A6930C2EB3ED293F11782F2E0A16FAF8E71 (void);
// 0x0000000F System.Boolean Cysharp.Threading.Tasks.MoveNextSource::GetResult(System.Int16)
extern void MoveNextSource_GetResult_m7D5CA709DB1E8E3A326C535CFEE3C4B193984509 (void);
// 0x00000010 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.MoveNextSource::GetStatus(System.Int16)
extern void MoveNextSource_GetStatus_m6B5BCBA538463914A0763C6D07E7BF30052535E0 (void);
// 0x00000011 System.Void Cysharp.Threading.Tasks.MoveNextSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void MoveNextSource_OnCompleted_m5C664C312F657C622D45B340327F09E023B59E98 (void);
// 0x00000012 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.MoveNextSource::UnsafeGetStatus()
extern void MoveNextSource_UnsafeGetStatus_mDDDE1D2E5038488B1F28655FDE2788B8AAEF27B5 (void);
// 0x00000013 System.Void Cysharp.Threading.Tasks.MoveNextSource::Cysharp.Threading.Tasks.IUniTaskSource.GetResult(System.Int16)
extern void MoveNextSource_Cysharp_Threading_Tasks_IUniTaskSource_GetResult_m5E9EA3CC0CA5AB3F3CE8CFB681997415D0282081 (void);
// 0x00000014 System.Void Cysharp.Threading.Tasks.MoveNextSource::.ctor()
extern void MoveNextSource__ctor_mF5C753B441B348A43DE69243830C74E2671D770A (void);
// 0x00000015 System.Boolean Cysharp.Threading.Tasks.IPlayerLoopItem::MoveNext()
// 0x00000016 System.Threading.SynchronizationContext Cysharp.Threading.Tasks.PlayerLoopHelper::get_UnitySynchronizationContext()
extern void PlayerLoopHelper_get_UnitySynchronizationContext_m6702A1ECB0D27155EDFEDFF97585FCE0BFFBB9A2 (void);
// 0x00000017 System.Int32 Cysharp.Threading.Tasks.PlayerLoopHelper::get_MainThreadId()
extern void PlayerLoopHelper_get_MainThreadId_m2FB5751481BE90F5CF3A74EBD9EDA1B5196148DB (void);
// 0x00000018 System.String Cysharp.Threading.Tasks.PlayerLoopHelper::get_ApplicationDataPath()
extern void PlayerLoopHelper_get_ApplicationDataPath_m9FEB5363657F8AD39E87CADC5C130661FA83F9C7 (void);
// 0x00000019 System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper::get_IsMainThread()
extern void PlayerLoopHelper_get_IsMainThread_m07256518E19B3A4EDE421D301F21B88219DB28E1 (void);
// 0x0000001A System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper::get_IsEditorApplicationQuitting()
extern void PlayerLoopHelper_get_IsEditorApplicationQuitting_m41F5E63B94BAA7BC025829359D13C0101BD74551 (void);
// 0x0000001B System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::set_IsEditorApplicationQuitting(System.Boolean)
extern void PlayerLoopHelper_set_IsEditorApplicationQuitting_m8FE072C71C6415A874DC4E074DFA91B8E81C1630 (void);
// 0x0000001C UnityEngine.LowLevel.PlayerLoopSystem[] Cysharp.Threading.Tasks.PlayerLoopHelper::InsertRunner(UnityEngine.LowLevel.PlayerLoopSystem,System.Boolean,System.Type,Cysharp.Threading.Tasks.Internal.ContinuationQueue,System.Type,Cysharp.Threading.Tasks.Internal.PlayerLoopRunner)
extern void PlayerLoopHelper_InsertRunner_m5EEF89AF5BE51154A325E7E9E2A2AF23B1C00739 (void);
// 0x0000001D UnityEngine.LowLevel.PlayerLoopSystem[] Cysharp.Threading.Tasks.PlayerLoopHelper::RemoveRunner(UnityEngine.LowLevel.PlayerLoopSystem,System.Type,System.Type)
extern void PlayerLoopHelper_RemoveRunner_m6FF4390C00AEED28ACB66E5EB7B123EC5098F2D3 (void);
// 0x0000001E UnityEngine.LowLevel.PlayerLoopSystem[] Cysharp.Threading.Tasks.PlayerLoopHelper::InsertUniTaskSynchronizationContext(UnityEngine.LowLevel.PlayerLoopSystem)
extern void PlayerLoopHelper_InsertUniTaskSynchronizationContext_mAA2E6C7D43E83D57DA53C399A002208F372DBA17 (void);
// 0x0000001F System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::Init()
extern void PlayerLoopHelper_Init_mD46E29166AB848484B0BB715017D5CC9144E51E1 (void);
// 0x00000020 System.Int32 Cysharp.Threading.Tasks.PlayerLoopHelper::FindLoopSystemIndex(UnityEngine.LowLevel.PlayerLoopSystem[],System.Type)
extern void PlayerLoopHelper_FindLoopSystemIndex_m4106AF197FEFB5C4CFE4810CF41FC6445D45103C (void);
// 0x00000021 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::InsertLoop(UnityEngine.LowLevel.PlayerLoopSystem[],Cysharp.Threading.Tasks.InjectPlayerLoopTimings,System.Type,Cysharp.Threading.Tasks.InjectPlayerLoopTimings,System.Int32,System.Boolean,System.Type,System.Type,Cysharp.Threading.Tasks.PlayerLoopTiming)
extern void PlayerLoopHelper_InsertLoop_m74221E1763E303D938F604BEBE2661BB030B6A4D (void);
// 0x00000022 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::Initialize(UnityEngine.LowLevel.PlayerLoopSystem&,Cysharp.Threading.Tasks.InjectPlayerLoopTimings)
extern void PlayerLoopHelper_Initialize_mAA02BE296392A43E3EF3F7A59A385085CB6C4D92 (void);
// 0x00000023 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::AddAction(Cysharp.Threading.Tasks.PlayerLoopTiming,Cysharp.Threading.Tasks.IPlayerLoopItem)
extern void PlayerLoopHelper_AddAction_mF282616C4AF98C738AAC104B74ECCD8B7C1AB429 (void);
// 0x00000024 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::ThrowInvalidLoopTiming(Cysharp.Threading.Tasks.PlayerLoopTiming)
extern void PlayerLoopHelper_ThrowInvalidLoopTiming_mC327106A4D15F456C694A0B1FF2C6D3217EEC125 (void);
// 0x00000025 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::AddContinuation(Cysharp.Threading.Tasks.PlayerLoopTiming,System.Action)
extern void PlayerLoopHelper_AddContinuation_m8F5569426F16FE3C4955AA4E10C6059EE9DC011C (void);
// 0x00000026 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::DumpCurrentPlayerLoop()
extern void PlayerLoopHelper_DumpCurrentPlayerLoop_mD2D86A1A43A50F07E46C4FA1F985CCEC68262B21 (void);
// 0x00000027 System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper::IsInjectedUniTaskPlayerLoop()
extern void PlayerLoopHelper_IsInjectedUniTaskPlayerLoop_m4B9629CAA7CC5DCEFFC804511732F7244A78DD6F (void);
// 0x00000028 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper::.cctor()
extern void PlayerLoopHelper__cctor_mEF9C6111E858A0E4413C3DCD2D60388780176516 (void);
// 0x00000029 System.Void Cysharp.Threading.Tasks.PlayerLoopHelper/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m5ACAD2E0947A0B8F7D33044A3600C9408B450EDA (void);
// 0x0000002A System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper/<>c__DisplayClass20_0::<RemoveRunner>b__0(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec__DisplayClass20_0_U3CRemoveRunnerU3Eb__0_m0301B2D473647BEF6EFAB4294494120C1BC2334D (void);
// 0x0000002B System.Void Cysharp.Threading.Tasks.PlayerLoopHelper/<>c::.cctor()
extern void U3CU3Ec__cctor_m98F2D6A1D5F93BE5E662BEFD3A504F022BCC45FF (void);
// 0x0000002C System.Void Cysharp.Threading.Tasks.PlayerLoopHelper/<>c::.ctor()
extern void U3CU3Ec__ctor_m1E750A7D82E38074DF51132188C7903529BD5731 (void);
// 0x0000002D System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper/<>c::<InsertUniTaskSynchronizationContext>b__21_0(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec_U3CInsertUniTaskSynchronizationContextU3Eb__21_0_m79CCFF31F329420228E4238BEBB53471A2D5FEF6 (void);
// 0x0000002E System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper/<>c::<InsertUniTaskSynchronizationContext>b__21_1(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec_U3CInsertUniTaskSynchronizationContextU3Eb__21_1_mB363C6A0536592BDB61D24C39A100B168CB0C306 (void);
// 0x0000002F System.Boolean Cysharp.Threading.Tasks.PlayerLoopHelper/<>c::<InsertUniTaskSynchronizationContext>b__21_2(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec_U3CInsertUniTaskSynchronizationContextU3Eb__21_2_m4B18558D5EECF740CE7A17C98C2248929EB7EE5A (void);
// 0x00000030 System.Void Cysharp.Threading.Tasks.TaskPool::.cctor()
extern void TaskPool__cctor_m3B5AE687814E0DDB21DE2BD209E841660819F58F (void);
// 0x00000031 System.Void Cysharp.Threading.Tasks.TaskPool::RegisterSizeGetter(System.Type,System.Func`1<System.Int32>)
extern void TaskPool_RegisterSizeGetter_m43ED66B9FB98E95BB57695F7BF32B2C43A45C99F (void);
// 0x00000032 T& Cysharp.Threading.Tasks.ITaskPoolNode`1::get_NextNode()
// 0x00000033 System.Int32 Cysharp.Threading.Tasks.TaskPool`1::get_Size()
// 0x00000034 System.Boolean Cysharp.Threading.Tasks.TaskPool`1::TryPop(T&)
// 0x00000035 System.Boolean Cysharp.Threading.Tasks.TaskPool`1::TryPush(T)
// 0x00000036 System.Void Cysharp.Threading.Tasks.ITriggerHandler`1::OnNext(T)
// 0x00000037 System.Void Cysharp.Threading.Tasks.ITriggerHandler`1::OnCompleted()
// 0x00000038 Cysharp.Threading.Tasks.ITriggerHandler`1<T> Cysharp.Threading.Tasks.ITriggerHandler`1::get_Prev()
// 0x00000039 System.Void Cysharp.Threading.Tasks.ITriggerHandler`1::set_Prev(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x0000003A Cysharp.Threading.Tasks.ITriggerHandler`1<T> Cysharp.Threading.Tasks.ITriggerHandler`1::get_Next()
// 0x0000003B System.Void Cysharp.Threading.Tasks.ITriggerHandler`1::set_Next(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x0000003C System.Void Cysharp.Threading.Tasks.TriggerEvent`1::LogError(System.Exception)
// 0x0000003D System.Void Cysharp.Threading.Tasks.TriggerEvent`1::SetResult(T)
// 0x0000003E System.Void Cysharp.Threading.Tasks.TriggerEvent`1::SetCompleted()
// 0x0000003F System.Void Cysharp.Threading.Tasks.TriggerEvent`1::Add(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x00000040 System.Void Cysharp.Threading.Tasks.TriggerEvent`1::Remove(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x00000041 Cysharp.Threading.Tasks.YieldAwaitable Cysharp.Threading.Tasks.UniTask::Yield()
extern void UniTask_Yield_m9D90B9C5DAB9B3FB34E3C7D027C891E8A2B5765E (void);
// 0x00000042 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask::Delay(System.Int32,System.Boolean,Cysharp.Threading.Tasks.PlayerLoopTiming,System.Threading.CancellationToken)
extern void UniTask_Delay_mEAF888C9FC626692FA569A01B9AFC320D1D7AD92 (void);
// 0x00000043 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask::Delay(System.TimeSpan,System.Boolean,Cysharp.Threading.Tasks.PlayerLoopTiming,System.Threading.CancellationToken)
extern void UniTask_Delay_m45C85FFC2C1FF43826381B27808DFAFA70B2DE4C (void);
// 0x00000044 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask::Delay(System.TimeSpan,Cysharp.Threading.Tasks.DelayType,Cysharp.Threading.Tasks.PlayerLoopTiming,System.Threading.CancellationToken)
extern void UniTask_Delay_m3D50FDD01A456066EBD0B9CD393D55092B34DA64 (void);
// 0x00000045 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask::FromException(System.Exception)
extern void UniTask_FromException_m3A5CDCDC99E68786578B2E57CCE0E899850B3DB0 (void);
// 0x00000046 Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.UniTask::FromException(System.Exception)
// 0x00000047 Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.UniTask::FromResult(T)
// 0x00000048 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask::FromCanceled(System.Threading.CancellationToken)
extern void UniTask_FromCanceled_m0BC69BD2FB4CCDA259486447594863EC1891EAB1 (void);
// 0x00000049 Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.UniTask::FromCanceled(System.Threading.CancellationToken)
// 0x0000004A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask::WhenAll(Cysharp.Threading.Tasks.UniTask[])
extern void UniTask_WhenAll_mC35EDD17A7A000CF85450B5361B331B2CD5DCD5B (void);
// 0x0000004B System.Void Cysharp.Threading.Tasks.UniTask::.ctor(Cysharp.Threading.Tasks.IUniTaskSource,System.Int16)
extern void UniTask__ctor_mA1D2FAF1D02391065D770F56EE3CFD028157CACD (void);
// 0x0000004C Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask::get_Status()
extern void UniTask_get_Status_m5CCD7973CFD636CF45F67AF23CC34349ABBAD0AD (void);
// 0x0000004D Cysharp.Threading.Tasks.UniTask/Awaiter Cysharp.Threading.Tasks.UniTask::GetAwaiter()
extern void UniTask_GetAwaiter_m1FE35EBA18DB40141C1025CECD4FC300E3A8C59C (void);
// 0x0000004E System.String Cysharp.Threading.Tasks.UniTask::ToString()
extern void UniTask_ToString_m4BF6DB11E21177970C4F895B04F0D3ED6673A215 (void);
// 0x0000004F System.Void Cysharp.Threading.Tasks.UniTask::.cctor()
extern void UniTask__cctor_m45B30383FFD9CE612F1C612F450BCAEEF1146C7D (void);
// 0x00000050 Cysharp.Threading.Tasks.UniTask/DelayPromise& Cysharp.Threading.Tasks.UniTask/DelayPromise::get_NextNode()
extern void DelayPromise_get_NextNode_m84F340FEC343C1A6E7803817B4D01880923A79D7 (void);
// 0x00000051 System.Void Cysharp.Threading.Tasks.UniTask/DelayPromise::.cctor()
extern void DelayPromise__cctor_m70D49DE0BB2FE45DF4466FB79A8DAA27238EF8FD (void);
// 0x00000052 System.Void Cysharp.Threading.Tasks.UniTask/DelayPromise::.ctor()
extern void DelayPromise__ctor_m86C2534E0D2671065C6A96648EF805D2835E0ECD (void);
// 0x00000053 Cysharp.Threading.Tasks.IUniTaskSource Cysharp.Threading.Tasks.UniTask/DelayPromise::Create(System.TimeSpan,Cysharp.Threading.Tasks.PlayerLoopTiming,System.Threading.CancellationToken,System.Int16&)
extern void DelayPromise_Create_mF031A3A8FD045DFAEF90BDF65C15DCFF9ABAAA02 (void);
// 0x00000054 System.Void Cysharp.Threading.Tasks.UniTask/DelayPromise::GetResult(System.Int16)
extern void DelayPromise_GetResult_m10ADD62F5CA36929284A7E4E34E76C1EB7ABC64A (void);
// 0x00000055 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/DelayPromise::GetStatus(System.Int16)
extern void DelayPromise_GetStatus_mEDBE0E5FEBD402F3B3273726AFC4924CEE628F7B (void);
// 0x00000056 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/DelayPromise::UnsafeGetStatus()
extern void DelayPromise_UnsafeGetStatus_m6867F464EBC6DDF793DAFD75A03C2CA7C4C6D756 (void);
// 0x00000057 System.Void Cysharp.Threading.Tasks.UniTask/DelayPromise::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void DelayPromise_OnCompleted_mD22E8ABCD7E791401EEF538EBB1CD785EEDC512F (void);
// 0x00000058 System.Boolean Cysharp.Threading.Tasks.UniTask/DelayPromise::MoveNext()
extern void DelayPromise_MoveNext_mAD44B8BDF81E7483379F52C67975D842939A3AEB (void);
// 0x00000059 System.Boolean Cysharp.Threading.Tasks.UniTask/DelayPromise::TryReturn()
extern void DelayPromise_TryReturn_mF9284E286BFCF8977449417A8D1BA917C9C826AE (void);
// 0x0000005A System.Void Cysharp.Threading.Tasks.UniTask/DelayPromise/<>c::.cctor()
extern void U3CU3Ec__cctor_mD32219EC581E38D75539FC5A42B7116FD2FEF6E2 (void);
// 0x0000005B System.Void Cysharp.Threading.Tasks.UniTask/DelayPromise/<>c::.ctor()
extern void U3CU3Ec__ctor_m1887ED2AB50224DE4EF4CF6582FCDE748D5C1630 (void);
// 0x0000005C System.Int32 Cysharp.Threading.Tasks.UniTask/DelayPromise/<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_m2D6234D973A4CF0FECA02D5B7EE3B5F1A768C926 (void);
// 0x0000005D Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise& Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::get_NextNode()
extern void DelayIgnoreTimeScalePromise_get_NextNode_mB89D3615EE78CE3AD3900B1AB7D43A80CFEC0978 (void);
// 0x0000005E System.Void Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::.cctor()
extern void DelayIgnoreTimeScalePromise__cctor_m27098F2A762AE7C6E07F9724A2E04983C7CA6773 (void);
// 0x0000005F System.Void Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::.ctor()
extern void DelayIgnoreTimeScalePromise__ctor_m1F7027E5F2F540C2B13A0A959B2004D30B20C3D7 (void);
// 0x00000060 Cysharp.Threading.Tasks.IUniTaskSource Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::Create(System.TimeSpan,Cysharp.Threading.Tasks.PlayerLoopTiming,System.Threading.CancellationToken,System.Int16&)
extern void DelayIgnoreTimeScalePromise_Create_m4961C20E2730FF8CA837FB95FB45BE808385C5E6 (void);
// 0x00000061 System.Void Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::GetResult(System.Int16)
extern void DelayIgnoreTimeScalePromise_GetResult_mEC798B6FFD702639DAFFB6197BB69BDDCF016053 (void);
// 0x00000062 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::GetStatus(System.Int16)
extern void DelayIgnoreTimeScalePromise_GetStatus_mB5152C3A164DD52624DDCFCF9DF3A9418C14AE83 (void);
// 0x00000063 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::UnsafeGetStatus()
extern void DelayIgnoreTimeScalePromise_UnsafeGetStatus_mA1A740FF4EDBD217FD6277C92ADCA80CFEEC546E (void);
// 0x00000064 System.Void Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void DelayIgnoreTimeScalePromise_OnCompleted_mFFE816798DF39DB20C95F9FE2D3589BA2921693C (void);
// 0x00000065 System.Boolean Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::MoveNext()
extern void DelayIgnoreTimeScalePromise_MoveNext_mC1066D2E2223F30DC9D2890D3F9D687FC7A8D863 (void);
// 0x00000066 System.Boolean Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise::TryReturn()
extern void DelayIgnoreTimeScalePromise_TryReturn_m0C5CA23820326509633B9BE717266C59C39849F7 (void);
// 0x00000067 System.Void Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise/<>c::.cctor()
extern void U3CU3Ec__cctor_m69B7EE04A226E23022C08C8D389A93149C74AC15 (void);
// 0x00000068 System.Void Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise/<>c::.ctor()
extern void U3CU3Ec__ctor_m6856E5126E9A56584EC7358DF07551B8DAD74E8B (void);
// 0x00000069 System.Int32 Cysharp.Threading.Tasks.UniTask/DelayIgnoreTimeScalePromise/<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_mC13B74D8452862A7FB4F614EA3075CA3E6AD8017 (void);
// 0x0000006A Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise& Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::get_NextNode()
extern void DelayRealtimePromise_get_NextNode_m09265DA87003AA9DC60843987CCEC274A5C8C340 (void);
// 0x0000006B System.Void Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::.cctor()
extern void DelayRealtimePromise__cctor_mB726D240EAAD9A21C218C43CA20692C09BBB6167 (void);
// 0x0000006C System.Void Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::.ctor()
extern void DelayRealtimePromise__ctor_mEF4B7AF172B64C94D6394E92C2E0EEE62DBE75E5 (void);
// 0x0000006D Cysharp.Threading.Tasks.IUniTaskSource Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::Create(System.TimeSpan,Cysharp.Threading.Tasks.PlayerLoopTiming,System.Threading.CancellationToken,System.Int16&)
extern void DelayRealtimePromise_Create_m7CA136381C44D0966ECE918DBF42F01D69D52FCE (void);
// 0x0000006E System.Void Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::GetResult(System.Int16)
extern void DelayRealtimePromise_GetResult_m618BC0D60E9A1AA8A7A777B42FF9240836D1AE03 (void);
// 0x0000006F Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::GetStatus(System.Int16)
extern void DelayRealtimePromise_GetStatus_mAEF59103E3D1DCD3C9DDF626D334FDE1537662E0 (void);
// 0x00000070 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::UnsafeGetStatus()
extern void DelayRealtimePromise_UnsafeGetStatus_m9381F2AC496BEB44D54188EF2F830F491667016E (void);
// 0x00000071 System.Void Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void DelayRealtimePromise_OnCompleted_mB21E2E33020C7750B04292458E4F91107017DE68 (void);
// 0x00000072 System.Boolean Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::MoveNext()
extern void DelayRealtimePromise_MoveNext_m20927B3514CA36B4F4CF87088B097A4BC3A6B516 (void);
// 0x00000073 System.Boolean Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise::TryReturn()
extern void DelayRealtimePromise_TryReturn_m0D940FFFA1506E7F27200CCECC4B733301D79E19 (void);
// 0x00000074 System.Void Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise/<>c::.cctor()
extern void U3CU3Ec__cctor_mE68B8CDFBA08013BF0E1BC1DDDD3E07537F51291 (void);
// 0x00000075 System.Void Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise/<>c::.ctor()
extern void U3CU3Ec__ctor_m7CF4C7BB8863530D2F3CCE7159C4D80C2C125204 (void);
// 0x00000076 System.Int32 Cysharp.Threading.Tasks.UniTask/DelayRealtimePromise/<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_m5CFB794D524A8FB0763DB4DC44D07AA4C723B6AD (void);
// 0x00000077 System.Void Cysharp.Threading.Tasks.UniTask/CanceledUniTaskCache`1::.cctor()
// 0x00000078 System.Void Cysharp.Threading.Tasks.UniTask/ExceptionResultSource::.ctor(System.Exception)
extern void ExceptionResultSource__ctor_m3EDA22E3AB29A3BFCAFB32EB3EF3F71572A9F763 (void);
// 0x00000079 System.Void Cysharp.Threading.Tasks.UniTask/ExceptionResultSource::GetResult(System.Int16)
extern void ExceptionResultSource_GetResult_m2D897950212D69E5EBD76D5434352B5DB51CC555 (void);
// 0x0000007A Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/ExceptionResultSource::GetStatus(System.Int16)
extern void ExceptionResultSource_GetStatus_m17E2C4B0161574E12EC3D6886CE17FEEA2FD4C81 (void);
// 0x0000007B Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/ExceptionResultSource::UnsafeGetStatus()
extern void ExceptionResultSource_UnsafeGetStatus_m148703AA3363F58FC8D95D73196C8D7DBB70506D (void);
// 0x0000007C System.Void Cysharp.Threading.Tasks.UniTask/ExceptionResultSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void ExceptionResultSource_OnCompleted_m31BB9A8AA43CA20170C3427A119BB7916E45C0E6 (void);
// 0x0000007D System.Void Cysharp.Threading.Tasks.UniTask/ExceptionResultSource`1::.ctor(System.Exception)
// 0x0000007E T Cysharp.Threading.Tasks.UniTask/ExceptionResultSource`1::GetResult(System.Int16)
// 0x0000007F System.Void Cysharp.Threading.Tasks.UniTask/ExceptionResultSource`1::Cysharp.Threading.Tasks.IUniTaskSource.GetResult(System.Int16)
// 0x00000080 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/ExceptionResultSource`1::GetStatus(System.Int16)
// 0x00000081 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/ExceptionResultSource`1::UnsafeGetStatus()
// 0x00000082 System.Void Cysharp.Threading.Tasks.UniTask/ExceptionResultSource`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x00000083 System.Void Cysharp.Threading.Tasks.UniTask/CanceledResultSource::.ctor(System.Threading.CancellationToken)
extern void CanceledResultSource__ctor_m77386FA032887459D5FBC0CB7C05DFF730AE99A0 (void);
// 0x00000084 System.Void Cysharp.Threading.Tasks.UniTask/CanceledResultSource::GetResult(System.Int16)
extern void CanceledResultSource_GetResult_m4E1F63AC3B076A2433889BD86C8BF7AA4D0ED137 (void);
// 0x00000085 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/CanceledResultSource::GetStatus(System.Int16)
extern void CanceledResultSource_GetStatus_m6404471BF151EA3B4157D2F8B4DFF3CB0E3C6E5E (void);
// 0x00000086 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/CanceledResultSource::UnsafeGetStatus()
extern void CanceledResultSource_UnsafeGetStatus_mDAAFAC3FD8E197FDA5BF4ED902FDB1D9B6EA0921 (void);
// 0x00000087 System.Void Cysharp.Threading.Tasks.UniTask/CanceledResultSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void CanceledResultSource_OnCompleted_mDA548D8E0328E86B2A1FD4AD2D594248760400C8 (void);
// 0x00000088 System.Void Cysharp.Threading.Tasks.UniTask/CanceledResultSource`1::.ctor(System.Threading.CancellationToken)
// 0x00000089 T Cysharp.Threading.Tasks.UniTask/CanceledResultSource`1::GetResult(System.Int16)
// 0x0000008A System.Void Cysharp.Threading.Tasks.UniTask/CanceledResultSource`1::Cysharp.Threading.Tasks.IUniTaskSource.GetResult(System.Int16)
// 0x0000008B Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/CanceledResultSource`1::GetStatus(System.Int16)
// 0x0000008C Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/CanceledResultSource`1::UnsafeGetStatus()
// 0x0000008D System.Void Cysharp.Threading.Tasks.UniTask/CanceledResultSource`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x0000008E System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise::.ctor(Cysharp.Threading.Tasks.UniTask[],System.Int32)
extern void WhenAllPromise__ctor_m15F92C2AFC53966643FD3D851D140470A8EEBFDC (void);
// 0x0000008F System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise::TryInvokeContinuation(Cysharp.Threading.Tasks.UniTask/WhenAllPromise,Cysharp.Threading.Tasks.UniTask/Awaiter&)
extern void WhenAllPromise_TryInvokeContinuation_m457A2DC5D19EDBEDC48F15C85740B408F43FB8BB (void);
// 0x00000090 System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise::GetResult(System.Int16)
extern void WhenAllPromise_GetResult_m277437082708A5A1213A1C5B88DD0DF422F28717 (void);
// 0x00000091 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/WhenAllPromise::GetStatus(System.Int16)
extern void WhenAllPromise_GetStatus_m631854632BB8FB4F2ED316817CAF599929D75438 (void);
// 0x00000092 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask/WhenAllPromise::UnsafeGetStatus()
extern void WhenAllPromise_UnsafeGetStatus_m4E73CAB8322D3E482947310063320D0308FE6368 (void);
// 0x00000093 System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void WhenAllPromise_OnCompleted_m2D3FB5AE8BA34D8E8B51CDEEC74460FB12B3837A (void);
// 0x00000094 System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise/<>c::.cctor()
extern void U3CU3Ec__cctor_m3941202D45EF5640744E45504357C4A147411FD3 (void);
// 0x00000095 System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise/<>c::.ctor()
extern void U3CU3Ec__ctor_mF8A6C798E29EEF181BEDCB2923BAD58BA7D7D1D8 (void);
// 0x00000096 System.Void Cysharp.Threading.Tasks.UniTask/WhenAllPromise/<>c::<.ctor>b__3_0(System.Object)
extern void U3CU3Ec_U3C_ctorU3Eb__3_0_mBA91021837B4ED21A956C6957D976125ED974066 (void);
// 0x00000097 System.Void Cysharp.Threading.Tasks.UniTask/Awaiter::.ctor(Cysharp.Threading.Tasks.UniTask&)
extern void Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF (void);
// 0x00000098 System.Boolean Cysharp.Threading.Tasks.UniTask/Awaiter::get_IsCompleted()
extern void Awaiter_get_IsCompleted_mDCC71AAE7FA5EA2B303D3EC512177FD122C8DA49 (void);
// 0x00000099 System.Void Cysharp.Threading.Tasks.UniTask/Awaiter::GetResult()
extern void Awaiter_GetResult_mFCA8BA81A9A7DA641DEFC7FC121FED87A3EC06CA (void);
// 0x0000009A System.Void Cysharp.Threading.Tasks.UniTask/Awaiter::OnCompleted(System.Action)
extern void Awaiter_OnCompleted_m080A1DBE2E806E1CDE6931514C3A6C8DBDF2E646 (void);
// 0x0000009B System.Void Cysharp.Threading.Tasks.UniTask/Awaiter::UnsafeOnCompleted(System.Action)
extern void Awaiter_UnsafeOnCompleted_m1428D2FC0145C8CABB14E4B9A15F38B6AE34F8E5 (void);
// 0x0000009C System.Void Cysharp.Threading.Tasks.UniTask/Awaiter::SourceOnCompleted(System.Action`1<System.Object>,System.Object)
extern void Awaiter_SourceOnCompleted_m71BEAA335CDD0FBD24F14F9B970B8E1B69AE01B4 (void);
// 0x0000009D System.Void Cysharp.Threading.Tasks.UniTask/<>c::.cctor()
extern void U3CU3Ec__cctor_m902D699C440A7E7E9ECCFB515C6293403DA46A5E (void);
// 0x0000009E System.Void Cysharp.Threading.Tasks.UniTask/<>c::.ctor()
extern void U3CU3Ec__ctor_mC6E69A9C636DA9F58B33651F73B729A6A3CA4626 (void);
// 0x0000009F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTask/<>c::<.cctor>b__174_0()
extern void U3CU3Ec_U3C_cctorU3Eb__174_0_mBCF7F67BA74FEC483866CCE43266BB39E532932B (void);
// 0x000000A0 System.Void Cysharp.Threading.Tasks.YieldAwaitable::.ctor(Cysharp.Threading.Tasks.PlayerLoopTiming)
extern void YieldAwaitable__ctor_mDFCED8E03E478AA784DCBAF67876091D6457590F (void);
// 0x000000A1 Cysharp.Threading.Tasks.YieldAwaitable/Awaiter Cysharp.Threading.Tasks.YieldAwaitable::GetAwaiter()
extern void YieldAwaitable_GetAwaiter_mABCF83C041AEE78B7915625989429CF87DC28EC9 (void);
// 0x000000A2 System.Void Cysharp.Threading.Tasks.YieldAwaitable/Awaiter::.ctor(Cysharp.Threading.Tasks.PlayerLoopTiming)
extern void Awaiter__ctor_m05FC94348E722A3E503CD81CE62E02A5163A58F0 (void);
// 0x000000A3 System.Boolean Cysharp.Threading.Tasks.YieldAwaitable/Awaiter::get_IsCompleted()
extern void Awaiter_get_IsCompleted_mF92B16767983ACC6C69D07E32F866F14E3E3A830 (void);
// 0x000000A4 System.Void Cysharp.Threading.Tasks.YieldAwaitable/Awaiter::GetResult()
extern void Awaiter_GetResult_m5B7F9896A518DB6316AD7C1741AFA0441CA40B3F (void);
// 0x000000A5 System.Void Cysharp.Threading.Tasks.YieldAwaitable/Awaiter::OnCompleted(System.Action)
extern void Awaiter_OnCompleted_mB06C744A459C12B974DF387EB24B33B8663524F7 (void);
// 0x000000A6 System.Void Cysharp.Threading.Tasks.YieldAwaitable/Awaiter::UnsafeOnCompleted(System.Action)
extern void Awaiter_UnsafeOnCompleted_m7891BED0BBDFA9C626E10CD515326D3E1A859634 (void);
// 0x000000A7 System.Void Cysharp.Threading.Tasks.AwaiterActions::Continuation(System.Object)
extern void AwaiterActions_Continuation_m6C950CC84C028955999620A11573A87154D4F2B9 (void);
// 0x000000A8 System.Void Cysharp.Threading.Tasks.AwaiterActions::.cctor()
extern void AwaiterActions__cctor_mBD78C0AA7AB86026FADD8D4A3C83939FFEF73D23 (void);
// 0x000000A9 System.Void Cysharp.Threading.Tasks.UniTask`1::.ctor(T)
// 0x000000AA System.Void Cysharp.Threading.Tasks.UniTask`1::.ctor(Cysharp.Threading.Tasks.IUniTaskSource`1<T>,System.Int16)
// 0x000000AB Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTask`1::get_Status()
// 0x000000AC Cysharp.Threading.Tasks.UniTask`1/Awaiter<T> Cysharp.Threading.Tasks.UniTask`1::GetAwaiter()
// 0x000000AD System.String Cysharp.Threading.Tasks.UniTask`1::ToString()
// 0x000000AE System.Void Cysharp.Threading.Tasks.UniTask`1/Awaiter::.ctor(Cysharp.Threading.Tasks.UniTask`1<T>&)
// 0x000000AF System.Boolean Cysharp.Threading.Tasks.UniTask`1/Awaiter::get_IsCompleted()
// 0x000000B0 T Cysharp.Threading.Tasks.UniTask`1/Awaiter::GetResult()
// 0x000000B1 System.Void Cysharp.Threading.Tasks.UniTask`1/Awaiter::OnCompleted(System.Action)
// 0x000000B2 System.Void Cysharp.Threading.Tasks.UniTask`1/Awaiter::UnsafeOnCompleted(System.Action)
// 0x000000B3 System.Void Cysharp.Threading.Tasks.ExceptionHolder::.ctor(System.Runtime.ExceptionServices.ExceptionDispatchInfo)
extern void ExceptionHolder__ctor_mACA78E10153393E338471868F382F842F20E7A5B (void);
// 0x000000B4 System.Runtime.ExceptionServices.ExceptionDispatchInfo Cysharp.Threading.Tasks.ExceptionHolder::GetException()
extern void ExceptionHolder_GetException_mC917B7558EA174CC6128C89165017F793D920B09 (void);
// 0x000000B5 System.Void Cysharp.Threading.Tasks.ExceptionHolder::Finalize()
extern void ExceptionHolder_Finalize_m82D25DF7F508A43AC6F79586FA9BDACB0DAA2354 (void);
// 0x000000B6 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::Reset()
// 0x000000B7 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::ReportUnhandledError()
// 0x000000B8 System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::TrySetResult(TResult)
// 0x000000B9 System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::TrySetException(System.Exception)
// 0x000000BA System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::TrySetCanceled(System.Threading.CancellationToken)
// 0x000000BB System.Int16 Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::get_Version()
// 0x000000BC Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::GetStatus(System.Int16)
// 0x000000BD Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::UnsafeGetStatus()
// 0x000000BE TResult Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::GetResult(System.Int16)
// 0x000000BF System.Void Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x000000C0 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSourceCore`1::ValidateToken(System.Int16)
// 0x000000C1 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSourceCoreShared::CompletionSentinel(System.Object)
extern void UniTaskCompletionSourceCoreShared_CompletionSentinel_m088BF1BB17117B2A50A114B1C6C067649F5C8768 (void);
// 0x000000C2 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSourceCoreShared::.cctor()
extern void UniTaskCompletionSourceCoreShared__cctor_mA4160F6CE3540A353D3F9B9C1B43188523482D7B (void);
// 0x000000C3 Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource& Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::get_NextNode()
extern void AutoResetUniTaskCompletionSource_get_NextNode_mBB71FA082E5CC41E1A2D2885F4FAE6374996B29D (void);
// 0x000000C4 System.Void Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::.cctor()
extern void AutoResetUniTaskCompletionSource__cctor_mA2356C28420E63AE061366E9E58451116FDA5C7C (void);
// 0x000000C5 System.Void Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::.ctor()
extern void AutoResetUniTaskCompletionSource__ctor_mE87351839003BD8B6C9ADBE8CB4C72B4EC361996 (void);
// 0x000000C6 Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::Create()
extern void AutoResetUniTaskCompletionSource_Create_mC4696C34F31CA03093424A4548E0D23999CA70D3 (void);
// 0x000000C7 Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::CreateFromCanceled(System.Threading.CancellationToken,System.Int16&)
extern void AutoResetUniTaskCompletionSource_CreateFromCanceled_m65C9559F6C548FD966D3A7BDB0C9BAA6B643C7CE (void);
// 0x000000C8 System.Boolean Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::TrySetCanceled(System.Threading.CancellationToken)
extern void AutoResetUniTaskCompletionSource_TrySetCanceled_m1228999B468118393A0BE24C4F290D4D376851CA (void);
// 0x000000C9 System.Void Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::GetResult(System.Int16)
extern void AutoResetUniTaskCompletionSource_GetResult_m1B23EAD60F9C66577E6708C8FE0F1B1E60C3D12C (void);
// 0x000000CA Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::GetStatus(System.Int16)
extern void AutoResetUniTaskCompletionSource_GetStatus_m09652E851FAC788D9936902498A7F351C988D23D (void);
// 0x000000CB Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::UnsafeGetStatus()
extern void AutoResetUniTaskCompletionSource_UnsafeGetStatus_m0E7D9961B936A86FFCBD5880459465D58E58D564 (void);
// 0x000000CC System.Void Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void AutoResetUniTaskCompletionSource_OnCompleted_m234B5E91D7C0E1B6EDACE14DF65AAD38742C47FF (void);
// 0x000000CD System.Boolean Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource::TryReturn()
extern void AutoResetUniTaskCompletionSource_TryReturn_mAE3E4441080A3493D20476911D735914E9F4212D (void);
// 0x000000CE System.Void Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource/<>c::.cctor()
extern void U3CU3Ec__cctor_m4EFFA1E2B75DFC2B46E630371E6B30B9FA0E2823 (void);
// 0x000000CF System.Void Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource/<>c::.ctor()
extern void U3CU3Ec__ctor_mF39FE9DF703393F9CC2DA2EE4A494FA4F40B521C (void);
// 0x000000D0 System.Int32 Cysharp.Threading.Tasks.AutoResetUniTaskCompletionSource/<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_mFD92DDA66D96BB26716BA5EC84DFB4F35ECB4D06 (void);
// 0x000000D1 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource::.ctor()
extern void UniTaskCompletionSource__ctor_m252D950A297A48EE25FDFDDFE111524C6598C0E4 (void);
// 0x000000D2 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource::MarkHandled()
extern void UniTaskCompletionSource_MarkHandled_m3EABFFA54BF7934490BCA86DA8F76AA023BE8144 (void);
// 0x000000D3 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.UniTaskCompletionSource::get_Task()
extern void UniTaskCompletionSource_get_Task_m71FDFD642F5DE334B395253D95E812AF5E191CD1 (void);
// 0x000000D4 System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSource::TrySetResult()
extern void UniTaskCompletionSource_TrySetResult_mD0DF7B8E60DD4FE6D4FC5EE0386CE075805E12DF (void);
// 0x000000D5 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource::GetResult(System.Int16)
extern void UniTaskCompletionSource_GetResult_m39A120F5B2805CF60DF0DDDF56AD13E6FE6BDED1 (void);
// 0x000000D6 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTaskCompletionSource::GetStatus(System.Int16)
extern void UniTaskCompletionSource_GetStatus_mED83FEAAD02A8E3145F2DA6625390DDFFC374002 (void);
// 0x000000D7 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTaskCompletionSource::UnsafeGetStatus()
extern void UniTaskCompletionSource_UnsafeGetStatus_m4C223CE67FF35004CE732D6ED295B814E3FD9E02 (void);
// 0x000000D8 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
extern void UniTaskCompletionSource_OnCompleted_mFF8DB2962201801A755B6B2EC59EFF6B3ADA4CC3 (void);
// 0x000000D9 System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSource::TrySignalCompletion(Cysharp.Threading.Tasks.UniTaskStatus)
extern void UniTaskCompletionSource_TrySignalCompletion_m1DF031CC4D1BC7CC43F6CF57FB5056173615B064 (void);
// 0x000000DA System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource`1::.ctor()
// 0x000000DB System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource`1::MarkHandled()
// 0x000000DC Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.UniTaskCompletionSource`1::get_Task()
// 0x000000DD System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSource`1::TrySetResult(T)
// 0x000000DE System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSource`1::TrySetCanceled(System.Threading.CancellationToken)
// 0x000000DF System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSource`1::TrySetException(System.Exception)
// 0x000000E0 T Cysharp.Threading.Tasks.UniTaskCompletionSource`1::GetResult(System.Int16)
// 0x000000E1 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource`1::Cysharp.Threading.Tasks.IUniTaskSource.GetResult(System.Int16)
// 0x000000E2 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTaskCompletionSource`1::GetStatus(System.Int16)
// 0x000000E3 Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.UniTaskCompletionSource`1::UnsafeGetStatus()
// 0x000000E4 System.Void Cysharp.Threading.Tasks.UniTaskCompletionSource`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x000000E5 System.Boolean Cysharp.Threading.Tasks.UniTaskCompletionSource`1::TrySignalCompletion(Cysharp.Threading.Tasks.UniTaskStatus)
// 0x000000E6 System.Void Cysharp.Threading.Tasks.UniTaskExtensions::Forget(Cysharp.Threading.Tasks.UniTask)
extern void UniTaskExtensions_Forget_m03A03134BA00CD83A83902AEF9352C8C0AC595A5 (void);
// 0x000000E7 System.Void Cysharp.Threading.Tasks.UniTaskExtensions/<>c::.cctor()
extern void U3CU3Ec__cctor_mA285C509DEFF7FA4F804AD59B0666DC18B4F50CA (void);
// 0x000000E8 System.Void Cysharp.Threading.Tasks.UniTaskExtensions/<>c::.ctor()
extern void U3CU3Ec__ctor_m2F38CAFAB4061F35011773F751E3385C14F2D29C (void);
// 0x000000E9 System.Void Cysharp.Threading.Tasks.UniTaskExtensions/<>c::<Forget>b__48_0(System.Object)
extern void U3CU3Ec_U3CForgetU3Eb__48_0_mA9B2D7825C681C612713C0BD5E8C9746E61BE272 (void);
// 0x000000EA Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.UniTaskObservableExtensions::ToUniTask(System.IObservable`1<T>,System.Boolean,System.Threading.CancellationToken)
// 0x000000EB System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/ToUniTaskObserver`1::.ctor(Cysharp.Threading.Tasks.UniTaskCompletionSource`1<T>,Cysharp.Threading.Tasks.Internal.SingleAssignmentDisposable,System.Threading.CancellationToken)
// 0x000000EC System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/ToUniTaskObserver`1::OnCanceled(System.Object)
// 0x000000ED System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/ToUniTaskObserver`1::OnNext(T)
// 0x000000EE System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/ToUniTaskObserver`1::OnError(System.Exception)
// 0x000000EF System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/ToUniTaskObserver`1::OnCompleted()
// 0x000000F0 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/ToUniTaskObserver`1::.cctor()
// 0x000000F1 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/FirstValueToUniTaskObserver`1::.ctor(Cysharp.Threading.Tasks.UniTaskCompletionSource`1<T>,Cysharp.Threading.Tasks.Internal.SingleAssignmentDisposable,System.Threading.CancellationToken)
// 0x000000F2 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/FirstValueToUniTaskObserver`1::OnCanceled(System.Object)
// 0x000000F3 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/FirstValueToUniTaskObserver`1::OnNext(T)
// 0x000000F4 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/FirstValueToUniTaskObserver`1::OnError(System.Exception)
// 0x000000F5 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/FirstValueToUniTaskObserver`1::OnCompleted()
// 0x000000F6 System.Void Cysharp.Threading.Tasks.UniTaskObservableExtensions/FirstValueToUniTaskObserver`1::.cctor()
// 0x000000F7 System.Void Cysharp.Threading.Tasks.UniTaskScheduler::InvokeUnobservedTaskException(System.Object)
extern void UniTaskScheduler_InvokeUnobservedTaskException_mB859444D8764BA7B062DBACE4C79E3D1ECE71F1D (void);
// 0x000000F8 System.Void Cysharp.Threading.Tasks.UniTaskScheduler::PublishUnobservedTaskException(System.Exception)
extern void UniTaskScheduler_PublishUnobservedTaskException_m4932FC5EE51B367FFF07E9E2FD42D24F8951FC11 (void);
// 0x000000F9 System.Void Cysharp.Threading.Tasks.UniTaskScheduler::.cctor()
extern void UniTaskScheduler__cctor_m7F4F0C3F92D11A9447999DEC1C7C9BC1C223EF77 (void);
// 0x000000FA System.Void Cysharp.Threading.Tasks.UniTaskSynchronizationContext::Run()
extern void UniTaskSynchronizationContext_Run_m5751075F0C4E830AF924C8375FB8BAF74D1C366A (void);
// 0x000000FB System.Void Cysharp.Threading.Tasks.UniTaskSynchronizationContext::.cctor()
extern void UniTaskSynchronizationContext__cctor_m2353BFC284D658ED3C78E0928E5053BBAC70A3FA (void);
// 0x000000FC System.Void Cysharp.Threading.Tasks.UniTaskSynchronizationContext/Callback::Invoke()
extern void Callback_Invoke_m380CCA09614F37D4ACF8EBFD996809A7E123E763 (void);
// 0x000000FD Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncAwakeTrigger::AwakeAsync()
extern void AsyncAwakeTrigger_AwakeAsync_mE54EFF0C28F9D8DBB69DEF7D3D6CB5526A363DED (void);
// 0x000000FE System.Void Cysharp.Threading.Tasks.Triggers.AsyncAwakeTrigger::.ctor()
extern void AsyncAwakeTrigger__ctor_m8E13CC1B068B30DB5B7149BADB561E43B42FF49F (void);
// 0x000000FF System.Threading.CancellationToken Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger::get_CancellationToken()
extern void AsyncDestroyTrigger_get_CancellationToken_mCEFB4CCD8C0AD6DF9430C6485464BC770F0D2F66 (void);
// 0x00000100 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger::Awake()
extern void AsyncDestroyTrigger_Awake_m82AFF16B342A001534AE5419AC313E0CBB46686F (void);
// 0x00000101 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger::OnDestroy()
extern void AsyncDestroyTrigger_OnDestroy_m48F9AD89E2A9B6EECD77EA3F2DDE52D3DC775F54 (void);
// 0x00000102 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger::OnDestroyAsync()
extern void AsyncDestroyTrigger_OnDestroyAsync_m81CBD8CC68A93CC570CED5B21FC08D31E23E18C4 (void);
// 0x00000103 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger::.ctor()
extern void AsyncDestroyTrigger__ctor_m49254BFE76A6173A9A48A049E688C60B1AEA004B (void);
// 0x00000104 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger/AwakeMonitor::.ctor(Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger)
extern void AwakeMonitor__ctor_m13FF87D0F2EC1F2650054E35566244122953EE71 (void);
// 0x00000105 System.Boolean Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger/AwakeMonitor::MoveNext()
extern void AwakeMonitor_MoveNext_m706CCFB8C097596366C88B9E1E6C43EB6A68E81D (void);
// 0x00000106 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger/<>c::.cctor()
extern void U3CU3Ec__cctor_m65B731720A82BBFE3D0846453EB0BC7CF2579047 (void);
// 0x00000107 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger/<>c::.ctor()
extern void U3CU3Ec__ctor_mD131D74A604773AB05426FE2A685B11E4F0F7BEE (void);
// 0x00000108 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDestroyTrigger/<>c::<OnDestroyAsync>b__7_0(System.Object)
extern void U3CU3Ec_U3COnDestroyAsyncU3Eb__7_0_m75541A8873DED6274A8AC671484A6C5D01F8E6B3 (void);
// 0x00000109 System.Void Cysharp.Threading.Tasks.Triggers.AsyncStartTrigger::Start()
extern void AsyncStartTrigger_Start_m214F3532BAE50DFF9E4C47037213291A0B6AF3E7 (void);
// 0x0000010A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncStartTrigger::StartAsync()
extern void AsyncStartTrigger_StartAsync_mD384D39AF89D7B2E64225EBA2960DD2D26C09738 (void);
// 0x0000010B System.Void Cysharp.Threading.Tasks.Triggers.AsyncStartTrigger::.ctor()
extern void AsyncStartTrigger__ctor_m37D4A0456EEE0B82F1C1801AB11A877D2B99EF8B (void);
// 0x0000010C System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::Awake()
// 0x0000010D System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::OnDestroy()
// 0x0000010E System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::AddHandler(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x0000010F System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::RemoveHandler(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x00000110 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::RaiseEvent(T)
// 0x00000111 Cysharp.Threading.Tasks.IUniTaskAsyncEnumerator`1<T> Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::GetAsyncEnumerator(System.Threading.CancellationToken)
// 0x00000112 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1::.ctor()
// 0x00000113 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::.ctor(Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1<T>,System.Threading.CancellationToken)
// 0x00000114 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::OnNext(T)
// 0x00000115 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::OnCompleted()
// 0x00000116 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::CancellationCallback(System.Object)
// 0x00000117 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::set_Current(T)
// 0x00000118 Cysharp.Threading.Tasks.ITriggerHandler`1<T> Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::Cysharp.Threading.Tasks.ITriggerHandler<T>.get_Prev()
// 0x00000119 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::Cysharp.Threading.Tasks.ITriggerHandler<T>.set_Prev(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x0000011A Cysharp.Threading.Tasks.ITriggerHandler`1<T> Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::Cysharp.Threading.Tasks.ITriggerHandler<T>.get_Next()
// 0x0000011B System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::Cysharp.Threading.Tasks.ITriggerHandler<T>.set_Next(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x0000011C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::DisposeAsync()
// 0x0000011D System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AsyncTriggerEnumerator::.cctor()
// 0x0000011E System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AwakeMonitor::.ctor(Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1<T>)
// 0x0000011F System.Boolean Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1/AwakeMonitor::MoveNext()
// 0x00000120 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOneShotTrigger::OneShotAsync()
// 0x00000121 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOneShotTrigger.OneShotAsync()
// 0x00000122 Cysharp.Threading.Tasks.ITriggerHandler`1<T> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.ITriggerHandler<T>.get_Prev()
// 0x00000123 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.ITriggerHandler<T>.set_Prev(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x00000124 Cysharp.Threading.Tasks.ITriggerHandler`1<T> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.ITriggerHandler<T>.get_Next()
// 0x00000125 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.ITriggerHandler<T>.set_Next(Cysharp.Threading.Tasks.ITriggerHandler`1<T>)
// 0x00000126 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::.ctor(Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1<T>,System.Boolean)
// 0x00000127 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::.ctor(Cysharp.Threading.Tasks.Triggers.AsyncTriggerBase`1<T>,System.Threading.CancellationToken,System.Boolean)
// 0x00000128 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::CancellationCallback(System.Object)
// 0x00000129 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Dispose()
// 0x0000012A T Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.IUniTaskSource<T>.GetResult(System.Int16)
// 0x0000012B System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.ITriggerHandler<T>.OnNext(T)
// 0x0000012C System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.ITriggerHandler<T>.OnCompleted()
// 0x0000012D System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.IUniTaskSource.GetResult(System.Int16)
// 0x0000012E Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.IUniTaskSource.GetStatus(System.Int16)
// 0x0000012F Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.IUniTaskSource.UnsafeGetStatus()
// 0x00000130 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.IUniTaskSource.OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x00000131 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncFixedUpdateHandler.FixedUpdateAsync()
// 0x00000132 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncLateUpdateHandler.LateUpdateAsync()
// 0x00000133 Cysharp.Threading.Tasks.UniTask`1<System.Int32> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorIKHandler.OnAnimatorIKAsync()
// 0x00000134 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorMoveHandler.OnAnimatorMoveAsync()
// 0x00000135 Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationFocusHandler.OnApplicationFocusAsync()
// 0x00000136 Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationPauseHandler.OnApplicationPauseAsync()
// 0x00000137 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationQuitHandler.OnApplicationQuitAsync()
// 0x00000138 Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<System.Single[],System.Int32>> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnAudioFilterReadHandler.OnAudioFilterReadAsync()
// 0x00000139 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameInvisibleHandler.OnBecameInvisibleAsync()
// 0x0000013A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameVisibleHandler.OnBecameVisibleAsync()
// 0x0000013B Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnBeforeTransformParentChangedHandler.OnBeforeTransformParentChangedAsync()
// 0x0000013C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCanvasGroupChangedHandler.OnCanvasGroupChangedAsync()
// 0x0000013D Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnterHandler.OnCollisionEnterAsync()
// 0x0000013E Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnter2DHandler.OnCollisionEnter2DAsync()
// 0x0000013F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExitHandler.OnCollisionExitAsync()
// 0x00000140 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExit2DHandler.OnCollisionExit2DAsync()
// 0x00000141 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStayHandler.OnCollisionStayAsync()
// 0x00000142 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStay2DHandler.OnCollisionStay2DAsync()
// 0x00000143 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ControllerColliderHit> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnControllerColliderHitHandler.OnControllerColliderHitAsync()
// 0x00000144 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnDisableHandler.OnDisableAsync()
// 0x00000145 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosHandler.OnDrawGizmosAsync()
// 0x00000146 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosSelectedHandler.OnDrawGizmosSelectedAsync()
// 0x00000147 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnEnableHandler.OnEnableAsync()
// 0x00000148 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnGUIHandler.OnGUIAsync()
// 0x00000149 Cysharp.Threading.Tasks.UniTask`1<System.Single> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreakHandler.OnJointBreakAsync()
// 0x0000014A Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Joint2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreak2DHandler.OnJointBreak2DAsync()
// 0x0000014B Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDownHandler.OnMouseDownAsync()
// 0x0000014C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDragHandler.OnMouseDragAsync()
// 0x0000014D Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseEnterHandler.OnMouseEnterAsync()
// 0x0000014E Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseExitHandler.OnMouseExitAsync()
// 0x0000014F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseOverHandler.OnMouseOverAsync()
// 0x00000150 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpHandler.OnMouseUpAsync()
// 0x00000151 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpAsButtonHandler.OnMouseUpAsButtonAsync()
// 0x00000152 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.GameObject> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleCollisionHandler.OnParticleCollisionAsync()
// 0x00000153 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleSystemStoppedHandler.OnParticleSystemStoppedAsync()
// 0x00000154 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleTriggerHandler.OnParticleTriggerAsync()
// 0x00000155 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ParticleSystemJobs.ParticleSystemJobData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleUpdateJobScheduledHandler.OnParticleUpdateJobScheduledAsync()
// 0x00000156 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPostRenderHandler.OnPostRenderAsync()
// 0x00000157 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPreCullHandler.OnPreCullAsync()
// 0x00000158 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPreRenderHandler.OnPreRenderAsync()
// 0x00000159 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformDimensionsChangeHandler.OnRectTransformDimensionsChangeAsync()
// 0x0000015A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformRemovedHandler.OnRectTransformRemovedAsync()
// 0x0000015B Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderImageHandler.OnRenderImageAsync()
// 0x0000015C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderObjectHandler.OnRenderObjectAsync()
// 0x0000015D Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnServerInitializedHandler.OnServerInitializedAsync()
// 0x0000015E Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformChildrenChangedHandler.OnTransformChildrenChangedAsync()
// 0x0000015F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformParentChangedHandler.OnTransformParentChangedAsync()
// 0x00000160 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnterHandler.OnTriggerEnterAsync()
// 0x00000161 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnter2DHandler.OnTriggerEnter2DAsync()
// 0x00000162 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExitHandler.OnTriggerExitAsync()
// 0x00000163 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExit2DHandler.OnTriggerExit2DAsync()
// 0x00000164 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStayHandler.OnTriggerStayAsync()
// 0x00000165 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStay2DHandler.OnTriggerStay2DAsync()
// 0x00000166 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnValidateHandler.OnValidateAsync()
// 0x00000167 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnWillRenderObjectHandler.OnWillRenderObjectAsync()
// 0x00000168 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncResetHandler.ResetAsync()
// 0x00000169 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncUpdateHandler.UpdateAsync()
// 0x0000016A Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnBeginDragHandler.OnBeginDragAsync()
// 0x0000016B Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnCancelHandler.OnCancelAsync()
// 0x0000016C Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnDeselectHandler.OnDeselectAsync()
// 0x0000016D Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnDragHandler.OnDragAsync()
// 0x0000016E Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnDropHandler.OnDropAsync()
// 0x0000016F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnEndDragHandler.OnEndDragAsync()
// 0x00000170 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnInitializePotentialDragHandler.OnInitializePotentialDragAsync()
// 0x00000171 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.AxisEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnMoveHandler.OnMoveAsync()
// 0x00000172 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerClickHandler.OnPointerClickAsync()
// 0x00000173 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerDownHandler.OnPointerDownAsync()
// 0x00000174 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerEnterHandler.OnPointerEnterAsync()
// 0x00000175 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerExitHandler.OnPointerExitAsync()
// 0x00000176 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerUpHandler.OnPointerUpAsync()
// 0x00000177 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnScrollHandler.OnScrollAsync()
// 0x00000178 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnSelectHandler.OnSelectAsync()
// 0x00000179 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnSubmitHandler.OnSubmitAsync()
// 0x0000017A Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::Cysharp.Threading.Tasks.Triggers.IAsyncOnUpdateSelectedHandler.OnUpdateSelectedAsync()
// 0x0000017B System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerHandler`1::.cctor()
// 0x0000017C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncFixedUpdateHandler::FixedUpdateAsync()
// 0x0000017D System.Void Cysharp.Threading.Tasks.Triggers.AsyncFixedUpdateTrigger::FixedUpdate()
extern void AsyncFixedUpdateTrigger_FixedUpdate_m0705C59DC91B487D841D1BFA37146F1A2516E0A1 (void);
// 0x0000017E Cysharp.Threading.Tasks.Triggers.IAsyncFixedUpdateHandler Cysharp.Threading.Tasks.Triggers.AsyncFixedUpdateTrigger::GetFixedUpdateAsyncHandler()
extern void AsyncFixedUpdateTrigger_GetFixedUpdateAsyncHandler_m86903CC33311A715FEF2BC4160AB89C8345A4BF4 (void);
// 0x0000017F Cysharp.Threading.Tasks.Triggers.IAsyncFixedUpdateHandler Cysharp.Threading.Tasks.Triggers.AsyncFixedUpdateTrigger::GetFixedUpdateAsyncHandler(System.Threading.CancellationToken)
extern void AsyncFixedUpdateTrigger_GetFixedUpdateAsyncHandler_m31D8686AD391D31AE4FF0BC638FE1A8057099B00 (void);
// 0x00000180 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncFixedUpdateTrigger::FixedUpdateAsync()
extern void AsyncFixedUpdateTrigger_FixedUpdateAsync_m6C4C01EF4279E013059F6B5AF954934D68CB2636 (void);
// 0x00000181 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncFixedUpdateTrigger::FixedUpdateAsync(System.Threading.CancellationToken)
extern void AsyncFixedUpdateTrigger_FixedUpdateAsync_m4E3A45C66F2A69C1D106A87DC493FEF17EA5AD98 (void);
// 0x00000182 System.Void Cysharp.Threading.Tasks.Triggers.AsyncFixedUpdateTrigger::.ctor()
extern void AsyncFixedUpdateTrigger__ctor_m33B852EDE6D2C4FF746BE68A19F2AEBAEC32E264 (void);
// 0x00000183 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncLateUpdateHandler::LateUpdateAsync()
// 0x00000184 System.Void Cysharp.Threading.Tasks.Triggers.AsyncLateUpdateTrigger::LateUpdate()
extern void AsyncLateUpdateTrigger_LateUpdate_m908E39EAC7BCBBD8407307ADA887EFC0C6F6C56A (void);
// 0x00000185 Cysharp.Threading.Tasks.Triggers.IAsyncLateUpdateHandler Cysharp.Threading.Tasks.Triggers.AsyncLateUpdateTrigger::GetLateUpdateAsyncHandler()
extern void AsyncLateUpdateTrigger_GetLateUpdateAsyncHandler_m2837CBD195E62CE172348EFD9FA6481B1EEAA93D (void);
// 0x00000186 Cysharp.Threading.Tasks.Triggers.IAsyncLateUpdateHandler Cysharp.Threading.Tasks.Triggers.AsyncLateUpdateTrigger::GetLateUpdateAsyncHandler(System.Threading.CancellationToken)
extern void AsyncLateUpdateTrigger_GetLateUpdateAsyncHandler_mE4912F3B20A7A67DDF305B0FB2E7D11EDE167D30 (void);
// 0x00000187 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncLateUpdateTrigger::LateUpdateAsync()
extern void AsyncLateUpdateTrigger_LateUpdateAsync_m896F3F380B142A12A9D0EEE3687771917AF121A3 (void);
// 0x00000188 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncLateUpdateTrigger::LateUpdateAsync(System.Threading.CancellationToken)
extern void AsyncLateUpdateTrigger_LateUpdateAsync_m2FCE0CF5654550D65A1AF2FB0A8FF75B4F326A89 (void);
// 0x00000189 System.Void Cysharp.Threading.Tasks.Triggers.AsyncLateUpdateTrigger::.ctor()
extern void AsyncLateUpdateTrigger__ctor_mDE0BD5556101ACC17A073C3BEAF2E953E9713219 (void);
// 0x0000018A Cysharp.Threading.Tasks.UniTask`1<System.Int32> Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorIKHandler::OnAnimatorIKAsync()
// 0x0000018B System.Void Cysharp.Threading.Tasks.Triggers.AsyncAnimatorIKTrigger::OnAnimatorIK(System.Int32)
extern void AsyncAnimatorIKTrigger_OnAnimatorIK_m51DFB984DEB2EC00B7AD0B1F927352E9E4CC4761 (void);
// 0x0000018C Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorIKHandler Cysharp.Threading.Tasks.Triggers.AsyncAnimatorIKTrigger::GetOnAnimatorIKAsyncHandler()
extern void AsyncAnimatorIKTrigger_GetOnAnimatorIKAsyncHandler_mBE26647CBFC257096638182A6B2A7C50901DEA30 (void);
// 0x0000018D Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorIKHandler Cysharp.Threading.Tasks.Triggers.AsyncAnimatorIKTrigger::GetOnAnimatorIKAsyncHandler(System.Threading.CancellationToken)
extern void AsyncAnimatorIKTrigger_GetOnAnimatorIKAsyncHandler_mD305BF5EA8A99541D1D794813C3495BAC6E49491 (void);
// 0x0000018E Cysharp.Threading.Tasks.UniTask`1<System.Int32> Cysharp.Threading.Tasks.Triggers.AsyncAnimatorIKTrigger::OnAnimatorIKAsync()
extern void AsyncAnimatorIKTrigger_OnAnimatorIKAsync_m801F08708AB20F736C6E0EDDA1A0735D16E2CDF4 (void);
// 0x0000018F Cysharp.Threading.Tasks.UniTask`1<System.Int32> Cysharp.Threading.Tasks.Triggers.AsyncAnimatorIKTrigger::OnAnimatorIKAsync(System.Threading.CancellationToken)
extern void AsyncAnimatorIKTrigger_OnAnimatorIKAsync_mA0385326ED58FFB084A5EB221BCFAC41EB465FE1 (void);
// 0x00000190 System.Void Cysharp.Threading.Tasks.Triggers.AsyncAnimatorIKTrigger::.ctor()
extern void AsyncAnimatorIKTrigger__ctor_m2887CB5E5E1D6D132117F83E2638A432E86BC85A (void);
// 0x00000191 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorMoveHandler::OnAnimatorMoveAsync()
// 0x00000192 System.Void Cysharp.Threading.Tasks.Triggers.AsyncAnimatorMoveTrigger::OnAnimatorMove()
extern void AsyncAnimatorMoveTrigger_OnAnimatorMove_mE63E052A1436122456502A2F96AC1BE0EBB259D8 (void);
// 0x00000193 Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorMoveHandler Cysharp.Threading.Tasks.Triggers.AsyncAnimatorMoveTrigger::GetOnAnimatorMoveAsyncHandler()
extern void AsyncAnimatorMoveTrigger_GetOnAnimatorMoveAsyncHandler_m65DEA11547D0D0A713764F0FCEFA130224AD7C52 (void);
// 0x00000194 Cysharp.Threading.Tasks.Triggers.IAsyncOnAnimatorMoveHandler Cysharp.Threading.Tasks.Triggers.AsyncAnimatorMoveTrigger::GetOnAnimatorMoveAsyncHandler(System.Threading.CancellationToken)
extern void AsyncAnimatorMoveTrigger_GetOnAnimatorMoveAsyncHandler_m68CB140AF1475976C256A589EF7671A0A567936A (void);
// 0x00000195 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncAnimatorMoveTrigger::OnAnimatorMoveAsync()
extern void AsyncAnimatorMoveTrigger_OnAnimatorMoveAsync_mF297FE216FF3121EE6C6A09E262979D423FD8D95 (void);
// 0x00000196 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncAnimatorMoveTrigger::OnAnimatorMoveAsync(System.Threading.CancellationToken)
extern void AsyncAnimatorMoveTrigger_OnAnimatorMoveAsync_mAC0EA3D08AB9C02C3AFDC19CA969B4E09F4F3E28 (void);
// 0x00000197 System.Void Cysharp.Threading.Tasks.Triggers.AsyncAnimatorMoveTrigger::.ctor()
extern void AsyncAnimatorMoveTrigger__ctor_mB5613243B04C56F343F620976FA2B75A008D908E (void);
// 0x00000198 Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationFocusHandler::OnApplicationFocusAsync()
// 0x00000199 System.Void Cysharp.Threading.Tasks.Triggers.AsyncApplicationFocusTrigger::OnApplicationFocus(System.Boolean)
extern void AsyncApplicationFocusTrigger_OnApplicationFocus_m0BAE68C2CA6849051E404C64312D1B12ACECEF6F (void);
// 0x0000019A Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationFocusHandler Cysharp.Threading.Tasks.Triggers.AsyncApplicationFocusTrigger::GetOnApplicationFocusAsyncHandler()
extern void AsyncApplicationFocusTrigger_GetOnApplicationFocusAsyncHandler_m31006D6BE29FD6659620E9E9ADE09B913B1611F8 (void);
// 0x0000019B Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationFocusHandler Cysharp.Threading.Tasks.Triggers.AsyncApplicationFocusTrigger::GetOnApplicationFocusAsyncHandler(System.Threading.CancellationToken)
extern void AsyncApplicationFocusTrigger_GetOnApplicationFocusAsyncHandler_m302C2618C792F1248A0EE540318D105D5D9CE120 (void);
// 0x0000019C Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.AsyncApplicationFocusTrigger::OnApplicationFocusAsync()
extern void AsyncApplicationFocusTrigger_OnApplicationFocusAsync_m95456513A6FE134DFB845612ABB34BAFF3931E1E (void);
// 0x0000019D Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.AsyncApplicationFocusTrigger::OnApplicationFocusAsync(System.Threading.CancellationToken)
extern void AsyncApplicationFocusTrigger_OnApplicationFocusAsync_m52AAA8B9AD8DFEDC4DD2AAF35CC4F36E082A880B (void);
// 0x0000019E System.Void Cysharp.Threading.Tasks.Triggers.AsyncApplicationFocusTrigger::.ctor()
extern void AsyncApplicationFocusTrigger__ctor_mCFDCB852BDB7435E375F9B33FA71FC15D5C06A9B (void);
// 0x0000019F Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationPauseHandler::OnApplicationPauseAsync()
// 0x000001A0 System.Void Cysharp.Threading.Tasks.Triggers.AsyncApplicationPauseTrigger::OnApplicationPause(System.Boolean)
extern void AsyncApplicationPauseTrigger_OnApplicationPause_m9FDD51239C80B79018288E68089CFDC9563062C1 (void);
// 0x000001A1 Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationPauseHandler Cysharp.Threading.Tasks.Triggers.AsyncApplicationPauseTrigger::GetOnApplicationPauseAsyncHandler()
extern void AsyncApplicationPauseTrigger_GetOnApplicationPauseAsyncHandler_m20387C844A34739BAADBAC6A94FE5AB522F548E4 (void);
// 0x000001A2 Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationPauseHandler Cysharp.Threading.Tasks.Triggers.AsyncApplicationPauseTrigger::GetOnApplicationPauseAsyncHandler(System.Threading.CancellationToken)
extern void AsyncApplicationPauseTrigger_GetOnApplicationPauseAsyncHandler_m7CCE64C34179EA13B3252CCE3F770AD2E23BF9AB (void);
// 0x000001A3 Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.AsyncApplicationPauseTrigger::OnApplicationPauseAsync()
extern void AsyncApplicationPauseTrigger_OnApplicationPauseAsync_m575250F184B2F1D7E2CF9F1D33B9030E06E24AEC (void);
// 0x000001A4 Cysharp.Threading.Tasks.UniTask`1<System.Boolean> Cysharp.Threading.Tasks.Triggers.AsyncApplicationPauseTrigger::OnApplicationPauseAsync(System.Threading.CancellationToken)
extern void AsyncApplicationPauseTrigger_OnApplicationPauseAsync_m24FFD7A00792AEA11A898E9CEA45D8D657D59EC9 (void);
// 0x000001A5 System.Void Cysharp.Threading.Tasks.Triggers.AsyncApplicationPauseTrigger::.ctor()
extern void AsyncApplicationPauseTrigger__ctor_m6960B692443D76CDE3A416F0BDD2003F3421A52F (void);
// 0x000001A6 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationQuitHandler::OnApplicationQuitAsync()
// 0x000001A7 System.Void Cysharp.Threading.Tasks.Triggers.AsyncApplicationQuitTrigger::OnApplicationQuit()
extern void AsyncApplicationQuitTrigger_OnApplicationQuit_m9306723AA7B2D8E88BAE359035F4134D594C63B6 (void);
// 0x000001A8 Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationQuitHandler Cysharp.Threading.Tasks.Triggers.AsyncApplicationQuitTrigger::GetOnApplicationQuitAsyncHandler()
extern void AsyncApplicationQuitTrigger_GetOnApplicationQuitAsyncHandler_mD8104FEF0CE5D02D1464DA6C57C71BD599CB265D (void);
// 0x000001A9 Cysharp.Threading.Tasks.Triggers.IAsyncOnApplicationQuitHandler Cysharp.Threading.Tasks.Triggers.AsyncApplicationQuitTrigger::GetOnApplicationQuitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncApplicationQuitTrigger_GetOnApplicationQuitAsyncHandler_mE34932D9EAE9999EB21B4DF29C8959CBDBFA6564 (void);
// 0x000001AA Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncApplicationQuitTrigger::OnApplicationQuitAsync()
extern void AsyncApplicationQuitTrigger_OnApplicationQuitAsync_m2DA58255F9D7942C2B942A60C88CCE9F12D75B0A (void);
// 0x000001AB Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncApplicationQuitTrigger::OnApplicationQuitAsync(System.Threading.CancellationToken)
extern void AsyncApplicationQuitTrigger_OnApplicationQuitAsync_mAC8895312602D2DFCC54CABC0EC5CB47CC8B2ED6 (void);
// 0x000001AC System.Void Cysharp.Threading.Tasks.Triggers.AsyncApplicationQuitTrigger::.ctor()
extern void AsyncApplicationQuitTrigger__ctor_mA6072BDB37D30675FC3A09770DA4A0BA8CE379CF (void);
// 0x000001AD Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<System.Single[],System.Int32>> Cysharp.Threading.Tasks.Triggers.IAsyncOnAudioFilterReadHandler::OnAudioFilterReadAsync()
// 0x000001AE System.Void Cysharp.Threading.Tasks.Triggers.AsyncAudioFilterReadTrigger::OnAudioFilterRead(System.Single[],System.Int32)
extern void AsyncAudioFilterReadTrigger_OnAudioFilterRead_m2D02FE76FB6E6DD2B2D883E0296ECED6F333E9FA (void);
// 0x000001AF Cysharp.Threading.Tasks.Triggers.IAsyncOnAudioFilterReadHandler Cysharp.Threading.Tasks.Triggers.AsyncAudioFilterReadTrigger::GetOnAudioFilterReadAsyncHandler()
extern void AsyncAudioFilterReadTrigger_GetOnAudioFilterReadAsyncHandler_m1697C83E0C00122713821C6C7B5A65EF80ADF8B9 (void);
// 0x000001B0 Cysharp.Threading.Tasks.Triggers.IAsyncOnAudioFilterReadHandler Cysharp.Threading.Tasks.Triggers.AsyncAudioFilterReadTrigger::GetOnAudioFilterReadAsyncHandler(System.Threading.CancellationToken)
extern void AsyncAudioFilterReadTrigger_GetOnAudioFilterReadAsyncHandler_mC1F1EE4BA08E62D3E20C334E289897F9584AC2EE (void);
// 0x000001B1 Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<System.Single[],System.Int32>> Cysharp.Threading.Tasks.Triggers.AsyncAudioFilterReadTrigger::OnAudioFilterReadAsync()
extern void AsyncAudioFilterReadTrigger_OnAudioFilterReadAsync_m384CAF4EEADA125FAE9AEA676BECF3BE384AE064 (void);
// 0x000001B2 Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<System.Single[],System.Int32>> Cysharp.Threading.Tasks.Triggers.AsyncAudioFilterReadTrigger::OnAudioFilterReadAsync(System.Threading.CancellationToken)
extern void AsyncAudioFilterReadTrigger_OnAudioFilterReadAsync_mF51A00CAA04517FE68E300879120C56E539F4215 (void);
// 0x000001B3 System.Void Cysharp.Threading.Tasks.Triggers.AsyncAudioFilterReadTrigger::.ctor()
extern void AsyncAudioFilterReadTrigger__ctor_mD290A53C2964CEA8E831951D54072B81C85D9069 (void);
// 0x000001B4 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameInvisibleHandler::OnBecameInvisibleAsync()
// 0x000001B5 System.Void Cysharp.Threading.Tasks.Triggers.AsyncBecameInvisibleTrigger::OnBecameInvisible()
extern void AsyncBecameInvisibleTrigger_OnBecameInvisible_m312E75A42367D87C26CB90CAF1CBA2B8620C7A93 (void);
// 0x000001B6 Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameInvisibleHandler Cysharp.Threading.Tasks.Triggers.AsyncBecameInvisibleTrigger::GetOnBecameInvisibleAsyncHandler()
extern void AsyncBecameInvisibleTrigger_GetOnBecameInvisibleAsyncHandler_mDBB9245CF1D12A9BA4A59209F976AB79832F5221 (void);
// 0x000001B7 Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameInvisibleHandler Cysharp.Threading.Tasks.Triggers.AsyncBecameInvisibleTrigger::GetOnBecameInvisibleAsyncHandler(System.Threading.CancellationToken)
extern void AsyncBecameInvisibleTrigger_GetOnBecameInvisibleAsyncHandler_mEB1592F42E403767884F3448FDFA548E51B3CCFA (void);
// 0x000001B8 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncBecameInvisibleTrigger::OnBecameInvisibleAsync()
extern void AsyncBecameInvisibleTrigger_OnBecameInvisibleAsync_m2143CBBB462806055C3108802F2E10CA22EB5E99 (void);
// 0x000001B9 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncBecameInvisibleTrigger::OnBecameInvisibleAsync(System.Threading.CancellationToken)
extern void AsyncBecameInvisibleTrigger_OnBecameInvisibleAsync_m04523D76A8779885CCD25F6D511269E8078CF0EE (void);
// 0x000001BA System.Void Cysharp.Threading.Tasks.Triggers.AsyncBecameInvisibleTrigger::.ctor()
extern void AsyncBecameInvisibleTrigger__ctor_mBE1BE4360FCD4675F22ED25BE442C6A04A3344F8 (void);
// 0x000001BB Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameVisibleHandler::OnBecameVisibleAsync()
// 0x000001BC System.Void Cysharp.Threading.Tasks.Triggers.AsyncBecameVisibleTrigger::OnBecameVisible()
extern void AsyncBecameVisibleTrigger_OnBecameVisible_m4A68CD721D490D0D864EE50462E9431F52D4370D (void);
// 0x000001BD Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameVisibleHandler Cysharp.Threading.Tasks.Triggers.AsyncBecameVisibleTrigger::GetOnBecameVisibleAsyncHandler()
extern void AsyncBecameVisibleTrigger_GetOnBecameVisibleAsyncHandler_m36014D2B5B069CF5CDDD5B6F23DE94BF6039A4F2 (void);
// 0x000001BE Cysharp.Threading.Tasks.Triggers.IAsyncOnBecameVisibleHandler Cysharp.Threading.Tasks.Triggers.AsyncBecameVisibleTrigger::GetOnBecameVisibleAsyncHandler(System.Threading.CancellationToken)
extern void AsyncBecameVisibleTrigger_GetOnBecameVisibleAsyncHandler_m96A7E4047E84F0DFB1DF8DF5EE36AAB8D4FE1B99 (void);
// 0x000001BF Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncBecameVisibleTrigger::OnBecameVisibleAsync()
extern void AsyncBecameVisibleTrigger_OnBecameVisibleAsync_m819B162C99891D4A126D02824BAE195D356B32E7 (void);
// 0x000001C0 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncBecameVisibleTrigger::OnBecameVisibleAsync(System.Threading.CancellationToken)
extern void AsyncBecameVisibleTrigger_OnBecameVisibleAsync_m0F62FB59355E30022A4AC5093A7B617793F45641 (void);
// 0x000001C1 System.Void Cysharp.Threading.Tasks.Triggers.AsyncBecameVisibleTrigger::.ctor()
extern void AsyncBecameVisibleTrigger__ctor_mF96F618AFEE8652778311FADDE31E57EEB1FEEA8 (void);
// 0x000001C2 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnBeforeTransformParentChangedHandler::OnBeforeTransformParentChangedAsync()
// 0x000001C3 System.Void Cysharp.Threading.Tasks.Triggers.AsyncBeforeTransformParentChangedTrigger::OnBeforeTransformParentChanged()
extern void AsyncBeforeTransformParentChangedTrigger_OnBeforeTransformParentChanged_m2033F9FC289E325B7DD121807132A0A1515F8D82 (void);
// 0x000001C4 Cysharp.Threading.Tasks.Triggers.IAsyncOnBeforeTransformParentChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncBeforeTransformParentChangedTrigger::GetOnBeforeTransformParentChangedAsyncHandler()
extern void AsyncBeforeTransformParentChangedTrigger_GetOnBeforeTransformParentChangedAsyncHandler_m8DBF9763FAF3EB8BB7D4E2F135E1D882F1D474F5 (void);
// 0x000001C5 Cysharp.Threading.Tasks.Triggers.IAsyncOnBeforeTransformParentChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncBeforeTransformParentChangedTrigger::GetOnBeforeTransformParentChangedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncBeforeTransformParentChangedTrigger_GetOnBeforeTransformParentChangedAsyncHandler_mB2AFF1867B9F0945D9C48CDB15A42717BCF2256C (void);
// 0x000001C6 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncBeforeTransformParentChangedTrigger::OnBeforeTransformParentChangedAsync()
extern void AsyncBeforeTransformParentChangedTrigger_OnBeforeTransformParentChangedAsync_m826500CA7C16A438D8A0FE79B6CFB19E4D031A57 (void);
// 0x000001C7 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncBeforeTransformParentChangedTrigger::OnBeforeTransformParentChangedAsync(System.Threading.CancellationToken)
extern void AsyncBeforeTransformParentChangedTrigger_OnBeforeTransformParentChangedAsync_mA7CFF5A5F194497A675AC3DC1A80316CCD206208 (void);
// 0x000001C8 System.Void Cysharp.Threading.Tasks.Triggers.AsyncBeforeTransformParentChangedTrigger::.ctor()
extern void AsyncBeforeTransformParentChangedTrigger__ctor_m7AA6FBD1056C6ECA50F8F1AF738945C3AB76261D (void);
// 0x000001C9 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnCanvasGroupChangedHandler::OnCanvasGroupChangedAsync()
// 0x000001CA System.Void Cysharp.Threading.Tasks.Triggers.AsyncOnCanvasGroupChangedTrigger::OnCanvasGroupChanged()
extern void AsyncOnCanvasGroupChangedTrigger_OnCanvasGroupChanged_m69D6880DA202080B0819B454F1434A53F932212C (void);
// 0x000001CB Cysharp.Threading.Tasks.Triggers.IAsyncOnCanvasGroupChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncOnCanvasGroupChangedTrigger::GetOnCanvasGroupChangedAsyncHandler()
extern void AsyncOnCanvasGroupChangedTrigger_GetOnCanvasGroupChangedAsyncHandler_mD3B7A0C3E999EB90C8CBFE428A8460D722AA4B44 (void);
// 0x000001CC Cysharp.Threading.Tasks.Triggers.IAsyncOnCanvasGroupChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncOnCanvasGroupChangedTrigger::GetOnCanvasGroupChangedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncOnCanvasGroupChangedTrigger_GetOnCanvasGroupChangedAsyncHandler_m9CA1095290F422C953B6DEFB71D91E7D8E1CAD37 (void);
// 0x000001CD Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncOnCanvasGroupChangedTrigger::OnCanvasGroupChangedAsync()
extern void AsyncOnCanvasGroupChangedTrigger_OnCanvasGroupChangedAsync_m722D89D498D9DCEAD853042DEE0E0D945956B3B8 (void);
// 0x000001CE Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncOnCanvasGroupChangedTrigger::OnCanvasGroupChangedAsync(System.Threading.CancellationToken)
extern void AsyncOnCanvasGroupChangedTrigger_OnCanvasGroupChangedAsync_mC2EB8B229349122F9CA04C2EB1BE68418CD762A3 (void);
// 0x000001CF System.Void Cysharp.Threading.Tasks.Triggers.AsyncOnCanvasGroupChangedTrigger::.ctor()
extern void AsyncOnCanvasGroupChangedTrigger__ctor_mC889A6EC2ED66CFF7072860A84035FDCFCED0D0B (void);
// 0x000001D0 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnterHandler::OnCollisionEnterAsync()
// 0x000001D1 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnterTrigger::OnCollisionEnter(UnityEngine.Collision)
extern void AsyncCollisionEnterTrigger_OnCollisionEnter_m576EF6AAD95BE3EAA9C9A0E1EECEF23A545112D3 (void);
// 0x000001D2 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnterTrigger::GetOnCollisionEnterAsyncHandler()
extern void AsyncCollisionEnterTrigger_GetOnCollisionEnterAsyncHandler_m589CA4EE529DEF4CC63888EA2D2822070C932A30 (void);
// 0x000001D3 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnterTrigger::GetOnCollisionEnterAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCollisionEnterTrigger_GetOnCollisionEnterAsyncHandler_m8DC554A919AA240FF95298B6A8E747A6B9F67337 (void);
// 0x000001D4 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnterTrigger::OnCollisionEnterAsync()
extern void AsyncCollisionEnterTrigger_OnCollisionEnterAsync_mBE068228882596B9D4B43BDEC7111584C637B963 (void);
// 0x000001D5 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnterTrigger::OnCollisionEnterAsync(System.Threading.CancellationToken)
extern void AsyncCollisionEnterTrigger_OnCollisionEnterAsync_m2439507C5A45010683A37BA756279414D397D09B (void);
// 0x000001D6 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnterTrigger::.ctor()
extern void AsyncCollisionEnterTrigger__ctor_mDB403BFED7F79FE27D9E6F3C0D8261365D2A3934 (void);
// 0x000001D7 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnter2DHandler::OnCollisionEnter2DAsync()
// 0x000001D8 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnter2DTrigger::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void AsyncCollisionEnter2DTrigger_OnCollisionEnter2D_mB56E55C4D44B6D280750B6C964F0A7BC0325F454 (void);
// 0x000001D9 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnter2DHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnter2DTrigger::GetOnCollisionEnter2DAsyncHandler()
extern void AsyncCollisionEnter2DTrigger_GetOnCollisionEnter2DAsyncHandler_m46E608BE38F043FF4283EB79E3B02C77BE786B55 (void);
// 0x000001DA Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionEnter2DHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnter2DTrigger::GetOnCollisionEnter2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCollisionEnter2DTrigger_GetOnCollisionEnter2DAsyncHandler_mB51C69915CFE4DB98C61ED4F04645E150562A062 (void);
// 0x000001DB Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnter2DTrigger::OnCollisionEnter2DAsync()
extern void AsyncCollisionEnter2DTrigger_OnCollisionEnter2DAsync_mBE48732F5AA51052EB12744D55F0A4C12544E018 (void);
// 0x000001DC Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnter2DTrigger::OnCollisionEnter2DAsync(System.Threading.CancellationToken)
extern void AsyncCollisionEnter2DTrigger_OnCollisionEnter2DAsync_mA437C6E73A616D8C90E66E1DE5995F8B332136FF (void);
// 0x000001DD System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionEnter2DTrigger::.ctor()
extern void AsyncCollisionEnter2DTrigger__ctor_m5EB716A5E55E52A0B9C9368BCF499C521A78B951 (void);
// 0x000001DE Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExitHandler::OnCollisionExitAsync()
// 0x000001DF System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionExitTrigger::OnCollisionExit(UnityEngine.Collision)
extern void AsyncCollisionExitTrigger_OnCollisionExit_m83F351BC70B9E59EB7322EDF57786BADD36D7D99 (void);
// 0x000001E0 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExitHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionExitTrigger::GetOnCollisionExitAsyncHandler()
extern void AsyncCollisionExitTrigger_GetOnCollisionExitAsyncHandler_mC1B1B8B17C2564FA8C5FE42F5F83084118C7389E (void);
// 0x000001E1 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExitHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionExitTrigger::GetOnCollisionExitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCollisionExitTrigger_GetOnCollisionExitAsyncHandler_mFF5DFA6D9C6A4407398CC847E578FD5530CCE5B4 (void);
// 0x000001E2 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncCollisionExitTrigger::OnCollisionExitAsync()
extern void AsyncCollisionExitTrigger_OnCollisionExitAsync_mCE741AE8D58C54C7571777825AEFA49C26E4E802 (void);
// 0x000001E3 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncCollisionExitTrigger::OnCollisionExitAsync(System.Threading.CancellationToken)
extern void AsyncCollisionExitTrigger_OnCollisionExitAsync_mE8B940E802B1093CFC376886852B71CF66B62440 (void);
// 0x000001E4 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionExitTrigger::.ctor()
extern void AsyncCollisionExitTrigger__ctor_m0D9E28D141C507C5EF9494271FCE28DCF3071161 (void);
// 0x000001E5 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExit2DHandler::OnCollisionExit2DAsync()
// 0x000001E6 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionExit2DTrigger::OnCollisionExit2D(UnityEngine.Collision2D)
extern void AsyncCollisionExit2DTrigger_OnCollisionExit2D_mB66ECCE2C6934F649F173A3A30C4FD8ADFF2BA92 (void);
// 0x000001E7 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExit2DHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionExit2DTrigger::GetOnCollisionExit2DAsyncHandler()
extern void AsyncCollisionExit2DTrigger_GetOnCollisionExit2DAsyncHandler_m59B0316F3273A9720B06F9CB7D7140A336411BCB (void);
// 0x000001E8 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionExit2DHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionExit2DTrigger::GetOnCollisionExit2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCollisionExit2DTrigger_GetOnCollisionExit2DAsyncHandler_m770E00F8020C4534C69CEC24E6E9845881B9ADEF (void);
// 0x000001E9 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncCollisionExit2DTrigger::OnCollisionExit2DAsync()
extern void AsyncCollisionExit2DTrigger_OnCollisionExit2DAsync_m81AAECFA0BC02A5941688D2E66D1D30ACF37B59E (void);
// 0x000001EA Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncCollisionExit2DTrigger::OnCollisionExit2DAsync(System.Threading.CancellationToken)
extern void AsyncCollisionExit2DTrigger_OnCollisionExit2DAsync_m15A0A6E0E576DB62029B2DAA0C753492FD7DA2BB (void);
// 0x000001EB System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionExit2DTrigger::.ctor()
extern void AsyncCollisionExit2DTrigger__ctor_m72A6DA79B12E512A9B10B6AA0F728D7BF6A4AF21 (void);
// 0x000001EC Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStayHandler::OnCollisionStayAsync()
// 0x000001ED System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionStayTrigger::OnCollisionStay(UnityEngine.Collision)
extern void AsyncCollisionStayTrigger_OnCollisionStay_mC235D9FDD5705CBE724FD6119BAC3B6150CC37AE (void);
// 0x000001EE Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStayHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionStayTrigger::GetOnCollisionStayAsyncHandler()
extern void AsyncCollisionStayTrigger_GetOnCollisionStayAsyncHandler_m283A9CCD50E5A89EA8521A618C960E8AEB44EE7C (void);
// 0x000001EF Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStayHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionStayTrigger::GetOnCollisionStayAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCollisionStayTrigger_GetOnCollisionStayAsyncHandler_mE5A7E194DC2FAAD29E5864EDADC831B856C5D9B1 (void);
// 0x000001F0 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncCollisionStayTrigger::OnCollisionStayAsync()
extern void AsyncCollisionStayTrigger_OnCollisionStayAsync_m84B8B23EDAA2C12CEFB89140E0B54084BC38BFCB (void);
// 0x000001F1 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision> Cysharp.Threading.Tasks.Triggers.AsyncCollisionStayTrigger::OnCollisionStayAsync(System.Threading.CancellationToken)
extern void AsyncCollisionStayTrigger_OnCollisionStayAsync_m0F925EE0CB4D2F8157334D56F9B05D5BBB3508C5 (void);
// 0x000001F2 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionStayTrigger::.ctor()
extern void AsyncCollisionStayTrigger__ctor_m34C682CFAD0F67A34A57DF60894ADE131F4856DA (void);
// 0x000001F3 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStay2DHandler::OnCollisionStay2DAsync()
// 0x000001F4 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionStay2DTrigger::OnCollisionStay2D(UnityEngine.Collision2D)
extern void AsyncCollisionStay2DTrigger_OnCollisionStay2D_mF8AEEFBB0CEC1348AFC6B79134DF01AD92BA2768 (void);
// 0x000001F5 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStay2DHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionStay2DTrigger::GetOnCollisionStay2DAsyncHandler()
extern void AsyncCollisionStay2DTrigger_GetOnCollisionStay2DAsyncHandler_m44B411FCCC0A476FBC95ED91C234A097CFE7C2AF (void);
// 0x000001F6 Cysharp.Threading.Tasks.Triggers.IAsyncOnCollisionStay2DHandler Cysharp.Threading.Tasks.Triggers.AsyncCollisionStay2DTrigger::GetOnCollisionStay2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCollisionStay2DTrigger_GetOnCollisionStay2DAsyncHandler_m1E98372172C63DBF103C6EED5B23FEDBA67DB194 (void);
// 0x000001F7 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncCollisionStay2DTrigger::OnCollisionStay2DAsync()
extern void AsyncCollisionStay2DTrigger_OnCollisionStay2DAsync_m454925D5989FBE757CC4F20D6B5E90AF17324129 (void);
// 0x000001F8 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collision2D> Cysharp.Threading.Tasks.Triggers.AsyncCollisionStay2DTrigger::OnCollisionStay2DAsync(System.Threading.CancellationToken)
extern void AsyncCollisionStay2DTrigger_OnCollisionStay2DAsync_mE6C36D3CC1673AE39A334C14F57CBDD3D995C44A (void);
// 0x000001F9 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCollisionStay2DTrigger::.ctor()
extern void AsyncCollisionStay2DTrigger__ctor_m7C30628923FB0E8C50DA3F4F38B82CB29BE3AD44 (void);
// 0x000001FA Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ControllerColliderHit> Cysharp.Threading.Tasks.Triggers.IAsyncOnControllerColliderHitHandler::OnControllerColliderHitAsync()
// 0x000001FB System.Void Cysharp.Threading.Tasks.Triggers.AsyncControllerColliderHitTrigger::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern void AsyncControllerColliderHitTrigger_OnControllerColliderHit_mAD227189F948D3E8E842944ACBFFD0888F1833E8 (void);
// 0x000001FC Cysharp.Threading.Tasks.Triggers.IAsyncOnControllerColliderHitHandler Cysharp.Threading.Tasks.Triggers.AsyncControllerColliderHitTrigger::GetOnControllerColliderHitAsyncHandler()
extern void AsyncControllerColliderHitTrigger_GetOnControllerColliderHitAsyncHandler_mC363F5C1D218419CBEA0C74DF453A72B01BC75FB (void);
// 0x000001FD Cysharp.Threading.Tasks.Triggers.IAsyncOnControllerColliderHitHandler Cysharp.Threading.Tasks.Triggers.AsyncControllerColliderHitTrigger::GetOnControllerColliderHitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncControllerColliderHitTrigger_GetOnControllerColliderHitAsyncHandler_m9DA2FBBC436C2D485A5837A344C868B2CE27D2EB (void);
// 0x000001FE Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ControllerColliderHit> Cysharp.Threading.Tasks.Triggers.AsyncControllerColliderHitTrigger::OnControllerColliderHitAsync()
extern void AsyncControllerColliderHitTrigger_OnControllerColliderHitAsync_m0E3347BABC50889B710B480D0693D7A53BD10C1A (void);
// 0x000001FF Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ControllerColliderHit> Cysharp.Threading.Tasks.Triggers.AsyncControllerColliderHitTrigger::OnControllerColliderHitAsync(System.Threading.CancellationToken)
extern void AsyncControllerColliderHitTrigger_OnControllerColliderHitAsync_mBC4C181546FE07D1FF3BA4BFCF9E210E61492FC2 (void);
// 0x00000200 System.Void Cysharp.Threading.Tasks.Triggers.AsyncControllerColliderHitTrigger::.ctor()
extern void AsyncControllerColliderHitTrigger__ctor_mD603E3F88B2D886F4B2EE4771C7961710B206BBB (void);
// 0x00000201 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnDisableHandler::OnDisableAsync()
// 0x00000202 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDisableTrigger::OnDisable()
extern void AsyncDisableTrigger_OnDisable_m27629A1A375B608AF0629315800517E7AED7F015 (void);
// 0x00000203 Cysharp.Threading.Tasks.Triggers.IAsyncOnDisableHandler Cysharp.Threading.Tasks.Triggers.AsyncDisableTrigger::GetOnDisableAsyncHandler()
extern void AsyncDisableTrigger_GetOnDisableAsyncHandler_m823BDBE58A1BC67F62D58A162C9795E2E73B724B (void);
// 0x00000204 Cysharp.Threading.Tasks.Triggers.IAsyncOnDisableHandler Cysharp.Threading.Tasks.Triggers.AsyncDisableTrigger::GetOnDisableAsyncHandler(System.Threading.CancellationToken)
extern void AsyncDisableTrigger_GetOnDisableAsyncHandler_m3D99632E3EC7B57B04F5217B46988BA4D61663DB (void);
// 0x00000205 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDisableTrigger::OnDisableAsync()
extern void AsyncDisableTrigger_OnDisableAsync_m39073507C2EC519C7CF1D55A0508D196889093C9 (void);
// 0x00000206 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDisableTrigger::OnDisableAsync(System.Threading.CancellationToken)
extern void AsyncDisableTrigger_OnDisableAsync_mC44ABD0117D19B65AF379FF1E0E6EDFD2D7561DD (void);
// 0x00000207 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDisableTrigger::.ctor()
extern void AsyncDisableTrigger__ctor_m873E78F2F96384352B79F5306757371CE6D6B1B0 (void);
// 0x00000208 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosHandler::OnDrawGizmosAsync()
// 0x00000209 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosTrigger::OnDrawGizmos()
extern void AsyncDrawGizmosTrigger_OnDrawGizmos_m95496C678B63AC47208033BC56C5440BC03CE692 (void);
// 0x0000020A Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosHandler Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosTrigger::GetOnDrawGizmosAsyncHandler()
extern void AsyncDrawGizmosTrigger_GetOnDrawGizmosAsyncHandler_m2AAE59F99CA9C0F991FCE13B362E2CFF59F3CF47 (void);
// 0x0000020B Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosHandler Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosTrigger::GetOnDrawGizmosAsyncHandler(System.Threading.CancellationToken)
extern void AsyncDrawGizmosTrigger_GetOnDrawGizmosAsyncHandler_m15240D56F23E0D8F06414C40EC28F6D58AEE5F2E (void);
// 0x0000020C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosTrigger::OnDrawGizmosAsync()
extern void AsyncDrawGizmosTrigger_OnDrawGizmosAsync_mAF42801C06E0525928B98F35189D4BD61DAB6BD8 (void);
// 0x0000020D Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosTrigger::OnDrawGizmosAsync(System.Threading.CancellationToken)
extern void AsyncDrawGizmosTrigger_OnDrawGizmosAsync_m5CF692CF3A528747E86265D019F0394828EE5F1D (void);
// 0x0000020E System.Void Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosTrigger::.ctor()
extern void AsyncDrawGizmosTrigger__ctor_m65E65F6170F606DFC6635AE428A7503EFA08EC93 (void);
// 0x0000020F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosSelectedHandler::OnDrawGizmosSelectedAsync()
// 0x00000210 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosSelectedTrigger::OnDrawGizmosSelected()
extern void AsyncDrawGizmosSelectedTrigger_OnDrawGizmosSelected_m0722FBFD1A3FC778E4D60B5030EF79AC9E4DF93C (void);
// 0x00000211 Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosSelectedHandler Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosSelectedTrigger::GetOnDrawGizmosSelectedAsyncHandler()
extern void AsyncDrawGizmosSelectedTrigger_GetOnDrawGizmosSelectedAsyncHandler_mCD428D02F0FB0DE82214E274C29DBF57AC62FD50 (void);
// 0x00000212 Cysharp.Threading.Tasks.Triggers.IAsyncOnDrawGizmosSelectedHandler Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosSelectedTrigger::GetOnDrawGizmosSelectedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncDrawGizmosSelectedTrigger_GetOnDrawGizmosSelectedAsyncHandler_m0608CC2047595CE12BBAA9A8AAB9C89AD8485C1C (void);
// 0x00000213 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosSelectedTrigger::OnDrawGizmosSelectedAsync()
extern void AsyncDrawGizmosSelectedTrigger_OnDrawGizmosSelectedAsync_m44E2CC815DC944A553E06565615D6FA79069A5F2 (void);
// 0x00000214 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosSelectedTrigger::OnDrawGizmosSelectedAsync(System.Threading.CancellationToken)
extern void AsyncDrawGizmosSelectedTrigger_OnDrawGizmosSelectedAsync_m201935EF54BD6E786376340262AA2795984DA421 (void);
// 0x00000215 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDrawGizmosSelectedTrigger::.ctor()
extern void AsyncDrawGizmosSelectedTrigger__ctor_m0CE776614C051D47985A62AD1FA82139ECB9FFFC (void);
// 0x00000216 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnEnableHandler::OnEnableAsync()
// 0x00000217 System.Void Cysharp.Threading.Tasks.Triggers.AsyncEnableTrigger::OnEnable()
extern void AsyncEnableTrigger_OnEnable_m70974D9CA4B9048370B1F96473C9AE5A2E5EB9D4 (void);
// 0x00000218 Cysharp.Threading.Tasks.Triggers.IAsyncOnEnableHandler Cysharp.Threading.Tasks.Triggers.AsyncEnableTrigger::GetOnEnableAsyncHandler()
extern void AsyncEnableTrigger_GetOnEnableAsyncHandler_m1A591CE6DF9392976AE6A241B5F0F3B714B35476 (void);
// 0x00000219 Cysharp.Threading.Tasks.Triggers.IAsyncOnEnableHandler Cysharp.Threading.Tasks.Triggers.AsyncEnableTrigger::GetOnEnableAsyncHandler(System.Threading.CancellationToken)
extern void AsyncEnableTrigger_GetOnEnableAsyncHandler_m4F773C7C6AE3ABF4715C0828825F7587F10C3544 (void);
// 0x0000021A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncEnableTrigger::OnEnableAsync()
extern void AsyncEnableTrigger_OnEnableAsync_m5C519820BED16BBEEA7420FE4F4EB94EF35AD920 (void);
// 0x0000021B Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncEnableTrigger::OnEnableAsync(System.Threading.CancellationToken)
extern void AsyncEnableTrigger_OnEnableAsync_m3E08A4558DE581805D69BA63E406917D40336026 (void);
// 0x0000021C System.Void Cysharp.Threading.Tasks.Triggers.AsyncEnableTrigger::.ctor()
extern void AsyncEnableTrigger__ctor_m797E2A5ADAA983DC15E69D8C301747BF45A774C6 (void);
// 0x0000021D Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnGUIHandler::OnGUIAsync()
// 0x0000021E System.Void Cysharp.Threading.Tasks.Triggers.AsyncGUITrigger::OnGUI()
extern void AsyncGUITrigger_OnGUI_mA60A8A66BEF86CA519EE4E2900D2BED234AA4214 (void);
// 0x0000021F Cysharp.Threading.Tasks.Triggers.IAsyncOnGUIHandler Cysharp.Threading.Tasks.Triggers.AsyncGUITrigger::GetOnGUIAsyncHandler()
extern void AsyncGUITrigger_GetOnGUIAsyncHandler_m9877F82A047A87ABC22B034E55835093E8FDDB66 (void);
// 0x00000220 Cysharp.Threading.Tasks.Triggers.IAsyncOnGUIHandler Cysharp.Threading.Tasks.Triggers.AsyncGUITrigger::GetOnGUIAsyncHandler(System.Threading.CancellationToken)
extern void AsyncGUITrigger_GetOnGUIAsyncHandler_m1577ACB3B4D075833499A6AF3F917CC8CEE4BDF6 (void);
// 0x00000221 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncGUITrigger::OnGUIAsync()
extern void AsyncGUITrigger_OnGUIAsync_m6B177ACF1208B8621259F08733A1C96A584CB3A3 (void);
// 0x00000222 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncGUITrigger::OnGUIAsync(System.Threading.CancellationToken)
extern void AsyncGUITrigger_OnGUIAsync_mDFB0A31E8B908689DA5B7AD631B78A5D2C59A8C5 (void);
// 0x00000223 System.Void Cysharp.Threading.Tasks.Triggers.AsyncGUITrigger::.ctor()
extern void AsyncGUITrigger__ctor_m662BBCEEFCDE94BECA4CAA56AF151BE17634B97C (void);
// 0x00000224 Cysharp.Threading.Tasks.UniTask`1<System.Single> Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreakHandler::OnJointBreakAsync()
// 0x00000225 System.Void Cysharp.Threading.Tasks.Triggers.AsyncJointBreakTrigger::OnJointBreak(System.Single)
extern void AsyncJointBreakTrigger_OnJointBreak_mD9EA2C90BE1BE9D6F8853766DD0CAAF021C19015 (void);
// 0x00000226 Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreakHandler Cysharp.Threading.Tasks.Triggers.AsyncJointBreakTrigger::GetOnJointBreakAsyncHandler()
extern void AsyncJointBreakTrigger_GetOnJointBreakAsyncHandler_m712AC149ADE057301DB3D9B6352820B11BAC4F11 (void);
// 0x00000227 Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreakHandler Cysharp.Threading.Tasks.Triggers.AsyncJointBreakTrigger::GetOnJointBreakAsyncHandler(System.Threading.CancellationToken)
extern void AsyncJointBreakTrigger_GetOnJointBreakAsyncHandler_mC167415E45318C712C024D067144002E26080AA2 (void);
// 0x00000228 Cysharp.Threading.Tasks.UniTask`1<System.Single> Cysharp.Threading.Tasks.Triggers.AsyncJointBreakTrigger::OnJointBreakAsync()
extern void AsyncJointBreakTrigger_OnJointBreakAsync_mB64C9BC08BC56D73D5110B5F08A26895E05A005C (void);
// 0x00000229 Cysharp.Threading.Tasks.UniTask`1<System.Single> Cysharp.Threading.Tasks.Triggers.AsyncJointBreakTrigger::OnJointBreakAsync(System.Threading.CancellationToken)
extern void AsyncJointBreakTrigger_OnJointBreakAsync_m6E19058FC6A70775AEC2F46BE218E89843E70403 (void);
// 0x0000022A System.Void Cysharp.Threading.Tasks.Triggers.AsyncJointBreakTrigger::.ctor()
extern void AsyncJointBreakTrigger__ctor_mC137FFE6730F70B7428DC27C2F2720477138E89D (void);
// 0x0000022B Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Joint2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreak2DHandler::OnJointBreak2DAsync()
// 0x0000022C System.Void Cysharp.Threading.Tasks.Triggers.AsyncJointBreak2DTrigger::OnJointBreak2D(UnityEngine.Joint2D)
extern void AsyncJointBreak2DTrigger_OnJointBreak2D_m01624E2455500DD44C35B71306E4419340264875 (void);
// 0x0000022D Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreak2DHandler Cysharp.Threading.Tasks.Triggers.AsyncJointBreak2DTrigger::GetOnJointBreak2DAsyncHandler()
extern void AsyncJointBreak2DTrigger_GetOnJointBreak2DAsyncHandler_m45780C88BDF8CC704979B961E7DC1EC49CDEF0DC (void);
// 0x0000022E Cysharp.Threading.Tasks.Triggers.IAsyncOnJointBreak2DHandler Cysharp.Threading.Tasks.Triggers.AsyncJointBreak2DTrigger::GetOnJointBreak2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncJointBreak2DTrigger_GetOnJointBreak2DAsyncHandler_m220EB0402717ABA05B1A6A5D890AED2B645AC638 (void);
// 0x0000022F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Joint2D> Cysharp.Threading.Tasks.Triggers.AsyncJointBreak2DTrigger::OnJointBreak2DAsync()
extern void AsyncJointBreak2DTrigger_OnJointBreak2DAsync_m2D70AE8F833194042855C9D7745939403B8093A9 (void);
// 0x00000230 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Joint2D> Cysharp.Threading.Tasks.Triggers.AsyncJointBreak2DTrigger::OnJointBreak2DAsync(System.Threading.CancellationToken)
extern void AsyncJointBreak2DTrigger_OnJointBreak2DAsync_m0D92E89FCAC9769301F88E2C1239671B8D01EC3A (void);
// 0x00000231 System.Void Cysharp.Threading.Tasks.Triggers.AsyncJointBreak2DTrigger::.ctor()
extern void AsyncJointBreak2DTrigger__ctor_mF63A573A77AC0A7C5B883281D5683D2B60AC4C50 (void);
// 0x00000232 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDownHandler::OnMouseDownAsync()
// 0x00000233 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseDownTrigger::OnMouseDown()
extern void AsyncMouseDownTrigger_OnMouseDown_mD70E1AB0E65B3197DD109E46E7225A3AE44D37F1 (void);
// 0x00000234 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDownHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseDownTrigger::GetOnMouseDownAsyncHandler()
extern void AsyncMouseDownTrigger_GetOnMouseDownAsyncHandler_m93DB1A59B96342FF3999D6A4B805F09CA47019FD (void);
// 0x00000235 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDownHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseDownTrigger::GetOnMouseDownAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseDownTrigger_GetOnMouseDownAsyncHandler_mDE0EB80F4116245D816CDFB1DF3154AC4E627998 (void);
// 0x00000236 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseDownTrigger::OnMouseDownAsync()
extern void AsyncMouseDownTrigger_OnMouseDownAsync_mBB53A0A2A3A30E3F24669934DAD5527AE0087579 (void);
// 0x00000237 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseDownTrigger::OnMouseDownAsync(System.Threading.CancellationToken)
extern void AsyncMouseDownTrigger_OnMouseDownAsync_m2632F5BCC92539EA8E26653B7701374891BF8428 (void);
// 0x00000238 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseDownTrigger::.ctor()
extern void AsyncMouseDownTrigger__ctor_m2763A2C928714E005383FC8A2F3439D14FCD596E (void);
// 0x00000239 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDragHandler::OnMouseDragAsync()
// 0x0000023A System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseDragTrigger::OnMouseDrag()
extern void AsyncMouseDragTrigger_OnMouseDrag_mB09792958890B7BE3A7A851859D822CBAEBDCFAA (void);
// 0x0000023B Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDragHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseDragTrigger::GetOnMouseDragAsyncHandler()
extern void AsyncMouseDragTrigger_GetOnMouseDragAsyncHandler_m9E6BD8B33DA44A23CD3BAB036CB0865004D973A3 (void);
// 0x0000023C Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseDragHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseDragTrigger::GetOnMouseDragAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseDragTrigger_GetOnMouseDragAsyncHandler_mC6B0344C1A7E183F0AB848CBE495AF2DC0AB4E9F (void);
// 0x0000023D Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseDragTrigger::OnMouseDragAsync()
extern void AsyncMouseDragTrigger_OnMouseDragAsync_mDDE64985357FE669552304A91AF7414D19DD49C5 (void);
// 0x0000023E Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseDragTrigger::OnMouseDragAsync(System.Threading.CancellationToken)
extern void AsyncMouseDragTrigger_OnMouseDragAsync_mFD3C6E9E0BF5F57BACF7E114916397A9AE50D4C6 (void);
// 0x0000023F System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseDragTrigger::.ctor()
extern void AsyncMouseDragTrigger__ctor_mBF7C5ED1FA43776B76CEF2BCB9502D865A11C1B6 (void);
// 0x00000240 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseEnterHandler::OnMouseEnterAsync()
// 0x00000241 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseEnterTrigger::OnMouseEnter()
extern void AsyncMouseEnterTrigger_OnMouseEnter_mFB4A1555DAE4077AB6F104083C305C53CC9BC4D1 (void);
// 0x00000242 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseEnterTrigger::GetOnMouseEnterAsyncHandler()
extern void AsyncMouseEnterTrigger_GetOnMouseEnterAsyncHandler_m67E0BB290E1CF9AE0761739F1ACE71A389E01D65 (void);
// 0x00000243 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseEnterTrigger::GetOnMouseEnterAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseEnterTrigger_GetOnMouseEnterAsyncHandler_m17A04209B766767E51845D2AAA81BEDB7F593F74 (void);
// 0x00000244 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseEnterTrigger::OnMouseEnterAsync()
extern void AsyncMouseEnterTrigger_OnMouseEnterAsync_m89544B77C50A5B3A1B8B5D1E79735CC0D551FA4B (void);
// 0x00000245 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseEnterTrigger::OnMouseEnterAsync(System.Threading.CancellationToken)
extern void AsyncMouseEnterTrigger_OnMouseEnterAsync_m7AFCF4A8C39320F1140D498F875302C8FBB51CC8 (void);
// 0x00000246 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseEnterTrigger::.ctor()
extern void AsyncMouseEnterTrigger__ctor_mA2DD14F92931DD86745DC13580D27D46DE24CB31 (void);
// 0x00000247 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseExitHandler::OnMouseExitAsync()
// 0x00000248 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseExitTrigger::OnMouseExit()
extern void AsyncMouseExitTrigger_OnMouseExit_m59257BF3027A7293984B895499081B94FD5F2354 (void);
// 0x00000249 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseExitHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseExitTrigger::GetOnMouseExitAsyncHandler()
extern void AsyncMouseExitTrigger_GetOnMouseExitAsyncHandler_mD172B6C09D3B6AEC264F117CB4624BBC1E8B0C70 (void);
// 0x0000024A Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseExitHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseExitTrigger::GetOnMouseExitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseExitTrigger_GetOnMouseExitAsyncHandler_m6E60B6F76951CFCA78CF8B930E41413E72F96F4C (void);
// 0x0000024B Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseExitTrigger::OnMouseExitAsync()
extern void AsyncMouseExitTrigger_OnMouseExitAsync_m82517A2EC3624E2BAAD6468ED1CF7B221C628487 (void);
// 0x0000024C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseExitTrigger::OnMouseExitAsync(System.Threading.CancellationToken)
extern void AsyncMouseExitTrigger_OnMouseExitAsync_mE7EA549AC351CAD99AAE7A4C1E721F3C01CACA3E (void);
// 0x0000024D System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseExitTrigger::.ctor()
extern void AsyncMouseExitTrigger__ctor_mC955EA41D6201C98903ADD11BE625717E6F445C3 (void);
// 0x0000024E Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseOverHandler::OnMouseOverAsync()
// 0x0000024F System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseOverTrigger::OnMouseOver()
extern void AsyncMouseOverTrigger_OnMouseOver_m1C685D9651467B77AEB5D1436D65A51FBCF72A7C (void);
// 0x00000250 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseOverHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseOverTrigger::GetOnMouseOverAsyncHandler()
extern void AsyncMouseOverTrigger_GetOnMouseOverAsyncHandler_m928D106360FA93208548EE72C0E21A5943F795EE (void);
// 0x00000251 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseOverHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseOverTrigger::GetOnMouseOverAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseOverTrigger_GetOnMouseOverAsyncHandler_m45A95B5495433A4D1CA430776C1D86960ECFF3E8 (void);
// 0x00000252 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseOverTrigger::OnMouseOverAsync()
extern void AsyncMouseOverTrigger_OnMouseOverAsync_m41C548B0190AD07924C6B7939E5D82980E8C3FAA (void);
// 0x00000253 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseOverTrigger::OnMouseOverAsync(System.Threading.CancellationToken)
extern void AsyncMouseOverTrigger_OnMouseOverAsync_mDDE132BA4B1F84A2BE21A2B94FFCCD7F54B3D32F (void);
// 0x00000254 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseOverTrigger::.ctor()
extern void AsyncMouseOverTrigger__ctor_mD55201077EF1E61CEE0FB9CE73DD253C8010C732 (void);
// 0x00000255 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpHandler::OnMouseUpAsync()
// 0x00000256 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseUpTrigger::OnMouseUp()
extern void AsyncMouseUpTrigger_OnMouseUp_m7DC9BBBC7E3714467EEA46017B05AAAA8C06B21E (void);
// 0x00000257 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseUpTrigger::GetOnMouseUpAsyncHandler()
extern void AsyncMouseUpTrigger_GetOnMouseUpAsyncHandler_m56C79C4B2DCB641BE60BD6F67DEC2079516D4E03 (void);
// 0x00000258 Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseUpTrigger::GetOnMouseUpAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseUpTrigger_GetOnMouseUpAsyncHandler_m9C0F4A2ADD55F597E9035573AD5CBB2CE0E25A5E (void);
// 0x00000259 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseUpTrigger::OnMouseUpAsync()
extern void AsyncMouseUpTrigger_OnMouseUpAsync_m22DAA9E6E97D23FB10F1CB90BD9D0449AC40FBF6 (void);
// 0x0000025A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseUpTrigger::OnMouseUpAsync(System.Threading.CancellationToken)
extern void AsyncMouseUpTrigger_OnMouseUpAsync_m0F62FC1FE5F6E8613C186999F83BE1477CFE0F06 (void);
// 0x0000025B System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseUpTrigger::.ctor()
extern void AsyncMouseUpTrigger__ctor_m8ADC4B17DBF78306C9970EAE792DF9A9951BDA0B (void);
// 0x0000025C Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpAsButtonHandler::OnMouseUpAsButtonAsync()
// 0x0000025D System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseUpAsButtonTrigger::OnMouseUpAsButton()
extern void AsyncMouseUpAsButtonTrigger_OnMouseUpAsButton_m28C398E98480BFA8F8F53906FC9AA76CA5348DAE (void);
// 0x0000025E Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpAsButtonHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseUpAsButtonTrigger::GetOnMouseUpAsButtonAsyncHandler()
extern void AsyncMouseUpAsButtonTrigger_GetOnMouseUpAsButtonAsyncHandler_mC351CFF7549764C6074889BB7BC6DCC4D178DFF9 (void);
// 0x0000025F Cysharp.Threading.Tasks.Triggers.IAsyncOnMouseUpAsButtonHandler Cysharp.Threading.Tasks.Triggers.AsyncMouseUpAsButtonTrigger::GetOnMouseUpAsButtonAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMouseUpAsButtonTrigger_GetOnMouseUpAsButtonAsyncHandler_mE44B444B1E913D898C48DEC635136D81E605A926 (void);
// 0x00000260 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseUpAsButtonTrigger::OnMouseUpAsButtonAsync()
extern void AsyncMouseUpAsButtonTrigger_OnMouseUpAsButtonAsync_mB2BAAE61D709EA3ECEC2E33A1FC45D709D910949 (void);
// 0x00000261 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncMouseUpAsButtonTrigger::OnMouseUpAsButtonAsync(System.Threading.CancellationToken)
extern void AsyncMouseUpAsButtonTrigger_OnMouseUpAsButtonAsync_mC59805D300AFF14E6F4F5F886C4AFA1D56591932 (void);
// 0x00000262 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMouseUpAsButtonTrigger::.ctor()
extern void AsyncMouseUpAsButtonTrigger__ctor_m8B080E387368B161D60D794B4F69869617D25937 (void);
// 0x00000263 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.GameObject> Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleCollisionHandler::OnParticleCollisionAsync()
// 0x00000264 System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleCollisionTrigger::OnParticleCollision(UnityEngine.GameObject)
extern void AsyncParticleCollisionTrigger_OnParticleCollision_m751F12DBF8C0A9B40DCFEA5A8E70CD17DD7012CE (void);
// 0x00000265 Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleCollisionHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleCollisionTrigger::GetOnParticleCollisionAsyncHandler()
extern void AsyncParticleCollisionTrigger_GetOnParticleCollisionAsyncHandler_m9E573001A0FE6D9F910F1FCA1C52902CDD97DC02 (void);
// 0x00000266 Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleCollisionHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleCollisionTrigger::GetOnParticleCollisionAsyncHandler(System.Threading.CancellationToken)
extern void AsyncParticleCollisionTrigger_GetOnParticleCollisionAsyncHandler_m86FD7A5FD578D80BD7F955B3177A547B7CA40B0C (void);
// 0x00000267 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.GameObject> Cysharp.Threading.Tasks.Triggers.AsyncParticleCollisionTrigger::OnParticleCollisionAsync()
extern void AsyncParticleCollisionTrigger_OnParticleCollisionAsync_m985AFDB84D8DE11C2A2775276CD1E5DE7AE3D0D5 (void);
// 0x00000268 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.GameObject> Cysharp.Threading.Tasks.Triggers.AsyncParticleCollisionTrigger::OnParticleCollisionAsync(System.Threading.CancellationToken)
extern void AsyncParticleCollisionTrigger_OnParticleCollisionAsync_m6161D1E223975321775AA2268D2147B84B9A4B22 (void);
// 0x00000269 System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleCollisionTrigger::.ctor()
extern void AsyncParticleCollisionTrigger__ctor_mAEA03BBACA41A213AD773F27375F8FFEEBF9DB74 (void);
// 0x0000026A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleSystemStoppedHandler::OnParticleSystemStoppedAsync()
// 0x0000026B System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleSystemStoppedTrigger::OnParticleSystemStopped()
extern void AsyncParticleSystemStoppedTrigger_OnParticleSystemStopped_m0BB207038D5D29E419B09D1FA1DE4CA6F9004886 (void);
// 0x0000026C Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleSystemStoppedHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleSystemStoppedTrigger::GetOnParticleSystemStoppedAsyncHandler()
extern void AsyncParticleSystemStoppedTrigger_GetOnParticleSystemStoppedAsyncHandler_m51E624F071B4BB1A698709A67201CE1A96A3B4FF (void);
// 0x0000026D Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleSystemStoppedHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleSystemStoppedTrigger::GetOnParticleSystemStoppedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncParticleSystemStoppedTrigger_GetOnParticleSystemStoppedAsyncHandler_m9ECDCE1AD60A74418F3BB37B5DE3680E856F2D8D (void);
// 0x0000026E Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncParticleSystemStoppedTrigger::OnParticleSystemStoppedAsync()
extern void AsyncParticleSystemStoppedTrigger_OnParticleSystemStoppedAsync_m0FB3F53A1B9DAE343FACE280EDCDE4986E5CC5B4 (void);
// 0x0000026F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncParticleSystemStoppedTrigger::OnParticleSystemStoppedAsync(System.Threading.CancellationToken)
extern void AsyncParticleSystemStoppedTrigger_OnParticleSystemStoppedAsync_mBF2E44551BDDA8812B370F2664E6307BF470CFD3 (void);
// 0x00000270 System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleSystemStoppedTrigger::.ctor()
extern void AsyncParticleSystemStoppedTrigger__ctor_m43AFD21B99F974A65692CD7FF010593671CDCE39 (void);
// 0x00000271 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleTriggerHandler::OnParticleTriggerAsync()
// 0x00000272 System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleTriggerTrigger::OnParticleTrigger()
extern void AsyncParticleTriggerTrigger_OnParticleTrigger_m33D86643A35447522AFAB46F2969317584392008 (void);
// 0x00000273 Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleTriggerHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleTriggerTrigger::GetOnParticleTriggerAsyncHandler()
extern void AsyncParticleTriggerTrigger_GetOnParticleTriggerAsyncHandler_m207D10B6570B7BB49C342BE3519208CECA61C49B (void);
// 0x00000274 Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleTriggerHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleTriggerTrigger::GetOnParticleTriggerAsyncHandler(System.Threading.CancellationToken)
extern void AsyncParticleTriggerTrigger_GetOnParticleTriggerAsyncHandler_mF11B045BF8ADD878F11CD3BB0C661092F8B223D6 (void);
// 0x00000275 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncParticleTriggerTrigger::OnParticleTriggerAsync()
extern void AsyncParticleTriggerTrigger_OnParticleTriggerAsync_mD840E0AA3A64696D296EAB0544656421A02387A0 (void);
// 0x00000276 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncParticleTriggerTrigger::OnParticleTriggerAsync(System.Threading.CancellationToken)
extern void AsyncParticleTriggerTrigger_OnParticleTriggerAsync_mC4218F4D150A0E1C01A6D66A0A0F1DC668F9408B (void);
// 0x00000277 System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleTriggerTrigger::.ctor()
extern void AsyncParticleTriggerTrigger__ctor_mC085D993BA271B2A8B9A5E06AC78F3F536243CA8 (void);
// 0x00000278 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ParticleSystemJobs.ParticleSystemJobData> Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleUpdateJobScheduledHandler::OnParticleUpdateJobScheduledAsync()
// 0x00000279 System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleUpdateJobScheduledTrigger::OnParticleUpdateJobScheduled(UnityEngine.ParticleSystemJobs.ParticleSystemJobData)
extern void AsyncParticleUpdateJobScheduledTrigger_OnParticleUpdateJobScheduled_m990067244B0769E88180A7F3887FEF8B2F9B643D (void);
// 0x0000027A Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleUpdateJobScheduledHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleUpdateJobScheduledTrigger::GetOnParticleUpdateJobScheduledAsyncHandler()
extern void AsyncParticleUpdateJobScheduledTrigger_GetOnParticleUpdateJobScheduledAsyncHandler_mB5886BA1112A1DEE15EE113B0A3EB6DA838F9FFE (void);
// 0x0000027B Cysharp.Threading.Tasks.Triggers.IAsyncOnParticleUpdateJobScheduledHandler Cysharp.Threading.Tasks.Triggers.AsyncParticleUpdateJobScheduledTrigger::GetOnParticleUpdateJobScheduledAsyncHandler(System.Threading.CancellationToken)
extern void AsyncParticleUpdateJobScheduledTrigger_GetOnParticleUpdateJobScheduledAsyncHandler_mCBA5A97FDA6D19FCE8777C7EFA23123CCF6A9F1B (void);
// 0x0000027C Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ParticleSystemJobs.ParticleSystemJobData> Cysharp.Threading.Tasks.Triggers.AsyncParticleUpdateJobScheduledTrigger::OnParticleUpdateJobScheduledAsync()
extern void AsyncParticleUpdateJobScheduledTrigger_OnParticleUpdateJobScheduledAsync_m7BFD0DD7EE318D546BC9AF5B3987F2A9DCB46DE9 (void);
// 0x0000027D Cysharp.Threading.Tasks.UniTask`1<UnityEngine.ParticleSystemJobs.ParticleSystemJobData> Cysharp.Threading.Tasks.Triggers.AsyncParticleUpdateJobScheduledTrigger::OnParticleUpdateJobScheduledAsync(System.Threading.CancellationToken)
extern void AsyncParticleUpdateJobScheduledTrigger_OnParticleUpdateJobScheduledAsync_mCB3326B52D80AA230C5B4E78CC6297403DB7DF2C (void);
// 0x0000027E System.Void Cysharp.Threading.Tasks.Triggers.AsyncParticleUpdateJobScheduledTrigger::.ctor()
extern void AsyncParticleUpdateJobScheduledTrigger__ctor_m9D92CC97CF40C32F047D2B332FBF50FC32429DF2 (void);
// 0x0000027F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnPostRenderHandler::OnPostRenderAsync()
// 0x00000280 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPostRenderTrigger::OnPostRender()
extern void AsyncPostRenderTrigger_OnPostRender_m6A344301FA3C6875671DEEFC6CA3F6310FB9313E (void);
// 0x00000281 Cysharp.Threading.Tasks.Triggers.IAsyncOnPostRenderHandler Cysharp.Threading.Tasks.Triggers.AsyncPostRenderTrigger::GetOnPostRenderAsyncHandler()
extern void AsyncPostRenderTrigger_GetOnPostRenderAsyncHandler_mED23D2BB8DF5EAE72452C87F7956C24FD1AC34E6 (void);
// 0x00000282 Cysharp.Threading.Tasks.Triggers.IAsyncOnPostRenderHandler Cysharp.Threading.Tasks.Triggers.AsyncPostRenderTrigger::GetOnPostRenderAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPostRenderTrigger_GetOnPostRenderAsyncHandler_mA12A2489D7FF41BAC1600E02B605E98D3360BA8B (void);
// 0x00000283 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncPostRenderTrigger::OnPostRenderAsync()
extern void AsyncPostRenderTrigger_OnPostRenderAsync_m77DD9B6B8676D926E0C1A61723BDDBB893D39F17 (void);
// 0x00000284 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncPostRenderTrigger::OnPostRenderAsync(System.Threading.CancellationToken)
extern void AsyncPostRenderTrigger_OnPostRenderAsync_m231BECAE2680E987C35214EB92D9693EADA4AD7D (void);
// 0x00000285 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPostRenderTrigger::.ctor()
extern void AsyncPostRenderTrigger__ctor_m65724E5E972301DDE66ABFBF154C1DB43CD66B56 (void);
// 0x00000286 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnPreCullHandler::OnPreCullAsync()
// 0x00000287 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPreCullTrigger::OnPreCull()
extern void AsyncPreCullTrigger_OnPreCull_mB8C1EC3B67FAE78AA652BAC3F62E040A45611A93 (void);
// 0x00000288 Cysharp.Threading.Tasks.Triggers.IAsyncOnPreCullHandler Cysharp.Threading.Tasks.Triggers.AsyncPreCullTrigger::GetOnPreCullAsyncHandler()
extern void AsyncPreCullTrigger_GetOnPreCullAsyncHandler_m8ECD951ACABD63751C9827C0E777518B735ACFCB (void);
// 0x00000289 Cysharp.Threading.Tasks.Triggers.IAsyncOnPreCullHandler Cysharp.Threading.Tasks.Triggers.AsyncPreCullTrigger::GetOnPreCullAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPreCullTrigger_GetOnPreCullAsyncHandler_m807441F3A8DC1D6A026076DD9776E743F62DDD53 (void);
// 0x0000028A Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncPreCullTrigger::OnPreCullAsync()
extern void AsyncPreCullTrigger_OnPreCullAsync_mBAFA9CA7B968528CAFDA7BC7A9E22B10A7182A24 (void);
// 0x0000028B Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncPreCullTrigger::OnPreCullAsync(System.Threading.CancellationToken)
extern void AsyncPreCullTrigger_OnPreCullAsync_m6DD3ED29EC36AC224B1F8A692F5F43C20FEC7C47 (void);
// 0x0000028C System.Void Cysharp.Threading.Tasks.Triggers.AsyncPreCullTrigger::.ctor()
extern void AsyncPreCullTrigger__ctor_mF28627C543F02FF51D0DD26AA1DDA8D84A315AB5 (void);
// 0x0000028D Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnPreRenderHandler::OnPreRenderAsync()
// 0x0000028E System.Void Cysharp.Threading.Tasks.Triggers.AsyncPreRenderTrigger::OnPreRender()
extern void AsyncPreRenderTrigger_OnPreRender_m655894FF19A236E354D9EE0A271C72DDBEE67F49 (void);
// 0x0000028F Cysharp.Threading.Tasks.Triggers.IAsyncOnPreRenderHandler Cysharp.Threading.Tasks.Triggers.AsyncPreRenderTrigger::GetOnPreRenderAsyncHandler()
extern void AsyncPreRenderTrigger_GetOnPreRenderAsyncHandler_m59B0FA38DBC1C256684A83C35125C2B4E0E92EB9 (void);
// 0x00000290 Cysharp.Threading.Tasks.Triggers.IAsyncOnPreRenderHandler Cysharp.Threading.Tasks.Triggers.AsyncPreRenderTrigger::GetOnPreRenderAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPreRenderTrigger_GetOnPreRenderAsyncHandler_m55779CA051D2F62199844763FBEE21E3E351CC6B (void);
// 0x00000291 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncPreRenderTrigger::OnPreRenderAsync()
extern void AsyncPreRenderTrigger_OnPreRenderAsync_m7E6F69F2AF47E4E04C3B6F3145E0C94BEA56D6C6 (void);
// 0x00000292 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncPreRenderTrigger::OnPreRenderAsync(System.Threading.CancellationToken)
extern void AsyncPreRenderTrigger_OnPreRenderAsync_mAE698C75E4BBB1C1683C0A27C7EA29AC39659AA7 (void);
// 0x00000293 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPreRenderTrigger::.ctor()
extern void AsyncPreRenderTrigger__ctor_m41C018E91D25D72553D40A71C1862A62A54FBA00 (void);
// 0x00000294 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformDimensionsChangeHandler::OnRectTransformDimensionsChangeAsync()
// 0x00000295 System.Void Cysharp.Threading.Tasks.Triggers.AsyncRectTransformDimensionsChangeTrigger::OnRectTransformDimensionsChange()
extern void AsyncRectTransformDimensionsChangeTrigger_OnRectTransformDimensionsChange_m4D147A3E1686F19D8D1E1D432DE7FC1BD7468DFB (void);
// 0x00000296 Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformDimensionsChangeHandler Cysharp.Threading.Tasks.Triggers.AsyncRectTransformDimensionsChangeTrigger::GetOnRectTransformDimensionsChangeAsyncHandler()
extern void AsyncRectTransformDimensionsChangeTrigger_GetOnRectTransformDimensionsChangeAsyncHandler_mC88B31DB52A51A35DBEDECE82F1C1C8929021ECD (void);
// 0x00000297 Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformDimensionsChangeHandler Cysharp.Threading.Tasks.Triggers.AsyncRectTransformDimensionsChangeTrigger::GetOnRectTransformDimensionsChangeAsyncHandler(System.Threading.CancellationToken)
extern void AsyncRectTransformDimensionsChangeTrigger_GetOnRectTransformDimensionsChangeAsyncHandler_m1F7822A0F70F51AAB0EAFFA4EF2A5EB240560C47 (void);
// 0x00000298 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncRectTransformDimensionsChangeTrigger::OnRectTransformDimensionsChangeAsync()
extern void AsyncRectTransformDimensionsChangeTrigger_OnRectTransformDimensionsChangeAsync_m0CF2185A6AB66B207AA402308ED1543331600225 (void);
// 0x00000299 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncRectTransformDimensionsChangeTrigger::OnRectTransformDimensionsChangeAsync(System.Threading.CancellationToken)
extern void AsyncRectTransformDimensionsChangeTrigger_OnRectTransformDimensionsChangeAsync_mF498150A5D2874E416F67263719F600F9EE54809 (void);
// 0x0000029A System.Void Cysharp.Threading.Tasks.Triggers.AsyncRectTransformDimensionsChangeTrigger::.ctor()
extern void AsyncRectTransformDimensionsChangeTrigger__ctor_mAA598E4BCABCE3C1CC14BC444B43D55B80913FB8 (void);
// 0x0000029B Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformRemovedHandler::OnRectTransformRemovedAsync()
// 0x0000029C System.Void Cysharp.Threading.Tasks.Triggers.AsyncRectTransformRemovedTrigger::OnRectTransformRemoved()
extern void AsyncRectTransformRemovedTrigger_OnRectTransformRemoved_m1292FE9EF5754A262299D4B71E0691000A9E4983 (void);
// 0x0000029D Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformRemovedHandler Cysharp.Threading.Tasks.Triggers.AsyncRectTransformRemovedTrigger::GetOnRectTransformRemovedAsyncHandler()
extern void AsyncRectTransformRemovedTrigger_GetOnRectTransformRemovedAsyncHandler_mCB506892D82DA5EFB2B85D34AF18EC04688046F8 (void);
// 0x0000029E Cysharp.Threading.Tasks.Triggers.IAsyncOnRectTransformRemovedHandler Cysharp.Threading.Tasks.Triggers.AsyncRectTransformRemovedTrigger::GetOnRectTransformRemovedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncRectTransformRemovedTrigger_GetOnRectTransformRemovedAsyncHandler_m6420AE5EA15B4EAA0C5CC440526B4804BEBF7347 (void);
// 0x0000029F Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncRectTransformRemovedTrigger::OnRectTransformRemovedAsync()
extern void AsyncRectTransformRemovedTrigger_OnRectTransformRemovedAsync_m52636D12D72390F1B2516C8D987301D5DF7B3209 (void);
// 0x000002A0 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncRectTransformRemovedTrigger::OnRectTransformRemovedAsync(System.Threading.CancellationToken)
extern void AsyncRectTransformRemovedTrigger_OnRectTransformRemovedAsync_m0258F87B763E60388FDF207F807A9049B2510279 (void);
// 0x000002A1 System.Void Cysharp.Threading.Tasks.Triggers.AsyncRectTransformRemovedTrigger::.ctor()
extern void AsyncRectTransformRemovedTrigger__ctor_m63D51C37FAE81E87B5DC5568EB9BDA338B3F77FB (void);
// 0x000002A2 Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderImageHandler::OnRenderImageAsync()
// 0x000002A3 System.Void Cysharp.Threading.Tasks.Triggers.AsyncRenderImageTrigger::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void AsyncRenderImageTrigger_OnRenderImage_m6BA68601EFEE0C4203E529C86541F3E995D23927 (void);
// 0x000002A4 Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderImageHandler Cysharp.Threading.Tasks.Triggers.AsyncRenderImageTrigger::GetOnRenderImageAsyncHandler()
extern void AsyncRenderImageTrigger_GetOnRenderImageAsyncHandler_m1A07A30154A3999B71558D5C3BA7B67D69E332AA (void);
// 0x000002A5 Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderImageHandler Cysharp.Threading.Tasks.Triggers.AsyncRenderImageTrigger::GetOnRenderImageAsyncHandler(System.Threading.CancellationToken)
extern void AsyncRenderImageTrigger_GetOnRenderImageAsyncHandler_mBC9E6E52E1432D79ACF1052FFA52E3D71F9521AA (void);
// 0x000002A6 Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> Cysharp.Threading.Tasks.Triggers.AsyncRenderImageTrigger::OnRenderImageAsync()
extern void AsyncRenderImageTrigger_OnRenderImageAsync_m15695771B3D3D1EE731505588D5C59BA46F395ED (void);
// 0x000002A7 Cysharp.Threading.Tasks.UniTask`1<System.ValueTuple`2<UnityEngine.RenderTexture,UnityEngine.RenderTexture>> Cysharp.Threading.Tasks.Triggers.AsyncRenderImageTrigger::OnRenderImageAsync(System.Threading.CancellationToken)
extern void AsyncRenderImageTrigger_OnRenderImageAsync_m894722EA905E1CF1E490C891E432AF1D9582CEDC (void);
// 0x000002A8 System.Void Cysharp.Threading.Tasks.Triggers.AsyncRenderImageTrigger::.ctor()
extern void AsyncRenderImageTrigger__ctor_m18A16A88B234F778B466DD0A82732A0D24D57B01 (void);
// 0x000002A9 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderObjectHandler::OnRenderObjectAsync()
// 0x000002AA System.Void Cysharp.Threading.Tasks.Triggers.AsyncRenderObjectTrigger::OnRenderObject()
extern void AsyncRenderObjectTrigger_OnRenderObject_m6284CD0D1D0E6A45D22CC4DC2EFF0EDF4DB06FFA (void);
// 0x000002AB Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderObjectHandler Cysharp.Threading.Tasks.Triggers.AsyncRenderObjectTrigger::GetOnRenderObjectAsyncHandler()
extern void AsyncRenderObjectTrigger_GetOnRenderObjectAsyncHandler_m46893092AEAC8422ED6DDADAA2F2B1828C82434A (void);
// 0x000002AC Cysharp.Threading.Tasks.Triggers.IAsyncOnRenderObjectHandler Cysharp.Threading.Tasks.Triggers.AsyncRenderObjectTrigger::GetOnRenderObjectAsyncHandler(System.Threading.CancellationToken)
extern void AsyncRenderObjectTrigger_GetOnRenderObjectAsyncHandler_m36200B7C98888046CA1E981893C9C12802752E29 (void);
// 0x000002AD Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncRenderObjectTrigger::OnRenderObjectAsync()
extern void AsyncRenderObjectTrigger_OnRenderObjectAsync_m88D2938A860B3D50F1C6755EE744D98D86F1F750 (void);
// 0x000002AE Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncRenderObjectTrigger::OnRenderObjectAsync(System.Threading.CancellationToken)
extern void AsyncRenderObjectTrigger_OnRenderObjectAsync_m095D596AFAE9A51FD36AE22B4C9FC7FB84CE0A82 (void);
// 0x000002AF System.Void Cysharp.Threading.Tasks.Triggers.AsyncRenderObjectTrigger::.ctor()
extern void AsyncRenderObjectTrigger__ctor_m44865FD89896C3310967341A6264C1518E85BE56 (void);
// 0x000002B0 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnServerInitializedHandler::OnServerInitializedAsync()
// 0x000002B1 System.Void Cysharp.Threading.Tasks.Triggers.AsyncServerInitializedTrigger::OnServerInitialized()
extern void AsyncServerInitializedTrigger_OnServerInitialized_m5B5BF7F93AA29141B67AB342FA3CB0A58C75FA62 (void);
// 0x000002B2 Cysharp.Threading.Tasks.Triggers.IAsyncOnServerInitializedHandler Cysharp.Threading.Tasks.Triggers.AsyncServerInitializedTrigger::GetOnServerInitializedAsyncHandler()
extern void AsyncServerInitializedTrigger_GetOnServerInitializedAsyncHandler_m721BC23A4300149AE4AFF878B743E11FAFED0EAE (void);
// 0x000002B3 Cysharp.Threading.Tasks.Triggers.IAsyncOnServerInitializedHandler Cysharp.Threading.Tasks.Triggers.AsyncServerInitializedTrigger::GetOnServerInitializedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncServerInitializedTrigger_GetOnServerInitializedAsyncHandler_mDC260D54895ED4EB77684425C4662D07A5E3ABD6 (void);
// 0x000002B4 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncServerInitializedTrigger::OnServerInitializedAsync()
extern void AsyncServerInitializedTrigger_OnServerInitializedAsync_mC7D833F2566DCFAA1C5EFC2068095EE4296D2F38 (void);
// 0x000002B5 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncServerInitializedTrigger::OnServerInitializedAsync(System.Threading.CancellationToken)
extern void AsyncServerInitializedTrigger_OnServerInitializedAsync_m11C96D6C590301B03F9ABCAD9061ACBD961CACC9 (void);
// 0x000002B6 System.Void Cysharp.Threading.Tasks.Triggers.AsyncServerInitializedTrigger::.ctor()
extern void AsyncServerInitializedTrigger__ctor_m381EB41606C60A11FD02FAACD077C73E72663748 (void);
// 0x000002B7 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformChildrenChangedHandler::OnTransformChildrenChangedAsync()
// 0x000002B8 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTransformChildrenChangedTrigger::OnTransformChildrenChanged()
extern void AsyncTransformChildrenChangedTrigger_OnTransformChildrenChanged_mD8F30C19ADDA4A8518AC039466085E30CCBF0CB2 (void);
// 0x000002B9 Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformChildrenChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncTransformChildrenChangedTrigger::GetOnTransformChildrenChangedAsyncHandler()
extern void AsyncTransformChildrenChangedTrigger_GetOnTransformChildrenChangedAsyncHandler_mBB6E764D4AB17D7705D5B5DF82486E5C9FDE9F83 (void);
// 0x000002BA Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformChildrenChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncTransformChildrenChangedTrigger::GetOnTransformChildrenChangedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTransformChildrenChangedTrigger_GetOnTransformChildrenChangedAsyncHandler_mAAA8725F908EAAB62E2294B24845056FE762E334 (void);
// 0x000002BB Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTransformChildrenChangedTrigger::OnTransformChildrenChangedAsync()
extern void AsyncTransformChildrenChangedTrigger_OnTransformChildrenChangedAsync_m5815B6E64017C06C99A7A0731358F23673373A77 (void);
// 0x000002BC Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTransformChildrenChangedTrigger::OnTransformChildrenChangedAsync(System.Threading.CancellationToken)
extern void AsyncTransformChildrenChangedTrigger_OnTransformChildrenChangedAsync_m039AF980CC5B5C0482489A664CA4E27FFFDFC2D2 (void);
// 0x000002BD System.Void Cysharp.Threading.Tasks.Triggers.AsyncTransformChildrenChangedTrigger::.ctor()
extern void AsyncTransformChildrenChangedTrigger__ctor_m8A7D94E9D7A1B9552324F38FC96325A61EDB1CDB (void);
// 0x000002BE Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformParentChangedHandler::OnTransformParentChangedAsync()
// 0x000002BF System.Void Cysharp.Threading.Tasks.Triggers.AsyncTransformParentChangedTrigger::OnTransformParentChanged()
extern void AsyncTransformParentChangedTrigger_OnTransformParentChanged_mC6D4BE9C7361D2D52D0F12DD6C2BBC362228D4C6 (void);
// 0x000002C0 Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformParentChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncTransformParentChangedTrigger::GetOnTransformParentChangedAsyncHandler()
extern void AsyncTransformParentChangedTrigger_GetOnTransformParentChangedAsyncHandler_mB07C2A5C8268B19CF55E9A594F94ADA550612D38 (void);
// 0x000002C1 Cysharp.Threading.Tasks.Triggers.IAsyncOnTransformParentChangedHandler Cysharp.Threading.Tasks.Triggers.AsyncTransformParentChangedTrigger::GetOnTransformParentChangedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTransformParentChangedTrigger_GetOnTransformParentChangedAsyncHandler_m48FFF091007D3A1D2EC7A7DD23B245A1F75C2237 (void);
// 0x000002C2 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTransformParentChangedTrigger::OnTransformParentChangedAsync()
extern void AsyncTransformParentChangedTrigger_OnTransformParentChangedAsync_m6971910B64FB85EED0106CC2147F4E44C6D97725 (void);
// 0x000002C3 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncTransformParentChangedTrigger::OnTransformParentChangedAsync(System.Threading.CancellationToken)
extern void AsyncTransformParentChangedTrigger_OnTransformParentChangedAsync_m2E2817A9692ACA4D57B6C3959EDF1C3BA9FDE99E (void);
// 0x000002C4 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTransformParentChangedTrigger::.ctor()
extern void AsyncTransformParentChangedTrigger__ctor_m2AFA067FB7D8AF9E9741E1BB6D4CEB163A73CBB1 (void);
// 0x000002C5 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnterHandler::OnTriggerEnterAsync()
// 0x000002C6 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnterTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void AsyncTriggerEnterTrigger_OnTriggerEnter_mB37A7745A8921BC91E6E3E1405CD3F55F3A0CA98 (void);
// 0x000002C7 Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnterTrigger::GetOnTriggerEnterAsyncHandler()
extern void AsyncTriggerEnterTrigger_GetOnTriggerEnterAsyncHandler_m828FB8FC4A0F5E351650DDC69AC6257541E0E410 (void);
// 0x000002C8 Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnterTrigger::GetOnTriggerEnterAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTriggerEnterTrigger_GetOnTriggerEnterAsyncHandler_mAE0A5C99547A55A5BB066FEE0093C108D3554AC9 (void);
// 0x000002C9 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnterTrigger::OnTriggerEnterAsync()
extern void AsyncTriggerEnterTrigger_OnTriggerEnterAsync_m9104DD0D7B9C49D82952FE14CDCA0A8D66327210 (void);
// 0x000002CA Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnterTrigger::OnTriggerEnterAsync(System.Threading.CancellationToken)
extern void AsyncTriggerEnterTrigger_OnTriggerEnterAsync_m49CB8ADBD70AF23DB5FCEB039577D656FAA7D292 (void);
// 0x000002CB System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnterTrigger::.ctor()
extern void AsyncTriggerEnterTrigger__ctor_mED114C4AC3A4AAB5611DE0CEF89337E4E9EF197C (void);
// 0x000002CC Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnter2DHandler::OnTriggerEnter2DAsync()
// 0x000002CD System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnter2DTrigger::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void AsyncTriggerEnter2DTrigger_OnTriggerEnter2D_m4A76B2B3A30D014199AEF338F718F4346995F2AE (void);
// 0x000002CE Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnter2DHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnter2DTrigger::GetOnTriggerEnter2DAsyncHandler()
extern void AsyncTriggerEnter2DTrigger_GetOnTriggerEnter2DAsyncHandler_m549AAF9382AF259D971A88482264EB3B2A307696 (void);
// 0x000002CF Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerEnter2DHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnter2DTrigger::GetOnTriggerEnter2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTriggerEnter2DTrigger_GetOnTriggerEnter2DAsyncHandler_m54E49E03BAB2E5D5D60BFE58C613CF7B9EB56AA6 (void);
// 0x000002D0 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnter2DTrigger::OnTriggerEnter2DAsync()
extern void AsyncTriggerEnter2DTrigger_OnTriggerEnter2DAsync_m1FD08D259A09497C9C847587011C7B9295C478D8 (void);
// 0x000002D1 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnter2DTrigger::OnTriggerEnter2DAsync(System.Threading.CancellationToken)
extern void AsyncTriggerEnter2DTrigger_OnTriggerEnter2DAsync_m7A7C4312B665096AABA1F5C90D2BE2E2E54B926E (void);
// 0x000002D2 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerEnter2DTrigger::.ctor()
extern void AsyncTriggerEnter2DTrigger__ctor_m8506BC0C1177C5CC23D2517F4F2BD4F798D85FB7 (void);
// 0x000002D3 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExitHandler::OnTriggerExitAsync()
// 0x000002D4 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerExitTrigger::OnTriggerExit(UnityEngine.Collider)
extern void AsyncTriggerExitTrigger_OnTriggerExit_m65D0D5ED581AE51F2E144015345DA004BDAE7389 (void);
// 0x000002D5 Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExitHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerExitTrigger::GetOnTriggerExitAsyncHandler()
extern void AsyncTriggerExitTrigger_GetOnTriggerExitAsyncHandler_m3541934C34BFC00CA6E1D155C434EACAD65323F2 (void);
// 0x000002D6 Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExitHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerExitTrigger::GetOnTriggerExitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTriggerExitTrigger_GetOnTriggerExitAsyncHandler_mAD35142C515F9F433825A755EF7F0EF575357607 (void);
// 0x000002D7 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerExitTrigger::OnTriggerExitAsync()
extern void AsyncTriggerExitTrigger_OnTriggerExitAsync_m0506F9F37FF7E065A2CB5BD43C3BD74704887CE2 (void);
// 0x000002D8 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerExitTrigger::OnTriggerExitAsync(System.Threading.CancellationToken)
extern void AsyncTriggerExitTrigger_OnTriggerExitAsync_mD22FB40BE1136E87660E936F60FB452FABDE24FD (void);
// 0x000002D9 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerExitTrigger::.ctor()
extern void AsyncTriggerExitTrigger__ctor_m41699831CB7F6A228617FBCD3AF3A12A309DD30C (void);
// 0x000002DA Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExit2DHandler::OnTriggerExit2DAsync()
// 0x000002DB System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerExit2DTrigger::OnTriggerExit2D(UnityEngine.Collider2D)
extern void AsyncTriggerExit2DTrigger_OnTriggerExit2D_m0B116706765FF54C1E18025B6EE32461F74EE11A (void);
// 0x000002DC Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExit2DHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerExit2DTrigger::GetOnTriggerExit2DAsyncHandler()
extern void AsyncTriggerExit2DTrigger_GetOnTriggerExit2DAsyncHandler_m6031C0D82F53F432BF19A2465DB00117B933DDCE (void);
// 0x000002DD Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerExit2DHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerExit2DTrigger::GetOnTriggerExit2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTriggerExit2DTrigger_GetOnTriggerExit2DAsyncHandler_m85A8371A2CC850F04E405341DE600D0BD13FFE8C (void);
// 0x000002DE Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerExit2DTrigger::OnTriggerExit2DAsync()
extern void AsyncTriggerExit2DTrigger_OnTriggerExit2DAsync_m5EE03B2172A0B9931A69A455123CDA808F098600 (void);
// 0x000002DF Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerExit2DTrigger::OnTriggerExit2DAsync(System.Threading.CancellationToken)
extern void AsyncTriggerExit2DTrigger_OnTriggerExit2DAsync_m00632B2548CD0ADC4369C7A5BB8A55D1977CDEF3 (void);
// 0x000002E0 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerExit2DTrigger::.ctor()
extern void AsyncTriggerExit2DTrigger__ctor_m18CD7479FA414B323FC6FC36F75C7A641BF4BD53 (void);
// 0x000002E1 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStayHandler::OnTriggerStayAsync()
// 0x000002E2 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerStayTrigger::OnTriggerStay(UnityEngine.Collider)
extern void AsyncTriggerStayTrigger_OnTriggerStay_m84AEF8BD5E0D0E7009AF75FD72EB35B251FA6C32 (void);
// 0x000002E3 Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStayHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerStayTrigger::GetOnTriggerStayAsyncHandler()
extern void AsyncTriggerStayTrigger_GetOnTriggerStayAsyncHandler_mA49FDB93C58E9DAD24AEFE69CF06C53D088EBE7A (void);
// 0x000002E4 Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStayHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerStayTrigger::GetOnTriggerStayAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTriggerStayTrigger_GetOnTriggerStayAsyncHandler_mB0DAB9AD8E8F489CCF5DF5DA5A900AE332E2670C (void);
// 0x000002E5 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerStayTrigger::OnTriggerStayAsync()
extern void AsyncTriggerStayTrigger_OnTriggerStayAsync_mA8972D945109767D84A6E79665143EA081455280 (void);
// 0x000002E6 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider> Cysharp.Threading.Tasks.Triggers.AsyncTriggerStayTrigger::OnTriggerStayAsync(System.Threading.CancellationToken)
extern void AsyncTriggerStayTrigger_OnTriggerStayAsync_m76FB08E3FD87DF4084B8445FB989E0960CB8B703 (void);
// 0x000002E7 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerStayTrigger::.ctor()
extern void AsyncTriggerStayTrigger__ctor_mF856C0C76C958CF2134AE02631ED82FB3BC9C093 (void);
// 0x000002E8 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStay2DHandler::OnTriggerStay2DAsync()
// 0x000002E9 System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerStay2DTrigger::OnTriggerStay2D(UnityEngine.Collider2D)
extern void AsyncTriggerStay2DTrigger_OnTriggerStay2D_m9BECEE469903EF4BAAF8C8F60F5134BD12A38A47 (void);
// 0x000002EA Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStay2DHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerStay2DTrigger::GetOnTriggerStay2DAsyncHandler()
extern void AsyncTriggerStay2DTrigger_GetOnTriggerStay2DAsyncHandler_mB85D0BDC1569ADC6D77BABD0E7DC585BCBF1B638 (void);
// 0x000002EB Cysharp.Threading.Tasks.Triggers.IAsyncOnTriggerStay2DHandler Cysharp.Threading.Tasks.Triggers.AsyncTriggerStay2DTrigger::GetOnTriggerStay2DAsyncHandler(System.Threading.CancellationToken)
extern void AsyncTriggerStay2DTrigger_GetOnTriggerStay2DAsyncHandler_m664F9A79F8D50099AB05AC60F322BA7A103F18D4 (void);
// 0x000002EC Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerStay2DTrigger::OnTriggerStay2DAsync()
extern void AsyncTriggerStay2DTrigger_OnTriggerStay2DAsync_m560D6EAC73326DAED1C878EAAD30203354DD327C (void);
// 0x000002ED Cysharp.Threading.Tasks.UniTask`1<UnityEngine.Collider2D> Cysharp.Threading.Tasks.Triggers.AsyncTriggerStay2DTrigger::OnTriggerStay2DAsync(System.Threading.CancellationToken)
extern void AsyncTriggerStay2DTrigger_OnTriggerStay2DAsync_m13E6D85207AA0D3DEDEAAE391D2543E63133397A (void);
// 0x000002EE System.Void Cysharp.Threading.Tasks.Triggers.AsyncTriggerStay2DTrigger::.ctor()
extern void AsyncTriggerStay2DTrigger__ctor_m5A99E8921602F7ADF6F12753863D020034EF4B03 (void);
// 0x000002EF Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnValidateHandler::OnValidateAsync()
// 0x000002F0 System.Void Cysharp.Threading.Tasks.Triggers.AsyncValidateTrigger::OnValidate()
extern void AsyncValidateTrigger_OnValidate_m4EB45C40CA6F395E5E3BB2F43077D1397FFB7D99 (void);
// 0x000002F1 Cysharp.Threading.Tasks.Triggers.IAsyncOnValidateHandler Cysharp.Threading.Tasks.Triggers.AsyncValidateTrigger::GetOnValidateAsyncHandler()
extern void AsyncValidateTrigger_GetOnValidateAsyncHandler_mFD485310050DFE5C9DB4E4E8707067A97EADDF64 (void);
// 0x000002F2 Cysharp.Threading.Tasks.Triggers.IAsyncOnValidateHandler Cysharp.Threading.Tasks.Triggers.AsyncValidateTrigger::GetOnValidateAsyncHandler(System.Threading.CancellationToken)
extern void AsyncValidateTrigger_GetOnValidateAsyncHandler_m97B53533031EC18ADBD0E27CCD7D4517C6DBB287 (void);
// 0x000002F3 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncValidateTrigger::OnValidateAsync()
extern void AsyncValidateTrigger_OnValidateAsync_m554192DE3BCFDD97898F1B79DD0C82E7B4F88B28 (void);
// 0x000002F4 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncValidateTrigger::OnValidateAsync(System.Threading.CancellationToken)
extern void AsyncValidateTrigger_OnValidateAsync_m348CD615CBED4C96AC9E06CB79DB8246FE29A68E (void);
// 0x000002F5 System.Void Cysharp.Threading.Tasks.Triggers.AsyncValidateTrigger::.ctor()
extern void AsyncValidateTrigger__ctor_mF8FAD51D9560D4FA8774BF252A22E56C81957F34 (void);
// 0x000002F6 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncOnWillRenderObjectHandler::OnWillRenderObjectAsync()
// 0x000002F7 System.Void Cysharp.Threading.Tasks.Triggers.AsyncWillRenderObjectTrigger::OnWillRenderObject()
extern void AsyncWillRenderObjectTrigger_OnWillRenderObject_m66778D8D448CE41839513EFA7298A084D5CD9209 (void);
// 0x000002F8 Cysharp.Threading.Tasks.Triggers.IAsyncOnWillRenderObjectHandler Cysharp.Threading.Tasks.Triggers.AsyncWillRenderObjectTrigger::GetOnWillRenderObjectAsyncHandler()
extern void AsyncWillRenderObjectTrigger_GetOnWillRenderObjectAsyncHandler_m3F3B7E5AE502B08B3EC76FBF33D8DB8D05E96180 (void);
// 0x000002F9 Cysharp.Threading.Tasks.Triggers.IAsyncOnWillRenderObjectHandler Cysharp.Threading.Tasks.Triggers.AsyncWillRenderObjectTrigger::GetOnWillRenderObjectAsyncHandler(System.Threading.CancellationToken)
extern void AsyncWillRenderObjectTrigger_GetOnWillRenderObjectAsyncHandler_m38AE715434BD96E22EF763AF39640796E8FACF17 (void);
// 0x000002FA Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncWillRenderObjectTrigger::OnWillRenderObjectAsync()
extern void AsyncWillRenderObjectTrigger_OnWillRenderObjectAsync_m07FA2F03D03B58516A3FF615EEEB10B4F096FF63 (void);
// 0x000002FB Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncWillRenderObjectTrigger::OnWillRenderObjectAsync(System.Threading.CancellationToken)
extern void AsyncWillRenderObjectTrigger_OnWillRenderObjectAsync_m718D31D27CE461610034BFE846636A8400DF22DE (void);
// 0x000002FC System.Void Cysharp.Threading.Tasks.Triggers.AsyncWillRenderObjectTrigger::.ctor()
extern void AsyncWillRenderObjectTrigger__ctor_mE32AA2A59BF32E5FDF7126C04FD93F8F394CBE73 (void);
// 0x000002FD Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncResetHandler::ResetAsync()
// 0x000002FE System.Void Cysharp.Threading.Tasks.Triggers.AsyncResetTrigger::Reset()
extern void AsyncResetTrigger_Reset_mBC8FF81E5B2EF8CBA1FB599E5298A89B618E048D (void);
// 0x000002FF Cysharp.Threading.Tasks.Triggers.IAsyncResetHandler Cysharp.Threading.Tasks.Triggers.AsyncResetTrigger::GetResetAsyncHandler()
extern void AsyncResetTrigger_GetResetAsyncHandler_m824B83F00073BF5575F86045D35A387139292431 (void);
// 0x00000300 Cysharp.Threading.Tasks.Triggers.IAsyncResetHandler Cysharp.Threading.Tasks.Triggers.AsyncResetTrigger::GetResetAsyncHandler(System.Threading.CancellationToken)
extern void AsyncResetTrigger_GetResetAsyncHandler_m79D5BA1E6024B03F5BC8C511EC155B84C69686B5 (void);
// 0x00000301 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncResetTrigger::ResetAsync()
extern void AsyncResetTrigger_ResetAsync_mB1359EA13240C409FE151CE01805E6487F009BA2 (void);
// 0x00000302 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncResetTrigger::ResetAsync(System.Threading.CancellationToken)
extern void AsyncResetTrigger_ResetAsync_mF397DE0A4FA0CB3897DDD83B3B6A429D4EDBD98B (void);
// 0x00000303 System.Void Cysharp.Threading.Tasks.Triggers.AsyncResetTrigger::.ctor()
extern void AsyncResetTrigger__ctor_m6FE44AA6AD1A192E1A453EA86E27B8325E57CFBE (void);
// 0x00000304 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.IAsyncUpdateHandler::UpdateAsync()
// 0x00000305 System.Void Cysharp.Threading.Tasks.Triggers.AsyncUpdateTrigger::Update()
extern void AsyncUpdateTrigger_Update_m6A8BB9E02929E519F4220BF0AC31B700657821F7 (void);
// 0x00000306 Cysharp.Threading.Tasks.Triggers.IAsyncUpdateHandler Cysharp.Threading.Tasks.Triggers.AsyncUpdateTrigger::GetUpdateAsyncHandler()
extern void AsyncUpdateTrigger_GetUpdateAsyncHandler_mEE20F6AFBDD0F73500F0E931E4B62A0CE6F803F6 (void);
// 0x00000307 Cysharp.Threading.Tasks.Triggers.IAsyncUpdateHandler Cysharp.Threading.Tasks.Triggers.AsyncUpdateTrigger::GetUpdateAsyncHandler(System.Threading.CancellationToken)
extern void AsyncUpdateTrigger_GetUpdateAsyncHandler_mC1A4D4D3D9B9E0FED5495C527C611137744FFD08 (void);
// 0x00000308 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncUpdateTrigger::UpdateAsync()
extern void AsyncUpdateTrigger_UpdateAsync_mBF4AC1B8394F446AC65E28B49C9007C14F8A62AC (void);
// 0x00000309 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.Triggers.AsyncUpdateTrigger::UpdateAsync(System.Threading.CancellationToken)
extern void AsyncUpdateTrigger_UpdateAsync_m6F62830AC2B911CE028627D336CF7FDE6A3122F7 (void);
// 0x0000030A System.Void Cysharp.Threading.Tasks.Triggers.AsyncUpdateTrigger::.ctor()
extern void AsyncUpdateTrigger__ctor_m7D5482B0A707705E896A434E95C7DE3B007BA131 (void);
// 0x0000030B Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnBeginDragHandler::OnBeginDragAsync()
// 0x0000030C System.Void Cysharp.Threading.Tasks.Triggers.AsyncBeginDragTrigger::UnityEngine.EventSystems.IBeginDragHandler.OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void AsyncBeginDragTrigger_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_mF4D36C965EF27FE13798AE33F7910DEEFD4BAEF7 (void);
// 0x0000030D Cysharp.Threading.Tasks.Triggers.IAsyncOnBeginDragHandler Cysharp.Threading.Tasks.Triggers.AsyncBeginDragTrigger::GetOnBeginDragAsyncHandler()
extern void AsyncBeginDragTrigger_GetOnBeginDragAsyncHandler_m99C573F5900022DD901EF0BF1CF1F0543DAD05B6 (void);
// 0x0000030E Cysharp.Threading.Tasks.Triggers.IAsyncOnBeginDragHandler Cysharp.Threading.Tasks.Triggers.AsyncBeginDragTrigger::GetOnBeginDragAsyncHandler(System.Threading.CancellationToken)
extern void AsyncBeginDragTrigger_GetOnBeginDragAsyncHandler_m9B14EBFD9667AD8C9316EE8F8D641002FBFD9159 (void);
// 0x0000030F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncBeginDragTrigger::OnBeginDragAsync()
extern void AsyncBeginDragTrigger_OnBeginDragAsync_mD7EF6BD3B2E760D6B953C47488E4786D218DB904 (void);
// 0x00000310 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncBeginDragTrigger::OnBeginDragAsync(System.Threading.CancellationToken)
extern void AsyncBeginDragTrigger_OnBeginDragAsync_m1918F458147993D3B7AF51D7C6A71D4359831F24 (void);
// 0x00000311 System.Void Cysharp.Threading.Tasks.Triggers.AsyncBeginDragTrigger::.ctor()
extern void AsyncBeginDragTrigger__ctor_m6934073E233A1682E395BA92A0D62CBE65299738 (void);
// 0x00000312 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnCancelHandler::OnCancelAsync()
// 0x00000313 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCancelTrigger::UnityEngine.EventSystems.ICancelHandler.OnCancel(UnityEngine.EventSystems.BaseEventData)
extern void AsyncCancelTrigger_UnityEngine_EventSystems_ICancelHandler_OnCancel_m4FDE66F8B68DDC571AFA979C44565ED72B616E4E (void);
// 0x00000314 Cysharp.Threading.Tasks.Triggers.IAsyncOnCancelHandler Cysharp.Threading.Tasks.Triggers.AsyncCancelTrigger::GetOnCancelAsyncHandler()
extern void AsyncCancelTrigger_GetOnCancelAsyncHandler_m1099B4B747D41A121B214E3BCCED5246AB878D8E (void);
// 0x00000315 Cysharp.Threading.Tasks.Triggers.IAsyncOnCancelHandler Cysharp.Threading.Tasks.Triggers.AsyncCancelTrigger::GetOnCancelAsyncHandler(System.Threading.CancellationToken)
extern void AsyncCancelTrigger_GetOnCancelAsyncHandler_mE6580CC2E3D0BE8F2A68F0315788967408551F84 (void);
// 0x00000316 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncCancelTrigger::OnCancelAsync()
extern void AsyncCancelTrigger_OnCancelAsync_mAB5C6E49A6FF9884F642F091E764F2C3BE1909E0 (void);
// 0x00000317 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncCancelTrigger::OnCancelAsync(System.Threading.CancellationToken)
extern void AsyncCancelTrigger_OnCancelAsync_m8354E1DE5C5B44E4ADAFEF83EFD2DB9C1105978B (void);
// 0x00000318 System.Void Cysharp.Threading.Tasks.Triggers.AsyncCancelTrigger::.ctor()
extern void AsyncCancelTrigger__ctor_mF5B969F04F0A30397156786E825F6639780B9413 (void);
// 0x00000319 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnDeselectHandler::OnDeselectAsync()
// 0x0000031A System.Void Cysharp.Threading.Tasks.Triggers.AsyncDeselectTrigger::UnityEngine.EventSystems.IDeselectHandler.OnDeselect(UnityEngine.EventSystems.BaseEventData)
extern void AsyncDeselectTrigger_UnityEngine_EventSystems_IDeselectHandler_OnDeselect_mAB62A2F625193E8EB567EA98DEAAD8F7338E2FE7 (void);
// 0x0000031B Cysharp.Threading.Tasks.Triggers.IAsyncOnDeselectHandler Cysharp.Threading.Tasks.Triggers.AsyncDeselectTrigger::GetOnDeselectAsyncHandler()
extern void AsyncDeselectTrigger_GetOnDeselectAsyncHandler_m6313BFA5E2A4EF4C62384AA934ED61286BC380F0 (void);
// 0x0000031C Cysharp.Threading.Tasks.Triggers.IAsyncOnDeselectHandler Cysharp.Threading.Tasks.Triggers.AsyncDeselectTrigger::GetOnDeselectAsyncHandler(System.Threading.CancellationToken)
extern void AsyncDeselectTrigger_GetOnDeselectAsyncHandler_mD7067A8EC1F5F7E3D4B81E13A1464C0CE196E14C (void);
// 0x0000031D Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncDeselectTrigger::OnDeselectAsync()
extern void AsyncDeselectTrigger_OnDeselectAsync_m6FF538527BFCA2139FE41C996BB104C6F953F819 (void);
// 0x0000031E Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncDeselectTrigger::OnDeselectAsync(System.Threading.CancellationToken)
extern void AsyncDeselectTrigger_OnDeselectAsync_m98CFBD0B3C3AE80C3C095842AFBD58852AE2DABD (void);
// 0x0000031F System.Void Cysharp.Threading.Tasks.Triggers.AsyncDeselectTrigger::.ctor()
extern void AsyncDeselectTrigger__ctor_mD911081015ED71919F39FAAB7E6E23A988EDA4AB (void);
// 0x00000320 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnDragHandler::OnDragAsync()
// 0x00000321 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDragTrigger::UnityEngine.EventSystems.IDragHandler.OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void AsyncDragTrigger_UnityEngine_EventSystems_IDragHandler_OnDrag_m650050B1A3637A54D4640B73D498D3AF2A80EA7B (void);
// 0x00000322 Cysharp.Threading.Tasks.Triggers.IAsyncOnDragHandler Cysharp.Threading.Tasks.Triggers.AsyncDragTrigger::GetOnDragAsyncHandler()
extern void AsyncDragTrigger_GetOnDragAsyncHandler_m792B59F8831AD3A5CD8CBE78FB9EE81ABBD2C449 (void);
// 0x00000323 Cysharp.Threading.Tasks.Triggers.IAsyncOnDragHandler Cysharp.Threading.Tasks.Triggers.AsyncDragTrigger::GetOnDragAsyncHandler(System.Threading.CancellationToken)
extern void AsyncDragTrigger_GetOnDragAsyncHandler_mD011C4C6710A5A29A7BBAD858DFF069EDD8A896A (void);
// 0x00000324 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncDragTrigger::OnDragAsync()
extern void AsyncDragTrigger_OnDragAsync_mE52D48B63CE940A0070247015A23FDB5847B0D22 (void);
// 0x00000325 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncDragTrigger::OnDragAsync(System.Threading.CancellationToken)
extern void AsyncDragTrigger_OnDragAsync_mFD7F0A4829E380EC9BE75E290062E33744DFAF59 (void);
// 0x00000326 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDragTrigger::.ctor()
extern void AsyncDragTrigger__ctor_m2E1E4AEA309FA552FAAA24009B15F5C12315A7DF (void);
// 0x00000327 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnDropHandler::OnDropAsync()
// 0x00000328 System.Void Cysharp.Threading.Tasks.Triggers.AsyncDropTrigger::UnityEngine.EventSystems.IDropHandler.OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void AsyncDropTrigger_UnityEngine_EventSystems_IDropHandler_OnDrop_mD54F5762DBB3975DFCB29E90675A22E7468C9C8A (void);
// 0x00000329 Cysharp.Threading.Tasks.Triggers.IAsyncOnDropHandler Cysharp.Threading.Tasks.Triggers.AsyncDropTrigger::GetOnDropAsyncHandler()
extern void AsyncDropTrigger_GetOnDropAsyncHandler_m710A98E56E9066858466C8C8B78A1280EA0B7010 (void);
// 0x0000032A Cysharp.Threading.Tasks.Triggers.IAsyncOnDropHandler Cysharp.Threading.Tasks.Triggers.AsyncDropTrigger::GetOnDropAsyncHandler(System.Threading.CancellationToken)
extern void AsyncDropTrigger_GetOnDropAsyncHandler_m39EA60EFEF6D8B34C615A6C1C09A3CD45E5A218B (void);
// 0x0000032B Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncDropTrigger::OnDropAsync()
extern void AsyncDropTrigger_OnDropAsync_m719D00F901423B744112C9B055A6FFD0B9FBA3C4 (void);
// 0x0000032C Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncDropTrigger::OnDropAsync(System.Threading.CancellationToken)
extern void AsyncDropTrigger_OnDropAsync_m4B0261239FF304E686F19EED6673D15603C58187 (void);
// 0x0000032D System.Void Cysharp.Threading.Tasks.Triggers.AsyncDropTrigger::.ctor()
extern void AsyncDropTrigger__ctor_m3F7CE17E66816B6C502FFD1322970B126DBBBA54 (void);
// 0x0000032E Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnEndDragHandler::OnEndDragAsync()
// 0x0000032F System.Void Cysharp.Threading.Tasks.Triggers.AsyncEndDragTrigger::UnityEngine.EventSystems.IEndDragHandler.OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void AsyncEndDragTrigger_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_mB1BE9EDD57ACD7A52AD5307761D66CA3744F3E4A (void);
// 0x00000330 Cysharp.Threading.Tasks.Triggers.IAsyncOnEndDragHandler Cysharp.Threading.Tasks.Triggers.AsyncEndDragTrigger::GetOnEndDragAsyncHandler()
extern void AsyncEndDragTrigger_GetOnEndDragAsyncHandler_m92AE52FAEEAA64C68B9BE79F942D25F047C9B98B (void);
// 0x00000331 Cysharp.Threading.Tasks.Triggers.IAsyncOnEndDragHandler Cysharp.Threading.Tasks.Triggers.AsyncEndDragTrigger::GetOnEndDragAsyncHandler(System.Threading.CancellationToken)
extern void AsyncEndDragTrigger_GetOnEndDragAsyncHandler_m4A5AD2C344FCAC54B14611C0F72D52A8731F3749 (void);
// 0x00000332 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncEndDragTrigger::OnEndDragAsync()
extern void AsyncEndDragTrigger_OnEndDragAsync_mD0632C140193F1EB06C90BC04DBB6BCBD1F252F1 (void);
// 0x00000333 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncEndDragTrigger::OnEndDragAsync(System.Threading.CancellationToken)
extern void AsyncEndDragTrigger_OnEndDragAsync_m656BD8AA647525EA96C4A423DBC238A49FE9FFA5 (void);
// 0x00000334 System.Void Cysharp.Threading.Tasks.Triggers.AsyncEndDragTrigger::.ctor()
extern void AsyncEndDragTrigger__ctor_m9CF68CD3A93B747C6A42B9E1990CAF00CEDD9833 (void);
// 0x00000335 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnInitializePotentialDragHandler::OnInitializePotentialDragAsync()
// 0x00000336 System.Void Cysharp.Threading.Tasks.Triggers.AsyncInitializePotentialDragTrigger::UnityEngine.EventSystems.IInitializePotentialDragHandler.OnInitializePotentialDrag(UnityEngine.EventSystems.PointerEventData)
extern void AsyncInitializePotentialDragTrigger_UnityEngine_EventSystems_IInitializePotentialDragHandler_OnInitializePotentialDrag_m1122772FE7F969337CE6437B0FCA48FCCA95339C (void);
// 0x00000337 Cysharp.Threading.Tasks.Triggers.IAsyncOnInitializePotentialDragHandler Cysharp.Threading.Tasks.Triggers.AsyncInitializePotentialDragTrigger::GetOnInitializePotentialDragAsyncHandler()
extern void AsyncInitializePotentialDragTrigger_GetOnInitializePotentialDragAsyncHandler_m9026E10BF5D23F7359C1E62E11BA99F44D6CA883 (void);
// 0x00000338 Cysharp.Threading.Tasks.Triggers.IAsyncOnInitializePotentialDragHandler Cysharp.Threading.Tasks.Triggers.AsyncInitializePotentialDragTrigger::GetOnInitializePotentialDragAsyncHandler(System.Threading.CancellationToken)
extern void AsyncInitializePotentialDragTrigger_GetOnInitializePotentialDragAsyncHandler_mF46B0E0F8B4B52248934D7A7F263A461BB6406FF (void);
// 0x00000339 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncInitializePotentialDragTrigger::OnInitializePotentialDragAsync()
extern void AsyncInitializePotentialDragTrigger_OnInitializePotentialDragAsync_mF69D7A921F0EC522DB7774D04046E6995F21E8F3 (void);
// 0x0000033A Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncInitializePotentialDragTrigger::OnInitializePotentialDragAsync(System.Threading.CancellationToken)
extern void AsyncInitializePotentialDragTrigger_OnInitializePotentialDragAsync_mFBA18411611F1372C9F0C01806115376E2C90E2E (void);
// 0x0000033B System.Void Cysharp.Threading.Tasks.Triggers.AsyncInitializePotentialDragTrigger::.ctor()
extern void AsyncInitializePotentialDragTrigger__ctor_m676286872B5182AA75E33E28E4A88D035DC9A1FC (void);
// 0x0000033C Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.AxisEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnMoveHandler::OnMoveAsync()
// 0x0000033D System.Void Cysharp.Threading.Tasks.Triggers.AsyncMoveTrigger::UnityEngine.EventSystems.IMoveHandler.OnMove(UnityEngine.EventSystems.AxisEventData)
extern void AsyncMoveTrigger_UnityEngine_EventSystems_IMoveHandler_OnMove_m516D96C8A670D55D2D8FDD6303B50ADF032A119B (void);
// 0x0000033E Cysharp.Threading.Tasks.Triggers.IAsyncOnMoveHandler Cysharp.Threading.Tasks.Triggers.AsyncMoveTrigger::GetOnMoveAsyncHandler()
extern void AsyncMoveTrigger_GetOnMoveAsyncHandler_m5BC71E2150066D7006B2948E8FBCB56DB8B37E77 (void);
// 0x0000033F Cysharp.Threading.Tasks.Triggers.IAsyncOnMoveHandler Cysharp.Threading.Tasks.Triggers.AsyncMoveTrigger::GetOnMoveAsyncHandler(System.Threading.CancellationToken)
extern void AsyncMoveTrigger_GetOnMoveAsyncHandler_m6DC2AAFFF4445CE9E3E86F55F64D8815648C17E9 (void);
// 0x00000340 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.AxisEventData> Cysharp.Threading.Tasks.Triggers.AsyncMoveTrigger::OnMoveAsync()
extern void AsyncMoveTrigger_OnMoveAsync_mEE3A3FD0434FC0CD1B0513BC836B945125955D32 (void);
// 0x00000341 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.AxisEventData> Cysharp.Threading.Tasks.Triggers.AsyncMoveTrigger::OnMoveAsync(System.Threading.CancellationToken)
extern void AsyncMoveTrigger_OnMoveAsync_m0D6E48C390DFFEE8754BAD04FD4AFC00DE152143 (void);
// 0x00000342 System.Void Cysharp.Threading.Tasks.Triggers.AsyncMoveTrigger::.ctor()
extern void AsyncMoveTrigger__ctor_m58A79FA8D0F55AB1155495110D302D31B750E634 (void);
// 0x00000343 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerClickHandler::OnPointerClickAsync()
// 0x00000344 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerClickTrigger::UnityEngine.EventSystems.IPointerClickHandler.OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void AsyncPointerClickTrigger_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_mF17716B26BBD8617FB2FB108EE482578330938C8 (void);
// 0x00000345 Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerClickHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerClickTrigger::GetOnPointerClickAsyncHandler()
extern void AsyncPointerClickTrigger_GetOnPointerClickAsyncHandler_m36796E8FFD89894A699F5D98CA2449F33C827114 (void);
// 0x00000346 Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerClickHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerClickTrigger::GetOnPointerClickAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPointerClickTrigger_GetOnPointerClickAsyncHandler_m5095467F79409EAD4064E465B0816CBCA09DEB07 (void);
// 0x00000347 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerClickTrigger::OnPointerClickAsync()
extern void AsyncPointerClickTrigger_OnPointerClickAsync_mA996A63CFE77031A3732902C348A89E49CA43C46 (void);
// 0x00000348 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerClickTrigger::OnPointerClickAsync(System.Threading.CancellationToken)
extern void AsyncPointerClickTrigger_OnPointerClickAsync_m32A09585E6928CD4B7C512116925616DC23DBB8E (void);
// 0x00000349 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerClickTrigger::.ctor()
extern void AsyncPointerClickTrigger__ctor_mB539AB18F317405415F99977FCCA9B4E8597A277 (void);
// 0x0000034A Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerDownHandler::OnPointerDownAsync()
// 0x0000034B System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerDownTrigger::UnityEngine.EventSystems.IPointerDownHandler.OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void AsyncPointerDownTrigger_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_m0F599F3ECE9C5BA039F0C7EB1705847E4AC96543 (void);
// 0x0000034C Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerDownHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerDownTrigger::GetOnPointerDownAsyncHandler()
extern void AsyncPointerDownTrigger_GetOnPointerDownAsyncHandler_mF4336BB9D8556120CC40302EADA21BB1648ADD2E (void);
// 0x0000034D Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerDownHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerDownTrigger::GetOnPointerDownAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPointerDownTrigger_GetOnPointerDownAsyncHandler_mAA363B4E92D9C606707459D7945B3C3A552C7500 (void);
// 0x0000034E Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerDownTrigger::OnPointerDownAsync()
extern void AsyncPointerDownTrigger_OnPointerDownAsync_m109E0B4B8404FC307B05F5438BE12C2C314AFA9F (void);
// 0x0000034F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerDownTrigger::OnPointerDownAsync(System.Threading.CancellationToken)
extern void AsyncPointerDownTrigger_OnPointerDownAsync_m0A91C9504F28835C272DA5230708EDE8193952A1 (void);
// 0x00000350 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerDownTrigger::.ctor()
extern void AsyncPointerDownTrigger__ctor_mCEC9E22FABB448746B967619E9B92CC5F5C0F753 (void);
// 0x00000351 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerEnterHandler::OnPointerEnterAsync()
// 0x00000352 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerEnterTrigger::UnityEngine.EventSystems.IPointerEnterHandler.OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void AsyncPointerEnterTrigger_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m78076152F21226339D9EA6D951970DFE7F4A5262 (void);
// 0x00000353 Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerEnterTrigger::GetOnPointerEnterAsyncHandler()
extern void AsyncPointerEnterTrigger_GetOnPointerEnterAsyncHandler_m647D420223B53D50706C1A74F8F57296D50FC62D (void);
// 0x00000354 Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerEnterHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerEnterTrigger::GetOnPointerEnterAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPointerEnterTrigger_GetOnPointerEnterAsyncHandler_mF7D0C87A38E06C03126C457B4D7CC57AEFFC541F (void);
// 0x00000355 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerEnterTrigger::OnPointerEnterAsync()
extern void AsyncPointerEnterTrigger_OnPointerEnterAsync_m11D56027250B34852209A9F70279A1F22BB26D09 (void);
// 0x00000356 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerEnterTrigger::OnPointerEnterAsync(System.Threading.CancellationToken)
extern void AsyncPointerEnterTrigger_OnPointerEnterAsync_mE591B254C8BD99EA4F6A980397DCB49C45DDB4B3 (void);
// 0x00000357 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerEnterTrigger::.ctor()
extern void AsyncPointerEnterTrigger__ctor_m2C22E7D5F605E1A691A2AE2D845385BB70CDDA9E (void);
// 0x00000358 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerExitHandler::OnPointerExitAsync()
// 0x00000359 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerExitTrigger::UnityEngine.EventSystems.IPointerExitHandler.OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void AsyncPointerExitTrigger_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m41EB252FA7F5EF0A0F388277F84F55ECF8DC6D59 (void);
// 0x0000035A Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerExitHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerExitTrigger::GetOnPointerExitAsyncHandler()
extern void AsyncPointerExitTrigger_GetOnPointerExitAsyncHandler_mD3B46A4AF5FBAFB5F006E29ACADE05B79B70DB44 (void);
// 0x0000035B Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerExitHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerExitTrigger::GetOnPointerExitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPointerExitTrigger_GetOnPointerExitAsyncHandler_m7F0F68056AA10801869346998C9EDA1BDDA0752F (void);
// 0x0000035C Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerExitTrigger::OnPointerExitAsync()
extern void AsyncPointerExitTrigger_OnPointerExitAsync_mB7F5B14299612EE468609618262314BAC4C7E064 (void);
// 0x0000035D Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerExitTrigger::OnPointerExitAsync(System.Threading.CancellationToken)
extern void AsyncPointerExitTrigger_OnPointerExitAsync_mB2C4919B58D410CAB8D61723511D299BD9248DDC (void);
// 0x0000035E System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerExitTrigger::.ctor()
extern void AsyncPointerExitTrigger__ctor_mBE66620F59FECD260E02BC5F09CB87EAC068B379 (void);
// 0x0000035F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerUpHandler::OnPointerUpAsync()
// 0x00000360 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerUpTrigger::UnityEngine.EventSystems.IPointerUpHandler.OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void AsyncPointerUpTrigger_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m527A125F43A676CEE8F515D51E8B5FFBF6A55B08 (void);
// 0x00000361 Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerUpHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerUpTrigger::GetOnPointerUpAsyncHandler()
extern void AsyncPointerUpTrigger_GetOnPointerUpAsyncHandler_m2DD93E63657198525CB4EE01A459722C3B5970AD (void);
// 0x00000362 Cysharp.Threading.Tasks.Triggers.IAsyncOnPointerUpHandler Cysharp.Threading.Tasks.Triggers.AsyncPointerUpTrigger::GetOnPointerUpAsyncHandler(System.Threading.CancellationToken)
extern void AsyncPointerUpTrigger_GetOnPointerUpAsyncHandler_mD827DF3DA91E33B4EE0F1457229733F2F17D2AEF (void);
// 0x00000363 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerUpTrigger::OnPointerUpAsync()
extern void AsyncPointerUpTrigger_OnPointerUpAsync_mFD18098C949BB616B7A816C347F6ECFBE02CFC70 (void);
// 0x00000364 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncPointerUpTrigger::OnPointerUpAsync(System.Threading.CancellationToken)
extern void AsyncPointerUpTrigger_OnPointerUpAsync_mA0079C88A2F824E854924A45B48BA43594BBA92E (void);
// 0x00000365 System.Void Cysharp.Threading.Tasks.Triggers.AsyncPointerUpTrigger::.ctor()
extern void AsyncPointerUpTrigger__ctor_m84BF6F4D45F028D589A11C0893361DA80196C9CD (void);
// 0x00000366 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnScrollHandler::OnScrollAsync()
// 0x00000367 System.Void Cysharp.Threading.Tasks.Triggers.AsyncScrollTrigger::UnityEngine.EventSystems.IScrollHandler.OnScroll(UnityEngine.EventSystems.PointerEventData)
extern void AsyncScrollTrigger_UnityEngine_EventSystems_IScrollHandler_OnScroll_m3B383490E6DA907F1C6342549B7A6D4F79CDA5F9 (void);
// 0x00000368 Cysharp.Threading.Tasks.Triggers.IAsyncOnScrollHandler Cysharp.Threading.Tasks.Triggers.AsyncScrollTrigger::GetOnScrollAsyncHandler()
extern void AsyncScrollTrigger_GetOnScrollAsyncHandler_mE25C1CE7F3ED17FC90394978D9861AD79D5B0F6C (void);
// 0x00000369 Cysharp.Threading.Tasks.Triggers.IAsyncOnScrollHandler Cysharp.Threading.Tasks.Triggers.AsyncScrollTrigger::GetOnScrollAsyncHandler(System.Threading.CancellationToken)
extern void AsyncScrollTrigger_GetOnScrollAsyncHandler_m2B1C0FDD5B265188DC96B563249EF8F2BBCB27E0 (void);
// 0x0000036A Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncScrollTrigger::OnScrollAsync()
extern void AsyncScrollTrigger_OnScrollAsync_mA4DF287320D375BFAB63B6FBD87929BFE4B21293 (void);
// 0x0000036B Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.PointerEventData> Cysharp.Threading.Tasks.Triggers.AsyncScrollTrigger::OnScrollAsync(System.Threading.CancellationToken)
extern void AsyncScrollTrigger_OnScrollAsync_mF7E99ECD43DF422B6367577B8AA2EE14ED8933DE (void);
// 0x0000036C System.Void Cysharp.Threading.Tasks.Triggers.AsyncScrollTrigger::.ctor()
extern void AsyncScrollTrigger__ctor_m982A622555EC0A7C55D4F871B63BD72F905994C7 (void);
// 0x0000036D Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnSelectHandler::OnSelectAsync()
// 0x0000036E System.Void Cysharp.Threading.Tasks.Triggers.AsyncSelectTrigger::UnityEngine.EventSystems.ISelectHandler.OnSelect(UnityEngine.EventSystems.BaseEventData)
extern void AsyncSelectTrigger_UnityEngine_EventSystems_ISelectHandler_OnSelect_m37F162B540ADEDFD827152E7EC623FB7094B1CC6 (void);
// 0x0000036F Cysharp.Threading.Tasks.Triggers.IAsyncOnSelectHandler Cysharp.Threading.Tasks.Triggers.AsyncSelectTrigger::GetOnSelectAsyncHandler()
extern void AsyncSelectTrigger_GetOnSelectAsyncHandler_mB655A93A87E8388D649CD1CCC7E7273A69E90AB0 (void);
// 0x00000370 Cysharp.Threading.Tasks.Triggers.IAsyncOnSelectHandler Cysharp.Threading.Tasks.Triggers.AsyncSelectTrigger::GetOnSelectAsyncHandler(System.Threading.CancellationToken)
extern void AsyncSelectTrigger_GetOnSelectAsyncHandler_mBDDF0332CEA42C3BF8AC26F037471CB78465B112 (void);
// 0x00000371 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncSelectTrigger::OnSelectAsync()
extern void AsyncSelectTrigger_OnSelectAsync_m48A6985C36A7CDDEA1E444E0A6B3BA32290DFEDB (void);
// 0x00000372 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncSelectTrigger::OnSelectAsync(System.Threading.CancellationToken)
extern void AsyncSelectTrigger_OnSelectAsync_m609638429AD51726597BA2CD806F0C0FE680927F (void);
// 0x00000373 System.Void Cysharp.Threading.Tasks.Triggers.AsyncSelectTrigger::.ctor()
extern void AsyncSelectTrigger__ctor_mD3476EF0DC05B542C78718CD836835880EEC2FB1 (void);
// 0x00000374 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnSubmitHandler::OnSubmitAsync()
// 0x00000375 System.Void Cysharp.Threading.Tasks.Triggers.AsyncSubmitTrigger::UnityEngine.EventSystems.ISubmitHandler.OnSubmit(UnityEngine.EventSystems.BaseEventData)
extern void AsyncSubmitTrigger_UnityEngine_EventSystems_ISubmitHandler_OnSubmit_m85B96C560936785E769ADAC133E336E60F0A8B56 (void);
// 0x00000376 Cysharp.Threading.Tasks.Triggers.IAsyncOnSubmitHandler Cysharp.Threading.Tasks.Triggers.AsyncSubmitTrigger::GetOnSubmitAsyncHandler()
extern void AsyncSubmitTrigger_GetOnSubmitAsyncHandler_m33816F843C417A716492E1C76E074C2BAE525542 (void);
// 0x00000377 Cysharp.Threading.Tasks.Triggers.IAsyncOnSubmitHandler Cysharp.Threading.Tasks.Triggers.AsyncSubmitTrigger::GetOnSubmitAsyncHandler(System.Threading.CancellationToken)
extern void AsyncSubmitTrigger_GetOnSubmitAsyncHandler_mF8897F1861D7F8C51699FEBCB3BDB1B9B2952C4F (void);
// 0x00000378 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncSubmitTrigger::OnSubmitAsync()
extern void AsyncSubmitTrigger_OnSubmitAsync_m925E417A2AF84E890FABFBA07F3BF09B7378289E (void);
// 0x00000379 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncSubmitTrigger::OnSubmitAsync(System.Threading.CancellationToken)
extern void AsyncSubmitTrigger_OnSubmitAsync_mDCE0134DD38203DFC0E7C92949E50A720CA1C99A (void);
// 0x0000037A System.Void Cysharp.Threading.Tasks.Triggers.AsyncSubmitTrigger::.ctor()
extern void AsyncSubmitTrigger__ctor_m1C92209AA130A99C9ACE4BEA8F2DC64304B34BAB (void);
// 0x0000037B Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.IAsyncOnUpdateSelectedHandler::OnUpdateSelectedAsync()
// 0x0000037C System.Void Cysharp.Threading.Tasks.Triggers.AsyncUpdateSelectedTrigger::UnityEngine.EventSystems.IUpdateSelectedHandler.OnUpdateSelected(UnityEngine.EventSystems.BaseEventData)
extern void AsyncUpdateSelectedTrigger_UnityEngine_EventSystems_IUpdateSelectedHandler_OnUpdateSelected_m1B9581CFB253871C2102B053B682278BA8540687 (void);
// 0x0000037D Cysharp.Threading.Tasks.Triggers.IAsyncOnUpdateSelectedHandler Cysharp.Threading.Tasks.Triggers.AsyncUpdateSelectedTrigger::GetOnUpdateSelectedAsyncHandler()
extern void AsyncUpdateSelectedTrigger_GetOnUpdateSelectedAsyncHandler_mB0F6D83BC172861063492980489E0B7EB89ADA98 (void);
// 0x0000037E Cysharp.Threading.Tasks.Triggers.IAsyncOnUpdateSelectedHandler Cysharp.Threading.Tasks.Triggers.AsyncUpdateSelectedTrigger::GetOnUpdateSelectedAsyncHandler(System.Threading.CancellationToken)
extern void AsyncUpdateSelectedTrigger_GetOnUpdateSelectedAsyncHandler_m1043A7AA796125EFE5CAA839EB9B41A7CF3F8D03 (void);
// 0x0000037F Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncUpdateSelectedTrigger::OnUpdateSelectedAsync()
extern void AsyncUpdateSelectedTrigger_OnUpdateSelectedAsync_m494136ADA379A17083E816B1A8AF5B274F67D80B (void);
// 0x00000380 Cysharp.Threading.Tasks.UniTask`1<UnityEngine.EventSystems.BaseEventData> Cysharp.Threading.Tasks.Triggers.AsyncUpdateSelectedTrigger::OnUpdateSelectedAsync(System.Threading.CancellationToken)
extern void AsyncUpdateSelectedTrigger_OnUpdateSelectedAsync_mD71A864AA8C64A4478FAFE216074A0CBCC4A5B18 (void);
// 0x00000381 System.Void Cysharp.Threading.Tasks.Triggers.AsyncUpdateSelectedTrigger::.ctor()
extern void AsyncUpdateSelectedTrigger__ctor_mECD611573F1F10CA33DAAC3DAB6903D54171C471 (void);
// 0x00000382 System.Void Cysharp.Threading.Tasks.Internal.ContinuationQueue::.ctor(Cysharp.Threading.Tasks.PlayerLoopTiming)
extern void ContinuationQueue__ctor_mC79FF88E0804F6D27B1CFC356DAD61615BF7A24E (void);
// 0x00000383 System.Void Cysharp.Threading.Tasks.Internal.ContinuationQueue::Enqueue(System.Action)
extern void ContinuationQueue_Enqueue_m177514E92921CC3865F3AF3091D7E0903B235E50 (void);
// 0x00000384 System.Void Cysharp.Threading.Tasks.Internal.ContinuationQueue::Run()
extern void ContinuationQueue_Run_m6E60144908373A67C66307A56F8C0D5A612EA764 (void);
// 0x00000385 System.Void Cysharp.Threading.Tasks.Internal.ContinuationQueue::RunCore()
extern void ContinuationQueue_RunCore_mA8B47350621EF411C87B9BF4255A6B608713CE65 (void);
// 0x00000386 System.Void Cysharp.Threading.Tasks.Internal.MinimumQueue`1::.ctor(System.Int32)
// 0x00000387 System.Int32 Cysharp.Threading.Tasks.Internal.MinimumQueue`1::get_Count()
// 0x00000388 System.Void Cysharp.Threading.Tasks.Internal.MinimumQueue`1::Enqueue(T)
// 0x00000389 T Cysharp.Threading.Tasks.Internal.MinimumQueue`1::Dequeue()
// 0x0000038A System.Void Cysharp.Threading.Tasks.Internal.MinimumQueue`1::Grow()
// 0x0000038B System.Void Cysharp.Threading.Tasks.Internal.MinimumQueue`1::SetCapacity(System.Int32)
// 0x0000038C System.Void Cysharp.Threading.Tasks.Internal.MinimumQueue`1::MoveNext(System.Int32&)
// 0x0000038D System.Void Cysharp.Threading.Tasks.Internal.MinimumQueue`1::ThrowForEmptyQueue()
// 0x0000038E System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner::.ctor(Cysharp.Threading.Tasks.PlayerLoopTiming)
extern void PlayerLoopRunner__ctor_mC2903BD5D95FBD25EBAB8A94B8F44CEF43031B80 (void);
// 0x0000038F System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner::AddAction(Cysharp.Threading.Tasks.IPlayerLoopItem)
extern void PlayerLoopRunner_AddAction_mEEC35DD90D8295CE01C50420E7EF39B640AF9682 (void);
// 0x00000390 System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner::Run()
extern void PlayerLoopRunner_Run_mE0A8E6A8A7C3F72DF1B45A128901018DB6743B38 (void);
// 0x00000391 System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner::RunCore()
extern void PlayerLoopRunner_RunCore_m6C97C81CE05C65E05140C6F3FA67F4ACCB908BC1 (void);
// 0x00000392 System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner/<>c::.cctor()
extern void U3CU3Ec__cctor_m31F71639A2C525704C2F5174479E8050889AEB8D (void);
// 0x00000393 System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner/<>c::.ctor()
extern void U3CU3Ec__ctor_mB2A7388A955AD37EA26AF64BBB35F310CA408F14 (void);
// 0x00000394 System.Void Cysharp.Threading.Tasks.Internal.PlayerLoopRunner/<>c::<.ctor>b__9_0(System.Exception)
extern void U3CU3Ec_U3C_ctorU3Eb__9_0_m491AE613FAF09E7739F0BF7D5CC44F395AF8B523 (void);
// 0x00000395 Cysharp.Threading.Tasks.Internal.StateTuple`1<T1> Cysharp.Threading.Tasks.Internal.StateTuple::Create(T1)
// 0x00000396 Cysharp.Threading.Tasks.Internal.StateTuple`2<T1,T2> Cysharp.Threading.Tasks.Internal.StateTuple::Create(T1,T2)
// 0x00000397 System.Void Cysharp.Threading.Tasks.Internal.StateTuple`1::Dispose()
// 0x00000398 System.Void Cysharp.Threading.Tasks.Internal.StateTuple`1::.ctor()
// 0x00000399 Cysharp.Threading.Tasks.Internal.StateTuple`1<T1> Cysharp.Threading.Tasks.Internal.StatePool`1::Create(T1)
// 0x0000039A System.Void Cysharp.Threading.Tasks.Internal.StatePool`1::Return(Cysharp.Threading.Tasks.Internal.StateTuple`1<T1>)
// 0x0000039B System.Void Cysharp.Threading.Tasks.Internal.StatePool`1::.cctor()
// 0x0000039C System.Void Cysharp.Threading.Tasks.Internal.StateTuple`2::Dispose()
// 0x0000039D System.Void Cysharp.Threading.Tasks.Internal.StateTuple`2::.ctor()
// 0x0000039E Cysharp.Threading.Tasks.Internal.StateTuple`2<T1,T2> Cysharp.Threading.Tasks.Internal.StatePool`2::Create(T1,T2)
// 0x0000039F System.Void Cysharp.Threading.Tasks.Internal.StatePool`2::Return(Cysharp.Threading.Tasks.Internal.StateTuple`2<T1,T2>)
// 0x000003A0 System.Void Cysharp.Threading.Tasks.Internal.StatePool`2::.cctor()
// 0x000003A1 Cysharp.Threading.Tasks.Internal.ValueStopwatch Cysharp.Threading.Tasks.Internal.ValueStopwatch::StartNew()
extern void ValueStopwatch_StartNew_m398D9548C1C9C3C505294ABB321A70B7361462E4 (void);
// 0x000003A2 System.Void Cysharp.Threading.Tasks.Internal.ValueStopwatch::.ctor(System.Int64)
extern void ValueStopwatch__ctor_mD72CC04E4E48BADF6C4A8DD48DB9FC1A1AC42EF5 (void);
// 0x000003A3 System.Boolean Cysharp.Threading.Tasks.Internal.ValueStopwatch::get_IsInvalid()
extern void ValueStopwatch_get_IsInvalid_m0A3D180006FB7784C23036BA26772A1D883E895A (void);
// 0x000003A4 System.Int64 Cysharp.Threading.Tasks.Internal.ValueStopwatch::get_ElapsedTicks()
extern void ValueStopwatch_get_ElapsedTicks_m191300A5FC932D5C0BAFE98BF7BE560DFFB6EA10 (void);
// 0x000003A5 System.Void Cysharp.Threading.Tasks.Internal.ValueStopwatch::.cctor()
extern void ValueStopwatch__cctor_mA0E464EE81081955FF8B29534A729335E5628FC0 (void);
// 0x000003A6 System.Void Cysharp.Threading.Tasks.Internal.SingleAssignmentDisposable::set_Disposable(System.IDisposable)
extern void SingleAssignmentDisposable_set_Disposable_mCC2C1EDD0B6BAB59C4D03B133D0E227DB232BC14 (void);
// 0x000003A7 System.Void Cysharp.Threading.Tasks.Internal.SingleAssignmentDisposable::Dispose()
extern void SingleAssignmentDisposable_Dispose_m8E24570E1565FC9AD333B75DA75FE503CC64BF10 (void);
// 0x000003A8 System.Void Cysharp.Threading.Tasks.Internal.SingleAssignmentDisposable::.ctor()
extern void SingleAssignmentDisposable__ctor_m0B130CC178E9F7EC8AE8EB0CB92DD1AA79BA1BAD (void);
// 0x000003A9 Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::Create()
extern void AsyncUniTaskMethodBuilder_Create_m5B36880F3EC6CACA4359D94D53B8DC633AA8C018 (void);
// 0x000003AA Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::get_Task()
extern void AsyncUniTaskMethodBuilder_get_Task_m2F98008CBCDF840A80869042FF5620775C3E3ED7 (void);
// 0x000003AB System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::SetException(System.Exception)
extern void AsyncUniTaskMethodBuilder_SetException_m7153CFFA0C20E8036CC35A0ECDD57649B1807A6C (void);
// 0x000003AC System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::SetResult()
extern void AsyncUniTaskMethodBuilder_SetResult_m3018C0048802DBD765F7796F44FED9C706E988F6 (void);
// 0x000003AD System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::AwaitUnsafeOnCompleted(TAwaiter&,TStateMachine&)
// 0x000003AE System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::Start(TStateMachine&)
// 0x000003AF System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void AsyncUniTaskMethodBuilder_SetStateMachine_mA8DEE00670CB37DB2DA98D16AF514F9B4B19B141 (void);
// 0x000003B0 Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1<T> Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::Create()
// 0x000003B1 Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::get_Task()
// 0x000003B2 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::SetException(System.Exception)
// 0x000003B3 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::SetResult(T)
// 0x000003B4 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::AwaitUnsafeOnCompleted(TAwaiter&,TStateMachine&)
// 0x000003B5 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::Start(TStateMachine&)
// 0x000003B6 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTaskMethodBuilder`1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x000003B7 System.Action Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise::get_MoveNext()
// 0x000003B8 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise::get_Task()
// 0x000003B9 System.Void Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise::SetResult()
// 0x000003BA System.Void Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise::SetException(System.Exception)
// 0x000003BB System.Action Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise`1::get_MoveNext()
// 0x000003BC Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise`1::get_Task()
// 0x000003BD System.Void Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise`1::SetResult(T)
// 0x000003BE System.Void Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise`1::SetException(System.Exception)
// 0x000003BF System.Action Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::get_MoveNext()
// 0x000003C0 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::.ctor()
// 0x000003C1 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::SetStateMachine(TStateMachine&,Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise&)
// 0x000003C2 Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1<TStateMachine>& Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::get_NextNode()
// 0x000003C3 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::.cctor()
// 0x000003C4 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::Return()
// 0x000003C5 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::Run()
// 0x000003C6 Cysharp.Threading.Tasks.UniTask Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::get_Task()
// 0x000003C7 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::SetResult()
// 0x000003C8 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::SetException(System.Exception)
// 0x000003C9 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::GetResult(System.Int16)
// 0x000003CA Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::GetStatus(System.Int16)
// 0x000003CB Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::UnsafeGetStatus()
// 0x000003CC System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x000003CD System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1/<>c::.cctor()
// 0x000003CE System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1/<>c::.ctor()
// 0x000003CF System.Int32 Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`1/<>c::<.cctor>b__12_0()
// 0x000003D0 System.Action Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::get_MoveNext()
// 0x000003D1 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::.ctor()
// 0x000003D2 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::SetStateMachine(TStateMachine&,Cysharp.Threading.Tasks.CompilerServices.IStateMachineRunnerPromise`1<T>&)
// 0x000003D3 Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2<TStateMachine,T>& Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::get_NextNode()
// 0x000003D4 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::.cctor()
// 0x000003D5 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::Return()
// 0x000003D6 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::Run()
// 0x000003D7 Cysharp.Threading.Tasks.UniTask`1<T> Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::get_Task()
// 0x000003D8 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::SetResult(T)
// 0x000003D9 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::SetException(System.Exception)
// 0x000003DA T Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::GetResult(System.Int16)
// 0x000003DB System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::Cysharp.Threading.Tasks.IUniTaskSource.GetResult(System.Int16)
// 0x000003DC Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::GetStatus(System.Int16)
// 0x000003DD Cysharp.Threading.Tasks.UniTaskStatus Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::UnsafeGetStatus()
// 0x000003DE System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2::OnCompleted(System.Action`1<System.Object>,System.Object,System.Int16)
// 0x000003DF System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2/<>c::.cctor()
// 0x000003E0 System.Void Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2/<>c::.ctor()
// 0x000003E1 System.Int32 Cysharp.Threading.Tasks.CompilerServices.AsyncUniTask`2/<>c::<.cctor>b__12_0()
static Il2CppMethodPointer s_methodPointers[993] = 
{
	AsyncMethodBuilderAttribute__ctor_mE063A71B5304DD0DD014F095271112AC8CBB9CDB,
	AsyncUnit_GetHashCode_m313965D898D515FE5DF5464ED8C0C9EF64B9F0C6,
	AsyncUnit_Equals_m7F9CC10F7DDF7211EAD2D0A56B413C82156C2E07,
	AsyncUnit_ToString_m3BB12D2C87E38E4D5B1D1A6F3BD3EEC6F2A6488F,
	CancellationTokenExtensions_Callback_mAAAA5DC087F3C99992626783AC1D13515FE0A51C,
	CancellationTokenExtensions_RegisterWithoutCaptureExecutionContext_mFA67F8204DAD04F3F4E4CC416B5E8055A343A08F,
	CancellationTokenExtensions_DisposeCallback_mA0FA6FDDC70BC2CDBA14B6B20D75750C1A1BB97A,
	CancellationTokenExtensions__cctor_m9D16193F0DF4E5D40308F660E4185900D6C8FB02,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UniTaskStatusExtensions_IsCompleted_m7A9E3A6930C2EB3ED293F11782F2E0A16FAF8E71,
	MoveNextSource_GetResult_m7D5CA709DB1E8E3A326C535CFEE3C4B193984509,
	MoveNextSource_GetStatus_m6B5BCBA538463914A0763C6D07E7BF30052535E0,
	MoveNextSource_OnCompleted_m5C664C312F657C622D45B340327F09E023B59E98,
	MoveNextSource_UnsafeGetStatus_mDDDE1D2E5038488B1F28655FDE2788B8AAEF27B5,
	MoveNextSource_Cysharp_Threading_Tasks_IUniTaskSource_GetResult_m5E9EA3CC0CA5AB3F3CE8CFB681997415D0282081,
	MoveNextSource__ctor_mF5C753B441B348A43DE69243830C74E2671D770A,
	NULL,
	PlayerLoopHelper_get_UnitySynchronizationContext_m6702A1ECB0D27155EDFEDFF97585FCE0BFFBB9A2,
	PlayerLoopHelper_get_MainThreadId_m2FB5751481BE90F5CF3A74EBD9EDA1B5196148DB,
	PlayerLoopHelper_get_ApplicationDataPath_m9FEB5363657F8AD39E87CADC5C130661FA83F9C7,
	PlayerLoopHelper_get_IsMainThread_m07256518E19B3A4EDE421D301F21B88219DB28E1,
	PlayerLoopHelper_get_IsEditorApplicationQuitting_m41F5E63B94BAA7BC025829359D13C0101BD74551,
	PlayerLoopHelper_set_IsEditorApplicationQuitting_m8FE072C71C6415A874DC4E074DFA91B8E81C1630,
	PlayerLoopHelper_InsertRunner_m5EEF89AF5BE51154A325E7E9E2A2AF23B1C00739,
	PlayerLoopHelper_RemoveRunner_m6FF4390C00AEED28ACB66E5EB7B123EC5098F2D3,
	PlayerLoopHelper_InsertUniTaskSynchronizationContext_mAA2E6C7D43E83D57DA53C399A002208F372DBA17,
	PlayerLoopHelper_Init_mD46E29166AB848484B0BB715017D5CC9144E51E1,
	PlayerLoopHelper_FindLoopSystemIndex_m4106AF197FEFB5C4CFE4810CF41FC6445D45103C,
	PlayerLoopHelper_InsertLoop_m74221E1763E303D938F604BEBE2661BB030B6A4D,
	PlayerLoopHelper_Initialize_mAA02BE296392A43E3EF3F7A59A385085CB6C4D92,
	PlayerLoopHelper_AddAction_mF282616C4AF98C738AAC104B74ECCD8B7C1AB429,
	PlayerLoopHelper_ThrowInvalidLoopTiming_mC327106A4D15F456C694A0B1FF2C6D3217EEC125,
	PlayerLoopHelper_AddContinuation_m8F5569426F16FE3C4955AA4E10C6059EE9DC011C,
	PlayerLoopHelper_DumpCurrentPlayerLoop_mD2D86A1A43A50F07E46C4FA1F985CCEC68262B21,
	PlayerLoopHelper_IsInjectedUniTaskPlayerLoop_m4B9629CAA7CC5DCEFFC804511732F7244A78DD6F,
	PlayerLoopHelper__cctor_mEF9C6111E858A0E4413C3DCD2D60388780176516,
	U3CU3Ec__DisplayClass20_0__ctor_m5ACAD2E0947A0B8F7D33044A3600C9408B450EDA,
	U3CU3Ec__DisplayClass20_0_U3CRemoveRunnerU3Eb__0_m0301B2D473647BEF6EFAB4294494120C1BC2334D,
	U3CU3Ec__cctor_m98F2D6A1D5F93BE5E662BEFD3A504F022BCC45FF,
	U3CU3Ec__ctor_m1E750A7D82E38074DF51132188C7903529BD5731,
	U3CU3Ec_U3CInsertUniTaskSynchronizationContextU3Eb__21_0_m79CCFF31F329420228E4238BEBB53471A2D5FEF6,
	U3CU3Ec_U3CInsertUniTaskSynchronizationContextU3Eb__21_1_mB363C6A0536592BDB61D24C39A100B168CB0C306,
	U3CU3Ec_U3CInsertUniTaskSynchronizationContextU3Eb__21_2_m4B18558D5EECF740CE7A17C98C2248929EB7EE5A,
	TaskPool__cctor_m3B5AE687814E0DDB21DE2BD209E841660819F58F,
	TaskPool_RegisterSizeGetter_m43ED66B9FB98E95BB57695F7BF32B2C43A45C99F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UniTask_Yield_m9D90B9C5DAB9B3FB34E3C7D027C891E8A2B5765E,
	UniTask_Delay_mEAF888C9FC626692FA569A01B9AFC320D1D7AD92,
	UniTask_Delay_m45C85FFC2C1FF43826381B27808DFAFA70B2DE4C,
	UniTask_Delay_m3D50FDD01A456066EBD0B9CD393D55092B34DA64,
	UniTask_FromException_m3A5CDCDC99E68786578B2E57CCE0E899850B3DB0,
	NULL,
	NULL,
	UniTask_FromCanceled_m0BC69BD2FB4CCDA259486447594863EC1891EAB1,
	NULL,
	UniTask_WhenAll_mC35EDD17A7A000CF85450B5361B331B2CD5DCD5B,
	UniTask__ctor_mA1D2FAF1D02391065D770F56EE3CFD028157CACD,
	UniTask_get_Status_m5CCD7973CFD636CF45F67AF23CC34349ABBAD0AD,
	UniTask_GetAwaiter_m1FE35EBA18DB40141C1025CECD4FC300E3A8C59C,
	UniTask_ToString_m4BF6DB11E21177970C4F895B04F0D3ED6673A215,
	UniTask__cctor_m45B30383FFD9CE612F1C612F450BCAEEF1146C7D,
	DelayPromise_get_NextNode_m84F340FEC343C1A6E7803817B4D01880923A79D7,
	DelayPromise__cctor_m70D49DE0BB2FE45DF4466FB79A8DAA27238EF8FD,
	DelayPromise__ctor_m86C2534E0D2671065C6A96648EF805D2835E0ECD,
	DelayPromise_Create_mF031A3A8FD045DFAEF90BDF65C15DCFF9ABAAA02,
	DelayPromise_GetResult_m10ADD62F5CA36929284A7E4E34E76C1EB7ABC64A,
	DelayPromise_GetStatus_mEDBE0E5FEBD402F3B3273726AFC4924CEE628F7B,
	DelayPromise_UnsafeGetStatus_m6867F464EBC6DDF793DAFD75A03C2CA7C4C6D756,
	DelayPromise_OnCompleted_mD22E8ABCD7E791401EEF538EBB1CD785EEDC512F,
	DelayPromise_MoveNext_mAD44B8BDF81E7483379F52C67975D842939A3AEB,
	DelayPromise_TryReturn_mF9284E286BFCF8977449417A8D1BA917C9C826AE,
	U3CU3Ec__cctor_mD32219EC581E38D75539FC5A42B7116FD2FEF6E2,
	U3CU3Ec__ctor_m1887ED2AB50224DE4EF4CF6582FCDE748D5C1630,
	U3CU3Ec_U3C_cctorU3Eb__4_0_m2D6234D973A4CF0FECA02D5B7EE3B5F1A768C926,
	DelayIgnoreTimeScalePromise_get_NextNode_mB89D3615EE78CE3AD3900B1AB7D43A80CFEC0978,
	DelayIgnoreTimeScalePromise__cctor_m27098F2A762AE7C6E07F9724A2E04983C7CA6773,
	DelayIgnoreTimeScalePromise__ctor_m1F7027E5F2F540C2B13A0A959B2004D30B20C3D7,
	DelayIgnoreTimeScalePromise_Create_m4961C20E2730FF8CA837FB95FB45BE808385C5E6,
	DelayIgnoreTimeScalePromise_GetResult_mEC798B6FFD702639DAFFB6197BB69BDDCF016053,
	DelayIgnoreTimeScalePromise_GetStatus_mB5152C3A164DD52624DDCFCF9DF3A9418C14AE83,
	DelayIgnoreTimeScalePromise_UnsafeGetStatus_mA1A740FF4EDBD217FD6277C92ADCA80CFEEC546E,
	DelayIgnoreTimeScalePromise_OnCompleted_mFFE816798DF39DB20C95F9FE2D3589BA2921693C,
	DelayIgnoreTimeScalePromise_MoveNext_mC1066D2E2223F30DC9D2890D3F9D687FC7A8D863,
	DelayIgnoreTimeScalePromise_TryReturn_m0C5CA23820326509633B9BE717266C59C39849F7,
	U3CU3Ec__cctor_m69B7EE04A226E23022C08C8D389A93149C74AC15,
	U3CU3Ec__ctor_m6856E5126E9A56584EC7358DF07551B8DAD74E8B,
	U3CU3Ec_U3C_cctorU3Eb__4_0_mC13B74D8452862A7FB4F614EA3075CA3E6AD8017,
	DelayRealtimePromise_get_NextNode_m09265DA87003AA9DC60843987CCEC274A5C8C340,
	DelayRealtimePromise__cctor_mB726D240EAAD9A21C218C43CA20692C09BBB6167,
	DelayRealtimePromise__ctor_mEF4B7AF172B64C94D6394E92C2E0EEE62DBE75E5,
	DelayRealtimePromise_Create_m7CA136381C44D0966ECE918DBF42F01D69D52FCE,
	DelayRealtimePromise_GetResult_m618BC0D60E9A1AA8A7A777B42FF9240836D1AE03,
	DelayRealtimePromise_GetStatus_mAEF59103E3D1DCD3C9DDF626D334FDE1537662E0,
	DelayRealtimePromise_UnsafeGetStatus_m9381F2AC496BEB44D54188EF2F830F491667016E,
	DelayRealtimePromise_OnCompleted_mB21E2E33020C7750B04292458E4F91107017DE68,
	DelayRealtimePromise_MoveNext_m20927B3514CA36B4F4CF87088B097A4BC3A6B516,
	DelayRealtimePromise_TryReturn_m0D940FFFA1506E7F27200CCECC4B733301D79E19,
	U3CU3Ec__cctor_mE68B8CDFBA08013BF0E1BC1DDDD3E07537F51291,
	U3CU3Ec__ctor_m7CF4C7BB8863530D2F3CCE7159C4D80C2C125204,
	U3CU3Ec_U3C_cctorU3Eb__4_0_m5CFB794D524A8FB0763DB4DC44D07AA4C723B6AD,
	NULL,
	ExceptionResultSource__ctor_m3EDA22E3AB29A3BFCAFB32EB3EF3F71572A9F763,
	ExceptionResultSource_GetResult_m2D897950212D69E5EBD76D5434352B5DB51CC555,
	ExceptionResultSource_GetStatus_m17E2C4B0161574E12EC3D6886CE17FEEA2FD4C81,
	ExceptionResultSource_UnsafeGetStatus_m148703AA3363F58FC8D95D73196C8D7DBB70506D,
	ExceptionResultSource_OnCompleted_m31BB9A8AA43CA20170C3427A119BB7916E45C0E6,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	CanceledResultSource__ctor_m77386FA032887459D5FBC0CB7C05DFF730AE99A0,
	CanceledResultSource_GetResult_m4E1F63AC3B076A2433889BD86C8BF7AA4D0ED137,
	CanceledResultSource_GetStatus_m6404471BF151EA3B4157D2F8B4DFF3CB0E3C6E5E,
	CanceledResultSource_UnsafeGetStatus_mDAAFAC3FD8E197FDA5BF4ED902FDB1D9B6EA0921,
	CanceledResultSource_OnCompleted_mDA548D8E0328E86B2A1FD4AD2D594248760400C8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	WhenAllPromise__ctor_m15F92C2AFC53966643FD3D851D140470A8EEBFDC,
	WhenAllPromise_TryInvokeContinuation_m457A2DC5D19EDBEDC48F15C85740B408F43FB8BB,
	WhenAllPromise_GetResult_m277437082708A5A1213A1C5B88DD0DF422F28717,
	WhenAllPromise_GetStatus_m631854632BB8FB4F2ED316817CAF599929D75438,
	WhenAllPromise_UnsafeGetStatus_m4E73CAB8322D3E482947310063320D0308FE6368,
	WhenAllPromise_OnCompleted_m2D3FB5AE8BA34D8E8B51CDEEC74460FB12B3837A,
	U3CU3Ec__cctor_m3941202D45EF5640744E45504357C4A147411FD3,
	U3CU3Ec__ctor_mF8A6C798E29EEF181BEDCB2923BAD58BA7D7D1D8,
	U3CU3Ec_U3C_ctorU3Eb__3_0_mBA91021837B4ED21A956C6957D976125ED974066,
	Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF,
	Awaiter_get_IsCompleted_mDCC71AAE7FA5EA2B303D3EC512177FD122C8DA49,
	Awaiter_GetResult_mFCA8BA81A9A7DA641DEFC7FC121FED87A3EC06CA,
	Awaiter_OnCompleted_m080A1DBE2E806E1CDE6931514C3A6C8DBDF2E646,
	Awaiter_UnsafeOnCompleted_m1428D2FC0145C8CABB14E4B9A15F38B6AE34F8E5,
	Awaiter_SourceOnCompleted_m71BEAA335CDD0FBD24F14F9B970B8E1B69AE01B4,
	U3CU3Ec__cctor_m902D699C440A7E7E9ECCFB515C6293403DA46A5E,
	U3CU3Ec__ctor_mC6E69A9C636DA9F58B33651F73B729A6A3CA4626,
	U3CU3Ec_U3C_cctorU3Eb__174_0_mBCF7F67BA74FEC483866CCE43266BB39E532932B,
	YieldAwaitable__ctor_mDFCED8E03E478AA784DCBAF67876091D6457590F,
	YieldAwaitable_GetAwaiter_mABCF83C041AEE78B7915625989429CF87DC28EC9,
	Awaiter__ctor_m05FC94348E722A3E503CD81CE62E02A5163A58F0,
	Awaiter_get_IsCompleted_mF92B16767983ACC6C69D07E32F866F14E3E3A830,
	Awaiter_GetResult_m5B7F9896A518DB6316AD7C1741AFA0441CA40B3F,
	Awaiter_OnCompleted_mB06C744A459C12B974DF387EB24B33B8663524F7,
	Awaiter_UnsafeOnCompleted_m7891BED0BBDFA9C626E10CD515326D3E1A859634,
	AwaiterActions_Continuation_m6C950CC84C028955999620A11573A87154D4F2B9,
	AwaiterActions__cctor_mBD78C0AA7AB86026FADD8D4A3C83939FFEF73D23,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ExceptionHolder__ctor_mACA78E10153393E338471868F382F842F20E7A5B,
	ExceptionHolder_GetException_mC917B7558EA174CC6128C89165017F793D920B09,
	ExceptionHolder_Finalize_m82D25DF7F508A43AC6F79586FA9BDACB0DAA2354,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UniTaskCompletionSourceCoreShared_CompletionSentinel_m088BF1BB17117B2A50A114B1C6C067649F5C8768,
	UniTaskCompletionSourceCoreShared__cctor_mA4160F6CE3540A353D3F9B9C1B43188523482D7B,
	AutoResetUniTaskCompletionSource_get_NextNode_mBB71FA082E5CC41E1A2D2885F4FAE6374996B29D,
	AutoResetUniTaskCompletionSource__cctor_mA2356C28420E63AE061366E9E58451116FDA5C7C,
	AutoResetUniTaskCompletionSource__ctor_mE87351839003BD8B6C9ADBE8CB4C72B4EC361996,
	AutoResetUniTaskCompletionSource_Create_mC4696C34F31CA03093424A4548E0D23999CA70D3,
	AutoResetUniTaskCompletionSource_CreateFromCanceled_m65C9559F6C548FD966D3A7BDB0C9BAA6B643C7CE,
	AutoResetUniTaskCompletionSource_TrySetCanceled_m1228999B468118393A0BE24C4F290D4D376851CA,
	AutoResetUniTaskCompletionSource_GetResult_m1B23EAD60F9C66577E6708C8FE0F1B1E60C3D12C,
	AutoResetUniTaskCompletionSource_GetStatus_m09652E851FAC788D9936902498A7F351C988D23D,
	AutoResetUniTaskCompletionSource_UnsafeGetStatus_m0E7D9961B936A86FFCBD5880459465D58E58D564,
	AutoResetUniTaskCompletionSource_OnCompleted_m234B5E91D7C0E1B6EDACE14DF65AAD38742C47FF,
	AutoResetUniTaskCompletionSource_TryReturn_mAE3E4441080A3493D20476911D735914E9F4212D,
	U3CU3Ec__cctor_m4EFFA1E2B75DFC2B46E630371E6B30B9FA0E2823,
	U3CU3Ec__ctor_mF39FE9DF703393F9CC2DA2EE4A494FA4F40B521C,
	U3CU3Ec_U3C_cctorU3Eb__4_0_mFD92DDA66D96BB26716BA5EC84DFB4F35ECB4D06,
	UniTaskCompletionSource__ctor_m252D950A297A48EE25FDFDDFE111524C6598C0E4,
	UniTaskCompletionSource_MarkHandled_m3EABFFA54BF7934490BCA86DA8F76AA023BE8144,
	UniTaskCompletionSource_get_Task_m71FDFD642F5DE334B395253D95E812AF5E191CD1,
	UniTaskCompletionSource_TrySetResult_mD0DF7B8E60DD4FE6D4FC5EE0386CE075805E12DF,
	UniTaskCompletionSource_GetResult_m39A120F5B2805CF60DF0DDDF56AD13E6FE6BDED1,
	UniTaskCompletionSource_GetStatus_mED83FEAAD02A8E3145F2DA6625390DDFFC374002,
	UniTaskCompletionSource_UnsafeGetStatus_m4C223CE67FF35004CE732D6ED295B814E3FD9E02,
	UniTaskCompletionSource_OnCompleted_mFF8DB2962201801A755B6B2EC59EFF6B3ADA4CC3,
	UniTaskCompletionSource_TrySignalCompletion_m1DF031CC4D1BC7CC43F6CF57FB5056173615B064,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UniTaskExtensions_Forget_m03A03134BA00CD83A83902AEF9352C8C0AC595A5,
	U3CU3Ec__cctor_mA285C509DEFF7FA4F804AD59B0666DC18B4F50CA,
	U3CU3Ec__ctor_m2F38CAFAB4061F35011773F751E3385C14F2D29C,
	U3CU3Ec_U3CForgetU3Eb__48_0_mA9B2D7825C681C612713C0BD5E8C9746E61BE272,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	UniTaskScheduler_InvokeUnobservedTaskException_mB859444D8764BA7B062DBACE4C79E3D1ECE71F1D,
	UniTaskScheduler_PublishUnobservedTaskException_m4932FC5EE51B367FFF07E9E2FD42D24F8951FC11,
	UniTaskScheduler__cctor_m7F4F0C3F92D11A9447999DEC1C7C9BC1C223EF77,
	UniTaskSynchronizationContext_Run_m5751075F0C4E830AF924C8375FB8BAF74D1C366A,
	UniTaskSynchronizationContext__cctor_m2353BFC284D658ED3C78E0928E5053BBAC70A3FA,
	Callback_Invoke_m380CCA09614F37D4ACF8EBFD996809A7E123E763,
	AsyncAwakeTrigger_AwakeAsync_mE54EFF0C28F9D8DBB69DEF7D3D6CB5526A363DED,
	AsyncAwakeTrigger__ctor_m8E13CC1B068B30DB5B7149BADB561E43B42FF49F,
	AsyncDestroyTrigger_get_CancellationToken_mCEFB4CCD8C0AD6DF9430C6485464BC770F0D2F66,
	AsyncDestroyTrigger_Awake_m82AFF16B342A001534AE5419AC313E0CBB46686F,
	AsyncDestroyTrigger_OnDestroy_m48F9AD89E2A9B6EECD77EA3F2DDE52D3DC775F54,
	AsyncDestroyTrigger_OnDestroyAsync_m81CBD8CC68A93CC570CED5B21FC08D31E23E18C4,
	AsyncDestroyTrigger__ctor_m49254BFE76A6173A9A48A049E688C60B1AEA004B,
	AwakeMonitor__ctor_m13FF87D0F2EC1F2650054E35566244122953EE71,
	AwakeMonitor_MoveNext_m706CCFB8C097596366C88B9E1E6C43EB6A68E81D,
	U3CU3Ec__cctor_m65B731720A82BBFE3D0846453EB0BC7CF2579047,
	U3CU3Ec__ctor_mD131D74A604773AB05426FE2A685B11E4F0F7BEE,
	U3CU3Ec_U3COnDestroyAsyncU3Eb__7_0_m75541A8873DED6274A8AC671484A6C5D01F8E6B3,
	AsyncStartTrigger_Start_m214F3532BAE50DFF9E4C47037213291A0B6AF3E7,
	AsyncStartTrigger_StartAsync_mD384D39AF89D7B2E64225EBA2960DD2D26C09738,
	AsyncStartTrigger__ctor_m37D4A0456EEE0B82F1C1801AB11A877D2B99EF8B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncFixedUpdateTrigger_FixedUpdate_m0705C59DC91B487D841D1BFA37146F1A2516E0A1,
	AsyncFixedUpdateTrigger_GetFixedUpdateAsyncHandler_m86903CC33311A715FEF2BC4160AB89C8345A4BF4,
	AsyncFixedUpdateTrigger_GetFixedUpdateAsyncHandler_m31D8686AD391D31AE4FF0BC638FE1A8057099B00,
	AsyncFixedUpdateTrigger_FixedUpdateAsync_m6C4C01EF4279E013059F6B5AF954934D68CB2636,
	AsyncFixedUpdateTrigger_FixedUpdateAsync_m4E3A45C66F2A69C1D106A87DC493FEF17EA5AD98,
	AsyncFixedUpdateTrigger__ctor_m33B852EDE6D2C4FF746BE68A19F2AEBAEC32E264,
	NULL,
	AsyncLateUpdateTrigger_LateUpdate_m908E39EAC7BCBBD8407307ADA887EFC0C6F6C56A,
	AsyncLateUpdateTrigger_GetLateUpdateAsyncHandler_m2837CBD195E62CE172348EFD9FA6481B1EEAA93D,
	AsyncLateUpdateTrigger_GetLateUpdateAsyncHandler_mE4912F3B20A7A67DDF305B0FB2E7D11EDE167D30,
	AsyncLateUpdateTrigger_LateUpdateAsync_m896F3F380B142A12A9D0EEE3687771917AF121A3,
	AsyncLateUpdateTrigger_LateUpdateAsync_m2FCE0CF5654550D65A1AF2FB0A8FF75B4F326A89,
	AsyncLateUpdateTrigger__ctor_mDE0BD5556101ACC17A073C3BEAF2E953E9713219,
	NULL,
	AsyncAnimatorIKTrigger_OnAnimatorIK_m51DFB984DEB2EC00B7AD0B1F927352E9E4CC4761,
	AsyncAnimatorIKTrigger_GetOnAnimatorIKAsyncHandler_mBE26647CBFC257096638182A6B2A7C50901DEA30,
	AsyncAnimatorIKTrigger_GetOnAnimatorIKAsyncHandler_mD305BF5EA8A99541D1D794813C3495BAC6E49491,
	AsyncAnimatorIKTrigger_OnAnimatorIKAsync_m801F08708AB20F736C6E0EDDA1A0735D16E2CDF4,
	AsyncAnimatorIKTrigger_OnAnimatorIKAsync_mA0385326ED58FFB084A5EB221BCFAC41EB465FE1,
	AsyncAnimatorIKTrigger__ctor_m2887CB5E5E1D6D132117F83E2638A432E86BC85A,
	NULL,
	AsyncAnimatorMoveTrigger_OnAnimatorMove_mE63E052A1436122456502A2F96AC1BE0EBB259D8,
	AsyncAnimatorMoveTrigger_GetOnAnimatorMoveAsyncHandler_m65DEA11547D0D0A713764F0FCEFA130224AD7C52,
	AsyncAnimatorMoveTrigger_GetOnAnimatorMoveAsyncHandler_m68CB140AF1475976C256A589EF7671A0A567936A,
	AsyncAnimatorMoveTrigger_OnAnimatorMoveAsync_mF297FE216FF3121EE6C6A09E262979D423FD8D95,
	AsyncAnimatorMoveTrigger_OnAnimatorMoveAsync_mAC0EA3D08AB9C02C3AFDC19CA969B4E09F4F3E28,
	AsyncAnimatorMoveTrigger__ctor_mB5613243B04C56F343F620976FA2B75A008D908E,
	NULL,
	AsyncApplicationFocusTrigger_OnApplicationFocus_m0BAE68C2CA6849051E404C64312D1B12ACECEF6F,
	AsyncApplicationFocusTrigger_GetOnApplicationFocusAsyncHandler_m31006D6BE29FD6659620E9E9ADE09B913B1611F8,
	AsyncApplicationFocusTrigger_GetOnApplicationFocusAsyncHandler_m302C2618C792F1248A0EE540318D105D5D9CE120,
	AsyncApplicationFocusTrigger_OnApplicationFocusAsync_m95456513A6FE134DFB845612ABB34BAFF3931E1E,
	AsyncApplicationFocusTrigger_OnApplicationFocusAsync_m52AAA8B9AD8DFEDC4DD2AAF35CC4F36E082A880B,
	AsyncApplicationFocusTrigger__ctor_mCFDCB852BDB7435E375F9B33FA71FC15D5C06A9B,
	NULL,
	AsyncApplicationPauseTrigger_OnApplicationPause_m9FDD51239C80B79018288E68089CFDC9563062C1,
	AsyncApplicationPauseTrigger_GetOnApplicationPauseAsyncHandler_m20387C844A34739BAADBAC6A94FE5AB522F548E4,
	AsyncApplicationPauseTrigger_GetOnApplicationPauseAsyncHandler_m7CCE64C34179EA13B3252CCE3F770AD2E23BF9AB,
	AsyncApplicationPauseTrigger_OnApplicationPauseAsync_m575250F184B2F1D7E2CF9F1D33B9030E06E24AEC,
	AsyncApplicationPauseTrigger_OnApplicationPauseAsync_m24FFD7A00792AEA11A898E9CEA45D8D657D59EC9,
	AsyncApplicationPauseTrigger__ctor_m6960B692443D76CDE3A416F0BDD2003F3421A52F,
	NULL,
	AsyncApplicationQuitTrigger_OnApplicationQuit_m9306723AA7B2D8E88BAE359035F4134D594C63B6,
	AsyncApplicationQuitTrigger_GetOnApplicationQuitAsyncHandler_mD8104FEF0CE5D02D1464DA6C57C71BD599CB265D,
	AsyncApplicationQuitTrigger_GetOnApplicationQuitAsyncHandler_mE34932D9EAE9999EB21B4DF29C8959CBDBFA6564,
	AsyncApplicationQuitTrigger_OnApplicationQuitAsync_m2DA58255F9D7942C2B942A60C88CCE9F12D75B0A,
	AsyncApplicationQuitTrigger_OnApplicationQuitAsync_mAC8895312602D2DFCC54CABC0EC5CB47CC8B2ED6,
	AsyncApplicationQuitTrigger__ctor_mA6072BDB37D30675FC3A09770DA4A0BA8CE379CF,
	NULL,
	AsyncAudioFilterReadTrigger_OnAudioFilterRead_m2D02FE76FB6E6DD2B2D883E0296ECED6F333E9FA,
	AsyncAudioFilterReadTrigger_GetOnAudioFilterReadAsyncHandler_m1697C83E0C00122713821C6C7B5A65EF80ADF8B9,
	AsyncAudioFilterReadTrigger_GetOnAudioFilterReadAsyncHandler_mC1F1EE4BA08E62D3E20C334E289897F9584AC2EE,
	AsyncAudioFilterReadTrigger_OnAudioFilterReadAsync_m384CAF4EEADA125FAE9AEA676BECF3BE384AE064,
	AsyncAudioFilterReadTrigger_OnAudioFilterReadAsync_mF51A00CAA04517FE68E300879120C56E539F4215,
	AsyncAudioFilterReadTrigger__ctor_mD290A53C2964CEA8E831951D54072B81C85D9069,
	NULL,
	AsyncBecameInvisibleTrigger_OnBecameInvisible_m312E75A42367D87C26CB90CAF1CBA2B8620C7A93,
	AsyncBecameInvisibleTrigger_GetOnBecameInvisibleAsyncHandler_mDBB9245CF1D12A9BA4A59209F976AB79832F5221,
	AsyncBecameInvisibleTrigger_GetOnBecameInvisibleAsyncHandler_mEB1592F42E403767884F3448FDFA548E51B3CCFA,
	AsyncBecameInvisibleTrigger_OnBecameInvisibleAsync_m2143CBBB462806055C3108802F2E10CA22EB5E99,
	AsyncBecameInvisibleTrigger_OnBecameInvisibleAsync_m04523D76A8779885CCD25F6D511269E8078CF0EE,
	AsyncBecameInvisibleTrigger__ctor_mBE1BE4360FCD4675F22ED25BE442C6A04A3344F8,
	NULL,
	AsyncBecameVisibleTrigger_OnBecameVisible_m4A68CD721D490D0D864EE50462E9431F52D4370D,
	AsyncBecameVisibleTrigger_GetOnBecameVisibleAsyncHandler_m36014D2B5B069CF5CDDD5B6F23DE94BF6039A4F2,
	AsyncBecameVisibleTrigger_GetOnBecameVisibleAsyncHandler_m96A7E4047E84F0DFB1DF8DF5EE36AAB8D4FE1B99,
	AsyncBecameVisibleTrigger_OnBecameVisibleAsync_m819B162C99891D4A126D02824BAE195D356B32E7,
	AsyncBecameVisibleTrigger_OnBecameVisibleAsync_m0F62FB59355E30022A4AC5093A7B617793F45641,
	AsyncBecameVisibleTrigger__ctor_mF96F618AFEE8652778311FADDE31E57EEB1FEEA8,
	NULL,
	AsyncBeforeTransformParentChangedTrigger_OnBeforeTransformParentChanged_m2033F9FC289E325B7DD121807132A0A1515F8D82,
	AsyncBeforeTransformParentChangedTrigger_GetOnBeforeTransformParentChangedAsyncHandler_m8DBF9763FAF3EB8BB7D4E2F135E1D882F1D474F5,
	AsyncBeforeTransformParentChangedTrigger_GetOnBeforeTransformParentChangedAsyncHandler_mB2AFF1867B9F0945D9C48CDB15A42717BCF2256C,
	AsyncBeforeTransformParentChangedTrigger_OnBeforeTransformParentChangedAsync_m826500CA7C16A438D8A0FE79B6CFB19E4D031A57,
	AsyncBeforeTransformParentChangedTrigger_OnBeforeTransformParentChangedAsync_mA7CFF5A5F194497A675AC3DC1A80316CCD206208,
	AsyncBeforeTransformParentChangedTrigger__ctor_m7AA6FBD1056C6ECA50F8F1AF738945C3AB76261D,
	NULL,
	AsyncOnCanvasGroupChangedTrigger_OnCanvasGroupChanged_m69D6880DA202080B0819B454F1434A53F932212C,
	AsyncOnCanvasGroupChangedTrigger_GetOnCanvasGroupChangedAsyncHandler_mD3B7A0C3E999EB90C8CBFE428A8460D722AA4B44,
	AsyncOnCanvasGroupChangedTrigger_GetOnCanvasGroupChangedAsyncHandler_m9CA1095290F422C953B6DEFB71D91E7D8E1CAD37,
	AsyncOnCanvasGroupChangedTrigger_OnCanvasGroupChangedAsync_m722D89D498D9DCEAD853042DEE0E0D945956B3B8,
	AsyncOnCanvasGroupChangedTrigger_OnCanvasGroupChangedAsync_mC2EB8B229349122F9CA04C2EB1BE68418CD762A3,
	AsyncOnCanvasGroupChangedTrigger__ctor_mC889A6EC2ED66CFF7072860A84035FDCFCED0D0B,
	NULL,
	AsyncCollisionEnterTrigger_OnCollisionEnter_m576EF6AAD95BE3EAA9C9A0E1EECEF23A545112D3,
	AsyncCollisionEnterTrigger_GetOnCollisionEnterAsyncHandler_m589CA4EE529DEF4CC63888EA2D2822070C932A30,
	AsyncCollisionEnterTrigger_GetOnCollisionEnterAsyncHandler_m8DC554A919AA240FF95298B6A8E747A6B9F67337,
	AsyncCollisionEnterTrigger_OnCollisionEnterAsync_mBE068228882596B9D4B43BDEC7111584C637B963,
	AsyncCollisionEnterTrigger_OnCollisionEnterAsync_m2439507C5A45010683A37BA756279414D397D09B,
	AsyncCollisionEnterTrigger__ctor_mDB403BFED7F79FE27D9E6F3C0D8261365D2A3934,
	NULL,
	AsyncCollisionEnter2DTrigger_OnCollisionEnter2D_mB56E55C4D44B6D280750B6C964F0A7BC0325F454,
	AsyncCollisionEnter2DTrigger_GetOnCollisionEnter2DAsyncHandler_m46E608BE38F043FF4283EB79E3B02C77BE786B55,
	AsyncCollisionEnter2DTrigger_GetOnCollisionEnter2DAsyncHandler_mB51C69915CFE4DB98C61ED4F04645E150562A062,
	AsyncCollisionEnter2DTrigger_OnCollisionEnter2DAsync_mBE48732F5AA51052EB12744D55F0A4C12544E018,
	AsyncCollisionEnter2DTrigger_OnCollisionEnter2DAsync_mA437C6E73A616D8C90E66E1DE5995F8B332136FF,
	AsyncCollisionEnter2DTrigger__ctor_m5EB716A5E55E52A0B9C9368BCF499C521A78B951,
	NULL,
	AsyncCollisionExitTrigger_OnCollisionExit_m83F351BC70B9E59EB7322EDF57786BADD36D7D99,
	AsyncCollisionExitTrigger_GetOnCollisionExitAsyncHandler_mC1B1B8B17C2564FA8C5FE42F5F83084118C7389E,
	AsyncCollisionExitTrigger_GetOnCollisionExitAsyncHandler_mFF5DFA6D9C6A4407398CC847E578FD5530CCE5B4,
	AsyncCollisionExitTrigger_OnCollisionExitAsync_mCE741AE8D58C54C7571777825AEFA49C26E4E802,
	AsyncCollisionExitTrigger_OnCollisionExitAsync_mE8B940E802B1093CFC376886852B71CF66B62440,
	AsyncCollisionExitTrigger__ctor_m0D9E28D141C507C5EF9494271FCE28DCF3071161,
	NULL,
	AsyncCollisionExit2DTrigger_OnCollisionExit2D_mB66ECCE2C6934F649F173A3A30C4FD8ADFF2BA92,
	AsyncCollisionExit2DTrigger_GetOnCollisionExit2DAsyncHandler_m59B0316F3273A9720B06F9CB7D7140A336411BCB,
	AsyncCollisionExit2DTrigger_GetOnCollisionExit2DAsyncHandler_m770E00F8020C4534C69CEC24E6E9845881B9ADEF,
	AsyncCollisionExit2DTrigger_OnCollisionExit2DAsync_m81AAECFA0BC02A5941688D2E66D1D30ACF37B59E,
	AsyncCollisionExit2DTrigger_OnCollisionExit2DAsync_m15A0A6E0E576DB62029B2DAA0C753492FD7DA2BB,
	AsyncCollisionExit2DTrigger__ctor_m72A6DA79B12E512A9B10B6AA0F728D7BF6A4AF21,
	NULL,
	AsyncCollisionStayTrigger_OnCollisionStay_mC235D9FDD5705CBE724FD6119BAC3B6150CC37AE,
	AsyncCollisionStayTrigger_GetOnCollisionStayAsyncHandler_m283A9CCD50E5A89EA8521A618C960E8AEB44EE7C,
	AsyncCollisionStayTrigger_GetOnCollisionStayAsyncHandler_mE5A7E194DC2FAAD29E5864EDADC831B856C5D9B1,
	AsyncCollisionStayTrigger_OnCollisionStayAsync_m84B8B23EDAA2C12CEFB89140E0B54084BC38BFCB,
	AsyncCollisionStayTrigger_OnCollisionStayAsync_m0F925EE0CB4D2F8157334D56F9B05D5BBB3508C5,
	AsyncCollisionStayTrigger__ctor_m34C682CFAD0F67A34A57DF60894ADE131F4856DA,
	NULL,
	AsyncCollisionStay2DTrigger_OnCollisionStay2D_mF8AEEFBB0CEC1348AFC6B79134DF01AD92BA2768,
	AsyncCollisionStay2DTrigger_GetOnCollisionStay2DAsyncHandler_m44B411FCCC0A476FBC95ED91C234A097CFE7C2AF,
	AsyncCollisionStay2DTrigger_GetOnCollisionStay2DAsyncHandler_m1E98372172C63DBF103C6EED5B23FEDBA67DB194,
	AsyncCollisionStay2DTrigger_OnCollisionStay2DAsync_m454925D5989FBE757CC4F20D6B5E90AF17324129,
	AsyncCollisionStay2DTrigger_OnCollisionStay2DAsync_mE6C36D3CC1673AE39A334C14F57CBDD3D995C44A,
	AsyncCollisionStay2DTrigger__ctor_m7C30628923FB0E8C50DA3F4F38B82CB29BE3AD44,
	NULL,
	AsyncControllerColliderHitTrigger_OnControllerColliderHit_mAD227189F948D3E8E842944ACBFFD0888F1833E8,
	AsyncControllerColliderHitTrigger_GetOnControllerColliderHitAsyncHandler_mC363F5C1D218419CBEA0C74DF453A72B01BC75FB,
	AsyncControllerColliderHitTrigger_GetOnControllerColliderHitAsyncHandler_m9DA2FBBC436C2D485A5837A344C868B2CE27D2EB,
	AsyncControllerColliderHitTrigger_OnControllerColliderHitAsync_m0E3347BABC50889B710B480D0693D7A53BD10C1A,
	AsyncControllerColliderHitTrigger_OnControllerColliderHitAsync_mBC4C181546FE07D1FF3BA4BFCF9E210E61492FC2,
	AsyncControllerColliderHitTrigger__ctor_mD603E3F88B2D886F4B2EE4771C7961710B206BBB,
	NULL,
	AsyncDisableTrigger_OnDisable_m27629A1A375B608AF0629315800517E7AED7F015,
	AsyncDisableTrigger_GetOnDisableAsyncHandler_m823BDBE58A1BC67F62D58A162C9795E2E73B724B,
	AsyncDisableTrigger_GetOnDisableAsyncHandler_m3D99632E3EC7B57B04F5217B46988BA4D61663DB,
	AsyncDisableTrigger_OnDisableAsync_m39073507C2EC519C7CF1D55A0508D196889093C9,
	AsyncDisableTrigger_OnDisableAsync_mC44ABD0117D19B65AF379FF1E0E6EDFD2D7561DD,
	AsyncDisableTrigger__ctor_m873E78F2F96384352B79F5306757371CE6D6B1B0,
	NULL,
	AsyncDrawGizmosTrigger_OnDrawGizmos_m95496C678B63AC47208033BC56C5440BC03CE692,
	AsyncDrawGizmosTrigger_GetOnDrawGizmosAsyncHandler_m2AAE59F99CA9C0F991FCE13B362E2CFF59F3CF47,
	AsyncDrawGizmosTrigger_GetOnDrawGizmosAsyncHandler_m15240D56F23E0D8F06414C40EC28F6D58AEE5F2E,
	AsyncDrawGizmosTrigger_OnDrawGizmosAsync_mAF42801C06E0525928B98F35189D4BD61DAB6BD8,
	AsyncDrawGizmosTrigger_OnDrawGizmosAsync_m5CF692CF3A528747E86265D019F0394828EE5F1D,
	AsyncDrawGizmosTrigger__ctor_m65E65F6170F606DFC6635AE428A7503EFA08EC93,
	NULL,
	AsyncDrawGizmosSelectedTrigger_OnDrawGizmosSelected_m0722FBFD1A3FC778E4D60B5030EF79AC9E4DF93C,
	AsyncDrawGizmosSelectedTrigger_GetOnDrawGizmosSelectedAsyncHandler_mCD428D02F0FB0DE82214E274C29DBF57AC62FD50,
	AsyncDrawGizmosSelectedTrigger_GetOnDrawGizmosSelectedAsyncHandler_m0608CC2047595CE12BBAA9A8AAB9C89AD8485C1C,
	AsyncDrawGizmosSelectedTrigger_OnDrawGizmosSelectedAsync_m44E2CC815DC944A553E06565615D6FA79069A5F2,
	AsyncDrawGizmosSelectedTrigger_OnDrawGizmosSelectedAsync_m201935EF54BD6E786376340262AA2795984DA421,
	AsyncDrawGizmosSelectedTrigger__ctor_m0CE776614C051D47985A62AD1FA82139ECB9FFFC,
	NULL,
	AsyncEnableTrigger_OnEnable_m70974D9CA4B9048370B1F96473C9AE5A2E5EB9D4,
	AsyncEnableTrigger_GetOnEnableAsyncHandler_m1A591CE6DF9392976AE6A241B5F0F3B714B35476,
	AsyncEnableTrigger_GetOnEnableAsyncHandler_m4F773C7C6AE3ABF4715C0828825F7587F10C3544,
	AsyncEnableTrigger_OnEnableAsync_m5C519820BED16BBEEA7420FE4F4EB94EF35AD920,
	AsyncEnableTrigger_OnEnableAsync_m3E08A4558DE581805D69BA63E406917D40336026,
	AsyncEnableTrigger__ctor_m797E2A5ADAA983DC15E69D8C301747BF45A774C6,
	NULL,
	AsyncGUITrigger_OnGUI_mA60A8A66BEF86CA519EE4E2900D2BED234AA4214,
	AsyncGUITrigger_GetOnGUIAsyncHandler_m9877F82A047A87ABC22B034E55835093E8FDDB66,
	AsyncGUITrigger_GetOnGUIAsyncHandler_m1577ACB3B4D075833499A6AF3F917CC8CEE4BDF6,
	AsyncGUITrigger_OnGUIAsync_m6B177ACF1208B8621259F08733A1C96A584CB3A3,
	AsyncGUITrigger_OnGUIAsync_mDFB0A31E8B908689DA5B7AD631B78A5D2C59A8C5,
	AsyncGUITrigger__ctor_m662BBCEEFCDE94BECA4CAA56AF151BE17634B97C,
	NULL,
	AsyncJointBreakTrigger_OnJointBreak_mD9EA2C90BE1BE9D6F8853766DD0CAAF021C19015,
	AsyncJointBreakTrigger_GetOnJointBreakAsyncHandler_m712AC149ADE057301DB3D9B6352820B11BAC4F11,
	AsyncJointBreakTrigger_GetOnJointBreakAsyncHandler_mC167415E45318C712C024D067144002E26080AA2,
	AsyncJointBreakTrigger_OnJointBreakAsync_mB64C9BC08BC56D73D5110B5F08A26895E05A005C,
	AsyncJointBreakTrigger_OnJointBreakAsync_m6E19058FC6A70775AEC2F46BE218E89843E70403,
	AsyncJointBreakTrigger__ctor_mC137FFE6730F70B7428DC27C2F2720477138E89D,
	NULL,
	AsyncJointBreak2DTrigger_OnJointBreak2D_m01624E2455500DD44C35B71306E4419340264875,
	AsyncJointBreak2DTrigger_GetOnJointBreak2DAsyncHandler_m45780C88BDF8CC704979B961E7DC1EC49CDEF0DC,
	AsyncJointBreak2DTrigger_GetOnJointBreak2DAsyncHandler_m220EB0402717ABA05B1A6A5D890AED2B645AC638,
	AsyncJointBreak2DTrigger_OnJointBreak2DAsync_m2D70AE8F833194042855C9D7745939403B8093A9,
	AsyncJointBreak2DTrigger_OnJointBreak2DAsync_m0D92E89FCAC9769301F88E2C1239671B8D01EC3A,
	AsyncJointBreak2DTrigger__ctor_mF63A573A77AC0A7C5B883281D5683D2B60AC4C50,
	NULL,
	AsyncMouseDownTrigger_OnMouseDown_mD70E1AB0E65B3197DD109E46E7225A3AE44D37F1,
	AsyncMouseDownTrigger_GetOnMouseDownAsyncHandler_m93DB1A59B96342FF3999D6A4B805F09CA47019FD,
	AsyncMouseDownTrigger_GetOnMouseDownAsyncHandler_mDE0EB80F4116245D816CDFB1DF3154AC4E627998,
	AsyncMouseDownTrigger_OnMouseDownAsync_mBB53A0A2A3A30E3F24669934DAD5527AE0087579,
	AsyncMouseDownTrigger_OnMouseDownAsync_m2632F5BCC92539EA8E26653B7701374891BF8428,
	AsyncMouseDownTrigger__ctor_m2763A2C928714E005383FC8A2F3439D14FCD596E,
	NULL,
	AsyncMouseDragTrigger_OnMouseDrag_mB09792958890B7BE3A7A851859D822CBAEBDCFAA,
	AsyncMouseDragTrigger_GetOnMouseDragAsyncHandler_m9E6BD8B33DA44A23CD3BAB036CB0865004D973A3,
	AsyncMouseDragTrigger_GetOnMouseDragAsyncHandler_mC6B0344C1A7E183F0AB848CBE495AF2DC0AB4E9F,
	AsyncMouseDragTrigger_OnMouseDragAsync_mDDE64985357FE669552304A91AF7414D19DD49C5,
	AsyncMouseDragTrigger_OnMouseDragAsync_mFD3C6E9E0BF5F57BACF7E114916397A9AE50D4C6,
	AsyncMouseDragTrigger__ctor_mBF7C5ED1FA43776B76CEF2BCB9502D865A11C1B6,
	NULL,
	AsyncMouseEnterTrigger_OnMouseEnter_mFB4A1555DAE4077AB6F104083C305C53CC9BC4D1,
	AsyncMouseEnterTrigger_GetOnMouseEnterAsyncHandler_m67E0BB290E1CF9AE0761739F1ACE71A389E01D65,
	AsyncMouseEnterTrigger_GetOnMouseEnterAsyncHandler_m17A04209B766767E51845D2AAA81BEDB7F593F74,
	AsyncMouseEnterTrigger_OnMouseEnterAsync_m89544B77C50A5B3A1B8B5D1E79735CC0D551FA4B,
	AsyncMouseEnterTrigger_OnMouseEnterAsync_m7AFCF4A8C39320F1140D498F875302C8FBB51CC8,
	AsyncMouseEnterTrigger__ctor_mA2DD14F92931DD86745DC13580D27D46DE24CB31,
	NULL,
	AsyncMouseExitTrigger_OnMouseExit_m59257BF3027A7293984B895499081B94FD5F2354,
	AsyncMouseExitTrigger_GetOnMouseExitAsyncHandler_mD172B6C09D3B6AEC264F117CB4624BBC1E8B0C70,
	AsyncMouseExitTrigger_GetOnMouseExitAsyncHandler_m6E60B6F76951CFCA78CF8B930E41413E72F96F4C,
	AsyncMouseExitTrigger_OnMouseExitAsync_m82517A2EC3624E2BAAD6468ED1CF7B221C628487,
	AsyncMouseExitTrigger_OnMouseExitAsync_mE7EA549AC351CAD99AAE7A4C1E721F3C01CACA3E,
	AsyncMouseExitTrigger__ctor_mC955EA41D6201C98903ADD11BE625717E6F445C3,
	NULL,
	AsyncMouseOverTrigger_OnMouseOver_m1C685D9651467B77AEB5D1436D65A51FBCF72A7C,
	AsyncMouseOverTrigger_GetOnMouseOverAsyncHandler_m928D106360FA93208548EE72C0E21A5943F795EE,
	AsyncMouseOverTrigger_GetOnMouseOverAsyncHandler_m45A95B5495433A4D1CA430776C1D86960ECFF3E8,
	AsyncMouseOverTrigger_OnMouseOverAsync_m41C548B0190AD07924C6B7939E5D82980E8C3FAA,
	AsyncMouseOverTrigger_OnMouseOverAsync_mDDE132BA4B1F84A2BE21A2B94FFCCD7F54B3D32F,
	AsyncMouseOverTrigger__ctor_mD55201077EF1E61CEE0FB9CE73DD253C8010C732,
	NULL,
	AsyncMouseUpTrigger_OnMouseUp_m7DC9BBBC7E3714467EEA46017B05AAAA8C06B21E,
	AsyncMouseUpTrigger_GetOnMouseUpAsyncHandler_m56C79C4B2DCB641BE60BD6F67DEC2079516D4E03,
	AsyncMouseUpTrigger_GetOnMouseUpAsyncHandler_m9C0F4A2ADD55F597E9035573AD5CBB2CE0E25A5E,
	AsyncMouseUpTrigger_OnMouseUpAsync_m22DAA9E6E97D23FB10F1CB90BD9D0449AC40FBF6,
	AsyncMouseUpTrigger_OnMouseUpAsync_m0F62FC1FE5F6E8613C186999F83BE1477CFE0F06,
	AsyncMouseUpTrigger__ctor_m8ADC4B17DBF78306C9970EAE792DF9A9951BDA0B,
	NULL,
	AsyncMouseUpAsButtonTrigger_OnMouseUpAsButton_m28C398E98480BFA8F8F53906FC9AA76CA5348DAE,
	AsyncMouseUpAsButtonTrigger_GetOnMouseUpAsButtonAsyncHandler_mC351CFF7549764C6074889BB7BC6DCC4D178DFF9,
	AsyncMouseUpAsButtonTrigger_GetOnMouseUpAsButtonAsyncHandler_mE44B444B1E913D898C48DEC635136D81E605A926,
	AsyncMouseUpAsButtonTrigger_OnMouseUpAsButtonAsync_mB2BAAE61D709EA3ECEC2E33A1FC45D709D910949,
	AsyncMouseUpAsButtonTrigger_OnMouseUpAsButtonAsync_mC59805D300AFF14E6F4F5F886C4AFA1D56591932,
	AsyncMouseUpAsButtonTrigger__ctor_m8B080E387368B161D60D794B4F69869617D25937,
	NULL,
	AsyncParticleCollisionTrigger_OnParticleCollision_m751F12DBF8C0A9B40DCFEA5A8E70CD17DD7012CE,
	AsyncParticleCollisionTrigger_GetOnParticleCollisionAsyncHandler_m9E573001A0FE6D9F910F1FCA1C52902CDD97DC02,
	AsyncParticleCollisionTrigger_GetOnParticleCollisionAsyncHandler_m86FD7A5FD578D80BD7F955B3177A547B7CA40B0C,
	AsyncParticleCollisionTrigger_OnParticleCollisionAsync_m985AFDB84D8DE11C2A2775276CD1E5DE7AE3D0D5,
	AsyncParticleCollisionTrigger_OnParticleCollisionAsync_m6161D1E223975321775AA2268D2147B84B9A4B22,
	AsyncParticleCollisionTrigger__ctor_mAEA03BBACA41A213AD773F27375F8FFEEBF9DB74,
	NULL,
	AsyncParticleSystemStoppedTrigger_OnParticleSystemStopped_m0BB207038D5D29E419B09D1FA1DE4CA6F9004886,
	AsyncParticleSystemStoppedTrigger_GetOnParticleSystemStoppedAsyncHandler_m51E624F071B4BB1A698709A67201CE1A96A3B4FF,
	AsyncParticleSystemStoppedTrigger_GetOnParticleSystemStoppedAsyncHandler_m9ECDCE1AD60A74418F3BB37B5DE3680E856F2D8D,
	AsyncParticleSystemStoppedTrigger_OnParticleSystemStoppedAsync_m0FB3F53A1B9DAE343FACE280EDCDE4986E5CC5B4,
	AsyncParticleSystemStoppedTrigger_OnParticleSystemStoppedAsync_mBF2E44551BDDA8812B370F2664E6307BF470CFD3,
	AsyncParticleSystemStoppedTrigger__ctor_m43AFD21B99F974A65692CD7FF010593671CDCE39,
	NULL,
	AsyncParticleTriggerTrigger_OnParticleTrigger_m33D86643A35447522AFAB46F2969317584392008,
	AsyncParticleTriggerTrigger_GetOnParticleTriggerAsyncHandler_m207D10B6570B7BB49C342BE3519208CECA61C49B,
	AsyncParticleTriggerTrigger_GetOnParticleTriggerAsyncHandler_mF11B045BF8ADD878F11CD3BB0C661092F8B223D6,
	AsyncParticleTriggerTrigger_OnParticleTriggerAsync_mD840E0AA3A64696D296EAB0544656421A02387A0,
	AsyncParticleTriggerTrigger_OnParticleTriggerAsync_mC4218F4D150A0E1C01A6D66A0A0F1DC668F9408B,
	AsyncParticleTriggerTrigger__ctor_mC085D993BA271B2A8B9A5E06AC78F3F536243CA8,
	NULL,
	AsyncParticleUpdateJobScheduledTrigger_OnParticleUpdateJobScheduled_m990067244B0769E88180A7F3887FEF8B2F9B643D,
	AsyncParticleUpdateJobScheduledTrigger_GetOnParticleUpdateJobScheduledAsyncHandler_mB5886BA1112A1DEE15EE113B0A3EB6DA838F9FFE,
	AsyncParticleUpdateJobScheduledTrigger_GetOnParticleUpdateJobScheduledAsyncHandler_mCBA5A97FDA6D19FCE8777C7EFA23123CCF6A9F1B,
	AsyncParticleUpdateJobScheduledTrigger_OnParticleUpdateJobScheduledAsync_m7BFD0DD7EE318D546BC9AF5B3987F2A9DCB46DE9,
	AsyncParticleUpdateJobScheduledTrigger_OnParticleUpdateJobScheduledAsync_mCB3326B52D80AA230C5B4E78CC6297403DB7DF2C,
	AsyncParticleUpdateJobScheduledTrigger__ctor_m9D92CC97CF40C32F047D2B332FBF50FC32429DF2,
	NULL,
	AsyncPostRenderTrigger_OnPostRender_m6A344301FA3C6875671DEEFC6CA3F6310FB9313E,
	AsyncPostRenderTrigger_GetOnPostRenderAsyncHandler_mED23D2BB8DF5EAE72452C87F7956C24FD1AC34E6,
	AsyncPostRenderTrigger_GetOnPostRenderAsyncHandler_mA12A2489D7FF41BAC1600E02B605E98D3360BA8B,
	AsyncPostRenderTrigger_OnPostRenderAsync_m77DD9B6B8676D926E0C1A61723BDDBB893D39F17,
	AsyncPostRenderTrigger_OnPostRenderAsync_m231BECAE2680E987C35214EB92D9693EADA4AD7D,
	AsyncPostRenderTrigger__ctor_m65724E5E972301DDE66ABFBF154C1DB43CD66B56,
	NULL,
	AsyncPreCullTrigger_OnPreCull_mB8C1EC3B67FAE78AA652BAC3F62E040A45611A93,
	AsyncPreCullTrigger_GetOnPreCullAsyncHandler_m8ECD951ACABD63751C9827C0E777518B735ACFCB,
	AsyncPreCullTrigger_GetOnPreCullAsyncHandler_m807441F3A8DC1D6A026076DD9776E743F62DDD53,
	AsyncPreCullTrigger_OnPreCullAsync_mBAFA9CA7B968528CAFDA7BC7A9E22B10A7182A24,
	AsyncPreCullTrigger_OnPreCullAsync_m6DD3ED29EC36AC224B1F8A692F5F43C20FEC7C47,
	AsyncPreCullTrigger__ctor_mF28627C543F02FF51D0DD26AA1DDA8D84A315AB5,
	NULL,
	AsyncPreRenderTrigger_OnPreRender_m655894FF19A236E354D9EE0A271C72DDBEE67F49,
	AsyncPreRenderTrigger_GetOnPreRenderAsyncHandler_m59B0FA38DBC1C256684A83C35125C2B4E0E92EB9,
	AsyncPreRenderTrigger_GetOnPreRenderAsyncHandler_m55779CA051D2F62199844763FBEE21E3E351CC6B,
	AsyncPreRenderTrigger_OnPreRenderAsync_m7E6F69F2AF47E4E04C3B6F3145E0C94BEA56D6C6,
	AsyncPreRenderTrigger_OnPreRenderAsync_mAE698C75E4BBB1C1683C0A27C7EA29AC39659AA7,
	AsyncPreRenderTrigger__ctor_m41C018E91D25D72553D40A71C1862A62A54FBA00,
	NULL,
	AsyncRectTransformDimensionsChangeTrigger_OnRectTransformDimensionsChange_m4D147A3E1686F19D8D1E1D432DE7FC1BD7468DFB,
	AsyncRectTransformDimensionsChangeTrigger_GetOnRectTransformDimensionsChangeAsyncHandler_mC88B31DB52A51A35DBEDECE82F1C1C8929021ECD,
	AsyncRectTransformDimensionsChangeTrigger_GetOnRectTransformDimensionsChangeAsyncHandler_m1F7822A0F70F51AAB0EAFFA4EF2A5EB240560C47,
	AsyncRectTransformDimensionsChangeTrigger_OnRectTransformDimensionsChangeAsync_m0CF2185A6AB66B207AA402308ED1543331600225,
	AsyncRectTransformDimensionsChangeTrigger_OnRectTransformDimensionsChangeAsync_mF498150A5D2874E416F67263719F600F9EE54809,
	AsyncRectTransformDimensionsChangeTrigger__ctor_mAA598E4BCABCE3C1CC14BC444B43D55B80913FB8,
	NULL,
	AsyncRectTransformRemovedTrigger_OnRectTransformRemoved_m1292FE9EF5754A262299D4B71E0691000A9E4983,
	AsyncRectTransformRemovedTrigger_GetOnRectTransformRemovedAsyncHandler_mCB506892D82DA5EFB2B85D34AF18EC04688046F8,
	AsyncRectTransformRemovedTrigger_GetOnRectTransformRemovedAsyncHandler_m6420AE5EA15B4EAA0C5CC440526B4804BEBF7347,
	AsyncRectTransformRemovedTrigger_OnRectTransformRemovedAsync_m52636D12D72390F1B2516C8D987301D5DF7B3209,
	AsyncRectTransformRemovedTrigger_OnRectTransformRemovedAsync_m0258F87B763E60388FDF207F807A9049B2510279,
	AsyncRectTransformRemovedTrigger__ctor_m63D51C37FAE81E87B5DC5568EB9BDA338B3F77FB,
	NULL,
	AsyncRenderImageTrigger_OnRenderImage_m6BA68601EFEE0C4203E529C86541F3E995D23927,
	AsyncRenderImageTrigger_GetOnRenderImageAsyncHandler_m1A07A30154A3999B71558D5C3BA7B67D69E332AA,
	AsyncRenderImageTrigger_GetOnRenderImageAsyncHandler_mBC9E6E52E1432D79ACF1052FFA52E3D71F9521AA,
	AsyncRenderImageTrigger_OnRenderImageAsync_m15695771B3D3D1EE731505588D5C59BA46F395ED,
	AsyncRenderImageTrigger_OnRenderImageAsync_m894722EA905E1CF1E490C891E432AF1D9582CEDC,
	AsyncRenderImageTrigger__ctor_m18A16A88B234F778B466DD0A82732A0D24D57B01,
	NULL,
	AsyncRenderObjectTrigger_OnRenderObject_m6284CD0D1D0E6A45D22CC4DC2EFF0EDF4DB06FFA,
	AsyncRenderObjectTrigger_GetOnRenderObjectAsyncHandler_m46893092AEAC8422ED6DDADAA2F2B1828C82434A,
	AsyncRenderObjectTrigger_GetOnRenderObjectAsyncHandler_m36200B7C98888046CA1E981893C9C12802752E29,
	AsyncRenderObjectTrigger_OnRenderObjectAsync_m88D2938A860B3D50F1C6755EE744D98D86F1F750,
	AsyncRenderObjectTrigger_OnRenderObjectAsync_m095D596AFAE9A51FD36AE22B4C9FC7FB84CE0A82,
	AsyncRenderObjectTrigger__ctor_m44865FD89896C3310967341A6264C1518E85BE56,
	NULL,
	AsyncServerInitializedTrigger_OnServerInitialized_m5B5BF7F93AA29141B67AB342FA3CB0A58C75FA62,
	AsyncServerInitializedTrigger_GetOnServerInitializedAsyncHandler_m721BC23A4300149AE4AFF878B743E11FAFED0EAE,
	AsyncServerInitializedTrigger_GetOnServerInitializedAsyncHandler_mDC260D54895ED4EB77684425C4662D07A5E3ABD6,
	AsyncServerInitializedTrigger_OnServerInitializedAsync_mC7D833F2566DCFAA1C5EFC2068095EE4296D2F38,
	AsyncServerInitializedTrigger_OnServerInitializedAsync_m11C96D6C590301B03F9ABCAD9061ACBD961CACC9,
	AsyncServerInitializedTrigger__ctor_m381EB41606C60A11FD02FAACD077C73E72663748,
	NULL,
	AsyncTransformChildrenChangedTrigger_OnTransformChildrenChanged_mD8F30C19ADDA4A8518AC039466085E30CCBF0CB2,
	AsyncTransformChildrenChangedTrigger_GetOnTransformChildrenChangedAsyncHandler_mBB6E764D4AB17D7705D5B5DF82486E5C9FDE9F83,
	AsyncTransformChildrenChangedTrigger_GetOnTransformChildrenChangedAsyncHandler_mAAA8725F908EAAB62E2294B24845056FE762E334,
	AsyncTransformChildrenChangedTrigger_OnTransformChildrenChangedAsync_m5815B6E64017C06C99A7A0731358F23673373A77,
	AsyncTransformChildrenChangedTrigger_OnTransformChildrenChangedAsync_m039AF980CC5B5C0482489A664CA4E27FFFDFC2D2,
	AsyncTransformChildrenChangedTrigger__ctor_m8A7D94E9D7A1B9552324F38FC96325A61EDB1CDB,
	NULL,
	AsyncTransformParentChangedTrigger_OnTransformParentChanged_mC6D4BE9C7361D2D52D0F12DD6C2BBC362228D4C6,
	AsyncTransformParentChangedTrigger_GetOnTransformParentChangedAsyncHandler_mB07C2A5C8268B19CF55E9A594F94ADA550612D38,
	AsyncTransformParentChangedTrigger_GetOnTransformParentChangedAsyncHandler_m48FFF091007D3A1D2EC7A7DD23B245A1F75C2237,
	AsyncTransformParentChangedTrigger_OnTransformParentChangedAsync_m6971910B64FB85EED0106CC2147F4E44C6D97725,
	AsyncTransformParentChangedTrigger_OnTransformParentChangedAsync_m2E2817A9692ACA4D57B6C3959EDF1C3BA9FDE99E,
	AsyncTransformParentChangedTrigger__ctor_m2AFA067FB7D8AF9E9741E1BB6D4CEB163A73CBB1,
	NULL,
	AsyncTriggerEnterTrigger_OnTriggerEnter_mB37A7745A8921BC91E6E3E1405CD3F55F3A0CA98,
	AsyncTriggerEnterTrigger_GetOnTriggerEnterAsyncHandler_m828FB8FC4A0F5E351650DDC69AC6257541E0E410,
	AsyncTriggerEnterTrigger_GetOnTriggerEnterAsyncHandler_mAE0A5C99547A55A5BB066FEE0093C108D3554AC9,
	AsyncTriggerEnterTrigger_OnTriggerEnterAsync_m9104DD0D7B9C49D82952FE14CDCA0A8D66327210,
	AsyncTriggerEnterTrigger_OnTriggerEnterAsync_m49CB8ADBD70AF23DB5FCEB039577D656FAA7D292,
	AsyncTriggerEnterTrigger__ctor_mED114C4AC3A4AAB5611DE0CEF89337E4E9EF197C,
	NULL,
	AsyncTriggerEnter2DTrigger_OnTriggerEnter2D_m4A76B2B3A30D014199AEF338F718F4346995F2AE,
	AsyncTriggerEnter2DTrigger_GetOnTriggerEnter2DAsyncHandler_m549AAF9382AF259D971A88482264EB3B2A307696,
	AsyncTriggerEnter2DTrigger_GetOnTriggerEnter2DAsyncHandler_m54E49E03BAB2E5D5D60BFE58C613CF7B9EB56AA6,
	AsyncTriggerEnter2DTrigger_OnTriggerEnter2DAsync_m1FD08D259A09497C9C847587011C7B9295C478D8,
	AsyncTriggerEnter2DTrigger_OnTriggerEnter2DAsync_m7A7C4312B665096AABA1F5C90D2BE2E2E54B926E,
	AsyncTriggerEnter2DTrigger__ctor_m8506BC0C1177C5CC23D2517F4F2BD4F798D85FB7,
	NULL,
	AsyncTriggerExitTrigger_OnTriggerExit_m65D0D5ED581AE51F2E144015345DA004BDAE7389,
	AsyncTriggerExitTrigger_GetOnTriggerExitAsyncHandler_m3541934C34BFC00CA6E1D155C434EACAD65323F2,
	AsyncTriggerExitTrigger_GetOnTriggerExitAsyncHandler_mAD35142C515F9F433825A755EF7F0EF575357607,
	AsyncTriggerExitTrigger_OnTriggerExitAsync_m0506F9F37FF7E065A2CB5BD43C3BD74704887CE2,
	AsyncTriggerExitTrigger_OnTriggerExitAsync_mD22FB40BE1136E87660E936F60FB452FABDE24FD,
	AsyncTriggerExitTrigger__ctor_m41699831CB7F6A228617FBCD3AF3A12A309DD30C,
	NULL,
	AsyncTriggerExit2DTrigger_OnTriggerExit2D_m0B116706765FF54C1E18025B6EE32461F74EE11A,
	AsyncTriggerExit2DTrigger_GetOnTriggerExit2DAsyncHandler_m6031C0D82F53F432BF19A2465DB00117B933DDCE,
	AsyncTriggerExit2DTrigger_GetOnTriggerExit2DAsyncHandler_m85A8371A2CC850F04E405341DE600D0BD13FFE8C,
	AsyncTriggerExit2DTrigger_OnTriggerExit2DAsync_m5EE03B2172A0B9931A69A455123CDA808F098600,
	AsyncTriggerExit2DTrigger_OnTriggerExit2DAsync_m00632B2548CD0ADC4369C7A5BB8A55D1977CDEF3,
	AsyncTriggerExit2DTrigger__ctor_m18CD7479FA414B323FC6FC36F75C7A641BF4BD53,
	NULL,
	AsyncTriggerStayTrigger_OnTriggerStay_m84AEF8BD5E0D0E7009AF75FD72EB35B251FA6C32,
	AsyncTriggerStayTrigger_GetOnTriggerStayAsyncHandler_mA49FDB93C58E9DAD24AEFE69CF06C53D088EBE7A,
	AsyncTriggerStayTrigger_GetOnTriggerStayAsyncHandler_mB0DAB9AD8E8F489CCF5DF5DA5A900AE332E2670C,
	AsyncTriggerStayTrigger_OnTriggerStayAsync_mA8972D945109767D84A6E79665143EA081455280,
	AsyncTriggerStayTrigger_OnTriggerStayAsync_m76FB08E3FD87DF4084B8445FB989E0960CB8B703,
	AsyncTriggerStayTrigger__ctor_mF856C0C76C958CF2134AE02631ED82FB3BC9C093,
	NULL,
	AsyncTriggerStay2DTrigger_OnTriggerStay2D_m9BECEE469903EF4BAAF8C8F60F5134BD12A38A47,
	AsyncTriggerStay2DTrigger_GetOnTriggerStay2DAsyncHandler_mB85D0BDC1569ADC6D77BABD0E7DC585BCBF1B638,
	AsyncTriggerStay2DTrigger_GetOnTriggerStay2DAsyncHandler_m664F9A79F8D50099AB05AC60F322BA7A103F18D4,
	AsyncTriggerStay2DTrigger_OnTriggerStay2DAsync_m560D6EAC73326DAED1C878EAAD30203354DD327C,
	AsyncTriggerStay2DTrigger_OnTriggerStay2DAsync_m13E6D85207AA0D3DEDEAAE391D2543E63133397A,
	AsyncTriggerStay2DTrigger__ctor_m5A99E8921602F7ADF6F12753863D020034EF4B03,
	NULL,
	AsyncValidateTrigger_OnValidate_m4EB45C40CA6F395E5E3BB2F43077D1397FFB7D99,
	AsyncValidateTrigger_GetOnValidateAsyncHandler_mFD485310050DFE5C9DB4E4E8707067A97EADDF64,
	AsyncValidateTrigger_GetOnValidateAsyncHandler_m97B53533031EC18ADBD0E27CCD7D4517C6DBB287,
	AsyncValidateTrigger_OnValidateAsync_m554192DE3BCFDD97898F1B79DD0C82E7B4F88B28,
	AsyncValidateTrigger_OnValidateAsync_m348CD615CBED4C96AC9E06CB79DB8246FE29A68E,
	AsyncValidateTrigger__ctor_mF8FAD51D9560D4FA8774BF252A22E56C81957F34,
	NULL,
	AsyncWillRenderObjectTrigger_OnWillRenderObject_m66778D8D448CE41839513EFA7298A084D5CD9209,
	AsyncWillRenderObjectTrigger_GetOnWillRenderObjectAsyncHandler_m3F3B7E5AE502B08B3EC76FBF33D8DB8D05E96180,
	AsyncWillRenderObjectTrigger_GetOnWillRenderObjectAsyncHandler_m38AE715434BD96E22EF763AF39640796E8FACF17,
	AsyncWillRenderObjectTrigger_OnWillRenderObjectAsync_m07FA2F03D03B58516A3FF615EEEB10B4F096FF63,
	AsyncWillRenderObjectTrigger_OnWillRenderObjectAsync_m718D31D27CE461610034BFE846636A8400DF22DE,
	AsyncWillRenderObjectTrigger__ctor_mE32AA2A59BF32E5FDF7126C04FD93F8F394CBE73,
	NULL,
	AsyncResetTrigger_Reset_mBC8FF81E5B2EF8CBA1FB599E5298A89B618E048D,
	AsyncResetTrigger_GetResetAsyncHandler_m824B83F00073BF5575F86045D35A387139292431,
	AsyncResetTrigger_GetResetAsyncHandler_m79D5BA1E6024B03F5BC8C511EC155B84C69686B5,
	AsyncResetTrigger_ResetAsync_mB1359EA13240C409FE151CE01805E6487F009BA2,
	AsyncResetTrigger_ResetAsync_mF397DE0A4FA0CB3897DDD83B3B6A429D4EDBD98B,
	AsyncResetTrigger__ctor_m6FE44AA6AD1A192E1A453EA86E27B8325E57CFBE,
	NULL,
	AsyncUpdateTrigger_Update_m6A8BB9E02929E519F4220BF0AC31B700657821F7,
	AsyncUpdateTrigger_GetUpdateAsyncHandler_mEE20F6AFBDD0F73500F0E931E4B62A0CE6F803F6,
	AsyncUpdateTrigger_GetUpdateAsyncHandler_mC1A4D4D3D9B9E0FED5495C527C611137744FFD08,
	AsyncUpdateTrigger_UpdateAsync_mBF4AC1B8394F446AC65E28B49C9007C14F8A62AC,
	AsyncUpdateTrigger_UpdateAsync_m6F62830AC2B911CE028627D336CF7FDE6A3122F7,
	AsyncUpdateTrigger__ctor_m7D5482B0A707705E896A434E95C7DE3B007BA131,
	NULL,
	AsyncBeginDragTrigger_UnityEngine_EventSystems_IBeginDragHandler_OnBeginDrag_mF4D36C965EF27FE13798AE33F7910DEEFD4BAEF7,
	AsyncBeginDragTrigger_GetOnBeginDragAsyncHandler_m99C573F5900022DD901EF0BF1CF1F0543DAD05B6,
	AsyncBeginDragTrigger_GetOnBeginDragAsyncHandler_m9B14EBFD9667AD8C9316EE8F8D641002FBFD9159,
	AsyncBeginDragTrigger_OnBeginDragAsync_mD7EF6BD3B2E760D6B953C47488E4786D218DB904,
	AsyncBeginDragTrigger_OnBeginDragAsync_m1918F458147993D3B7AF51D7C6A71D4359831F24,
	AsyncBeginDragTrigger__ctor_m6934073E233A1682E395BA92A0D62CBE65299738,
	NULL,
	AsyncCancelTrigger_UnityEngine_EventSystems_ICancelHandler_OnCancel_m4FDE66F8B68DDC571AFA979C44565ED72B616E4E,
	AsyncCancelTrigger_GetOnCancelAsyncHandler_m1099B4B747D41A121B214E3BCCED5246AB878D8E,
	AsyncCancelTrigger_GetOnCancelAsyncHandler_mE6580CC2E3D0BE8F2A68F0315788967408551F84,
	AsyncCancelTrigger_OnCancelAsync_mAB5C6E49A6FF9884F642F091E764F2C3BE1909E0,
	AsyncCancelTrigger_OnCancelAsync_m8354E1DE5C5B44E4ADAFEF83EFD2DB9C1105978B,
	AsyncCancelTrigger__ctor_mF5B969F04F0A30397156786E825F6639780B9413,
	NULL,
	AsyncDeselectTrigger_UnityEngine_EventSystems_IDeselectHandler_OnDeselect_mAB62A2F625193E8EB567EA98DEAAD8F7338E2FE7,
	AsyncDeselectTrigger_GetOnDeselectAsyncHandler_m6313BFA5E2A4EF4C62384AA934ED61286BC380F0,
	AsyncDeselectTrigger_GetOnDeselectAsyncHandler_mD7067A8EC1F5F7E3D4B81E13A1464C0CE196E14C,
	AsyncDeselectTrigger_OnDeselectAsync_m6FF538527BFCA2139FE41C996BB104C6F953F819,
	AsyncDeselectTrigger_OnDeselectAsync_m98CFBD0B3C3AE80C3C095842AFBD58852AE2DABD,
	AsyncDeselectTrigger__ctor_mD911081015ED71919F39FAAB7E6E23A988EDA4AB,
	NULL,
	AsyncDragTrigger_UnityEngine_EventSystems_IDragHandler_OnDrag_m650050B1A3637A54D4640B73D498D3AF2A80EA7B,
	AsyncDragTrigger_GetOnDragAsyncHandler_m792B59F8831AD3A5CD8CBE78FB9EE81ABBD2C449,
	AsyncDragTrigger_GetOnDragAsyncHandler_mD011C4C6710A5A29A7BBAD858DFF069EDD8A896A,
	AsyncDragTrigger_OnDragAsync_mE52D48B63CE940A0070247015A23FDB5847B0D22,
	AsyncDragTrigger_OnDragAsync_mFD7F0A4829E380EC9BE75E290062E33744DFAF59,
	AsyncDragTrigger__ctor_m2E1E4AEA309FA552FAAA24009B15F5C12315A7DF,
	NULL,
	AsyncDropTrigger_UnityEngine_EventSystems_IDropHandler_OnDrop_mD54F5762DBB3975DFCB29E90675A22E7468C9C8A,
	AsyncDropTrigger_GetOnDropAsyncHandler_m710A98E56E9066858466C8C8B78A1280EA0B7010,
	AsyncDropTrigger_GetOnDropAsyncHandler_m39EA60EFEF6D8B34C615A6C1C09A3CD45E5A218B,
	AsyncDropTrigger_OnDropAsync_m719D00F901423B744112C9B055A6FFD0B9FBA3C4,
	AsyncDropTrigger_OnDropAsync_m4B0261239FF304E686F19EED6673D15603C58187,
	AsyncDropTrigger__ctor_m3F7CE17E66816B6C502FFD1322970B126DBBBA54,
	NULL,
	AsyncEndDragTrigger_UnityEngine_EventSystems_IEndDragHandler_OnEndDrag_mB1BE9EDD57ACD7A52AD5307761D66CA3744F3E4A,
	AsyncEndDragTrigger_GetOnEndDragAsyncHandler_m92AE52FAEEAA64C68B9BE79F942D25F047C9B98B,
	AsyncEndDragTrigger_GetOnEndDragAsyncHandler_m4A5AD2C344FCAC54B14611C0F72D52A8731F3749,
	AsyncEndDragTrigger_OnEndDragAsync_mD0632C140193F1EB06C90BC04DBB6BCBD1F252F1,
	AsyncEndDragTrigger_OnEndDragAsync_m656BD8AA647525EA96C4A423DBC238A49FE9FFA5,
	AsyncEndDragTrigger__ctor_m9CF68CD3A93B747C6A42B9E1990CAF00CEDD9833,
	NULL,
	AsyncInitializePotentialDragTrigger_UnityEngine_EventSystems_IInitializePotentialDragHandler_OnInitializePotentialDrag_m1122772FE7F969337CE6437B0FCA48FCCA95339C,
	AsyncInitializePotentialDragTrigger_GetOnInitializePotentialDragAsyncHandler_m9026E10BF5D23F7359C1E62E11BA99F44D6CA883,
	AsyncInitializePotentialDragTrigger_GetOnInitializePotentialDragAsyncHandler_mF46B0E0F8B4B52248934D7A7F263A461BB6406FF,
	AsyncInitializePotentialDragTrigger_OnInitializePotentialDragAsync_mF69D7A921F0EC522DB7774D04046E6995F21E8F3,
	AsyncInitializePotentialDragTrigger_OnInitializePotentialDragAsync_mFBA18411611F1372C9F0C01806115376E2C90E2E,
	AsyncInitializePotentialDragTrigger__ctor_m676286872B5182AA75E33E28E4A88D035DC9A1FC,
	NULL,
	AsyncMoveTrigger_UnityEngine_EventSystems_IMoveHandler_OnMove_m516D96C8A670D55D2D8FDD6303B50ADF032A119B,
	AsyncMoveTrigger_GetOnMoveAsyncHandler_m5BC71E2150066D7006B2948E8FBCB56DB8B37E77,
	AsyncMoveTrigger_GetOnMoveAsyncHandler_m6DC2AAFFF4445CE9E3E86F55F64D8815648C17E9,
	AsyncMoveTrigger_OnMoveAsync_mEE3A3FD0434FC0CD1B0513BC836B945125955D32,
	AsyncMoveTrigger_OnMoveAsync_m0D6E48C390DFFEE8754BAD04FD4AFC00DE152143,
	AsyncMoveTrigger__ctor_m58A79FA8D0F55AB1155495110D302D31B750E634,
	NULL,
	AsyncPointerClickTrigger_UnityEngine_EventSystems_IPointerClickHandler_OnPointerClick_mF17716B26BBD8617FB2FB108EE482578330938C8,
	AsyncPointerClickTrigger_GetOnPointerClickAsyncHandler_m36796E8FFD89894A699F5D98CA2449F33C827114,
	AsyncPointerClickTrigger_GetOnPointerClickAsyncHandler_m5095467F79409EAD4064E465B0816CBCA09DEB07,
	AsyncPointerClickTrigger_OnPointerClickAsync_mA996A63CFE77031A3732902C348A89E49CA43C46,
	AsyncPointerClickTrigger_OnPointerClickAsync_m32A09585E6928CD4B7C512116925616DC23DBB8E,
	AsyncPointerClickTrigger__ctor_mB539AB18F317405415F99977FCCA9B4E8597A277,
	NULL,
	AsyncPointerDownTrigger_UnityEngine_EventSystems_IPointerDownHandler_OnPointerDown_m0F599F3ECE9C5BA039F0C7EB1705847E4AC96543,
	AsyncPointerDownTrigger_GetOnPointerDownAsyncHandler_mF4336BB9D8556120CC40302EADA21BB1648ADD2E,
	AsyncPointerDownTrigger_GetOnPointerDownAsyncHandler_mAA363B4E92D9C606707459D7945B3C3A552C7500,
	AsyncPointerDownTrigger_OnPointerDownAsync_m109E0B4B8404FC307B05F5438BE12C2C314AFA9F,
	AsyncPointerDownTrigger_OnPointerDownAsync_m0A91C9504F28835C272DA5230708EDE8193952A1,
	AsyncPointerDownTrigger__ctor_mCEC9E22FABB448746B967619E9B92CC5F5C0F753,
	NULL,
	AsyncPointerEnterTrigger_UnityEngine_EventSystems_IPointerEnterHandler_OnPointerEnter_m78076152F21226339D9EA6D951970DFE7F4A5262,
	AsyncPointerEnterTrigger_GetOnPointerEnterAsyncHandler_m647D420223B53D50706C1A74F8F57296D50FC62D,
	AsyncPointerEnterTrigger_GetOnPointerEnterAsyncHandler_mF7D0C87A38E06C03126C457B4D7CC57AEFFC541F,
	AsyncPointerEnterTrigger_OnPointerEnterAsync_m11D56027250B34852209A9F70279A1F22BB26D09,
	AsyncPointerEnterTrigger_OnPointerEnterAsync_mE591B254C8BD99EA4F6A980397DCB49C45DDB4B3,
	AsyncPointerEnterTrigger__ctor_m2C22E7D5F605E1A691A2AE2D845385BB70CDDA9E,
	NULL,
	AsyncPointerExitTrigger_UnityEngine_EventSystems_IPointerExitHandler_OnPointerExit_m41EB252FA7F5EF0A0F388277F84F55ECF8DC6D59,
	AsyncPointerExitTrigger_GetOnPointerExitAsyncHandler_mD3B46A4AF5FBAFB5F006E29ACADE05B79B70DB44,
	AsyncPointerExitTrigger_GetOnPointerExitAsyncHandler_m7F0F68056AA10801869346998C9EDA1BDDA0752F,
	AsyncPointerExitTrigger_OnPointerExitAsync_mB7F5B14299612EE468609618262314BAC4C7E064,
	AsyncPointerExitTrigger_OnPointerExitAsync_mB2C4919B58D410CAB8D61723511D299BD9248DDC,
	AsyncPointerExitTrigger__ctor_mBE66620F59FECD260E02BC5F09CB87EAC068B379,
	NULL,
	AsyncPointerUpTrigger_UnityEngine_EventSystems_IPointerUpHandler_OnPointerUp_m527A125F43A676CEE8F515D51E8B5FFBF6A55B08,
	AsyncPointerUpTrigger_GetOnPointerUpAsyncHandler_m2DD93E63657198525CB4EE01A459722C3B5970AD,
	AsyncPointerUpTrigger_GetOnPointerUpAsyncHandler_mD827DF3DA91E33B4EE0F1457229733F2F17D2AEF,
	AsyncPointerUpTrigger_OnPointerUpAsync_mFD18098C949BB616B7A816C347F6ECFBE02CFC70,
	AsyncPointerUpTrigger_OnPointerUpAsync_mA0079C88A2F824E854924A45B48BA43594BBA92E,
	AsyncPointerUpTrigger__ctor_m84BF6F4D45F028D589A11C0893361DA80196C9CD,
	NULL,
	AsyncScrollTrigger_UnityEngine_EventSystems_IScrollHandler_OnScroll_m3B383490E6DA907F1C6342549B7A6D4F79CDA5F9,
	AsyncScrollTrigger_GetOnScrollAsyncHandler_mE25C1CE7F3ED17FC90394978D9861AD79D5B0F6C,
	AsyncScrollTrigger_GetOnScrollAsyncHandler_m2B1C0FDD5B265188DC96B563249EF8F2BBCB27E0,
	AsyncScrollTrigger_OnScrollAsync_mA4DF287320D375BFAB63B6FBD87929BFE4B21293,
	AsyncScrollTrigger_OnScrollAsync_mF7E99ECD43DF422B6367577B8AA2EE14ED8933DE,
	AsyncScrollTrigger__ctor_m982A622555EC0A7C55D4F871B63BD72F905994C7,
	NULL,
	AsyncSelectTrigger_UnityEngine_EventSystems_ISelectHandler_OnSelect_m37F162B540ADEDFD827152E7EC623FB7094B1CC6,
	AsyncSelectTrigger_GetOnSelectAsyncHandler_mB655A93A87E8388D649CD1CCC7E7273A69E90AB0,
	AsyncSelectTrigger_GetOnSelectAsyncHandler_mBDDF0332CEA42C3BF8AC26F037471CB78465B112,
	AsyncSelectTrigger_OnSelectAsync_m48A6985C36A7CDDEA1E444E0A6B3BA32290DFEDB,
	AsyncSelectTrigger_OnSelectAsync_m609638429AD51726597BA2CD806F0C0FE680927F,
	AsyncSelectTrigger__ctor_mD3476EF0DC05B542C78718CD836835880EEC2FB1,
	NULL,
	AsyncSubmitTrigger_UnityEngine_EventSystems_ISubmitHandler_OnSubmit_m85B96C560936785E769ADAC133E336E60F0A8B56,
	AsyncSubmitTrigger_GetOnSubmitAsyncHandler_m33816F843C417A716492E1C76E074C2BAE525542,
	AsyncSubmitTrigger_GetOnSubmitAsyncHandler_mF8897F1861D7F8C51699FEBCB3BDB1B9B2952C4F,
	AsyncSubmitTrigger_OnSubmitAsync_m925E417A2AF84E890FABFBA07F3BF09B7378289E,
	AsyncSubmitTrigger_OnSubmitAsync_mDCE0134DD38203DFC0E7C92949E50A720CA1C99A,
	AsyncSubmitTrigger__ctor_m1C92209AA130A99C9ACE4BEA8F2DC64304B34BAB,
	NULL,
	AsyncUpdateSelectedTrigger_UnityEngine_EventSystems_IUpdateSelectedHandler_OnUpdateSelected_m1B9581CFB253871C2102B053B682278BA8540687,
	AsyncUpdateSelectedTrigger_GetOnUpdateSelectedAsyncHandler_mB0F6D83BC172861063492980489E0B7EB89ADA98,
	AsyncUpdateSelectedTrigger_GetOnUpdateSelectedAsyncHandler_m1043A7AA796125EFE5CAA839EB9B41A7CF3F8D03,
	AsyncUpdateSelectedTrigger_OnUpdateSelectedAsync_m494136ADA379A17083E816B1A8AF5B274F67D80B,
	AsyncUpdateSelectedTrigger_OnUpdateSelectedAsync_mD71A864AA8C64A4478FAFE216074A0CBCC4A5B18,
	AsyncUpdateSelectedTrigger__ctor_mECD611573F1F10CA33DAAC3DAB6903D54171C471,
	ContinuationQueue__ctor_mC79FF88E0804F6D27B1CFC356DAD61615BF7A24E,
	ContinuationQueue_Enqueue_m177514E92921CC3865F3AF3091D7E0903B235E50,
	ContinuationQueue_Run_m6E60144908373A67C66307A56F8C0D5A612EA764,
	ContinuationQueue_RunCore_mA8B47350621EF411C87B9BF4255A6B608713CE65,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerLoopRunner__ctor_mC2903BD5D95FBD25EBAB8A94B8F44CEF43031B80,
	PlayerLoopRunner_AddAction_mEEC35DD90D8295CE01C50420E7EF39B640AF9682,
	PlayerLoopRunner_Run_mE0A8E6A8A7C3F72DF1B45A128901018DB6743B38,
	PlayerLoopRunner_RunCore_m6C97C81CE05C65E05140C6F3FA67F4ACCB908BC1,
	U3CU3Ec__cctor_m31F71639A2C525704C2F5174479E8050889AEB8D,
	U3CU3Ec__ctor_mB2A7388A955AD37EA26AF64BBB35F310CA408F14,
	U3CU3Ec_U3C_ctorU3Eb__9_0_m491AE613FAF09E7739F0BF7D5CC44F395AF8B523,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ValueStopwatch_StartNew_m398D9548C1C9C3C505294ABB321A70B7361462E4,
	ValueStopwatch__ctor_mD72CC04E4E48BADF6C4A8DD48DB9FC1A1AC42EF5,
	ValueStopwatch_get_IsInvalid_m0A3D180006FB7784C23036BA26772A1D883E895A,
	ValueStopwatch_get_ElapsedTicks_m191300A5FC932D5C0BAFE98BF7BE560DFFB6EA10,
	ValueStopwatch__cctor_mA0E464EE81081955FF8B29534A729335E5628FC0,
	SingleAssignmentDisposable_set_Disposable_mCC2C1EDD0B6BAB59C4D03B133D0E227DB232BC14,
	SingleAssignmentDisposable_Dispose_m8E24570E1565FC9AD333B75DA75FE503CC64BF10,
	SingleAssignmentDisposable__ctor_m0B130CC178E9F7EC8AE8EB0CB92DD1AA79BA1BAD,
	AsyncUniTaskMethodBuilder_Create_m5B36880F3EC6CACA4359D94D53B8DC633AA8C018,
	AsyncUniTaskMethodBuilder_get_Task_m2F98008CBCDF840A80869042FF5620775C3E3ED7,
	AsyncUniTaskMethodBuilder_SetException_m7153CFFA0C20E8036CC35A0ECDD57649B1807A6C,
	AsyncUniTaskMethodBuilder_SetResult_m3018C0048802DBD765F7796F44FED9C706E988F6,
	NULL,
	NULL,
	AsyncUniTaskMethodBuilder_SetStateMachine_mA8DEE00670CB37DB2DA98D16AF514F9B4B19B141,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
extern void AsyncUnit_GetHashCode_m313965D898D515FE5DF5464ED8C0C9EF64B9F0C6_AdjustorThunk (void);
extern void AsyncUnit_Equals_m7F9CC10F7DDF7211EAD2D0A56B413C82156C2E07_AdjustorThunk (void);
extern void AsyncUnit_ToString_m3BB12D2C87E38E4D5B1D1A6F3BD3EEC6F2A6488F_AdjustorThunk (void);
extern void UniTask__ctor_mA1D2FAF1D02391065D770F56EE3CFD028157CACD_AdjustorThunk (void);
extern void UniTask_get_Status_m5CCD7973CFD636CF45F67AF23CC34349ABBAD0AD_AdjustorThunk (void);
extern void UniTask_GetAwaiter_m1FE35EBA18DB40141C1025CECD4FC300E3A8C59C_AdjustorThunk (void);
extern void UniTask_ToString_m4BF6DB11E21177970C4F895B04F0D3ED6673A215_AdjustorThunk (void);
extern void Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF_AdjustorThunk (void);
extern void Awaiter_get_IsCompleted_mDCC71AAE7FA5EA2B303D3EC512177FD122C8DA49_AdjustorThunk (void);
extern void Awaiter_GetResult_mFCA8BA81A9A7DA641DEFC7FC121FED87A3EC06CA_AdjustorThunk (void);
extern void Awaiter_OnCompleted_m080A1DBE2E806E1CDE6931514C3A6C8DBDF2E646_AdjustorThunk (void);
extern void Awaiter_UnsafeOnCompleted_m1428D2FC0145C8CABB14E4B9A15F38B6AE34F8E5_AdjustorThunk (void);
extern void Awaiter_SourceOnCompleted_m71BEAA335CDD0FBD24F14F9B970B8E1B69AE01B4_AdjustorThunk (void);
extern void YieldAwaitable__ctor_mDFCED8E03E478AA784DCBAF67876091D6457590F_AdjustorThunk (void);
extern void YieldAwaitable_GetAwaiter_mABCF83C041AEE78B7915625989429CF87DC28EC9_AdjustorThunk (void);
extern void Awaiter__ctor_m05FC94348E722A3E503CD81CE62E02A5163A58F0_AdjustorThunk (void);
extern void Awaiter_get_IsCompleted_mF92B16767983ACC6C69D07E32F866F14E3E3A830_AdjustorThunk (void);
extern void Awaiter_GetResult_m5B7F9896A518DB6316AD7C1741AFA0441CA40B3F_AdjustorThunk (void);
extern void Awaiter_OnCompleted_mB06C744A459C12B974DF387EB24B33B8663524F7_AdjustorThunk (void);
extern void Awaiter_UnsafeOnCompleted_m7891BED0BBDFA9C626E10CD515326D3E1A859634_AdjustorThunk (void);
extern void Callback_Invoke_m380CCA09614F37D4ACF8EBFD996809A7E123E763_AdjustorThunk (void);
extern void ValueStopwatch__ctor_mD72CC04E4E48BADF6C4A8DD48DB9FC1A1AC42EF5_AdjustorThunk (void);
extern void ValueStopwatch_get_IsInvalid_m0A3D180006FB7784C23036BA26772A1D883E895A_AdjustorThunk (void);
extern void ValueStopwatch_get_ElapsedTicks_m191300A5FC932D5C0BAFE98BF7BE560DFFB6EA10_AdjustorThunk (void);
extern void AsyncUniTaskMethodBuilder_get_Task_m2F98008CBCDF840A80869042FF5620775C3E3ED7_AdjustorThunk (void);
extern void AsyncUniTaskMethodBuilder_SetException_m7153CFFA0C20E8036CC35A0ECDD57649B1807A6C_AdjustorThunk (void);
extern void AsyncUniTaskMethodBuilder_SetResult_m3018C0048802DBD765F7796F44FED9C706E988F6_AdjustorThunk (void);
extern void AsyncUniTaskMethodBuilder_SetStateMachine_mA8DEE00670CB37DB2DA98D16AF514F9B4B19B141_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[28] = 
{
	{ 0x06000002, AsyncUnit_GetHashCode_m313965D898D515FE5DF5464ED8C0C9EF64B9F0C6_AdjustorThunk },
	{ 0x06000003, AsyncUnit_Equals_m7F9CC10F7DDF7211EAD2D0A56B413C82156C2E07_AdjustorThunk },
	{ 0x06000004, AsyncUnit_ToString_m3BB12D2C87E38E4D5B1D1A6F3BD3EEC6F2A6488F_AdjustorThunk },
	{ 0x0600004B, UniTask__ctor_mA1D2FAF1D02391065D770F56EE3CFD028157CACD_AdjustorThunk },
	{ 0x0600004C, UniTask_get_Status_m5CCD7973CFD636CF45F67AF23CC34349ABBAD0AD_AdjustorThunk },
	{ 0x0600004D, UniTask_GetAwaiter_m1FE35EBA18DB40141C1025CECD4FC300E3A8C59C_AdjustorThunk },
	{ 0x0600004E, UniTask_ToString_m4BF6DB11E21177970C4F895B04F0D3ED6673A215_AdjustorThunk },
	{ 0x06000097, Awaiter__ctor_m5C4926AF1F0B75F44AC954B944D307066AA132CF_AdjustorThunk },
	{ 0x06000098, Awaiter_get_IsCompleted_mDCC71AAE7FA5EA2B303D3EC512177FD122C8DA49_AdjustorThunk },
	{ 0x06000099, Awaiter_GetResult_mFCA8BA81A9A7DA641DEFC7FC121FED87A3EC06CA_AdjustorThunk },
	{ 0x0600009A, Awaiter_OnCompleted_m080A1DBE2E806E1CDE6931514C3A6C8DBDF2E646_AdjustorThunk },
	{ 0x0600009B, Awaiter_UnsafeOnCompleted_m1428D2FC0145C8CABB14E4B9A15F38B6AE34F8E5_AdjustorThunk },
	{ 0x0600009C, Awaiter_SourceOnCompleted_m71BEAA335CDD0FBD24F14F9B970B8E1B69AE01B4_AdjustorThunk },
	{ 0x060000A0, YieldAwaitable__ctor_mDFCED8E03E478AA784DCBAF67876091D6457590F_AdjustorThunk },
	{ 0x060000A1, YieldAwaitable_GetAwaiter_mABCF83C041AEE78B7915625989429CF87DC28EC9_AdjustorThunk },
	{ 0x060000A2, Awaiter__ctor_m05FC94348E722A3E503CD81CE62E02A5163A58F0_AdjustorThunk },
	{ 0x060000A3, Awaiter_get_IsCompleted_mF92B16767983ACC6C69D07E32F866F14E3E3A830_AdjustorThunk },
	{ 0x060000A4, Awaiter_GetResult_m5B7F9896A518DB6316AD7C1741AFA0441CA40B3F_AdjustorThunk },
	{ 0x060000A5, Awaiter_OnCompleted_mB06C744A459C12B974DF387EB24B33B8663524F7_AdjustorThunk },
	{ 0x060000A6, Awaiter_UnsafeOnCompleted_m7891BED0BBDFA9C626E10CD515326D3E1A859634_AdjustorThunk },
	{ 0x060000FC, Callback_Invoke_m380CCA09614F37D4ACF8EBFD996809A7E123E763_AdjustorThunk },
	{ 0x060003A2, ValueStopwatch__ctor_mD72CC04E4E48BADF6C4A8DD48DB9FC1A1AC42EF5_AdjustorThunk },
	{ 0x060003A3, ValueStopwatch_get_IsInvalid_m0A3D180006FB7784C23036BA26772A1D883E895A_AdjustorThunk },
	{ 0x060003A4, ValueStopwatch_get_ElapsedTicks_m191300A5FC932D5C0BAFE98BF7BE560DFFB6EA10_AdjustorThunk },
	{ 0x060003AA, AsyncUniTaskMethodBuilder_get_Task_m2F98008CBCDF840A80869042FF5620775C3E3ED7_AdjustorThunk },
	{ 0x060003AB, AsyncUniTaskMethodBuilder_SetException_m7153CFFA0C20E8036CC35A0ECDD57649B1807A6C_AdjustorThunk },
	{ 0x060003AC, AsyncUniTaskMethodBuilder_SetResult_m3018C0048802DBD765F7796F44FED9C706E988F6_AdjustorThunk },
	{ 0x060003AF, AsyncUniTaskMethodBuilder_SetStateMachine_mA8DEE00670CB37DB2DA98D16AF514F9B4B19B141_AdjustorThunk },
};
static const int32_t s_InvokerIndices[993] = 
{
	3119,
	3708,
	2685,
	3723,
	5263,
	4438,
	5263,
	5339,
	2336,
	1129,
	3104,
	3708,
	-1,
	5205,
	2727,
	2336,
	1129,
	3708,
	3104,
	3786,
	3747,
	5316,
	5310,
	5316,
	5327,
	5327,
	5267,
	3962,
	4551,
	5173,
	5339,
	4756,
	3860,
	4958,
	4966,
	5259,
	4966,
	5339,
	5327,
	5339,
	3786,
	2753,
	5339,
	3786,
	2753,
	2753,
	2753,
	5339,
	4981,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5342,
	4383,
	4385,
	4384,
	5240,
	-1,
	-1,
	5239,
	-1,
	5240,
	1842,
	3708,
	3834,
	3723,
	5339,
	3662,
	5339,
	3786,
	4321,
	3104,
	2336,
	3708,
	1129,
	3747,
	3747,
	5339,
	3786,
	3708,
	3662,
	5339,
	3786,
	4321,
	3104,
	2336,
	3708,
	1129,
	3747,
	3747,
	5339,
	3786,
	3708,
	3662,
	5339,
	3786,
	4321,
	3104,
	2336,
	3708,
	1129,
	3747,
	3747,
	5339,
	3786,
	3708,
	-1,
	3119,
	3104,
	2336,
	3708,
	1129,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3059,
	3104,
	2336,
	3708,
	1129,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1843,
	4972,
	3104,
	2336,
	3708,
	1129,
	5339,
	3786,
	3119,
	3051,
	3747,
	3786,
	3119,
	3119,
	1847,
	5339,
	3786,
	3776,
	3105,
	3842,
	3105,
	3747,
	3786,
	3119,
	3119,
	5263,
	5339,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3119,
	3723,
	3786,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5263,
	5339,
	3662,
	5339,
	3786,
	5316,
	4779,
	2693,
	3104,
	2336,
	3708,
	1129,
	3747,
	5339,
	3786,
	3708,
	3786,
	3786,
	3776,
	3747,
	3104,
	2336,
	3708,
	1129,
	2728,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5272,
	5339,
	3786,
	3119,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5263,
	5263,
	5339,
	5339,
	5339,
	3786,
	3776,
	3786,
	3668,
	3786,
	3786,
	3776,
	3786,
	3119,
	3747,
	5339,
	3786,
	3119,
	3786,
	3776,
	3786,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3776,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3642,
	3105,
	3723,
	2469,
	3642,
	2090,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3635,
	3142,
	3723,
	2469,
	3635,
	2082,
	3786,
	3635,
	3142,
	3723,
	2469,
	3635,
	2082,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3630,
	1843,
	3723,
	2469,
	3630,
	2077,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3638,
	3119,
	3723,
	2469,
	3638,
	2086,
	3786,
	3639,
	3119,
	3723,
	2469,
	3639,
	2087,
	3786,
	3638,
	3119,
	3723,
	2469,
	3638,
	2086,
	3786,
	3639,
	3119,
	3723,
	2469,
	3639,
	2087,
	3786,
	3638,
	3119,
	3723,
	2469,
	3638,
	2086,
	3786,
	3639,
	3119,
	3723,
	2469,
	3639,
	2087,
	3786,
	3640,
	3119,
	3723,
	2469,
	3640,
	2088,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3647,
	3148,
	3723,
	2469,
	3647,
	2094,
	3786,
	3643,
	3119,
	3723,
	2469,
	3643,
	2091,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3641,
	3119,
	3723,
	2469,
	3641,
	2089,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3645,
	3121,
	3723,
	2469,
	3645,
	2092,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3632,
	1847,
	3723,
	2469,
	3632,
	2079,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3636,
	3119,
	3723,
	2469,
	3636,
	2084,
	3786,
	3637,
	3119,
	3723,
	2469,
	3637,
	2085,
	3786,
	3636,
	3119,
	3723,
	2469,
	3636,
	2084,
	3786,
	3637,
	3119,
	3723,
	2469,
	3637,
	2085,
	3786,
	3636,
	3119,
	3723,
	2469,
	3636,
	2084,
	3786,
	3637,
	3119,
	3723,
	2469,
	3637,
	2085,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3776,
	3786,
	3723,
	2469,
	3776,
	2898,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3634,
	3119,
	3723,
	2469,
	3634,
	2081,
	3786,
	3634,
	3119,
	3723,
	2469,
	3634,
	2081,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3633,
	3119,
	3723,
	2469,
	3633,
	2080,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3646,
	3119,
	3723,
	2469,
	3646,
	2093,
	3786,
	3634,
	3119,
	3723,
	2469,
	3634,
	2081,
	3786,
	3634,
	3119,
	3723,
	2469,
	3634,
	2081,
	3786,
	3634,
	3119,
	3723,
	2469,
	3634,
	2081,
	3786,
	3105,
	3119,
	3786,
	3786,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3105,
	3119,
	3786,
	3786,
	5339,
	3786,
	3119,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	5335,
	3106,
	3747,
	3709,
	5339,
	3119,
	3786,
	3786,
	5297,
	3776,
	3119,
	3786,
	-1,
	-1,
	3119,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3723,
	3776,
	3786,
	3119,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[33] = 
{
	{ 0x02000034, { 0, 2 } },
	{ 0x02000036, { 2, 4 } },
	{ 0x0200003E, { 18, 5 } },
	{ 0x0200004B, { 23, 3 } },
	{ 0x0200004C, { 26, 2 } },
	{ 0x0200004E, { 28, 2 } },
	{ 0x02000053, { 30, 7 } },
	{ 0x02000057, { 46, 6 } },
	{ 0x02000058, { 52, 6 } },
	{ 0x02000061, { 58, 8 } },
	{ 0x02000062, { 66, 6 } },
	{ 0x02000063, { 72, 1 } },
	{ 0x02000065, { 73, 15 } },
	{ 0x020000FB, { 88, 5 } },
	{ 0x020000FF, { 97, 2 } },
	{ 0x02000100, { 99, 7 } },
	{ 0x02000101, { 106, 2 } },
	{ 0x02000102, { 108, 7 } },
	{ 0x02000106, { 119, 3 } },
	{ 0x02000109, { 126, 11 } },
	{ 0x0200010A, { 137, 5 } },
	{ 0x0200010B, { 142, 22 } },
	{ 0x0200010C, { 164, 5 } },
	{ 0x06000046, { 6, 5 } },
	{ 0x06000047, { 11, 2 } },
	{ 0x06000049, { 13, 5 } },
	{ 0x060000EA, { 37, 9 } },
	{ 0x06000395, { 93, 2 } },
	{ 0x06000396, { 95, 2 } },
	{ 0x060003AD, { 115, 3 } },
	{ 0x060003AE, { 118, 1 } },
	{ 0x060003B4, { 122, 3 } },
	{ 0x060003B5, { 125, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[169] = 
{
	{ (Il2CppRGCTXDataType)2, 1938 },
	{ (Il2CppRGCTXDataType)2, 13250 },
	{ (Il2CppRGCTXDataType)2, 13261 },
	{ (Il2CppRGCTXDataType)3, 36369 },
	{ (Il2CppRGCTXDataType)3, 36370 },
	{ (Il2CppRGCTXDataType)3, 36368 },
	{ (Il2CppRGCTXDataType)3, 46948 },
	{ (Il2CppRGCTXDataType)2, 8218 },
	{ (Il2CppRGCTXDataType)3, 17139 },
	{ (Il2CppRGCTXDataType)2, 16065 },
	{ (Il2CppRGCTXDataType)3, 36923 },
	{ (Il2CppRGCTXDataType)2, 16066 },
	{ (Il2CppRGCTXDataType)3, 36924 },
	{ (Il2CppRGCTXDataType)2, 7147 },
	{ (Il2CppRGCTXDataType)2, 7145 },
	{ (Il2CppRGCTXDataType)3, 7896 },
	{ (Il2CppRGCTXDataType)2, 16064 },
	{ (Il2CppRGCTXDataType)3, 36922 },
	{ (Il2CppRGCTXDataType)2, 7146 },
	{ (Il2CppRGCTXDataType)3, 7897 },
	{ (Il2CppRGCTXDataType)2, 16071 },
	{ (Il2CppRGCTXDataType)3, 36926 },
	{ (Il2CppRGCTXDataType)2, 7148 },
	{ (Il2CppRGCTXDataType)2, 7010 },
	{ (Il2CppRGCTXDataType)3, 7306 },
	{ (Il2CppRGCTXDataType)2, 1976 },
	{ (Il2CppRGCTXDataType)3, 36927 },
	{ (Il2CppRGCTXDataType)2, 13274 },
	{ (Il2CppRGCTXDataType)3, 36753 },
	{ (Il2CppRGCTXDataType)3, 36754 },
	{ (Il2CppRGCTXDataType)2, 16070 },
	{ (Il2CppRGCTXDataType)3, 36925 },
	{ (Il2CppRGCTXDataType)3, 36891 },
	{ (Il2CppRGCTXDataType)3, 36890 },
	{ (Il2CppRGCTXDataType)3, 36889 },
	{ (Il2CppRGCTXDataType)3, 36888 },
	{ (Il2CppRGCTXDataType)3, 36887 },
	{ (Il2CppRGCTXDataType)2, 16059 },
	{ (Il2CppRGCTXDataType)3, 36884 },
	{ (Il2CppRGCTXDataType)2, 15988 },
	{ (Il2CppRGCTXDataType)3, 36324 },
	{ (Il2CppRGCTXDataType)2, 8472 },
	{ (Il2CppRGCTXDataType)3, 17758 },
	{ (Il2CppRGCTXDataType)2, 11376 },
	{ (Il2CppRGCTXDataType)3, 36885 },
	{ (Il2CppRGCTXDataType)3, 36886 },
	{ (Il2CppRGCTXDataType)2, 15989 },
	{ (Il2CppRGCTXDataType)2, 15989 },
	{ (Il2CppRGCTXDataType)3, 36895 },
	{ (Il2CppRGCTXDataType)3, 36896 },
	{ (Il2CppRGCTXDataType)3, 36897 },
	{ (Il2CppRGCTXDataType)3, 36325 },
	{ (Il2CppRGCTXDataType)2, 8473 },
	{ (Il2CppRGCTXDataType)2, 8473 },
	{ (Il2CppRGCTXDataType)3, 36892 },
	{ (Il2CppRGCTXDataType)3, 36894 },
	{ (Il2CppRGCTXDataType)3, 36893 },
	{ (Il2CppRGCTXDataType)3, 17759 },
	{ (Il2CppRGCTXDataType)3, 36366 },
	{ (Il2CppRGCTXDataType)2, 7041 },
	{ (Il2CppRGCTXDataType)3, 7380 },
	{ (Il2CppRGCTXDataType)3, 36364 },
	{ (Il2CppRGCTXDataType)3, 36365 },
	{ (Il2CppRGCTXDataType)3, 36367 },
	{ (Il2CppRGCTXDataType)2, 6965 },
	{ (Il2CppRGCTXDataType)3, 5512 },
	{ (Il2CppRGCTXDataType)3, 5515 },
	{ (Il2CppRGCTXDataType)2, 6966 },
	{ (Il2CppRGCTXDataType)3, 5514 },
	{ (Il2CppRGCTXDataType)3, 5418 },
	{ (Il2CppRGCTXDataType)3, 5513 },
	{ (Il2CppRGCTXDataType)2, 6966 },
	{ (Il2CppRGCTXDataType)3, 5419 },
	{ (Il2CppRGCTXDataType)3, 36748 },
	{ (Il2CppRGCTXDataType)3, 36752 },
	{ (Il2CppRGCTXDataType)3, 5416 },
	{ (Il2CppRGCTXDataType)2, 6967 },
	{ (Il2CppRGCTXDataType)2, 6967 },
	{ (Il2CppRGCTXDataType)3, 5605 },
	{ (Il2CppRGCTXDataType)3, 36749 },
	{ (Il2CppRGCTXDataType)3, 5417 },
	{ (Il2CppRGCTXDataType)3, 36745 },
	{ (Il2CppRGCTXDataType)3, 36750 },
	{ (Il2CppRGCTXDataType)2, 13267 },
	{ (Il2CppRGCTXDataType)3, 36746 },
	{ (Il2CppRGCTXDataType)3, 36751 },
	{ (Il2CppRGCTXDataType)3, 36747 },
	{ (Il2CppRGCTXDataType)3, 5604 },
	{ (Il2CppRGCTXDataType)2, 17054 },
	{ (Il2CppRGCTXDataType)3, 29179 },
	{ (Il2CppRGCTXDataType)3, 29180 },
	{ (Il2CppRGCTXDataType)3, 29182 },
	{ (Il2CppRGCTXDataType)3, 29181 },
	{ (Il2CppRGCTXDataType)3, 34381 },
	{ (Il2CppRGCTXDataType)2, 15447 },
	{ (Il2CppRGCTXDataType)3, 34389 },
	{ (Il2CppRGCTXDataType)2, 15450 },
	{ (Il2CppRGCTXDataType)3, 34382 },
	{ (Il2CppRGCTXDataType)2, 15449 },
	{ (Il2CppRGCTXDataType)2, 15448 },
	{ (Il2CppRGCTXDataType)3, 9279 },
	{ (Il2CppRGCTXDataType)2, 15454 },
	{ (Il2CppRGCTXDataType)3, 34397 },
	{ (Il2CppRGCTXDataType)3, 9278 },
	{ (Il2CppRGCTXDataType)2, 7399 },
	{ (Il2CppRGCTXDataType)3, 9277 },
	{ (Il2CppRGCTXDataType)3, 34390 },
	{ (Il2CppRGCTXDataType)2, 15452 },
	{ (Il2CppRGCTXDataType)2, 15451 },
	{ (Il2CppRGCTXDataType)3, 9282 },
	{ (Il2CppRGCTXDataType)2, 15457 },
	{ (Il2CppRGCTXDataType)3, 34402 },
	{ (Il2CppRGCTXDataType)3, 9281 },
	{ (Il2CppRGCTXDataType)2, 7401 },
	{ (Il2CppRGCTXDataType)3, 9280 },
	{ (Il2CppRGCTXDataType)3, 6505 },
	{ (Il2CppRGCTXDataType)2, 7003 },
	{ (Il2CppRGCTXDataType)2, 991 },
	{ (Il2CppRGCTXDataType)2, 75 },
	{ (Il2CppRGCTXDataType)2, 13226 },
	{ (Il2CppRGCTXDataType)3, 46954 },
	{ (Il2CppRGCTXDataType)3, 46960 },
	{ (Il2CppRGCTXDataType)3, 6958 },
	{ (Il2CppRGCTXDataType)2, 7007 },
	{ (Il2CppRGCTXDataType)2, 992 },
	{ (Il2CppRGCTXDataType)2, 76 },
	{ (Il2CppRGCTXDataType)3, 6504 },
	{ (Il2CppRGCTXDataType)3, 6503 },
	{ (Il2CppRGCTXDataType)2, 7000 },
	{ (Il2CppRGCTXDataType)3, 35747 },
	{ (Il2CppRGCTXDataType)2, 7000 },
	{ (Il2CppRGCTXDataType)3, 6502 },
	{ (Il2CppRGCTXDataType)1, 7000 },
	{ (Il2CppRGCTXDataType)2, 5564 },
	{ (Il2CppRGCTXDataType)3, 1 },
	{ (Il2CppRGCTXDataType)3, 35748 },
	{ (Il2CppRGCTXDataType)2, 1327 },
	{ (Il2CppRGCTXDataType)2, 5580 },
	{ (Il2CppRGCTXDataType)3, 22 },
	{ (Il2CppRGCTXDataType)2, 5580 },
	{ (Il2CppRGCTXDataType)2, 7002 },
	{ (Il2CppRGCTXDataType)3, 35749 },
	{ (Il2CppRGCTXDataType)3, 6957 },
	{ (Il2CppRGCTXDataType)3, 6956 },
	{ (Il2CppRGCTXDataType)2, 7004 },
	{ (Il2CppRGCTXDataType)3, 35750 },
	{ (Il2CppRGCTXDataType)2, 7004 },
	{ (Il2CppRGCTXDataType)3, 6954 },
	{ (Il2CppRGCTXDataType)1, 7004 },
	{ (Il2CppRGCTXDataType)2, 5611 },
	{ (Il2CppRGCTXDataType)3, 228 },
	{ (Il2CppRGCTXDataType)3, 36758 },
	{ (Il2CppRGCTXDataType)3, 35751 },
	{ (Il2CppRGCTXDataType)2, 1329 },
	{ (Il2CppRGCTXDataType)3, 36762 },
	{ (Il2CppRGCTXDataType)2, 16075 },
	{ (Il2CppRGCTXDataType)3, 36928 },
	{ (Il2CppRGCTXDataType)3, 36760 },
	{ (Il2CppRGCTXDataType)3, 36759 },
	{ (Il2CppRGCTXDataType)3, 36755 },
	{ (Il2CppRGCTXDataType)3, 6955 },
	{ (Il2CppRGCTXDataType)3, 36756 },
	{ (Il2CppRGCTXDataType)3, 36761 },
	{ (Il2CppRGCTXDataType)3, 36757 },
	{ (Il2CppRGCTXDataType)2, 5616 },
	{ (Il2CppRGCTXDataType)3, 239 },
	{ (Il2CppRGCTXDataType)2, 5616 },
	{ (Il2CppRGCTXDataType)2, 7006 },
	{ (Il2CppRGCTXDataType)3, 35752 },
};
extern const CustomAttributesCacheGenerator g_UniTask_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UniTask_CodeGenModule;
const Il2CppCodeGenModule g_UniTask_CodeGenModule = 
{
	"UniTask.dll",
	993,
	s_methodPointers,
	28,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	33,
	s_rgctxIndices,
	169,
	s_rgctxValues,
	NULL,
	g_UniTask_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
